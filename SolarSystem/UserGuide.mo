within SolarSystem;
model UserGuide "What can you expect from this package"
  extends Modelica.Icons.Information;
  annotation (Documentation(info="<html>
<p>The SolarSystem library is a Buildings library extension.</p>
<p>What you can expect ?</p>
<p><b>Package Content</b></p>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"1\"><tr>
<td><p align=\"center\"><h4>Name</h4></p></td>
<td><p align=\"center\"><h4>Description</h4></p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.UserGuide\">UserGuide</a> </p></td>
<td><p>What can you expect from this package</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.Models\">Models</a> </p></td>
<td><p>Solars systems define to run simulation or validate models</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.Solar_Collector\">Solar_Collector</a> </p></td>
<td><p>Solar collectors models</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.Exchanger_and_Tank\">Exchanger_and_Tank</a> </p></td>
<td><p>Tanks with exchanger models</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.Valves\">Valves</a> </p></td>
<td><p>Valves to control flow direction</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.House\">House</a> </p></td>
<td><p>Extension of Buildings House models</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.Heat_transfer\">Heat_transfer</a> </p></td>
<td><p>Extending of Buildings transmitters</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.Control\">Control</a> </p></td>
<td><p>Useful models to control solar systems</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.Media\">Media</a> </p></td>
<td><p>New media based on Modelica and Buildings media</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.Utilities\">Utilities</a> </p></td>
<td><p>Useful tools extending Modelica and Buildings library</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.Data\">Data</a> </p></td>
<td><p>Data used inside other models</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.BaseClasses\">BaseClasses</a> </p></td>
<td><p>Extending of Buildings library Collector BaseClasses</p></td>
</tr>
<tr>
<td><p><a href=\"Modelica://SolarSystem.Testing\">Testing</a> </p></td>
<td><p>Only used to test specific element in a safe space</p></td>
</tr>
</table>
<p><br><br>This library is hightly unstable, and in constant improvement.</p>
</html>"));
end UserGuide;
