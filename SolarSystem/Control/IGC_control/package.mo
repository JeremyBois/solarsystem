within SolarSystem.Control;
package IGC_control "Algorithm to control regulation for IGC solar system"
  extends Modelica.Icons.VariantsPackage;


annotation (Documentation(info="<html>
<p>Contains modified algorithm to allow temperature scheduled as temperature instruction.</p>
</html>"));
end IGC_control;
