within SolarSystem.Control.IGC_control.Instruction_variable;
model Chauff_state

  import SI = Modelica.SIunits;

 parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states : true = ON, false = OFF)";

  Modelica.Blocks.Interfaces.BooleanOutput Chauff[n](each start=true)
    "ON = true, OFF = false" annotation (Placement(transformation(extent={{160,-20},
            {200,20}}), iconTransformation(extent={{160,-20},{200,20}})));
  Modelica.Blocks.Logical.LessEqual     lessEqualThreshold1[n]
    annotation (Placement(transformation(extent={{36,-60},{56,-40}})));
  Modelica.Blocks.Interfaces.RealVectorInput
                                       Tambiant[n] "T. in the room"
    annotation (Placement(transformation(extent={{-120,-70},{-80,-30}}),
        iconTransformation(extent={{-124,-10},{-84,30}})));
  Modelica.Blocks.Interfaces.RealInput  DTeco(start=0) "Return DTeco value"
    annotation (Placement(transformation(extent={{-120,0},{-80,40}}),
        iconTransformation(extent={{-120,40},{-80,80}})));
  Modelica.Blocks.Logical.Pre breaker1[n](each pre_u_start=true)
    "Break algebraic loop to avoid infinite loop" annotation (Placement(
        transformation(
        extent={{-8,-8},{8,8}},
        rotation=-90,
        origin={-20,58})));
  Modelica.Blocks.Math.BooleanToReal CHAUFF_real[n] annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={-20,30})));
  Modelica.Blocks.Routing.Replicator replicator2(
                                                nout=n) annotation (
      Placement(transformation(extent={{-70,14},{-56,26}})));
  Modelica.Blocks.Math.Add3 compare[n](
    each k1=1,
    each k2=-1,
    each k3=0.25)
                 annotation (Placement(transformation(extent={{-40,-20},{-20,0}})));
  Modelica.Blocks.Interfaces.BooleanInput  ECS "ON = true, OFF = false"
    annotation (Placement(transformation(extent={{-120,-100},{-80,-60}}),
        iconTransformation(extent={{-120,-100},{-80,-60}})));
  Modelica.Blocks.MathBoolean.Not opposite
    annotation (Placement(transformation(extent={{-22,-86},{-8,-74}})));
  Buildings.Utilities.Math.BooleanReplicator replicator1(nout=n)
                                                        annotation (
      Placement(transformation(extent={{0,-86},{14,-74}})));
  Modelica.Blocks.Logical.And conditions[n]
    "If S4 flow rate is minimum and deltaT_test false, it's false otherwise it's true "
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Logical.Pre breaker2 annotation (Placement(
        transformation(extent={{-56,-86},{-42,-74}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n]
    "Tinstruction in the room" annotation (Placement(transformation(extent={{
            -120,-30},{-80,10}}), iconTransformation(extent={{-124,-60},{-84,
            -20}})));
equation

  connect(Tambiant, lessEqualThreshold1.u1) annotation (Line(
      points={{-100,-50},{34,-50}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(breaker1.y, CHAUFF_real.u) annotation (Line(
      points={{-20,49.2},{-20,37.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Chauff, breaker1.u) annotation (Line(
      points={{180,0},{184,0},{184,80},{-20,80},{-20,67.6}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(DTeco, replicator2.u) annotation (Line(
      points={{-100,20},{-71.4,20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(compare.y, lessEqualThreshold1.u2) annotation (Line(
      points={{-19,-10},{-16,-10},{-16,-58},{34,-58}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(replicator2.y, compare.u2) annotation (Line(
      points={{-55.3,20},{-50,20},{-50,-10},{-42,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(CHAUFF_real.y, compare.u3) annotation (Line(
      points={{-20,23.4},{-20,8},{-60,8},{-60,-18},{-42,-18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(opposite.y, replicator1.u) annotation (Line(
      points={{-6.6,-80},{-1.4,-80}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator1.y, conditions.u2) annotation (Line(
      points={{14.7,-80},{20,-80},{20,-30},{80,-30},{80,-8},{98,-8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(lessEqualThreshold1.y, conditions.u1) annotation (Line(
      points={{57,-50},{60,-50},{60,0},{98,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(conditions.y, Chauff) annotation (Line(
      points={{121,0},{180,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(breaker2.y, opposite.u) annotation (Line(
      points={{-41.3,-80},{-24.8,-80}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(ECS, breaker2.u) annotation (Line(
      points={{-100,-80},{-57.4,-80}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tinstruction, compare.u1) annotation (Line(
      points={{-100,-10},{-72,-10},{-72,-2},{-42,-2}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-100},{180,80}},
          preserveAspectRatio=false),
                      graphics), Icon(coordinateSystem(extent={{-100,-100},{180,
            80}},  preserveAspectRatio=false),
                    graphics={
        Polygon(
          points={{-100,80},{180,80},{180,-100},{-100,-100},{-100,80},{-100,80},
              {-100,80}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-68,44},{146,-62}},
          lineColor={0,128,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          textString="Nb Room
%n")}));
end Chauff_state;
