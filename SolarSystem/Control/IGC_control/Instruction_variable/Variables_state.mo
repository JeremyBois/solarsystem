within SolarSystem.Control.IGC_control.Instruction_variable;
model Variables_state "Set variables used to process computer control"

  import SI = Modelica.SIunits;

parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states : true = ON, false = OFF)";
//Computer control of variables (usefulVariables)
parameter SI.ThermodynamicTemperature pad=276.15
    "Used to compute TsolarInstruction";

  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-120,68},{-80,108}}),
        iconTransformation(extent={{-120,68},{-80,108}})));
  Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-120,28},{-80,68}}),
        iconTransformation(extent={{-120,28},{-80,68}})));
  Modelica.Blocks.Logical.Switch DTeco_switch "Switch between DTeco values"
    annotation (Placement(transformation(extent={{52,62},{72,82}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=0.3) "If true"
    annotation (Placement(transformation(extent={{12,74},{32,94}})));
  Modelica.Blocks.Sources.RealExpression realExpression1(y=0) "If false"
    annotation (Placement(transformation(extent={{12,44},{32,64}})));
  Modelica.Blocks.Interfaces.RealOutput DTeco(start=0) "Return DTeco value"
    annotation (Placement(transformation(extent={{140,52},{180,92}}),
        iconTransformation(extent={{164,42},{200,78}})));
  Modelica.Blocks.Interfaces.RealInput Text "T. from outdoor"
    annotation (Placement(transformation(extent={{-120,-10},{-80,30}}),
        iconTransformation(extent={{-120,-10},{-80,30}})));
  Modelica.Blocks.Interfaces.RealOutput TsolarInstruction[n]
    annotation (Placement(transformation(extent={{140,-40},{180,0}}),
        iconTransformation(extent={{164,-18},{200,18}})));
  Modelica.Blocks.Interfaces.RealOutput T1_out "T. after solar panels"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={70,-40}),
        iconTransformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={20,-44})));
  Modelica.Blocks.Interfaces.RealOutput T7_out "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={30,-40}),
        iconTransformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={80,-44})));
  Modelica.Blocks.Routing.RealPassThrough T7_pass "Just pass the signal"
    annotation (Placement(transformation(extent={{-20,-20},{0,0}})));
  Modelica.Blocks.Routing.RealPassThrough T1_pass "Just pass the signal"
    annotation (Placement(transformation(extent={{-20,10},{0,30}})));
  Modelica.Blocks.Interfaces.RealInput Text_mini
    "Outdoor day average temperature"
    annotation (Placement(transformation(extent={{-120,-50},{-80,-10}}),
        iconTransformation(extent={{-120,-48},{-80,-8}})));
  Modelica.Blocks.Logical.GreaterEqual
                                 DTeco_switch1 "Switch between DTeco values"
    annotation (Placement(transformation(extent={{-16,62},{4,82}})));
  Modelica.Blocks.Sources.RealExpression compare(y=T7 + 5 - 15*DTeco)
    annotation (Placement(transformation(extent={{-60,48},{-40,68}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n]
    "Tinstruction in the room" annotation (Placement(transformation(extent={{-68,82},
            {-32,118}}),     iconTransformation(extent={{-60,90},{-32,118}})));
equation

// Unit conversion to use input and set output
for i in 1:n loop
TsolarInstruction[i] = Tinstruction[i];
end for;

  connect(realExpression1.y, DTeco_switch.u3) annotation (Line(
      points={{33,54},{42,54},{42,64},{50,64}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(realExpression.y, DTeco_switch.u1) annotation (Line(
      points={{33,84},{40,84},{40,80},{50,80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(DTeco_switch.y, DTeco) annotation (Line(
      points={{73,72},{160,72}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1_out, T1_out) annotation (Line(
      points={{70,-40},{70,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7_pass.y, T7_out) annotation (Line(
      points={{1,-10},{30,-10},{30,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1_pass.y, T1_out) annotation (Line(
      points={{1,20},{70,20},{70,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, T1_pass.u) annotation (Line(
      points={{-100,88},{-66,88},{-66,20},{-22,20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7, T7_pass.u) annotation (Line(
      points={{-100,48},{-72,48},{-72,-10},{-22,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(DTeco_switch1.y, DTeco_switch.u2) annotation (Line(
      points={{5,72},{50,72}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T1, DTeco_switch1.u1) annotation (Line(
      points={{-100,88},{-66,88},{-66,72},{-18,72}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(compare.y, DTeco_switch1.u2) annotation (Line(
      points={{-39,58},{-26,58},{-26,64},{-18,64}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-40},{180,100}},
          preserveAspectRatio=false),
                      graphics), Icon(coordinateSystem(extent={{-100,-40},{180,
            100}}, preserveAspectRatio=false),
                    graphics={
        Polygon(
          points={{-100,100},{180,100},{180,-40},{-100,-40},{-100,100},
              {-100,100},{-100,100}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-14,76},{96,40}},
          lineColor={0,128,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          textString="%DTeco"),
        Text(
          extent={{-62,22},{138,-10}},
          lineColor={0,128,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          textString="%TsolarInstruction")}));
end Variables_state;
