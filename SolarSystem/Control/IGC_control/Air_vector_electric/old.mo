within SolarSystem.Control.IGC_control.Air_vector_electric;
package old
  model Heating_mod

    import SI = Modelica.SIunits;
  parameter Integer n = 2
      "Number of heated rooms (used to sizing vector holding the rooms pumps states : true = ON, false = OFF)";

    Modelica.Blocks.Interfaces.RealInput Tambiant[n] "T. in the room"
      annotation (Placement(transformation(extent={{-120,110},{-80,150}}),
          iconTransformation(extent={{-112,104},{-80,136}})));
    Modelica.Blocks.Interfaces.RealInput  DTeco "Return DTeco value"
      annotation (Placement(transformation(extent={{-120,70},{-80,110}}),
          iconTransformation(extent={{-112,64},{-80,96}})));
    Modelica.Blocks.Interfaces.BooleanInput V3V_solar
      "Open to solar panels = true, Open to storage tank = false"
      annotation (Placement(transformation(extent={{-120,-50},{-80,-10}}),
          iconTransformation(extent={{-114,-78},{-80,-44}})));
    Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
      annotation (Placement(transformation(extent={{-122,-270},{-82,-230}}),
          iconTransformation(extent={{-112,-266},{-78,-232}})));
    Modelica.Blocks.Interfaces.RealInput T8
      "T. at heating start point (just before extra exchanger input)"
      annotation (Placement(transformation(extent={{-122,-240},{-82,-200}}),
          iconTransformation(extent={{-112,-306},{-78,-272}})));
    Modelica.Blocks.Interfaces.RealInput TsolarInstruction[n] annotation (
        Placement(transformation(extent={{-120,30},{-80,70}}), iconTransformation(
            extent={{-112,24},{-80,56}})));

    Modelica.Blocks.MathBoolean.Or all_on[n](each nu=2)
      annotation (Placement(transformation(extent={{180,40},{200,60}})));
    Modelica.Blocks.Math.Add3 deltaT_2[n](
      each k1=1,
      each k2=1,
      each k3=-1) "Tinstruction + 0.1 - DTeco"
      annotation (Placement(transformation(extent={{20,38},{38,56}})));
    Modelica.Blocks.MathBoolean.And on2[n](each nu=3)
      annotation (Placement(transformation(extent={{140,40},{160,60}})));
    Modelica.Blocks.Logical.Less deltaT_2_test[n]
      annotation (Placement(transformation(extent={{60,40},{80,60}})));
    Modelica.Blocks.Logical.Less delta_T3[n]
      annotation (Placement(transformation(extent={{60,0},{80,20}})));
    Modelica.Blocks.MathBoolean.And on3[n](each nu=3)
      annotation (Placement(transformation(extent={{140,0},{160,20}})));
    Buildings.Utilities.Math.BooleanReplicator
                                       replicator4(
                                                  nout=n) annotation (
        Placement(transformation(extent={{-60,-40},{-40,-20}})));
    Modelica.Blocks.Logical.Greater deltaT_test[n]
      annotation (Placement(transformation(extent={{60,-208},{80,-188}})));
    Modelica.Blocks.Logical.Or or1[n]
      annotation (Placement(transformation(extent={{100,-208},{120,-188}})));
    Modelica.Blocks.Math.Add      deltaT_5(k1=+1, k2=-1) "T8 - T7"
      annotation (Placement(transformation(extent={{-54,-236},{-34,-216}})));
    Modelica.Blocks.Logical.LessThreshold deltaT_4_test(threshold=2.5)
      annotation (Placement(transformation(extent={{-10,-240},{10,-220}})));
    Buildings.Utilities.Math.BooleanReplicator
                                       replicator9(
                                                  nout=n) annotation (
        Placement(transformation(extent={{60,-240},{80,-220}})));
    Modelica.Blocks.Logical.Not not_off2[n]
      annotation (Placement(transformation(extent={{180,-208},{200,-188}})));
    Modelica.Blocks.MathBoolean.And off2[n](each nu=2)
      annotation (Placement(transformation(extent={{140,-208},{160,-188}})));
    Modelica.Blocks.Logical.Not not_solar[
                                         n] "V3V to tank"
      annotation (Placement(transformation(extent={{-6,-48},{6,-36}})));
    Modelica.Blocks.MathBoolean.And final_state[n](each nu=3)
      annotation (Placement(transformation(extent={{240,-72},{264,-48}})));
    Modelica.Blocks.Logical.GreaterEqualThreshold T5_test[n](each threshold=
          273.15 + 40)
      annotation (Placement(transformation(extent={{60,-80},{80,-60}})));
    Modelica.Blocks.Logical.Greater          T1_test[n]
      annotation (Placement(transformation(extent={{60,-120},{80,-100}})));
    Modelica.Blocks.Routing.Replicator replicator5(
                                                  nout=n) annotation (
        Placement(transformation(extent={{20,-80},{40,-60}})));
    Modelica.Blocks.Routing.Replicator replicator6(
                                                  nout=n) annotation (
        Placement(transformation(extent={{20,-104},{36,-88}})));
    Modelica.Blocks.Logical.Or or2[n]
      annotation (Placement(transformation(extent={{100,-280},{120,-260}})));
    Modelica.Blocks.Math.Add add2[n](each k1=1, each k2=1)
      "TsolarInstruction + 0.2"
      annotation (Placement(transformation(extent={{-10,-288},{10,-268}})));
    Modelica.Blocks.Sources.RealExpression cst2[n](each y=0.3)
      "Minimal temperature inside each rooms"
      annotation (Placement(transformation(extent={{-62,-272},{-44,-254}})));
    Modelica.Blocks.Logical.Greater add_test[n]
      annotation (Placement(transformation(extent={{58,-288},{78,-268}})));
    Modelica.Blocks.MathBoolean.And off3[n](each nu=2)
      annotation (Placement(transformation(extent={{140,-280},{160,-260}})));
    Modelica.Blocks.Logical.Not not_off3[n]
      annotation (Placement(transformation(extent={{180,-280},{200,-260}})));
    Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
      annotation (Placement(transformation(extent={{-120,-90},{-80,-50}}),
          iconTransformation(extent={{-114,-192},{-82,-160}})));
    Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
      annotation (Placement(transformation(extent={{-120,-120},{-80,-80}}),
          iconTransformation(extent={{-114,-150},{-82,-118}})));

    Utilities.Timers.Wait_for wait_for "T8 - T7 timer"
      annotation (Placement(transformation(extent={{20,-240},{40,-220}})));
    Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S5(start=true)
      "ON = true, OFF = false (pump S5)"
      annotation (Placement(transformation(extent={{300,-260},{340,-220}}),
          iconTransformation(extent={{300,-260},{340,-220}})));

    Modelica.Blocks.Interfaces.BooleanOutput pumpControl_Sj[
                                                           n](each start=true)
      "ON = true, OFF = false (pump Sj)"
      annotation (Placement(transformation(extent={{310,-80},{350,-40}}),
          iconTransformation(extent={{300,-90},{340,-50}})));

    Utilities.Logical.True_seeker true_seeker(n=n)
      annotation (Placement(transformation(extent={{252,-226},{292,-194}})));

    Modelica.Blocks.Routing.Replicator replicator1(
                                                  nout=n) annotation (
        Placement(transformation(extent={{-64,80},{-44,100}})));
    Modelica.Blocks.Sources.RealExpression cst1[n](each y=0.2)
      annotation (Placement(transformation(extent={{-6,42},{6,52}})));
    Modelica.Blocks.Math.Add add1[n](each k1=1, each k2=1)
      "TsolarInstruction + 0.2"
      annotation (Placement(transformation(extent={{18,-200},{38,-180}})));
    Utilities.Timers.Stay_on stay_on[n](each pause=60, out_value(each start=false))
      annotation (Placement(transformation(extent={{280,-68},{296,-52}})));
    Modelica.Blocks.Math.Max max_28_T7
      "Return the minimal value between T3 and T4"
      annotation (Placement(transformation(extent={{-70,-132},{-52,-114}})));
    Modelica.Blocks.Sources.RealExpression cst3(each y=40 + 273.15)
      annotation (Placement(transformation(extent={{-100,-126},{-78,-110}})));
    Modelica.Blocks.Math.Add add4(each k2=1, each k1=1)
      "TsolarInstruction + 0.2"
      annotation (Placement(transformation(extent={{-14,-128},{2,-112}})));
    Modelica.Blocks.Sources.RealExpression cst4(each y=5)
      annotation (Placement(transformation(extent={{-100,-162},{-78,-146}})));
    Modelica.Blocks.Routing.Replicator replicator2(
                                                  nout=n) annotation (
        Placement(transformation(extent={{20,-128},{36,-112}})));
    Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n]
      "Tinstruction in the room" annotation (Placement(transformation(extent={{-120,
              -200},{-80,-160}}), iconTransformation(extent={{-114,-222},{-92,-200}})));
  equation
    connect(Tambiant, deltaT_2_test.u1)
                                   annotation (Line(
        points={{-100,130},{48,130},{48,50},{58,50}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tambiant,delta_T3. u1) annotation (Line(
        points={{-100,130},{48,130},{48,10},{58,10}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(delta_T3.y, on3.u[1])      annotation (Line(
        points={{81,10},{106,10},{106,14.6667},{140,14.6667}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(V3V_solar, replicator4.u)   annotation (Line(
        points={{-100,-30},{-62,-30}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(replicator4.y, on3.u[2])      annotation (Line(
        points={{-39,-30},{128,-30},{128,10},{140,10}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(on2.y, all_on.u[1])        annotation (Line(
        points={{161.5,50},{170,50},{170,53.5},{180,53.5}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(on3.y, all_on.u[2])        annotation (Line(
        points={{161.5,10},{168,10},{168,46.5},{180,46.5}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Tambiant, deltaT_test.u1)
                                    annotation (Line(
        points={{-100,130},{48,130},{48,-198},{58,-198}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(deltaT_test.y, or1.u1)     annotation (Line(
        points={{81,-198},{98,-198}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(deltaT_5.y, deltaT_4_test.u)
                                      annotation (Line(
        points={{-33,-226},{-24,-226},{-24,-230},{-12,-230}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(replicator9.y, or1.u2)       annotation (Line(
        points={{81,-230},{92,-230},{92,-206},{98,-206}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(replicator4.y,not_solar. u) annotation (Line(
        points={{-39,-30},{-22,-30},{-22,-42},{-7.2,-42}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(not_solar.y, off2.u[1])      annotation (Line(
        points={{6.6,-42},{116,-42},{116,-176},{128,-176},{128,-194.5},{140,-194.5}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(or1.y, off2.u[2])            annotation (Line(
        points={{121,-198},{124,-198},{124,-201.5},{140,-201.5}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(off2.y, not_off2.u)       annotation (Line(
        points={{161.5,-198},{178,-198}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(all_on.y, final_state.u[1]) annotation (Line(
        points={{201.5,50},{220,50},{220,-54.4},{240,-54.4}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(not_off2.y, final_state.u[2])
                                         annotation (Line(
        points={{201,-198},{226,-198},{226,-62},{240,-62},{240,-60}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(deltaT_2_test.y, on2.u[1]) annotation (Line(
        points={{81,50},{112,50},{112,54.6667},{140,54.6667}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(not_solar.y, on2.u[2])      annotation (Line(
        points={{6.6,-42},{124,-42},{124,50},{140,50}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(replicator5.y, T5_test.u)   annotation (Line(
        points={{41,-70},{58,-70}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T5_test.y, on2.u[3])        annotation (Line(
        points={{81,-70},{132,-70},{132,45.3333},{140,45.3333}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T1_test.y, on3.u[3])        annotation (Line(
        points={{81,-110},{136,-110},{136,5.33333},{140,5.33333}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(replicator9.y, or2.u1)       annotation (Line(
        points={{81,-230},{92,-230},{92,-270},{98,-270}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Tambiant, add_test.u1)  annotation (Line(
        points={{-100,130},{48,130},{48,-278},{56,-278}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(add_test.y, or2.u2)        annotation (Line(
        points={{79,-278},{98,-278}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(replicator4.y, off3.u[1])      annotation (Line(
        points={{-39,-30},{128,-30},{128,-266.5},{140,-266.5}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(or2.y, off3.u[2])            annotation (Line(
        points={{121,-270},{130,-270},{130,-273.5},{140,-273.5}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(off3.y, not_off3.u)       annotation (Line(
        points={{161.5,-270},{178,-270}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(not_off3.y, final_state.u[3])
                                         annotation (Line(
        points={{201,-270},{232,-270},{232,-65.6},{240,-65.6}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T5,replicator5. u) annotation (Line(
        points={{-100,-70},{18,-70}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(deltaT_4_test.y, wait_for.in_value) annotation (Line(
        points={{11,-230},{20,-230}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(wait_for.out_value, replicator9.u) annotation (Line(
        points={{41.4,-230},{58,-230}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(true_seeker.y, pumpControl_S5) annotation (Line(
        points={{292,-210},{306,-210},{306,-240},{320,-240}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(on3.y, true_seeker.u) annotation (Line(
        points={{161.5,10},{210,10},{210,-210},{252,-210}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(TsolarInstruction, delta_T3.u2) annotation (Line(
        points={{-100,50},{-26,50},{-26,2},{58,2}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(add2.y, add_test.u2) annotation (Line(
        points={{11,-278},{31.5,-278},{31.5,-286},{56,-286}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(cst2.y, add2.u2) annotation (Line(
        points={{-43.1,-263},{-30,-263},{-30,-284},{-12,-284}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(TsolarInstruction, add2.u1) annotation (Line(
        points={{-100,50},{-26,50},{-26,-272},{-12,-272}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(deltaT_2.y, deltaT_2_test.u2) annotation (Line(
        points={{38.9,47},{44,47},{44,42},{58,42}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(DTeco, replicator1.u) annotation (Line(
        points={{-100,90},{-66,90}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(replicator1.y, deltaT_2.u3) annotation (Line(
        points={{-43,90},{-34,90},{-34,39.8},{18.2,39.8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(cst1.y, deltaT_2.u2) annotation (Line(
        points={{6.6,47},{18.2,47}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(cst2.y, add1.u2) annotation (Line(
        points={{-43.1,-263},{-30,-263},{-30,-196},{16,-196}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(add1.y, deltaT_test.u2) annotation (Line(
        points={{39,-190},{44,-190},{44,-206},{58,-206}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(final_state.y, stay_on.in_value) annotation (Line(
        points={{265.8,-60},{280,-60}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(stay_on.out_value, pumpControl_Sj) annotation (Line(
        points={{297.6,-60},{330,-60}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T7, max_28_T7.u2) annotation (Line(
        points={{-102,-250},{-68,-250},{-68,-160},{-68,-146},{-80,-146},{-80,-128.4},
            {-76,-128.4},{-71.8,-128.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(cst3.y, max_28_T7.u1) annotation (Line(
        points={{-76.9,-118},{-74,-118},{-74,-117.6},{-71.8,-117.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(max_28_T7.y, add4.u1) annotation (Line(
        points={{-51.1,-123},{-46,-123},{-46,-115.2},{-15.6,-115.2}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(cst4.y, add4.u2) annotation (Line(
        points={{-76.9,-154},{-60,-154},{-60,-134},{-40,-134},{-40,-124.8},{-15.6,
            -124.8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(replicator6.y, T1_test.u1) annotation (Line(
        points={{36.8,-96},{48,-96},{48,-110},{58,-110}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(replicator2.y, T1_test.u2) annotation (Line(
        points={{36.8,-120},{48,-120},{48,-118},{58,-118}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(add4.y, replicator2.u) annotation (Line(
        points={{2.8,-120},{18.4,-120}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T1, replicator6.u) annotation (Line(
        points={{-100,-100},{13,-100},{13,-96},{18.4,-96}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tinstruction, add1.u1) annotation (Line(
        points={{-100,-180},{-20,-180},{-20,-184},{16,-184}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tinstruction, deltaT_2.u1) annotation (Line(
        points={{-100,-180},{-20,-180},{-20,54},{0,54},{0,54.2},{18.2,54.2}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T8, deltaT_5.u1) annotation (Line(
        points={{-102,-220},{-56,-220}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T7, deltaT_5.u2) annotation (Line(
        points={{-102,-250},{-68,-250},{-68,-232},{-56,-232}},
        color={0,0,127},
        smooth=Smooth.None));

    annotation (Diagram(coordinateSystem(extent={{-100,-300},{320,140}},
            preserveAspectRatio=false),  graphics), Icon(coordinateSystem(extent={{-100,
              -300},{320,140}}, preserveAspectRatio=false),  graphics={
          Text(
            extent={{-58,24},{284,-236}},
            lineColor={0,128,0},
            fillColor={0,0,0},
            fillPattern=FillPattern.Solid,
            textString="Nb Room
%n"),     Polygon(
            points={{-100,140},{320,140},{320,-300},{-100,-300},{-100,140},{-100,140},
                {-100,140}},
            lineColor={255,0,255},
            smooth=Smooth.None)}));
  end Heating_mod;

  model BackupHeater_mod "Control when we need to turn on the backup heater"

    Modelica.Blocks.Interfaces.BooleanOutput BackupHeater(start=false)
      "true = needed, false otherwise"
      annotation (Placement(transformation(extent={{140,-20},{180,20}}),
          iconTransformation(extent={{140,-20},{180,20}})));
    Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
      annotation (Placement(transformation(extent={{-120,-60},{-80,-20}}),
          iconTransformation(extent={{-120,-60},{-80,-20}})));
    Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
      annotation (Placement(transformation(extent={{-120,10},{-80,50}}),
          iconTransformation(extent={{-120,0},{-80,40}})));
    Modelica.Blocks.Logical.Or  sum_conditions
      annotation (Placement(transformation(extent={{60,-10},{80,10}})));
    Modelica.Blocks.Logical.OnOffController T3_test(bandwidth=1)
      annotation (Placement(transformation(extent={{0,20},{20,40}})));
    Modelica.Blocks.Logical.OnOffController T4_test(bandwidth=2.5)
      annotation (Placement(transformation(extent={{0,-60},{20,-40}})));
  protected
    Modelica.Blocks.Sources.RealExpression T3_set(each y=40)
      annotation (Placement(transformation(extent={{-40,34},{-22,50}})));
  protected
    Modelica.Blocks.Sources.RealExpression T4_set(each y=42.5)
      annotation (Placement(transformation(extent={{-44,-38},{-26,-22}})));
  equation
    connect(sum_conditions.y, BackupHeater) annotation (Line(
        points={{81,0},{160,0}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T3_set.y, T3_test.reference) annotation (Line(
        points={{-21.1,42},{-12,42},{-12,36},{-2,36}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T3, T3_test.u) annotation (Line(
        points={{-100,30},{-40,30},{-40,24},{-2,24}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T4_set.y, T4_test.reference) annotation (Line(
        points={{-25.1,-30},{-18,-30},{-18,-44},{-2,-44}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T4, T4_test.u) annotation (Line(
        points={{-100,-40},{-40,-40},{-40,-56},{-2,-56}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T3_test.y, sum_conditions.u1) annotation (Line(
        points={{21,30},{40,30},{40,0},{58,0}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T4_test.y, sum_conditions.u2) annotation (Line(
        points={{21,-50},{40,-50},{40,-8},{58,-8}},
        color={255,0,255},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-100,-80},{160,60}},
            preserveAspectRatio=false),graphics), Icon(coordinateSystem(
            extent={{-100,-80},{160,60}},   preserveAspectRatio=false),
                                             graphics={
          Polygon(
            points={{-100,60},{160,60},{160,-80},{-100,-80},{-100,60},{-100,60},{
                -100,60}},
            lineColor={255,0,255},
            smooth=Smooth.None),
          Text(
            extent={{-88,52},{124,-66}},
            lineColor={0,128,0},
            textString="Electrical heat tank control")}),
      Documentation(info="<html>
<p>Not currently used. Only PID to keep T4 at 40&deg;C</p>
</html>"));
  end BackupHeater_mod;

  model Regul_Tamb "Regulation of temperature inside the house"

  parameter Integer n=2
      "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
    Modelica.Blocks.Interfaces.RealVectorInput Tambiant[n] annotation (Placement(
          transformation(extent={{-120,20},{-80,60}}), iconTransformation(extent={
              {-112,28},{-90,50}})));
    Modelica.Blocks.Interfaces.RealVectorInput
                                         Tinstruction[n] annotation (Placement(
          transformation(extent={{-120,-20},{-80,20}}), iconTransformation(extent={{-112,
              -12},{-90,10}})));
    Modelica.Blocks.Interfaces.RealOutput fan_speed_out[n] "Heating fan speed"
      annotation (Placement(transformation(extent={{-20,-20},{20,20}}),
          iconTransformation(extent={{-12,4},{18,34}})));
    Buildings.Controls.Continuous.LimPID pid_fan[n](
      each yMax=yMax,
      each yMin=yMin,
      each controllerType=controllerType,
      each k=k,
      each Ti=Ti,
      each reverseAction=reverseAction,
      each Td=Td,
      each wp=wp,
      each wd=wd,
      each Ni=Ni) annotation (Placement(transformation(extent={{-60,10},{-40,-10}})));

    parameter Modelica.Blocks.Types.SimpleController controllerType=.Modelica.Blocks.Types.SimpleController.PID
      "Type of controller";
    parameter Real k=1 "Gain of controller";
    parameter Modelica.SIunits.Time Ti=0.5 "Time constant of Integrator block";
    parameter Modelica.SIunits.Time Td=0.1 "Time constant of Derivative block";
    parameter Real yMax=900/3600 "Upper limit of output";
    parameter Real yMin=100/3600 "Lower limit of output";
    parameter Real wp=1 "Set-point weight for Proportional block (0..1)";
    parameter Real wd=0 "Set-point weight for Derivative block (0..1)";
    parameter Real Ni=0.9 "Ni*Ti is time constant of anti-windup compensation";
    parameter Boolean reverseAction=false
      "Set to true for throttling the water flow rate through a cooling coil controller";
  equation
    connect(Tinstruction, pid_fan.u_s) annotation (Line(
        points={{-100,0},{-62,0}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tambiant, pid_fan.u_m) annotation (Line(
        points={{-100,40},{-50,40},{-50,12}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(pid_fan.y, fan_speed_out) annotation (Line(
        points={{-39,0},{0,0}},
        color={0,0,127},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
              -20},{0,60}}),     graphics), Icon(coordinateSystem(
            preserveAspectRatio=false, extent={{-100,-20},{0,60}}),     graphics={
          Polygon(
            points={{-100,60},{0,60},{0,-20},{-100,-20},{-100,60},{-100,60},{-100,
                60}},
            lineColor={255,0,255},
            smooth=Smooth.None),
          Text(
            extent={{-98,52},{-14,-12}},
            lineColor={0,128,0},
            textString="Fan speed control")}));
  end Regul_Tamb;

  model Regul_Tsouff_noSmooth "Regulation of temperature inside the house"

  parameter Integer n=2
      "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
  parameter Modelica.SIunits.Time wait_for=60
      "How much time we have to wait before the outter value can be true";
    parameter Real is_on_max=2950
      "Value where the pump speed considered at max speed (hight limit)";
    parameter Real is_on_min=2900
      "Value where the pump speed considered at max speed (low limit)";
    parameter Modelica.Blocks.Types.SimpleController Sj_controllerType=.Modelica.Blocks.Types.SimpleController.PID
      "Type of controller"
     annotation (Dialog(tab="Sj"));
    parameter Real Sj_k=1 "Gain of controller"
     annotation (Dialog(tab="Sj"));
    parameter Modelica.SIunits.Time Sj_Ti=0.5
      "Time constant of Integrator block"
     annotation (Dialog(tab="Sj"));
    parameter Modelica.SIunits.Time Sj_Td=0.1
      "Time constant of Derivative block"
     annotation (Dialog(tab="Sj"));
    parameter Real Sj_yMax=1 "Upper limit of speed"
     annotation (Dialog(tab="Sj"));
    parameter Real Sj_yMin=0.1 "Lower limit of speed"
     annotation (Dialog(tab="Sj"));
    parameter Real Sj_wp=1 "Set-point weight for Proportional block (0..1)"
     annotation (Dialog(tab="Sj"));
    parameter Real Sj_wd=0 "Set-point weight for Derivative block (0..1)"
     annotation (Dialog(tab="Sj"));
    parameter Real Sj_Ni=0.9
      "Ni*Ti is time constant of anti-windup compensation"
     annotation (Dialog(tab="Sj"));
    parameter Boolean Sj_reverseAction=false
      "Set to true for throttling the water flow rate through a cooling coil controller"
       annotation (Dialog(tab="Sj"));
    parameter Real Sj_Nd= 10
      "The higher Nd, the more ideal the derivative block"
      annotation (Dialog(tab="Sj"));
    Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n] annotation (Placement(
          transformation(extent={{-120,-80},{-80,-40}}),
                                                       iconTransformation(extent={{-114,
              -78},{-86,-50}})));
    Modelica.Blocks.Interfaces.RealVectorInput Tinstruction_souff[n] annotation (
        Placement(transformation(extent={{-120,-20},{-80,20}}),
          iconTransformation(extent={{-114,-8},{-86,20}})));
    Modelica.Blocks.Interfaces.RealOutput pump_speed_out[n]
      "Heating pumps speed"
      annotation (Placement(transformation(extent={{180,20},{220,60}}),
          iconTransformation(extent={{180,14},{210,44}})));
    Buildings.Controls.Continuous.LimPID pid_pump[n](
      each controllerType=Sj_controllerType,
      each k=Sj_k,
      each Ti=Sj_Ti,
      each Td=Sj_Td,
      each yMax=Sj_yMax,
      each yMin=Sj_yMin,
      each wp=Sj_wp,
      each wd=Sj_wd,
      each Ni=Sj_Ni,
      each reverseAction=Sj_reverseAction,
      each Nd=Sj_Nd)
      annotation (Placement(transformation(extent={{-72,32},{-52,12}})));
    Modelica.Blocks.Interfaces.BooleanVectorInput pump_state[n]
      "Heating pumps states" annotation (Placement(transformation(extent={{-120,-50},
              {-80,-10}}), iconTransformation(extent={{-114,-42},{-86,-14}})));
  protected
    Modelica.Blocks.Logical.Switch switch[n]
      "Set to zero the pump speed if pump must be off"
      annotation (Placement(transformation(extent={{-44,-8},{-24,12}})));
    Modelica.Blocks.Sources.RealExpression zeros[n](each y=0)
      annotation (Placement(transformation(extent={{-68,-12},{-54,0}})));
  public
    Utilities.Timers.Wait_for timer[n](each wait_for=wait_for)
      "Wait for 60 sec before "
      annotation (Placement(transformation(extent={{54,-28},{70,-12}})));
    Modelica.Blocks.Logical.Or pump_test[n]
      "Test if pump off or on for more than 60 sec"
      annotation (Placement(transformation(extent={{10,-32},{22,-20}})));
    Modelica.Blocks.Logical.Hysteresis       Full_speed[n](each uLow=is_on_min, each uHigh=
          is_on_max)
      annotation (Placement(transformation(extent={{-12,-52},{0,-40}})));
  protected
    Modelica.Blocks.Logical.OnOffController need_heat[n](each bandwidth=2)
      "True if Tsouff below Tinstruction_souff"
      annotation (Placement(transformation(extent={{-10,34},{10,54}})));
    Modelica.Blocks.Logical.And need_elec[n] "True if electric heater needed"
      annotation (Placement(transformation(extent={{34,-26},{46,-14}})));
  public
    parameter Modelica.Blocks.Types.SimpleController Elec_controllerType=.Modelica.Blocks.Types.SimpleController.PID
      "Type of controller" annotation (Dialog(tab="Elec"));
    parameter Real Elec_k=1 "Gain of controller" annotation (Dialog(tab="Elec"));
    parameter Modelica.SIunits.Time Elec_Ti=0.5
      "Time constant of Integrator block" annotation (Dialog(tab="Elec"));
    parameter Modelica.SIunits.Time Elec_Td=0.1
      "Time constant of Derivative block" annotation (Dialog(tab="Elec"));
    parameter Real Elec_yMax=1 "Upper limit of output"
      annotation (Dialog(tab="Elec"));
    parameter Real Elec_yMin=0 "Lower limit of output"
      annotation (Dialog(tab="Elec"));
    parameter Real Elec_wp=1 "Set-point weight for Proportional block (0..1)"
      annotation (Dialog(tab="Elec"));
    parameter Real Elec_wd=0 "Set-point weight for Derivative block (0..1)"
      annotation (Dialog(tab="Elec"));
    parameter Real Elec_Ni=0.9
      "Ni*Ti is time constant of anti-windup compensation"
      annotation (Dialog(tab="Elec"));
    parameter Real Elec_Nd=10
      "The higher Nd, the more ideal the derivative block"
      annotation (Dialog(tab="Elec"));
    parameter Boolean Elec_reverseAction=false
      "Set to true for throttling the water flow rate through a cooling coil controller"
      annotation (Dialog(tab="Elec"));
    parameter Real electrical_power[n]={5000, 5000}
      "Maximum value of eletrical power"
      annotation (Dialog(tab="Elec", group="Power"));
  public
    Buildings.Controls.Continuous.LimPID pid_Elec[n](
      each initType=Modelica.Blocks.Types.InitPID.InitialState,
      each controllerType=Elec_controllerType,
      each k=Elec_k,
      each Ti=Elec_Ti,
      each Td=Elec_Td,
      each yMax=Elec_yMax,
      each yMin=Elec_yMin,
      each wp=Elec_wp,
      each wd=Elec_wd,
      each Ni=Elec_Ni,
      each Nd=Elec_Nd,
      each reverseAction=Elec_reverseAction) "Controller for heating"
      annotation (Placement(transformation(extent={{92,-26},{104,-14}})));
  public
    Modelica.Blocks.Math.Gain gaiHea[n](k=electrical_power) "Gain for heating"
      annotation (Placement(transformation(extent={{124,-24},{132,-16}})));
  public
    Modelica.Blocks.Continuous.Integrator Energy_integrator[n](
      each k=1,
      each initType=Modelica.Blocks.Types.Init.InitialState,
      each y_start=0,
      each u(unit="W"),
      each y(unit="J")) "Heating energy in Joules"
      annotation (Placement(transformation(extent={{152,-68},{168,-52}})));
    Modelica.Blocks.Math.Mean Power[n](each f=1/3600)
      "Hourly averaged heating power"
      annotation (Placement(transformation(extent={{152,0},{168,16}})));
    Modelica.Blocks.Interfaces.RealOutput Elec_Heating[n]
      "Power of electric power " annotation (Placement(transformation(
          extent={{-20,20},{20,-20}},
          rotation=0,
          origin={200,-20}), iconTransformation(
          extent={{-16,-16},{16,16}},
          rotation=0,
          origin={196,-10})));
    Modelica.Blocks.Interfaces.RealOutput Energy[n] "Power of electric power "
      annotation (Placement(transformation(
          extent={{-20,20},{20,-20}},
          rotation=0,
          origin={200,-60}), iconTransformation(
          extent={{-16,-16},{16,16}},
          rotation=0,
          origin={196,-54})));
    Modelica.Blocks.Logical.Switch switch_elec[n]
      annotation (Placement(transformation(extent={{110,-48},{118,-40}})));
    Modelica.Blocks.Interfaces.RealVectorInput Texch_out[n] annotation (Placement(
          transformation(extent={{-120,18},{-80,58}}), iconTransformation(extent=
              {{-114,28},{-86,56}})));

  protected
    Modelica.Blocks.Logical.Not pump_state_test[n]
      annotation (Placement(transformation(extent={{-40,-36},{-28,-24}})));
  equation
    connect(Tinstruction_souff, pid_pump.u_s) annotation (Line(
        points={{-100,0},{-80,0},{-80,22},{-74,22}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(pid_pump.y, switch.u1) annotation (Line(
        points={{-51,22},{-50,22},{-50,10},{-46,10}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(pump_state, switch.u2) annotation (Line(
        points={{-100,-30},{-80,-30},{-80,-8},{-74,-8},{-74,2},{-46,2}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(zeros.y, switch.u3) annotation (Line(
        points={{-53.3,-6},{-46,-6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(switch.y, pump_speed_out) annotation (Line(
        points={{-23,2},{30,2},{30,40},{200,40}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Full_speed.y, pump_test.u2) annotation (Line(
        points={{0.6,-46},{6,-46},{6,-30.8},{8.8,-30.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(switch.y, Full_speed.u) annotation (Line(
        points={{-23,2},{-18,2},{-18,-46},{-13.2,-46}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(pump_test.y, need_elec.u2) annotation (Line(
        points={{22.6,-26},{28,-26},{28,-24.8},{32.8,-24.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(need_heat.y, need_elec.u1) annotation (Line(
        points={{11,44},{26,44},{26,-20},{32.8,-20}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Tinstruction_souff, need_heat.reference) annotation (Line(
        points={{-100,0},{-80,0},{-80,50},{-12,50}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(need_elec.y, timer.in_value) annotation (Line(
        points={{46.6,-20},{54,-20}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Tsouff, pid_Elec.u_m) annotation (Line(
        points={{-100,-60},{98,-60},{98,-27.2}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(Tinstruction_souff, pid_Elec.u_s) annotation (Line(
        points={{-100,0},{-80,0},{-80,60},{86,60},{86,-20},{90.8,-20}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(timer.out_value, switch_elec.u2) annotation (Line(
        points={{71.12,-20},{76,-20},{76,-44},{109.2,-44}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(zeros.y, switch_elec.u3) annotation (Line(
        points={{-53.3,-6},{-50,-6},{-50,-54},{106,-54},{106,-47.2},{109.2,
            -47.2}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(pid_Elec.y, switch_elec.u1) annotation (Line(
        points={{104.6,-20},{106,-20},{106,-40.8},{109.2,-40.8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(gaiHea.y, Power.u) annotation (Line(
        points={{132.4,-20},{140,-20},{140,8},{150.4,8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(gaiHea.y, Energy_integrator.u) annotation (Line(
        points={{132.4,-20},{140,-20},{140,-60},{150.4,-60}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(gaiHea.y, Elec_Heating) annotation (Line(
        points={{132.4,-20},{200,-20}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Energy_integrator.y, Energy) annotation (Line(
        points={{168.8,-60},{200,-60}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Texch_out, need_heat.u) annotation (Line(
        points={{-100,38},{-12,38}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Texch_out, pid_pump.u_m) annotation (Line(
        points={{-100,38},{-62,38},{-62,34}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(switch_elec.y, gaiHea.u) annotation (Line(
        points={{118.4,-44},{120,-44},{120,-20},{123.2,-20}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(pump_state, pump_state_test.u) annotation (Line(
        points={{-100,-30},{-41.2,-30}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(pump_state_test.y, pump_test.u1) annotation (Line(
        points={{-27.4,-30},{0,-30},{0,-26},{8.8,-26}},
        color={255,0,255},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
              -80},{200,60}}),   graphics), Icon(coordinateSystem(
            preserveAspectRatio=false, extent={{-100,-80},{200,60}}),   graphics={
          Polygon(
            points={{-100,60},{200,60},{200,-80},{-100,-80},{-100,60},{-100,60},{
                -100,60}},
            lineColor={255,0,255},
            smooth=Smooth.None),
          Text(
            extent={{-40,36},{148,-52}},
            lineColor={0,128,0},
            textString="Electrical heating control")}),
      Documentation(info="<html>
<p>Need blowing temperature and exhanger outlet temperature for PIDS.</p>
<p>Electrical can useblowing one but in order to optimize power of solar collector we must use the exchanger outlet temperature.</p>
</html>"));
  end Regul_Tsouff_noSmooth;

  model Heating_mod_souff

    import SI = Modelica.SIunits;
  parameter Integer n = 2
      "Number of heated rooms (used to sizing vector holding the rooms pumps states : true = ON, false = OFF)";

    Modelica.Blocks.Interfaces.RealVectorInput TafterHex[n]
      "Temperature just after the exchanger" annotation (Placement(transformation(
            extent={{-120,110},{-80,150}}), iconTransformation(extent={{-112,104},
              {-80,136}})));
    Modelica.Blocks.Interfaces.BooleanInput V3V_solar
      "Open to solar panels = true, Open to storage tank = false"
      annotation (Placement(transformation(extent={{-122,-28},{-78,16}}),
          iconTransformation(extent={{-114,-78},{-80,-44}})));
    Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
      annotation (Placement(transformation(extent={{-120,-300},{-80,-260}}),
          iconTransformation(extent={{-112,-266},{-78,-232}})));
    Modelica.Blocks.Interfaces.RealVectorInput
                                         TsolarInstruction[n] annotation (
        Placement(transformation(extent={{-118,-226},{-78,-186}}),
                                                               iconTransformation(
            extent={{-118,-226},{-86,-194}})));

    Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
      annotation (Placement(transformation(extent={{-120,-72},{-80,-32}}),
          iconTransformation(extent={{-116,-180},{-84,-148}})));
    Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
      annotation (Placement(transformation(extent={{-120,-120},{-80,-80}}),
          iconTransformation(extent={{-116,-130},{-84,-98}})));

    Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S5(start=true)
      "ON = true, OFF = false (pump S5)"
      annotation (Placement(transformation(extent={{314,-260},{354,-220}}),
          iconTransformation(extent={{314,-260},{354,-220}})));

    Modelica.Blocks.Interfaces.BooleanOutput pumpControl_Sj[n](
                                                              each start=true)
      "ON = true, OFF = false (pump Sj)"
      annotation (Placement(transformation(extent={{310,-80},{350,-40}}),
          iconTransformation(extent={{312,-92},{352,-52}})));

  public
    Utilities.Timers.Stay_on stay_on_Sj[n](each pause=60, out_value(each start=false))
      annotation (Placement(transformation(extent={{280,-68},{296,-52}})));
  protected
    Modelica.Blocks.Math.Max max_28_T7
      "Return the minimal value between T3 and T4"
      annotation (Placement(transformation(extent={{-60,-142},{-42,-124}})));
    Modelica.Blocks.Sources.RealExpression cst3(each y=40 + 273.15)
      annotation (Placement(transformation(extent={{-100,-136},{-76,-118}})));
  protected
    Modelica.Blocks.Math.Add sum_T1(each k2=1, each k1=1)
      "TsolarInstruction + 0.2"
      annotation (Placement(transformation(extent={{-20,-140},{0,-120}})));
  protected
    Modelica.Blocks.Sources.RealExpression cst4(each y=5)
      annotation (Placement(transformation(extent={{-100,-172},{-78,-156}})));
  public
    Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n]
      "Blowing temperature instruction"
                                 annotation (Placement(transformation(extent={{-118,30},
              {-78,70}}),         iconTransformation(extent={{-116,14},{-90,42}})));
    Modelica.Blocks.Logical.Or final_state[n]
      "Test if Tamb is  between lower and upper instruction"
      annotation (Placement(transformation(extent={{240,-74},{268,-46}})));
    Modelica.Blocks.Logical.Hysteresis            T5_Test(uLow=273.15 + 39, uHigh=
         273.15 + 41)
      annotation (Placement(transformation(extent={{-20,-62},{0,-42}})));
    Modelica.Blocks.MathBoolean.And T5_and_Delta_test_1(nu=2)
      "Test if Tamb is  between lower and upper instruction"
      annotation (Placement(transformation(extent={{100,-20},{120,0}})));
  protected
    Buildings.Utilities.Math.BooleanReplicator on1_replicator(nout=n)
      "Replicator for V3V and Temp test for Tinstruction on mode"
      annotation (Placement(transformation(extent={{144,-20},{164,0}})));
  public
    Modelica.Blocks.Logical.And On_IndirectSun[n]
      "Activation of pump with indirect sun"
      annotation (Placement(transformation(extent={{180,20},{204,44}})));
    Modelica.Blocks.MathBoolean.And T1_and_Delta_test_2(nu=2)
      "Test if Tamb is  between lower and upper instruction"
      annotation (Placement(transformation(extent={{100,-110},{120,-90}})));
  protected
    Buildings.Utilities.Math.BooleanReplicator on2_replicator(nout=n)
      "Replicator for V3V and Temp test for Tinstruction on mode"
      annotation (Placement(transformation(extent={{144,-110},{164,-90}})));
  public
    Modelica.Blocks.Logical.Greater Test_T1
      "Test if Tamb is lower than instruction"
      annotation (Placement(transformation(extent={{30,-110},{50,-90}})));
    Modelica.Blocks.Logical.Not V3V_inverse
      annotation (Placement(transformation(extent={{28,-16},{50,6}})));
    Modelica.Blocks.Logical.And On_DirectSun[n]
      "Activation of pump with direct sun"
      annotation (Placement(transformation(extent={{180,-140},{206,-114}})));
    Utilities.Timers.Stay_on stay_on_S5(each pause=60, out_value(start=false))
      annotation (Placement(transformation(extent={{284,-248},{300,-232}})));
  protected
    Utilities.Logical.True_seeker true_seeker(n=n)
      annotation (Placement(transformation(extent={{228,-256},{270,-224}})));
  public
    Modelica.Blocks.Logical.OnOffController need_heat1[n](each bandwidth=1)
      "True if Tsouff below Tinstruction_souff"
      annotation (Placement(transformation(extent={{42,80},{62,100}})));
    Modelica.Blocks.Logical.OnOffController need_heat2[n](each bandwidth=1)
      "True if Tsouff below Tinstruction_souff"
      annotation (Placement(transformation(extent={{30,-206},{50,-186}})));
  equation

    connect(stay_on_Sj.out_value, pumpControl_Sj) annotation (Line(
        points={{297.6,-60},{330,-60}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T7, max_28_T7.u2) annotation (Line(
        points={{-100,-280},{-66,-280},{-66,-138.4},{-61.8,-138.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(cst3.y, max_28_T7.u1) annotation (Line(
        points={{-74.8,-127},{-74,-127},{-74,-127.6},{-61.8,-127.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(max_28_T7.y, sum_T1.u1) annotation (Line(
        points={{-41.1,-133},{-28,-133},{-28,-124},{-22,-124}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(cst4.y, sum_T1.u2) annotation (Line(
        points={{-76.9,-164},{-30,-164},{-30,-136},{-22,-136}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(final_state.y, stay_on_Sj.in_value) annotation (Line(
        points={{269.4,-60},{280,-60}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T5, T5_Test.u) annotation (Line(
        points={{-100,-52},{-22,-52}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T5_and_Delta_test_1.y, on1_replicator.u) annotation (Line(
        points={{121.5,-10},{142,-10}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(on1_replicator.y, On_IndirectSun.u2) annotation (Line(
        points={{165,-10},{172,-10},{172,22.4},{177.6,22.4}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(On_IndirectSun.y, final_state.u1) annotation (Line(
        points={{205.2,32},{220,32},{220,-60},{237.2,-60}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T1_and_Delta_test_2.y, on2_replicator.u) annotation (Line(
        points={{121.5,-100},{142,-100}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T1, Test_T1.u1) annotation (Line(
        points={{-100,-100},{28,-100}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(sum_T1.y, Test_T1.u2) annotation (Line(
        points={{1,-130},{8,-130},{8,-108},{28,-108}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(V3V_solar, V3V_inverse.u) annotation (Line(
        points={{-100,-6},{10,-6},{10,-6},{10,-5},{18,-5},{25.8,-5}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(V3V_solar, T1_and_Delta_test_2.u[1]) annotation (Line(
        points={{-100,-6},{10,-6},{10,-80},{60,-80},{60,-96.5},{100,-96.5}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Test_T1.y, T1_and_Delta_test_2.u[2]) annotation (Line(
        points={{51,-100},{76,-100},{76,-103.5},{100,-103.5}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(V3V_inverse.y, T5_and_Delta_test_1.u[1]) annotation (Line(
        points={{51.1,-5},{78,-5},{78,-6.5},{100,-6.5}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T5_Test.y, T5_and_Delta_test_1.u[2]) annotation (Line(
        points={{1,-52},{60,-52},{60,-13.5},{100,-13.5}},
        color={255,0,255},
        smooth=Smooth.None));

    connect(on2_replicator.y, On_DirectSun.u1) annotation (Line(
        points={{165,-100},{170,-100},{170,-127},{177.4,-127}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(On_DirectSun.y, final_state.u2) annotation (Line(
        points={{207.3,-127},{220,-127},{220,-71.2},{237.2,-71.2}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(On_DirectSun.y, true_seeker.u) annotation (Line(
        points={{207.3,-127},{220,-127},{220,-240},{228,-240}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(true_seeker.y, stay_on_S5.in_value) annotation (Line(
        points={{270,-240},{284,-240}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(stay_on_S5.out_value, pumpControl_S5) annotation (Line(
        points={{301.6,-240},{334,-240}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Tinstruction, need_heat1.reference) annotation (Line(
        points={{-98,50},{-72,50},{-72,96},{40,96}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(TafterHex, need_heat1.u) annotation (Line(
        points={{-100,130},{20,130},{20,84},{40,84}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(need_heat1.y, On_IndirectSun.u1) annotation (Line(
        points={{63,90},{120,90},{120,32},{177.6,32}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(need_heat2.y, On_DirectSun.u2) annotation (Line(
        points={{51,-196},{114,-196},{114,-137.4},{177.4,-137.4}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(TsolarInstruction, need_heat2.reference) annotation (Line(
        points={{-98,-206},{-36,-206},{-36,-190},{28,-190}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(TafterHex, need_heat2.u) annotation (Line(
        points={{-100,130},{20,130},{20,-202},{28,-202}},
        color={0,0,127},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-100,-300},{320,140}},
            preserveAspectRatio=false),  graphics), Icon(coordinateSystem(extent={{-100,
              -300},{320,140}}, preserveAspectRatio=false),  graphics={
          Text(
            extent={{-58,24},{284,-236}},
            lineColor={0,128,0},
            fillColor={0,0,0},
            fillPattern=FillPattern.Solid,
            textString="Nb Room
%n"),     Polygon(
            points={{-100,140},{320,140},{320,-300},{-100,-300},{-100,140},{-100,140},
                {-100,140}},
            lineColor={255,0,255},
            smooth=Smooth.None)}));
  end Heating_mod_souff;

  model Algo_one_elec_souff
    "All INPUT temperature must be in Kelvin(K) not �C or �F, otherwise might lead to strange behaviour. Use when only one room"
    import SolarSystem;

    parameter Integer n=2
      "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

    // Weather file info
    parameter String WeatherFile="C:/Users/bois/Documents/Dymola/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos"
      "Name of weather data file" annotation (Dialog(tab="Weather"));
    parameter Modelica.SIunits.ThermodynamicTemperature Tmoy_first=279.94
      "Average temperature from first day" annotation (Dialog(tab="Weather"));
    parameter Modelica.SIunits.ThermodynamicTemperature Tmin_first=275.15
      "Minimal temperature from first day" annotation (Dialog(tab="Weather"));
    parameter Real offset=3600*24 "Temperature offset (time of an output step)"
      annotation (Dialog(tab="Weather"));
    parameter Modelica.SIunits.ThermodynamicTemperature pad=276.15
      "Used to compute TsolarInstruction";

    SolisArt_mod.solarTank_mod solarTank_mod1(pumpControl_S5)
      annotation (Placement(transformation(extent={{120,104},{156,140}})));
    SolisArt_mod.storageTank_mod storageTank_mod1
      annotation (Placement(transformation(extent={{120,56},{166,92}})));
    SolisArt_mod.SolarValve_mod solarValve_mod
      annotation (Placement(transformation(extent={{-58,-58},{-26,-34}})));
    Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
      annotation (Placement(transformation(extent={{-134,118},{-106,146}}),
          iconTransformation(extent={{-130,116},{-108,138}})));
    Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
      annotation (Placement(transformation(extent={{-134,94},{-106,122}}),
          iconTransformation(extent={{-130,86},{-108,108}})));
    Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
      annotation (Placement(transformation(extent={{-134,70},{-106,98}}),
          iconTransformation(extent={{-130,56},{-108,78}})));
    Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
      annotation (Placement(transformation(extent={{-134,6},{-106,34}}),
          iconTransformation(extent={{-130,-2},{-108,20}})));
    Modelica.Blocks.Interfaces.RealInput T8
      "T. at heating start point (just before extra exchanger input)"
      annotation (Placement(transformation(extent={{-134,-28},{-106,0}}),
          iconTransformation(extent={{-130,-32},{-108,-10}})));
    Modelica.Blocks.Interfaces.RealVectorInput T_afterHex[n]
      "Temperature after the exchanger" annotation (Placement(transformation(
            extent={{-134,-110},{-106,-82}}), iconTransformation(extent={{-128,
              -104},{-106,-82}})));
    Modelica.Blocks.Interfaces.RealInput Text "T. from outdoor"
      annotation (Placement(transformation(extent={{-134,-144},{-106,-116}}),
          iconTransformation(extent={{-128,-146},{-104,-122}})));
    Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
      annotation (Placement(transformation(extent={{-134,44},{-106,72}}),
          iconTransformation(extent={{-130,28},{-108,50}})));

    Modelica.Blocks.Interfaces.BooleanOutput
                                          S5_outter(start=false)
      "ON = 1, OFF = 0 (pump S5)"
      annotation (Placement(transformation(extent={{372,124},{404,156}}),
          iconTransformation(extent={{370,36},{400,66}})));
    Modelica.Blocks.Interfaces.BooleanOutput
                                          Sj_outter[n](each start=true)
      "ON = 1, OFF = 0 (pump Sj)"
      annotation (Placement(transformation(extent={{372,4},{404,36}}),
          iconTransformation(extent={{370,-70},{400,-40}})));

    Modelica.Blocks.Interfaces.RealOutput    V3V_solar_outter(start=1)
      "Open to solar panels = 1, Open to storage tank = 0"
      annotation (Placement(transformation(extent={{372,-64},{404,-32}}),
          iconTransformation(extent={{370,86},{400,116}})));

    SolarSystem.Control.IGC_control.Air_vector_electric.old.Heating_mod_souff heating_mod(n=n)
      annotation (Placement(transformation(extent={{138,-40},{188,6}})));
    Modelica.Blocks.Logical.Or or_S5
      annotation (Placement(transformation(extent={{338,130},{358,150}})));

    Modelica.Blocks.Math.BooleanToReal real_solar
                                             annotation (Placement(transformation(
          extent={{-8,8},{8,-8}},
          rotation=0,
          origin={348,-48})));

    Instruction_variable.Variables_state
                                 variables_state(pad=
          pad, n=n)
               annotation (Placement(transformation(extent={{44,-16},{86,6}})));
    Modelica.Blocks.Interfaces.BooleanOutput S6_outter(start=false)
      "ON = 1, OFF = 0 (pump S6)"
      annotation (Placement(transformation(extent={{370,84},{402,116}}),
          iconTransformation(extent={{370,-16},{400,14}})));
    Utilities.Other.MiniAverage extract_TdryBulb(
      filNam=WeatherFile,
      Tmoy_first=Tmoy_first,
      Tmin_first=Tmin_first,
      offset=offset) annotation (Placement(transformation(extent={{-24,-90},{18,
              -62}})));
    Utilities.Timers.Stay_on stay_on(out_value(start=false), pause=60)
      annotation (Placement(transformation(extent={{300,112},{316,128}})));
    Modelica.Blocks.Interfaces.RealVectorInput T_souff_instruction[n]
      "Blowing temperature instruction" annotation (Placement(transformation(
            extent={{-134,-82},{-106,-54}}), iconTransformation(extent={{-132,-72},
              {-110,-50}})));
    Modelica.Blocks.Interfaces.RealOutput T_souff_instruction_out[n]
      "Blowing temperature instruction" annotation (Placement(transformation(
          extent={{-14,-14},{14,14}},
          rotation=90,
          origin={218,166}), iconTransformation(
          extent={{-11,-11},{11,11}},
          rotation=90,
          origin={271,157})));
    Modelica.Blocks.Interfaces.RealOutput T_afterhex_out[n]
      "T. after the exchanger" annotation (Placement(transformation(
          extent={{-14,-14},{14,14}},
          rotation=90,
          origin={260,166}), iconTransformation(
          extent={{-11,-11},{11,11}},
          rotation=90,
          origin={325,157})));
  equation
    connect(T1, solarTank_mod1.T1) annotation (Line(
        points={{-120,132},{-72,132},{-72,132.5},{120,132.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T1, storageTank_mod1.T1) annotation (Line(
        points={{-120,132},{-72,132},{-72,82},{120,82}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T1, solarValve_mod.T1) annotation (Line(
        points={{-120,132},{-72,132},{-72,-37},{-58,-37}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T3, solarTank_mod1.T3) annotation (Line(
        points={{-120,108},{-76,108},{-76,126.5},{120,126.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T3, storageTank_mod1.T3) annotation (Line(
        points={{-120,108},{-76,108},{-76,72},{120,72}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T3, solarValve_mod.T3) annotation (Line(
        points={{-120,108},{-76,108},{-76,-43},{-58,-43}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T4, solarValve_mod.T4) annotation (Line(
        points={{-120,84},{-80,84},{-80,-49},{-58,-49}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T5, storageTank_mod1.T5) annotation (Line(
        points={{-120,58},{-86,58},{-86,62},{120,62}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T5, solarValve_mod.T5) annotation (Line(
        points={{-120,58},{-86,58},{-86,-55},{-58,-55}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(solarValve_mod.V3V_solar, solarTank_mod1.V3V_solar)
      annotation (Line(
        points={{-26,-46},{24,-46},{24,111.5},{120,111.5}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(solarValve_mod.V3V_solar, heating_mod.V3V_solar) annotation (
        Line(
        points={{-26,-46},{106,-46},{106,-15.0136},{138.357,-15.0136}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T5, heating_mod.T5) annotation (Line(
        points={{-120,58},{-86,58},{-86,-25.7818},{138,-25.7818}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(solarTank_mod1.pumpControl_S5, or_S5.u1) annotation (Line(
        points={{156,119},{180,119},{180,80},{280,80},{280,140},{336,140}},
        color={255,0,255},
        smooth=Smooth.None));

    connect(real_solar.y, V3V_solar_outter) annotation (Line(
        points={{356.8,-48},{388,-48}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(or_S5.y, S5_outter) annotation (Line(
        points={{359,140},{388,140}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(heating_mod.pumpControl_Sj, Sj_outter) annotation (Line(
        points={{189.429,-16.1636},{200,-16.1636},{200,10},{320,10},{320,20},{
            388,20}},
        color={255,0,255},
        smooth=Smooth.None));

    connect(solarValve_mod.V3V_solar, real_solar.u) annotation (Line(
        points={{-26,-46},{156,-46},{156,-48},{338.4,-48}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(variables_state.TsolarInstruction, heating_mod.TsolarInstruction)
      annotation (Line(
        points={{86.3,-9.71429},{99.15,-9.71429},{99.15,-30.5909},{137.762,
            -30.5909}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(variables_state.T1_out, heating_mod.T1) annotation (Line(
        points={{62,-16.6286},{62,-20.5545},{138,-20.5545}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(variables_state.T7_out, heating_mod.T7) annotation (Line(
        points={{71,-16.6286},{71,-34.6682},{138.595,-34.6682}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T1, variables_state.T1) annotation (Line(
        points={{-120,132},{-72,132},{-72,4.11429},{44,4.11429}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T7, variables_state.T7) annotation (Line(
        points={{-120,20},{-90,20},{-90,-2.17143},{44,-2.17143}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Text, variables_state.Text) annotation (Line(
        points={{-120,-130},{34,-130},{34,-8.14286},{44,-8.14286}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(storageTank_mod1.pumpControl_S6, S6_outter) annotation (Line(
        points={{166,64},{320,64},{320,100},{386,100}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(extract_TdryBulb.TminOfDay, variables_state.Text_mini) annotation (
        Line(
        points={{19.26,-70.4},{36,-70.4},{36,-14.1143},{44,-14.1143}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(heating_mod.pumpControl_S5, stay_on.in_value) annotation (
        Line(
        points={{189.667,-33.7273},{192,-33.7273},{192,60},{290,60},{290,120},{
            300,120}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(stay_on.out_value, or_S5.u2) annotation (Line(
        points={{317.6,120},{320,120},{320,132},{336,132}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T_souff_instruction, variables_state.Tinstruction) annotation (Line(
        points={{-120,-68},{-102,-68},{-102,14},{52.1,14},{52.1,6.62857}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T_souff_instruction, heating_mod.Tinstruction) annotation (Line(
        points={{-120,-68},{-102,-68},{-102,14},{116,14},{116,-5.70909},{
            137.643,-5.70909}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T_souff_instruction, T_souff_instruction_out) annotation (Line(
        points={{-120,-68},{-102,-68},{-102,34},{218,34},{218,166}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T_afterHex, heating_mod.TafterHex) annotation (Line(
        points={{-120,-96},{126,-96},{126,3.90909},{138.476,3.90909}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T_afterHex, T_afterhex_out) annotation (Line(
        points={{-120,-96},{260,-96},{260,166}},
        color={0,0,127},
        smooth=Smooth.None));
                                                                                                        annotation(Dialog(tab="Execution cycle"),
                Diagram(coordinateSystem(extent={{-120,-180},{380,160}},
            preserveAspectRatio=false),   graphics, defaultComponentName = "algo_states"), Icon(coordinateSystem(
            extent={{-120,-180},{380,160}}, preserveAspectRatio=false),
          graphics={Rectangle(
            extent={{-120,160},{382,-180}},
            lineColor={90,150,90},
            fillPattern=FillPattern.CrossDiag,
            fillColor={0,127,127}),
          Text(
            extent={{-124,108},{380,28}},
            lineColor={0,0,0},
            fontName="Consolas",
            textStyle={TextStyle.Bold},
            textString="Algo"),
          Text(
            extent={{-120,-24},{388,-102}},
            lineColor={0,0,0},
            fontName="Consolas",
            textStyle={TextStyle.Bold},
            textString="States")}),
      Documentation(info="<html>
<p>Groups all algorithms into one block.</p>
<p>This modul must be used with pump_algo modul to have real pump state inputs.</p>
</html>"));
  end Algo_one_elec_souff;

  model pump_pids_noSmooth "flow control for each pump"
    import SolarSystem;
    // How many heated rooms
    parameter Integer n=2
      "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

    // Flow Control
    parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm right_pumps=1
      "Prescribed rotational speed for pumps of right extra valve side (unit=1/min)";
    parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm left_pumps=1
      "Prescribed rotational speed for pumps of left extra valve side";
    parameter Modelica.SIunits.Time temporizations[n - 1]={300}
      "How much time we have to wait before the outter value can be true (n-1 values)";
    parameter Modelica.SIunits.Time modulations[2]={180,180}
      "How much time we have to wait before the outter value can be true (2 values)";

    // S6
    parameter Modelica.Blocks.Types.SimpleController S6_controller=Modelica.Blocks.Types.SimpleController.PID
      "Type of controller" annotation (Dialog(tab="S6"));
    parameter Real S6_schedule[:,:]=[0, 10]
      "Scheduled consigne setup (repeat every days)"
      annotation (Dialog(tab="S6"));
    parameter Real S6_k=0.1 "Gain of controller" annotation (Dialog(tab="S6"));
    parameter Modelica.SIunits.Time S6_Ti=60
      "Time constant of Integrator block"
      annotation (Dialog(tab="S6"));
    parameter Modelica.SIunits.Time S6_Td=60
      "Time constant of Derivative block"
      annotation (Dialog(tab="S6"));
    parameter Real S6_yMax=1 "Upper limit of output" annotation (Dialog(tab="S6"));
    parameter Real S6_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S6"));
    parameter Real S6_wp=1 "Set-point weight for Proportional block (0..1)"
      annotation (Dialog(tab="S6"));
    parameter Real S6_wd=0 "Set-point weight for Derivative block (0..1)"
      annotation (Dialog(tab="S6"));

    // S5
    parameter Modelica.Blocks.Types.SimpleController S5_controller=Modelica.Blocks.Types.SimpleController.PID
      "Type of controller" annotation (Dialog(tab="S5"));
    parameter Real S5_schedule[:,:]=[0, 10]
      "Scheduled consigne setup (repeat every days)"
      annotation (Dialog(tab="S5"));
    parameter Real S5_k=0.1 "Gain of controller" annotation (Dialog(tab="S5"));
    parameter Modelica.SIunits.Time S5_Ti=60
      "Time constant of Integrator block"
      annotation (Dialog(tab="S5"));
    parameter Modelica.SIunits.Time S5_Td=60
      "Time constant of Derivative block"
      annotation (Dialog(tab="S5"));
    parameter Real S5_yMax=1 "Upper limit of output" annotation (Dialog(tab="S5"));
    parameter Real S5_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S5"));
    parameter Real S5_wp=1 "Set-point weight for Proportional block (0..1)"
      annotation (Dialog(tab="S5"));
    parameter Real S5_wd=0 "Set-point weight for Derivative block (0..1)"
      annotation (Dialog(tab="S5"));

    // S4
    parameter Modelica.Blocks.Types.SimpleController S4_controller=Modelica.Blocks.Types.SimpleController.PID
      "Type of controller" annotation (Dialog(tab="S4"));
    parameter Real S4_schedule[:,:]=[0, 10]
      "Scheduled consigne setup (repeat every days)"
      annotation (Dialog(tab="S4"));
    parameter Real S4_k=0.1 "Gain of controller" annotation (Dialog(tab="S4"));
    parameter Modelica.SIunits.Time S4_Ti=60
      "Time constant of Integrator block"
      annotation (Dialog(tab="S4"));
    parameter Modelica.SIunits.Time S4_Td=60
      "Time constant of Derivative block"
      annotation (Dialog(tab="S4"));
    parameter Real S4_yMax=1 "Upper limit of output" annotation (Dialog(tab="S4"));
    parameter Real S4_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S4"));
    parameter Real S4_wp=1 "Set-point weight for Proportional block (0..1)"
      annotation (Dialog(tab="S4"));
    parameter Real S4_wd=0 "Set-point weight for Derivative block (0..1)"
      annotation (Dialog(tab="S4"));

    // Sj
    parameter Modelica.Blocks.Types.SimpleController Sj_controllerType=.Modelica.Blocks.Types.SimpleController.PID
      "Type of controller"
     annotation (Dialog(tab="Sj", group="PID"));
    parameter Real Sj_k=1 "Gain of controller"
     annotation (Dialog(tab="Sj", group="PID"));
    parameter Modelica.SIunits.Time Sj_Ti=0.5
      "Time constant of Integrator block"
     annotation (Dialog(tab="Sj", group="PID"));
    parameter Modelica.SIunits.Time Sj_Td=0.1
      "Time constant of Derivative block"
     annotation (Dialog(tab="Sj", group="PID"));
    parameter Real Sj_yMax=1 "Upper limit of speed"
     annotation (Dialog(tab="Sj", group="PID"));
    parameter Real Sj_yMin=0.1 "Lower limit of speed"
     annotation (Dialog(tab="Sj", group="PID"));
    parameter Real Sj_wp=1 "Set-point weight for Proportional block (0..1)"
     annotation (Dialog(tab="Sj", group="PID"));
    parameter Real Sj_wd=0 "Set-point weight for Derivative block (0..1)"
     annotation (Dialog(tab="Sj", group="PID"));
    parameter Real Sj_Ni=0.9
      "Ni*Ti is time constant of anti-windup compensation"
     annotation (Dialog(tab="Sj", group="PID"));
    parameter Boolean Sj_reverseAction=false
      "Set to true for throttling the water flow rate through a cooling coil controller"
       annotation (Dialog(tab="Sj", group="PID"));
    parameter Real Sj_Nd= 10
      "The higher Nd, the more ideal the derivative block"
      annotation (Dialog(tab="Sj", group="PID"));
    parameter Modelica.SIunits.Time wait_for=60
      "How much time we have to wait before the outter value can be true"
      annotation (Dialog(tab="Sj", group="Speed"));
    parameter Real is_on_max=0.98
      "Value where the pump speed considered at max speed (hight limit)" annotation (Dialog(tab="Sj", group="Speed"));
    parameter Real is_on_min=0.94
      "Value where the pump speed considered at max speed (low limit)"
      annotation (Dialog(tab="Sj", group="Speed"));

   // Electrical PID
    parameter Modelica.Blocks.Types.SimpleController Elec_controllerType=.Modelica.Blocks.Types.SimpleController.PID
      "Type of controller" annotation (Dialog(tab="Elec"));
    parameter Real Elec_k=1 "Gain of controller" annotation (Dialog(tab="Elec"));
    parameter Modelica.SIunits.Time Elec_Ti=0.5
      "Time constant of Integrator block" annotation (Dialog(tab="Elec"));
    parameter Modelica.SIunits.Time Elec_Td=0.1
      "Time constant of Derivative block" annotation (Dialog(tab="Elec"));
    parameter Real Elec_yMax=1 "Upper limit of output"
      annotation (Dialog(tab="Elec"));
    parameter Real Elec_yMin=0 "Lower limit of output"
      annotation (Dialog(tab="Elec"));
    parameter Real Elec_wp=1 "Set-point weight for Proportional block (0..1)"
      annotation (Dialog(tab="Elec"));
    parameter Real Elec_wd=0 "Set-point weight for Derivative block (0..1)"
      annotation (Dialog(tab="Elec"));
    parameter Real Elec_Ni=0.9
      "Ni*Ti is time constant of anti-windup compensation"
      annotation (Dialog(tab="Elec"));
    parameter Real Elec_Nd=10
      "The higher Nd, the more ideal the derivative block"
      annotation (Dialog(tab="Elec"));
    parameter Boolean Elec_reverseAction=false
      "Set to true for throttling the water flow rate through a cooling coil controller"
      annotation (Dialog(tab="Elec"));
    parameter Modelica.Blocks.Types.SimpleController fan_controllerType=.Modelica.Blocks.Types.SimpleController.PID
      "Type of controller" annotation (Dialog(tab="fan"));
    parameter Real fan_k=1 "Gain of controller" annotation (Dialog(tab="fan"));
    parameter Modelica.SIunits.Time fan_Ti=0.5
      "Time constant of Integrator block"
      annotation (Dialog(tab="fan"));
    parameter Modelica.SIunits.Time fan_Td=0.1
      "Time constant of Derivative block"
      annotation (Dialog(tab="fan"));
    parameter Real fan_yMax=0.9 "Upper limit of output" annotation (Dialog(tab="fan"));
    parameter Real fan_yMin=0.1 "Lower limit of output" annotation (Dialog(tab="fan"));
    parameter Real fan_wp=1 "Set-point weight for Proportional block (0..1)"
      annotation (Dialog(tab="fan"));
    parameter Real fan_wd=0 "Set-point weight for Derivative block (0..1)"
      annotation (Dialog(tab="fan"));
    parameter Boolean fan_reverseAction=false
      "Set to true for throttling the water flow rate through a cooling coil controller"
      annotation (Dialog(tab="fan"));
    parameter Real electrical_power[n]={5000, 5000}
      "Maximum value of eletrical power"
      annotation (Dialog(tab="Elec", group="Power"));

    Modelica.Blocks.Interfaces.BooleanInput  pumpControl_Sj[
                                                           n]
      "ON = true, OFF = false (pump Sj)"
      annotation (Placement(transformation(extent={{-120,-56},{-80,-16}}),
          iconTransformation(extent={{-108,-60},{-74,-26}})));
    Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S5
      "ON = true, OFF = false (pump S5)"
      annotation (Placement(transformation(extent={{-120,-22},{-80,18}}),
          iconTransformation(extent={{-114,-16},{-80,18}})));
    Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S6
      "ON = true, OFF = false (pump S6)"
      annotation (Placement(transformation(extent={{-120,20},{-80,60}}),
          iconTransformation(extent={{-114,26},{-80,60}})));
    Modelica.Blocks.Interfaces.RealOutput S6_outter(start=0)
      "ON = 1, OFF = 0 (pump S6)"
      annotation (Placement(transformation(extent={{222,-16},{254,16}}),
          iconTransformation(extent={{228,-20},{252,4}})));
    Modelica.Blocks.Interfaces.RealOutput S5_outter(start=0)
      "ON = 1, OFF = 0 (pump S5)"
      annotation (Placement(transformation(extent={{222,-56},{254,-24}}),
          iconTransformation(extent={{228,-52},{252,-28}})));

    Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S6(table=
          S6_schedule) "Scheduled consigne setup (repeat every days)"
      annotation (Placement(transformation(extent={{80,2},{100,22}})));
    Buildings.Controls.Continuous.LimPID pid_S6(
      controllerType=S6_controller,
      k=S6_k,
      Ti=S6_Ti,
      Td=S6_Td,
      wp=S6_wp,
      wd=S6_wd,
      yMax=S6_yMax,
      yMin=S6_yMin,
      reverseAction=true)
      annotation (Placement(transformation(extent={{142,2},{162,22}})));
    Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S5(table=
          S5_schedule) "Scheduled consigne setup (repeat every days)"
      annotation (Placement(transformation(extent={{80,-44},{100,-24}})));
    Buildings.Controls.Continuous.LimPID pid_S5(
      controllerType=S5_controller,
      k=S5_k,
      Ti=S5_Ti,
      Td=S5_Td,
      wp=S5_wp,
      wd=S5_wd,
      yMax=S5_yMax,
      yMin=S5_yMin,
      reverseAction=true)
      annotation (Placement(transformation(extent={{140,-44},{160,-24}})));
    Modelica.Blocks.Math.MultiSum delta_storage(k={1,-1}, nu=2) "T1 - T5"
      annotation (Placement(transformation(extent={{126,-6},{134,2}})));
    Modelica.Blocks.Math.MultiSum delta_solar(k={1,-1}, nu=2) "T1 - T3"
      annotation (Placement(transformation(extent={{126,-52},{134,-44}})));
    Modelica.Blocks.Interfaces.RealInput T1 "T. after collector"
      annotation (Placement(transformation(extent={{-16,16},{16,-16}},
          rotation=-90,
          origin={-92,100}),
          iconTransformation(extent={{-16,-16},{16,16}},
          rotation=-90,
          origin={-90,96})));
    Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
      annotation (Placement(transformation(extent={{-16,16},{16,-16}},
          rotation=-90,
          origin={-60,100}),
          iconTransformation(extent={{-16,-16},{16,16}},
          rotation=-90,
          origin={-54,96})));
    Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
      annotation (Placement(transformation(extent={{-16,16},{16,-16}},
          rotation=-90,
          origin={-12,100}),
          iconTransformation(extent={{-16,-16},{16,16}},
          rotation=-90,
          origin={18,96})));

    Modelica.Blocks.Logical.Switch S6_if_max
      annotation (Placement(transformation(extent={{180,-4},{188,4}})));
    Modelica.Blocks.Logical.Switch S5_if_max
      annotation (Placement(transformation(extent={{180,-48},{188,-40}})));
    Modelica.Blocks.Sources.RealExpression S6_static(each y=0.5)
      annotation (Placement(transformation(extent={{156,-16},{170,-2}})));
    Modelica.Blocks.Sources.RealExpression S5_static(each y=0.5)
      annotation (Placement(transformation(extent={{158,-56},{172,-44}})));
    Modelica.Blocks.Interfaces.RealOutput    pumps_state[n + 2]
      "[S6, S5, S4, Sj*n] states" annotation (Placement(transformation(
          extent={{-14,-14},{14,14}},
          rotation=-90,
          origin={20,-106}),  iconTransformation(
          extent={{-16,-16},{16,16}},
          rotation=-90,
          origin={0,-104})));

    SolarSystem.Control.IGC_control.Air_vector_electric.old.Pump_control_deprecated
      pumps_state_algo(
      n=n,
      temporizations=temporizations,
      modulations=modulations)
      annotation (Placement(transformation(extent={{-44,-46},{6,-10}})));
    Modelica.Blocks.Math.Gain S6_speed(k=left_pumps)
      annotation (Placement(transformation(extent={{194,-12},{202,-4}})));
    Modelica.Blocks.Math.Gain S5_speed(k=left_pumps)
      annotation (Placement(transformation(extent={{194,-54},{202,-46}})));
    Modelica.Blocks.Math.Product S6_speed_state
      "Another multiplicator to check pump state"
      annotation (Placement(transformation(extent={{210,-4},{218,4}})));
    Modelica.Blocks.Math.BooleanToReal to_real[n + 2] annotation (Placement(
          transformation(
          extent={{-4,-4},{4,4}},
          rotation=0,
          origin={176,42})));
    Modelica.Blocks.Math.Product S5_speed_state
      "Another multiplicator to check pump state"
      annotation (Placement(transformation(extent={{210,-44},{218,-36}})));
    Modelica.Blocks.Math.BooleanToReal Pumps_state_ToReal[n + 2] annotation (
        Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=-90,
          origin={20,-68})));
    SolarSystem.Control.IGC_control.Air_vector_electric.old.Regul_Tamb regul_Tamb(
      n=n,
      each controllerType=fan_controllerType,
      each k=fan_k,
      each Ti=fan_Ti,
      each Td=fan_Td,
      each yMax=fan_yMax,
      each yMin=fan_yMin,
      each wp=fan_wp,
      each wd=fan_wd,
      each reverseAction=fan_reverseAction,
      Ni=fan_Ni)
      annotation (Placement(transformation(extent={{122,52},{158,70}})));
    Modelica.Blocks.Interfaces.RealVectorInput
                                         Tambiant[n] "T. in the room"
      annotation (Placement(transformation(extent={{18,80},{44,106}}),
          iconTransformation(extent={{52,88},{74,110}})));
    Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n] "T. in the room"
      annotation (Placement(transformation(extent={{-6,80},{20,106}}),
          iconTransformation(extent={{88,88},{110,110}})));
    Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n] annotation (Placement(
          transformation(extent={{58,80},{84,106}}),   iconTransformation(extent={{156,88},
              {178,110}})));
    Modelica.Blocks.Interfaces.RealVectorInput Tinstruction_souff[n] annotation (
        Placement(transformation(extent={{36,80},{62,106}}),
          iconTransformation(extent={{124,88},{146,110}})));
    Modelica.Blocks.Interfaces.RealOutput Sj_outter[n]
      "ON = 1, OFF = 0 (pump Sj)" annotation (Placement(transformation(extent={{226,
              80},{258,112}}), iconTransformation(extent={{228,68},{252,92}})));
    Modelica.Blocks.Interfaces.RealOutput fan_outter[n]
      "ON = 1, OFF = 0 (pump Sj)" annotation (Placement(transformation(extent={{226,
              58},{258,90}}), iconTransformation(extent={{228,38},{252,62}})));
    Modelica.Blocks.Interfaces.RealInput T4 "T. inside DHW tank"
      annotation (Placement(transformation(extent={{-16,16},{16,-16}},
          rotation=-90,
          origin={-36,100}),
          iconTransformation(extent={{-16,-16},{16,16}},
          rotation=-90,
          origin={-18,96})));
    Electric_DHW electric_DHW
      annotation (Placement(transformation(extent={{90,-92},{166,-76}})));
    Modelica.Blocks.Interfaces.RealOutput Elec_DHW "ON = 1, OFF = 0 (pump S5)"
                                  annotation (Placement(transformation(extent={{224,
              -96},{258,-62}}), iconTransformation(extent={{228,-88},{254,-62}})));
    Modelica.Blocks.Interfaces.RealOutput Elec_heating[n]
      "ON = 1, OFF = 0 (pump S5)" annotation (Placement(transformation(extent={{224,
              24},{258,58}}), iconTransformation(extent={{228,8},{252,32}})));
    Modelica.Blocks.Math.Gain Sj_speed[n](each k=right_pumps)
      annotation (Placement(transformation(extent={{202,92},{210,100}})));
    Modelica.Blocks.Interfaces.RealVectorInput Texch_out[n] annotation (Placement(
          transformation(extent={{76,80},{102,106}}), iconTransformation(extent={
              {188,88},{210,110}})));
    SolarSystem.Control.IGC_control.Air_vector_electric.old.Regul_Tsouff_noSmooth
      regul_Tsouff(
      electrical_power=electrical_power,
      n=n,
      wait_for=wait_for,
      is_on_max=is_on_max,
      is_on_min=is_on_min,
      Sj_controllerType=Sj_controllerType,
      Sj_k=Sj_k,
      Sj_Ti=Sj_Ti,
      Sj_Td=Sj_Td,
      Sj_yMax=Sj_yMax,
      Sj_yMin=Sj_yMin,
      Sj_wp=Sj_wp,
      Sj_wd=Sj_wd,
      Sj_Ni=Sj_Ni,
      Sj_reverseAction=Sj_reverseAction,
      Sj_Nd=Sj_Nd,
      Elec_controllerType=Elec_controllerType,
      Elec_k=Elec_k,
      Elec_Ti=Elec_Ti,
      Elec_Td=Elec_Td,
      Elec_yMax=Elec_yMax,
      Elec_yMin=Elec_yMin,
      Elec_wp=Elec_wp,
      Elec_wd=Elec_wd,
      Elec_Ni=Elec_Ni,
      Elec_Nd=Elec_Nd,
      Elec_reverseAction=Elec_reverseAction)
      annotation (Placement(transformation(extent={{118,78},{180,92}})));

    parameter Real fan_Ni=0.9
      "Ni*Ti is time constant of anti-windup compensation"
      annotation (Dialog(tab="fan"));
  equation
    connect(control_S6.y[1], pid_S6.u_s) annotation (Line(
        points={{101,12},{140,12}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(control_S5.y[1], pid_S5.u_s) annotation (Line(
        points={{101,-34},{138,-34}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(delta_solar.y, pid_S5.u_m) annotation (Line(
        points={{134.68,-48},{140,-48},{140,-52},{150,-52},{150,-46}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(delta_storage.y, pid_S6.u_m) annotation (Line(
        points={{134.68,-2},{140,-2},{140,-6},{152,-6},{152,0}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T1, delta_storage.u[1]) annotation (Line(
        points={{-92,100},{-92,66},{-38,66},{-38,20},{60,20},{60,-0.6},{126,-0.6}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T1, delta_solar.u[1]) annotation (Line(
        points={{-92,100},{-92,66},{-38,66},{-38,20},{60,20},{60,-46.6},{126,
            -46.6}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T5, delta_storage.u[2]) annotation (Line(
        points={{-12,100},{-12,72},{20,72},{20,52},{72,52},{72,-3.4},{126,-3.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T3, delta_solar.u[2]) annotation (Line(
        points={{-60,100},{-60,68},{-20,68},{-20,32},{64,32},{64,-49.4},{126,
            -49.4}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(S6_static.y, S6_if_max.u3) annotation (Line(
        points={{170.7,-9},{176.2,-9},{176.2,-3.2},{179.2,-3.2}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S5_static.y, S5_if_max.u3) annotation (Line(
        points={{172.7,-50},{175.15,-50},{175.15,-47.2},{179.2,-47.2}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(pid_S5.y, S5_if_max.u1) annotation (Line(
        points={{161,-34},{172,-34},{172,-40.8},{179.2,-40.8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(pid_S6.y, S6_if_max.u1) annotation (Line(
        points={{163,12},{172,12},{172,3.2},{179.2,3.2}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(pumpControl_S6, pumps_state_algo.pumps_system[1]) annotation (Line(
        points={{-100,40},{-74,40},{-74,-22.15},{-44.4167,-22.15}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(pumpControl_S5, pumps_state_algo.pumps_system[2]) annotation (Line(
        points={{-100,-2},{-74,-2},{-74,-15.85},{-44.4167,-15.85}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(pumps_state_algo.pumps_modulation[1], S6_if_max.u2) annotation (Line(
        points={{4.33333,-39.7},{4,-39.7},{4,-36},{50,-36},{50,-12},{174,-12},{
            174,0},{179.2,0}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(pumps_state_algo.pumps_modulation[2], S5_if_max.u2) annotation (Line(
        points={{4.33333,-34.3},{2,-34.3},{2,-36},{50,-36},{50,-12},{174,-12},{
            174,-44},{179.2,-44}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(S6_if_max.y, S6_speed.u) annotation (Line(
        points={{188.4,0},{190,0},{190,-8},{193.2,-8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S5_if_max.y, S5_speed.u) annotation (Line(
        points={{188.4,-44},{190,-44},{190,-50},{193.2,-50}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(pumpControl_Sj, pumps_state_algo.pumps_heat) annotation (Line(
        points={{-100,-36},{-56,-36},{-56,-37},{-44.4167,-37}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(to_real[1].y, S6_speed_state.u1) annotation (Line(
        points={{180.4,42},{204,42},{204,2.4},{209.2,2.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S6_speed.y, S6_speed_state.u2) annotation (Line(
        points={{202.4,-8},{206,-8},{206,-2.4},{209.2,-2.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S5_speed.y, S5_speed_state.u2) annotation (Line(
        points={{202.4,-50},{206,-50},{206,-42.4},{209.2,-42.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(to_real[2].y, S5_speed_state.u1) annotation (Line(
        points={{180.4,42},{204,42},{204,-37.6},{209.2,-37.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S6_speed_state.y, S6_outter) annotation (Line(
        points={{218.4,0},{238,0}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S5_speed_state.y, S5_outter) annotation (Line(
        points={{218.4,-40},{238,-40}},
        color={0,0,127},
        smooth=Smooth.None));

        // Connect each heating pumps (first three pump are system wide pumps)

    connect(pumps_state_algo.pumps_state, Pumps_state_ToReal.u) annotation (Line(
        points={{4.33333,-19},{20,-19},{20,-58.4}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Pumps_state_ToReal.y, pumps_state) annotation (Line(
        points={{20,-76.8},{20,-106}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(pumps_state_algo.pumps_state, to_real.u) annotation (Line(
        points={{4.33333,-19},{20,-19},{20,42},{171.2,42}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Tambiant, regul_Tamb.Tambiant) annotation (Line(
        points={{31,93},{31,74},{108,74},{108,65.275},{121.64,65.275}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tinstruction, regul_Tamb.Tinstruction) annotation (Line(
        points={{7,93},{7,70},{100,70},{100,56.275},{121.64,56.275}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(regul_Tamb.fan_speed_out, fan_outter) annotation (Line(
        points={{159.08,60.775},{212,60.775},{212,74},{242,74}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T4, electric_DHW.T4) annotation (Line(
        points={{-36,100},{-36,72},{-12,72},{-12,36},{54,36},{54,-84},{90,-84}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(electric_DHW.Elec_DHW, Elec_DHW) annotation (Line(
        points={{165.367,-79.8},{226,-79.8},{226,-80},{234,-80},{234,-79},{241,
            -79}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(Sj_speed.y, Sj_outter) annotation (Line(
        points={{210.4,96},{242,96}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(regul_Tsouff.pump_speed_out, Sj_speed.u) annotation (Line(
        points={{178.967,88.9},{189.483,88.9},{189.483,96},{201.2,96}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(regul_Tsouff.Elec_Heating, Elec_heating) annotation (Line(
        points={{179.173,85},{179.173,86},{190,86},{190,80},{220,80},{220,42},{
            226,42},{226,41},{241,41}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(pumps_state_algo.pumps_state[3:2+n], regul_Tsouff.pump_state) annotation (
        Line(
        points={{4.33333,-19},{28,-19},{28,80},{108,80},{108,83.2},{118,83.2}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Tsouff, regul_Tsouff.Tsouff) annotation (Line(
        points={{71,93},{71,79.6},{118,79.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Texch_out, regul_Tsouff.Texch_out) annotation (Line(
        points={{89,93},{104.5,93},{104.5,90.2},{118,90.2}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tinstruction_souff, regul_Tsouff.Tinstruction_souff) annotation (Line(
        points={{49,93},{49,88},{50,88},{50,82},{102,82},{102,86.6},{118,86.6}},
        color={0,0,127},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-100,-100},{240,100}},
            preserveAspectRatio=false), graphics, defaultComponentName = "pump_control"), Icon(coordinateSystem(extent={{-100,
              -100},{240,100}}, preserveAspectRatio=false), graphics={
                    Rectangle(
            extent={{-100,100},{240,-100}},
            lineColor={90,150,90},
            fillPattern=FillPattern.CrossDiag,
            fillColor={0,127,127}),
          Text(
            extent={{-100,62},{238,16}},
            lineColor={0,0,0},
            fontName="Consolas",
            textStyle={TextStyle.Bold},
            textString="PID"),
          Text(
            extent={{-96,-20},{240,-52}},
            lineColor={0,0,0},
            textString="Flow switch",
            fontName="Consolas",
            textStyle={TextStyle.Bold})}),
      Documentation(info="<html>
<p>Control all pumps according to pump state from algo.</p>
<p><br><br>Be careful only work with two heating pump fo now.</p>
</html>"),
      experiment(
        StopTime=2e+006,
        Interval=240,
        __Dymola_Algorithm="esdirk23a"),
      __Dymola_experimentSetupOutput(doublePrecision=true));
  end pump_pids_noSmooth;

  model Pump_control_deprecated
    "Used to know pump state (tempo, modulation, ...) but need at least n == 2 (check last version do not use this one)"
    parameter Integer n=1 "Number of heating pump (Sj)";
    parameter Modelica.SIunits.Time temporizations[n-1]=fill(300, n-1)
      "How much time we have to wait before the outter value can be true (n-1 values)";

    parameter Modelica.SIunits.Time modulations[2]=fill(180, 2)
      "How much time we have to wait before the outter value can be true (2 values)";

    Utilities.Logical.BoolSwitch flow_limit[2] annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=0,
          origin={70,0})));
    Modelica.Blocks.Sources.BooleanExpression normal[2](each y=true)
      annotation (Placement(transformation(extent={{16,2},{36,22}})));
    Modelica.Blocks.Sources.BooleanExpression module[2](each y=false)
      annotation (Placement(transformation(extent={{16,-22},{36,-2}})));
    Utilities.Timers.Wait_for modulation_algo[2](wait_for=modulations)
      annotation (Placement(transformation(extent={{-12,-10},{8,10}})));
    Modelica.Blocks.Interfaces.BooleanInput pumps_system[2] "[S6, S5]"
      annotation (Placement(transformation(extent={{-120,20},{-80,60}}),
          iconTransformation(extent={{-116,26},{-88,54}})));
    Modelica.Blocks.Interfaces.BooleanInput pumps_heat[n] "[Sj]*n" annotation (
        Placement(transformation(extent={{-120,-20},{-80,20}}),
          iconTransformation(extent={{-116,-14},{-88,14}})));

    Utilities.Timers.Wait_for_tempo temporization_algo(n=n, wait_for=temporizations)
      annotation (Placement(transformation(extent={{-60,22},{-40,38}})));

    Modelica.Blocks.Interfaces.BooleanOutput pumps_state[2 + n]
      "[S6, S5, Sj*n] states"
      annotation (Placement(transformation(extent={{120,20},{160,60}}),
          iconTransformation(extent={{-12,-12},{12,12}},
          rotation=0,
          origin={132,40})));
    Modelica.Blocks.Interfaces.BooleanOutput pumps_modulation[2]
      "[S6, S5] multiplicator" annotation (Placement(transformation(extent={{120,-20},
              {160,20}}), iconTransformation(
          extent={{-12,-12},{12,12}},
          rotation=0,
          origin={132,0})));
    Utilities.Logical.Multiplex2_boolean multiplex_boolean(      n2=n, n1=2)
      annotation (Placement(transformation(extent={{-4,26},{16,46}})));
    Modelica.Blocks.Logical.Pre Cut_SolarLoop[2](each pre_u_start=false)
      annotation (Placement(transformation(extent={{92,-10},{112,10}})));
    Modelica.Blocks.Logical.Pre Cut_loop1[2 + n](each pre_u_start=false)
      annotation (Placement(transformation(extent={{90,30},{110,50}})));
  equation
    connect(normal.y, flow_limit.u1) annotation (Line(
        points={{37,12},{40,12},{40,8},{58,8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(module.y, flow_limit.u3) annotation (Line(
        points={{37,-12},{40,-12},{40,-8},{58,-8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(pumps_heat, temporization_algo.in_value) annotation (Line(
        points={{-100,0},{-70,0},{-70,30},{-60,30}},
        color={255,0,255},
        smooth=Smooth.None));

    connect(modulation_algo.out_value, flow_limit.u2) annotation (Line(
        points={{9.4,0},{58,0}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(temporization_algo.out_value, multiplex_boolean.u2) annotation (Line(
        points={{-38.6,30},{-6,30}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(pumps_system, multiplex_boolean.u1) annotation (Line(
        points={{-100,40},{-20,40},{-20,42},{-6,42}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(pumps_system[1], modulation_algo[1].in_value) annotation (Line(
        points={{-100,30},{-100,16},{-40,16},{-40,0},{-12,0}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(pumps_system[2], modulation_algo[2].in_value) annotation (Line(
        points={{-100,50},{-20,50},{-20,0},{-12,0}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(flow_limit.y, Cut_SolarLoop.u) annotation (Line(
        points={{81,0},{90,0}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Cut_SolarLoop.y, pumps_modulation) annotation (Line(
        points={{113,0},{140,0}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Cut_loop1.y, pumps_state) annotation (Line(
        points={{111,40},{140,40}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(multiplex_boolean.y, Cut_loop1.u) annotation (Line(
        points={{17,36},{40,36},{40,40},{88,40}},
        color={255,0,255},
        smooth=Smooth.None));
                                                                                        annotation (Dialog(group="Temporization"), Icon(
          coordinateSystem(extent={{-100,-20},{140,60}},   preserveAspectRatio=false),
          graphics),
      Documentation(info="<html>
<p>Inputs and outputs order :</p>
<p>S6, S5, S4, Sj (heating pumps)</p>
<p>Heating pump order is important. Used to temporizations</p>
<p>n = heating pumps number</p>
<p>Each pumps start at 50&percnt; except if V3V extra open to backup (always a modulation on S5 and S6 yet)</p>
<p>Must have two heating pump at least due to temporization algorithm.</p>
</html>"),      Diagram(coordinateSystem(extent={{-100,-20},{140,60}},
            preserveAspectRatio=false),
                        graphics),defaultComponentName = "flow_out");
  end Pump_control_deprecated;
end old;
