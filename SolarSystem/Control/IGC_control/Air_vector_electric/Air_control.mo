within SolarSystem.Control.IGC_control.Air_vector_electric;
model Air_control
  "Group all models to compute fan speed, electrical heat, pump speed, and blowing temperature instruction"
  extends
    SolarSystem.Control.IGC_control.Air_vector_electric.Algorithm_characteristics;
  Modelica.Blocks.Interfaces.RealVectorInput Texch_outlet[n]
    "Temperature just after the exchanger outlet (air side)" annotation (
      Placement(transformation(extent={{-110,-40},{-94,-24}}),
                                                             iconTransformation(
          extent={{-106,-36},{-94,-24}})));
  Modelica.Blocks.Interfaces.BooleanVectorInput Pump_state[n]
    "Heating pumps states" annotation (Placement(transformation(extent={{-106,52},
            {-90,68}}),  iconTransformation(extent={{-106,42},{-94,54}})));
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_heat[n] "True = need"
    annotation (Placement(transformation(extent={{-106,34},{-90,50}}),
        iconTransformation(extent={{-106,24},{-94,36}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_heating_Power[n]
    "Electrical power to keep setpoint temperature inside house" annotation (
      Placement(transformation(extent={{96,78},{116,98}}), iconTransformation(
          extent={{96,84},{108,96}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_heating_Energy[n]
    "Electrical energy used to keep setpoint temperature inside house"
    annotation (Placement(transformation(extent={{96,58},{116,78}}),
        iconTransformation(extent={{96,54},{108,66}})));
  Modelica.Blocks.Interfaces.RealOutput Fan_VolFlow[n]
    "Fan mass flow rate output" annotation (Placement(transformation(extent={{96,12},
            {118,34}}),        iconTransformation(extent={{96,12},{118,34}})));
  Modelica.Blocks.Interfaces.RealOutput Sj_normSpeed_out[n]
    "Sj normalized speed rate output" annotation (Placement(transformation(
          extent={{96,-30},{116,-10}}), iconTransformation(extent={{96,-26},{
            108,-14}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n] annotation (Placement(
        transformation(extent={{-106,86},{-90,102}}),iconTransformation(extent={{-106,84},
            {-94,96}})));
  Modelica.Blocks.Interfaces.RealVectorInput
                                       Tinstruction[n] annotation (Placement(
        transformation(extent={{-106,4},{-90,20}}),   iconTransformation(extent={{-106,6},
            {-94,18}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tambiant[n] annotation (Placement(
        transformation(extent={{-110,-20},{-94,-4}}),iconTransformation(extent={{-106,
            -16},{-94,-4}})));
  Modelica.Blocks.Interfaces.RealInput Text "Exterior temperature" annotation (
      Placement(transformation(extent={{-112,68},{-92,90}}), iconTransformation(
          extent={{-104,64},{-94,74}})));

  Sj_control sj_control(
    n=n,
    tempo_Sj=tempo_Sj,
    Sj_controllerType=Sj_controllerType,
    Sj_k=Sj_k,
    Sj_Ti=Sj_Ti,
    Sj_Td=Sj_Td,
    Sj_yMax=Sj_yMax,
    Sj_yMin=Sj_yMin,
    Sj_wp=Sj_wp,
    Sj_wd=Sj_wd,
    Sj_Ni=Sj_Ni,
    Sj_Nd=Sj_Nd,
    Sj_reverseAction=Sj_reverseAction)
    annotation (Placement(transformation(extent={{-14,-30},{14,-16}})));
  Electrical_heat electrical_heat(
    n=n,
    electrical_power=electrical_power,
    Elec_controllerType=Elec_controllerType,
    Elec_k=Elec_k,
    Elec_Ti=Elec_Ti,
    Elec_Td=Elec_Td,
    Elec_yMax=Elec_yMax,
    Elec_yMin=Elec_yMin,
    Elec_wp=Elec_wp,
    Elec_wd=Elec_wd,
    Elec_Ni=Elec_Ni,
    Elec_Nd=Elec_Nd,
    Elec_reverseAction=Elec_reverseAction)
    annotation (Placement(transformation(extent={{40,70},{74,88}})));
  Tsouff_activation tsouff_activation(
    n=n,
    tempo_souff=tempo_souff,
    Tsouff_max=Tsouff_max,
    minimal_speed=minimal_speed,
    fan_bandwidth=fan_bandwidth)
    annotation (Placement(transformation(extent={{-70,28},{-42,44}})));
  Tsouff_control tsouff_control(
    n=n,
    Tsouff_max=Tsouff_max,
    Tinstruction_controllerType=Tinstruction_controllerType,
    Tinstruction_k=Tinstruction_k,
    Tinstruction_Ti=Tinstruction_Ti,
    Tinstruction_Td=Tinstruction_Td,
    Tinstruction_yMax=Tinstruction_yMax,
    Tinstruction_yMin=Tinstruction_yMin,
    Tinstruction_wp=Tinstruction_wp,
    Tinstruction_wd=Tinstruction_wd,
    Tinstruction_Ni=Tinstruction_Ni,
    Tinstruction_Nd=Tinstruction_Nd,
    Tinstruction_reverseAction=Tinstruction_reverseAction)
    annotation (Placement(transformation(extent={{-18,42},{12,60}})));
  Fan_control fan_control(
    n=n,
    fan_controllerType=fan_controllerType,
    fan_k=fan_k,
    fan_Ti=fan_Ti,
    fan_Td=fan_Td,
    fan_yMax=fan_yMax,
    fan_yMin=fan_yMin,
    fan_wp=fan_wp,
    fan_wd=fan_wd,
    fan_Ni=fan_Ni,
    fan_Nd=fan_Nd,
    fan_reverseAction=fan_reverseAction)
    annotation (Placement(transformation(extent={{-18,14},{16,34}})));
  Modelica.Blocks.Logical.Pre cut_loop[n](each pre_u_start=false)
    "Used  to cut code for algebric loop"
    annotation (Placement(transformation(extent={{-38,32},{-30,40}})));
equation
  connect(sj_control.Need_electricalPower, electrical_heat.Need_electricalPower)
    annotation (Line(
      points={{15.1667,-25.66},{34,-25.66},{34,79.225},{40.2429,79.225}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(electrical_heat.Elec_Heating, Elec_heating_Power) annotation (Line(
      points={{74.4857,83.5},{87.2429,83.5},{87.2429,88},{106,88}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(electrical_heat.Energy, Elec_heating_Energy) annotation (Line(
      points={{74.4857,74.5},{86.2429,74.5},{86.2429,68},{106,68}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsouff, electrical_heat.Tsouff) annotation (Line(
      points={{-98,94},{0,94},{0,72.475},{40.2429,72.475}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(tsouff_control.Tsouff_instruction, tsouff_activation.Tsouff_instruction)
    annotation (Line(
      points={{13.2,51.6},{20,51.6},{20,64},{-80,64},{-80,40.2},{-69.8,40.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(tsouff_control.Tsouff_instruction, electrical_heat.Tsouff_instruction)
    annotation (Line(
      points={{13.2,51.6},{16,51.6},{16,52},{20,52},{20,85.525},{40.2429,85.525}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(Tinstruction, fan_control.Tinstruction) annotation (Line(
      points={{-98,12},{-28,12},{-28,30.2},{-18.34,30.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant, fan_control.Tambiant) annotation (Line(
      points={{-102,-12},{-24,-12},{-24,17.8},{-17.66,17.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texch_outlet, sj_control.Texch_outlet) annotation (Line(
      points={{-102,-32},{-40,-32},{-40,-17.68},{-14,-17.68}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Pump_state, sj_control.Pump_state) annotation (Line(
      points={{-98,60},{-88,60},{-88,-21.32},{-14,-21.32}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(tsouff_control.Tsouff_instruction, sj_control.Tsouff_instruction)
    annotation (Line(
      points={{13.2,51.6},{20,51.6},{20,52},{20,52},{20,64},{-80,64},{-80,
          -24.96},{-14,-24.96}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant, tsouff_control.Tambiant) annotation (Line(
      points={{-102,-12},{-24,-12},{-24,53.1},{-18.3,53.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction, tsouff_control.Tinstruction) annotation (Line(
      points={{-98,12},{-28,12},{-28,58},{-20,58},{-20,57.9},{-18.3,57.9}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text, tsouff_control.Text) annotation (Line(
      points={{-102,79},{-40,79},{-40,44.1},{-17.7,44.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sj_control.Pump_normSpeed_out, Sj_normSpeed_out) annotation (Line(
      points={{14.9333,-20.2},{56.4667,-20.2},{56.4667,-20},{106,-20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(tsouff_activation.Need_limitSpeed, cut_loop.u) annotation (Line(
      points={{-42.2,36.2},{-41.1,36.2},{-41.1,36},{-38.8,36}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(cut_loop.y, fan_control.Need_limitSpeed) annotation (Line(
      points={{-29.6,36},{-26,36},{-26,23.8},{-18.34,23.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(cut_loop.y, tsouff_control.Need_limitSpeed) annotation (Line(
      points={{-29.6,36},{-26,36},{-26,48.3},{-18.3,48.3}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_heat, sj_control.Need_heat) annotation (Line(
      points={{-98,42},{-90,42},{-90,-28.6},{-14,-28.6}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_heat, tsouff_activation.Need_heat) annotation (Line(
      points={{-98,42},{-90,42},{-90,36.2},{-69.8,36.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(fan_control.Fan_VolFlow_out, Fan_VolFlow) annotation (Line(
      points={{18.04,23.6},{86,23.6},{86,23},{107,23}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(fan_control.Fan_VolFlow_out, tsouff_activation.Fan_speed) annotation (
     Line(
      points={{18.04,23.6},{26,23.6},{26,0},{-74,0},{-74,31.4},{-69.8,31.4}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-40},{100,100}},
          preserveAspectRatio=false), graphics),                        Icon(
        coordinateSystem(extent={{-100,-40},{100,100}}, preserveAspectRatio=false),
                    graphics));
end Air_control;
