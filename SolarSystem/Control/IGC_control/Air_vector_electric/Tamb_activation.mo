within SolarSystem.Control.IGC_control.Air_vector_electric;
model Tamb_activation "Control inside temperature according instruction one"
parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
protected
  Modelica.Blocks.Logical.OnOffController need_heat[n](each bandwidth=1)
    annotation (Placement(transformation(extent={{-40,40},{-20,60}})));
public
  Modelica.Blocks.Interfaces.RealVectorInput Tamb[n]
    "Temperature inside the room" annotation (Placement(transformation(extent={{-114,16},
            {-86,44}}),          iconTransformation(extent={{-108,24},{-94,38}})));
public
  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n]
    "Inside temperature instruction"
                               annotation (Placement(transformation(extent={{-114,56},
            {-86,84}}),         iconTransformation(extent={{-108,62},{-94,76}})));
  Modelica.Blocks.Interfaces.BooleanOutput Need_heat[n](each start=true)
    "ON = true, OFF = false" annotation (Placement(transformation(extent={{4,30},
            {44,70}}), iconTransformation(extent={{12,42},{30,60}})));
equation
  connect(need_heat.y, Need_heat) annotation (Line(
      points={{-19,50},{24,50}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tinstruction, need_heat.reference) annotation (Line(
      points={{-100,70},{-80,70},{-80,56},{-42,56}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb, need_heat.u) annotation (Line(
      points={{-100,30},{-80,30},{-80,44},{-42,44}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,20},
            {20,80}}),    graphics), Icon(coordinateSystem(extent={{-100,20},{20,
            80}}, preserveAspectRatio=false), graphics={
        Polygon(
          points={{-100,80},{20,80},{20,20},{-100,20},{-100,80},{-100,80},{-100,
              80}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-84,84},{0,20}},
          lineColor={0,128,0},
          textString="Need Heat ?")}));
end Tamb_activation;
