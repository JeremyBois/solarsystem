within SolarSystem.Control.IGC_control.Air_vector_electric;
model Electric_DHW "Control of Electric power pull inside the tank"
  parameter Modelica.SIunits.Temperature hotWater_setPoint=40 + 273.15
    "Hot water setpoint for DHW tank";
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside DHW tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=0,
        origin={-100,20}),
        iconTransformation(extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-100,40})));
protected
  Modelica.Blocks.Sources.RealExpression Setpoint(each y=hotWater_setPoint)
    "algo need at least 2 heating pumps"
    annotation (Placement(transformation(extent={{-100,52},{-80,68}})));
protected
  Buildings.Controls.Continuous.LimPID pip_elec(
    Td=60,
    initType=Modelica.Blocks.Types.InitPID.InitialState,
    Ti=300,
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=0.1) "Controller for heating"
    annotation (Placement(transformation(extent={{-60,36},{-52,44}})));
protected
  Modelica.Blocks.Math.Gain gaiHea(k=1E6) "Gain for heating"
    annotation (Placement(transformation(extent={{-40,36},{-32,44}})));
public
  Modelica.Blocks.Interfaces.RealOutput Elec_DHW "Power of electric power "
    annotation (Placement(transformation(
        extent={{-16,16},{16,-16}},
        rotation=0,
        origin={20,40}), iconTransformation(
        extent={{-9,-9},{9,9}},
        rotation=0,
        origin={19,61})));
protected
  Modelica.Blocks.Continuous.Integrator Energy_integrator(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{-20,4},{-4,20}})));
public
  Modelica.Blocks.Math.Mean Power(f=1/3600) "Hourly averaged heating power"
    annotation (Placement(transformation(extent={{-20,60},{-4,76}})));
  Modelica.Blocks.Interfaces.RealOutput Energy annotation (Placement(
        transformation(
        extent={{-16,16},{16,-16}},
        rotation=0,
        origin={20,12}), iconTransformation(
        extent={{-9,-9},{9,9}},
        rotation=0,
        origin={19,19})));

equation
  connect(Setpoint.y, pip_elec.u_s) annotation (Line(
      points={{-79,60},{-70,60},{-70,40},{-60.8,40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, pip_elec.u_m) annotation (Line(
      points={{-100,20},{-56,20},{-56,35.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pip_elec.y, gaiHea.u) annotation (Line(
      points={{-51.6,40},{-40.8,40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiHea.y, Elec_DHW) annotation (Line(
      points={{-31.6,40},{20,40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiHea.y, Power.u) annotation (Line(
      points={{-31.6,40},{-28,40},{-28,68},{-21.6,68}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiHea.y, Energy_integrator.u) annotation (Line(
      points={{-31.6,40},{-28,40},{-28,12},{-21.6,12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Energy_integrator.y, Energy) annotation (Line(
      points={{-3.2,12},{20,12}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,0},
            {20,80}}),    graphics), Icon(coordinateSystem(extent={{-100,0},{20,
            80}}, preserveAspectRatio=false), graphics={
        Polygon(
          points={{-100,80},{20,80},{20,0},{-100,0},{-100,80},{-100,80},{-100,
              80}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-84,70},{0,6}},
          lineColor={0,128,0},
          textString="Electrical DHW control")}));
end Electric_DHW;
