within SolarSystem.Control.IGC_control.Air_vector_electric;
model pump_pids "flow control for each pump"

    extends
    SolarSystem.Control.IGC_control.Air_vector_electric.Algorithm_characteristics;

  // Flow Control
  parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm right_pumps=1
    "Prescribed rotational speed for pumps of right extra valve side (unit=1/min)";
  parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm left_pumps=1
    "Prescribed rotational speed for pumps of left extra valve side";
  parameter Modelica.SIunits.Time temporizations[n]={300, 300}
    "How much time we have to wait before the outter value can be true (n-1 values)";
  parameter Modelica.SIunits.Time modulations[2]={180,180}
    "How much time we have to wait before the outter value can be true (2 values)";

  // S6
  parameter Modelica.Blocks.Types.SimpleController S6_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S6"));
  parameter Real S6_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S6"));
  parameter Real S6_k=0.1 "Gain of controller" annotation (Dialog(tab="S6"));
  parameter Modelica.SIunits.Time S6_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S6"));
  parameter Modelica.SIunits.Time S6_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S6"));
  parameter Real S6_yMax=1 "Upper limit of output" annotation (Dialog(tab="S6"));
  parameter Real S6_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S6"));
  parameter Real S6_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S6"));
  parameter Real S6_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S6"));

  // S5
  parameter Modelica.Blocks.Types.SimpleController S5_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S5"));
  parameter Real S5_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S5"));
  parameter Real S5_k=0.1 "Gain of controller" annotation (Dialog(tab="S5"));
  parameter Modelica.SIunits.Time S5_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S5"));
  parameter Modelica.SIunits.Time S5_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S5"));
  parameter Real S5_yMax=1 "Upper limit of output" annotation (Dialog(tab="S5"));
  parameter Real S5_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S5"));
  parameter Real S5_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S5"));
  parameter Real S5_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S5"));

  Modelica.Blocks.Interfaces.BooleanVectorInput
                                           pumpControl_Sj[n]
    "ON = true, OFF = false (pump Sj)"
    annotation (Placement(transformation(extent={{-112,-56},{-72,-16}}),
        iconTransformation(extent={{-116,-76},{-90,-50}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S5
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{-120,-22},{-80,18}}),
        iconTransformation(extent={{-114,-16},{-88,10}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S6
    "ON = true, OFF = false (pump S6)"
    annotation (Placement(transformation(extent={{-120,20},{-80,60}}),
        iconTransformation(extent={{-114,26},{-88,52}})));
  Modelica.Blocks.Interfaces.RealOutput S6_outter(start=0)
    "ON = 1, OFF = 0 (pump S6)"
    annotation (Placement(transformation(extent={{236,-20},{262,6}}),
        iconTransformation(extent={{234,-56},{258,-32}})));
  Modelica.Blocks.Interfaces.RealOutput S5_outter(start=0)
    "ON = 1, OFF = 0 (pump S5)"
    annotation (Placement(transformation(extent={{234,-60},{260,-34}}),
        iconTransformation(extent={{234,-92},{258,-68}})));

  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S6(table=
        S6_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,-6},{100,14}})));
  Buildings.Controls.Continuous.LimPID pid_S6(
    controllerType=S6_controller,
    k=S6_k,
    Ti=S6_Ti,
    Td=S6_Td,
    wp=S6_wp,
    wd=S6_wd,
    yMax=S6_yMax,
    yMin=S6_yMin,
    reverseAction=true)
    annotation (Placement(transformation(extent={{142,-6},{162,14}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S5(table=
        S5_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,-52},{100,-32}})));
  Buildings.Controls.Continuous.LimPID pid_S5(
    controllerType=S5_controller,
    k=S5_k,
    Ti=S5_Ti,
    Td=S5_Td,
    wp=S5_wp,
    wd=S5_wd,
    yMax=S5_yMax,
    yMin=S5_yMin,
    reverseAction=true)
    annotation (Placement(transformation(extent={{140,-52},{160,-32}})));
  Modelica.Blocks.Math.MultiSum delta_storage(k={1,-1}, nu=2) "T1 - T5"
    annotation (Placement(transformation(extent={{116,-18},{124,-10}})));
  Modelica.Blocks.Math.MultiSum delta_solar(k={1,-1}, nu=2) "T1 - T3"
    annotation (Placement(transformation(extent={{116,-64},{124,-56}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after collector"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-92,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={-90,96})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-60,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={-54,96})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-34,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={-20,96})));

  Modelica.Blocks.Logical.Switch S6_if_max
    annotation (Placement(transformation(extent={{180,-12},{188,-4}})));
  Modelica.Blocks.Logical.Switch S5_if_max
    annotation (Placement(transformation(extent={{180,-56},{188,-48}})));
  Modelica.Blocks.Sources.RealExpression S6_static(each y=0.5)
    annotation (Placement(transformation(extent={{154,-26},{168,-12}})));
  Modelica.Blocks.Sources.RealExpression S5_static(each y=0.5)
    annotation (Placement(transformation(extent={{158,-68},{172,-56}})));
  Modelica.Blocks.Interfaces.RealOutput    pumps_state[2 + n]
    "[S6, S5, Sj*n] states"     annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=-90,
        origin={40,-108}),  iconTransformation(
        extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={0,-104})));

  Pump_control pumps_state_algo(
    n=n,
    temporizations=temporizations,
    modulations=modulations)
    annotation (Placement(transformation(extent={{-44,-54},{6,-18}})));
  Modelica.Blocks.Math.Gain S6_speed(k=left_pumps)
    annotation (Placement(transformation(extent={{194,-12},{202,-4}})));
  Modelica.Blocks.Math.Gain S5_speed(k=left_pumps)
    annotation (Placement(transformation(extent={{194,-56},{202,-48}})));
  Modelica.Blocks.Math.Product S6_speed_state
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{216,-12},{224,-4}})));
  Modelica.Blocks.Math.BooleanToReal to_real[n + 2] annotation (Placement(
        transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={120,26})));
  Modelica.Blocks.Math.Product S5_speed_state
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{214,-52},{222,-44}})));
  Modelica.Blocks.Math.BooleanToReal Pumps_state_ToReal[2 + n] annotation (
      Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=-90,
        origin={40,-76})));
  Modelica.Blocks.Interfaces.RealVectorInput
                                       Tambiant[n] "T. in the room"
    annotation (Placement(transformation(extent={{-8,82},{18,108}}),
        iconTransformation(extent={{38,88},{60,110}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n] "T. in the room"
    annotation (Placement(transformation(extent={{12,82},{38,108}}),
        iconTransformation(extent={{68,88},{90,110}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n] annotation (Placement(
        transformation(extent={{76,82},{102,108}}),  iconTransformation(extent={{188,88},
            {210,110}})));
  Modelica.Blocks.Interfaces.RealInput Text annotation (Placement(
        transformation(
        extent={{-13,-13},{13,13}},
        rotation=-90,
        origin={69,101}), iconTransformation(extent={{-11,-11},{11,11}},
        rotation=-90,
        origin={167,99})));
  Modelica.Blocks.Interfaces.RealOutput Sj_outter[n] "Sj pumps speed"
                                annotation (Placement(transformation(extent={{234,14},
            {260,40}}),      iconTransformation(extent={{234,-22},{258,2}})));
  Modelica.Blocks.Interfaces.RealOutput Fan_VolFlow[n]
    "ON = 1, OFF = 0 (pump Sj)" annotation (Placement(transformation(extent={{
            234,38},{260,64}}), iconTransformation(extent={{234,14},{258,38}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_heating_Power[n]
    "Electrical power to keep setpoint temperature inside house" annotation (
      Placement(transformation(extent={{234,80},{258,104}}), iconTransformation(
          extent={{234,80},{258,104}})));
  Modelica.Blocks.Math.Gain Sj_toSpeed[n](each k=right_pumps)
    annotation (Placement(transformation(extent={{210,36},{218,44}})));
  Modelica.Blocks.Interfaces.RealVectorInput Texch_out[n] annotation (Placement(
        transformation(extent={{-26,82},{0,108}}),  iconTransformation(extent={{10,88},
            {32,110}})));
  Air_control air_control(
    n=n,
    Tsouff_max=Tsouff_max,
    tempo_Sj=tempo_Sj,
    Sj_controllerType=Sj_controllerType,
    Sj_k=Sj_k,
    Sj_Ti=Sj_Ti,
    Sj_Td=Sj_Td,
    Sj_yMax=Sj_yMax,
    Sj_yMin=Sj_yMin,
    Sj_wp=Sj_wp,
    Sj_wd=Sj_wd,
    Sj_Ni=Sj_Ni,
    Sj_Nd=Sj_Nd,
    Sj_reverseAction=Sj_reverseAction,
    electrical_power=electrical_power,
    Elec_controllerType=Elec_controllerType,
    Elec_k=Elec_k,
    Elec_Ti=Elec_Ti,
    Elec_Td=Elec_Td,
    Elec_yMax=Elec_yMax,
    Elec_yMin=Elec_yMin,
    Elec_wp=Elec_wp,
    Elec_wd=Elec_wd,
    Elec_Ni=Elec_Ni,
    Elec_Nd=Elec_Nd,
    Elec_reverseAction=Elec_reverseAction,
    tempo_souff=tempo_souff,
    minimal_speed=minimal_speed,
    fan_bandwidth=fan_bandwidth,
    Tinstruction_controllerType=Tinstruction_controllerType,
    Tinstruction_k=Tinstruction_k,
    Tinstruction_Ti=Tinstruction_Ti,
    Tinstruction_Td=Tinstruction_Td,
    Tinstruction_yMax=Tinstruction_yMax,
    Tinstruction_yMin=Tinstruction_yMin,
    Tinstruction_wp=Tinstruction_wp,
    Tinstruction_wd=Tinstruction_wd,
    Tinstruction_Ni=Tinstruction_Ni,
    Tinstruction_Nd=Tinstruction_Nd,
    Tinstruction_reverseAction=Tinstruction_reverseAction,
    fan_controllerType=fan_controllerType,
    fan_k=fan_k,
    fan_Ti=fan_Ti,
    fan_Td=fan_Td,
    fan_yMax=fan_yMax,
    fan_yMin=fan_yMin,
    fan_wp=fan_wp,
    fan_wd=fan_wd,
    fan_Ni=fan_Ni,
    fan_Nd=fan_Nd,
    fan_reverseAction=fan_reverseAction)
    annotation (Placement(transformation(extent={{118,48},{188,96}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_heating_Energy[n]
    "Electrical energy used to keep setpoint temperature inside house"
    annotation (Placement(transformation(extent={{234,58},{258,82}}),
        iconTransformation(extent={{234,50},{258,74}})));
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_heat[n] annotation (
      Placement(transformation(extent={{36,82},{62,108}}), iconTransformation(
          extent={{112,88},{136,112}})));
equation
  connect(control_S6.y[1], pid_S6.u_s) annotation (Line(
      points={{101,4},{140,4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(control_S5.y[1], pid_S5.u_s) annotation (Line(
      points={{101,-42},{138,-42}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_solar.y, pid_S5.u_m) annotation (Line(
      points={{124.68,-60},{150,-60},{150,-54}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_storage.y, pid_S6.u_m) annotation (Line(
      points={{124.68,-14},{152,-14},{152,-8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, delta_storage.u[1]) annotation (Line(
      points={{-92,100},{-92,58},{-38,58},{-38,12},{60,12},{60,-12.6},{116,-12.6}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T1, delta_solar.u[1]) annotation (Line(
      points={{-92,100},{-92,58},{-38,58},{-38,12},{60,12},{60,-58.6},{116,-58.6}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T5, delta_storage.u[2]) annotation (Line(
      points={{-34,100},{-34,64},{20,64},{20,44},{72,44},{72,-15.4},{116,-15.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, delta_solar.u[2]) annotation (Line(
      points={{-60,100},{-60,60},{-20,60},{-20,24},{64,24},{64,-61.4},{116,-61.4}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(S6_static.y, S6_if_max.u3) annotation (Line(
      points={{168.7,-19},{176.2,-19},{176.2,-11.2},{179.2,-11.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_static.y, S5_if_max.u3) annotation (Line(
      points={{172.7,-62},{175.15,-62},{175.15,-55.2},{179.2,-55.2}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(pid_S5.y, S5_if_max.u1) annotation (Line(
      points={{161,-42},{172,-42},{172,-48.8},{179.2,-48.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_S6.y, S6_if_max.u1) annotation (Line(
      points={{163,4},{172,4},{172,-4.8},{179.2,-4.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumpControl_S6, pumps_state_algo.pumps_system[1]) annotation (Line(
      points={{-100,40},{-74,40},{-74,-30.15},{-44.4167,-30.15}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumpControl_S5, pumps_state_algo.pumps_system[2]) annotation (Line(
      points={{-100,-2},{-74,-2},{-74,-23.85},{-44.4167,-23.85}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_modulation[1], S6_if_max.u2) annotation (Line(
      points={{4.33333,-47.7},{4,-47.7},{4,-44},{50,-44},{50,-26},{174,-26},{
          174,-8},{179.2,-8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_modulation[2], S5_if_max.u2) annotation (Line(
      points={{4.33333,-42.3},{2,-42.3},{2,-44},{50,-44},{50,-26},{174,-26},{
          174,-52},{179.2,-52}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(S6_if_max.y, S6_speed.u) annotation (Line(
      points={{188.4,-8},{193.2,-8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_if_max.y, S5_speed.u) annotation (Line(
      points={{188.4,-52},{193.2,-52}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumpControl_Sj, pumps_state_algo.pumps_heat) annotation (Line(
      points={{-92,-36},{-56,-36},{-56,-45},{-44.4167,-45}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(to_real[1].y, S6_speed_state.u1) annotation (Line(
      points={{124.4,26},{210,26},{210,-5.6},{215.2,-5.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S6_speed.y, S6_speed_state.u2) annotation (Line(
      points={{202.4,-8},{210,-8},{210,-10.4},{215.2,-10.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_speed.y, S5_speed_state.u2) annotation (Line(
      points={{202.4,-52},{210,-52},{210,-50.4},{213.2,-50.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(to_real[2].y, S5_speed_state.u1) annotation (Line(
      points={{124.4,26},{210,26},{210,-45.6},{213.2,-45.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S6_speed_state.y, S6_outter) annotation (Line(
      points={{224.4,-8},{236,-8},{236,-7},{249,-7}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_speed_state.y, S5_outter) annotation (Line(
      points={{222.4,-48},{234,-48},{234,-47},{247,-47}},
      color={0,0,127},
      smooth=Smooth.None));

  // Connect each heating pumps (first two pumps are system wide pumps)

  connect(pumps_state_algo.pumps_state, Pumps_state_ToReal.u) annotation (Line(
      points={{4.33333,-27},{40,-27},{40,-66.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Pumps_state_ToReal.y, pumps_state) annotation (Line(
      points={{40,-84.8},{40,-108}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_state, to_real.u) annotation (Line(
      points={{4.33333,-27},{40,-27},{40,26},{115.2,26}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(Sj_toSpeed.y, Sj_outter) annotation (Line(
      points={{218.4,40},{228,40},{228,27},{247,27}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(air_control.Sj_normSpeed_out, Sj_toSpeed.u) annotation (Line(
      points={{188.7,54.8571},{200,54.8571},{200,40},{209.2,40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(air_control.Elec_heating_Energy, Elec_heating_Energy) annotation (
      Line(
      points={{188.7,82.2857},{220,82.2857},{220,70},{246,70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(air_control.Elec_heating_Power, Elec_heating_Power) annotation (Line(
      points={{188.7,92.5714},{236,92.5714},{236,92},{246,92}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsouff, air_control.Tsouff) annotation (Line(
      points={{89,95},{105.5,95},{105.5,92.5714},{118,92.5714}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texch_out, air_control.Texch_outlet) annotation (Line(
      points={{-13,95},{-13,66},{54,66},{54,51.4286},{118,51.4286}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant, air_control.Tambiant) annotation (Line(
      points={{5,95},{5,70},{66,70},{66,58.2857},{118,58.2857}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction, air_control.Tinstruction) annotation (Line(
      points={{25,95},{25,74},{72,74},{72,65.8286},{118,65.8286}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text, air_control.Text) annotation (Line(
      points={{69,101},{69,85.3714},{118.35,85.3714}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_state[3:2+n], air_control.Pump_state) annotation (
      Line(
      points={{4.33333,-27},{40,-27},{40,78.1714},{118,78.1714}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_heat, air_control.Need_heat) annotation (Line(
      points={{49,95},{49,76},{110,76},{110,72},{118,72}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(air_control.Fan_VolFlow, Fan_VolFlow) annotation (Line(
      points={{190.45,69.6},{218,69.6},{218,51},{247,51}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-100},{240,100}},
          preserveAspectRatio=false), graphics, defaultComponentName = "pump_control"), Icon(coordinateSystem(extent={{-100,
            -100},{240,100}}, preserveAspectRatio=false), graphics={
                  Rectangle(
          extent={{-100,100},{240,-100}},
          lineColor={90,150,90},
          fillPattern=FillPattern.CrossDiag,
          fillColor={0,127,127}),
        Text(
          extent={{-100,62},{238,16}},
          lineColor={0,0,0},
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="PID"),
        Text(
          extent={{-96,-20},{240,-52}},
          lineColor={0,0,0},
          textString="Flow switch",
          fontName="Consolas",
          textStyle={TextStyle.Bold})}),
    Documentation(info="<html>
<p>Control all pumps according to pump state from algo.</p>
<p><br><br>Be careful only work with two heating pump fo now.</p>
</html>"),
    experiment(
      StopTime=2e+006,
      Interval=240,
      __Dymola_Algorithm="esdirk23a"),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end pump_pids;
