﻿within SolarSystem.Control.IGC_control.Air_vector_electric;
model Tsouff_activation "Need more power than by increasing temperature ?"
parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
  parameter Modelica.SIunits.Time tempo_souff[n]= fill(600, n)
    "How much time we have to wait before switching between Temperature or speed regulation to speed or temperature regulation";
  parameter Modelica.SIunits.Temperature Tsouff_max[n]= fill(273.15 + 32, n)
    "Maximum value of blowing temperature instruction";
  parameter Modelica.Blocks.Interfaces.RealOutput minimal_speed[n]= fill(100/3600, n)
    "Minimal speed of fans corresponding to sanitary flow rate";
  parameter Real fan_bandwidth[n]=fill(100/3600, n)
    "Bandwidth around the signal (half upper, half lower)";

  Modelica.Blocks.Interfaces.RealVectorInput Tsouff_instruction[n] annotation (
      Placement(transformation(extent={{-108,32},{-92,48}}), iconTransformation(
          extent={{-106,34},{-92,48}})));
  Utilities.Timers.Wait_for timer_instruction_test[n](wait_for=tempo_souff)
    "If `in_value` stays True at least during `wait_for` time `out_value` is True else False"
    annotation (Placement(transformation(extent={{-52,36},{-44,44}})));
  Modelica.Blocks.Logical.Hysteresis Tinstruction_test[n](uLow=Tsouff_max -
        fill(1.1, size(Tsouff_max, 1)), uHigh=Tsouff_max - fill(0.1, size(
        Tsouff_max, 1)))
    annotation (Placement(transformation(extent={{-82,36},{-74,44}})));
protected
  Modelica.Blocks.Logical.OnOffController speed_test[
                                                    n](bandwidth=
        fan_bandwidth)
    annotation (Placement(transformation(extent={{-64,-18},{-54,-8}})));
public
  Utilities.Timers.Wait_for timer_no_more_speedUp[n](wait_for=tempo_souff)
    "If `in_value` stays True at least during `wait_for` time `out_value` is True else False"
    annotation (Placement(transformation(extent={{-18,24},{-10,32}})));
  Modelica.Blocks.Interfaces.RealVectorInput Fan_speed[n] "Current fan speed"
    annotation (Placement(transformation(extent={{-108,-22},{-92,-6}}),
        iconTransformation(extent={{-106,-10},{-92,4}})));
protected
  Modelica.Blocks.Sources.RealExpression     Fan_minimalSpeed[n](y=minimal_speed)
    "Minimal fan speed" annotation (Placement(transformation(extent={{-100,6},{-90,16}}),
                   iconTransformation(extent={{-108,14},{-94,28}})));
public
  Modelica.Blocks.Interfaces.BooleanOutput Need_limitSpeed[n](each start=true)
    "ON = true, OFF = false" annotation (Placement(transformation(extent={{32,4},
            {64,36}}), iconTransformation(extent={{30,12},{48,30}})));
protected
  Modelica.Blocks.Math.Add minimal_speed_shift[n](each k2=1, each k1=1)
    "Add speed_test bandwidth to minimal fan speed"
    annotation (Placement(transformation(extent={{-78,-6},{-70,2}})));
protected
  Modelica.Blocks.Sources.RealExpression bandwidth[n](y=fan_bandwidth/2 + fill(
        0.001, n))
    annotation (Placement(transformation(extent={{-100,24},{-84,32}})));
public
  Modelica.Blocks.Logical.And No_more_speedUp[n]
    "No more need for fan speed up (Fan minimal speed is enough)"
    annotation (Placement(transformation(extent={{-34,24},{-26,32}})));
public
  Modelica.Blocks.Logical.Or Need_limitSpeed_choice[n]
    "True if at least one input True"
    annotation (Placement(transformation(extent={{6,14},{18,26}})));
public
  Modelica.Blocks.Logical.Not Not_need_speedUp[n]
    "True if we don’t need speed up"
    annotation (Placement(transformation(extent={{-34,-4},{-26,4}})));

public
  Modelica.Blocks.Logical.And Tinstruction_needed[n]
    annotation (Placement(transformation(extent={{-64,40},{-56,48}})));
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_heat[n] "True = need"
    annotation (Placement(transformation(extent={{-108,46},{-92,62}}),
        iconTransformation(extent={{-106,14},{-92,28}})));
equation
  connect(Tsouff_instruction, Tinstruction_test.u) annotation (Line(
      points={{-100,40},{-82.8,40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Fan_speed, speed_test.u) annotation (Line(
      points={{-100,-14},{-98,-14},{-98,-16},{-65,-16}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(minimal_speed_shift.y, speed_test.reference) annotation (Line(
      points={{-69.6,-2},{-68,-2},{-68,-10},{-65,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(timer_instruction_test.out_value, No_more_speedUp.u1) annotation (
      Line(
      points={{-43.44,40},{-40,40},{-40,28},{-34.8,28}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(speed_test.y, No_more_speedUp.u2) annotation (Line(
      points={{-53.5,-13},{-46,-13},{-46,24.8},{-34.8,24.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(No_more_speedUp.y, timer_no_more_speedUp.in_value) annotation (Line(
      points={{-25.6,28},{-18,28}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(timer_no_more_speedUp.out_value, Need_limitSpeed_choice.u1)
    annotation (Line(
      points={{-9.44,28},{0,28},{0,20},{4.8,20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Not_need_speedUp.y, Need_limitSpeed_choice.u2) annotation (Line(
      points={{-25.6,0},{0,0},{0,15.2},{4.8,15.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(timer_instruction_test.out_value, Not_need_speedUp.u) annotation (
      Line(
      points={{-43.44,40},{-40,40},{-40,0},{-34.8,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_limitSpeed_choice.y, Need_limitSpeed) annotation (Line(
      points={{18.6,20},{48,20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(bandwidth.y, minimal_speed_shift.u1) annotation (Line(
      points={{-83.2,28},{-82,28},{-82,0.4},{-78.8,0.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Fan_minimalSpeed.y, minimal_speed_shift.u2) annotation (Line(
      points={{-89.5,11},{-86,11},{-86,-4.4},{-78.8,-4.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction_needed.y, timer_instruction_test.in_value) annotation (
      Line(
      points={{-55.6,44},{-54,44},{-54,40},{-52,40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tinstruction_test.y, Tinstruction_needed.u2) annotation (Line(
      points={{-73.6,40},{-70,40},{-70,40.8},{-64.8,40.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_heat, Tinstruction_needed.u1) annotation (Line(
      points={{-100,54},{-70,54},{-70,44},{-64.8,44}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -20},{40,60}}),
                          graphics), Icon(coordinateSystem(extent={{-100,-20},{
            40,60}},
                  preserveAspectRatio=false), graphics={
        Polygon(
          points={{-100,60},{40,60},{40,-20},{-100,-20},{-100,60},{-100,60},{-100,60}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-84,58},{20,-12}},
          lineColor={0,128,0},
          textString="Need more speed ?")}));
end Tsouff_activation;
