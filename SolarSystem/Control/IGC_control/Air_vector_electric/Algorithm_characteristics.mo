within SolarSystem.Control.IGC_control.Air_vector_electric;
model Algorithm_characteristics
  "Contains all parameters for all control block of Sj pumps, fans, electrical heating supply, blowing instruction temperature"

  // Main informationsa about system algorithm
  parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
  parameter Modelica.SIunits.Temperature Tsouff_max[n]= fill(273.15 + 32, n)
    "Maximum value of blowing temperature instruction";

  //
  //
  // Sj_control
  //
  //

  parameter Modelica.SIunits.Time tempo_Sj[n]=fill(60, n)
    "How much time we have to wait before we start using Electrical power"
    annotation (Dialog(group="Sj_control"));

  // PID Sj
  parameter Modelica.Blocks.Types.SimpleController Sj_controllerType[n]=fill(Modelica.Blocks.Types.SimpleController.PID, n)
    "Type of controller"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_k[n]=fill(1, n) "Gain of controller"
   annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Ti[n]=fill(0.5, n)
    "Time constant of Integrator block"
   annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Td[n]=fill(0.1, n)
    "Time constant of Derivative block"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMax[n]=fill(1, n) "Upper limit of speed"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMin[n]=fill(0, n) "Lower limit of speed"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_wp[n]=fill(1, n)
    "Set-point weight for Proportional block (0..1)"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_wd[n]=fill(0, n)
    "Set-point weight for Derivative block (0..1)"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_Ni[n]=fill(0.9, n)
    "Ni*Ti is time constant of anti-windup compensation"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_Nd[n]=fill(10, n)
    "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Sj"));
  parameter Boolean Sj_reverseAction[n]=fill(false, n)
    "Set to true for throttling the water flow rate through a cooling coil controller"
     annotation (Dialog(tab="Sj"));

  //
  //
  // Electrical heat
  //
  //

  parameter Modelica.SIunits.Power electrical_power[n]=fill(5000, n)
    "Maximum value of eletrical power"
    annotation (Dialog(group="Electrical_heat"));

  // PID Elec
  parameter Modelica.Blocks.Types.SimpleController Elec_controllerType[n]=fill(Modelica.Blocks.Types.SimpleController.PID, n)
    "Type of controller" annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_k[n]=fill(1, n) "Gain of controller"  annotation (Dialog(tab="Electrical", group="PID"));
  parameter Modelica.SIunits.Time Elec_Ti[n]=fill(0.5, n)
    "Time constant of Integrator block" annotation (Dialog(tab="Electrical", group="PID"));
  parameter Modelica.SIunits.Time Elec_Td[n]=fill(0.1, n)
    "Time constant of Derivative block" annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_yMax[n]=fill(1, n) "Upper limit of output"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_yMin[n]=fill(0, n) "Lower limit of output"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_wp[n]=fill(1, n)
    "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_wd[n]=fill(0, n)
    "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_Ni[n]=fill(0.9, n)
    "Ni*Ti is time constant of anti-windup compensation"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_Nd[n]=fill(10, n)
    "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Boolean Elec_reverseAction[n]=fill(false, n)
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Electrical", group="PID"));

  //
  //
  // T_souff_activation
  //
  //

  parameter Modelica.SIunits.Time tempo_souff[n]= fill(600, n)
    "How much time we have to wait before switching between Temperature or speed regulation to speed or temperature regulation"
    annotation (Dialog(group="T_souff_activation"));
  parameter Modelica.Blocks.Interfaces.RealOutput minimal_speed[n]= fill(100/3600, n)
    "Minimal speed of fans corresponding to sanitary flow rate"
    annotation (Dialog(group="T_souff_activation"));
  parameter Real fan_bandwidth[n]=fill(100/3600, n)
    "Bandwidth around the signal (half upper, half lower)"
    annotation (Dialog(group="T_souff_activation"));

  //
  //
  // Tsouff_control
  //
  //

  // PID Tinstruction
  parameter Modelica.Blocks.Types.SimpleController Tinstruction_controllerType[n]=fill(.Modelica.Blocks.Types.SimpleController.PID, n)
    "Type of controller"
    annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_k[n]=fill(1, n) "Gain of controller"
  annotation (Dialog(tab="Tinstruction"));
  parameter Modelica.SIunits.Time Tinstruction_Ti[n]=fill(0.5, n)
    "Time constant of Integrator block"
    annotation (Dialog(tab="Tinstruction"));
  parameter Modelica.SIunits.Time Tinstruction_Td[n]=fill(0.1, n)
    "Time constant of Derivative block"
    annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_yMax[n]=fill(1, n) "Upper limit of output"
  annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_yMin[n]=fill(0, n) "Lower limit of output"
  annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_wp[n]=fill(1, n)
    "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_wd[n]=fill(0, n)
    "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_Ni[n]=fill(0.9, n)
    "Ni*Ti is time constant of anti-windup compensation"
    annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_Nd[n]=fill(10, n)
    "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Tinstruction"));
  parameter Boolean Tinstruction_reverseAction[n]=fill(false, n)
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Tinstruction"));

  //
  //
  // Fan_control
  //
  //

  // PID FAN
  parameter Modelica.Blocks.Types.SimpleController fan_controllerType[n]=fill(.Modelica.Blocks.Types.SimpleController.PID, n)
    "Type of controller" annotation (Dialog(tab="Fan"));
  parameter Real fan_k[n]=fill(1, n) "Gain of controller" annotation (Dialog(tab="Fan"));
  parameter Modelica.SIunits.Time fan_Ti[n]=fill(0.5, n)
    "Time constant of Integrator block" annotation (Dialog(tab="Fan"));
  parameter Modelica.SIunits.Time fan_Td[n]=fill(0.1, n)
    "Time constant of Derivative block" annotation (Dialog(tab="Fan"));
  parameter Real fan_yMax[n]=fill(900/3600, n) "Upper limit of output" annotation (Dialog(tab="Fan"));
  parameter Real fan_yMin[n]=fill(100/3600, n) "Lower limit of output" annotation (Dialog(tab="Fan"));
  parameter Real fan_wp[n]=fill(1, n)
    "Set-point weight for Proportional block (0..1)"                                   annotation (Dialog(tab="Fan"));
  parameter Real fan_wd[n]=fill(0, n)
    "Set-point weight for Derivative block (0..1)"                                   annotation (Dialog(tab="Fan"));
  parameter Real fan_Ni[n]=fill(0.9, n)
    "Ni*Ti is time constant of anti-windup compensation" annotation (Dialog(tab="Fan"));
  parameter Real fan_Nd[n]=fill(10, n)
    "The higher Nd, the more ideal the derivative block"                        annotation (Dialog(tab="Fan"));
  parameter Boolean fan_reverseAction[n]=fill(false, n)
    "Set to true for throttling the water flow rate through a cooling coil controller" annotation (Dialog(tab="Fan"));
end Algorithm_characteristics;
