within SolarSystem.Control.IGC_control.Air_vector_electric;
model Sj_control
  "Compute Sj speed according to the exchanger outlet temperature"
   parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
  parameter Modelica.SIunits.Time tempo_Sj[n]=fill(60, n)
    "How much time we have to wait before we start using Electrical power";
  parameter Modelica.Blocks.Types.SimpleController Sj_controllerType[n]=fill(Modelica.Blocks.Types.SimpleController.PID, n)
    "Type of controller"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_k[n]=fill(1, n) "Gain of controller"
   annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Ti[n]=fill(0.5, n)
    "Time constant of Integrator block"
   annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Td[n]=fill(0.1, n)
    "Time constant of Derivative block"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMax[n]=fill(1, n) "Upper limit of speed"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMin[n]=fill(0.1, n) "Lower limit of speed"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_wp[n]=fill(1, n)
    "Set-point weight for Proportional block (0..1)"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_wd[n]=fill(0, n)
    "Set-point weight for Derivative block (0..1)"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_Ni[n]=fill(0.9, n)
    "Ni*Ti is time constant of anti-windup compensation"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_Nd[n]=fill(10, n)
    "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Sj"));
  parameter Boolean Sj_reverseAction[n]=fill(false, n)
    "Set to true for throttling the water flow rate through a cooling coil controller"
     annotation (Dialog(tab="Sj"));

  Buildings.Controls.Continuous.LimPID pid_pump[n](
    controllerType=Sj_controllerType,
    k=Sj_k,
    Ti=Sj_Ti,
    Td=Sj_Td,
    yMax=Sj_yMax,
    yMin=Sj_yMin,
    wp=Sj_wp,
    wd=Sj_wd,
    Ni=Sj_Ni,
    reverseAction=Sj_reverseAction,
    Nd=Sj_Nd)
    annotation (Placement(transformation(extent={{-70,50},{-50,70}})));
  Modelica.Blocks.Logical.Switch switch[n]
    "Set to zero the pump speed if pump must be off"
    annotation (Placement(transformation(extent={{-20,30},{0,50}})));
protected
  Modelica.Blocks.Sources.RealExpression zeros[n](each y=0)
    annotation (Placement(transformation(extent={{-54,26},{-40,38}})));
public
  Modelica.Blocks.Interfaces.RealVectorInput Texch_outlet[n]
    "Temperature just after the exchanger outlet (air side)" annotation (
      Placement(transformation(extent={{-110,50},{-90,70}}), iconTransformation(
          extent={{-106,62},{-94,74}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tsouff_instruction[n] annotation (
      Placement(transformation(extent={{-110,10},{-90,30}}), iconTransformation(
          extent={{-106,10},{-94,22}})));
  Modelica.Blocks.Interfaces.BooleanVectorInput Pump_state[n]
    "Heating pumps states" annotation (Placement(transformation(extent={{-110,30},
            {-90,50}}),  iconTransformation(extent={{-106,36},{-94,48}})));
  Modelica.Blocks.Interfaces.RealOutput Pump_normSpeed_out[n]
    "Heating pumps normalized speed" annotation (Placement(transformation(
          extent={{16,28},{40,52}}), iconTransformation(extent={{12,38},{36,62}})));
protected
  Modelica.Blocks.Logical.OnOffController need_elec[n](each bandwidth=2)
    "True if Texch_outlet below Tinstruction_souff"
    annotation (Placement(transformation(extent={{-72,0},{-60,12}})));
public
  Utilities.Timers.Wait_for timer[n](wait_for=tempo_Sj)
    "Wait for 60 sec before "
    annotation (Placement(transformation(extent={{-16,-8},{0,8}})));
  Modelica.Blocks.Interfaces.BooleanOutput Need_electricalPower[n](each start=false)
    "ON = true, OFF = false" annotation (Placement(transformation(extent={{16,-12},
            {42,14}}), iconTransformation(extent={{12,-2},{38,24}})));
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_heat[n] "True = need"
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}}),
        iconTransformation(extent={{-106,-16},{-94,-4}})));
public
  Modelica.Blocks.Logical.And Need_elec_heat[n] "We need electrical heating"
    annotation (Placement(transformation(extent={{-34,-4},{-26,4}})));
equation
  connect(Pump_state, switch.u2) annotation (Line(
      points={{-100,40},{-22,40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(zeros.y, switch.u3) annotation (Line(
      points={{-39.3,32},{-22,32}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_pump.y, switch.u1) annotation (Line(
      points={{-49,60},{-40,60},{-40,48},{-22,48}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(switch.y, Pump_normSpeed_out) annotation (Line(
      points={{1,40},{28,40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsouff_instruction, need_elec.reference) annotation (Line(
      points={{-100,20},{-80,20},{-80,9.6},{-73.2,9.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texch_outlet, need_elec.u) annotation (Line(
      points={{-100,60},{-86,60},{-86,2.4},{-73.2,2.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(timer.out_value,Need_electricalPower)  annotation (Line(
      points={{1.12,0},{16,0},{16,1},{29,1}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(need_elec.y, Need_elec_heat.u1) annotation (Line(
      points={{-59.4,6},{-40,6},{-40,0},{-34.8,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_heat, Need_elec_heat.u2) annotation (Line(
      points={{-100,0},{-80,0},{-80,-3.2},{-34.8,-3.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_elec_heat.y, timer.in_value) annotation (Line(
      points={{-25.6,0},{-16,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tsouff_instruction, pid_pump.u_s) annotation (Line(
      points={{-100,20},{-80,20},{-80,60},{-72,60}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texch_outlet, pid_pump.u_m) annotation (Line(
      points={{-100,60},{-86,60},{-86,40},{-60,40},{-60,48}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -20},{20,80}}),
                          graphics), Icon(coordinateSystem(extent={{-100,-20},{20,
            80}}, preserveAspectRatio=false), graphics={
        Polygon(
          points={{-100,80},{20,80},{20,-20},{-100,-20},{-100,80},{-100,80},{-100,
              80}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-84,70},{0,6}},
          lineColor={0,128,0},
          textString="Sj pump control")}));
end Sj_control;
