within SolarSystem.Control.IGC_control.Air_vector_electric.Tests;
model Sj_control_test "Test of the `Sj_control` model"
  extends Modelica.Icons.Example;

  Modelica.Blocks.Sources.Sine Toutlet4(
    freqHz=1/500,
    amplitude=40,
    offset=273.15 + 25,
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-120,24},{-100,44}})));
  Modelica.Blocks.Sources.RealExpression Tinstruction_souff[4](each y=32 + 273.15)
    annotation (Placement(transformation(extent={{-120,-26},{-100,-6}})));
  Modelica.Blocks.Sources.BooleanExpression Sj_state[4](y={false,true,true,true})
    annotation (Placement(transformation(extent={{-120,-6},{-100,14}})));
  Sj_control sj_control(n=4)
    annotation (Placement(transformation(extent={{-42,-20},{48,50}})));
  Modelica.Blocks.Sources.RealExpression Toutlet2(each y=26 + 273.15)
    annotation (Placement(transformation(extent={{-120,64},{-100,84}})));
  Modelica.Blocks.Sources.RealExpression Toutlet1(each y=26 + 273.15)
    annotation (Placement(transformation(extent={{-120,82},{-100,102}})));
  Modelica.Blocks.Sources.RealExpression Toutlet3(each y=33 + 273.15)
    annotation (Placement(transformation(extent={{-120,48},{-100,68}})));
  Modelica.Blocks.Sources.BooleanExpression need_heat[4](y={false,true,false,true})
    annotation (Placement(transformation(extent={{-120,-46},{-100,-26}})));
equation
  connect(Toutlet1.y, sj_control.Texch_outlet[1]) annotation (Line(
      points={{-99,92},{-72,92},{-72,38.45},{-42,38.45}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Toutlet2.y, sj_control.Texch_outlet[2]) annotation (Line(
      points={{-99,74},{-72,74},{-72,40.55},{-42,40.55}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Toutlet3.y, sj_control.Texch_outlet[3]) annotation (Line(
      points={{-99,58},{-70,58},{-70,42.65},{-42,42.65}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Toutlet4.y, sj_control.Texch_outlet[4]) annotation (Line(
      points={{-99,34},{-70,34},{-70,44.75},{-42,44.75}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Sj_state.y, sj_control.Pump_state) annotation (Line(
      points={{-99,4},{-72,4},{-72,23.4},{-42,23.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tinstruction_souff.y, sj_control.Tsouff_instruction) annotation (Line(
      points={{-99,-16},{-72,-16},{-72,5.2},{-42,5.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(need_heat.y, sj_control.NeedHeat) annotation (Line(
      points={{-99,-36},{-58,-36},{-58,-13},{-42,-13}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{120,100}}),
                    graphics),
    experiment(StopTime=1100, __Dymola_Algorithm="Lsodar"),
    __Dymola_experimentSetupOutput,
    Documentation(info="<html>
<p><u><b>RESULTS:</b></u></p>
<p><i>Test 1:</i></p>
<ul>
<li>Pump_speed_out = 0</li>
<li>Need_electricalPower = false</li>
</ul>
<p><i>Test 2:</i></p>
<ul>
<li>Pump_speed_out = 1</li>
<li>Need_electricalPower = false --&GT; true</li>
</ul>
<p><br><i>Test 3:</i></p>
<ul>
<li>Pump_speed_out = 1 --&GT; 0.1</li>
<li>Need_electricalPower = false --&GT; false</li>
</ul>
<p><br><i>Test 4:</i></p>
<ul>
<li>Pump_speed_out = 1 --&GT; 0.1 --&GT; 1 ...</li>
<li>Need_electricalPower = false --&GT; true --&GT; false ...</li>
</ul>
<p><br><br><u><b>NOTES:</b></u></p>
<p><i>0.1</i> is the lowest value returned by the PID</p>
<p><i>0</i> is the lowest value when pump OFF</p>
</html>"),
    Icon(coordinateSystem(extent={{-120,-100},{120,100}})));
end Sj_control_test;
