within SolarSystem.Control.IGC_control.Air_vector_electric.Tests;
model Fan_control_test "Test of the `Fan_control` model"
  extends Modelica.Icons.Example;

  Modelica.Blocks.Sources.RealExpression Tinstruction[3](each y=20 + 273.15)
    annotation (Placement(transformation(extent={{-120,-26},{-100,-6}})));
  Modelica.Blocks.Sources.BooleanExpression Need_limitSpeed[3](y={false,false,true})
    annotation (Placement(transformation(extent={{-120,-6},{-100,14}})));
  Fan_control fan_control(n=3)
    annotation (Placement(transformation(extent={{-42,-20},{48,50}})));
  Modelica.Blocks.Sources.RealExpression Tamb2(each y=23 + 273.15)
    annotation (Placement(transformation(extent={{-120,34},{-100,54}})));
  Modelica.Blocks.Sources.RealExpression Tamb1(each y=17 + 273.15)
    annotation (Placement(transformation(extent={{-120,54},{-100,74}})));
  Modelica.Blocks.Sources.RealExpression Tamb3(each y=12 + 273.15)
    annotation (Placement(transformation(extent={{-120,14},{-100,34}})));
equation
  connect(Tinstruction.y, fan_control.Tinstruction) annotation (Line(
      points={{-99,-16},{-72,-16},{-72,-6.7},{-42.9,-6.7}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Need_limitSpeed.y, fan_control.Need_limitSpeed) annotation (Line(
      points={{-99,4},{-72,4},{-72,14.3},{-42.9,14.3}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tamb1.y, fan_control.Tambiant[1]) annotation (Line(
      points={{-99,64},{-72,64},{-72,32.0333},{-42.9,32.0333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb2.y, fan_control.Tambiant[2]) annotation (Line(
      points={{-99,44},{-72,44},{-72,35.3},{-42.9,35.3}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb3.y, fan_control.Tambiant[3]) annotation (Line(
      points={{-99,24},{-70,24},{-70,38.5667},{-42.9,38.5667}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{120,
            100}}), graphics),
    experiment(StopTime=1100, __Dymola_Algorithm="Lsodar"),
    __Dymola_experimentSetupOutput,
    Documentation(info="<html>
<p><u><b>RESULTS:</b></u></p>
<p><i>Test 1: Tamb &GT; Tinstruction</i></p>
<ul>
<li>switch_speed_temp.u1 = 900/3600</li>
<li>switch_speed_temp.u3 = 100/3600</li>
<li>Tsouff_instruction =900/3600</li>
</ul>
<p><i>Test 2: Tamb &LT; Tinstruction (allow speed up)</i></p>
<ul>
<li>switch_speed_temp.u1 = 100/3600</li>
<li>switch_speed_temp.u3 = 100/3600</li>
<li>Tsouff_instruction = 100/3600</li>
</ul>
<p><br><i>Test 3: Tamb &LT; Tinstruction (not allow speed up)</i></p>
<ul>
<li>switch_speed_temp.u1 = 900/3600</li>
<li>switch_speed_temp.u3 = 100/3600</li>
<li>Tsouff_instruction = 100/3600</li>
</ul>
</html>"),
    Icon(coordinateSystem(extent={{-120,-100},{120,100}})));
end Fan_control_test;
