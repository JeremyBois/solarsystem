within SolarSystem.Control.IGC_control.Air_vector_electric;
model Pump_control "Used to know pump state (tempo, modulation, ...)"
  parameter Integer n=2 "Number of heating pump (Sj)";
  parameter Modelica.SIunits.Time temporizations[n]=fill(300, n) if n > 1
    "How much time we have to wait before the outter value can be true (n-1 values)";

  parameter Modelica.SIunits.Time modulations[2]=fill(180, 2)
    "How much time we have to wait before the outter value can be true (2 values)";

  Utilities.Logical.BoolSwitch flow_limit[2] annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={70,0})));
  Modelica.Blocks.Sources.BooleanExpression normal[2](each y=true)
    annotation (Placement(transformation(extent={{16,2},{36,22}})));
  Modelica.Blocks.Sources.BooleanExpression module[2](each y=false)
    annotation (Placement(transformation(extent={{16,-22},{36,-2}})));
  Utilities.Timers.Wait_for modulation_algo[2](wait_for=modulations)
    annotation (Placement(transformation(extent={{-12,-10},{8,10}})));
  Modelica.Blocks.Interfaces.BooleanInput pumps_system[2] "[S6, S5]"
    annotation (Placement(transformation(extent={{-120,20},{-80,60}}),
        iconTransformation(extent={{-116,26},{-88,54}})));
  Modelica.Blocks.Interfaces.BooleanInput pumps_heat[n] "[Sj]*n" annotation (
      Placement(transformation(extent={{-120,-20},{-80,20}}),
        iconTransformation(extent={{-116,-14},{-88,14}})));

  Utilities.Timers.Wait_for_tempo_crack temporization_algo(n=n, wait_for=
        temporizations) if                                                            n > 1
    annotation (Placement(transformation(extent={{-60,22},{-40,38}})));

  Modelica.Blocks.Interfaces.BooleanOutput pumps_state[2 + n]
    "[S6, S5, Sj*n] states"
    annotation (Placement(transformation(extent={{120,20},{160,60}}),
        iconTransformation(extent={{-12,-12},{12,12}},
        rotation=0,
        origin={132,40})));
  Modelica.Blocks.Interfaces.BooleanOutput pumps_modulation[2]
    "[S6, S5] multiplicator" annotation (Placement(transformation(extent={{120,-20},
            {160,20}}), iconTransformation(
        extent={{-12,-12},{12,12}},
        rotation=0,
        origin={132,0})));
  Utilities.Logical.Multiplex2_boolean multiplex_boolean(      n2=n, n1=2)
    annotation (Placement(transformation(extent={{-4,26},{16,46}})));
  Modelica.Blocks.Logical.Pre Cut_SolarLoop[2](each pre_u_start=false)
    annotation (Placement(transformation(extent={{92,-10},{112,10}})));
  Modelica.Blocks.Logical.Pre Cut_loop1[2 + n](each pre_u_start=false)
    annotation (Placement(transformation(extent={{90,30},{110,50}})));
equation
  connect(normal.y, flow_limit.u1) annotation (Line(
      points={{37,12},{40,12},{40,8},{58,8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(module.y, flow_limit.u3) annotation (Line(
      points={{37,-12},{40,-12},{40,-8},{58,-8}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(modulation_algo.out_value, flow_limit.u2) annotation (Line(
      points={{9.4,0},{58,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_system, multiplex_boolean.u1) annotation (Line(
      points={{-100,40},{-20,40},{-20,42},{-6,42}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_system[1], modulation_algo[1].in_value) annotation (Line(
      points={{-100,30},{-100,16},{-40,16},{-40,0},{-12,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_system[2], modulation_algo[2].in_value) annotation (Line(
      points={{-100,50},{-20,50},{-20,0},{-12,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(flow_limit.y, Cut_SolarLoop.u) annotation (Line(
      points={{81,0},{90,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Cut_SolarLoop.y, pumps_modulation) annotation (Line(
      points={{113,0},{140,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Cut_loop1.y, pumps_state) annotation (Line(
      points={{111,40},{140,40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(multiplex_boolean.y, Cut_loop1.u) annotation (Line(
      points={{17,36},{40,36},{40,40},{88,40}},
      color={255,0,255},
      smooth=Smooth.None));

  if n > 1 then
    for i in 1:n loop
      connect(pumps_heat, temporization_algo.in_value) annotation (Line(
        points={{-100,0},{-70,0},{-70,30},{-60,30}},
        color={255,0,255},
        smooth=Smooth.None));
      connect(temporization_algo.out_value, multiplex_boolean.u2) annotation (Line(
        points={{-38.6,30},{-6,30}},
        color={255,0,255},
        smooth=Smooth.None));
    end for;
  else
    connect(pumps_heat, multiplex_boolean.u2) annotation (Line(
        points={{-100,0},{-30,0},{-30,30},{-6,30}},
        color={255,0,255},
        smooth=Smooth.None));
  end if;

                                                                                          annotation (Dialog(group="Temporization"), Icon(
        coordinateSystem(extent={{-100,-20},{140,60}},   preserveAspectRatio=false),
        graphics),
    Documentation(info="<html>
<p>Inputs and outputs order :</p>
<p>S6, S5, S4, Sj (heating pumps)</p>
<p>Heating pump order is important. Used to temporizations</p>
<p>n = heating pumps number</p>
<p>Each pumps start at 50&percnt; except if V3V extra open to backup (always a modulation on S5 and S6 yet)</p>
<p>Must have two heating pump at least due to temporization algorithm.</p>
</html>"),    Diagram(coordinateSystem(extent={{-100,-20},{140,60}},
          preserveAspectRatio=false),
                      graphics),defaultComponentName = "flow_out");
end Pump_control;
