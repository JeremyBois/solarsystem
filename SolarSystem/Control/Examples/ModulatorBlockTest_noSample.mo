within SolarSystem.Control.Examples;
model ModulatorBlockTest_noSample
  extends Modelica.Icons.Example;

  Modelica.Blocks.Continuous.Integrator FlowIntegrator
    annotation (Placement(transformation(extent={{74,-74},{100,-48}})));
public
  BaseClasses.DiffuseIsotropic RadiationInd(
    outSkyCon=false,
    outGroCon=false,
    til=0.5235987755983) "Indirect radiation on the panel"
    annotation (Placement(transformation(extent={{-62,-68},{-22,-28}})));
  BaseClasses.DirectTiltedSurface RadiationDir(
    azi(displayUnit="deg") = 3.1415926535898,
    til=0.5235987755983,
    lat(displayUnit="deg") = 0.78539816339745)
    "Direct radiation on the panel"
    annotation (Placement(transformation(extent={{-62,-14},{-22,26}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Meteo(filNam=
        "C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-120,-20},{-100,0}})));
  Scheduled_solarPanel_control_noSample
                                modulator(
    minGlobalRadiation=30,
    flowElse=0.0000005,
    flowRate(start=0.0000005))
    annotation (Placement(transformation(extent={{42,-8},{78,16}})));
  Modelica.Blocks.Sources.Constant pulse(k=21 + 273.15)
    annotation (Placement(transformation(extent={{4,68},{24,88}})));
public
  Buildings.BoundaryConditions.WeatherData.Bus WeatherData
    annotation (Placement(transformation(extent={{-16,10},{22,48}}),
        iconTransformation(extent={{-16,104},{22,142}})));
equation
  connect(Meteo.weaBus, RadiationDir.weaBus) annotation (Line(
      points={{-100,-10},{-76,-10},{-76,6},{-62,6}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Meteo.weaBus, RadiationInd.weaBus) annotation (Line(
      points={{-100,-10},{-76,-10},{-76,-48},{-62,-48}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));

  connect(RadiationInd.H, modulator.difRad)     annotation (Line(
      points={{-20,-48},{2,-48},{2,8.63636},{44.0483,8.63636}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(RadiationDir.H, modulator.dirRad)     annotation (Line(
      points={{-20,6},{4,6},{4,13.2182},{44.0483,13.2182}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(Meteo.weaBus, WeatherData) annotation (Line(
      points={{-100,-10},{-100,46},{-14,46},{-14,29},{3,29}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(WeatherData.TDryBul, modulator.T_in)
                                          annotation (Line(
      points={{3,29},{24.5,29},{24.5,1.10909},{44.0483,1.10909}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(pulse.y, modulator.T_out) annotation (Line(
      points={{25,78},{28,78},{28,-4.29091},{43.9862,-4.29091}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(modulator.flowRate, FlowIntegrator.u) annotation (Line(
      points={{77.131,3.56364},{88,3.56364},{88,-28},{56,-28},{56,-61},
          {71.4,-61}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-120,-100},{100,100}},
          preserveAspectRatio=true),
                      graphics), Icon(coordinateSystem(extent={{-120,-100},
            {100,100}})),
    experiment(StopTime=3.1536e+007, Interval=120));
end ModulatorBlockTest_noSample;
