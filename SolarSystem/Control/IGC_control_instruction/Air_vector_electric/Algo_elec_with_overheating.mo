within SolarSystem.Control.IGC_control_instruction.Air_vector_electric;
model Algo_elec_with_overheating
  "Algo for one real room with overheating by solar energy enable."

  parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
  parameter Modelica.SIunits.Temperature hotWater_setPoint=40 + 273.15
    "Hot water setpoint for DHW tank";
  SolisArt_mod.solarTank_mod solarTank_mod1(pumpControl_S5)
    annotation (Placement(transformation(extent={{120,104},{156,140}})));
  SolisArt_mod.storageTank_mod storageTank_mod1
    annotation (Placement(transformation(extent={{120,56},{166,92}})));
  SolisArt_mod.SolarValve_mod solarValve_mod
    annotation (Placement(transformation(extent={{-52,-72},{-20,-48}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-134,118},{-106,146}}),
        iconTransformation(extent={{-130,116},{-108,138}})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-134,94},{-106,122}}),
        iconTransformation(extent={{-130,86},{-108,108}})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
    annotation (Placement(transformation(extent={{-134,70},{-106,98}}),
        iconTransformation(extent={{-130,56},{-108,78}})));
  Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-134,6},{-106,34}}),
        iconTransformation(extent={{-130,-2},{-108,20}})));
  Modelica.Blocks.Interfaces.RealInput T8
    "T. at heating start point (just before extra exchanger input)"
    annotation (Placement(transformation(extent={{-134,-28},{-106,0}}),
        iconTransformation(extent={{-130,-32},{-108,-10}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tamb[n]
    "Temperature inside the room" annotation (Placement(transformation(extent={{-136,
            -156},{-108,-128}}),      iconTransformation(extent={{-130,-150},{
            -108,-128}})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-134,44},{-106,72}}),
        iconTransformation(extent={{-130,28},{-108,50}})));

  Modelica.Blocks.Interfaces.BooleanOutput
                                        S5_outter(start=false)
    "ON = 1, OFF = 0 (pump S5)"
    annotation (Placement(transformation(extent={{372,124},{404,156}}),
        iconTransformation(extent={{368,-118},{398,-88}})));
  Modelica.Blocks.Interfaces.BooleanOutput
                                        Sj_outter[n](each start=true)
    "ON = 1, OFF = 0 (pump Sj)"
    annotation (Placement(transformation(extent={{370,-16},{402,16}}),
        iconTransformation(extent={{368,-164},{398,-134}})));

  Modelica.Blocks.Interfaces.RealOutput    V3V_solar_outter(start=1)
    "Open to solar panels = 1, Open to storage tank = 0"
    annotation (Placement(transformation(extent={{374,-76},{406,-44}}),
        iconTransformation(extent={{370,90},{400,120}})));

  Heating_mod_with_overheating
              heating_mod(n=n, pumpControl_S5(start=false))
    annotation (Placement(transformation(extent={{140,-42},{194,12}})));
  Modelica.Blocks.Logical.Or or_S5
    annotation (Placement(transformation(extent={{338,130},{358,150}})));

  Modelica.Blocks.Math.BooleanToReal real_solar
                                           annotation (Placement(transformation(
        extent={{-8,8},{8,-8}},
        rotation=0,
        origin={348,-60})));

  Modelica.Blocks.Interfaces.BooleanOutput S6_outter(start=false)
    "ON = 1, OFF = 0 (pump S6)"
    annotation (Placement(transformation(extent={{370,84},{402,116}}),
        iconTransformation(extent={{366,-66},{396,-36}})));
  Utilities.Timers.Stay_on stay_on(out_value(start=false), pause=60)
    annotation (Placement(transformation(extent={{300,112},{316,128}})));
  Modelica.Blocks.Interfaces.RealVectorInput T_instruction[n]
    "Inside temperature instruction" annotation (Placement(transformation(
          extent={{-134,-82},{-106,-54}}), iconTransformation(extent={{-130,
            -114},{-108,-92}})));
  Modelica.Blocks.Interfaces.RealOutput T_instruction_out[n]
    "Inside temperature instruction" annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={218,166}), iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=90,
        origin={221,157})));
  Modelica.Blocks.Interfaces.RealOutput Tamb_out[n]
    "Temperature inside the room" annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={260,166}), iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=90,
        origin={269,157})));
  heating_activation
                  tempInside_test(n=n)
    annotation (Placement(transformation(extent={{42,-4},{88,24}})));
  Electric_DHW electric_DHW(hotWater_setPoint=hotWater_setPoint)
    annotation (Placement(transformation(extent={{64,-136},{130,-106}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_DHW_Power
    "Electrical power to keep setpoint temperature inside DHW tank" annotation (
     Placement(transformation(extent={{372,-130},{406,-96}}),
        iconTransformation(extent={{370,50},{398,78}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_DHW_Energy
    "Electrical Energy used to keep setpoint temperature inside DHW tank"
    annotation (Placement(transformation(extent={{370,-162},{404,-128}}),
        iconTransformation(extent={{370,10},{398,38}})));

  Modelica.Blocks.Interfaces.BooleanOutput Need_heat[n] annotation (Placement(
        transformation(extent={{374,44},{406,76}}), iconTransformation(extent={{-13,-13},
            {13,13}},
        rotation=90,
        origin={369,157})));

  Modelica.Blocks.Interfaces.RealVectorInput T_solar_instruction[n]
    "Inside solar temperature instruction" annotation (Placement(transformation(
          extent={{-134,-114},{-106,-86}}), iconTransformation(extent={{-130,
            -76},{-108,-54}})));
  Modelica.Blocks.Interfaces.BooleanOutput Need_overheat[n] annotation (
      Placement(transformation(extent={{374,14},{406,46}}), iconTransformation(
        extent={{-13,-13},{13,13}},
        rotation=90,
        origin={321,157})));
  Modelica.Blocks.Interfaces.RealOutput Tsolar_instruction_out[n]
    "Setpoint for solar overheating." annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={164,166}), iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=90,
        origin={179,157})));
equation
  connect(T1, solarTank_mod1.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,132.5},{120,132.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, storageTank_mod1.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,82},{120,82}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, solarValve_mod.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,-51},{-52,-51}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, solarTank_mod1.T3) annotation (Line(
      points={{-120,108},{-76,108},{-76,126.5},{120,126.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, storageTank_mod1.T3) annotation (Line(
      points={{-120,108},{-76,108},{-76,72},{120,72}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, solarValve_mod.T3) annotation (Line(
      points={{-120,108},{-76,108},{-76,-57},{-52,-57}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, solarValve_mod.T4) annotation (Line(
      points={{-120,84},{-80,84},{-80,-63},{-52,-63}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, storageTank_mod1.T5) annotation (Line(
      points={{-120,58},{-86,58},{-86,62},{120,62}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, solarValve_mod.T5) annotation (Line(
      points={{-120,58},{-86,58},{-86,-69},{-52,-69}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(solarValve_mod.V3V_solar, solarTank_mod1.V3V_solar)
    annotation (Line(
      points={{-20,-60},{24,-60},{24,111.5},{120,111.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(solarValve_mod.V3V_solar, heating_mod.V3V_solar) annotation (
      Line(
      points={{-20,-60},{106,-60},{106,-9.76875},{140.386,-9.76875}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T5, heating_mod.T5) annotation (Line(
      points={{-120,58},{-86,58},{-86,-27.15},{140,-27.15}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarTank_mod1.pumpControl_S5, or_S5.u1) annotation (Line(
      points={{156,119},{180,119},{180,140},{336,140}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(real_solar.y, V3V_solar_outter) annotation (Line(
      points={{356.8,-60},{390,-60}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(or_S5.y, S5_outter) annotation (Line(
      points={{359,140},{388,140}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(heating_mod.pumpControl_Sj, Sj_outter) annotation (Line(
      points={{194,-4.875},{194,0},{386,0}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(solarValve_mod.V3V_solar, real_solar.u) annotation (Line(
      points={{-20,-60},{338.4,-60}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(storageTank_mod1.pumpControl_S6, S6_outter) annotation (Line(
      points={{166,64},{320,64},{320,100},{386,100}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(heating_mod.pumpControl_S5, stay_on.in_value) annotation (
      Line(
      points={{194,-31.875},{194,-30},{220,-30},{220,120},{300,120}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(stay_on.out_value, or_S5.u2) annotation (Line(
      points={{317.6,120},{320,120},{320,132},{336,132}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(T1, heating_mod.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,-18.7125},{140,-18.7125}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7, heating_mod.T7) annotation (Line(
      points={{-120,20},{-80,20},{-80,22},{-92,22},{-92,-35.7563},{140.129,
          -35.7563}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb, Tamb_out) annotation (Line(
      points={{-122,-142},{260,-142},{260,166}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T_instruction, T_instruction_out) annotation (Line(
      points={{-120,-68},{-100,-68},{-100,146},{218,146},{218,166}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T_instruction, tempInside_test.Tinstruction) annotation (Line(
      points={{-120,-68},{-100,-68},{-100,9.72},{41.6167,9.72}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb, tempInside_test.Tamb) annotation (Line(
      points={{-122,-142},{34,-142},{34,-0.92},{41.6167,-0.92}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, electric_DHW.T4) annotation (Line(
      points={{-120,84},{-80,84},{-80,-121},{64,-121}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(electric_DHW.Elec_DHW, Elec_DHW_Power) annotation (Line(
      points={{129.45,-113.125},{253.725,-113.125},{253.725,-113},{389,-113}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(electric_DHW.Energy, Elec_DHW_Energy) annotation (Line(
      points={{129.45,-128.875},{258.725,-128.875},{258.725,-145},{387,-145}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(tempInside_test.Need_heat, Need_heat) annotation (Line(
      points={{88.3833,4.68},{120,4.68},{120,40},{240,40},{240,60},{390,60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T_solar_instruction, tempInside_test.Tsolar_instruction) annotation (
      Line(
      points={{-120,-100},{0,-100},{0,20.92},{41.6167,20.92}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(tempInside_test.Need_overheat, heating_mod.Need_overheat) annotation (
     Line(
      points={{88.3833,15.88},{100,15.88},{100,8.45625},{140.386,8.45625}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(tempInside_test.Need_heat, heating_mod.Need_heat) annotation (Line(
      points={{88.3833,4.68},{120,4.68},{120,0.35625},{140.386,0.35625}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(tempInside_test.Need_overheat, Need_overheat) annotation (Line(
      points={{88.3833,15.88},{100,15.88},{100,30},{390,30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T_solar_instruction, Tsolar_instruction_out) annotation (Line(
      points={{-120,-100},{0,-100},{0,148},{164,148},{164,166}},
      color={0,0,127},
      smooth=Smooth.None));
                                                                                                      annotation(Dialog(tab="Execution cycle"),
              Diagram(coordinateSystem(extent={{-120,-180},{380,160}},
          preserveAspectRatio=false),   graphics, defaultComponentName = "algo_states"), Icon(coordinateSystem(
          extent={{-120,-180},{380,160}}, preserveAspectRatio=false),
        graphics={Rectangle(
          extent={{-120,160},{382,-180}},
          lineColor={90,150,90},
          fillPattern=FillPattern.CrossDiag,
          fillColor={0,127,127}),
        Text(
          extent={{-124,108},{380,28}},
          lineColor={0,0,0},
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="Algo"),
        Text(
          extent={{-120,-24},{388,-102}},
          lineColor={0,0,0},
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="States")}),
    Documentation(info="<html>
<p>Groups all algorithms into one block.</p>
<p>This modul must be used with pump_algo modul to have real pump state inputs.</p>
</html>"));
end Algo_elec_with_overheating;
