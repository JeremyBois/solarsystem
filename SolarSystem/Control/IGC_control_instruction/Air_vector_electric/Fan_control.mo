within SolarSystem.Control.IGC_control_instruction.Air_vector_electric;
model Fan_control "Regulation of fan speed according to inside temperature"

  parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

  // PID FAN
  parameter Modelica.Blocks.Types.SimpleController fan_controllerType[n]=fill(.Modelica.Blocks.Types.SimpleController.PID, n)
    "Type of controller" annotation (Dialog(tab="Fan"));
  parameter Real fan_k[n]=fill(1, n) "Gain of controller" annotation (Dialog(tab="Fan"));
  parameter Modelica.SIunits.Time fan_Ti[n]=fill(0.5, n)
    "Time constant of Integrator block" annotation (Dialog(tab="Fan"));
  parameter Modelica.SIunits.Time fan_Td[n]=fill(0.1, n)
    "Time constant of Derivative block" annotation (Dialog(tab="Fan"));
  parameter Real fan_wp[n]=fill(1, n)
    "Set-point weight for Proportional block (0..1)"                                   annotation (Dialog(tab="Fan"));
  parameter Real fan_wd[n]=fill(0, n)
    "Set-point weight for Derivative block (0..1)"                                   annotation (Dialog(tab="Fan"));
  parameter Real fan_Ni[n]=fill(0.9, n)
    "Ni*Ti is time constant of anti-windup compensation" annotation (Dialog(tab="Fan"));
  parameter Real fan_Nd[n]=fill(10, n)
    "The higher Nd, the more ideal the derivative block"                        annotation (Dialog(tab="Fan"));
  parameter Boolean fan_reverseAction[n]=fill(false, n)
    "Set to true for throttling the water flow rate through a cooling coil controller" annotation (Dialog(tab="Fan"));
  Modelica.Blocks.Interfaces.RealVectorInput Tambiant[n] annotation (Placement(
        transformation(extent={{-114,-24},{-86,4}}), iconTransformation(extent={{-106,-8},
            {-92,6}})));
  Modelica.Blocks.Interfaces.RealVectorInput
                                       Tinstruction[n] annotation (Placement(
        transformation(extent={{-116,56},{-88,84}}),  iconTransformation(extent={{-108,54},
            {-94,68}})));
  Modelica.Blocks.Interfaces.RealOutput Fan_flowNormalized_out[n]
    "Fan flow normalized" annotation (Placement(transformation(extent={{-6,6},{22,
            34}}), iconTransformation(extent={{-2,20},{14,36}})));
  Buildings.Controls.Continuous.LimPID pid_fan[n](
    controllerType=fan_controllerType,
    k=fan_k,
    Ti=fan_Ti,
    Td=fan_Td,
    wp=fan_wp,
    wd=fan_wd,
    Ni=fan_Ni,
    Nd=fan_Nd,
    reverseAction=fan_reverseAction,
    each yMax=1,
    each yMin=0)
    "`ymin` must match minimal flow factor corresponding to minimal flow rate inside the room"
                annotation (Placement(transformation(extent={{-80,60},{-60,80}})));

  Modelica.Blocks.Interfaces.BooleanVectorInput Need_limitFlow[n]
    "Fan state (True leads to minimal flow rate)" annotation (Placement(
        transformation(extent={{-116,16},{-88,44}}), iconTransformation(extent=
            {{-108,22},{-94,36}})));
  Modelica.Blocks.Logical.Switch switch[n]
    "Set to zero the fan flow if pump must be off"
    annotation (Placement(transformation(extent={{-48,10},{-28,30}})));
protected
  Modelica.Blocks.Sources.RealExpression zeros[n](each y=0)
    annotation (Placement(transformation(extent={{-94,6},{-80,18}})));
public
  Modelica.Blocks.Logical.Not need_flowUp[n] "True if we need flow up"
    annotation (Placement(transformation(extent={{-82,26},{-74,34}})));
equation
  connect(Tinstruction, pid_fan.u_s) annotation (Line(
      points={{-102,70},{-82,70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant, pid_fan.u_m) annotation (Line(
      points={{-100,-10},{-70,-10},{-70,58}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(switch.y, Fan_flowNormalized_out) annotation (Line(
      points={{-27,20},{8,20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_fan.y, switch.u1) annotation (Line(
      points={{-59,70},{-56,70},{-56,28},{-50,28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(zeros.y, switch.u3) annotation (Line(
      points={{-79.3,12},{-50,12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(need_flowUp.y, switch.u2) annotation (Line(
      points={{-73.6,30},{-60,30},{-60,20},{-50,20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_limitFlow, need_flowUp.u) annotation (Line(
      points={{-102,30},{-82.8,30}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -20},{0,80}}),     graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-20},{0,80}}),     graphics={
        Polygon(
          points={{-100,80},{0,80},{0,-20},{-100,-20},{-100,80},{-100,80},{-100,
              80}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-90,60},{-6,-4}},
          lineColor={0,128,0},
          textString="Fan VolFlow control")}),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Differences between IGC_control_instruction and IGC_control:</span></h4>
<ul>
<li>Return a normalized flow between 0 and 1.</li>
</ul>
</html>"));
end Fan_control;
