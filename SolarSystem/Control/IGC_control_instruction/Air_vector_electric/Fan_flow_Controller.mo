within SolarSystem.Control.IGC_control_instruction.Air_vector_electric;
model Fan_flow_Controller
  "Compute the corresponding flow rate according to minimal outside air minimal flow rate."
  parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

  // Fan
  parameter Real fan_Max[n]=fill(900/3600, n) "Upper limit of fan flow rate." annotation (Dialog(tab="Fan"));

  Modelica.Blocks.Interfaces.RealInput Minimal_flow[n] "Minimal flow rate."
    annotation (Placement(transformation(extent={{-118,16},{-90,44}}),
        iconTransformation(extent={{-104,34},{-92,46}})));
  Modelica.Blocks.Interfaces.RealInput Computed_coef[n] annotation (Placement(
        transformation(extent={{-120,74},{-92,102}}), iconTransformation(extent={{-104,74},
            {-92,86}})));
  Modelica.Blocks.Interfaces.RealOutput Fan_VolFlow_out[n] "Heating fan speed"
    annotation (Placement(transformation(extent={{34,46},{62,74}}),
        iconTransformation(extent={{34,52},{50,68}})));
  Modelica.Blocks.Math.Max max[n]
    annotation (Placement(transformation(extent={{-40,50},{-20,70}})));
  Modelica.Blocks.Math.Gain max_flow[n](k=fan_Max)
    annotation (Placement(transformation(extent={{-80,68},{-66,82}})));
equation
  connect(Computed_coef, max_flow.u) annotation (Line(
      points={{-106,88},{-88,88},{-88,75},{-81.4,75}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(max_flow.y, max.u1) annotation (Line(
      points={{-65.3,75},{-54,75},{-54,66},{-42,66}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Minimal_flow, max.u2) annotation (Line(
      points={{-104,30},{-54,30},{-54,54},{-42,54}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(max.y, Fan_VolFlow_out) annotation (Line(
      points={{-19,60},{48,60}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Icon(coordinateSystem(extent={{-100,20},{40,100}},
          preserveAspectRatio=false),                             graphics={
        Polygon(
          points={{-100,100},{40,100},{40,20},{-100,20},{-100,100},{-100,100},{-100,
              100}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-72,96},{12,32}},
          lineColor={0,128,0},
          textString="Fan controller")}),
                                 Diagram(coordinateSystem(extent={{-100,20},{40,
            100}}, preserveAspectRatio=false), graphics));
end Fan_flow_Controller;
