﻿within SolarSystem.Control.IGC_control_instruction.Air_vector_electric;
model Tsouff_activation "Need more power than by increasing temperature ?"
parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
  parameter Modelica.SIunits.Time tempo_souff[n]= fill(600, n)
    "How much time we have to wait before switching between Temperature or speed regulation to speed or temperature regulation";
  parameter Modelica.SIunits.Temperature Tsouff_max[n]= fill(273.15 + 32, n)
    "Maximum value of blowing temperature instruction";
  parameter Modelica.Blocks.Interfaces.RealOutput max_sanitary_flow[n]= fill(100/3600, n)
    "Minimal speed of fans corresponding to sanitary flow rate";
  parameter Real fan_bandwidth[n]=fill(100/3600, n)
    "Bandwidth around the signal (half upper, half lower)";
  Modelica.Blocks.Interfaces.RealVectorInput Tsouff_instruction[n] annotation (
      Placement(transformation(extent={{-108,32},{-92,48}}), iconTransformation(
          extent={{-106,34},{-92,48}})));
  Utilities.Timers.Wait_for timer_instruction_test[n](wait_for=tempo_souff)
    "If `in_value` stays True at least during `wait_for` time `out_value` is True else False"
    annotation (Placement(transformation(extent={{-52,36},{-44,44}})));
  Modelica.Blocks.Logical.Hysteresis Tinstruction_test[n](uLow=Tsouff_max -
        fill(1.1, size(Tsouff_max, 1)), uHigh=Tsouff_max - fill(0.1, size(
        Tsouff_max, 1)))
    annotation (Placement(transformation(extent={{-82,36},{-74,44}})));
protected
  Modelica.Blocks.Logical.OnOffController flow_is_minimal[n](bandwidth=
        fan_bandwidth)
    annotation (Placement(transformation(extent={{-64,-18},{-54,-8}})));
public
  Utilities.Timers.Wait_for timer_no_more_speedUp[n](wait_for=tempo_souff)
    "If `in_value` stays True at least during `wait_for` time `out_value` is True else False"
    annotation (Placement(transformation(extent={{-22,24},{-14,32}})));
  Modelica.Blocks.Interfaces.RealVectorInput Fan_flow[n]
    "Current fan volumic flow rate." annotation (Placement(transformation(
          extent={{-108,-22},{-92,-6}}), iconTransformation(extent={{-106,-10},{
            -92,4}})));
public
  Modelica.Blocks.Interfaces.BooleanOutput Need_limitFlow[n](each start=true)
    "ON = true, OFF = false" annotation (Placement(transformation(extent={{32,4},
            {64,36}}), iconTransformation(extent={{30,12},{48,30}})));
protected
  Modelica.Blocks.Math.Add minimal_flow_shift[n](each k2=1, each k1=1)
    "Add speed_test bandwidth to minimal fan flow"
    annotation (Placement(transformation(extent={{-78,-4},{-70,4}})));
protected
  Modelica.Blocks.Sources.RealExpression bandwidth[n](y=fan_bandwidth/2 + fill(0.001,
        n))
    annotation (Placement(transformation(extent={{-100,24},{-84,32}})));
public
  Modelica.Blocks.Logical.And No_more_flowUp[n]
    "No more need for fan flow up (Fan minimal flow is enough)"
    annotation (Placement(transformation(extent={{-34,24},{-26,32}})));
public
  Modelica.Blocks.Logical.Or Need_limitFlow_choice[n]
    "True if at least one input True"
    annotation (Placement(transformation(extent={{-4,14},{8,26}})));
public
  Modelica.Blocks.Logical.Not Not_need_flowUp[n]
    "True if we don’t need flow up"
    annotation (Placement(transformation(extent={{-34,-4},{-26,4}})));

public
  Modelica.Blocks.Logical.And Tinstruction_needed[n]
    annotation (Placement(transformation(extent={{-64,40},{-56,48}})));
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_heat[n] "True = need"
    annotation (Placement(transformation(extent={{-108,46},{-92,62}}),
        iconTransformation(extent={{-106,14},{-92,28}})));
protected
  Modelica.Blocks.Sources.RealExpression Fan_max_sanitary_flow[n](y=
        max_sanitary_flow) "Maximal value for sanitary flow." annotation (
      Placement(transformation(extent={{-100,8},{-90,18}}), iconTransformation(
          extent={{-108,14},{-94,28}})));
equation
  connect(Tsouff_instruction, Tinstruction_test.u) annotation (Line(
      points={{-100,40},{-82.8,40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Fan_flow, flow_is_minimal.u) annotation (Line(
      points={{-100,-14},{-98,-14},{-98,-16},{-65,-16}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(minimal_flow_shift.y, flow_is_minimal.reference) annotation (Line(
      points={{-69.6,0},{-68,0},{-68,-10},{-65,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(timer_instruction_test.out_value, No_more_flowUp.u1) annotation (Line(
      points={{-43.44,40},{-40,40},{-40,28},{-34.8,28}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(flow_is_minimal.y, No_more_flowUp.u2) annotation (Line(
      points={{-53.5,-13},{-46,-13},{-46,24.8},{-34.8,24.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(No_more_flowUp.y, timer_no_more_speedUp.in_value) annotation (Line(
      points={{-25.6,28},{-22,28}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(timer_no_more_speedUp.out_value, Need_limitFlow_choice.u1)
    annotation (Line(
      points={{-13.44,28},{-10,28},{-10,20},{-5.2,20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(timer_instruction_test.out_value, Not_need_flowUp.u) annotation (Line(
      points={{-43.44,40},{-40,40},{-40,0},{-34.8,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_limitFlow_choice.y, Need_limitFlow) annotation (Line(
      points={{8.6,20},{48,20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(bandwidth.y, minimal_flow_shift.u1) annotation (Line(
      points={{-83.2,28},{-82,28},{-82,2.4},{-78.8,2.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction_needed.y, timer_instruction_test.in_value) annotation (
      Line(
      points={{-55.6,44},{-54,44},{-54,40},{-52,40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tinstruction_test.y, Tinstruction_needed.u2) annotation (Line(
      points={{-73.6,40},{-70,40},{-70,40.8},{-64.8,40.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_heat, Tinstruction_needed.u1) annotation (Line(
      points={{-100,54},{-70,54},{-70,44},{-64.8,44}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Fan_max_sanitary_flow.y, minimal_flow_shift.u2) annotation (Line(
      points={{-89.5,13},{-88,13},{-88,-2.4},{-78.8,-2.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Not_need_flowUp.y, Need_limitFlow_choice.u2) annotation (Line(
      points={{-25.6,0},{-10,0},{-10,15.2},{-5.2,15.2}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -20},{40,60}}),
                          graphics), Icon(coordinateSystem(extent={{-100,-20},{
            40,60}},
                  preserveAspectRatio=false), graphics={
        Polygon(
          points={{-100,60},{40,60},{40,-20},{-100,-20},{-100,60},{-100,60},{-100,60}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-84,58},{20,-12}},
          lineColor={0,128,0},
          textString="Need more speed ?")}),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Differences between IGC_control_instruction and IGC_control:</span></h4>
<ul>
<li>Maximal sanitary flow rate is used to check if we are at sanitary flow rate (avoid a if clause and provide hyst&eacute;risis).</li>
<li>Previous point is a hack because bandwidth is a parameter and then does not allow change during simulation.</li>
</ul>
</html>"));
end Tsouff_activation;
