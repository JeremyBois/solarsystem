within SolarSystem.Control.IGC_control_instruction.Air_vector_electric;
model Tsouff_control "Control inside temperature according instruction one"
  parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
  parameter Modelica.SIunits.Temperature Tsouff_max[n]=fill(32 + 273.15, n)
    "Maximum value of blowing temperature instruction";

  // PID Tinstruction
  parameter Modelica.Blocks.Types.SimpleController Tinstruction_controllerType[n]=fill(.Modelica.Blocks.Types.SimpleController.PID, n)
    "Type of controller"
    annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_k[n]=fill(1, n) "Gain of controller"
  annotation (Dialog(tab="Tinstruction"));
  parameter Modelica.SIunits.Time Tinstruction_Ti[n]=fill(0.5, n)
    "Time constant of Integrator block"
    annotation (Dialog(tab="Tinstruction"));
  parameter Modelica.SIunits.Time Tinstruction_Td[n]=fill(0.1, n)
    "Time constant of Derivative block"
    annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_yMax[n]=fill(1, n) "Upper limit of output"
  annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_yMin[n]=fill(0, n) "Lower limit of output"
  annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_wp[n]=fill(1, n)
    "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_wd[n]=fill(0, n)
    "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_Ni[n]=fill(0.9, n)
    "Ni*Ti is time constant of anti-windup compensation"
    annotation (Dialog(tab="Tinstruction"));
  parameter Real Tinstruction_Nd[n]=fill(10, n)
    "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Tinstruction"));
  parameter Boolean Tinstruction_reverseAction[n]=fill(false, n)
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Tinstruction"));

  Modelica.Blocks.Interfaces.RealInput Text "Exterior temperature" annotation (
      Placement(transformation(extent={{-114,16},{-86,44}}), iconTransformation(
          extent={{-104,22},{-94,32}})));
protected
  Modelica.Blocks.Math.Add Delta_instruction[n](each k1=1, each k2=-1)
    annotation (Placement(transformation(extent={{-64,30},{-58,36}})));
protected
  Modelica.Blocks.Sources.RealExpression Tmax[n](y=Tsouff_max)
    annotation (Placement(transformation(extent={{-94,36},{-80,48}})));
public
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_limitFlow[n](each start=
        true) "ON = true, OFF = false" annotation (Placement(transformation(
          extent={{-92,44},{-106,58}}), iconTransformation(extent={{-106,36},{-96,
            46}})));
  Buildings.Controls.Continuous.LimPID pid_Tinstruction[n](
    controllerType=Tinstruction_controllerType,
    k=Tinstruction_k,
    Ti=Tinstruction_Ti,
    Td=Tinstruction_Td,
    yMax=Tinstruction_yMax,
    yMin=Tinstruction_yMin,
    wp=Tinstruction_wp,
    wd=Tinstruction_wd,
    Ni=Tinstruction_Ni,
    Nd=Tinstruction_Nd,
    reverseAction=Tinstruction_reverseAction)
    annotation (Placement(transformation(extent={{-64,60},{-56,68}})));
  Modelica.Blocks.Logical.Switch switch_speed_temp[n]
    "Switch to fix temperature when fan speed modulated"
    annotation (Placement(transformation(extent={{-18,48},{-12,54}})));
  Modelica.Blocks.Interfaces.RealOutput Tsouff_instruction[n]
    "Instruction temperature of blowing" annotation (Placement(transformation(
        extent={{-9,9},{9,-9}},
        rotation=0,
        origin={5,51}), iconTransformation(
        extent={{-8,-8},{8,8}},
        rotation=0,
        origin={4,52})));
  Modelica.Blocks.Interfaces.RealVectorInput Tambiant[n] annotation (Placement(
        transformation(extent={{-106,56},{-92,70}}), iconTransformation(extent={{-106,52},
            {-96,62}})));
  Modelica.Blocks.Math.Product Delta_PID_product[n]
    annotation (Placement(transformation(extent={{-50,32},{-44,38}})));
  Modelica.Blocks.Routing.Replicator Text_replicator(nout=n)
    annotation (Placement(transformation(extent={{-80,26},{-72,34}})));

  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n]
    "Setpoint temperature inside the room" annotation (Placement(transformation(
          extent={{-106,68},{-92,82}}), iconTransformation(extent={{-106,68},{-96,
            78}})));
protected
  Modelica.Blocks.Math.Add new_Tinstruction[n](each k1=1, each k2=1)
    annotation (Placement(transformation(extent={{-34,32},{-28,38}})));
public
  Modelica.Blocks.Interfaces.RealVectorInput Tsolar_instruction[n]
    "Setpoint temperature inside the room for overheating" annotation (
      Placement(transformation(extent={{-106,84},{-92,98}}), iconTransformation(
          extent={{-106,86},{-96,96}})));
public
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_overheating[n](each start=
        true) "ON = true, OFF = false" annotation (Placement(transformation(
          extent={{-78,92},{-94,106}}), iconTransformation(extent={{-86,96},{
            -76,106}})));
  Modelica.Blocks.Logical.Switch Setpoint_used[n]
    "Switch between solar setpoint and basic setpoint according to overheating flag."
    annotation (Placement(transformation(
        extent={{-5,-5},{5,5}},
        rotation=0,
        origin={-75,79})));
equation
  connect(Tmax.y, switch_speed_temp.u3) annotation (Line(
      points={{-79.3,42},{-70,42},{-70,48.6},{-18.6,48.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Need_limitFlow, switch_speed_temp.u2) annotation (Line(
      points={{-99,51},{-18.6,51}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tambiant, pid_Tinstruction.u_m) annotation (Line(
      points={{-99,63},{-80,63},{-80,56},{-60,56},{-60,59.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_Tinstruction.y, Delta_PID_product.u1) annotation (Line(
      points={{-55.6,64},{-54,64},{-54,36.8},{-50.6,36.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Delta_instruction.y, Delta_PID_product.u2) annotation (Line(
      points={{-57.7,33},{-54,33},{-54,33.2},{-50.6,33.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text, Text_replicator.u) annotation (Line(
      points={{-100,30},{-80.8,30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text_replicator.y, Delta_instruction.u2) annotation (Line(
      points={{-71.6,30},{-68,30},{-68,31.2},{-64.6,31.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tmax.y, Delta_instruction.u1) annotation (Line(
      points={{-79.3,42},{-70,42},{-70,34.8},{-64.6,34.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(switch_speed_temp.y, Tsouff_instruction) annotation (Line(
      points={{-11.7,51},{5,51}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text_replicator.y, new_Tinstruction.u2) annotation (Line(
      points={{-71.6,30},{-68,30},{-68,22},{-40,22},{-40,33.2},{-34.6,33.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Delta_PID_product.y, new_Tinstruction.u1) annotation (Line(
      points={{-43.7,35},{-37.85,35},{-37.85,36.8},{-34.6,36.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(new_Tinstruction.y, switch_speed_temp.u1) annotation (Line(
      points={{-27.7,35},{-24,35},{-24,53.4},{-18.6,53.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsolar_instruction, Setpoint_used.u1) annotation (Line(
      points={{-99,91},{-88,91},{-88,83},{-81,83}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction, Setpoint_used.u3) annotation (Line(
      points={{-99,75},{-81,75}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Setpoint_used.y, pid_Tinstruction.u_s) annotation (Line(
      points={{-69.5,79},{-68,79},{-68,64},{-64.8,64}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Need_overheating, Setpoint_used.u2) annotation (Line(
      points={{-86,99},{-86,79},{-81,79}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,20},
            {0,100}}),    graphics), Icon(coordinateSystem(extent={{-100,20},{0,
            100}},preserveAspectRatio=false), graphics={
        Polygon(
          points={{-100,100},{0,100},{0,20},{-100,20},{-100,100},{-100,100},{
              -100,100}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-88,84},{-4,20}},
          lineColor={0,128,0},
          textString="Tsouff instruction
control")}));
end Tsouff_control;
