within SolarSystem.Control.IGC_control_instruction.Air_vector_electric.Tests;
model Tsouff_activation_test "Test of the `Tsouff_activation` model"
  extends Modelica.Icons.Example;

  Modelica.Blocks.Sources.RealExpression Tsouff_instruction[3](each y=32 +
        273.15)
    annotation (Placement(transformation(extent={{-120,72},{-100,92}})));
  Tsouff_activation tsouff_activation(n=3, tempo=fill(100, tsouff_activation.n))
    annotation (Placement(transformation(extent={{-40,-20},{50,50}})));
  Modelica.Blocks.Sources.RealExpression Fan_speed2(each y=500/3600)
    annotation (Placement(transformation(extent={{-120,14},{-100,34}})));
  Modelica.Blocks.Sources.RealExpression Fan_speed1(each y=100/3600)
    annotation (Placement(transformation(extent={{-120,34},{-100,54}})));
  Modelica.Blocks.Sources.Sine Fan_speed3(
    freqHz=1/500,
    amplitude=500/3600,
    offset=50/3600,
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-118,-20},{-98,0}})));
equation
  connect(Fan_speed1.y, tsouff_activation.Fan_speed[1]) annotation (Line(
      points={{-99,44},{-76,44},{-76,-5.70833},{-40.6429,-5.70833}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Fan_speed2.y, tsouff_activation.Fan_speed[2]) annotation (Line(
      points={{-99,24},{-80,24},{-80,-1.625},{-40.6429,-1.625}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Fan_speed3.y, tsouff_activation.Fan_speed[3]) annotation (Line(
      points={{-97,-10},{-60,-10},{-60,2.45833},{-40.6429,2.45833}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsouff_instruction.y, tsouff_activation.Tsouff_instruction)
    annotation (Line(
      points={{-99,82},{-60,82},{-60,33.375},{-39.3571,33.375}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{
            120,100}}),
                    graphics),
    experiment(StopTime=1100, __Dymola_Algorithm="Lsodar"),
    __Dymola_experimentSetupOutput,
    Documentation(info="<html>
<p><u><b>RESULTS:</b></u></p>
<p><br><i>Test 1:</i></p><p><br><img src=\"modelica://SolarSystem/Control/IGC_control/Air_vector_electric/Tests/Tsouff_activation_test1.png\"/></p>
<p><br><br><br><i>Test 2:</i></p><p><br><br><br><img src=\"modelica://SolarSystem/Control/IGC_control/Air_vector_electric/Tests/Tsouff_activation_test2.png\"/></p>
<p><br><br><i>Test 3:</i></p>
<p><img src=\"modelica://SolarSystem/Control/IGC_control/Air_vector_electric/Tests/Tsouff_activation_test3.png\"/></p>
</html>"),
    Icon(coordinateSystem(extent={{-120,-100},{120,100}})));
end Tsouff_activation_test;
