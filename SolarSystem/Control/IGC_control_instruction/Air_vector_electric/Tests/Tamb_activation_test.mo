within SolarSystem.Control.IGC_control_instruction.Air_vector_electric.Tests;
model Tamb_activation_test "Test of the `Tamb_activation` model"
  extends Modelica.Icons.Example;

  Modelica.Blocks.Sources.Sine Tamb2(
    freqHz=1/500,
    amplitude=40,
    offset=273.15 + 10,
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-120,22},{-100,42}})));
  Modelica.Blocks.Sources.RealExpression Tinstruction[3](each y=20 + 273.15)
    annotation (Placement(transformation(extent={{-120,66},{-100,86}})));
  Tamb_activation tamb_test(n=3)
    annotation (Placement(transformation(extent={{-54,34},{10,66}})));
  Modelica.Blocks.Sources.RealExpression Tamb1(y=22 + 273.15)
    annotation (Placement(transformation(extent={{-120,48},{-100,68}})));
  Modelica.Blocks.Sources.RealExpression Tamb3(y=18 + 273.15)
    annotation (Placement(transformation(extent={{-120,-8},{-100,12}})));
equation
  connect(Tinstruction.y, tamb_test.Tinstruction) annotation (Line(
      points={{-99,76},{-78,76},{-78,60.1333},{-54.5333,60.1333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb1.y, tamb_test.Tamb[1]) annotation (Line(
      points={{-99,58},{-77.5,58},{-77.5,37.3778},{-54.5333,37.3778}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb2.y, tamb_test.Tamb[2]) annotation (Line(
      points={{-99,32},{-76,32},{-76,39.8667},{-54.5333,39.8667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb3.y, tamb_test.Tamb[3]) annotation (Line(
      points={{-99,2},{-76,2},{-76,42.3556},{-54.5333,42.3556}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{120,100}}),
                    graphics),
    experiment(StopTime=1100, __Dymola_Algorithm="Lsodar"),
    __Dymola_experimentSetupOutput,
    Documentation(info="<html>
<p><u><b>RESULTS:</b></u></p>
<p><i>Test 1:</i></p>
<ul>
<li>NeedHeat = false</li>
</ul>
<p><br><i>Test 2:</i></p>
<ul>
<li>NeedHeat = true --&GT; false --&GT; true ...</li>
</ul>
<p><br><i>Test 3:</i></p>
<ul>
<li>NeedHeat = true</li>
</ul>
</html>"),
    Icon(coordinateSystem(extent={{-120,-100},{120,100}})));
end Tamb_activation_test;
