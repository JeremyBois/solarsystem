within SolarSystem.Control.IGC_control_instruction.Air_vector_electric.Tests;
model Electrical_heat_test "Test of the `Electrical_heat` model"
  extends Modelica.Icons.Example;

  Modelica.Blocks.Sources.RealExpression Tsouff_instruction[3](each y=32 +
        273.15)
    annotation (Placement(transformation(extent={{-120,72},{-100,92}})));
  Modelica.Blocks.Sources.BooleanExpression Need_electricalPower[3](y={true,
        true,false})
    annotation (Placement(transformation(extent={{-120,52},{-100,72}})));
  Electrical_heat electrical_heat(
    n=3,
    electrical_power={5000,5000,5000},
    Elec_yMin={0.2,0.2,0.2})
    annotation (Placement(transformation(extent={{-42,-20},{48,50}})));
  Modelica.Blocks.Sources.RealExpression Tsouff2(each y=33 + 273.15)
    annotation (Placement(transformation(extent={{-120,14},{-100,34}})));
  Modelica.Blocks.Sources.RealExpression Tsouff1(each y=26 + 273.15)
    annotation (Placement(transformation(extent={{-120,34},{-100,54}})));
  Modelica.Blocks.Sources.RealExpression Tsouff3(each y=26 + 273.15)
    annotation (Placement(transformation(extent={{-120,-14},{-100,6}})));
equation
  connect(Tsouff1.y, electrical_heat.Tsouff[1]) annotation (Line(
      points={{-99,44},{-70,44},{-70,-14.4583},{-41.3571,-14.4583}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsouff2.y, electrical_heat.Tsouff[2]) annotation (Line(
      points={{-99,24},{-72,24},{-72,-10.375},{-41.3571,-10.375}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsouff3.y, electrical_heat.Tsouff[3]) annotation (Line(
      points={{-99,-4},{-70,-4},{-70,-6.29167},{-41.3571,-6.29167}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Need_electricalPower.y, electrical_heat.Need_electricalPower)
    annotation (Line(
      points={{-99,62},{-64,62},{-64,15.875},{-41.3571,15.875}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tsouff_instruction.y, electrical_heat.Tsouff_instruction) annotation (
     Line(
      points={{-99,82},{-56,82},{-56,40},{-48,40},{-48,40.375},{-41.3571,40.375}},
      color={0,0,127},
      smooth=Smooth.None));

  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{
            120,100}}),
                    graphics),
    experiment(StopTime=1100, __Dymola_Algorithm="Lsodar"),
    __Dymola_experimentSetupOutput,
    Documentation(info="<html>
<p><u><b>RESULTS:</b></u></p>
<p><i>Test 1:</i></p>
<ul>
<li>switch_elec = 0.2 --&GT;1</li>
</ul>
<p><i>Test 2:</i></p>
<ul>
<li>switch_elec = 1 --&GT; 0.2</li>
</ul>
<p><br><i>Test 3:</i></p>
<ul>
<li>switch_elec = 0</li>
</ul>
<p><br><u><b>NOTES:</b></u></p>
<p><i>0.2</i> is the lowest value returned by the PID</p>
<p><i>0</i> is the lowest value when we don&rsquo;t need electrical power</p>
</html>"),
    Icon(coordinateSystem(extent={{-120,-100},{120,100}})));
end Electrical_heat_test;
