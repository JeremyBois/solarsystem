within SolarSystem.Control.IGC_control_instruction.Air_vector_electric.Tests;
model Tsouff_control_test "Test of the `Tsouff_control` model"
  extends Modelica.Icons.Example;

  Modelica.Blocks.Sources.Sine Text(
    freqHz=1/500,
    amplitude=10,
    offset=273.15 + 10,
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-120,-28},{-100,-8}})));
  Modelica.Blocks.Sources.RealExpression Tinstruction[3](each y=20 + 273.15)
    annotation (Placement(transformation(extent={{-120,74},{-100,94}})));
  Modelica.Blocks.Sources.BooleanExpression Need_limitSpeed[3](y={true,true,
        false})
    annotation (Placement(transformation(extent={{-120,-6},{-100,14}})));
  Tsouff_control tsouff_control(n=3)
    annotation (Placement(transformation(extent={{-42,-20},{48,50}})));
  Modelica.Blocks.Sources.RealExpression Tamb2(each y=23 + 273.15)
    annotation (Placement(transformation(extent={{-120,34},{-100,54}})));
  Modelica.Blocks.Sources.RealExpression Tamb1(each y=17 + 273.15)
    annotation (Placement(transformation(extent={{-120,54},{-100,74}})));
  Modelica.Blocks.Sources.RealExpression Tamb3(each y=23 + 273.15)
    annotation (Placement(transformation(extent={{-120,14},{-100,34}})));
equation
  connect(Tinstruction.y, tsouff_control.Tinstruction) annotation (Line(
      points={{-99,84},{-70,84},{-70,41.8333},{-42.9,41.8333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb1.y, tsouff_control.Tambiant[1]) annotation (Line(
      points={{-99,64},{-72,64},{-72,19.2778},{-42.9,19.2778}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb2.y, tsouff_control.Tambiant[2]) annotation (Line(
      points={{-99,44},{-70,44},{-70,23.1667},{-42.9,23.1667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb3.y, tsouff_control.Tambiant[3]) annotation (Line(
      points={{-99,24},{-70,24},{-70,27.0556},{-42.9,27.0556}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Need_limitSpeed.y, tsouff_control.Need_limitSpeed) annotation (Line(
      points={{-99,4},{-70,4},{-70,4.5},{-42.9,4.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Text.y, tsouff_control.Text) annotation (Line(
      points={{-99,-18},{-70,-18},{-70,-11.8333},{-41.1,-11.8333}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-100},{
            120,100}}),
                    graphics),
    experiment(StopTime=1100, __Dymola_Algorithm="Lsodar"),
    __Dymola_experimentSetupOutput,
    Documentation(info="<html>
<p><u><b>RESULTS:</b></u></p>
<p><i>Test 1: Tamb &LT; Tinstruction</i></p>
<ul>
<li>switch_speed_temp.u1 = 273.15 + 32</li>
<li>switch_speed_temp.u3 = 273.15 + 32</li>
<li>Tsouff_instruction = 273.15 + 32</li>
</ul>
<p><i>Test 2: Tamb &GT; Tinstruction (not allow speed up)</i></p>
<ul>
<li>switch_speed_temp.u1 = Text</li>
<li>switch_speed_temp.u3 = 273.15 + 32</li>
<li>Tsouff_instruction = Text</li>
</ul>
<p><br><i>Test 3: Tamb &GT; Tinstruction (allow speed up)</i></p>
<ul>
<li>switch_speed_temp.u1 = Text</li>
<li>switch_speed_temp.u3 = 273.15 + 32</li>
<li>Tsouff_instruction = 273.15 + 32</li>
</ul>
</html>"),
    Icon(coordinateSystem(extent={{-120,-100},{120,100}})));
end Tsouff_control_test;
