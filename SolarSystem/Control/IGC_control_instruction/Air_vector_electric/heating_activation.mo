within SolarSystem.Control.IGC_control_instruction.Air_vector_electric;
model heating_activation "Control if we need a solar overheating or just heat."
parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
protected
  Modelica.Blocks.Logical.OnOffController need_heat[n](each bandwidth=0.5)
    annotation (Placement(transformation(extent={{-56,40},{-36,60}})));
public
  Modelica.Blocks.Interfaces.RealVectorInput Tamb[n]
    "Temperature inside the room" annotation (Placement(transformation(extent={{-114,16},
            {-86,44}}),          iconTransformation(extent={{-108,24},{-94,38}})));
public
  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n]
    "Inside temperature instruction" annotation (Placement(transformation(
          extent={{-114,56},{-86,84}}), iconTransformation(extent={{-108,62},{-94,
            76}})));
  Modelica.Blocks.Interfaces.BooleanOutput Need_heat[n](each start=true)
    "ON = true, OFF = false" annotation (Placement(transformation(extent={{0,70},
            {40,30}}), iconTransformation(extent={{12,42},{30,60}})));
public
  Modelica.Blocks.Interfaces.RealVectorInput Tsolar_instruction[n]
    "Inside temperature solar instruction" annotation (Placement(transformation(
          extent={{-114,96},{-86,124}}), iconTransformation(extent={{-108,102},
            {-94,116}})));
  Modelica.Blocks.Interfaces.BooleanOutput Need_overheat[n](each start=true)
    "ON = true, OFF = false" annotation (Placement(transformation(extent={{6,80},
            {46,120}}), iconTransformation(extent={{12,82},{30,100}})));
protected
  Modelica.Blocks.Logical.OnOffController overheat_allowed[n](each bandwidth=1)
    "Check if we can overheat the room."
    annotation (Placement(transformation(extent={{-70,90},{-50,110}})));
public
  Modelica.Blocks.Logical.And need_overheating[n]
    annotation (Placement(transformation(extent={{-28,92},{-12,108}})));
public
  Modelica.Blocks.Logical.Not test_heating[n] "Test if we need heat."
    annotation (Placement(transformation(extent={{-12,64},{-28,80}})));
equation
  connect(need_heat.y, Need_heat) annotation (Line(
      points={{-35,50},{20,50}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tinstruction, need_heat.reference) annotation (Line(
      points={{-100,70},{-80,70},{-80,56},{-58,56}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb, need_heat.u) annotation (Line(
      points={{-100,30},{-80,30},{-80,44},{-58,44}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb, overheat_allowed.u) annotation (Line(
      points={{-100,30},{-80,30},{-80,94},{-72,94}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsolar_instruction, overheat_allowed.reference) annotation (Line(
      points={{-100,110},{-80,110},{-80,106},{-72,106}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(overheat_allowed.y, need_overheating.u1) annotation (Line(
      points={{-49,100},{-29.6,100}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(need_overheating.y, Need_overheat) annotation (Line(
      points={{-11.2,100},{26,100}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_heat, test_heating.u) annotation (Line(
      points={{20,50},{20,72},{-10.4,72}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(test_heating.y, need_overheating.u2) annotation (Line(
      points={{-28.8,72},{-40,72},{-40,93.6},{-29.6,93.6}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,20},
            {20,120}}),   graphics), Icon(coordinateSystem(extent={{-100,20},{
            20,120}},
                  preserveAspectRatio=false), graphics={
        Polygon(
          points={{-100,120},{20,120},{20,20},{-100,20},{-100,80},{-100,80},{
              -100,120}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-80,108},{4,36}},
          lineColor={0,128,0},
          textString="Need Heat ?")}));
end heating_activation;
