within SolarSystem.Control.IGC_control_instruction.Air_vector_electric.old;
model Regul_Tsouff_noSmooth "Regulation of temperature inside the house"

parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
parameter Modelica.SIunits.Time wait_for=60
    "How much time we have to wait before the outter value can be true";
  parameter Real is_on_max=2950
    "Value where the pump speed considered at max speed (hight limit)";
  parameter Real is_on_min=2900
    "Value where the pump speed considered at max speed (low limit)";
  parameter Modelica.Blocks.Types.SimpleController Sj_controllerType=.Modelica.Blocks.Types.SimpleController.PID
    "Type of controller"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_k=1 "Gain of controller"
   annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Ti=0.5 "Time constant of Integrator block"
   annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Td=0.1 "Time constant of Derivative block"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMax=1 "Upper limit of speed"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMin=0.1 "Lower limit of speed"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_wp=1 "Set-point weight for Proportional block (0..1)"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_wd=0 "Set-point weight for Derivative block (0..1)"
   annotation (Dialog(tab="Sj"));
  parameter Real Sj_Ni=0.9 "Ni*Ti is time constant of anti-windup compensation"
   annotation (Dialog(tab="Sj"));
  parameter Boolean Sj_reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller"
     annotation (Dialog(tab="Sj"));
  parameter Real Sj_Nd= 10 "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Sj"));
  Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n] annotation (Placement(
        transformation(extent={{-120,-80},{-80,-40}}),
                                                     iconTransformation(extent={{-114,
            -78},{-86,-50}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction_souff[n] annotation (
      Placement(transformation(extent={{-120,-20},{-80,20}}),
        iconTransformation(extent={{-114,-8},{-86,20}})));
  Modelica.Blocks.Interfaces.RealOutput pump_speed_out[n] "Heating pumps speed"
    annotation (Placement(transformation(extent={{180,20},{220,60}}),
        iconTransformation(extent={{180,14},{210,44}})));
  Buildings.Controls.Continuous.LimPID pid_pump[n](
    each controllerType=Sj_controllerType,
    each k=Sj_k,
    each Ti=Sj_Ti,
    each Td=Sj_Td,
    each yMax=Sj_yMax,
    each yMin=Sj_yMin,
    each wp=Sj_wp,
    each wd=Sj_wd,
    each Ni=Sj_Ni,
    each reverseAction=Sj_reverseAction,
    each Nd=Sj_Nd)
    annotation (Placement(transformation(extent={{-72,32},{-52,12}})));
  Modelica.Blocks.Interfaces.BooleanVectorInput pump_state[n]
    "Heating pumps states" annotation (Placement(transformation(extent={{-120,-50},
            {-80,-10}}), iconTransformation(extent={{-114,-42},{-86,-14}})));
protected
  Modelica.Blocks.Logical.Switch switch[n]
    "Set to zero the pump speed if pump must be off"
    annotation (Placement(transformation(extent={{-44,-8},{-24,12}})));
  Modelica.Blocks.Sources.RealExpression zeros[n](each y=0)
    annotation (Placement(transformation(extent={{-68,-12},{-54,0}})));
public
  Utilities.Timers.Wait_for timer[n](each wait_for=wait_for)
    "Wait for 60 sec before "
    annotation (Placement(transformation(extent={{54,-28},{70,-12}})));
  Modelica.Blocks.Logical.Or pump_test[n]
    "Test if pump off or on for more than 60 sec"
    annotation (Placement(transformation(extent={{10,-32},{22,-20}})));
  Modelica.Blocks.Logical.Hysteresis       Full_speed[n](each uLow=is_on_min, each uHigh=
        is_on_max)
    annotation (Placement(transformation(extent={{-12,-52},{0,-40}})));
protected
  Modelica.Blocks.Logical.OnOffController need_heat[n](each bandwidth=2)
    "True if Tsouff below Tinstruction_souff"
    annotation (Placement(transformation(extent={{-10,34},{10,54}})));
  Modelica.Blocks.Logical.And need_elec[n] "True if electric heater needed"
    annotation (Placement(transformation(extent={{34,-26},{46,-14}})));
public
  parameter Modelica.Blocks.Types.SimpleController Elec_controllerType=.Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="Elec"));
  parameter Real Elec_k=1 "Gain of controller" annotation (Dialog(tab="Elec"));
  parameter Modelica.SIunits.Time Elec_Ti=0.5
    "Time constant of Integrator block" annotation (Dialog(tab="Elec"));
  parameter Modelica.SIunits.Time Elec_Td=0.1
    "Time constant of Derivative block" annotation (Dialog(tab="Elec"));
  parameter Real Elec_yMax=1 "Upper limit of output"
    annotation (Dialog(tab="Elec"));
  parameter Real Elec_yMin=0 "Lower limit of output"
    annotation (Dialog(tab="Elec"));
  parameter Real Elec_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Elec"));
  parameter Real Elec_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Elec"));
  parameter Real Elec_Ni=0.9
    "Ni*Ti is time constant of anti-windup compensation"
    annotation (Dialog(tab="Elec"));
  parameter Real Elec_Nd=10
    "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Elec"));
  parameter Boolean Elec_reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Elec"));
  parameter Real electrical_power[n]={5000, 5000}
    "Maximum value of eletrical power"
    annotation (Dialog(tab="Elec", group="Power"));
public
  Buildings.Controls.Continuous.LimPID pid_Elec[n](
    each initType=Modelica.Blocks.Types.InitPID.InitialState,
    each controllerType=Elec_controllerType,
    each k=Elec_k,
    each Ti=Elec_Ti,
    each Td=Elec_Td,
    each yMax=Elec_yMax,
    each yMin=Elec_yMin,
    each wp=Elec_wp,
    each wd=Elec_wd,
    each Ni=Elec_Ni,
    each Nd=Elec_Nd,
    each reverseAction=Elec_reverseAction) "Controller for heating"
    annotation (Placement(transformation(extent={{92,-26},{104,-14}})));
public
  Modelica.Blocks.Math.Gain gaiHea[n](k=electrical_power) "Gain for heating"
    annotation (Placement(transformation(extent={{124,-24},{132,-16}})));
public
  Modelica.Blocks.Continuous.Integrator Energy_integrator[n](
    each k=1,
    each initType=Modelica.Blocks.Types.Init.InitialState,
    each y_start=0,
    each u(unit="W"),
    each y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{152,-68},{168,-52}})));
  Modelica.Blocks.Math.Mean Power[n](each f=1/3600)
    "Hourly averaged heating power"
    annotation (Placement(transformation(extent={{152,0},{168,16}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_Heating[n]
    "Power of electric power " annotation (Placement(transformation(
        extent={{-20,20},{20,-20}},
        rotation=0,
        origin={200,-20}), iconTransformation(
        extent={{-16,-16},{16,16}},
        rotation=0,
        origin={196,-10})));
  Modelica.Blocks.Interfaces.RealOutput Energy[n] "Power of electric power "
    annotation (Placement(transformation(
        extent={{-20,20},{20,-20}},
        rotation=0,
        origin={200,-60}), iconTransformation(
        extent={{-16,-16},{16,16}},
        rotation=0,
        origin={196,-54})));
  Modelica.Blocks.Logical.Switch switch_elec[n]
    annotation (Placement(transformation(extent={{110,-48},{118,-40}})));
  Modelica.Blocks.Interfaces.RealVectorInput Texch_out[n] annotation (Placement(
        transformation(extent={{-120,18},{-80,58}}), iconTransformation(extent=
            {{-114,28},{-86,56}})));

protected
  Modelica.Blocks.Logical.Not pump_state_test[n]
    annotation (Placement(transformation(extent={{-40,-36},{-28,-24}})));
equation
  connect(Tinstruction_souff, pid_pump.u_s) annotation (Line(
      points={{-100,0},{-80,0},{-80,22},{-74,22}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_pump.y, switch.u1) annotation (Line(
      points={{-51,22},{-50,22},{-50,10},{-46,10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pump_state, switch.u2) annotation (Line(
      points={{-100,-30},{-80,-30},{-80,-8},{-74,-8},{-74,2},{-46,2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(zeros.y, switch.u3) annotation (Line(
      points={{-53.3,-6},{-46,-6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(switch.y, pump_speed_out) annotation (Line(
      points={{-23,2},{30,2},{30,40},{200,40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Full_speed.y, pump_test.u2) annotation (Line(
      points={{0.6,-46},{6,-46},{6,-30.8},{8.8,-30.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(switch.y, Full_speed.u) annotation (Line(
      points={{-23,2},{-18,2},{-18,-46},{-13.2,-46}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pump_test.y, need_elec.u2) annotation (Line(
      points={{22.6,-26},{28,-26},{28,-24.8},{32.8,-24.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(need_heat.y, need_elec.u1) annotation (Line(
      points={{11,44},{26,44},{26,-20},{32.8,-20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tinstruction_souff, need_heat.reference) annotation (Line(
      points={{-100,0},{-80,0},{-80,50},{-12,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(need_elec.y, timer.in_value) annotation (Line(
      points={{46.6,-20},{54,-20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tsouff, pid_Elec.u_m) annotation (Line(
      points={{-100,-60},{98,-60},{98,-27.2}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(Tinstruction_souff, pid_Elec.u_s) annotation (Line(
      points={{-100,0},{-80,0},{-80,60},{86,60},{86,-20},{90.8,-20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(timer.out_value, switch_elec.u2) annotation (Line(
      points={{71.12,-20},{76,-20},{76,-44},{109.2,-44}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(zeros.y, switch_elec.u3) annotation (Line(
      points={{-53.3,-6},{-50,-6},{-50,-54},{106,-54},{106,-47.2},{109.2,
          -47.2}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(pid_Elec.y, switch_elec.u1) annotation (Line(
      points={{104.6,-20},{106,-20},{106,-40.8},{109.2,-40.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiHea.y, Power.u) annotation (Line(
      points={{132.4,-20},{140,-20},{140,8},{150.4,8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiHea.y, Energy_integrator.u) annotation (Line(
      points={{132.4,-20},{140,-20},{140,-60},{150.4,-60}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiHea.y, Elec_Heating) annotation (Line(
      points={{132.4,-20},{200,-20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Energy_integrator.y, Energy) annotation (Line(
      points={{168.8,-60},{200,-60}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texch_out, need_heat.u) annotation (Line(
      points={{-100,38},{-12,38}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texch_out, pid_pump.u_m) annotation (Line(
      points={{-100,38},{-62,38},{-62,34}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(switch_elec.y, gaiHea.u) annotation (Line(
      points={{118.4,-44},{120,-44},{120,-20},{123.2,-20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pump_state, pump_state_test.u) annotation (Line(
      points={{-100,-30},{-41.2,-30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pump_state_test.y, pump_test.u1) annotation (Line(
      points={{-27.4,-30},{0,-30},{0,-26},{8.8,-26}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -80},{200,60}}),   graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-80},{200,60}}),   graphics={
        Polygon(
          points={{-100,60},{200,60},{200,-80},{-100,-80},{-100,60},{-100,60},{
              -100,60}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-40,36},{148,-52}},
          lineColor={0,128,0},
          textString="Electrical heating control")}),
    Documentation(info="<html>
<p>Need blowing temperature and exhanger outlet temperature for PIDS.</p>
<p>Electrical can useblowing one but in order to optimize power of solar collector we must use the exchanger outlet temperature.</p>
</html>"));
end Regul_Tsouff_noSmooth;
