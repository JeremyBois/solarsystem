within SolarSystem.Control.IGC_control_instruction.Air_vector_electric.old;
model Regul_Tamb "Regulation of temperature inside the house"

parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
  Modelica.Blocks.Interfaces.RealVectorInput Tambiant[n] annotation (Placement(
        transformation(extent={{-120,20},{-80,60}}), iconTransformation(extent={
            {-112,28},{-90,50}})));
  Modelica.Blocks.Interfaces.RealVectorInput
                                       Tinstruction[n] annotation (Placement(
        transformation(extent={{-120,-20},{-80,20}}), iconTransformation(extent={{-112,
            -12},{-90,10}})));
  Modelica.Blocks.Interfaces.RealOutput fan_speed_out[n] "Heating fan speed"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}}),
        iconTransformation(extent={{-12,4},{18,34}})));
  Buildings.Controls.Continuous.LimPID pid_fan[n](
    each yMax=yMax,
    each yMin=yMin,
    each controllerType=controllerType,
    each k=k,
    each Ti=Ti,
    each reverseAction=reverseAction,
    each Td=Td,
    each wp=wp,
    each wd=wd,
    each Ni=Ni) annotation (Placement(transformation(extent={{-60,10},{-40,-10}})));

  parameter Modelica.Blocks.Types.SimpleController controllerType=.Modelica.Blocks.Types.SimpleController.PID
    "Type of controller";
  parameter Real k=1 "Gain of controller";
  parameter Modelica.SIunits.Time Ti=0.5 "Time constant of Integrator block";
  parameter Modelica.SIunits.Time Td=0.1 "Time constant of Derivative block";
  parameter Real yMax=900/3600 "Upper limit of output";
  parameter Real yMin=100/3600 "Lower limit of output";
  parameter Real wp=1 "Set-point weight for Proportional block (0..1)";
  parameter Real wd=0 "Set-point weight for Derivative block (0..1)";
  parameter Real Ni=0.9 "Ni*Ti is time constant of anti-windup compensation";
  parameter Boolean reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller";
equation
  connect(Tinstruction, pid_fan.u_s) annotation (Line(
      points={{-100,0},{-62,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant, pid_fan.u_m) annotation (Line(
      points={{-100,40},{-50,40},{-50,12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_fan.y, fan_speed_out) annotation (Line(
      points={{-39,0},{0,0}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -20},{0,60}}),     graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-20},{0,60}}),     graphics={
        Polygon(
          points={{-100,60},{0,60},{0,-20},{-100,-20},{-100,60},{-100,60},{-100,
              60}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-98,52},{-14,-12}},
          lineColor={0,128,0},
          textString="Fan speed control")}));
end Regul_Tamb;
