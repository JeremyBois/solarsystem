within SolarSystem.Control.IGC_control_instruction.Air_vector_electric.old;
model Heating_mod

  import SI = Modelica.SIunits;
parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states : true = ON, false = OFF)";

  Modelica.Blocks.Interfaces.RealInput Tambiant[n] "T. in the room"
    annotation (Placement(transformation(extent={{-120,110},{-80,150}}),
        iconTransformation(extent={{-112,104},{-80,136}})));
  Modelica.Blocks.Interfaces.RealInput  DTeco "Return DTeco value"
    annotation (Placement(transformation(extent={{-120,70},{-80,110}}),
        iconTransformation(extent={{-112,64},{-80,96}})));
  Modelica.Blocks.Interfaces.BooleanInput V3V_solar
    "Open to solar panels = true, Open to storage tank = false"
    annotation (Placement(transformation(extent={{-120,-50},{-80,-10}}),
        iconTransformation(extent={{-114,-78},{-80,-44}})));
  Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-122,-270},{-82,-230}}),
        iconTransformation(extent={{-112,-266},{-78,-232}})));
  Modelica.Blocks.Interfaces.RealInput T8
    "T. at heating start point (just before extra exchanger input)"
    annotation (Placement(transformation(extent={{-122,-240},{-82,-200}}),
        iconTransformation(extent={{-112,-306},{-78,-272}})));
  Modelica.Blocks.Interfaces.RealInput TsolarInstruction[n] annotation (
      Placement(transformation(extent={{-120,30},{-80,70}}), iconTransformation(
          extent={{-112,24},{-80,56}})));

  Modelica.Blocks.MathBoolean.Or all_on[n](each nu=2)
    annotation (Placement(transformation(extent={{180,40},{200,60}})));
  Modelica.Blocks.Math.Add3 deltaT_2[n](
    each k1=1,
    each k2=1,
    each k3=-1) "Tinstruction + 0.1 - DTeco"
    annotation (Placement(transformation(extent={{20,38},{38,56}})));
  Modelica.Blocks.MathBoolean.And on2[n](each nu=3)
    annotation (Placement(transformation(extent={{140,40},{160,60}})));
  Modelica.Blocks.Logical.Less deltaT_2_test[n]
    annotation (Placement(transformation(extent={{60,40},{80,60}})));
  Modelica.Blocks.Logical.Less delta_T3[n]
    annotation (Placement(transformation(extent={{60,0},{80,20}})));
  Modelica.Blocks.MathBoolean.And on3[n](each nu=3)
    annotation (Placement(transformation(extent={{140,0},{160,20}})));
  Buildings.Utilities.Math.BooleanReplicator
                                     replicator4(
                                                nout=n) annotation (
      Placement(transformation(extent={{-60,-40},{-40,-20}})));
  Modelica.Blocks.Logical.Greater deltaT_test[n]
    annotation (Placement(transformation(extent={{60,-208},{80,-188}})));
  Modelica.Blocks.Logical.Or or1[n]
    annotation (Placement(transformation(extent={{100,-208},{120,-188}})));
  Modelica.Blocks.Math.Add      deltaT_5(k1=+1, k2=-1) "T8 - T7"
    annotation (Placement(transformation(extent={{-54,-236},{-34,-216}})));
  Modelica.Blocks.Logical.LessThreshold deltaT_4_test(threshold=2.5)
    annotation (Placement(transformation(extent={{-10,-240},{10,-220}})));
  Buildings.Utilities.Math.BooleanReplicator
                                     replicator9(
                                                nout=n) annotation (
      Placement(transformation(extent={{60,-240},{80,-220}})));
  Modelica.Blocks.Logical.Not not_off2[n]
    annotation (Placement(transformation(extent={{180,-208},{200,-188}})));
  Modelica.Blocks.MathBoolean.And off2[n](each nu=2)
    annotation (Placement(transformation(extent={{140,-208},{160,-188}})));
  Modelica.Blocks.Logical.Not not_solar[
                                       n] "V3V to tank"
    annotation (Placement(transformation(extent={{-6,-48},{6,-36}})));
  Modelica.Blocks.MathBoolean.And final_state[n](each nu=3)
    annotation (Placement(transformation(extent={{240,-72},{264,-48}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold T5_test[n](each threshold=
        273.15 + 40)
    annotation (Placement(transformation(extent={{60,-80},{80,-60}})));
  Modelica.Blocks.Logical.Greater          T1_test[n]
    annotation (Placement(transformation(extent={{60,-120},{80,-100}})));
  Modelica.Blocks.Routing.Replicator replicator5(
                                                nout=n) annotation (
      Placement(transformation(extent={{20,-80},{40,-60}})));
  Modelica.Blocks.Routing.Replicator replicator6(
                                                nout=n) annotation (
      Placement(transformation(extent={{20,-104},{36,-88}})));
  Modelica.Blocks.Logical.Or or2[n]
    annotation (Placement(transformation(extent={{100,-280},{120,-260}})));
  Modelica.Blocks.Math.Add add2[n](each k1=1, each k2=1)
    "TsolarInstruction + 0.2"
    annotation (Placement(transformation(extent={{-10,-288},{10,-268}})));
  Modelica.Blocks.Sources.RealExpression cst2[n](each y=0.3)
    "Minimal temperature inside each rooms"
    annotation (Placement(transformation(extent={{-62,-272},{-44,-254}})));
  Modelica.Blocks.Logical.Greater add_test[n]
    annotation (Placement(transformation(extent={{58,-288},{78,-268}})));
  Modelica.Blocks.MathBoolean.And off3[n](each nu=2)
    annotation (Placement(transformation(extent={{140,-280},{160,-260}})));
  Modelica.Blocks.Logical.Not not_off3[n]
    annotation (Placement(transformation(extent={{180,-280},{200,-260}})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-120,-90},{-80,-50}}),
        iconTransformation(extent={{-114,-192},{-82,-160}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-120,-120},{-80,-80}}),
        iconTransformation(extent={{-114,-150},{-82,-118}})));

  Utilities.Timers.Wait_for wait_for "T8 - T7 timer"
    annotation (Placement(transformation(extent={{20,-240},{40,-220}})));
  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S5(start=true)
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{300,-260},{340,-220}}),
        iconTransformation(extent={{300,-260},{340,-220}})));

  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_Sj[
                                                         n](each start=true)
    "ON = true, OFF = false (pump Sj)"
    annotation (Placement(transformation(extent={{310,-80},{350,-40}}),
        iconTransformation(extent={{300,-90},{340,-50}})));

  Utilities.Logical.True_seeker true_seeker(n=n)
    annotation (Placement(transformation(extent={{252,-226},{292,-194}})));

  Modelica.Blocks.Routing.Replicator replicator1(
                                                nout=n) annotation (
      Placement(transformation(extent={{-64,80},{-44,100}})));
  Modelica.Blocks.Sources.RealExpression cst1[n](each y=0.2)
    annotation (Placement(transformation(extent={{-6,42},{6,52}})));
  Modelica.Blocks.Math.Add add1[n](each k1=1, each k2=1)
    "TsolarInstruction + 0.2"
    annotation (Placement(transformation(extent={{18,-200},{38,-180}})));
  Utilities.Timers.Stay_on stay_on[n](each pause=60, out_value(each start=false))
    annotation (Placement(transformation(extent={{280,-68},{296,-52}})));
  Modelica.Blocks.Math.Max max_28_T7
    "Return the minimal value between T3 and T4"
    annotation (Placement(transformation(extent={{-70,-132},{-52,-114}})));
  Modelica.Blocks.Sources.RealExpression cst3(each y=40 + 273.15)
    annotation (Placement(transformation(extent={{-100,-126},{-78,-110}})));
  Modelica.Blocks.Math.Add add4(each k2=1, each k1=1) "TsolarInstruction + 0.2"
    annotation (Placement(transformation(extent={{-14,-128},{2,-112}})));
  Modelica.Blocks.Sources.RealExpression cst4(each y=5)
    annotation (Placement(transformation(extent={{-100,-162},{-78,-146}})));
  Modelica.Blocks.Routing.Replicator replicator2(
                                                nout=n) annotation (
      Placement(transformation(extent={{20,-128},{36,-112}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n]
    "Tinstruction in the room" annotation (Placement(transformation(extent={{-120,
            -200},{-80,-160}}), iconTransformation(extent={{-114,-222},{-92,-200}})));
equation
  connect(Tambiant, deltaT_2_test.u1)
                                 annotation (Line(
      points={{-100,130},{48,130},{48,50},{58,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant,delta_T3. u1) annotation (Line(
      points={{-100,130},{48,130},{48,10},{58,10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_T3.y, on3.u[1])      annotation (Line(
      points={{81,10},{106,10},{106,14.6667},{140,14.6667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_solar, replicator4.u)   annotation (Line(
      points={{-100,-30},{-62,-30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator4.y, on3.u[2])      annotation (Line(
      points={{-39,-30},{128,-30},{128,10},{140,10}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on2.y, all_on.u[1])        annotation (Line(
      points={{161.5,50},{170,50},{170,53.5},{180,53.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on3.y, all_on.u[2])        annotation (Line(
      points={{161.5,10},{168,10},{168,46.5},{180,46.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tambiant, deltaT_test.u1)
                                  annotation (Line(
      points={{-100,130},{48,130},{48,-198},{58,-198}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(deltaT_test.y, or1.u1)     annotation (Line(
      points={{81,-198},{98,-198}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(deltaT_5.y, deltaT_4_test.u)
                                    annotation (Line(
      points={{-33,-226},{-24,-226},{-24,-230},{-12,-230}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(replicator9.y, or1.u2)       annotation (Line(
      points={{81,-230},{92,-230},{92,-206},{98,-206}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator4.y,not_solar. u) annotation (Line(
      points={{-39,-30},{-22,-30},{-22,-42},{-7.2,-42}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(not_solar.y, off2.u[1])      annotation (Line(
      points={{6.6,-42},{116,-42},{116,-176},{128,-176},{128,-194.5},{140,-194.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(or1.y, off2.u[2])            annotation (Line(
      points={{121,-198},{124,-198},{124,-201.5},{140,-201.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(off2.y, not_off2.u)       annotation (Line(
      points={{161.5,-198},{178,-198}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(all_on.y, final_state.u[1]) annotation (Line(
      points={{201.5,50},{220,50},{220,-54.4},{240,-54.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(not_off2.y, final_state.u[2])
                                       annotation (Line(
      points={{201,-198},{226,-198},{226,-62},{240,-62},{240,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(deltaT_2_test.y, on2.u[1]) annotation (Line(
      points={{81,50},{112,50},{112,54.6667},{140,54.6667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(not_solar.y, on2.u[2])      annotation (Line(
      points={{6.6,-42},{124,-42},{124,50},{140,50}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator5.y, T5_test.u)   annotation (Line(
      points={{41,-70},{58,-70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5_test.y, on2.u[3])        annotation (Line(
      points={{81,-70},{132,-70},{132,45.3333},{140,45.3333}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T1_test.y, on3.u[3])        annotation (Line(
      points={{81,-110},{136,-110},{136,5.33333},{140,5.33333}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator9.y, or2.u1)       annotation (Line(
      points={{81,-230},{92,-230},{92,-270},{98,-270}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tambiant, add_test.u1)  annotation (Line(
      points={{-100,130},{48,130},{48,-278},{56,-278}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add_test.y, or2.u2)        annotation (Line(
      points={{79,-278},{98,-278}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator4.y, off3.u[1])      annotation (Line(
      points={{-39,-30},{128,-30},{128,-266.5},{140,-266.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(or2.y, off3.u[2])            annotation (Line(
      points={{121,-270},{130,-270},{130,-273.5},{140,-273.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(off3.y, not_off3.u)       annotation (Line(
      points={{161.5,-270},{178,-270}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(not_off3.y, final_state.u[3])
                                       annotation (Line(
      points={{201,-270},{232,-270},{232,-65.6},{240,-65.6}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T5,replicator5. u) annotation (Line(
      points={{-100,-70},{18,-70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(deltaT_4_test.y, wait_for.in_value) annotation (Line(
      points={{11,-230},{20,-230}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(wait_for.out_value, replicator9.u) annotation (Line(
      points={{41.4,-230},{58,-230}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(true_seeker.y, pumpControl_S5) annotation (Line(
      points={{292,-210},{306,-210},{306,-240},{320,-240}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on3.y, true_seeker.u) annotation (Line(
      points={{161.5,10},{210,10},{210,-210},{252,-210}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(TsolarInstruction, delta_T3.u2) annotation (Line(
      points={{-100,50},{-26,50},{-26,2},{58,2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add2.y, add_test.u2) annotation (Line(
      points={{11,-278},{31.5,-278},{31.5,-286},{56,-286}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst2.y, add2.u2) annotation (Line(
      points={{-43.1,-263},{-30,-263},{-30,-284},{-12,-284}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TsolarInstruction, add2.u1) annotation (Line(
      points={{-100,50},{-26,50},{-26,-272},{-12,-272}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(deltaT_2.y, deltaT_2_test.u2) annotation (Line(
      points={{38.9,47},{44,47},{44,42},{58,42}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(DTeco, replicator1.u) annotation (Line(
      points={{-100,90},{-66,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(replicator1.y, deltaT_2.u3) annotation (Line(
      points={{-43,90},{-34,90},{-34,39.8},{18.2,39.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst1.y, deltaT_2.u2) annotation (Line(
      points={{6.6,47},{18.2,47}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst2.y, add1.u2) annotation (Line(
      points={{-43.1,-263},{-30,-263},{-30,-196},{16,-196}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add1.y, deltaT_test.u2) annotation (Line(
      points={{39,-190},{44,-190},{44,-206},{58,-206}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(final_state.y, stay_on.in_value) annotation (Line(
      points={{265.8,-60},{280,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(stay_on.out_value, pumpControl_Sj) annotation (Line(
      points={{297.6,-60},{330,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T7, max_28_T7.u2) annotation (Line(
      points={{-102,-250},{-68,-250},{-68,-160},{-68,-146},{-80,-146},{-80,-128.4},
          {-76,-128.4},{-71.8,-128.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst3.y, max_28_T7.u1) annotation (Line(
      points={{-76.9,-118},{-74,-118},{-74,-117.6},{-71.8,-117.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(max_28_T7.y, add4.u1) annotation (Line(
      points={{-51.1,-123},{-46,-123},{-46,-115.2},{-15.6,-115.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst4.y, add4.u2) annotation (Line(
      points={{-76.9,-154},{-60,-154},{-60,-134},{-40,-134},{-40,-124.8},{-15.6,
          -124.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(replicator6.y, T1_test.u1) annotation (Line(
      points={{36.8,-96},{48,-96},{48,-110},{58,-110}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(replicator2.y, T1_test.u2) annotation (Line(
      points={{36.8,-120},{48,-120},{48,-118},{58,-118}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add4.y, replicator2.u) annotation (Line(
      points={{2.8,-120},{18.4,-120}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, replicator6.u) annotation (Line(
      points={{-100,-100},{13,-100},{13,-96},{18.4,-96}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction, add1.u1) annotation (Line(
      points={{-100,-180},{-20,-180},{-20,-184},{16,-184}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction, deltaT_2.u1) annotation (Line(
      points={{-100,-180},{-20,-180},{-20,54},{0,54},{0,54.2},{18.2,54.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T8, deltaT_5.u1) annotation (Line(
      points={{-102,-220},{-56,-220}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7, deltaT_5.u2) annotation (Line(
      points={{-102,-250},{-68,-250},{-68,-232},{-56,-232}},
      color={0,0,127},
      smooth=Smooth.None));

  annotation (Diagram(coordinateSystem(extent={{-100,-300},{320,140}},
          preserveAspectRatio=false),  graphics), Icon(coordinateSystem(extent={{-100,
            -300},{320,140}}, preserveAspectRatio=false),  graphics={
        Text(
          extent={{-58,24},{284,-236}},
          lineColor={0,128,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          textString="Nb Room
%n"),   Polygon(
          points={{-100,140},{320,140},{320,-300},{-100,-300},{-100,140},{-100,140},
              {-100,140}},
          lineColor={255,0,255},
          smooth=Smooth.None)}));
end Heating_mod;
