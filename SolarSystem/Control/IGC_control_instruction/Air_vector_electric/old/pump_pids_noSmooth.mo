within SolarSystem.Control.IGC_control_instruction.Air_vector_electric.old;
model pump_pids_noSmooth "flow control for each pump"
  import SolarSystem;
  // How many heated rooms
  parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

  // Flow Control
  parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm right_pumps=1
    "Prescribed rotational speed for pumps of right extra valve side (unit=1/min)";
  parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm left_pumps=1
    "Prescribed rotational speed for pumps of left extra valve side";
  parameter Modelica.SIunits.Time temporizations[n - 1]={300}
    "How much time we have to wait before the outter value can be true (n-1 values)";
  parameter Modelica.SIunits.Time modulations[2]={180,180}
    "How much time we have to wait before the outter value can be true (2 values)";

  // S6
  parameter Modelica.Blocks.Types.SimpleController S6_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S6"));
  parameter Real S6_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S6"));
  parameter Real S6_k=0.1 "Gain of controller" annotation (Dialog(tab="S6"));
  parameter Modelica.SIunits.Time S6_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S6"));
  parameter Modelica.SIunits.Time S6_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S6"));
  parameter Real S6_yMax=1 "Upper limit of output" annotation (Dialog(tab="S6"));
  parameter Real S6_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S6"));
  parameter Real S6_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S6"));
  parameter Real S6_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S6"));

  // S5
  parameter Modelica.Blocks.Types.SimpleController S5_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S5"));
  parameter Real S5_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S5"));
  parameter Real S5_k=0.1 "Gain of controller" annotation (Dialog(tab="S5"));
  parameter Modelica.SIunits.Time S5_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S5"));
  parameter Modelica.SIunits.Time S5_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S5"));
  parameter Real S5_yMax=1 "Upper limit of output" annotation (Dialog(tab="S5"));
  parameter Real S5_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S5"));
  parameter Real S5_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S5"));
  parameter Real S5_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S5"));

  // S4
  parameter Modelica.Blocks.Types.SimpleController S4_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S4"));
  parameter Real S4_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S4"));
  parameter Real S4_k=0.1 "Gain of controller" annotation (Dialog(tab="S4"));
  parameter Modelica.SIunits.Time S4_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S4"));
  parameter Modelica.SIunits.Time S4_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S4"));
  parameter Real S4_yMax=1 "Upper limit of output" annotation (Dialog(tab="S4"));
  parameter Real S4_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S4"));
  parameter Real S4_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S4"));
  parameter Real S4_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S4"));

  // Sj
  parameter Modelica.Blocks.Types.SimpleController Sj_controllerType=.Modelica.Blocks.Types.SimpleController.PID
    "Type of controller"
   annotation (Dialog(tab="Sj", group="PID"));
  parameter Real Sj_k=1 "Gain of controller"
   annotation (Dialog(tab="Sj", group="PID"));
  parameter Modelica.SIunits.Time Sj_Ti=0.5 "Time constant of Integrator block"
   annotation (Dialog(tab="Sj", group="PID"));
  parameter Modelica.SIunits.Time Sj_Td=0.1 "Time constant of Derivative block"
   annotation (Dialog(tab="Sj", group="PID"));
  parameter Real Sj_yMax=1 "Upper limit of speed"
   annotation (Dialog(tab="Sj", group="PID"));
  parameter Real Sj_yMin=0.1 "Lower limit of speed"
   annotation (Dialog(tab="Sj", group="PID"));
  parameter Real Sj_wp=1 "Set-point weight for Proportional block (0..1)"
   annotation (Dialog(tab="Sj", group="PID"));
  parameter Real Sj_wd=0 "Set-point weight for Derivative block (0..1)"
   annotation (Dialog(tab="Sj", group="PID"));
  parameter Real Sj_Ni=0.9 "Ni*Ti is time constant of anti-windup compensation"
   annotation (Dialog(tab="Sj", group="PID"));
  parameter Boolean Sj_reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller"
     annotation (Dialog(tab="Sj", group="PID"));
  parameter Real Sj_Nd= 10 "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Sj", group="PID"));
  parameter Modelica.SIunits.Time wait_for=60
    "How much time we have to wait before the outter value can be true"
    annotation (Dialog(tab="Sj", group="Speed"));
  parameter Real is_on_max=0.98
    "Value where the pump speed considered at max speed (hight limit)" annotation (Dialog(tab="Sj", group="Speed"));
  parameter Real is_on_min=0.94
    "Value where the pump speed considered at max speed (low limit)"
    annotation (Dialog(tab="Sj", group="Speed"));

 // Electrical PID
  parameter Modelica.Blocks.Types.SimpleController Elec_controllerType=.Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="Elec"));
  parameter Real Elec_k=1 "Gain of controller" annotation (Dialog(tab="Elec"));
  parameter Modelica.SIunits.Time Elec_Ti=0.5
    "Time constant of Integrator block" annotation (Dialog(tab="Elec"));
  parameter Modelica.SIunits.Time Elec_Td=0.1
    "Time constant of Derivative block" annotation (Dialog(tab="Elec"));
  parameter Real Elec_yMax=1 "Upper limit of output"
    annotation (Dialog(tab="Elec"));
  parameter Real Elec_yMin=0 "Lower limit of output"
    annotation (Dialog(tab="Elec"));
  parameter Real Elec_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Elec"));
  parameter Real Elec_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Elec"));
  parameter Real Elec_Ni=0.9
    "Ni*Ti is time constant of anti-windup compensation"
    annotation (Dialog(tab="Elec"));
  parameter Real Elec_Nd=10
    "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Elec"));
  parameter Boolean Elec_reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Elec"));
  parameter Modelica.Blocks.Types.SimpleController fan_controllerType=.Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="fan"));
  parameter Real fan_k=1 "Gain of controller" annotation (Dialog(tab="fan"));
  parameter Modelica.SIunits.Time fan_Ti=0.5
    "Time constant of Integrator block"
    annotation (Dialog(tab="fan"));
  parameter Modelica.SIunits.Time fan_Td=0.1
    "Time constant of Derivative block"
    annotation (Dialog(tab="fan"));
  parameter Real fan_yMax=0.9 "Upper limit of output" annotation (Dialog(tab="fan"));
  parameter Real fan_yMin=0.1 "Lower limit of output" annotation (Dialog(tab="fan"));
  parameter Real fan_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="fan"));
  parameter Real fan_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="fan"));
  parameter Boolean fan_reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="fan"));
  parameter Real electrical_power[n]={5000, 5000}
    "Maximum value of eletrical power"
    annotation (Dialog(tab="Elec", group="Power"));

  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_Sj[
                                                         n]
    "ON = true, OFF = false (pump Sj)"
    annotation (Placement(transformation(extent={{-120,-56},{-80,-16}}),
        iconTransformation(extent={{-108,-60},{-74,-26}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S5
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{-120,-22},{-80,18}}),
        iconTransformation(extent={{-114,-16},{-80,18}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S6
    "ON = true, OFF = false (pump S6)"
    annotation (Placement(transformation(extent={{-120,20},{-80,60}}),
        iconTransformation(extent={{-114,26},{-80,60}})));
  Modelica.Blocks.Interfaces.RealOutput S6_outter(start=0)
    "ON = 1, OFF = 0 (pump S6)"
    annotation (Placement(transformation(extent={{222,-16},{254,16}}),
        iconTransformation(extent={{228,-20},{252,4}})));
  Modelica.Blocks.Interfaces.RealOutput S5_outter(start=0)
    "ON = 1, OFF = 0 (pump S5)"
    annotation (Placement(transformation(extent={{222,-56},{254,-24}}),
        iconTransformation(extent={{228,-52},{252,-28}})));

  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S6(table=
        S6_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,2},{100,22}})));
  Buildings.Controls.Continuous.LimPID pid_S6(
    controllerType=S6_controller,
    k=S6_k,
    Ti=S6_Ti,
    Td=S6_Td,
    wp=S6_wp,
    wd=S6_wd,
    yMax=S6_yMax,
    yMin=S6_yMin,
    reverseAction=true)
    annotation (Placement(transformation(extent={{142,2},{162,22}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S5(table=
        S5_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,-44},{100,-24}})));
  Buildings.Controls.Continuous.LimPID pid_S5(
    controllerType=S5_controller,
    k=S5_k,
    Ti=S5_Ti,
    Td=S5_Td,
    wp=S5_wp,
    wd=S5_wd,
    yMax=S5_yMax,
    yMin=S5_yMin,
    reverseAction=true)
    annotation (Placement(transformation(extent={{140,-44},{160,-24}})));
  Modelica.Blocks.Math.MultiSum delta_storage(k={1,-1}, nu=2) "T1 - T5"
    annotation (Placement(transformation(extent={{126,-6},{134,2}})));
  Modelica.Blocks.Math.MultiSum delta_solar(k={1,-1}, nu=2) "T1 - T3"
    annotation (Placement(transformation(extent={{126,-52},{134,-44}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after collector"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-92,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={-90,96})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-60,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={-54,96})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-12,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={18,96})));

  Modelica.Blocks.Logical.Switch S6_if_max
    annotation (Placement(transformation(extent={{180,-4},{188,4}})));
  Modelica.Blocks.Logical.Switch S5_if_max
    annotation (Placement(transformation(extent={{180,-48},{188,-40}})));
  Modelica.Blocks.Sources.RealExpression S6_static(each y=0.5)
    annotation (Placement(transformation(extent={{156,-16},{170,-2}})));
  Modelica.Blocks.Sources.RealExpression S5_static(each y=0.5)
    annotation (Placement(transformation(extent={{158,-56},{172,-44}})));
  Modelica.Blocks.Interfaces.RealOutput    pumps_state[n + 2]
    "[S6, S5, S4, Sj*n] states" annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=-90,
        origin={20,-106}),  iconTransformation(
        extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={0,-104})));

  SolarSystem.Control.IGC_control.Air_vector_electric.old.Pump_control_deprecated
    pumps_state_algo(
    n=n,
    temporizations=temporizations,
    modulations=modulations)
    annotation (Placement(transformation(extent={{-44,-46},{6,-10}})));
  Modelica.Blocks.Math.Gain S6_speed(k=left_pumps)
    annotation (Placement(transformation(extent={{194,-12},{202,-4}})));
  Modelica.Blocks.Math.Gain S5_speed(k=left_pumps)
    annotation (Placement(transformation(extent={{194,-54},{202,-46}})));
  Modelica.Blocks.Math.Product S6_speed_state
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{210,-4},{218,4}})));
  Modelica.Blocks.Math.BooleanToReal to_real[n + 2] annotation (Placement(
        transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={176,42})));
  Modelica.Blocks.Math.Product S5_speed_state
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{210,-44},{218,-36}})));
  Modelica.Blocks.Math.BooleanToReal Pumps_state_ToReal[n + 2] annotation (
      Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=-90,
        origin={20,-68})));
  SolarSystem.Control.IGC_control.Air_vector_electric.old.Regul_Tamb regul_Tamb(
    n=n,
    each controllerType=fan_controllerType,
    each k=fan_k,
    each Ti=fan_Ti,
    each Td=fan_Td,
    each yMax=fan_yMax,
    each yMin=fan_yMin,
    each wp=fan_wp,
    each wd=fan_wd,
    each reverseAction=fan_reverseAction,
    Ni=fan_Ni)
    annotation (Placement(transformation(extent={{122,52},{158,70}})));
  Modelica.Blocks.Interfaces.RealVectorInput
                                       Tambiant[n] "T. in the room"
    annotation (Placement(transformation(extent={{18,80},{44,106}}),
        iconTransformation(extent={{52,88},{74,110}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n] "T. in the room"
    annotation (Placement(transformation(extent={{-6,80},{20,106}}),
        iconTransformation(extent={{88,88},{110,110}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n] annotation (Placement(
        transformation(extent={{58,80},{84,106}}),   iconTransformation(extent={{156,88},
            {178,110}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction_souff[n] annotation (
      Placement(transformation(extent={{36,80},{62,106}}),
        iconTransformation(extent={{124,88},{146,110}})));
  Modelica.Blocks.Interfaces.RealOutput Sj_outter[n]
    "ON = 1, OFF = 0 (pump Sj)" annotation (Placement(transformation(extent={{226,
            80},{258,112}}), iconTransformation(extent={{228,68},{252,92}})));
  Modelica.Blocks.Interfaces.RealOutput fan_outter[n]
    "ON = 1, OFF = 0 (pump Sj)" annotation (Placement(transformation(extent={{226,
            58},{258,90}}), iconTransformation(extent={{228,38},{252,62}})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside DHW tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-36,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={-18,96})));
  Electric_DHW electric_DHW
    annotation (Placement(transformation(extent={{90,-92},{166,-76}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_DHW "ON = 1, OFF = 0 (pump S5)"
                                annotation (Placement(transformation(extent={{224,
            -96},{258,-62}}), iconTransformation(extent={{228,-88},{254,-62}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_heating[n]
    "ON = 1, OFF = 0 (pump S5)" annotation (Placement(transformation(extent={{224,
            24},{258,58}}), iconTransformation(extent={{228,8},{252,32}})));
  Modelica.Blocks.Math.Gain Sj_speed[n](each k=right_pumps)
    annotation (Placement(transformation(extent={{202,92},{210,100}})));
  Modelica.Blocks.Interfaces.RealVectorInput Texch_out[n] annotation (Placement(
        transformation(extent={{76,80},{102,106}}), iconTransformation(extent={
            {188,88},{210,110}})));
  SolarSystem.Control.IGC_control.Air_vector_electric.old.Regul_Tsouff_noSmooth
    regul_Tsouff(
    electrical_power=electrical_power,
    n=n,
    wait_for=wait_for,
    is_on_max=is_on_max,
    is_on_min=is_on_min,
    Sj_controllerType=Sj_controllerType,
    Sj_k=Sj_k,
    Sj_Ti=Sj_Ti,
    Sj_Td=Sj_Td,
    Sj_yMax=Sj_yMax,
    Sj_yMin=Sj_yMin,
    Sj_wp=Sj_wp,
    Sj_wd=Sj_wd,
    Sj_Ni=Sj_Ni,
    Sj_reverseAction=Sj_reverseAction,
    Sj_Nd=Sj_Nd,
    Elec_controllerType=Elec_controllerType,
    Elec_k=Elec_k,
    Elec_Ti=Elec_Ti,
    Elec_Td=Elec_Td,
    Elec_yMax=Elec_yMax,
    Elec_yMin=Elec_yMin,
    Elec_wp=Elec_wp,
    Elec_wd=Elec_wd,
    Elec_Ni=Elec_Ni,
    Elec_Nd=Elec_Nd,
    Elec_reverseAction=Elec_reverseAction)
    annotation (Placement(transformation(extent={{118,78},{180,92}})));

  parameter Real fan_Ni=0.9
    "Ni*Ti is time constant of anti-windup compensation"
    annotation (Dialog(tab="fan"));
equation
  connect(control_S6.y[1], pid_S6.u_s) annotation (Line(
      points={{101,12},{140,12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(control_S5.y[1], pid_S5.u_s) annotation (Line(
      points={{101,-34},{138,-34}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_solar.y, pid_S5.u_m) annotation (Line(
      points={{134.68,-48},{140,-48},{140,-52},{150,-52},{150,-46}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_storage.y, pid_S6.u_m) annotation (Line(
      points={{134.68,-2},{140,-2},{140,-6},{152,-6},{152,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, delta_storage.u[1]) annotation (Line(
      points={{-92,100},{-92,66},{-38,66},{-38,20},{60,20},{60,-0.6},{126,-0.6}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T1, delta_solar.u[1]) annotation (Line(
      points={{-92,100},{-92,66},{-38,66},{-38,20},{60,20},{60,-46.6},{126,
          -46.6}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T5, delta_storage.u[2]) annotation (Line(
      points={{-12,100},{-12,72},{20,72},{20,52},{72,52},{72,-3.4},{126,-3.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, delta_solar.u[2]) annotation (Line(
      points={{-60,100},{-60,68},{-20,68},{-20,32},{64,32},{64,-49.4},{126,
          -49.4}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(S6_static.y, S6_if_max.u3) annotation (Line(
      points={{170.7,-9},{176.2,-9},{176.2,-3.2},{179.2,-3.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_static.y, S5_if_max.u3) annotation (Line(
      points={{172.7,-50},{175.15,-50},{175.15,-47.2},{179.2,-47.2}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(pid_S5.y, S5_if_max.u1) annotation (Line(
      points={{161,-34},{172,-34},{172,-40.8},{179.2,-40.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_S6.y, S6_if_max.u1) annotation (Line(
      points={{163,12},{172,12},{172,3.2},{179.2,3.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumpControl_S6, pumps_state_algo.pumps_system[1]) annotation (Line(
      points={{-100,40},{-74,40},{-74,-22.15},{-44.4167,-22.15}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumpControl_S5, pumps_state_algo.pumps_system[2]) annotation (Line(
      points={{-100,-2},{-74,-2},{-74,-15.85},{-44.4167,-15.85}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_modulation[1], S6_if_max.u2) annotation (Line(
      points={{4.33333,-39.7},{4,-39.7},{4,-36},{50,-36},{50,-12},{174,-12},{
          174,0},{179.2,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_modulation[2], S5_if_max.u2) annotation (Line(
      points={{4.33333,-34.3},{2,-34.3},{2,-36},{50,-36},{50,-12},{174,-12},{
          174,-44},{179.2,-44}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(S6_if_max.y, S6_speed.u) annotation (Line(
      points={{188.4,0},{190,0},{190,-8},{193.2,-8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_if_max.y, S5_speed.u) annotation (Line(
      points={{188.4,-44},{190,-44},{190,-50},{193.2,-50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumpControl_Sj, pumps_state_algo.pumps_heat) annotation (Line(
      points={{-100,-36},{-56,-36},{-56,-37},{-44.4167,-37}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(to_real[1].y, S6_speed_state.u1) annotation (Line(
      points={{180.4,42},{204,42},{204,2.4},{209.2,2.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S6_speed.y, S6_speed_state.u2) annotation (Line(
      points={{202.4,-8},{206,-8},{206,-2.4},{209.2,-2.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_speed.y, S5_speed_state.u2) annotation (Line(
      points={{202.4,-50},{206,-50},{206,-42.4},{209.2,-42.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(to_real[2].y, S5_speed_state.u1) annotation (Line(
      points={{180.4,42},{204,42},{204,-37.6},{209.2,-37.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S6_speed_state.y, S6_outter) annotation (Line(
      points={{218.4,0},{238,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_speed_state.y, S5_outter) annotation (Line(
      points={{218.4,-40},{238,-40}},
      color={0,0,127},
      smooth=Smooth.None));

      // Connect each heating pumps (first three pump are system wide pumps)

  connect(pumps_state_algo.pumps_state, Pumps_state_ToReal.u) annotation (Line(
      points={{4.33333,-19},{20,-19},{20,-58.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Pumps_state_ToReal.y, pumps_state) annotation (Line(
      points={{20,-76.8},{20,-106}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_state, to_real.u) annotation (Line(
      points={{4.33333,-19},{20,-19},{20,42},{171.2,42}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tambiant, regul_Tamb.Tambiant) annotation (Line(
      points={{31,93},{31,74},{108,74},{108,65.275},{121.64,65.275}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction, regul_Tamb.Tinstruction) annotation (Line(
      points={{7,93},{7,70},{100,70},{100,56.275},{121.64,56.275}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(regul_Tamb.fan_speed_out, fan_outter) annotation (Line(
      points={{159.08,60.775},{212,60.775},{212,74},{242,74}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T4, electric_DHW.T4) annotation (Line(
      points={{-36,100},{-36,72},{-12,72},{-12,36},{54,36},{54,-84},{90,-84}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(electric_DHW.Elec_DHW, Elec_DHW) annotation (Line(
      points={{165.367,-79.8},{226,-79.8},{226,-80},{234,-80},{234,-79},{241,
          -79}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(Sj_speed.y, Sj_outter) annotation (Line(
      points={{210.4,96},{242,96}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(regul_Tsouff.pump_speed_out, Sj_speed.u) annotation (Line(
      points={{178.967,88.9},{189.483,88.9},{189.483,96},{201.2,96}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(regul_Tsouff.Elec_Heating, Elec_heating) annotation (Line(
      points={{179.173,85},{179.173,86},{190,86},{190,80},{220,80},{220,42},{
          226,42},{226,41},{241,41}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_state[3:2+n], regul_Tsouff.pump_state) annotation (
      Line(
      points={{4.33333,-19},{28,-19},{28,80},{108,80},{108,83.2},{118,83.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tsouff, regul_Tsouff.Tsouff) annotation (Line(
      points={{71,93},{71,79.6},{118,79.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texch_out, regul_Tsouff.Texch_out) annotation (Line(
      points={{89,93},{104.5,93},{104.5,90.2},{118,90.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction_souff, regul_Tsouff.Tinstruction_souff) annotation (Line(
      points={{49,93},{49,88},{50,88},{50,82},{102,82},{102,86.6},{118,86.6}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-100},{240,100}},
          preserveAspectRatio=false), graphics, defaultComponentName = "pump_control"), Icon(coordinateSystem(extent={{-100,
            -100},{240,100}}, preserveAspectRatio=false), graphics={
                  Rectangle(
          extent={{-100,100},{240,-100}},
          lineColor={90,150,90},
          fillPattern=FillPattern.CrossDiag,
          fillColor={0,127,127}),
        Text(
          extent={{-100,62},{238,16}},
          lineColor={0,0,0},
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="PID"),
        Text(
          extent={{-96,-20},{240,-52}},
          lineColor={0,0,0},
          textString="Flow switch",
          fontName="Consolas",
          textStyle={TextStyle.Bold})}),
    Documentation(info="<html>
<p>Control all pumps according to pump state from algo.</p>
<p><br><br>Be careful only work with two heating pump fo now.</p>
</html>"),
    experiment(
      StopTime=2e+006,
      Interval=240,
      __Dymola_Algorithm="esdirk23a"),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end pump_pids_noSmooth;
