within SolarSystem.Control.IGC_control_instruction.Air_vector_electric.old;
model Heating_mod_souff

  import SI = Modelica.SIunits;
parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states : true = ON, false = OFF)";

  Modelica.Blocks.Interfaces.RealVectorInput TafterHex[n]
    "Temperature just after the exchanger" annotation (Placement(transformation(
          extent={{-120,110},{-80,150}}), iconTransformation(extent={{-112,104},
            {-80,136}})));
  Modelica.Blocks.Interfaces.BooleanInput V3V_solar
    "Open to solar panels = true, Open to storage tank = false"
    annotation (Placement(transformation(extent={{-122,-28},{-78,16}}),
        iconTransformation(extent={{-114,-78},{-80,-44}})));
  Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-120,-300},{-80,-260}}),
        iconTransformation(extent={{-112,-266},{-78,-232}})));
  Modelica.Blocks.Interfaces.RealVectorInput
                                       TsolarInstruction[n] annotation (
      Placement(transformation(extent={{-118,-226},{-78,-186}}),
                                                             iconTransformation(
          extent={{-118,-226},{-86,-194}})));

  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-120,-72},{-80,-32}}),
        iconTransformation(extent={{-116,-180},{-84,-148}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-120,-120},{-80,-80}}),
        iconTransformation(extent={{-116,-130},{-84,-98}})));

  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S5(start=true)
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{314,-260},{354,-220}}),
        iconTransformation(extent={{314,-260},{354,-220}})));

  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_Sj[n](
                                                            each start=true)
    "ON = true, OFF = false (pump Sj)"
    annotation (Placement(transformation(extent={{310,-80},{350,-40}}),
        iconTransformation(extent={{312,-92},{352,-52}})));

public
  Utilities.Timers.Stay_on stay_on_Sj[n](each pause=60, out_value(each start=false))
    annotation (Placement(transformation(extent={{280,-68},{296,-52}})));
protected
  Modelica.Blocks.Math.Max max_28_T7
    "Return the minimal value between T3 and T4"
    annotation (Placement(transformation(extent={{-60,-142},{-42,-124}})));
  Modelica.Blocks.Sources.RealExpression cst3(each y=40 + 273.15)
    annotation (Placement(transformation(extent={{-100,-136},{-76,-118}})));
protected
  Modelica.Blocks.Math.Add sum_T1(each k2=1, each k1=1)
    "TsolarInstruction + 0.2"
    annotation (Placement(transformation(extent={{-20,-140},{0,-120}})));
protected
  Modelica.Blocks.Sources.RealExpression cst4(each y=5)
    annotation (Placement(transformation(extent={{-100,-172},{-78,-156}})));
public
  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n]
    "Blowing temperature instruction"
                               annotation (Placement(transformation(extent={{-118,30},
            {-78,70}}),         iconTransformation(extent={{-116,14},{-90,42}})));
  Modelica.Blocks.Logical.Or final_state[n]
    "Test if Tamb is  between lower and upper instruction"
    annotation (Placement(transformation(extent={{240,-74},{268,-46}})));
  Modelica.Blocks.Logical.Hysteresis            T5_Test(uLow=273.15 + 39, uHigh=
       273.15 + 41)
    annotation (Placement(transformation(extent={{-20,-62},{0,-42}})));
  Modelica.Blocks.MathBoolean.And T5_and_Delta_test_1(nu=2)
    "Test if Tamb is  between lower and upper instruction"
    annotation (Placement(transformation(extent={{100,-20},{120,0}})));
protected
  Buildings.Utilities.Math.BooleanReplicator on1_replicator(nout=n)
    "Replicator for V3V and Temp test for Tinstruction on mode"
    annotation (Placement(transformation(extent={{144,-20},{164,0}})));
public
  Modelica.Blocks.Logical.And On_IndirectSun[n]
    "Activation of pump with indirect sun"
    annotation (Placement(transformation(extent={{180,20},{204,44}})));
  Modelica.Blocks.MathBoolean.And T1_and_Delta_test_2(nu=2)
    "Test if Tamb is  between lower and upper instruction"
    annotation (Placement(transformation(extent={{100,-110},{120,-90}})));
protected
  Buildings.Utilities.Math.BooleanReplicator on2_replicator(nout=n)
    "Replicator for V3V and Temp test for Tinstruction on mode"
    annotation (Placement(transformation(extent={{144,-110},{164,-90}})));
public
  Modelica.Blocks.Logical.Greater Test_T1
    "Test if Tamb is lower than instruction"
    annotation (Placement(transformation(extent={{30,-110},{50,-90}})));
  Modelica.Blocks.Logical.Not V3V_inverse
    annotation (Placement(transformation(extent={{28,-16},{50,6}})));
  Modelica.Blocks.Logical.And On_DirectSun[n]
    "Activation of pump with direct sun"
    annotation (Placement(transformation(extent={{180,-140},{206,-114}})));
  Utilities.Timers.Stay_on stay_on_S5(each pause=60, out_value(start=false))
    annotation (Placement(transformation(extent={{284,-248},{300,-232}})));
protected
  Utilities.Logical.True_seeker true_seeker(n=n)
    annotation (Placement(transformation(extent={{228,-256},{270,-224}})));
public
  Modelica.Blocks.Logical.OnOffController need_heat1[n](each bandwidth=1)
    "True if Tsouff below Tinstruction_souff"
    annotation (Placement(transformation(extent={{42,80},{62,100}})));
  Modelica.Blocks.Logical.OnOffController need_heat2[n](each bandwidth=1)
    "True if Tsouff below Tinstruction_souff"
    annotation (Placement(transformation(extent={{30,-206},{50,-186}})));
equation

  connect(stay_on_Sj.out_value, pumpControl_Sj) annotation (Line(
      points={{297.6,-60},{330,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T7, max_28_T7.u2) annotation (Line(
      points={{-100,-280},{-66,-280},{-66,-138.4},{-61.8,-138.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst3.y, max_28_T7.u1) annotation (Line(
      points={{-74.8,-127},{-74,-127},{-74,-127.6},{-61.8,-127.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(max_28_T7.y, sum_T1.u1) annotation (Line(
      points={{-41.1,-133},{-28,-133},{-28,-124},{-22,-124}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst4.y, sum_T1.u2) annotation (Line(
      points={{-76.9,-164},{-30,-164},{-30,-136},{-22,-136}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(final_state.y, stay_on_Sj.in_value) annotation (Line(
      points={{269.4,-60},{280,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T5, T5_Test.u) annotation (Line(
      points={{-100,-52},{-22,-52}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5_and_Delta_test_1.y, on1_replicator.u) annotation (Line(
      points={{121.5,-10},{142,-10}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on1_replicator.y, On_IndirectSun.u2) annotation (Line(
      points={{165,-10},{172,-10},{172,22.4},{177.6,22.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(On_IndirectSun.y, final_state.u1) annotation (Line(
      points={{205.2,32},{220,32},{220,-60},{237.2,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T1_and_Delta_test_2.y, on2_replicator.u) annotation (Line(
      points={{121.5,-100},{142,-100}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T1, Test_T1.u1) annotation (Line(
      points={{-100,-100},{28,-100}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sum_T1.y, Test_T1.u2) annotation (Line(
      points={{1,-130},{8,-130},{8,-108},{28,-108}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(V3V_solar, V3V_inverse.u) annotation (Line(
      points={{-100,-6},{10,-6},{10,-6},{10,-5},{18,-5},{25.8,-5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_solar, T1_and_Delta_test_2.u[1]) annotation (Line(
      points={{-100,-6},{10,-6},{10,-80},{60,-80},{60,-96.5},{100,-96.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Test_T1.y, T1_and_Delta_test_2.u[2]) annotation (Line(
      points={{51,-100},{76,-100},{76,-103.5},{100,-103.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_inverse.y, T5_and_Delta_test_1.u[1]) annotation (Line(
      points={{51.1,-5},{78,-5},{78,-6.5},{100,-6.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T5_Test.y, T5_and_Delta_test_1.u[2]) annotation (Line(
      points={{1,-52},{60,-52},{60,-13.5},{100,-13.5}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(on2_replicator.y, On_DirectSun.u1) annotation (Line(
      points={{165,-100},{170,-100},{170,-127},{177.4,-127}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(On_DirectSun.y, final_state.u2) annotation (Line(
      points={{207.3,-127},{220,-127},{220,-71.2},{237.2,-71.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(On_DirectSun.y, true_seeker.u) annotation (Line(
      points={{207.3,-127},{220,-127},{220,-240},{228,-240}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(true_seeker.y, stay_on_S5.in_value) annotation (Line(
      points={{270,-240},{284,-240}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(stay_on_S5.out_value, pumpControl_S5) annotation (Line(
      points={{301.6,-240},{334,-240}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tinstruction, need_heat1.reference) annotation (Line(
      points={{-98,50},{-72,50},{-72,96},{40,96}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TafterHex, need_heat1.u) annotation (Line(
      points={{-100,130},{20,130},{20,84},{40,84}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(need_heat1.y, On_IndirectSun.u1) annotation (Line(
      points={{63,90},{120,90},{120,32},{177.6,32}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(need_heat2.y, On_DirectSun.u2) annotation (Line(
      points={{51,-196},{114,-196},{114,-137.4},{177.4,-137.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(TsolarInstruction, need_heat2.reference) annotation (Line(
      points={{-98,-206},{-36,-206},{-36,-190},{28,-190}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TafterHex, need_heat2.u) annotation (Line(
      points={{-100,130},{20,130},{20,-202},{28,-202}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-300},{320,140}},
          preserveAspectRatio=false),  graphics), Icon(coordinateSystem(extent={{-100,
            -300},{320,140}}, preserveAspectRatio=false),  graphics={
        Text(
          extent={{-58,24},{284,-236}},
          lineColor={0,128,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          textString="Nb Room
%n"),   Polygon(
          points={{-100,140},{320,140},{320,-300},{-100,-300},{-100,140},{-100,140},
              {-100,140}},
          lineColor={255,0,255},
          smooth=Smooth.None)}));
end Heating_mod_souff;
