within SolarSystem.Control.IGC_control_instruction.Air_vector_electric.old;
model Algo_one_elec_souff
  "All INPUT temperature must be in Kelvin(K) not �C or �F, otherwise might lead to strange behaviour. Use when only one room"
  import SolarSystem;

  parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

  // Weather file info
  parameter String WeatherFile="C:/Users/bois/Documents/Dymola/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos"
    "Name of weather data file" annotation (Dialog(tab="Weather"));
  parameter Modelica.SIunits.ThermodynamicTemperature Tmoy_first=279.94
    "Average temperature from first day" annotation (Dialog(tab="Weather"));
  parameter Modelica.SIunits.ThermodynamicTemperature Tmin_first=275.15
    "Minimal temperature from first day" annotation (Dialog(tab="Weather"));
  parameter Real offset=3600*24 "Temperature offset (time of an output step)"
    annotation (Dialog(tab="Weather"));
  parameter Modelica.SIunits.ThermodynamicTemperature pad=276.15
    "Used to compute TsolarInstruction";

  SolisArt_mod.solarTank_mod solarTank_mod1(pumpControl_S5)
    annotation (Placement(transformation(extent={{120,104},{156,140}})));
  SolisArt_mod.storageTank_mod storageTank_mod1
    annotation (Placement(transformation(extent={{120,56},{166,92}})));
  SolisArt_mod.SolarValve_mod solarValve_mod
    annotation (Placement(transformation(extent={{-58,-58},{-26,-34}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-134,118},{-106,146}}),
        iconTransformation(extent={{-130,116},{-108,138}})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-134,94},{-106,122}}),
        iconTransformation(extent={{-130,86},{-108,108}})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
    annotation (Placement(transformation(extent={{-134,70},{-106,98}}),
        iconTransformation(extent={{-130,56},{-108,78}})));
  Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-134,6},{-106,34}}),
        iconTransformation(extent={{-130,-2},{-108,20}})));
  Modelica.Blocks.Interfaces.RealInput T8
    "T. at heating start point (just before extra exchanger input)"
    annotation (Placement(transformation(extent={{-134,-28},{-106,0}}),
        iconTransformation(extent={{-130,-32},{-108,-10}})));
  Modelica.Blocks.Interfaces.RealVectorInput T_afterHex[n]
    "Temperature after the exchanger" annotation (Placement(transformation(
          extent={{-134,-110},{-106,-82}}), iconTransformation(extent={{-128,
            -104},{-106,-82}})));
  Modelica.Blocks.Interfaces.RealInput Text "T. from outdoor"
    annotation (Placement(transformation(extent={{-134,-144},{-106,-116}}),
        iconTransformation(extent={{-128,-146},{-104,-122}})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-134,44},{-106,72}}),
        iconTransformation(extent={{-130,28},{-108,50}})));

  Modelica.Blocks.Interfaces.BooleanOutput
                                        S5_outter(start=false)
    "ON = 1, OFF = 0 (pump S5)"
    annotation (Placement(transformation(extent={{372,124},{404,156}}),
        iconTransformation(extent={{370,36},{400,66}})));
  Modelica.Blocks.Interfaces.BooleanOutput
                                        Sj_outter[n](each start=true)
    "ON = 1, OFF = 0 (pump Sj)"
    annotation (Placement(transformation(extent={{372,4},{404,36}}),
        iconTransformation(extent={{370,-70},{400,-40}})));

  Modelica.Blocks.Interfaces.RealOutput    V3V_solar_outter(start=1)
    "Open to solar panels = 1, Open to storage tank = 0"
    annotation (Placement(transformation(extent={{372,-64},{404,-32}}),
        iconTransformation(extent={{370,86},{400,116}})));

  SolarSystem.Control.IGC_control.Air_vector_electric.old.Heating_mod_souff heating_mod(n=n)
    annotation (Placement(transformation(extent={{138,-40},{188,6}})));
  Modelica.Blocks.Logical.Or or_S5
    annotation (Placement(transformation(extent={{338,130},{358,150}})));

  Modelica.Blocks.Math.BooleanToReal real_solar
                                           annotation (Placement(transformation(
        extent={{-8,8},{8,-8}},
        rotation=0,
        origin={348,-48})));

  Instruction_variable.Variables_state
                               variables_state(pad=
        pad, n=n)
             annotation (Placement(transformation(extent={{44,-16},{86,6}})));
  Modelica.Blocks.Interfaces.BooleanOutput S6_outter(start=false)
    "ON = 1, OFF = 0 (pump S6)"
    annotation (Placement(transformation(extent={{370,84},{402,116}}),
        iconTransformation(extent={{370,-16},{400,14}})));
  Utilities.Other.MiniAverage extract_TdryBulb(
    filNam=WeatherFile,
    Tmoy_first=Tmoy_first,
    Tmin_first=Tmin_first,
    offset=offset) annotation (Placement(transformation(extent={{-24,-90},{18,
            -62}})));
  Utilities.Timers.Stay_on stay_on(out_value(start=false), pause=60)
    annotation (Placement(transformation(extent={{300,112},{316,128}})));
  Modelica.Blocks.Interfaces.RealVectorInput T_souff_instruction[n]
    "Blowing temperature instruction" annotation (Placement(transformation(
          extent={{-134,-82},{-106,-54}}), iconTransformation(extent={{-132,-72},
            {-110,-50}})));
  Modelica.Blocks.Interfaces.RealOutput T_souff_instruction_out[n]
    "Blowing temperature instruction" annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={218,166}), iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=90,
        origin={271,157})));
  Modelica.Blocks.Interfaces.RealOutput T_afterhex_out[n]
    "T. after the exchanger" annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={260,166}), iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=90,
        origin={325,157})));
equation
  connect(T1, solarTank_mod1.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,132.5},{120,132.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, storageTank_mod1.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,82},{120,82}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, solarValve_mod.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,-37},{-58,-37}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, solarTank_mod1.T3) annotation (Line(
      points={{-120,108},{-76,108},{-76,126.5},{120,126.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, storageTank_mod1.T3) annotation (Line(
      points={{-120,108},{-76,108},{-76,72},{120,72}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, solarValve_mod.T3) annotation (Line(
      points={{-120,108},{-76,108},{-76,-43},{-58,-43}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, solarValve_mod.T4) annotation (Line(
      points={{-120,84},{-80,84},{-80,-49},{-58,-49}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, storageTank_mod1.T5) annotation (Line(
      points={{-120,58},{-86,58},{-86,62},{120,62}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, solarValve_mod.T5) annotation (Line(
      points={{-120,58},{-86,58},{-86,-55},{-58,-55}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(solarValve_mod.V3V_solar, solarTank_mod1.V3V_solar)
    annotation (Line(
      points={{-26,-46},{24,-46},{24,111.5},{120,111.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(solarValve_mod.V3V_solar, heating_mod.V3V_solar) annotation (
      Line(
      points={{-26,-46},{106,-46},{106,-15.0136},{138.357,-15.0136}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T5, heating_mod.T5) annotation (Line(
      points={{-120,58},{-86,58},{-86,-25.7818},{138,-25.7818}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarTank_mod1.pumpControl_S5, or_S5.u1) annotation (Line(
      points={{156,119},{180,119},{180,80},{280,80},{280,140},{336,140}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(real_solar.y, V3V_solar_outter) annotation (Line(
      points={{356.8,-48},{388,-48}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(or_S5.y, S5_outter) annotation (Line(
      points={{359,140},{388,140}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(heating_mod.pumpControl_Sj, Sj_outter) annotation (Line(
      points={{189.429,-16.1636},{200,-16.1636},{200,10},{320,10},{320,20},{
          388,20}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(solarValve_mod.V3V_solar, real_solar.u) annotation (Line(
      points={{-26,-46},{156,-46},{156,-48},{338.4,-48}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(variables_state.TsolarInstruction, heating_mod.TsolarInstruction)
    annotation (Line(
      points={{86.3,-9.71429},{99.15,-9.71429},{99.15,-30.5909},{137.762,
          -30.5909}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(variables_state.T1_out, heating_mod.T1) annotation (Line(
      points={{62,-16.6286},{62,-20.5545},{138,-20.5545}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(variables_state.T7_out, heating_mod.T7) annotation (Line(
      points={{71,-16.6286},{71,-34.6682},{138.595,-34.6682}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, variables_state.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,4.11429},{44,4.11429}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7, variables_state.T7) annotation (Line(
      points={{-120,20},{-90,20},{-90,-2.17143},{44,-2.17143}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text, variables_state.Text) annotation (Line(
      points={{-120,-130},{34,-130},{34,-8.14286},{44,-8.14286}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(storageTank_mod1.pumpControl_S6, S6_outter) annotation (Line(
      points={{166,64},{320,64},{320,100},{386,100}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(extract_TdryBulb.TminOfDay, variables_state.Text_mini) annotation (
      Line(
      points={{19.26,-70.4},{36,-70.4},{36,-14.1143},{44,-14.1143}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(heating_mod.pumpControl_S5, stay_on.in_value) annotation (
      Line(
      points={{189.667,-33.7273},{192,-33.7273},{192,60},{290,60},{290,120},{
          300,120}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(stay_on.out_value, or_S5.u2) annotation (Line(
      points={{317.6,120},{320,120},{320,132},{336,132}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T_souff_instruction, variables_state.Tinstruction) annotation (Line(
      points={{-120,-68},{-102,-68},{-102,14},{52.1,14},{52.1,6.62857}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T_souff_instruction, heating_mod.Tinstruction) annotation (Line(
      points={{-120,-68},{-102,-68},{-102,14},{116,14},{116,-5.70909},{
          137.643,-5.70909}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T_souff_instruction, T_souff_instruction_out) annotation (Line(
      points={{-120,-68},{-102,-68},{-102,34},{218,34},{218,166}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T_afterHex, heating_mod.TafterHex) annotation (Line(
      points={{-120,-96},{126,-96},{126,3.90909},{138.476,3.90909}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T_afterHex, T_afterhex_out) annotation (Line(
      points={{-120,-96},{260,-96},{260,166}},
      color={0,0,127},
      smooth=Smooth.None));
                                                                                                      annotation(Dialog(tab="Execution cycle"),
              Diagram(coordinateSystem(extent={{-120,-180},{380,160}},
          preserveAspectRatio=false),   graphics, defaultComponentName = "algo_states"), Icon(coordinateSystem(
          extent={{-120,-180},{380,160}}, preserveAspectRatio=false),
        graphics={Rectangle(
          extent={{-120,160},{382,-180}},
          lineColor={90,150,90},
          fillPattern=FillPattern.CrossDiag,
          fillColor={0,127,127}),
        Text(
          extent={{-124,108},{380,28}},
          lineColor={0,0,0},
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="Algo"),
        Text(
          extent={{-120,-24},{388,-102}},
          lineColor={0,0,0},
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="States")}),
    Documentation(info="<html>
<p>Groups all algorithms into one block.</p>
<p>This modul must be used with pump_algo modul to have real pump state inputs.</p>
</html>"));
end Algo_one_elec_souff;
