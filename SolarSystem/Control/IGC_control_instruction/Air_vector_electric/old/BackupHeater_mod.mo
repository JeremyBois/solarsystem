within SolarSystem.Control.IGC_control_instruction.Air_vector_electric.old;
model BackupHeater_mod "Control when we need to turn on the backup heater"

  Modelica.Blocks.Interfaces.BooleanOutput BackupHeater(start=false)
    "true = needed, false otherwise"
    annotation (Placement(transformation(extent={{140,-20},{180,20}}),
        iconTransformation(extent={{140,-20},{180,20}})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
    annotation (Placement(transformation(extent={{-120,-60},{-80,-20}}),
        iconTransformation(extent={{-120,-60},{-80,-20}})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-120,10},{-80,50}}),
        iconTransformation(extent={{-120,0},{-80,40}})));
  Modelica.Blocks.Logical.Or  sum_conditions
    annotation (Placement(transformation(extent={{60,-10},{80,10}})));
  Modelica.Blocks.Logical.OnOffController T3_test(bandwidth=1)
    annotation (Placement(transformation(extent={{0,20},{20,40}})));
  Modelica.Blocks.Logical.OnOffController T4_test(bandwidth=2.5)
    annotation (Placement(transformation(extent={{0,-60},{20,-40}})));
protected
  Modelica.Blocks.Sources.RealExpression T3_set(each y=40)
    annotation (Placement(transformation(extent={{-40,34},{-22,50}})));
protected
  Modelica.Blocks.Sources.RealExpression T4_set(each y=42.5)
    annotation (Placement(transformation(extent={{-44,-38},{-26,-22}})));
equation
  connect(sum_conditions.y, BackupHeater) annotation (Line(
      points={{81,0},{160,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T3_set.y, T3_test.reference) annotation (Line(
      points={{-21.1,42},{-12,42},{-12,36},{-2,36}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, T3_test.u) annotation (Line(
      points={{-100,30},{-40,30},{-40,24},{-2,24}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4_set.y, T4_test.reference) annotation (Line(
      points={{-25.1,-30},{-18,-30},{-18,-44},{-2,-44}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, T4_test.u) annotation (Line(
      points={{-100,-40},{-40,-40},{-40,-56},{-2,-56}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3_test.y, sum_conditions.u1) annotation (Line(
      points={{21,30},{40,30},{40,0},{58,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T4_test.y, sum_conditions.u2) annotation (Line(
      points={{21,-50},{40,-50},{40,-8},{58,-8}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-80},{160,60}},
          preserveAspectRatio=false),graphics), Icon(coordinateSystem(
          extent={{-100,-80},{160,60}},   preserveAspectRatio=false),
                                           graphics={
        Polygon(
          points={{-100,60},{160,60},{160,-80},{-100,-80},{-100,60},{-100,60},{
              -100,60}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-88,52},{124,-66}},
          lineColor={0,128,0},
          textString="Electrical heat tank control")}),
    Documentation(info="<html>
<p>Not currently used. Only PID to keep T4 at 40&deg;C</p>
</html>"));
end BackupHeater_mod;
