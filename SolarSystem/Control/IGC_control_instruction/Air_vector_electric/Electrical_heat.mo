within SolarSystem.Control.IGC_control_instruction.Air_vector_electric;
model Electrical_heat "Control electrical power"
parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";
parameter Modelica.SIunits.Power electrical_power[n]=fill(5000, n)
    "Maximum value of eletrical power"
    annotation (Dialog(tab="Electrical", group="Power"));
    // PID
  parameter Modelica.Blocks.Types.SimpleController Elec_controllerType[n]=fill(Modelica.Blocks.Types.SimpleController.PID, n)
    "Type of controller" annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_k[n]=fill(1, n) "Gain of controller"  annotation (Dialog(tab="Electrical", group="PID"));
  parameter Modelica.SIunits.Time Elec_Ti[n]=fill(0.5, n)
    "Time constant of Integrator block" annotation (Dialog(tab="Electrical", group="PID"));
  parameter Modelica.SIunits.Time Elec_Td[n]=fill(0.1, n)
    "Time constant of Derivative block" annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_yMax[n]=fill(1, n) "Upper limit of output"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_yMin[n]=fill(0, n) "Lower limit of output"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_wp[n]=fill(1, n)
    "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_wd[n]=fill(0, n)
    "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_Ni[n]=fill(0.9, n)
    "Ni*Ti is time constant of anti-windup compensation"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Real Elec_Nd[n]=fill(10, n)
    "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Electrical", group="PID"));
  parameter Boolean Elec_reverseAction[n]=fill(false, n)
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Electrical", group="PID"));
public
  Modelica.Blocks.Math.Gain gaiHea[n](k=electrical_power) "Gain for heating"
    annotation (Placement(transformation(extent={{-52,56},{-44,64}})));
protected
  Modelica.Blocks.Continuous.Integrator Energy_integrator[n](
    each k=1,
    each initType=Modelica.Blocks.Types.Init.InitialState,
    each y_start=0,
    each u(unit="W"),
    each y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{-28,12},{-12,28}})));
public
  Modelica.Blocks.Math.Mean Power[n](each f=1/3600)
    "Hourly averaged heating power"
    annotation (Placement(transformation(extent={{-28,36},{-12,52}})));
  Modelica.Blocks.Logical.Switch switch_elec[n]
    annotation (Placement(transformation(extent={{-72,36},{-64,44}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_Heating[n]
    "Power of electric power " annotation (Placement(transformation(
        extent={{-20,20},{20,-20}},
        rotation=0,
        origin={20,60}),   iconTransformation(
        extent={{-8,-8},{8,8}},
        rotation=0,
        origin={22,60})));
  Modelica.Blocks.Interfaces.RealOutput Energy[n] "Power of electric power "
    annotation (Placement(transformation(
        extent={{-20,20},{20,-20}},
        rotation=0,
        origin={20,20}),   iconTransformation(
        extent={{-8,-8},{8,8}},
        rotation=0,
        origin={22,20})));
public
  Buildings.Controls.Continuous.LimPID pid_Elec[n](
    controllerType=Elec_controllerType,
    k=Elec_k,
    Ti=Elec_Ti,
    Td=Elec_Td,
    yMax=Elec_yMax,
    yMin=Elec_yMin,
    wp=Elec_wp,
    wd=Elec_wd,
    Ni=Elec_Ni,
    Nd=Elec_Nd,
    reverseAction=Elec_reverseAction) "Controller for heating"
    annotation (Placement(transformation(extent={{-92,54},{-80,66}})));
protected
  Modelica.Blocks.Sources.RealExpression zeros[n](each y=0)
    annotation (Placement(transformation(extent={{-94,16},{-80,28}})));
public
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_electricalPower[n]
    "Does system need electrical power supply to cover losses ?" annotation (
      Placement(transformation(extent={{-128,32},{-112,48}}),
        iconTransformation(extent={{-126,34},{-112,48}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tsouff_instruction[n] annotation (
      Placement(transformation(extent={{-128,60},{-112,76}}),
        iconTransformation(extent={{-126,62},{-112,76}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n] annotation (Placement(
        transformation(extent={{-128,2},{-112,18}}), iconTransformation(extent={{-126,4},
            {-112,18}})));
  Buildings.HeatTransfer.Sources.PrescribedTemperature
    Tsouff_instruction_sensor[n]
    "Recup blowing temperature instruction and transform it into temperature. Allows Celsius easy conversion."
    annotation (Placement(transformation(extent={{-100,70},{-92,78}})));
equation
  connect(gaiHea.y,Power. u) annotation (Line(
      points={{-43.6,60},{-40,60},{-40,44},{-29.6,44}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiHea.y,Energy_integrator. u) annotation (Line(
      points={{-43.6,60},{-40,60},{-40,20},{-29.6,20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiHea.y,Elec_Heating)  annotation (Line(
      points={{-43.6,60},{20,60}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Energy_integrator.y,Energy)  annotation (Line(
      points={{-11.2,20},{20,20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(switch_elec.y,gaiHea. u) annotation (Line(
      points={{-63.6,40},{-60,40},{-60,60},{-52.8,60}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_Elec.y, switch_elec.u1) annotation (Line(
      points={{-79.4,60},{-76,60},{-76,43.2},{-72.8,43.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(zeros.y, switch_elec.u3) annotation (Line(
      points={{-79.3,22},{-76,22},{-76,36.8},{-72.8,36.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Need_electricalPower, switch_elec.u2) annotation (Line(
      points={{-120,40},{-72.8,40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tsouff, pid_Elec.u_m) annotation (Line(
      points={{-120,10},{-100,10},{-100,46},{-86,46},{-86,52.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsouff_instruction, pid_Elec.u_s) annotation (Line(
      points={{-120,68},{-100,68},{-100,60},{-93.2,60}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsouff_instruction, Tsouff_instruction_sensor.T) annotation (Line(
      points={{-120,68},{-112,68},{-112,74},{-100.8,74}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,0},
            {20,80}}),    graphics), Icon(coordinateSystem(extent={{-120,0},{20,
            80}}, preserveAspectRatio=false), graphics={
        Polygon(
          points={{-120,80},{20,80},{20,0},{-120,0},{-120,80},{-120,80},{-120,80}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-98,76},{0,6}},
          lineColor={0,128,0},
          textString="Need Electric power ?")}));
end Electrical_heat;
