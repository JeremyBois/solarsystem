within SolarSystem.Control.IGC_control_instruction.Air_vector_electric;
model Heating_mod_with_overheating
  "Allow activation of overheating for direct solar heating."

  import SI = Modelica.SIunits;
  parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

  Modelica.Blocks.Interfaces.BooleanVectorInput
                                          V3V_solar
    "Open to solar panels = true, Open to storage tank = false"
    annotation (Placement(transformation(extent={{-122,-28},{-78,16}}),
        iconTransformation(extent={{-114,-46},{-80,-12}})));
  Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-122,-206},{-82,-166}}),
        iconTransformation(extent={{-116,-200},{-82,-166}})));

  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-120,-72},{-80,-32}}),
        iconTransformation(extent={{-116,-148},{-84,-116}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-120,-120},{-80,-80}}),
        iconTransformation(extent={{-116,-98},{-84,-66}})));

  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S5(start=true)
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{300,-180},{340,-140}}),
        iconTransformation(extent={{300,-180},{340,-140}})));

  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_Sj[n](
                                                            each start=true)
    "ON = true, OFF = false (pump Sj)"
    annotation (Placement(transformation(extent={{310,-80},{350,-40}}),
        iconTransformation(extent={{300,-20},{340,20}})));

public
  Utilities.Timers.Stay_on stay_on_Sj[n](each pause=60, out_value(each start=false))
    annotation (Placement(transformation(extent={{280,-68},{296,-52}})));
protected
  Modelica.Blocks.Math.Max max_40_T7
    "Return the maximal value between 40 and T7"
    annotation (Placement(transformation(extent={{-60,-142},{-42,-124}})));
  Modelica.Blocks.Sources.RealExpression cst3(each y=40 + 273.15)
    annotation (Placement(transformation(extent={{-100,-136},{-76,-118}})));
protected
  Modelica.Blocks.Math.Add add_5(each k2=1, each k1=1)
    annotation (Placement(transformation(extent={{-18,-140},{2,-120}})));
protected
  Modelica.Blocks.Sources.RealExpression cst4(each y=5)
    annotation (Placement(transformation(extent={{-100,-172},{-78,-156}})));
public
  Modelica.Blocks.Logical.Or final_state[n]
    "Test if Tamb is  between lower and upper instruction"
    annotation (Placement(transformation(extent={{240,-74},{268,-46}})));
  Modelica.Blocks.Logical.Hysteresis            T5_Test(uLow=273.15 + 39, uHigh=
       273.15 + 41)
    annotation (Placement(transformation(extent={{-20,-62},{0,-42}})));
protected
  Modelica.Blocks.MathBoolean.And T5_and_Delta_test_1(nu=2)
    "Test if Tamb is  between lower and upper instruction"
    annotation (Placement(transformation(extent={{90,-20},{110,0}})));
protected
  Buildings.Utilities.Math.BooleanReplicator on1_replicator(nout=n)
    "Replicator for V3V and Temp test for Tinstruction on mode"
    annotation (Placement(transformation(extent={{130,-20},{150,0}})));
public
  Modelica.Blocks.Logical.And On_IndirectSun[n]
    "Activation of pump with indirect sun"
    annotation (Placement(transformation(extent={{180,20},{204,44}})));
protected
  Modelica.Blocks.MathBoolean.And T1_and_Delta_test_2(nu=2)
    "Test if Tamb is  between lower and upper instruction"
    annotation (Placement(transformation(extent={{90,-110},{110,-90}})));
protected
  Buildings.Utilities.Math.BooleanReplicator on2_replicator(nout=n)
    "Replicator for V3V and Temp test for Tinstruction on mode"
    annotation (Placement(transformation(extent={{130,-110},{150,-90}})));
protected
  Modelica.Blocks.Logical.Greater Test_T1
    "Test if Tamb is lower than instruction"
    annotation (Placement(transformation(extent={{30,-110},{50,-90}})));
  Modelica.Blocks.Logical.Not V3V_inverse
    annotation (Placement(transformation(extent={{28,-16},{50,6}})));
public
  Modelica.Blocks.Logical.And On_DirectSun[n]
    "Activation of pump with direct sun"
    annotation (Placement(transformation(extent={{180,-140},{206,-114}})));
  Utilities.Timers.Stay_on stay_on_S5(each pause=60, out_value(start=false))
    annotation (Placement(transformation(extent={{284,-208},{300,-192}})));
protected
  Utilities.Logical.True_seeker true_seeker(n=n)
    annotation (Placement(transformation(extent={{228,-216},{270,-184}})));
public
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_heat[n] annotation (
      Placement(transformation(extent={{-122,18},{-78,62}}), iconTransformation(
          extent={{-114,14},{-80,48}})));

public
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_overheat[n] annotation (
      Placement(transformation(extent={{-122,58},{-78,102}}),
        iconTransformation(extent={{-114,62},{-80,96}})));
public
  Modelica.Blocks.Logical.Or  overheat_or_heat[n]
    "Test if overheating or heating needed."
    annotation (Placement(transformation(extent={{26,54},{52,80}})));
equation
  connect(stay_on_Sj.out_value, pumpControl_Sj) annotation (Line(
      points={{297.6,-60},{330,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T7,max_40_T7. u2) annotation (Line(
      points={{-102,-186},{-66,-186},{-66,-138.4},{-61.8,-138.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst3.y,max_40_T7. u1) annotation (Line(
      points={{-74.8,-127},{-74,-127},{-74,-127.6},{-61.8,-127.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(max_40_T7.y, add_5.u1) annotation (Line(
      points={{-41.1,-133},{-28,-133},{-28,-124},{-20,-124}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst4.y, add_5.u2) annotation (Line(
      points={{-76.9,-164},{-30,-164},{-30,-136},{-20,-136}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(final_state.y, stay_on_Sj.in_value) annotation (Line(
      points={{269.4,-60},{280,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T5, T5_Test.u) annotation (Line(
      points={{-100,-52},{-22,-52}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5_and_Delta_test_1.y, on1_replicator.u) annotation (Line(
      points={{111.5,-10},{128,-10}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on1_replicator.y, On_IndirectSun.u2) annotation (Line(
      points={{151,-10},{172,-10},{172,22.4},{177.6,22.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(On_IndirectSun.y, final_state.u1) annotation (Line(
      points={{205.2,32},{220,32},{220,-60},{237.2,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T1_and_Delta_test_2.y, on2_replicator.u) annotation (Line(
      points={{111.5,-100},{128,-100}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T1, Test_T1.u1) annotation (Line(
      points={{-100,-100},{28,-100}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add_5.y, Test_T1.u2) annotation (Line(
      points={{3,-130},{8,-130},{8,-108},{28,-108}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(V3V_solar, V3V_inverse.u) annotation (Line(
      points={{-100,-6},{10,-6},{10,-5},{25.8,-5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_solar, T1_and_Delta_test_2.u[1]) annotation (Line(
      points={{-100,-6},{10,-6},{10,-80},{60,-80},{60,-96.5},{90,-96.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Test_T1.y, T1_and_Delta_test_2.u[2]) annotation (Line(
      points={{51,-100},{76,-100},{76,-103.5},{90,-103.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_inverse.y, T5_and_Delta_test_1.u[1]) annotation (Line(
      points={{51.1,-5},{78,-5},{78,-6.5},{90,-6.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T5_Test.y, T5_and_Delta_test_1.u[2]) annotation (Line(
      points={{1,-52},{60,-52},{60,-13.5},{90,-13.5}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(on2_replicator.y, On_DirectSun.u1) annotation (Line(
      points={{151,-100},{170,-100},{170,-127},{177.4,-127}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(On_DirectSun.y, final_state.u2) annotation (Line(
      points={{207.3,-127},{220,-127},{220,-71.2},{237.2,-71.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(On_DirectSun.y, true_seeker.u) annotation (Line(
      points={{207.3,-127},{220,-127},{220,-200},{228,-200}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(true_seeker.y, stay_on_S5.in_value) annotation (Line(
      points={{270,-200},{284,-200}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(stay_on_S5.out_value, pumpControl_S5) annotation (Line(
      points={{301.6,-200},{312,-200},{312,-160},{320,-160}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_heat, On_IndirectSun.u1) annotation (Line(
      points={{-100,40},{160,40},{160,32},{177.6,32}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_heat, overheat_or_heat.u2) annotation (Line(
      points={{-100,40},{-20,40},{-20,56.6},{23.4,56.6}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_overheat, overheat_or_heat.u1) annotation (Line(
      points={{-100,80},{-20,80},{-20,67},{23.4,67}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(overheat_or_heat.y, On_DirectSun.u2) annotation (Line(
      points={{53.3,67},{166,67},{166,-137.4},{177.4,-137.4}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-220},{320,100}},
          preserveAspectRatio=false),  graphics), Icon(coordinateSystem(extent={{-100,
            -220},{320,100}}, preserveAspectRatio=false),  graphics={
        Text(
          extent={{-64,80},{278,-180}},
          lineColor={0,128,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          textString="Nb Room
%n"),   Polygon(
          points={{-100,100},{320,100},{320,-220},{-100,-220},{-100,60},{-100,
              60},{-100,100}},
          lineColor={255,0,255},
          smooth=Smooth.None)}),
    Documentation(info="<html>
<p>Allow overheating if possible for direct solar heating (using power directly from the collectors). Only temperature instruction take into consideration for indirect solar heating (using tank).</p>
</html>"));
end Heating_mod_with_overheating;
