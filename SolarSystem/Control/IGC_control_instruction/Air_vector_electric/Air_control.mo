within SolarSystem.Control.IGC_control_instruction.Air_vector_electric;
model Air_control
  "Group all models to compute fan speed, electrical heat, pump speed, and blowing temperature instruction"
  extends
    SolarSystem.Control.IGC_control_instruction.Air_vector_electric.Algorithm_characteristics;
  Modelica.Blocks.Interfaces.RealVectorInput Texch_outlet[n]
    "Temperature just after the exchanger outlet (air side)" annotation (
      Placement(transformation(extent={{-110,-42},{-94,-26}}),
                                                             iconTransformation(
          extent={{-106,-38},{-94,-26}})));
  Modelica.Blocks.Interfaces.BooleanVectorInput Pump_state[n]
    "Heating pumps states" annotation (Placement(transformation(extent={{-106,52},
            {-90,68}}),  iconTransformation(extent={{-106,66},{-94,78}})));
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_heat[n] "True = need"
    annotation (Placement(transformation(extent={{-106,18},{-90,34}}),
        iconTransformation(extent={{-106,28},{-94,40}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_heating_Power[n]
    "Electrical power to keep setpoint temperature inside house" annotation (
      Placement(transformation(extent={{96,78},{116,98}}), iconTransformation(
          extent={{96,84},{108,96}})));
  Modelica.Blocks.Interfaces.RealOutput Elec_heating_Energy[n]
    "Electrical energy used to keep setpoint temperature inside house"
    annotation (Placement(transformation(extent={{96,58},{116,78}}),
        iconTransformation(extent={{96,54},{108,66}})));
  Modelica.Blocks.Interfaces.RealOutput Fan_VolFlow[n]
    "Fan mass flow rate output" annotation (Placement(transformation(extent={{94,-4},
            {116,18}}),        iconTransformation(extent={{96,-6},{108,6}})));
  Modelica.Blocks.Interfaces.RealOutput Sj_normSpeed_out[n]
    "Sj normalized speed rate output" annotation (Placement(transformation(
          extent={{96,-30},{116,-10}}), iconTransformation(extent={{96,-26},{
            108,-14}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n] annotation (Placement(
        transformation(extent={{-106,86},{-90,102}}),iconTransformation(extent={{-106,
            104},{-94,116}})));
  Modelica.Blocks.Interfaces.RealVectorInput
                                       Tinstruction[n] annotation (Placement(
        transformation(extent={{-106,4},{-90,20}}),   iconTransformation(extent={{-106,10},
            {-94,22}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tambiant[n] annotation (Placement(
        transformation(extent={{-110,-26},{-94,-10}}),
                                                     iconTransformation(extent={{-106,
            -22},{-94,-10}})));
  Modelica.Blocks.Interfaces.RealInput Text "Exterior temperature" annotation (
      Placement(transformation(extent={{-112,68},{-92,90}}), iconTransformation(
          extent={{-104,84},{-94,94}})));

  Sj_control sj_control(
    n=n,
    Sj_controllerType=Sj_controllerType,
    Sj_k=Sj_k,
    Sj_Ti=Sj_Ti,
    Sj_Td=Sj_Td,
    Sj_yMax=Sj_yMax,
    Sj_yMin=Sj_yMin,
    Sj_wp=Sj_wp,
    Sj_wd=Sj_wd,
    Sj_Ni=Sj_Ni,
    Sj_Nd=Sj_Nd,
    Sj_reverseAction=Sj_reverseAction,
    tempo_Elec=tempo_Elec)
    annotation (Placement(transformation(extent={{-14,-30},{14,-16}})));
  Electrical_heat electrical_heat(
    n=n,
    electrical_power=electrical_power,
    Elec_controllerType=Elec_controllerType,
    Elec_k=Elec_k,
    Elec_Ti=Elec_Ti,
    Elec_Td=Elec_Td,
    Elec_yMax=Elec_yMax,
    Elec_yMin=Elec_yMin,
    Elec_wp=Elec_wp,
    Elec_wd=Elec_wd,
    Elec_Ni=Elec_Ni,
    Elec_Nd=Elec_Nd,
    Elec_reverseAction=Elec_reverseAction)
    annotation (Placement(transformation(extent={{40,70},{74,88}})));
  Tsouff_activation tsouff_activation(
    n=n,
    tempo_souff=tempo_souff,
    Tsouff_max=Tsouff_max,
    max_sanitary_flow=max_sanitary_flow,
    fan_bandwidth=max_sanitary_flow)
    annotation (Placement(transformation(extent={{-72,28},{-44,44}})));
  Tsouff_control tsouff_control(
    n=n,
    Tsouff_max=Tsouff_max,
    Tinstruction_controllerType=Tinstruction_controllerType,
    Tinstruction_k=Tinstruction_k,
    Tinstruction_Ti=Tinstruction_Ti,
    Tinstruction_Td=Tinstruction_Td,
    Tinstruction_yMax=Tinstruction_yMax,
    Tinstruction_yMin=Tinstruction_yMin,
    Tinstruction_wp=Tinstruction_wp,
    Tinstruction_wd=Tinstruction_wd,
    Tinstruction_Ni=Tinstruction_Ni,
    Tinstruction_Nd=Tinstruction_Nd,
    Tinstruction_reverseAction=Tinstruction_reverseAction)
    annotation (Placement(transformation(extent={{-16,42},{14,60}})));
  Fan_control fan_control(
    n=n,
    fan_controllerType=fan_controllerType,
    fan_k=fan_k,
    fan_Ti=fan_Ti,
    fan_Td=fan_Td,
    fan_wp=fan_wp,
    fan_wd=fan_wd,
    fan_Ni=fan_Ni,
    fan_Nd=fan_Nd,
    fan_reverseAction=fan_reverseAction)
    annotation (Placement(transformation(extent={{-18,16},{16,36}})));
  Modelica.Blocks.Logical.Pre cut_loop[n](each pre_u_start=false)
    "Used  to cut code for algebric loop"
    annotation (Placement(transformation(extent={{-38,32},{-30,40}})));
protected
  Utilities.Other.Schedule Flow_mini(table=minimal_flow_evolution)
    "Reduced flow during unoccupancy (work hours 9-18) and minimal flow during occupancy (all week-end and 0-9 and 18-24 during works days)."
    annotation (Placement(transformation(extent={{-100,106},{-90,116}})));
public
  Fan_flow_Controller fan_flow_Controller(n=n, fan_Max=fan_Max)
    annotation (Placement(transformation(extent={{46,14},{74,30}})));
  Modelica.Blocks.Interfaces.RealOutput minimal_flow_rate[n]
    "Fan mass flow rate output" annotation (Placement(transformation(extent={{96,
            34},{118,56}}), iconTransformation(extent={{96,14},{108,26}})));
  Modelica.Blocks.Routing.Replicator replicator(nout=n)
    annotation (Placement(transformation(extent={{-86,106},{-76,116}})));
  Modelica.Blocks.Interfaces.BooleanVectorInput Need_overheat[n] "True = need"
    annotation (Placement(transformation(extent={{-106,34},{-90,50}}),
        iconTransformation(extent={{-106,48},{-94,60}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tsolar_instruction[n]
    "Setpoint for solar overheating" annotation (Placement(transformation(
          extent={{-106,-10},{-90,6}}), iconTransformation(extent={{-106,-6},{
            -94,6}})));
equation
  connect(sj_control.Need_electricalPower, electrical_heat.Need_electricalPower)
    annotation (Line(
      points={{15.1667,-25.66},{34,-25.66},{34,79.225},{40.2429,79.225}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(electrical_heat.Elec_Heating, Elec_heating_Power) annotation (Line(
      points={{74.4857,83.5},{87.2429,83.5},{87.2429,88},{106,88}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(electrical_heat.Energy, Elec_heating_Energy) annotation (Line(
      points={{74.4857,74.5},{86.2429,74.5},{86.2429,68},{106,68}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsouff, electrical_heat.Tsouff) annotation (Line(
      points={{-98,94},{0,94},{0,72.475},{40.2429,72.475}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(tsouff_control.Tsouff_instruction, tsouff_activation.Tsouff_instruction)
    annotation (Line(
      points={{15.2,49.2},{20,49.2},{20,64},{-80,64},{-80,40.2},{-71.8,40.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(tsouff_control.Tsouff_instruction, electrical_heat.Tsouff_instruction)
    annotation (Line(
      points={{15.2,49.2},{16,49.2},{16,52},{20,52},{20,85.525},{40.2429,85.525}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(Tinstruction, fan_control.Tinstruction) annotation (Line(
      points={{-98,12},{-28,12},{-28,32.2},{-18.34,32.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant, fan_control.Tambiant) annotation (Line(
      points={{-102,-18},{-24,-18},{-24,19.8},{-17.66,19.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texch_outlet, sj_control.Texch_outlet) annotation (Line(
      points={{-102,-34},{-40,-34},{-40,-17.4},{-14,-17.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Pump_state, sj_control.Pump_state) annotation (Line(
      points={{-98,60},{-88,60},{-88,-20.2},{-14,-20.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(tsouff_control.Tsouff_instruction, sj_control.Tsouff_instruction)
    annotation (Line(
      points={{15.2,49.2},{20,49.2},{20,64},{-80,64},{-80,-23},{-14,-23}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant, tsouff_control.Tambiant) annotation (Line(
      points={{-102,-18},{-24,-18},{-24,50.325},{-16.3,50.325}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction, tsouff_control.Tinstruction) annotation (Line(
      points={{-98,12},{-28,12},{-28,54},{-20,54},{-20,53.925},{-16.3,53.925}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text, tsouff_control.Text) annotation (Line(
      points={{-102,79},{-40,79},{-40,43.575},{-15.7,43.575}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sj_control.Pump_normSpeed_out, Sj_normSpeed_out) annotation (Line(
      points={{14.9333,-20.2},{56.4667,-20.2},{56.4667,-20},{106,-20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Need_heat, sj_control.Need_heat) annotation (Line(
      points={{-98,26},{-90,26},{-90,-25.8},{-14,-25.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_heat, tsouff_activation.Need_heat) annotation (Line(
      points={{-98,26},{-90,26},{-90,36.2},{-71.8,36.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(fan_flow_Controller.Fan_VolFlow_out, Fan_VolFlow) annotation (Line(
      points={{74.4,22},{92,22},{92,7},{105,7}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(fan_control.Fan_flowNormalized_out, fan_flow_Controller.Computed_coef)
    annotation (Line(
      points={{18.04,25.6},{46,25.6},{46,26},{46.4,26}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Flow_mini.y[1], replicator.u) annotation (Line(
      points={{-89.5,111},{-87,111}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(replicator.y, fan_flow_Controller.Minimal_flow) annotation (Line(
      points={{-75.5,111},{28,111},{28,18},{46.4,18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(replicator.y, minimal_flow_rate) annotation (Line(
      points={{-75.5,111},{28,111},{28,45},{107,45}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(fan_flow_Controller.Fan_VolFlow_out, tsouff_activation.Fan_flow)
    annotation (Line(
      points={{74.4,22},{80,22},{80,0},{-76,0},{-76,31.4},{-71.8,31.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(tsouff_activation.Need_limitFlow, cut_loop.u) annotation (Line(
      points={{-44.2,36.2},{-41.1,36.2},{-41.1,36},{-38.8,36}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_overheat, sj_control.Need_overheat) annotation (Line(
      points={{-98,42},{-84,42},{-84,-28.6},{-14,-28.6}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(cut_loop.y, fan_control.Need_limitFlow) annotation (Line(
      points={{-29.6,36},{-26,36},{-26,25.8},{-18.34,25.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(cut_loop.y, tsouff_control.Need_limitFlow) annotation (Line(
      points={{-29.6,36},{-26,36},{-26,46.725},{-16.3,46.725}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Need_overheat, tsouff_control.Need_overheating) annotation (Line(
      points={{-98,42},{-84,42},{-84,86},{-10.3,86},{-10.3,60.225}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tsolar_instruction, tsouff_control.Tsolar_instruction) annotation (
      Line(
      points={{-98,-2},{-82,-2},{-82,57.975},{-16.3,57.975}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-40},{100,120}},
          preserveAspectRatio=false), graphics),                        Icon(
        coordinateSystem(extent={{-100,-40},{100,120}}, preserveAspectRatio=false),
                    graphics),
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Differences between IGC_control_instruction and IGC_control:</span></h4>
<ul>
<li>Minimal flow not a constant anymore but a value that evolve according to user schedule.</li>
<li>Using normalized flow from <a href=\"SolarSystem.Control.IGC_control_instruction.Air_vector_electric.Fan_control\">Fan_control</a> we compute real flow using <a href=\"SolarSystem.Control.IGC_control_instruction.Air_vector_electric.Fan_flow_Controller\">Fan_flow_Controller</a></li>
</ul>
</html>"));
end Air_control;
