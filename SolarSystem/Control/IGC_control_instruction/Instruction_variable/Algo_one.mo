within SolarSystem.Control.IGC_control_instruction.Instruction_variable;
model Algo_one
  "All INPUT temperature must be in Kelvin(K) not �C or �F, otherwise might lead to strange behaviour. Use when only one room"
  import SolarSystem;

  parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

  // Weather file info
  parameter String WeatherFile="C:/Users/bois/Documents/Dymola/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos"
    "Name of weather data file" annotation (Dialog(tab="Weather"));
  parameter Modelica.SIunits.ThermodynamicTemperature Tmoy_first=279.94
    "Average temperature from first day" annotation (Dialog(tab="Weather"));
  parameter Modelica.SIunits.ThermodynamicTemperature Tmin_first=275.15
    "Minimal temperature from first day" annotation (Dialog(tab="Weather"));
  parameter Real offset=3600*24 "Temperature offset (time of an output step)"
    annotation (Dialog(tab="Weather"));
  Modelica.Blocks.Interfaces.RealOutput    V3V_extra_outter(start=1)
    "Open to solar panels = 1, Open to extra tank = 0"
    annotation (Placement(transformation(extent={{374,-126},{406,-94}}),
        iconTransformation(extent={{370,126},{400,156}})));

  SolisArt_mod.solarTank_mod solarTank_mod1(pumpControl_S5)
    annotation (Placement(transformation(extent={{120,104},{156,140}})));
  SolisArt_mod.storageTank_mod storageTank_mod1(pumpControl_S6, deltaT_hys_test(
        uHigh=9))
    annotation (Placement(transformation(extent={{120,62},{166,98}})));
  SolisArt_mod.SolarValve_mod solarValve_mod
    annotation (Placement(transformation(extent={{-58,-60},{-26,-36}})));
  SolarSystem.Control.IGC_control.Instruction_variable.ExtraValve_mod_one
                                                     extraValve_mod(
    n=n,
    ECS_out,
    CHAUFF_out)
    annotation (Placement(transformation(extent={{-12,-102},{22,-66}})));
  SolisArt_mod.BackupHeater_mod backupHeater_mod(BackupHeater)
    annotation (Placement(transformation(extent={{126,-176},{172,-128}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-134,118},{-106,146}}),
        iconTransformation(extent={{-130,116},{-108,138}})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-134,94},{-106,122}}),
        iconTransformation(extent={{-130,86},{-108,108}})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
    annotation (Placement(transformation(extent={{-134,70},{-106,98}}),
        iconTransformation(extent={{-130,56},{-108,78}})));
  Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-134,6},{-106,34}}),
        iconTransformation(extent={{-130,-2},{-108,20}})));
  Modelica.Blocks.Interfaces.RealInput T8
    "T. at heating start point (just before extra exchanger input)"
    annotation (Placement(transformation(extent={{-132,-28},{-104,0}}),
        iconTransformation(extent={{-130,-32},{-108,-10}})));
  Modelica.Blocks.Interfaces.RealVectorInput
                                       Tambiant[n](start=Tambiant_out_start)
    "T. in the room"
    annotation (Placement(transformation(extent={{-134,-116},{-106,-88}}),
        iconTransformation(extent={{-132,-104},{-110,-82}})));
  Modelica.Blocks.Interfaces.RealInput Text "T. from outdoor"
    annotation (Placement(transformation(extent={{-134,-144},{-106,-116}}),
        iconTransformation(extent={{-128,-146},{-104,-122}})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-134,44},{-106,72}}),
        iconTransformation(extent={{-130,28},{-108,50}})));

  Modelica.Blocks.Interfaces.RealInput flow_S4 "S4 pump flow rate"
    annotation (Placement(transformation(extent={{-18,-18},{18,18}},
        rotation=-90,
        origin={0,160}),  iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=-90,
        origin={-51,161})));

  Modelica.Blocks.Interfaces.BooleanOutput
                                        S4_outter(start=false)
    "ON = 1, OFF = 0 (pump S4)"
    annotation (Placement(transformation(extent={{372,44},{404,76}}),
        iconTransformation(extent={{370,-58},{400,-28}})));
  Modelica.Blocks.Interfaces.BooleanOutput
                                        S5_outter(start=false)
    "ON = 1, OFF = 0 (pump S5)"
    annotation (Placement(transformation(extent={{372,124},{404,156}}),
        iconTransformation(extent={{370,-18},{400,12}})));
  Modelica.Blocks.Interfaces.BooleanOutput
                                        Sj_outter[n](each start=true)
    "ON = 1, OFF = 0 (pump Sj)"
    annotation (Placement(transformation(extent={{372,4},{404,36}}),
        iconTransformation(extent={{370,-98},{400,-68}})));

  Modelica.Blocks.Interfaces.RealOutput    V3V_solar_outter(start=1)
    "Open to solar panels = 1, Open to storage tank = 0"
    annotation (Placement(transformation(extent={{372,-84},{404,-52}}),
        iconTransformation(extent={{370,86},{400,116}})));

  Modelica.Blocks.Interfaces.RealOutput    BackupHeater_outter(start=0)
    "1 = needed, 0 otherwise"
    annotation (Placement(transformation(extent={{370,-176},{402,-144}}),
        iconTransformation(extent={{370,-166},{400,-136}})));
  SolarSystem.Control.IGC_control.Instruction_variable.Chauff_state
                                               chauff_state(n=n)
    annotation (Placement(transformation(extent={{230,-34},{278,2}})));
  SolarSystem.Control.IGC_control.Instruction_variable.Heating_mod
                                              heating_mod(n=n,
      Tambiant_out_start=Tambiant_out_start)
    annotation (Placement(transformation(extent={{134,-40},{184,6}})));
  Modelica.Blocks.Logical.Or or_S5
    annotation (Placement(transformation(extent={{338,130},{358,150}})));
  Modelica.Blocks.Routing.BooleanPassThrough ECS
                                          annotation (Placement(transformation(
        extent={{-19.5,-19.5},{19.5,19.5}},
        rotation=180,
        origin={238.5,-119.5})));

  Modelica.Blocks.Math.BooleanToReal real_solar
                                           annotation (Placement(transformation(
        extent={{-8,8},{8,-8}},
        rotation=0,
        origin={348,-68})));
  Modelica.Blocks.Math.BooleanToReal real_extra
                                           annotation (Placement(transformation(
        extent={{-8,8},{8,-8}},
        rotation=0,
        origin={348,-110})));
  Modelica.Blocks.Math.BooleanToReal real_backup
                                           annotation (Placement(transformation(
        extent={{-8,8},{8,-8}},
        rotation=0,
        origin={348,-160})));

  SolarSystem.Control.IGC_control.Instruction_variable.Variables_state
                                                  variables_state(pad=pad)
    annotation (Placement(transformation(extent={{42,-16},{84,6}})));
  Modelica.Blocks.Interfaces.BooleanOutput S6_outter(start=false)
    "ON = 1, OFF = 0 (pump S6)"
    annotation (Placement(transformation(extent={{370,84},{402,116}}),
        iconTransformation(extent={{370,24},{400,54}})));
  Utilities.Other.MiniAverage extract_TdryBulb(
    filNam=WeatherFile,
    Tmoy_first=Tmoy_first,
    Tmin_first=Tmin_first,
    offset=offset) annotation (Placement(transformation(extent={{-120,-180},{-78,-152}})));

  parameter Modelica.SIunits.ThermodynamicTemperature Tambiant_out_start[n]={291.15,291.15}
    "T. in the rooms";
  parameter Modelica.SIunits.ThermodynamicTemperature pad=276.15
    "Used to compute TsolarInstruction";
  Modelica.Blocks.Interfaces.RealInput mini_flow_S4 "S4 mini pump flow rate"
    annotation (Placement(transformation(extent={{-18,-18},{18,18}},
        rotation=-90,
        origin={44,160}), iconTransformation(
        extent={{-11,-11},{11,11}},
        rotation=-90,
        origin={-1,161})));
  SolisArt_mod.extraTank_mod extraTank_mod1
    annotation (Placement(transformation(extent={{42,16},{96,60}})));
  Utilities.Timers.Stay_on stay_on(out_value(start=false), pause=60)
    annotation (Placement(transformation(extent={{300,112},{316,128}})));
  Utilities.Logical.True_seeker true_seeker(n=n)
    annotation (Placement(transformation(extent={{60,-178},{78,-160}})));
  Modelica.Blocks.Interfaces.RealVectorInput Tinstruction[n] "T. in the room"
    annotation (Placement(transformation(extent={{-134,-82},{-106,-54}}),
        iconTransformation(extent={{-132,-72},{-110,-50}})));
equation
  connect(T1, solarTank_mod1.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,132.5},{120,132.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, storageTank_mod1.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,88},{120,88}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, solarValve_mod.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,-39},{-58,-39}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, extraValve_mod.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,-69},{-12,-69}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, solarTank_mod1.T3) annotation (Line(
      points={{-120,108},{-76,108},{-76,126.5},{120,126.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, storageTank_mod1.T3) annotation (Line(
      points={{-120,108},{-76,108},{-76,78},{120,78}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, solarValve_mod.T3) annotation (Line(
      points={{-120,108},{-76,108},{-76,-45},{-58,-45}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, solarValve_mod.T4) annotation (Line(
      points={{-120,84},{-80,84},{-80,-51},{-58,-51}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, extraValve_mod.T4) annotation (Line(
      points={{-120,84},{-80,84},{-80,-73},{-12,-73}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7, extraValve_mod.T7) annotation (Line(
      points={{-120,20},{-90,20},{-90,-80},{-12,-80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T8, backupHeater_mod.T8) annotation (Line(
      points={{-118,-14},{-94,-14},{-94,-135.385},{126,-135.385}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(extraValve_mod.V3V_extra, backupHeater_mod.V3V_extra)
    annotation (Line(
      points={{23.275,-84},{30,-84},{30,-146.462},{126,-146.462}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(extraValve_mod.ECS_out, backupHeater_mod.ECS) annotation (
      Line(
      points={{23.275,-71},{28,-71},{28,-157.538},{126,-157.538}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tambiant, extraValve_mod.Tambiant) annotation (Line(
      points={{-120,-102},{-70,-102},{-70,-84.6},{-12.425,-84.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, storageTank_mod1.T5) annotation (Line(
      points={{-120,58},{-86,58},{-86,68},{120,68}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, solarValve_mod.T5) annotation (Line(
      points={{-120,58},{-86,58},{-86,-57},{-58,-57}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(solarValve_mod.V3V_solar, solarTank_mod1.V3V_solar)
    annotation (Line(
      points={{-26,-48},{24,-48},{24,111.5},{120,111.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(solarValve_mod.V3V_solar, heating_mod.V3V_solar) annotation (
      Line(
      points={{-26,-48},{106,-48},{106,-15.0136},{134.357,-15.0136}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T8, heating_mod.T8) annotation (Line(
      points={{-118,-14},{2,-14},{2,-38},{122,-38},{122,-38.85},{134.595,-38.85}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, heating_mod.T5) annotation (Line(
      points={{-120,58},{-86,58},{-86,-27.0364},{134.238,-27.0364}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant, heating_mod.Tambiant) annotation (Line(
      points={{-120,-102},{-70,-102},{-70,-114},{92,-114},{92,3.90909},{134.476,
          3.90909}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(heating_mod.Tambiant_out, chauff_state.Tambiant) annotation (
      Line(
      points={{183.881,4.01364},{216,4.01364},{216,-12},{229.314,-12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarTank_mod1.pumpControl_S5, or_S5.u1) annotation (Line(
      points={{156,119},{180,119},{180,80},{280,80},{280,140},{336,140}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(chauff_state.Chauff, extraValve_mod.CHAUFF) annotation (Line(
      points={{278,-14},{292,-14},{292,-64},{-26,-64},{-26,-100},{-12.2125,-100}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(ECS.y, extraValve_mod.ECS) annotation (Line(
      points={{217.05,-119.5},{200,-119.5},{200,-180},{-60,-180},{-60,-95.2},{-12.2125,
          -95.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(real_backup.y, BackupHeater_outter) annotation (Line(
      points={{356.8,-160},{386,-160}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(real_solar.y, V3V_solar_outter) annotation (Line(
      points={{356.8,-68},{388,-68}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(real_extra.y, V3V_extra_outter) annotation (Line(
      points={{356.8,-110},{390,-110}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(backupHeater_mod.BackupHeater, real_backup.u) annotation (Line(
      points={{173.769,-150.154},{320.885,-150.154},{320.885,-160},{338.4,-160}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(or_S5.y, S5_outter) annotation (Line(
      points={{359,140},{388,140}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(heating_mod.pumpControl_Sj, Sj_outter) annotation (Line(
      points={{184,-15.9545},{200,-15.9545},{200,10},{320,10},{320,20},{388,20}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(solarValve_mod.V3V_solar, real_solar.u) annotation (Line(
      points={{-26,-48},{320,-48},{320,-68},{338.4,-68}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(variables_state.DTeco, heating_mod.DTeco) annotation (Line(
      points={{84.3,-0.285714},{120,-0.285714},{120,0},{128,0},{128,-0.272727},
          {134.476,-0.272727}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(variables_state.TsolarInstruction, heating_mod.TsolarInstruction)
    annotation (Line(
      points={{84.3,-9.71429},{99.15,-9.71429},{99.15,-4.45455},{134.476,
          -4.45455}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(variables_state.T1_out, heating_mod.T1) annotation (Line(
      points={{60,-16.6286},{60,-22.6455},{134.238,-22.6455}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(variables_state.T7_out, heating_mod.T7) annotation (Line(
      points={{69,-16.6286},{69,-34.6682},{134.595,-34.6682}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, variables_state.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,4.11429},{42,4.11429}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7, variables_state.T7) annotation (Line(
      points={{-120,20},{-90,20},{-90,-2.17143},{42,-2.17143}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text, variables_state.Text) annotation (Line(
      points={{-120,-130},{34,-130},{34,-8.14286},{42,-8.14286}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(storageTank_mod1.pumpControl_S6, S6_outter) annotation (Line(
      points={{166,70},{320,70},{320,100},{386,100}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(extract_TdryBulb.TminOfDay, variables_state.Text_mini) annotation (
      Line(
      points={{-76.74,-160.4},{36,-160.4},{36,-14.1143},{42,-14.1143}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(mini_flow_S4, extraTank_mod1.mini_flow_S4) annotation (Line(
      points={{44,160},{44,60},{45.6818,60}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(flow_S4, extraTank_mod1.flow_S4) annotation (Line(
      points={{0,160},{0,56.3333},{42,56.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, extraTank_mod1.T3) annotation (Line(
      points={{-120,108},{-76,108},{-76,49},{42,49}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, extraTank_mod1.T1) annotation (Line(
      points={{-120,132},{-72,132},{-72,44.1111},{42,44.1111}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, extraTank_mod1.T4) annotation (Line(
      points={{-120,84},{-80,84},{-80,36.7778},{42,36.7778}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(extraTank_mod1.pumpControl_S4, S4_outter) annotation (Line(
      points={{96,40.4444},{320,40.4444},{320,60},{388,60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(extraTank_mod1.ECS, ECS.u) annotation (Line(
      points={{96,31.6444},{306,31.6444},{306,-119.5},{261.9,-119.5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(extraValve_mod.V3V_extra, extraTank_mod1.V3V_extra)
    annotation (Line(
      points={{23.275,-84},{30,-84},{30,19.6667},{42,19.6667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(extraValve_mod.V3V_extra, heating_mod.V3V_extra) annotation (
      Line(
      points={{23.275,-84},{30,-84},{30,-22},{102,-22},{102,-10.6227},{134.357,
          -10.6227}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(variables_state.DTeco, chauff_state.DTeco) annotation (Line(
      points={{84.3,-0.285714},{120,-0.285714},{120,14},{220,14},{220,-2},{230,
          -2}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(heating_mod.V3V_extra_out, real_extra.u) annotation (Line(
      points={{183.881,-2.25909},{208,-2.25909},{208,-90},{320,-90},{320,-110},
          {338.4,-110}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(extraValve_mod.ECS_out, chauff_state.ECS) annotation (Line(
      points={{23.275,-71},{220,-71},{220,-30},{230,-30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(heating_mod.pumpControl_S5, stay_on.in_value) annotation (
      Line(
      points={{184,-33.7273},{192,-33.7273},{192,60},{290,60},{290,120},{300,
          120}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(stay_on.out_value, or_S5.u2) annotation (Line(
      points={{317.6,120},{320,120},{320,132},{336,132}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(extraValve_mod.CHAUFF_out, true_seeker.u) annotation (Line(
      points={{23.4875,-96},{26,-96},{26,-169},{60,-169}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(true_seeker.y, backupHeater_mod.CHAUFF) annotation (Line(
      points={{78,-169},{124,-169},{124,-168},{126,-168},{126,-168.615},{126,
          -168.615}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(extraValve_mod.CHAUFF_out, heating_mod.CHAUFF) annotation (Line(
      points={{23.4875,-96},{120,-96},{120,-18.9864},{134.357,-18.9864}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tinstruction, extraValve_mod.Tinstruction) annotation (Line(
      points={{-120,-68},{-102,-68},{-102,-89.8},{-12.425,-89.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction, variables_state.Tinstruction) annotation (Line(
      points={{-120,-68},{-102,-68},{-102,14},{50.1,14},{50.1,6.62857}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(heating_mod.Tinstruction_out, chauff_state.Tinstruction) annotation (
      Line(
      points={{184.119,-25.4682},{220,-25.4682},{220,-22},{229.314,-22}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction, heating_mod.Tinstruction) annotation (Line(
      points={{-120,-68},{-102,-68},{-102,14},{116,14},{116,-30.6955},{133.643,
          -30.6955}},
      color={0,0,127},
      smooth=Smooth.None));

                                                                                                      annotation(Dialog(tab="Execution cycle"),
              Diagram(coordinateSystem(extent={{-120,-180},{380,160}},
          preserveAspectRatio=false),   graphics, defaultComponentName = "algo_states"), Icon(coordinateSystem(
          extent={{-120,-180},{380,160}}, preserveAspectRatio=false),
        graphics={Rectangle(
          extent={{-120,160},{382,-180}},
          lineColor={90,150,90},
          fillPattern=FillPattern.CrossDiag,
          fillColor={0,127,127}),
        Text(
          extent={{-124,108},{380,28}},
          lineColor={0,0,0},
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="Algo"),
        Text(
          extent={{-120,-24},{388,-102}},
          lineColor={0,0,0},
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="States")}),
    Documentation(info="<html>
<p>Groups all algorithms into one block.</p>
<p>This modul must be used with pump_algo modul to have real pump state inputs.</p>
</html>"));
end Algo_one;
