within SolarSystem.Control.Bloc.Example;
model wait_for_test
  import SolarSystem;
  extends Modelica.Icons.Example;
  SolarSystem.Classes.Control.Bloc.Wait_for time_it[2]
    "Outter value can become true because inner value lower than 2.5 during at least 90sec"
                                                   annotation (
      Placement(transformation(extent={{58,40},{100,80}})));
  Modelica.Blocks.Sources.SawTooth enoughTime[2](
    amplitude=4,
    offset=-1,
    period=200)
    "Period which are longest enough to allow true time_it outter value"
               annotation (Placement(transformation(extent={{-100,60},
            {-80,80}})));
  Modelica.Blocks.Logical.LessThreshold Condition[2](threshold=2.5)
    "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
  Modelica.Blocks.Sources.SawTooth noEnoughTime(
    amplitude=4,
    offset=-1,
    period=100)
    "Period which aren't longest enough to allow true time_it outter value"
               annotation (Placement(transformation(extent={{-100,-80},
            {-80,-60}})));
  Modelica.Blocks.Logical.LessThreshold condition1(threshold=2.5)
    "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-60,-80},{-40,-60}})));
  SolarSystem.Classes.Control.Bloc.Wait_for time_it1
    "Outter value can't become true because inner value lower than 2.5 during less than 90sec"
                                                   annotation (
      Placement(transformation(extent={{58,-80},{100,-40}})));
equation
  connect(Condition.y, time_it.in_value) annotation (Line(
      points={{-39,70},{0,70},{0,60},{58,60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(enoughTime.y, Condition.u) annotation (Line(
      points={{-79,70},{-62,70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(noEnoughTime.y, condition1.u) annotation (Line(
      points={{-79,-70},{-62,-70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(condition1.y, time_it1.in_value) annotation (Line(
      points={{-39,-70},{0,-70},{0,-60},{58,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(graphics),
    experiment(StopTime=10000),
    __Dymola_experimentSetupOutput);
end wait_for_test;
