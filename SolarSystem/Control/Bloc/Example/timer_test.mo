within SolarSystem.Control.Bloc.Example;
model timer_test
  extends Modelica.Icons.Example;
  SolarSystem.Control.Bloc.Real_timer timer1(out_value(start=252), pause=1000)
    annotation (Placement(transformation(extent={{0,52},{44,88}})));
  Modelica.Blocks.Sources.Sine sine1(
    freqHz=1/86400,
    offset=273.15,
    amplitude=20,
    y(unit="K"),
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-78,60},{-58,80}})));
  SolarSystem.Control.Bloc.Real_timer timer[2](
    out_value(start={252,252}),
    started_value(start={0,0}),
    pause={1000,300})
    annotation (Placement(transformation(extent={{0,-18},{44,18}})));
  Modelica.Blocks.Sources.Sine sine2(
    freqHz=1/86400,
    offset=273.15,
    amplitude=20,
    y(unit="K"),
    phase=-1.5707963267949,
    startTime=200)
    annotation (Placement(transformation(extent={{-80,20},{-60,40}})));
  Modelica.Blocks.Sources.Sine sine3(
    freqHz=1/86400,
    offset=273.15,
    amplitude=20,
    y(unit="K"),
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-82,-40},{-62,-20}})));
equation
  connect(sine1.y, timer1.in_value) annotation (Line(
      points={{-57,70},{0,70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine2.y, timer[1].in_value) annotation (Line(
      points={{-59,30},{-40,30},{-40,0},{0,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine3.y, timer[2].in_value) annotation (Line(
      points={{-61,-30},{-40,-30},{-40,0},{0,0}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(graphics), Documentation(info="<html>
<p>Test the timer with only one input</p>
<p>Test timer with multiple inputs and differents &QUOT;pause&QUOT; values.</p>
</html>"));
end timer_test;
