within SolarSystem.Control.Bloc;
model storage_solar_control_noTimer
  import Assemblage;
  SolarSystem.Control.SolisArt_mod.SolarValve_mod solarValve_mod
    annotation (Placement(transformation(extent={{-10,58},{10,82}})));
  SolarSystem.Control.SolisArt_mod.storageTank_mod storageTank_mod(
      pumpControl_S6(start=true))
    annotation (Placement(transformation(extent={{-14,-4},{10,14}})));
  SolarSystem.Control.SolisArt_mod.solarTank_mod solarTank_mod(pumpControl_S5
      (start=false))
    annotation (Placement(transformation(extent={{-12,-80},{12,-56}})));
  Modelica.Blocks.Math.RealToBoolean booleanToReal
    annotation (Placement(transformation(extent={{-66,-82},{-56,-72}})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-112,48},{-88,72}})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
    annotation (Placement(transformation(extent={{-112,18},{-88,42}}),
        iconTransformation(extent={{-112,18},{-88,42}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-112,78},{-88,102}}),
        iconTransformation(extent={{-112,78},{-88,102}})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-112,-12},{-88,12}}),
        iconTransformation(extent={{-112,-12},{-88,12}})));
  Modelica.Blocks.Interfaces.RealInput solar_valve_pos
    "T. inside storage tank"
    annotation (Placement(transformation(extent={{-112,-82},{-88,-58}}),
        iconTransformation(extent={{-112,-82},{-88,-58}})));
  Modelica.Blocks.Interfaces.BooleanOutput V3V_solar(start=true)
    "Open to solar panels = true, Open to storage tank = false"
    annotation (Placement(transformation(extent={{100,50},{140,90}})));
  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S6(start=false)
    "ON = true, OFF = false (pump S6)"
    annotation (Placement(transformation(extent={{100,-20},{140,20}}),
        iconTransformation(extent={{100,-20},{140,20}})));
  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S5(start=true)
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{100,-90},{140,-50}}),
        iconTransformation(extent={{100,-90},{140,-50}})));
equation
  connect(solar_valve_pos, booleanToReal.u) annotation (Line(
      points={{-100,-70},{-82.5,-70},{-82.5,-77},{-67,-77}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, solarValve_mod.T1) annotation (Line(
      points={{-100,90},{-60,90},{-60,79},{-10,79}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, storageTank_mod.T1) annotation (Line(
      points={{-100,90},{-60,90},{-60,9},{-14,9}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, solarTank_mod.T1) annotation (Line(
      points={{-100,90},{-60,90},{-60,-61},{-12,-61}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, solarValve_mod.T3) annotation (Line(
      points={{-100,60},{-40,60},{-40,73},{-10,73}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, storageTank_mod.T3) annotation (Line(
      points={{-100,60},{-40,60},{-40,1},{-14,1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, solarTank_mod.T3) annotation (Line(
      points={{-100,60},{-40,60},{-40,-65},{-12,-65}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, storageTank_mod.T5) annotation (Line(
      points={{-100,0},{-80,0},{-80,5},{-14,5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, solarValve_mod.T5) annotation (Line(
      points={{-100,0},{-80,0},{-80,32},{-20,32},{-20,61},{-10,61}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, solarValve_mod.T4) annotation (Line(
      points={{-100,30},{-84,30},{-84,40},{-30,40},{-30,67},{-10,67}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(booleanToReal.y, solarTank_mod.V3V_solar) annotation (Line(
      points={{-55.5,-77},{-34.75,-77},{-34.75,-75},{-12,-75}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(solarTank_mod.pumpControl_S5, pumpControl_S5) annotation (
      Line(
      points={{12,-70},{120,-70}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(storageTank_mod.pumpControl_S6, pumpControl_S6) annotation (
      Line(
      points={{10,0},{58,0},{58,0},{120,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(solarValve_mod.V3V_solar, V3V_solar) annotation (Line(
      points={{10,70},{120,70}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-100},{100,100}},
          preserveAspectRatio=true),
                      graphics), Icon(coordinateSystem(extent={{-100,-100},{100,
            100}}),                   graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={90,150,90},
          fillPattern=FillPattern.CrossDiag,
          fillColor={0,127,127}), Text(
          extent={{-70,74},{72,-60}},
          lineColor={0,0,0},
          fillColor={255,85,255},
          fillPattern=FillPattern.CrossDiag,
          textStyle={TextStyle.Bold},
          fontName="Consolas",
          textString="Control
   Solar pump
   Storage pump
   Solar valve",
          horizontalAlignment=TextAlignment.Left)}));
end storage_solar_control_noTimer;
