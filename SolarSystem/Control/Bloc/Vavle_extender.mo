within SolarSystem.Control.Bloc;
model Vavle_extender

  Modelica.Blocks.Interfaces.BooleanInput V3V(start=true)
    annotation (Placement(transformation(extent={{-120,-20},{-80,20}}),
        iconTransformation(extent={{-120,-20},{-80,20}})));
  Modelica.Blocks.Interfaces.RealOutput same(start=start)
    "1 = open, 0 = close"
    annotation (Placement(transformation(extent={{100,20},{140,60}}),
        iconTransformation(extent={{100,20},{140,60}})));
  Modelica.Blocks.Interfaces.RealOutput not_same(start=-start)
    "1 = open, 0 = close"
    annotation (Placement(transformation(extent={{100,-60},{140,-20}}),
        iconTransformation(extent={{100,-60},{140,-20}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal
    annotation (Placement(transformation(extent={{38,32},{54,48}})));
  Modelica.Blocks.Math.BooleanToReal booleanToReal1
    annotation (Placement(transformation(extent={{44,-48},{60,-32}})));
  Modelica.Blocks.Logical.Not opposite
    "compute opposite, when solar openned storage closed"
    annotation (Placement(transformation(extent={{-14,-50},{6,-30}})));
  parameter Real start;
equation
  connect(V3V, booleanToReal.u)         annotation (Line(
      points={{-100,0},{-24,0},{-24,40},{36.4,40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V, opposite.u)         annotation (Line(
      points={{-100,0},{-24,0},{-24,-40},{-16,-40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(opposite.y, booleanToReal1.u) annotation (Line(
      points={{7,-40},{42.4,-40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(booleanToReal1.y, not_same)      annotation (Line(
      points={{60.8,-40},{120,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(booleanToReal.y, same)        annotation (Line(
      points={{54.8,40},{120,40}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(graphics), Icon(graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={90,150,90},
          fillPattern=FillPattern.CrossDiag,
          fillColor={230,75,230}), Text(
          extent={{-80,76},{62,-58}},
          lineColor={0,0,0},
          fillColor={255,85,255},
          fillPattern=FillPattern.CrossDiag,
          textStyle={TextStyle.Bold},
          fontName="Consolas",
          textString="Control
valve state")}));
end Vavle_extender;
