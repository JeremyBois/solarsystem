within SolarSystem.Control.Bloc.Timers;
model Wait_for_tempo_algo
  " : This model provides a simple way to add temporization between activations."

  Modelica.Blocks.Interfaces.BooleanInput in_value[n]
    annotation (Placement(transformation(extent={{-120,-20},{-80,20}}),
        iconTransformation(extent={{-120,-20},{-80,20}})));
  Modelica.Blocks.Interfaces.BooleanOutput out_value[n](each start=false)
    annotation (Placement(transformation(extent={{94,-20},{134,20}}),
        iconTransformation(extent={{94,-20},{134,20}})));
 parameter Integer n=2 "Number of inputs";
 parameter Modelica.SIunits.Time  wait_for[n-1]={90}
    "How much time we have to wait before the outter value can be true (n-1 values)";
 Modelica.SIunits.Time started_value[n];
 Modelica.SIunits.Time timer[n];

algorithm
for i in (1:n-1) loop
  // Condition reached (Avoid reset if out_value[i+1] still true)
  when out_value[i] and not out_value[i+1] and in_value[i+1] then
    started_value[i+1] :=time;
  end when;
  // Compute time passed since the condition reaching
   if (out_value[i] and in_value[i+1]) or out_value[i+1] then
     timer[i+1] :=time - started_value[i + 1];
   else
     timer[i+1] :=0;
   end if;

  // Return true if wait enough else false
  out_value[i+1] :=(if (timer[i + 1] >= wait_for[i] and in_value[i + 1]) then true
       else false);

//No temporization on the first element
out_value[1] :=in_value[1];
end for;

  annotation (Diagram(graphics), Icon(graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={100,0,100},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={250,0,50}),  Text(
          extent={{-100,66},{96,40}},
          lineColor={0,0,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="Tempo (sec)",
          fontName="Consolas"),
        Text(
          extent={{-100,-30},{100,-60}},
          lineColor={0,0,0},
          textString="%wait_for",
          fontName="Consolas")}),defaultComponentName = "tempo",
    Documentation(revisions="<html>
</html>", info="<html>
<p>This modul works like the Wait_for modul. The only difference between them is on the temporization .</p>
<p>This model provides a simple way to add temporization between activations.</p>
<p>The first vector element stay unchanged but the other elements are actived only if the previous element is activated since at least the wait-for time.</p>
<p>For example you want to start a new pump only if the first is already in function since 5&apos; and the third pump only if the second pump is on since 3&apos;, ... </p>
<p>You can see how it works with the &QUOT;wait_for_test&QUOT; in the &QUOT;example&QUOT; package : <a href=\"SolarSystem.Classes.Control.Bloc.Example.wait_for_tempo_test\">SolarSystem.Classes.Control.Bloc.Example.wait_for_tempo_test</a></p>
</html>"));
end Wait_for_tempo_algo;
