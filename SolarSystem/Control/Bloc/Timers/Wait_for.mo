within SolarSystem.Control.Bloc.Timers;
model Wait_for
  " : This modul provides a simple way to wait during a certain time (wait_for) if the inner condition becomes true before switch to true the outter value."

  Modelica.Blocks.Interfaces.BooleanInput
                                       in_value
    annotation (Placement(transformation(extent={{-120,-20},{-80,20}}),
        iconTransformation(extent={{-120,-20},{-80,20}})));
  Modelica.Blocks.Interfaces.BooleanOutput
                                        out_value
    annotation (Placement(transformation(extent={{94,-20},{134,20}}),
        iconTransformation(extent={{94,-20},{134,20}})));

 parameter Modelica.SIunits.Time  wait_for=90
    "How much time we have to wait before the outter value can be true";
 discrete Modelica.SIunits.Time started_value;
 Modelica.SIunits.Time timer;

equation
// Condition reached
when in_value then
  started_value = time;
end when;

// Compute time passed since the conditions reached
 if in_value then
   timer = time - started_value;
 else
   timer = 0;
 end if;

// Return true if wait enough else false
out_value = (if (timer >= wait_for and in_value) then true else false);

  annotation (Diagram(graphics), Icon(graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,255},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={127,0,127}), Text(
          extent={{-96,66},{100,40}},
          lineColor={0,0,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="Tempo (sec)",
          fontName="Consolas"),
        Text(
          extent={{-104,-26},{96,-56}},
          lineColor={0,0,0},
          textString="%wait_for",
          fontName="Consolas")}),
    Documentation(revisions="<html>
</html>", info="<html>
<p>This modul provides a simple way to wait during a certain time (wait_for) when the inner condition becomes true before switch to true the outter value.</p>
<p>If the inner value change before expectation ended, this modul wait until the inner value become true again and reinit the waiting time variable. So the outter value can become true only if the inner value is true during at least &QUOT;wait_for&QUOT; time.</p>
<p>Using this modul can help to solve &QUOT;yoyo&QUOT; effects and smooth the simulation output.</p>
<p>For example you want to start a pump if a condition became true but you want to be sure this condition will stay true a least 90 sec before allow the pump to start.</p>
<p>You can see how it works with the &QUOT;wait_for_test&QUOT; in the &QUOT;example&QUOT; package : <a href=\"SolarSystem.Classes.Control.Bloc.Example.wait_for_test\">SolarSystem.Classes.Control.Bloc.Example.wait_for_test</a></p>
</html>"));
end Wait_for;
