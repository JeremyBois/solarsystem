within SolarSystem.Control.Bloc.Utilities.SolisArt;
model split_input

  // 40 et 70 l/h/m2
  parameter Modelica.SIunits.MassFlowRate base[2]={0.0110, 0.0192}
    "Base mass flow rate : [1] >> flow to panel, [2]  >> flow to backup";

  // 20 et 35 l/h/m2
  parameter Modelica.SIunits.MassFlowRate add[2]={0.0055, 0.0096}
    "If more than one pump on : [1] >> flow to panel, [2]  >> flow to backup";

  parameter Integer nb_panel=6 "How many collector";

  parameter Modelica.SIunits.Area area_panel=2.32 "Collecting area";

  Modelica.Blocks.Interfaces.IntegerInput in_value[2]
    annotation (Placement(transformation(extent={{-120,-88},{-80,-48}}),
        iconTransformation(extent={{-120,-88},{-80,-48}})));
  Modelica.Blocks.Interfaces.RealOutput out_value[2]
    annotation (Placement(transformation(extent={{80,-20},{120,20}}),
        iconTransformation(extent={{80,-20},{120,20}})));

  Modelica.Blocks.Interfaces.BooleanInput V3V_extra
    "Open to solar panels = true, Open to extra tank = false"
    annotation (Placement(transformation(extent={{-120,40},{-80,80}}),
        iconTransformation(extent={{-120,46},{-80,86}})));
equation

  // Different flows rate if V3V to backup heating
  if not V3V_extra then
    // S6, S5
    if in_value[1] == 0 then
      out_value[1] = 0;
    else
      out_value[1] = ((base[1] + (in_value[1] - 1)* add[1]) / in_value[1]) * (nb_panel*area_panel);
    end if;
    // S4 et Sj
    if in_value[2] == 0 then
      out_value[2] = 0;
    else
      out_value[2] = ((base[2] + (in_value[2] - 1)* add[2]) / in_value[2]) * (nb_panel*area_panel);
    end if;

  // Same mass flows rate if V3V to solar collectors
  else
    if (in_value[1] + in_value[2]) == 0 then
      out_value[1] = 0;
    else
      // Add counters
      out_value[1] = ((base[1] + (in_value[1] + in_value[2] - 1)* add[1]) / (in_value[1] + in_value[2])) * (nb_panel*area_panel);
    end if;
    // Transfer flow rate
    out_value[2] = out_value[1];
  end if;

  annotation (Diagram(graphics), Icon(graphics={
        Polygon(
          points={{-100,100},{100,100},{100,-100},{-100,-100},{-100,100},{-100,100},
              {-100,100}},
          lineColor={0,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-98,54},{106,38}},
          lineColor={0,0,0},
          fontName="Consolas",
          textString="base : %base"),
        Text(
          extent={{-96,-38},{104,-54}},
          lineColor={0,0,0},
          fontName="Consolas",
          textString="add : %add")}),defaultComponentName = "splitter",
    Documentation(info="<html>
<p><br/><img src=\"modelica://SolarSystem/Images/equations/equation-2hqEDuou.png\" alt=\"out_value = ((base + (in_value - 1) * add) / (in_value)) * (area_panel * nb_panel)\"/></p>
</html>"));
end split_input;
