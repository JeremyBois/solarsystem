within SolarSystem.Control.Bloc.Utilities;
model Counter " : out_value = sum of true in in_value."
 parameter Integer n=2 "Number of inputs";
  Modelica.Blocks.Interfaces.BooleanInput in_value[n]
    annotation (Placement(transformation(extent={{-120,-20},{-80,20}}),
        iconTransformation(extent={{-120,-20},{-80,20}})));
  Integer count "How many true ?";

  Modelica.Blocks.Interfaces.IntegerOutput
                                        out_value
    annotation (Placement(transformation(extent={{80,-20},{120,20}}),
        iconTransformation(extent={{80,-20},{120,20}})));
algorithm
  count :=0;
  for i in (1:n) loop
    if in_value[i] then
      count := count + 1;
    end if;
  end for;

equation
  out_value = count;

  annotation (Diagram(coordinateSystem(extent={{-100,-100},{100,100}},
          preserveAspectRatio=true),
                      graphics), Icon(coordinateSystem(extent={{-100,
            -100},{100,100}}),        graphics={
        Text(
          extent={{-100,-32},{100,-70}},
          lineColor={0,0,0},
          textString="%n",
          fontName="Consolas"),
        Text(
          extent={{-100,52},{100,34}},
          lineColor={0,0,0},
          fontName="Consolas",
          textString="Input size"),
        Polygon(
          points={{-100,100},{100,100},{100,-100},{-100,-100},{-100,100},{-100,100},
              {-100,100}},
          lineColor={255,0,255},
          smooth=Smooth.None)}),                defaultComponentName = "counter",
    Documentation(info="<html>
<p>This&nbsp;model&nbsp;provides&nbsp;a&nbsp;simple&nbsp;way&nbsp;to&nbsp;count&nbsp;how&nbsp;many&nbsp;true&nbsp;in&nbsp;the in_value vector.</p>
<p>out_value = sum of true in in_value.</p>
</html>"));
end Counter;
