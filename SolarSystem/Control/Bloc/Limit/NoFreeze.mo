within SolarSystem.Control.Bloc.Limit;
model NoFreeze
 parameter Modelica.SIunits.Temperature Tmini=273.15;

  Modelica.Blocks.Logical.Switch modifier_switch
    annotation (Placement(transformation(extent={{34,-8},{50,8}})));
  Modelica.Blocks.Sources.RealExpression limit(y=Tmini) "limit"
    annotation (Placement(transformation(extent={{2,32},{18,48}})));
  Modelica.Blocks.Interfaces.RealInput Tcollector "T inside collector"
    annotation (Placement(transformation(extent={{-120,-80},{-80,-40}}),
        iconTransformation(extent={{-120,-80},{-80,-40}})));
  Modelica.Blocks.Interfaces.RealOutput Text_out annotation (Placement(
        transformation(extent={{80,-20},{120,20}}), iconTransformation(extent={{80,-20},
            {120,20}})));

  Modelica.Blocks.Interfaces.RealInput Text "Text" annotation (Placement(
        transformation(extent={{-120,30},{-80,70}}), iconTransformation(extent={{-120,40},
            {-80,80}})));
  Modelica.Blocks.Logical.And conditions
    annotation (Placement(transformation(extent={{4,-10},{24,10}})));
  Modelica.Blocks.Logical.LessThreshold      greaterThreshold(threshold=273.15)
    annotation (Placement(transformation(extent={{-50,40},{-30,60}})));
  Modelica.Blocks.Logical.Hysteresis    greaterThreshold1(uLow=273.15, uHigh=0.1
         + 273.15)
    annotation (Placement(transformation(extent={{-62,-70},{-42,-50}})));
  Modelica.Blocks.Logical.Not                greaterThreshold2
    annotation (Placement(transformation(extent={{-32,-70},{-12,-50}})));
equation
  connect(modifier_switch.y, Text_out) annotation (Line(
      points={{50.8,0},{100,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(conditions.y, modifier_switch.u2) annotation (Line(
      points={{25,0},{32.4,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(limit.y, modifier_switch.u1) annotation (Line(
      points={{18.8,40},{26,40},{26,6.4},{32.4,6.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text, modifier_switch.u3) annotation (Line(
      points={{-100,50},{-72,50},{-72,-20},{26,-20},{26,-6.4},{32.4,-6.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tcollector, greaterThreshold1.u) annotation (Line(
      points={{-100,-60},{-64,-60}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text, greaterThreshold.u) annotation (Line(
      points={{-100,50},{-52,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(greaterThreshold.y, conditions.u1) annotation (Line(
      points={{-29,50},{-4,50},{-4,0},{2,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(greaterThreshold1.y, greaterThreshold2.u) annotation (Line(
      points={{-41,-60},{-34,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(greaterThreshold2.y, conditions.u2) annotation (Line(
      points={{-11,-60},{-4,-60},{-4,-8},{2,-8}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                      graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
                                      graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,255},
          fillColor={170,255,255},
          fillPattern=FillPattern.Solid)}));
end NoFreeze;
