within SolarSystem.Control.Bloc.Limit;
model Overheat "Add complete shadow on collectors to avoid simulation crash"
 parameter Integer n=2 "Number of inputs";
 parameter Modelica.SIunits.Temp_K threshold[2]={363.15, 383.15}
    "Beside threshold[1] output = 0, above threshold[2] output = 1";

  Modelica.Blocks.Logical.Switch modifier_switch
    annotation (Placement(transformation(extent={{24,-8},{40,8}})));
  Modelica.Blocks.Sources.RealExpression coef(y=0) "Shadow"
    annotation (Placement(transformation(extent={{0,10},{16,26}})));
  Modelica.Blocks.Sources.RealExpression coef1(y=1) "No shadow"
    annotation (Placement(transformation(extent={{0,-26},{16,-10}})));
  Modelica.Blocks.Interfaces.RealInput in_value[n] "T1 T5 T4"
    annotation (Placement(transformation(extent={{-128,-20},{-88,20}}),
        iconTransformation(extent={{-128,-20},{-88,20}})));
  Modelica.Blocks.Interfaces.RealOutput out_value
    annotation (Placement(transformation(extent={{92,-20},{132,20}}),
        iconTransformation(extent={{92,-20},{132,20}})));
  True_seeker true_seeker(n=n)
    annotation (Placement(transformation(extent={{-20,-10},{0,10}})));
  Modelica.Blocks.Logical.Hysteresis       check[n](
    each uLow=threshold[1],
    each uHigh=threshold[2],
    each pre_y_start=false) "Check overheating"
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));

equation
  connect(coef.y, modifier_switch.u1) annotation (Line(
      points={{16.8,18},{20,18},{20,6.4},{22.4,6.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(coef1.y, modifier_switch.u3) annotation (Line(
      points={{16.8,-18},{20,-18},{20,-6.4},{22.4,-6.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(modifier_switch.y, out_value) annotation (Line(
      points={{40.8,0},{112,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(check.y, true_seeker.u) annotation (Line(
      points={{-39,0},{-20,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(true_seeker.y, modifier_switch.u2) annotation (Line(
      points={{0,0},{22.4,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(in_value, check.u) annotation (Line(
      points={{-108,0},{-62,0}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-100,-100},{100,100}}),
                      graphics), Icon(graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,255},
          fillColor={170,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-100,10},{100,-4}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString="Overheat ?",
          fontName="Consolas",
          textStyle={TextStyle.Bold})}));
end Overheat;
