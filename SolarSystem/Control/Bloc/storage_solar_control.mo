within SolarSystem.Control.Bloc;
model storage_solar_control
  import Assemblage;
  import SolarSystem;
  SolarSystem.Classes.Control.SolisArt_mod.SolarValve_mod solarValve_mod
    annotation (Placement(transformation(extent={{-10,58},{10,82}})));
  SolarSystem.Classes.Control.Bloc.Real_timer
                             real_timer5(pause=V3Vsolar_pause)
    annotation (Placement(transformation(extent={{-36,82},{-24,90}})));
  SolarSystem.Classes.Control.SolisArt_mod.storageTank_mod storageTank_mod(
      pumpControl_S6(start=true))
    annotation (Placement(transformation(extent={{-14,-4},{10,14}})));
  SolarSystem.Classes.Control.SolisArt_mod.solarTank_mod solarTank_mod(
      pumpControl_S5(start=false))
    annotation (Placement(transformation(extent={{-12,-80},{12,-56}})));
  Modelica.Blocks.Math.RealToBoolean booleanToReal
    annotation (Placement(transformation(extent={{-66,-82},{-56,-72}})));
  SolarSystem.Classes.Control.Bloc.Boolean_timer
                                boolean_timer(pause=pumpS5_pause)
    annotation (Placement(transformation(extent={{-34,-82},{-22,-72}})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-112,48},{-88,72}})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
    annotation (Placement(transformation(extent={{-112,18},{-88,42}}),
        iconTransformation(extent={{-112,18},{-88,42}})));
  SolarSystem.Classes.Control.Bloc.Real_timer
                             real_timer1(pause=pumpS6_pause)
    annotation (Placement(transformation(extent={{-36,-14},{-24,-6}})));
  SolarSystem.Classes.Control.Bloc.Real_timer
                             real_timer2(pause=V3Vsolar_pause)
    annotation (Placement(transformation(extent={{-36,52},{-24,60}})));
  SolarSystem.Classes.Control.Bloc.Real_timer
                             real_timer3(pause=pumpS6_pause)
    annotation (Placement(transformation(extent={{-36,10},{-24,18}})));
  SolarSystem.Classes.Control.Bloc.Real_timer
                             real_timer4(pause=pumpS6_pause)
    annotation (Placement(transformation(extent={{-36,-2},{-24,6}})));
  SolarSystem.Classes.Control.Bloc.Real_timer
                             real_timer6(pause=pumpS5_pause)
    annotation (Placement(transformation(extent={{-34,-70},{-22,-62}})));
  SolarSystem.Classes.Control.Bloc.Real_timer
                             real_timer7(pause=pumpS5_pause)
    annotation (Placement(transformation(extent={{-34,-60},{-22,-52}})));
  SolarSystem.Classes.Control.Bloc.Real_timer
                             real_timer8(pause=V3Vsolar_pause)
    annotation (Placement(transformation(extent={{-36,72},{-24,80}})));
  SolarSystem.Classes.Control.Bloc.Real_timer
                             real_timer9(pause=V3Vsolar_pause)
    annotation (Placement(transformation(extent={{-36,62},{-24,70}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-112,78},{-88,102}}),
        iconTransformation(extent={{-112,78},{-88,102}})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-112,-12},{-88,12}}),
        iconTransformation(extent={{-112,-12},{-88,12}})));
  Modelica.Blocks.Interfaces.RealInput solar_valve_pos
    "T. inside storage tank"
    annotation (Placement(transformation(extent={{-112,-82},{-88,-58}}),
        iconTransformation(extent={{-112,-82},{-88,-58}})));
  Modelica.Blocks.Interfaces.BooleanOutput V3V_solar(start=true)
    "Open to solar panels = true, Open to storage tank = false"
    annotation (Placement(transformation(extent={{100,50},{140,90}})));
  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S6(start=false)
    "ON = true, OFF = false (pump S6)"
    annotation (Placement(transformation(extent={{100,-20},{140,20}}),
        iconTransformation(extent={{100,-20},{140,20}})));
  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S5(start=true)
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{100,-90},{140,-50}}),
        iconTransformation(extent={{100,-90},{140,-50}})));
  parameter Modelica.SIunits.Time V3Vsolar_pause=200;
  parameter Modelica.SIunits.Time pumpS5_pause=200;
  parameter Modelica.SIunits.Time pumpS6_pause=200;
equation
  connect(booleanToReal.y, boolean_timer.in_value) annotation (Line(
      points={{-55.5,-77},{-34,-77}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(real_timer5.out_value, solarValve_mod.T1) annotation (Line(
      points={{-22.8,86},{-17,86},{-17,79},{-10,79}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(real_timer6.out_value, solarTank_mod.T3) annotation (Line(
      points={{-20.8,-66},{-17,-66},{-17,-65},{-12,-65}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(real_timer7.out_value, solarTank_mod.T1) annotation (Line(
      points={{-20.8,-56},{-18,-56},{-18,-61},{-12,-61}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(real_timer1.out_value, storageTank_mod.T3) annotation (Line(
      points={{-22.8,-10},{-20,-10},{-20,1},{-14,1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(real_timer4.out_value, storageTank_mod.T5) annotation (Line(
      points={{-22.8,2},{-20,2},{-20,5},{-14,5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(real_timer3.out_value, storageTank_mod.T1) annotation (Line(
      points={{-22.8,14},{-20,14},{-20,9},{-14,9}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(real_timer8.out_value, solarValve_mod.T3) annotation (Line(
      points={{-22.8,76},{-18,76},{-18,73},{-10,73}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(real_timer9.out_value, solarValve_mod.T4) annotation (Line(
      points={{-22.8,66},{-18,66},{-18,67},{-10,67}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(real_timer2.out_value, solarValve_mod.T5) annotation (Line(
      points={{-22.8,56},{-18,56},{-18,61},{-10,61}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solar_valve_pos, booleanToReal.u) annotation (Line(
      points={{-100,-70},{-82.5,-70},{-82.5,-77},{-67,-77}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, real_timer5.in_value) annotation (Line(
      points={{-100,90},{-58,90},{-58,86},{-36,86}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, real_timer8.in_value) annotation (Line(
      points={{-100,60},{-54,60},{-54,76},{-36,76}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, real_timer9.in_value) annotation (Line(
      points={{-100,30},{-50,30},{-50,66},{-36,66}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, real_timer2.in_value) annotation (Line(
      points={{-100,0},{-74,0},{-74,36},{-46,36},{-46,56},{-36,56}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, real_timer3.in_value) annotation (Line(
      points={{-100,90},{-58,90},{-58,14},{-36,14}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, real_timer4.in_value) annotation (Line(
      points={{-100,0},{-74,0},{-74,2},{-36,2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, real_timer1.in_value) annotation (Line(
      points={{-100,60},{-54,60},{-54,-10},{-36,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, real_timer7.in_value) annotation (Line(
      points={{-100,90},{-58,90},{-58,-56},{-34,-56}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, real_timer6.in_value) annotation (Line(
      points={{-100,60},{-54,60},{-54,-66},{-34,-66}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarTank_mod.pumpControl_S5, pumpControl_S5) annotation (
      Line(
      points={{12,-70},{120,-70}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(storageTank_mod.pumpControl_S6, pumpControl_S6) annotation (
      Line(
      points={{10,0},{120,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(boolean_timer.out_value, solarTank_mod.V3V_solar) annotation (
     Line(
      points={{-20.8,-77},{-16.4,-77},{-16.4,-75},{-12,-75}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(solarValve_mod.V3V_solar, V3V_solar) annotation (Line(
      points={{10,70},{56,70},{56,70},{120,70}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(graphics), Icon(graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={90,150,90},
          fillPattern=FillPattern.CrossDiag,
          fillColor={0,127,127}), Text(
          extent={{-70,74},{72,-60}},
          lineColor={0,0,0},
          fillColor={255,85,255},
          fillPattern=FillPattern.CrossDiag,
          textStyle={TextStyle.Bold},
          fontName="Consolas",
          textString="Control
   Solar pump
   Storage pump
   Solar valve",
          horizontalAlignment=TextAlignment.Left)}));
end storage_solar_control;
