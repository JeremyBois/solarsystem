within SolarSystem.Control.Bloc.Logical;
block Multiplex2_boolean "Multiplexer block for two input connectors"
  extends Modelica.Blocks.Interfaces.BlockIcon;
  parameter Integer n1=1 "dimension of input signal connector 1";
  parameter Integer n2=1 "dimension of input signal connector 2";
  Modelica.Blocks.Interfaces.BooleanInput
                                       u1[n1]
    "Connector of Real input signals 1" annotation (Placement(transformation(
          extent={{-140,40},{-100,80}}, rotation=0)));
  Modelica.Blocks.Interfaces.BooleanInput
                                       u2[n2]
    "Connector of Real input signals 2" annotation (Placement(transformation(
          extent={{-140,-80},{-100,-40}}, rotation=0)));
  Modelica.Blocks.Interfaces.BooleanOutput
                                        y[n1 + n2]
    "Connector of Real output signals" annotation (Placement(transformation(
          extent={{100,-10},{120,10}}, rotation=0)));

equation
  [y] = [u1; u2];
  annotation (
    Documentation(info="<HTML>
<p>
The output connector is the <b>concatenation</b> of the two input connectors.
Note, that the dimensions of the input connector signals have to be
explicitly defined via parameters n1 and n2.
</p>
</HTML>
"), Icon(coordinateSystem(
        preserveAspectRatio=true,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Line(points={{8,0},{102,0}}, color={127,0,127}),
        Ellipse(
          extent={{-14,16},{16,-14}},
          fillColor={127,0,127},
          fillPattern=FillPattern.Solid,
          lineColor={127,0,127}),
        Line(points={{-98,60},{-60,60},{-4,6}}, color={127,0,127}),
        Line(points={{-98,-60},{-60,-60},{-4,-4}}, color={127,0,127})}),
    Diagram(coordinateSystem(
        preserveAspectRatio=true,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Line(points={{-98,60},{-60,60},{-4,6}}, color={255,0,255}),
        Line(points={{-98,-60},{-60,-60},{-4,-4}}, color={255,0,255}),
        Line(points={{8,0},{102,0}}, color={255,0,255}),
        Ellipse(
          extent={{-14,16},{16,-14}},
          fillColor={255,0,255},
          fillPattern=FillPattern.Solid,
          lineColor={255,0,255})}));
end Multiplex2_boolean;
