within SolarSystem.Control.Bloc.Logical;
block BoolSwitch "Switch between two Boolean signals"
  extends Modelica.Blocks.Interfaces.partialBooleanBlockIcon;
  Modelica.Blocks.Interfaces.BooleanInput u1
    "Connector of first Boolean input signal"
                                 annotation (Placement(transformation(extent=
            {{-140,60},{-100,100}}, rotation=0)));
  Modelica.Blocks.Interfaces.BooleanInput u2
    "Connector of Boolean input signal"
                                    annotation (Placement(transformation(
          extent={{-140,-20},{-100,20}}, rotation=0)));
  Modelica.Blocks.Interfaces.BooleanInput u3
    "Connector of second Boolean input signal"
                                 annotation (Placement(transformation(extent=
            {{-140,-100},{-100,-60}}, rotation=0)));
  Modelica.Blocks.Interfaces.BooleanOutput y
    "Connector of Boolean output signal"
                                 annotation (Placement(transformation(extent=
            {{100,-10},{120,10}}, rotation=0)));

equation
  y = if u2 then u1 else u3;
  annotation (defaultComponentName="switch1",
    Documentation(info="<html>
<p>The Logical.Switch switches, depending on the
logical connector u2 (the middle connector)
between the two possible input signals
u1 (upper connector) and u3 (lower connector).</p>
<p>If u2 is <b>true</b>, the output signal y is set equal to
u1, else it is set equal to u3.</p>
</html>
"), Icon(coordinateSystem(
        preserveAspectRatio=true,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics={
        Line(
          points={{12,0},{100,0}},
          pattern=LinePattern.Solid,
          thickness=0.25,
          arrow={Arrow.None,Arrow.None},
          color={255,0,127}),
        Line(
          points={{-100,0},{-40,0}},
          color={255,0,127},
          pattern=LinePattern.Solid,
          thickness=0.25,
          arrow={Arrow.None,Arrow.None}),
        Line(
          points={{-100,-80},{-40,-80},{-40,-80}},
          pattern=LinePattern.Solid,
          thickness=0.25,
          arrow={Arrow.None,Arrow.None},
          color={255,0,127}),
        Line(points={{-40,12},{-40,-12}}, color={255,0,127}),
        Line(points={{-100,80},{-38,80}}, color={255,0,127}),
        Line(
          points={{-38,80},{6,2}},
          thickness=1,
          color={255,0,127}),
        Ellipse(
          extent={{2,8},{18,-6}},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          lineColor={0,0,255})}),
    Diagram(coordinateSystem(
        preserveAspectRatio=true,
        extent={{-100,-100},{100,100}},
        grid={2,2}), graphics));
end BoolSwitch;
