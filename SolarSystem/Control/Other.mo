within SolarSystem.Control;
package Other
  extends Modelica.Icons.VariantsPackage;
  model Scheduled_solarPanel_control1
    "Use input schedules and deltaT to manage solar panel flow rate"

  parameter Integer n=16 "Schedules' lenght"
  annotation(Dialog(connectorSizing=true));
  parameter Integer matrixSchedule[2, n] = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21; 0, 0, 0, 0, 0, 0, 0, 30, 30, 30, 30, 30, 0, 0, 0, 0]
      "Schedule time (hour, minute) (matrixSchedule columns must be equal to matrixFlowValue rows)";
  parameter Real matrixFlowValue[n] = {0.1, 0.1, 0.1, 0.12, 0.15, 0.18, 0.2, 0.25, 0.3, 0.25, 0.2, 0.18, 0.17, 0.15, 0.1, 0.1}
      "Scheduled flow (matrixFlowValue rows must be equal to matrixSchedule columns) ";

  parameter Real minGlobalRadiation = 80
      "Minimal radiation to feed solar panel with water [W/m2]";

  parameter Real hysteresisLow = 0
      "Minimal deltaT (T_out - T_in) value to turn off the pump";

  parameter Real hysteresisHight = 4
      "Minimal deltaT (T_out - T_in) value to turn on the pump";

  parameter Real flowElse = 0 "Solar flow out of schedule";

    Utilities.Other.Horloge clock(Heure_debut=0)
      "\"Cut time into hour, minute, seconde\""
      annotation (Placement(transformation(extent={{-400,190},{-334,262}})));
    Modelica.Blocks.Interfaces.RealInput T_in "T before object"
      annotation (Placement(transformation(extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-400,-30}), iconTransformation(extent={{-33,-33},{33,33}},
            origin={-367,47})));
    Modelica.Blocks.Interfaces.RealInput T_out "T after object"
      annotation (Placement(transformation(extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-400,-70}), iconTransformation(extent={{-32,-32},{32,32}},
            origin={-368,-52})));
    Modelica.Blocks.Logical.Hysteresis enoughCold(uLow=hysteresisLow, uHigh=
          hysteresisHight,
      y(start=false))
      annotation (Placement(transformation(extent={{-320,-60},{-300,-40}})));
    Modelica.Blocks.Math.MultiSum deltaPanel(k={-1,1}, nu=2)
      annotation (Placement(transformation(extent={{-358,-60},{-338,-40}})));

    Modelica.Blocks.Logical.LessThreshold lessiH_mini(threshold=
          matrixSchedule[1, 1])
      annotation (Placement(transformation(extent={{-174,298},{-152,320}})));
    Modelica.Blocks.Logical.GreaterThreshold greaterMin_mini(threshold=
          matrixSchedule[2, 1])
      annotation (Placement(transformation(extent={{-200,260},{-180,280}})));
    Modelica.Blocks.Logical.LogicalSwitch beforeSchedules
      annotation (Placement(transformation(extent={{-122,256},{-94,284}})));
    Modelica.Blocks.Logical.LessEqualThreshold lessEqualH_mini(threshold=
          matrixSchedule[1, 1])
      annotation (Placement(transformation(extent={{-172,220},{-152,240}})));
    Modelica.Blocks.Logical.GreaterThreshold greaterH_max(threshold=
          matrixSchedule[1, n])
      annotation (Placement(transformation(extent={{-172,180},{-152,200}})));
    Modelica.Blocks.Logical.LessThreshold greaterMin_max(threshold=
          matrixSchedule[2, n])
      annotation (Placement(transformation(extent={{-200,140},{-180,160}})));
    Modelica.Blocks.Logical.LogicalSwitch afterSchedules
      annotation (Placement(transformation(extent={{-122,136},{-94,164}})));
    Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualH_max(threshold=
         matrixSchedule[1, n])
      annotation (Placement(transformation(extent={{-172,100},{-152,120}})));
    Modelica.Blocks.Logical.Or beforeOrAfter "False if outside schedules"
      annotation (Placement(transformation(extent={{-56,196},{-28,224}})));
    Modelica.Blocks.Logical.Not insideSchedule "false becomes true "
      annotation (Placement(transformation(extent={{12,192},{48,228}})));
    Modelica.Blocks.Logical.And allowFlow
      "( enoughSun or EnoughCold ) and Inside schedule"
      annotation (Placement(transformation(extent={{70,86},{108,124}})));
    Modelica.Blocks.Math.IntegerToReal integerToReal "int(H) ----> real(H)"
      annotation (Placement(transformation(extent={{-300,240},{-278,262}})));
    Modelica.Blocks.Math.IntegerToReal integerToReal1
      "int(min) ---> float(min)"
      annotation (Placement(transformation(extent={{-300,198},{-276,222}})));
    Modelica.Blocks.Interfaces.RealOutput flowRate(start=0) "[Kg/s]"
      annotation (Placement(transformation(extent={{142,68},{214,140}}),
          iconTransformation(extent={{118,44},{214,140}})));

    Modelica.Blocks.Logical.Hysteresis stopCondition(
      y(start=false),
      uLow=273.15 + 90,
      uHigh=273.15 + 110)
      annotation (Placement(transformation(extent={{-194,-88},{-158,-52}})));
    Modelica.Blocks.Logical.And conditions
      "( enoughSun or EnoughCold ) and Inside schedule"
      annotation (Placement(transformation(extent={{10,-34},{48,4}})));
    Modelica.Blocks.Logical.Not NotStopCondition "false becomes true "
      annotation (Placement(transformation(extent={{-126,-88},{-90,-52}})));
  algorithm
    for i in 1:size(matrixFlowValue, 1) loop

      // Flow rate inside the object is allowed
      if allowFlow.y then

        // We reach a schedule time ---> adjust the flow rate
        if clock.H == matrixSchedule[1,i] and clock.min == matrixSchedule[2,i] then
           flowRate :=matrixFlowValue[i];

        // Keep actual flow rate until we reach an other schedule time
        else
          flowRate :=flowRate;
        end if;

      // Out of schedule or (no enough sun and no enough cold) ---> default flow rate
      else
         flowRate :=flowElse;
      end if;

    end for;

  equation
    connect(deltaPanel.y, enoughCold.u) annotation (Line(
        points={{-336.3,-50},{-322,-50}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T_in, deltaPanel.u[1]) annotation (Line(
        points={{-400,-30},{-380,-30},{-380,-46.5},{-358,-46.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T_out, deltaPanel.u[2]) annotation (Line(
        points={{-400,-70},{-378,-70},{-378,-53.5},{-358,-53.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(greaterMin_mini.y, beforeSchedules.u2) annotation (Line(
        points={{-179,270},{-124.8,270}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(lessiH_mini.y, beforeSchedules.u1)  annotation (Line(
        points={{-150.9,309},{-132,309},{-132,281.2},{-124.8,281.2}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(lessEqualH_mini.y, beforeSchedules.u3)  annotation (Line(
        points={{-151,230},{-132,230},{-132,258.8},{-124.8,258.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(greaterMin_max.y, afterSchedules.u2)    annotation (Line(
        points={{-179,150},{-124.8,150}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(greaterH_max.y, afterSchedules.u1)   annotation (Line(
        points={{-151,190},{-132,190},{-132,161.2},{-124.8,161.2}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(greaterEqualH_max.y, afterSchedules.u3)   annotation (Line(
        points={{-151,110},{-132,110},{-132,138.8},{-124.8,138.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(beforeSchedules.y, beforeOrAfter.u1)
                                     annotation (Line(
        points={{-92.6,270},{-84,270},{-84,210},{-58.8,210}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(afterSchedules.y, beforeOrAfter.u2)
                                      annotation (Line(
        points={{-92.6,150},{-84,150},{-84,198.8},{-58.8,198.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(beforeOrAfter.y, insideSchedule.u)
                           annotation (Line(
        points={{-26.6,210},{8.4,210}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(insideSchedule.y, allowFlow.u1)
                             annotation (Line(
        points={{49.8,210},{58,210},{58,105},{66.2,105}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(clock.H, integerToReal.u) annotation (Line(
        points={{-340.6,244},{-320,244},{-320,251},{-302.2,251}},
        color={255,127,0},
        smooth=Smooth.None));
    connect(clock.min, integerToReal1.u) annotation (Line(
        points={{-340.6,229.6},{-319.3,229.6},{-319.3,210},{-302.4,210}},
        color={255,127,0},
        smooth=Smooth.None));

    connect(integerToReal1.y, greaterMin_mini.u)   annotation (Line(
        points={{-274.8,210},{-238,210},{-238,270},{-202,270}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, lessEqualH_mini.u)    annotation (Line(
        points={{-276.9,251},{-254,251},{-254,230},{-174,230}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal1.y, greaterMin_max.u)    annotation (Line(
        points={{-274.8,210},{-238,210},{-238,150},{-202,150}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, greaterH_max.u)   annotation (Line(
        points={{-276.9,251},{-254,251},{-254,190},{-174,190}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, greaterEqualH_max.u)   annotation (Line(
        points={{-276.9,251},{-254,251},{-254,110},{-174,110}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, lessiH_mini.u)    annotation (Line(
        points={{-276.9,251},{-254,251},{-254,309},{-176.2,309}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T_out, stopCondition.u) annotation (Line(
        points={{-400,-70},{-197.6,-70}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(conditions.y, allowFlow.u2) annotation (Line(
        points={{49.9,-15},{58,-15},{58,89.8},{66.2,89.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(stopCondition.y, NotStopCondition.u) annotation (Line(
        points={{-156.2,-70},{-129.6,-70}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(NotStopCondition.y, conditions.u2) annotation (Line(
        points={{-88.2,-70},{-56,-70},{-56,-30.2},{6.2,-30.2}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(enoughCold.y, conditions.u1) annotation (Line(
        points={{-299,-50},{-216,-50},{-216,-15},{6.2,-15}},
        color={255,0,255},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-400,-120},{180,320}},
            preserveAspectRatio=true), graphics), Icon(coordinateSystem(extent={{-400,
              -120},{180,320}}, preserveAspectRatio=true), graphics));
  end Scheduled_solarPanel_control1;

  model Scheduled_solarPanel_control2
    "Use input schedules and weather data informations to manage solar panel flow rate"

  parameter Integer n=16 "Schedules' lenght"
  annotation(Dialog(connectorSizing=true));
  parameter Integer matrixSchedule[2, n] = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21; 0, 0, 0, 0, 0, 0, 0, 30, 30, 30, 30, 30, 0, 0, 0, 0]
      "Schedule time (hour, minute) (matrixSchedule columns must be equal to matrixFlowValue rows)";
  parameter Real matrixFlowValue[n] = {0.1, 0.1, 0.1, 0.12, 0.15, 0.18, 0.2, 0.25, 0.3, 0.25, 0.2, 0.18, 0.17, 0.15, 0.1, 0.1}
      "Scheduled flow (matrixFlowValue rows must be equal to matrixSchedule columns) ";

  parameter Real minGlobalRadiation = 80
      "Minimal radiation to feed solar panel with water [W/m2]";

  parameter Real hysteresisLow = 0
      "Minimal deltaT (T_out - T_in) value to turn off the pump";

  parameter Real hysteresisHight = 4
      "Minimal deltaT (T_out - T_in) value to turn on the pump";

   parameter Modelica.SIunits.Duration It=60
      "need minutes ? (set to 60), need only hours or more accurate compilation ? (set to 3600)";

  parameter Real flowElse = 0 "Solar flow out of schedule";

    Utilities.Other.Horloge clock(Heure_debut=0)
      "\"Cut time into hour, minute, seconde\""
      annotation (Placement(transformation(extent={{-400,190},{-334,262}})));
    Modelica.Blocks.Interfaces.RealInput dirRad "Direct radiation"
      annotation (Placement(transformation(extent={{-420,50},{-380,90}}),
          iconTransformation(extent={{-400,236},{-334,302}})));
    Modelica.Blocks.Interfaces.RealInput difRad "Diffuse radiation"
      annotation (Placement(transformation(extent={{-420,10},{-380,50}}),
          iconTransformation(extent={{-400,152},{-334,218}})));
    Modelica.Blocks.Math.MultiSum globalRadiation(      k={1,1}, nu=2)
      annotation (Placement(transformation(extent={{-288,44},{-268,64}})));
    Modelica.Blocks.Logical.GreaterThreshold enoughSun(threshold=
          minGlobalRadiation)
      annotation (Placement(transformation(extent={{-248,44},{-228,64}})));

    Modelica.Blocks.Logical.LessThreshold lessiH_mini(threshold=
          matrixSchedule[1, 1])
      annotation (Placement(transformation(extent={{-174,298},{-152,320}})));
    Modelica.Blocks.Logical.GreaterThreshold greaterMin_mini(threshold=
          matrixSchedule[2, 1])
      annotation (Placement(transformation(extent={{-200,260},{-180,280}})));
    Modelica.Blocks.Logical.LogicalSwitch beforeSchedules
      annotation (Placement(transformation(extent={{-122,256},{-94,284}})));
    Modelica.Blocks.Logical.LessEqualThreshold lessEqualH_mini(threshold=
          matrixSchedule[1, 1])
      annotation (Placement(transformation(extent={{-172,220},{-152,240}})));
    Modelica.Blocks.Logical.GreaterThreshold greaterH_max(threshold=
          matrixSchedule[1, n])
      annotation (Placement(transformation(extent={{-172,180},{-152,200}})));
    Modelica.Blocks.Logical.LessThreshold greaterMin_max(threshold=
          matrixSchedule[2, n])
      annotation (Placement(transformation(extent={{-200,140},{-180,160}})));
    Modelica.Blocks.Logical.LogicalSwitch afterSchedules
      annotation (Placement(transformation(extent={{-122,136},{-94,164}})));
    Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualH_max(threshold=
         matrixSchedule[1, n])
      annotation (Placement(transformation(extent={{-172,100},{-152,120}})));
    Modelica.Blocks.Logical.Or beforeOrAfter "False if outside schedules"
      annotation (Placement(transformation(extent={{-56,196},{-28,224}})));
    Modelica.Blocks.Logical.Not insideSchedule "false becomes true "
      annotation (Placement(transformation(extent={{12,192},{48,228}})));
    Modelica.Blocks.Logical.And allowFlow
      "( enoughSun or EnoughCold ) and Inside schedule"
      annotation (Placement(transformation(extent={{70,86},{108,124}})));
    Modelica.Blocks.Math.IntegerToReal integerToReal "int(H) ----> real(H)"
      annotation (Placement(transformation(extent={{-300,240},{-278,262}})));
    Modelica.Blocks.Math.IntegerToReal integerToReal1
      "int(min) ---> float(min)"
      annotation (Placement(transformation(extent={{-300,198},{-276,222}})));
    Modelica.Blocks.Interfaces.RealOutput flowRate(start=0) "[Kg/s]"
      annotation (Placement(transformation(extent={{142,68},{214,140}}),
          iconTransformation(extent={{118,44},{214,140}})));

  public
    Utilities.Other.ZeroOrder sample1(t0=0, It=It)
      "It must be a multiple of data time step"
      annotation (Placement(transformation(extent={{-346,62},{-320,78}})));
    Utilities.Other.ZeroOrder sample2(t0=0, It=It)
      "It must be a multiple of data time step"
      annotation (Placement(transformation(extent={{-346,22},{-320,38}})));
  algorithm
    for i in 1:size(matrixFlowValue, 1) loop

      // Flow rate inside the object is allowed
      if allowFlow.y then

        // We reach a schedule time ---> adjust the flow rate
        if clock.H == matrixSchedule[1,i] and clock.min == matrixSchedule[2,i] then
           flowRate :=matrixFlowValue[i];

        // Keep actual flow rate until we reach an other schedule time
        else
          flowRate :=flowRate;
        end if;

      // Out of schedule or (no enough sun and no enough cold) ---> default flow rate
      else
         flowRate :=flowElse;
      end if;

    end for;

  equation
    connect(globalRadiation.y, enoughSun.u) annotation (Line(
        points={{-266.3,54},{-250,54}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(greaterMin_mini.y, beforeSchedules.u2) annotation (Line(
        points={{-179,270},{-124.8,270}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(lessiH_mini.y, beforeSchedules.u1)  annotation (Line(
        points={{-150.9,309},{-132,309},{-132,281.2},{-124.8,281.2}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(lessEqualH_mini.y, beforeSchedules.u3)  annotation (Line(
        points={{-151,230},{-132,230},{-132,258.8},{-124.8,258.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(greaterMin_max.y, afterSchedules.u2)    annotation (Line(
        points={{-179,150},{-124.8,150}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(greaterH_max.y, afterSchedules.u1)   annotation (Line(
        points={{-151,190},{-132,190},{-132,161.2},{-124.8,161.2}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(greaterEqualH_max.y, afterSchedules.u3)   annotation (Line(
        points={{-151,110},{-132,110},{-132,138.8},{-124.8,138.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(beforeSchedules.y, beforeOrAfter.u1)
                                     annotation (Line(
        points={{-92.6,270},{-84,270},{-84,210},{-58.8,210}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(afterSchedules.y, beforeOrAfter.u2)
                                      annotation (Line(
        points={{-92.6,150},{-84,150},{-84,198.8},{-58.8,198.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(beforeOrAfter.y, insideSchedule.u)
                           annotation (Line(
        points={{-26.6,210},{8.4,210}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(insideSchedule.y, allowFlow.u1)
                             annotation (Line(
        points={{49.8,210},{58,210},{58,105},{66.2,105}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(clock.H, integerToReal.u) annotation (Line(
        points={{-340.6,244},{-320,244},{-320,251},{-302.2,251}},
        color={255,127,0},
        smooth=Smooth.None));
    connect(clock.min, integerToReal1.u) annotation (Line(
        points={{-340.6,229.6},{-319.3,229.6},{-319.3,210},{-302.4,210}},
        color={255,127,0},
        smooth=Smooth.None));

    connect(integerToReal1.y, greaterMin_mini.u)   annotation (Line(
        points={{-274.8,210},{-238,210},{-238,270},{-202,270}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, lessEqualH_mini.u)    annotation (Line(
        points={{-276.9,251},{-254,251},{-254,230},{-174,230}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal1.y, greaterMin_max.u)    annotation (Line(
        points={{-274.8,210},{-238,210},{-238,150},{-202,150}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, greaterH_max.u)   annotation (Line(
        points={{-276.9,251},{-254,251},{-254,190},{-174,190}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, greaterEqualH_max.u)   annotation (Line(
        points={{-276.9,251},{-254,251},{-254,110},{-174,110}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, lessiH_mini.u)    annotation (Line(
        points={{-276.9,251},{-254,251},{-254,309},{-176.2,309}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(sample1.y, globalRadiation.u[1]) annotation (Line(
        points={{-320.52,70},{-304,70},{-304,57.5},{-288,57.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(sample2.y, globalRadiation.u[2]) annotation (Line(
        points={{-320.52,30},{-305.24,30},{-305.24,50.5},{-288,50.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(sample2.u, difRad) annotation (Line(
        points={{-346,30},{-400,30}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(sample1.u, dirRad) annotation (Line(
        points={{-346,70},{-400,70}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(enoughSun.y, allowFlow.u2) annotation (Line(
        points={{-227,54},{58,54},{58,89.8},{66.2,89.8}},
        color={255,0,255},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-400,-120},{180,320}},
            preserveAspectRatio=true), graphics), Icon(coordinateSystem(extent={{-400,
              -120},{180,320}}, preserveAspectRatio=true), graphics));
  end Scheduled_solarPanel_control2;

  model Scheduled_solarPanel_control3
    "Use input schedules, deltaT and weather data informations to manage solar panel flow rate"

  parameter Integer n=16 "Schedules' lenght"
  annotation(Dialog(connectorSizing=true));
  parameter Integer matrixSchedule[2, n] = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21; 0, 0, 0, 0, 0, 0, 0, 30, 30, 30, 30, 30, 0, 0, 0, 0]
      "Schedule time (hour, minute) (matrixSchedule columns must be equal to matrixFlowValue rows)";
  parameter Real matrixFlowValue[n] = {0.1, 0.1, 0.1, 0.12, 0.15, 0.18, 0.2, 0.25, 0.3, 0.25, 0.2, 0.18, 0.17, 0.15, 0.1, 0.1}
      "Scheduled flow (matrixFlowValue rows must be equal to matrixSchedule columns) ";

  parameter Real minGlobalRadiation = 80
      "Minimal radiation to feed solar panel with water [W/m2]";

  parameter Real hysteresisLow = 0
      "Minimal deltaT (T_out - T_in) value to turn off the pump";

  parameter Real hysteresisHight = 4
      "Minimal deltaT (T_out - T_in) value to turn on the pump";

   parameter Modelica.SIunits.Duration It=60
      "need minutes ? (set to 60), need only hours or more accurate compilation ? (set to 3600)";

  parameter Real flowElse = 0 "Solar flow out of schedule";

  Integer indice
  annotation(Dialog(connectorSizing=true));

    Utilities.Other.Horloge clock(Heure_debut=0)
      "\"Cut time into hour, minute, seconde\""
      annotation (Placement(transformation(extent={{-400,190},{-334,262}})));
    Modelica.Blocks.Interfaces.RealInput dirRad "Direct radiation"
      annotation (Placement(transformation(extent={{-420,50},{-380,90}}),
          iconTransformation(extent={{-400,236},{-334,302}})));
    Modelica.Blocks.Interfaces.RealInput difRad "Diffuse radiation"
      annotation (Placement(transformation(extent={{-420,10},{-380,50}}),
          iconTransformation(extent={{-400,152},{-334,218}})));
    Modelica.Blocks.Interfaces.RealInput T_in "T before object"
      annotation (Placement(transformation(extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-400,-30}), iconTransformation(extent={{-33,-33},{33,33}},
            origin={-367,47})));
    Modelica.Blocks.Interfaces.RealInput T_out "T after object"
      annotation (Placement(transformation(extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-400,-70}), iconTransformation(extent={{-32,-32},{32,32}},
            origin={-368,-52})));
    Modelica.Blocks.Logical.Hysteresis enoughCold(uLow=hysteresisLow, uHigh=
          hysteresisHight,
      y(start=false))
      annotation (Placement(transformation(extent={{-320,-60},{-300,-40}})));
    Modelica.Blocks.Math.MultiSum globalRadiation(      k={1,1}, nu=2)
      annotation (Placement(transformation(extent={{-288,44},{-268,64}})));
    Modelica.Blocks.Logical.Or startConditions "EnoughSun or EnoughCold"
      annotation (Placement(transformation(extent={{-152,-4},{-116,32}})));
    Modelica.Blocks.Logical.GreaterThreshold enoughSun(threshold=
          minGlobalRadiation)
      annotation (Placement(transformation(extent={{-248,44},{-228,64}})));

    Modelica.Blocks.Logical.LessThreshold lessiH_mini(threshold=
          matrixSchedule[1, 1])
      annotation (Placement(transformation(extent={{-174,298},{-152,320}})));
    Modelica.Blocks.Logical.GreaterThreshold greaterMin_mini(threshold=
          matrixSchedule[2, 1])
      annotation (Placement(transformation(extent={{-200,260},{-180,280}})));
    Modelica.Blocks.Logical.LogicalSwitch beforeSchedules
      annotation (Placement(transformation(extent={{-122,256},{-94,284}})));
    Modelica.Blocks.Logical.LessEqualThreshold lessEqualH_mini(threshold=
          matrixSchedule[1, 1])
      annotation (Placement(transformation(extent={{-172,220},{-152,240}})));
    Modelica.Blocks.Logical.GreaterThreshold greaterH_max(threshold=
          matrixSchedule[1, n])
      annotation (Placement(transformation(extent={{-172,180},{-152,200}})));
    Modelica.Blocks.Logical.LessThreshold greaterMin_max(threshold=
          matrixSchedule[2, n])
      annotation (Placement(transformation(extent={{-200,140},{-180,160}})));
    Modelica.Blocks.Logical.LogicalSwitch afterSchedules
      annotation (Placement(transformation(extent={{-122,136},{-94,164}})));
    Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualH_max(threshold=
         matrixSchedule[1, n])
      annotation (Placement(transformation(extent={{-172,100},{-152,120}})));
    Modelica.Blocks.Logical.Or beforeOrAfter "False if outside schedules"
      annotation (Placement(transformation(extent={{-56,196},{-28,224}})));
    Modelica.Blocks.Logical.Not insideSchedule "false becomes true "
      annotation (Placement(transformation(extent={{12,192},{48,228}})));
    Modelica.Blocks.Logical.And allowFlow
      "( enoughSun or EnoughCold ) and Inside schedule"
      annotation (Placement(transformation(extent={{70,86},{108,124}})));
    Modelica.Blocks.Math.IntegerToReal integerToReal "int(H) ----> real(H)"
      annotation (Placement(transformation(extent={{-300,240},{-278,262}})));
    Modelica.Blocks.Math.IntegerToReal integerToReal1
      "int(min) ---> float(min)"
      annotation (Placement(transformation(extent={{-300,198},{-276,222}})));
    Modelica.Blocks.Interfaces.RealOutput flowRate(start=0) "[Kg/s]"
      annotation (Placement(transformation(extent={{142,68},{214,140}}),
          iconTransformation(extent={{118,44},{214,140}})));

    Modelica.Blocks.Logical.Hysteresis stopCondition(
      y(start=false),
      uLow=273.15 + 90,
      uHigh=273.15 + 110)
      annotation (Placement(transformation(extent={{-194,-88},{-158,-52}})));
    Modelica.Blocks.Logical.And conditions
      "( enoughSun or EnoughCold ) and Inside schedule"
      annotation (Placement(transformation(extent={{10,-34},{48,4}})));
    Modelica.Blocks.Logical.Not NotStopCondition "false becomes true "
      annotation (Placement(transformation(extent={{-126,-88},{-90,-52}})));
    Modelica.Blocks.Math.MultiSum deltaPanel(k={-1,1}, nu=2)
      annotation (Placement(transformation(extent={{-358,-60},{-338,-40}})));
  public
    Utilities.Other.ZeroOrder sample1(t0=0, It=It)
      "It must be a multiple of data time step"
      annotation (Placement(transformation(extent={{-360,62},{-334,78}})));
    Utilities.Other.ZeroOrder sample2(t0=0, It=It)
      "It must be a multiple of data time step"
      annotation (Placement(transformation(extent={{-360,22},{-334,38}})));
  algorithm
    for i in 1:n loop

      // Flow rate inside the object is allowed
      if allowFlow.y then

        // We reach a schedule time ---> adjust the flow rate
        if clock.H == matrixSchedule[1,i] and clock.min == matrixSchedule[2,i] then
           flowRate :=matrixFlowValue[i];
           indice := i;

        // Keep actual flow rate until we reach an other schedule time
        else

           // Why i have to do this ????
           if indice > n then
             indice :=n;
           elseif indice < 1 then
             indice :=1;
           end if;
           // Why ??

          flowRate := matrixFlowValue[indice];
          //flowRate := flowRate;
          //indice :=indice;
        end if;

      // Out of schedule or (no enough sun and no enough cold) ---> default flow rate
      else
         flowRate :=flowElse;
      end if;

    end for;

  equation
    connect(globalRadiation.y, enoughSun.u) annotation (Line(
        points={{-266.3,54},{-250,54}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(greaterMin_mini.y, beforeSchedules.u2) annotation (Line(
        points={{-179,270},{-124.8,270}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(lessiH_mini.y, beforeSchedules.u1)  annotation (Line(
        points={{-150.9,309},{-132,309},{-132,281.2},{-124.8,281.2}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(lessEqualH_mini.y, beforeSchedules.u3)  annotation (Line(
        points={{-151,230},{-132,230},{-132,258.8},{-124.8,258.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(greaterMin_max.y, afterSchedules.u2)    annotation (Line(
        points={{-179,150},{-124.8,150}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(greaterH_max.y, afterSchedules.u1)   annotation (Line(
        points={{-151,190},{-132,190},{-132,161.2},{-124.8,161.2}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(greaterEqualH_max.y, afterSchedules.u3)   annotation (Line(
        points={{-151,110},{-132,110},{-132,138.8},{-124.8,138.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(beforeSchedules.y, beforeOrAfter.u1)
                                     annotation (Line(
        points={{-92.6,270},{-84,270},{-84,210},{-58.8,210}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(afterSchedules.y, beforeOrAfter.u2)
                                      annotation (Line(
        points={{-92.6,150},{-84,150},{-84,198.8},{-58.8,198.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(beforeOrAfter.y, insideSchedule.u)
                           annotation (Line(
        points={{-26.6,210},{8.4,210}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(enoughSun.y, startConditions.u1)
                                       annotation (Line(
        points={{-227,54},{-220,54},{-220,14},{-155.6,14}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(enoughCold.y, startConditions.u2)
                                        annotation (Line(
        points={{-299,-50},{-220,-50},{-220,-0.4},{-155.6,-0.4}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(insideSchedule.y, allowFlow.u1)
                             annotation (Line(
        points={{49.8,210},{58,210},{58,105},{66.2,105}},
        color={255,0,255},
        smooth=Smooth.None));

    connect(integerToReal1.y, greaterMin_mini.u)   annotation (Line(
        points={{-274.8,210},{-238,210},{-238,270},{-202,270}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, lessEqualH_mini.u)    annotation (Line(
        points={{-276.9,251},{-254,251},{-254,230},{-174,230}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal1.y, greaterMin_max.u)    annotation (Line(
        points={{-274.8,210},{-238,210},{-238,150},{-202,150}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, greaterH_max.u)   annotation (Line(
        points={{-276.9,251},{-254,251},{-254,190},{-174,190}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, greaterEqualH_max.u)   annotation (Line(
        points={{-276.9,251},{-254,251},{-254,110},{-174,110}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integerToReal.y, lessiH_mini.u)    annotation (Line(
        points={{-276.9,251},{-254,251},{-254,309},{-176.2,309}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(startConditions.y, conditions.u1) annotation (Line(
        points={{-114.2,14},{-56,14},{-56,-15},{6.2,-15}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(conditions.y, allowFlow.u2) annotation (Line(
        points={{49.9,-15},{58,-15},{58,89.8},{66.2,89.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(stopCondition.y, NotStopCondition.u) annotation (Line(
        points={{-156.2,-70},{-129.6,-70}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(NotStopCondition.y, conditions.u2) annotation (Line(
        points={{-88.2,-70},{-56,-70},{-56,-30.2},{6.2,-30.2}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(deltaPanel.y, enoughCold.u) annotation (Line(
        points={{-336.3,-50},{-322,-50}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T_in, deltaPanel.u[1]) annotation (Line(
        points={{-400,-30},{-380,-30},{-380,-46.5},{-358,-46.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T_out, deltaPanel.u[2]) annotation (Line(
        points={{-400,-70},{-378,-70},{-378,-53.5},{-358,-53.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T_out, stopCondition.u) annotation (Line(
        points={{-400,-70},{-197.6,-70}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(clock.H, integerToReal.u) annotation (Line(
        points={{-340.6,244},{-322,244},{-322,251},{-302.2,251}},
        color={255,127,0},
        smooth=Smooth.None));
    connect(clock.min, integerToReal1.u) annotation (Line(
        points={{-340.6,229.6},{-322.3,229.6},{-322.3,210},{-302.4,210}},
        color={255,127,0},
        smooth=Smooth.None));
    connect(dirRad, sample1.u) annotation (Line(
        points={{-400,70},{-360,70}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(difRad, sample2.u) annotation (Line(
        points={{-400,30},{-380,30},{-380,30},{-360,30}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(sample1.y, globalRadiation.u[1]) annotation (Line(
        points={{-334.52,70},{-312,70},{-312,57.5},{-288,57.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(sample2.y, globalRadiation.u[2]) annotation (Line(
        points={{-334.52,30},{-312,30},{-312,50.5},{-288,50.5}},
        color={0,0,127},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-400,-120},{180,320}},
            preserveAspectRatio=true), graphics), Icon(coordinateSystem(extent={{-400,
              -120},{180,320}}, preserveAspectRatio=true), graphics));
  end Scheduled_solarPanel_control3;

  model Scheduled_solarPanel_control_noSample
    "Use input schedules, deltaT and weather data informations to manage solar panel flow rate "

  parameter Real minGlobalRadiation = 80
      "Minimal radiation to feed solar panel with water [W/m2]";

  parameter Real hysteresisLow = 0
      "Minimal deltaT (T_out - T_in) value to turn off the pump";

  parameter Real hysteresisHight = 4
      "Minimal deltaT (T_out - T_in) value to turn on the pump";

  parameter Real flowElse = 0.0000005 "Solar flow out of schedule";

  discrete Real Tjour;

    Modelica.Blocks.Interfaces.RealInput dirRad "Direct radiation"
      annotation (Placement(transformation(extent={{-420,50},{-380,90}}),
          iconTransformation(extent={{-400,236},{-334,302}})));
    Modelica.Blocks.Interfaces.RealInput difRad "Diffuse radiation"
      annotation (Placement(transformation(extent={{-420,10},{-380,50}}),
          iconTransformation(extent={{-400,152},{-334,218}})));
    Modelica.Blocks.Interfaces.RealInput T_in "T before object"
      annotation (Placement(transformation(extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-400,-30}), iconTransformation(extent={{-33,-33},{33,33}},
            origin={-367,47})));
    Modelica.Blocks.Interfaces.RealInput T_out "T after object"
      annotation (Placement(transformation(extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-400,-70}), iconTransformation(extent={{-32,-32},{32,32}},
            origin={-368,-52})));
    Modelica.Blocks.Logical.Hysteresis enoughCold(uLow=hysteresisLow, uHigh=
          hysteresisHight,
      y(start=false))
      annotation (Placement(transformation(extent={{-320,-60},{-300,-40}})));
    Modelica.Blocks.Math.MultiSum globalRadiation(      k={1,1}, nu=2)
      annotation (Placement(transformation(extent={{-288,44},{-268,64}})));
    Modelica.Blocks.Logical.Or startConditions "EnoughSun or EnoughCold"
      annotation (Placement(transformation(extent={{-152,-4},{-116,32}})));
    Modelica.Blocks.Logical.GreaterThreshold enoughSun(threshold=
          minGlobalRadiation)
      annotation (Placement(transformation(extent={{-248,44},{-228,64}})));

    Modelica.Blocks.Interfaces.RealOutput flowRate(start=0) "[Kg/s]"
      annotation (Placement(transformation(extent={{142,68},{214,140}}),
          iconTransformation(extent={{118,44},{214,140}})));

    Modelica.Blocks.Logical.Hysteresis stopCondition(
      y(start=false),
      uLow=273.15 + 90,
      uHigh=273.15 + 110)
      annotation (Placement(transformation(extent={{-194,-88},{-158,-52}})));
    Modelica.Blocks.Logical.And allowFlow
      "( enoughSun or EnoughCold ) and Inside schedule"
      annotation (Placement(transformation(extent={{-186,132},{-148,170}})));
    Modelica.Blocks.Logical.Not NotStopCondition "false becomes true "
      annotation (Placement(transformation(extent={{-126,-88},{-90,-52}})));
    Modelica.Blocks.Math.MultiSum deltaPanel(k={-1,1}, nu=2)
      annotation (Placement(transformation(extent={{-358,-60},{-338,-40}})));
    Modelica.Blocks.Tables.CombiTable1D combiTable1D(
      tableOnFile=tableOnFile,
      table=table,
      fileName=fileName)
      annotation (Placement(transformation(extent={{-198,202},{-138,262}})));
    Modelica.Blocks.Sources.RealExpression minOfDay(y=(time - Tjour)/3600)
      "Transform time in secondes to hours"
      annotation (Placement(transformation(extent={{-340,218},{-270,260}})));

    Modelica.Blocks.Logical.Switch switch1
      annotation (Placement(transformation(extent={{-30,132},{34,196}})));
    Modelica.Blocks.Sources.RealExpression flowElseVal(y=flowElse)
      annotation (Placement(transformation(extent={{-120,96},{-74,130}})));
    parameter Real table[:,:]=[0,0; 5.99,0; 6,1e-1; 8.99,1e-1; 9,1.2e-1; 9.99,1.2e-1;
        10,1.5e-1; 10.99,1.5e-1; 11,1.8e-1; 11.99,1.8e-1; 12,2e-1; 13.49,2e-1; 13.5,
        2.5e-1; 14.49,2.5e-1; 14.5,3e-1; 15.49,3e-1; 15.5,2.5e-1; 16.49,2.5e-1; 16.5,
        2e-1; 17.49,2e-1; 17.5,1.8e-1; 17.99,1.8e-1; 18,1.7e-1; 18.99,1.7e-1; 19,1.5e-1;
        19.99,1.5e-1; 20,1e-1; 21,1e-1; 21.01,0; 24,0]
      "table matrix (col1 = decimal hour ; col2 = flow rate)"
      annotation (Dialog(group="Table", enable = not tableOnFile));
    parameter Boolean tableOnFile=false
      "true, if table is defined on file or in function usertab"
      annotation (Dialog(group="Table"));
    parameter String fileName="No name" "file where matrix is stored"
      annotation(Dialog(group="Table", enable = tableOnFile,
                           __Dymola_loadSelector(filter="Text files (*.txt);;Matlab files (*.mat)",
                           caption="Open file in which table is present")));

  equation
    //Tout les 24h TIjour est mise � jour
    when mod(time, 3600*24) < 1 then
      Tjour = time;
    end when;

    connect(globalRadiation.y, enoughSun.u) annotation (Line(
        points={{-266.3,54},{-250,54}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(enoughSun.y, startConditions.u1)
                                       annotation (Line(
        points={{-227,54},{-220,54},{-220,14},{-155.6,14}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(enoughCold.y, startConditions.u2)
                                        annotation (Line(
        points={{-299,-50},{-220,-50},{-220,-0.4},{-155.6,-0.4}},
        color={255,0,255},
        smooth=Smooth.None));

    connect(startConditions.y, allowFlow.u1)  annotation (Line(
        points={{-114.2,14},{-102,14},{-102,80},{-234,80},{-234,80},{-234,151},{-189.8,
            151}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(stopCondition.y, NotStopCondition.u) annotation (Line(
        points={{-156.2,-70},{-129.6,-70}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(NotStopCondition.y, allowFlow.u2)  annotation (Line(
        points={{-88.2,-70},{-44,-70},{-44,60},{-210,60},{-210,135.8},{-189.8,135.8}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(deltaPanel.y, enoughCold.u) annotation (Line(
        points={{-336.3,-50},{-322,-50}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T_in, deltaPanel.u[1]) annotation (Line(
        points={{-400,-30},{-380,-30},{-380,-46.5},{-358,-46.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T_out, deltaPanel.u[2]) annotation (Line(
        points={{-400,-70},{-378,-70},{-378,-53.5},{-358,-53.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T_out, stopCondition.u) annotation (Line(
        points={{-400,-70},{-197.6,-70}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(minOfDay.y, combiTable1D.u[1]) annotation (Line(
        points={{-266.5,239},{-227.25,239},{-227.25,232},{-204,232}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(switch1.y, flowRate) annotation (Line(
        points={{37.2,164},{92,164},{92,104},{178,104}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(allowFlow.y, switch1.u2) annotation (Line(
        points={{-146.1,151},{-92.05,151},{-92.05,164},{-36.4,164}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(combiTable1D.y[1], switch1.u1) annotation (Line(
        points={{-135,232},{-88,232},{-88,189.6},{-36.4,189.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(flowElseVal.y, switch1.u3)
                                     annotation (Line(
        points={{-71.7,113},{-64,113},{-64,138.4},{-36.4,138.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(dirRad, globalRadiation.u[1]) annotation (Line(
        points={{-400,70},{-346,70},{-346,57.5},{-288,57.5}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(difRad, globalRadiation.u[2]) annotation (Line(
        points={{-400,30},{-344,30},{-344,50.5},{-288,50.5}},
        color={0,0,127},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-400,-120},{180,320}},
            preserveAspectRatio=true), graphics), Icon(coordinateSystem(extent={{-400,
              -120},{180,320}}, preserveAspectRatio=true), graphics));
  end Scheduled_solarPanel_control_noSample;

  package Examples
    extends Modelica.Icons.ExamplesPackage;
    model ModulatorBlockTest
      extends Modelica.Icons.Example;

      Modelica.Blocks.Continuous.Integrator FlowIntegrator
        annotation (Placement(transformation(extent={{74,-74},{100,-48}})));
    public
      BaseClasses.DiffuseIsotropic RadiationInd(
        outSkyCon=false,
        outGroCon=false,
        til=0.5235987755983) "Indirect radiation on the panel"
        annotation (Placement(transformation(extent={{-62,-68},{-22,-28}})));
      BaseClasses.DirectTiltedSurface RadiationDir(
        azi(displayUnit="deg") = 3.1415926535898,
        til=0.5235987755983,
        lat(displayUnit="deg") = 0.78539816339745)
        "Direct radiation on the panel"
        annotation (Placement(transformation(extent={{-62,-14},{-22,26}})));
      Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Meteo(filNam=
            "C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
        "Donn�es m�t�o de la station m�t�o (Ajaccio)"
        annotation (Placement(transformation(extent={{-120,-20},{-100,0}})));
      Scheduled_solarPanel_control3 modulator(
        minGlobalRadiation=30,
        flowElse=0.0000005,
        flowRate(start=0.0000005))
        annotation (Placement(transformation(extent={{44,-8},{80,16}})));
      Modelica.Blocks.Sources.Constant pulse(k=21 + 273.15)
        annotation (Placement(transformation(extent={{4,68},{24,88}})));
    public
      Buildings.BoundaryConditions.WeatherData.Bus WeatherData
        annotation (Placement(transformation(extent={{-16,10},{22,48}}),
            iconTransformation(extent={{-16,104},{22,142}})));
    equation
      connect(Meteo.weaBus, RadiationDir.weaBus) annotation (Line(
          points={{-100,-10},{-76,-10},{-76,6},{-62,6}},
          color={255,204,51},
          thickness=0.5,
          smooth=Smooth.None));
      connect(Meteo.weaBus, RadiationInd.weaBus) annotation (Line(
          points={{-100,-10},{-76,-10},{-76,-48},{-62,-48}},
          color={255,204,51},
          thickness=0.5,
          smooth=Smooth.None));

      connect(RadiationInd.H, modulator.difRad)     annotation (Line(
          points={{-20,-48},{2,-48},{2,8.63636},{46.0483,8.63636}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(RadiationDir.H, modulator.dirRad)     annotation (Line(
          points={{-20,6},{4,6},{4,13.2182},{46.0483,13.2182}},
          color={0,0,127},
          smooth=Smooth.None));

      connect(Meteo.weaBus, WeatherData) annotation (Line(
          points={{-100,-10},{-100,46},{-14,46},{-14,29},{3,29}},
          color={255,204,51},
          thickness=0.5,
          smooth=Smooth.None), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));
      connect(WeatherData.TDryBul, modulator.T_in)
                                              annotation (Line(
          points={{3,29},{24.5,29},{24.5,1.10909},{46.0483,1.10909}},
          color={255,204,51},
          thickness=0.5,
          smooth=Smooth.None), Text(
          string="%first",
          index=-1,
          extent={{-6,3},{-6,3}}));
      connect(pulse.y, modulator.T_out) annotation (Line(
          points={{25,78},{28,78},{28,-4.29091},{45.9862,-4.29091}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(modulator.flowRate, FlowIntegrator.u) annotation (Line(
          points={{79.131,3.56364},{88,3.56364},{88,-28},{56,-28},{56,-61},{71.4,-61}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(extent={{-120,-100},{100,100}},
              preserveAspectRatio=true),
                          graphics), Icon(coordinateSystem(extent={{-120,-100},
                {100,100}})),
        experiment(StopTime=3.1536e+007));
    end ModulatorBlockTest;

    model ModulatorBlockTest_noSample
      extends Modelica.Icons.Example;

      Modelica.Blocks.Continuous.Integrator FlowIntegrator
        annotation (Placement(transformation(extent={{74,-74},{100,-48}})));
    public
      BaseClasses.DiffuseIsotropic RadiationInd(
        outSkyCon=false,
        outGroCon=false,
        til=0.5235987755983) "Indirect radiation on the panel"
        annotation (Placement(transformation(extent={{-62,-68},{-22,-28}})));
      BaseClasses.DirectTiltedSurface RadiationDir(
        azi(displayUnit="deg") = 3.1415926535898,
        til=0.5235987755983,
        lat(displayUnit="deg") = 0.78539816339745)
        "Direct radiation on the panel"
        annotation (Placement(transformation(extent={{-62,-14},{-22,26}})));
      Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Meteo(filNam=
            "C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
        "Donn�es m�t�o de la station m�t�o (Ajaccio)"
        annotation (Placement(transformation(extent={{-120,-20},{-100,0}})));
      Scheduled_solarPanel_control_noSample
                                    modulator(
        minGlobalRadiation=30,
        flowElse=0.0000005,
        flowRate(start=0.0000005))
        annotation (Placement(transformation(extent={{42,-8},{78,16}})));
      Modelica.Blocks.Sources.Constant pulse(k=21 + 273.15)
        annotation (Placement(transformation(extent={{4,68},{24,88}})));
    public
      Buildings.BoundaryConditions.WeatherData.Bus WeatherData
        annotation (Placement(transformation(extent={{-16,10},{22,48}}),
            iconTransformation(extent={{-16,104},{22,142}})));
    equation
      connect(Meteo.weaBus, RadiationDir.weaBus) annotation (Line(
          points={{-100,-10},{-76,-10},{-76,6},{-62,6}},
          color={255,204,51},
          thickness=0.5,
          smooth=Smooth.None));
      connect(Meteo.weaBus, RadiationInd.weaBus) annotation (Line(
          points={{-100,-10},{-76,-10},{-76,-48},{-62,-48}},
          color={255,204,51},
          thickness=0.5,
          smooth=Smooth.None));

      connect(RadiationInd.H, modulator.difRad)     annotation (Line(
          points={{-20,-48},{2,-48},{2,8.63636},{44.0483,8.63636}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(RadiationDir.H, modulator.dirRad)     annotation (Line(
          points={{-20,6},{4,6},{4,13.2182},{44.0483,13.2182}},
          color={0,0,127},
          smooth=Smooth.None));

      connect(Meteo.weaBus, WeatherData) annotation (Line(
          points={{-100,-10},{-100,46},{-14,46},{-14,29},{3,29}},
          color={255,204,51},
          thickness=0.5,
          smooth=Smooth.None), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));
      connect(WeatherData.TDryBul, modulator.T_in)
                                              annotation (Line(
          points={{3,29},{24.5,29},{24.5,1.10909},{44.0483,1.10909}},
          color={255,204,51},
          thickness=0.5,
          smooth=Smooth.None), Text(
          string="%first",
          index=-1,
          extent={{-6,3},{-6,3}}));
      connect(pulse.y, modulator.T_out) annotation (Line(
          points={{25,78},{28,78},{28,-4.29091},{43.9862,-4.29091}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(modulator.flowRate, FlowIntegrator.u) annotation (Line(
          points={{77.131,3.56364},{88,3.56364},{88,-28},{56,-28},{56,-61},{71.4,-61}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(extent={{-120,-100},{100,100}},
              preserveAspectRatio=true),
                          graphics), Icon(coordinateSystem(extent={{-120,-100},
                {100,100}})),
        experiment(StopTime=3.1536e+007, Interval=120));
    end ModulatorBlockTest_noSample;
  end Examples;
end Other;
