within SolarSystem.Control.SolisArt_mod.Pumps;
model Pump_control "Used to know pump state (tempo, modulation, ...)"
  parameter Integer n=4 "Number of heating pump (Sj)";
  parameter Modelica.SIunits.Time temporizations[n-1]={300, 300, 300}
    "How much time we have to wait before the outter value can be true (n-1 values)";

  parameter Modelica.SIunits.Time modulations[2]={180, 180}
    "How much time we have to wait before the outter value can be true (2 values)";

  Bloc.Logical.BoolSwitch flow_limit[2] annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={102,0})));
  Modelica.Blocks.Sources.BooleanExpression normal[2](each y=true)
    annotation (Placement(transformation(extent={{48,2},{68,22}})));
  Modelica.Blocks.Sources.BooleanExpression module[2](each y=false)
    annotation (Placement(transformation(extent={{48,-24},{68,-4}})));
  Bloc.Timers.Wait_for modulation_algo[2](wait_for=modulations)
    annotation (Placement(transformation(extent={{20,-10},{40,10}})));
  Modelica.Blocks.Interfaces.BooleanInput pumps_system[3] "[S6, S5, S4]"
    annotation (Placement(transformation(extent={{-120,20},{-80,60}}),
        iconTransformation(extent={{-116,26},{-88,54}})));
  Modelica.Blocks.Interfaces.BooleanInput pumps_heat[n] "[Sj]*n" annotation (
      Placement(transformation(extent={{-120,-20},{-80,20}}),
        iconTransformation(extent={{-116,-14},{-88,14}})));

  Bloc.Timers.Wait_for_tempo temporization_algo(n=n, wait_for=temporizations)
    annotation (Placement(transformation(extent={{-40,22},{-20,38}})));

  Modelica.Blocks.Interfaces.BooleanOutput pumps_state[3 + n]
    "[S6, S5, S4, Sj*n] states"
    annotation (Placement(transformation(extent={{120,20},{160,60}}),
        iconTransformation(extent={{-12,-12},{12,12}},
        rotation=0,
        origin={132,40})));
  Modelica.Blocks.Interfaces.BooleanOutput pumps_modulation[2]
    "[S6, S5] multiplicator" annotation (Placement(transformation(extent={{120,-20},
            {160,20}}), iconTransformation(
        extent={{-12,-12},{12,12}},
        rotation=0,
        origin={132,0})));
  Bloc.Logical.Multiplex2_boolean multiplex_boolean(n1=3, n2=n)
    annotation (Placement(transformation(extent={{22,26},{40,44}})));
equation
  connect(normal.y, flow_limit.u1) annotation (Line(
      points={{69,12},{80,12},{80,8},{90,8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(module.y, flow_limit.u3) annotation (Line(
      points={{69,-14},{80,-14},{80,-8},{90,-8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumps_heat, temporization_algo.in_value) annotation (Line(
      points={{-100,0},{-60,0},{-60,30},{-40,30}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(modulation_algo.out_value, flow_limit.u2) annotation (Line(
      points={{41.4,0},{90,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(temporization_algo.out_value, multiplex_boolean.u2) annotation (Line(
      points={{-18.6,30},{0,30},{0,29.6},{20.2,29.6}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_system, multiplex_boolean.u1) annotation (Line(
      points={{-100,40},{0,40},{0,40.4},{20.2,40.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(multiplex_boolean.y, pumps_state) annotation (Line(
      points={{40.9,35},{100,35},{100,40},{140,40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_system[1], modulation_algo[1].in_value) annotation (Line(
      points={{-100,26.6667},{-100,20},{0,20},{0,0},{20,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_system[2], modulation_algo[2].in_value) annotation (Line(
      points={{-100,40},{0,40},{0,0},{20,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(flow_limit.y, pumps_modulation) annotation (Line(
      points={{113,0},{140,0}},
      color={255,0,255},
      smooth=Smooth.None));
                                                                                      annotation (Dialog(group="Temporization"), Icon(
        coordinateSystem(extent={{-100,-20},{140,60}},   preserveAspectRatio=false),
        graphics),
    Documentation(info="<html>
<p>Inputs and outputs order :</p>
<p>S6, S5, S4, Sj (heating pumps)</p>
<p>Heating pump order is important. Used to temporizations</p>
<p>n = heating pumps number</p>
<p>Each pumps start at 50&percnt; except if V3V extra open to backup (always a modulation on S5 and S6 yet)</p>
<p>Must have two heating pump at least due to temporization algorithm.</p>
</html>"),    Diagram(coordinateSystem(extent={{-100,-20},{140,60}},
          preserveAspectRatio=false),
                      graphics),defaultComponentName = "flow_out");
end Pump_control;
