within SolarSystem.Control.SolisArt_mod;
model pump_algo

  // How many heated rooms
  parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

  // Flow Control
  parameter Integer nb_panel=6 "How many collector ?";
  parameter Modelica.SIunits.Area area_panel=2.32 "Collecting area";
  parameter Modelica.SIunits.Time temporizations[n - 1]={300}
    "How much time we have to wait before the outter value can be true (n-1 values)";
  parameter Modelica.SIunits.Time modulations[n + 3]={180,180,180,180,180}
    "How much time we have to wait before the outter value can be true (n+3 values)";
  parameter Modelica.SIunits.MassFlowRate base[2]={0.0110,0.0192}
    "Base mass flow rate : [1] >> flow to panel, [2]  >> flow to backup";
  parameter Modelica.SIunits.MassFlowRate add[2]={0.0055,0.0096}
    "If more than one pump on : [1] >> flow to panel, [2]  >> flow to backup";
  parameter Modelica.SIunits.MassFlowRate m_flow_mini=0.0
    "Value of Real output";

  // S6
  parameter Modelica.Blocks.Types.SimpleController S6_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S6"));
  parameter Real S6_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S6"));
  parameter Real S6_k=0.1 "Gain of controller" annotation (Dialog(tab="S6"));
  parameter Modelica.SIunits.Time S6_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S6"));
  parameter Modelica.SIunits.Time S6_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S6"));
  parameter Real S6_yMax=1 "Upper limit of output" annotation (Dialog(tab="S6"));
  parameter Real S6_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S6"));
  parameter Real S6_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S6"));
  parameter Real S6_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S6"));

  // S5
  parameter Modelica.Blocks.Types.SimpleController S5_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S5"));
  parameter Real S5_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S5"));
  parameter Real S5_k=0.1 "Gain of controller" annotation (Dialog(tab="S5"));
  parameter Modelica.SIunits.Time S5_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S5"));
  parameter Modelica.SIunits.Time S5_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S5"));
  parameter Real S5_yMax=1 "Upper limit of output" annotation (Dialog(tab="S5"));
  parameter Real S5_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S5"));
  parameter Real S5_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S5"));
  parameter Real S5_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S5"));

  // S4
  parameter Modelica.Blocks.Types.SimpleController S4_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S4"));
  parameter Real S4_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S4"));
  parameter Real S4_k=0.1 "Gain of controller" annotation (Dialog(tab="S4"));
  parameter Modelica.SIunits.Time S4_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S4"));
  parameter Modelica.SIunits.Time S4_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S4"));
  parameter Real S4_yMax=1 "Upper limit of output" annotation (Dialog(tab="S4"));
  parameter Real S4_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S4"));
  parameter Real S4_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S4"));
  parameter Real S4_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S4"));

  // Sj
  parameter Modelica.Blocks.Types.SimpleController Sj_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="Sj"));
  parameter Real Sj_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="Sj"));
  parameter Real Sj_k=0.1 "Gain of controller" annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Ti=60 "Time constant of Integrator block" annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMax=1 "Upper limit of output" annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="Sj"));
  parameter Real Sj_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Sj"));
  parameter Real Sj_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Sj"));

  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_Sj[
                                                         n]
    "ON = true, OFF = false (pump Sj)"
    annotation (Placement(transformation(extent={{-120,-110},{-80,-70}}),
        iconTransformation(extent={{-114,-96},{-80,-62}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S4
    "ON = true, OFF = false (pump S4)"
    annotation (Placement(transformation(extent={{-120,-62},{-80,-22}}),
        iconTransformation(extent={{-114,-56},{-80,-22}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S5
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{-120,-22},{-80,18}}),
        iconTransformation(extent={{-114,-16},{-80,18}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S6
    "ON = true, OFF = false (pump S6)"
    annotation (Placement(transformation(extent={{-120,18},{-80,58}}),
        iconTransformation(extent={{-114,24},{-80,58}})));
  Modelica.Blocks.Interfaces.RealOutput S6_outter(start=0)
    "ON = 1, OFF = 0 (pump S6)"
    annotation (Placement(transformation(extent={{230,-16},{262,16}}),
        iconTransformation(extent={{226,-48},{258,-16}})));
  Modelica.Blocks.Interfaces.RealOutput S5_outter(start=0)
    "ON = 1, OFF = 0 (pump S5)"
    annotation (Placement(transformation(extent={{230,-56},{262,-24}}),
        iconTransformation(extent={{228,12},{260,44}})));
  Modelica.Blocks.Interfaces.RealOutput S4_outter(start=0)
    "ON = 1, OFF = 0 (pump S4)"
    annotation (Placement(transformation(extent={{230,-96},{262,-64}}),
        iconTransformation(extent={{228,64},{260,96}})));

  Modelica.Blocks.Interfaces.RealOutput Sj_outter[n](each start=0)
    "ON = 1, OFF = 0 (pump Sj)"
    annotation (Placement(transformation(extent={{232,54},{264,86}}),
        iconTransformation(extent={{228,-100},{260,-68}})));
  Flow_max flow_out(
    n=n,
    temporizations=temporizations,
    modulations=modulations,
    nb_panel=nb_panel,
    area_panel=area_panel,
    base=base,
    add=add,
    m_flow_mini=m_flow_mini)
    annotation (Placement(transformation(extent={{-30,-32},{48,20}})));
  Modelica.Blocks.Interfaces.BooleanInput V3V_extra
    "Open to solar panels = true, Open to extra tank = false"
    annotation (Placement(transformation(extent={{-120,50},{-80,90}}),
        iconTransformation(extent={{-114,64},{-80,98}})));
  Modelica.Blocks.Math.Product mod_Sj[n]
    annotation (Placement(transformation(extent={{200,60},{220,80}})));
  Modelica.Blocks.Math.Product mod_S6
    annotation (Placement(transformation(extent={{200,-10},{220,10}})));
  Modelica.Blocks.Math.Product mod_S5
    annotation (Placement(transformation(extent={{200,-50},{220,-30}})));
  Modelica.Blocks.Math.Product mod_S4
    annotation (Placement(transformation(extent={{200,-90},{220,-70}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_Sj(table=
        Sj_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,80},{100,100}})));
  Buildings.Controls.Continuous.LimPID pid_Sj[n](each k=Sj_k, each Td=Sj_Td,
    each wp=Sj_wp,
    each wd=Sj_wd,
    each controllerType=Sj_controller,
    each Ti=Sj_Ti,
    each yMax=Sj_yMax,
    each yMin=Sj_yMin,
    each reverseAction=Sj_reverseAction,
    each Nd=Sj_Nd)
    annotation (Placement(transformation(extent={{140,80},{160,100}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S6(table=
        S6_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,10},{100,30}})));
  Buildings.Controls.Continuous.LimPID pid_S6(
    controllerType=S6_controller,
    k=S6_k,
    Ti=S6_Ti,
    Td=S6_Td,
    wp=S6_wp,
    wd=S6_wd,
    reverseAction=true,
    yMax=S6_yMax,
    yMin=S6_yMin)
    annotation (Placement(transformation(extent={{142,10},{162,30}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S5(table=
        S5_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,-36},{100,-16}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S4(table=
        S4_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,-74},{100,-54}})));
  Buildings.Controls.Continuous.LimPID pid_S5(
    controllerType=S5_controller,
    k=S5_k,
    Ti=S5_Ti,
    Td=S5_Td,
    wp=S5_wp,
    wd=S5_wd,
    yMax=S5_yMax,
    yMin=S5_yMin,
    reverseAction=true)
    annotation (Placement(transformation(extent={{140,-36},{160,-16}})));
  Buildings.Controls.Continuous.LimPID pid_S4(
    controllerType=S4_controller,
    k=S4_k,
    Ti=S4_Ti,
    Td=S4_Td,
    wp=S4_wp,
    wd=S4_wd,
    yMax=S4_yMax,
    yMin=S4_yMin,
    reverseAction=true)
    annotation (Placement(transformation(extent={{140,-74},{160,-54}})));
  Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={40,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={100,96})));
  Modelica.Blocks.Interfaces.RealInput T8 "T. at heat transmitters start"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={70,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={140,96})));
  Modelica.Blocks.Math.MultiSum delta_heat(k={1,-1}, nu=2) "T8 - T7"
    annotation (Placement(transformation(extent={{104,68},{116,80}})));
  Modelica.Blocks.Routing.Replicator delta_replicator(nout=n)
                                                        annotation (
      Placement(transformation(extent={{124,68},{134,80}})));
  Modelica.Blocks.Math.MultiSum delta_storage(k={1,-1}, nu=2) "T1 - T5"
    annotation (Placement(transformation(extent={{126,2},{134,10}})));
  Modelica.Blocks.Math.MultiSum delta_solar(k={1,-1}, nu=2) "T1 - T3"
    annotation (Placement(transformation(extent={{126,-44},{134,-36}})));
  Modelica.Blocks.Math.MultiSum delta_extra(k={1,-1}, nu=2) "T8 - T4"
    annotation (Placement(transformation(extent={{126,-82},{134,-74}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after collector"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-80,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={-60,96})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-50,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={-20,96})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-20,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={20,96})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={10,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={60,96})));
  Modelica.Blocks.Routing.Replicator control_replicator(nout=n)
                                                        annotation (
      Placement(transformation(extent={{124,84},{134,96}})));

  Modelica.Blocks.Logical.Switch S6_if_max
    annotation (Placement(transformation(extent={{182,0},{194,12}})));
  Modelica.Blocks.Logical.Switch Sj_if_max[n]
    annotation (Placement(transformation(extent={{182,70},{194,82}})));
  Modelica.Blocks.Logical.Switch S5_if_max
    annotation (Placement(transformation(extent={{182,-40},{194,-28}})));
  Modelica.Blocks.Logical.Switch S4_if_max
    annotation (Placement(transformation(extent={{182,-80},{194,-68}})));
  Modelica.Blocks.Sources.RealExpression S6_no_mod(each y=1)
    annotation (Placement(transformation(extent={{160,-2},{168,6}})));
  Modelica.Blocks.Sources.RealExpression Sj_no_mod[n](each y=1)
    annotation (Placement(transformation(extent={{154,66},{162,76}})));
  Modelica.Blocks.Sources.RealExpression S6_no_mod1(each y=1)
    annotation (Placement(transformation(extent={{164,-44},{170,-34}})));
  Modelica.Blocks.Sources.RealExpression S6_no_mod2(each y=1)
    annotation (Placement(transformation(extent={{164,-84},{170,-74}})));
  Modelica.Blocks.Interfaces.RealOutput pumps_miniFlows[3 + n] "Minimal flow "
    annotation (Placement(transformation(extent={{-14,-14},{14,14}},
        rotation=-90,
        origin={40,-110}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={100,-104})));
  Modelica.Blocks.Interfaces.BooleanOutput pumps_state[n + 3]
    "[S6, S5, S4, Sj*n] states" annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=-90,
        origin={-20,-110}), iconTransformation(
        extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={0,-104})));
  parameter Real Sj_Nd=10 "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Sj"));
  parameter Boolean Sj_reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Sj"));

equation
  connect(V3V_extra, flow_out.V3V_extra) annotation (Line(
      points={{-100,70},{-40,70},{-40,14.8},{-30,14.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumpControl_Sj, flow_out.in_value) annotation (Line(
      points={{-100,-90},{-40,-90},{-40,-26.8},{-30,-26.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumpControl_S6, flow_out.in_value_other[1]) annotation (Line(
      points={{-100,38},{-50,38},{-50,-9.46667},{-30,-9.46667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumpControl_S5, flow_out.in_value_other[2]) annotation (Line(
      points={{-100,-2},{-50,-2},{-50,-6},{-30,-6}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumpControl_S4, flow_out.in_value_other[3]) annotation (Line(
      points={{-100,-42},{-50,-42},{-50,-2.53333},{-30,-2.53333}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(mod_Sj.y, Sj_outter)     annotation (Line(
      points={{221,70},{248,70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(mod_S6.y, S6_outter)     annotation (Line(
      points={{221,0},{246,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(mod_S5.y, S5_outter)     annotation (Line(
      points={{221,-40},{246,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(mod_S4.y, S4_outter)     annotation (Line(
      points={{221,-80},{246,-80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(flow_out.out_value[1], mod_S6.u2)     annotation (Line(
      points={{51.12,-6},{198,-6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(flow_out.out_value[2],mod_S5. u2)     annotation (Line(
      points={{51.12,-6},{120,-6},{120,-46},{198,-46}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(flow_out.out_value[3],mod_S4. u2)     annotation (Line(
      points={{51.12,-6},{120,-6},{120,-86},{198,-86}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(flow_out.out_value[4:n+3], mod_Sj.u2)   annotation (Line(
      points={{51.12,-6},{120,-6},{120,64},{198,64}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(control_S6.y[1], pid_S6.u_s) annotation (Line(
      points={{101,20},{140,20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(control_S5.y[1], pid_S5.u_s) annotation (Line(
      points={{101,-26},{138,-26}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(control_S4.y[1], pid_S4.u_s) annotation (Line(
      points={{101,-64},{138,-64}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T8, delta_heat.u[1]) annotation (Line(
      points={{70,100},{70,76.1},{104,76.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7, delta_heat.u[2]) annotation (Line(
      points={{40,100},{40,71.9},{104,71.9}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_heat.y, delta_replicator.u) annotation (Line(
      points={{117.02,74},{123,74}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_replicator.y, pid_Sj.u_m) annotation (Line(
      points={{134.5,74},{150,74},{150,78}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_solar.y, pid_S5.u_m) annotation (Line(
      points={{134.68,-40},{140,-40},{140,-44},{150,-44},{150,-38}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_storage.y, pid_S6.u_m) annotation (Line(
      points={{134.68,6},{140,6},{140,2},{152,2},{152,8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_extra.y, pid_S4.u_m) annotation (Line(
      points={{134.68,-78},{140,-78},{140,-82},{150,-82},{150,-76}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, delta_storage.u[1]) annotation (Line(
      points={{-80,100},{-80,74},{-38,74},{-38,28},{60,28},{60,7.4},{126,7.4}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T1, delta_solar.u[1]) annotation (Line(
      points={{-80,100},{-80,74},{-38,74},{-38,28},{60,28},{60,-38.6},{126,-38.6}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T5, delta_storage.u[2]) annotation (Line(
      points={{10,100},{10,80},{20,80},{20,60},{72,60},{72,4.6},{126,4.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, delta_solar.u[2]) annotation (Line(
      points={{-50,100},{-50,76},{-20,76},{-20,40},{64,40},{64,-41.4},{126,-41.4}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T8, delta_extra.u[1]) annotation (Line(
      points={{70,100},{70,64},{110,64},{110,-76.6},{126,-76.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, delta_extra.u[2]) annotation (Line(
      points={{-20,100},{-20,80},{0,80},{0,54},{68,54},{68,-79.4},{126,-79.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(control_Sj.y[1], control_replicator.u) annotation (Line(
      points={{101,90},{123,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(control_replicator.y, pid_Sj.u_s) annotation (Line(
      points={{134.5,90},{138,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S6_if_max.y, mod_S6.u1) annotation (Line(
      points={{194.6,6},{198,6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Sj_if_max.y, mod_Sj.u1) annotation (Line(
      points={{194.6,76},{198,76}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_if_max.y, mod_S5.u1) annotation (Line(
      points={{194.6,-34},{198,-34}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S4_if_max.y, mod_S4.u1) annotation (Line(
      points={{194.6,-74},{198,-74}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(flow_out.out_stateValue[1], S6_if_max.u2) annotation (Line(
      points={{51.12,-21.6},{78,-21.6},{78,-4},{154,-4},{154,6},{180.8,6}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(flow_out.out_stateValue[2], S5_if_max.u2) annotation (Line(
      points={{51.12,-21.6},{78,-21.6},{78,-48},{162,-48},{162,-34},{180.8,-34}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(flow_out.out_stateValue[3], S4_if_max.u2) annotation (Line(
      points={{51.12,-21.6},{78,-21.6},{78,-92},{162,-92},{162,-74},{180.8,-74}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(flow_out.out_stateValue[4:n + 3], Sj_if_max.u2) annotation (Line(
      points={{51.12,-21.6},{78,-21.6},{78,60},{166,60},{166,76},{180.8,76}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Sj_no_mod.y, Sj_if_max.u3) annotation (Line(
      points={{162.4,71},{168.65,71},{168.65,71.2},{180.8,71.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S6_no_mod.y, S6_if_max.u3) annotation (Line(
      points={{168.4,2},{170.2,2},{170.2,1.2},{180.8,1.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S6_no_mod1.y, S5_if_max.u3) annotation (Line(
      points={{170.3,-39},{171.15,-39},{171.15,-38.8},{180.8,-38.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S6_no_mod2.y, S4_if_max.u3) annotation (Line(
      points={{170.3,-79},{171.15,-79},{171.15,-78.8},{180.8,-78.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(flow_out.S4_flow_mini, pumps_miniFlows) annotation (Line(
      points={{32.4,-34.6},{32.4,-65.3},{40,-65.3},{40,-110}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(flow_out.pumps_state, pumps_state) annotation (Line(
      points={{-3.48,-34.6},{-3.48,-67.3},{-20,-67.3},{-20,-110}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(pid_Sj.y, Sj_if_max.u1) annotation (Line(
      points={{161,90},{170,90},{170,80.8},{180.8,80.8}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(pid_S5.y, S5_if_max.u1) annotation (Line(
      points={{161,-26},{172,-26},{172,-29.2},{180.8,-29.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_S6.y, S6_if_max.u1) annotation (Line(
      points={{163,20},{172,20},{172,10.8},{180.8,10.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_S4.y, S4_if_max.u1) annotation (Line(
      points={{161,-64},{170,-64},{170,-69.2},{180.8,-69.2}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-100},{240,100}},
          preserveAspectRatio=false), graphics, defaultComponentName = "pump_control"), Icon(coordinateSystem(extent={{-100,
            -100},{240,100}}, preserveAspectRatio=true),  graphics={
                  Rectangle(
          extent={{-100,100},{240,-100}},
          lineColor={90,150,90},
          fillPattern=FillPattern.CrossDiag,
          fillColor={0,127,127}),
        Text(
          extent={{-100,62},{238,16}},
          lineColor={0,0,0},
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="PID"),
        Text(
          extent={{-96,-20},{240,-52}},
          lineColor={0,0,0},
          textString="Flow switch",
          fontName="Consolas",
          textStyle={TextStyle.Bold})}),
    Documentation(info="<html>
<p>Control all pumps according to pump state from algo.</p>
</html>"));
end pump_algo;
