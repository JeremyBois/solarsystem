within SolarSystem.Control.SolisArt_mod;
model ExtraValve_mod
  "Lock or unlock the extra valve (true = open to solar, false = open to extra tank). Use when only one room"
  import SI = Modelica.SIunits;

parameter SI.ThermodynamicTemperature Tinstruction[n] = {293.15, 293.15}
    "Inside desired temperature";

parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-120,130},{-80,170}})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
    annotation (Placement(transformation(extent={{-120,90},{-80,130}})));
  Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-120,50},{-80,90}}),
        iconTransformation(extent={{-120,20},{-80,60}})));
  Modelica.Blocks.Interfaces.BooleanInput  ECS "ON = true, OFF = false"
    annotation (Placement(transformation(extent={{-120,-110},{-80,-70}}),
        iconTransformation(extent={{-120,-110},{-80,-70}})));
  Modelica.Blocks.Interfaces.BooleanInput CHAUFF[n] "ON = true, OFF = false"
    annotation (Placement(transformation(extent={{-120,-150},{-80,-110}}),
        iconTransformation(extent={{-120,-158},{-80,-118}})));
  Modelica.Blocks.Interfaces.RealInput Tambiant[n] "T. in the room"
    annotation (Placement(transformation(extent={{-120,-10},{-80,30}}),
        iconTransformation(extent={{-120,-60},{-80,-20}})));
  Modelica.Blocks.Interfaces.BooleanOutput V3V_extra(start=true)
    "Open to solar panels = true, Open to extra tank = false"
    annotation (Placement(transformation(extent={{212,-20},{252,20}}),
        iconTransformation(extent={{212,-20},{252,20}})));
  Modelica.Blocks.Logical.Not opposite_ECS "Return the opposite ECS state"
    annotation (Placement(transformation(extent={{-60,-100},{-40,-80}})));
  Modelica.Blocks.Logical.Greater delta_T1_greater_test
    "delta_T is greater than threshold ?"
    annotation (Placement(transformation(extent={{-10,130},{10,150}})));
  Modelica.Blocks.Logical.GreaterThreshold delta_T1_greater_test1(threshold=10)
    "delta_T1 is greater than threshold ?"
    annotation (Placement(transformation(extent={{-10,40},{10,60}})));
  Modelica.Blocks.Math.MultiSum delta_T1(     k={1,-1}, nu=2) "T1 - T7"
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  Modelica.Blocks.Logical.GreaterEqual          T4_greaterEqual_test
    "T4 is greater or equal to threshold ?"
    annotation (Placement(transformation(extent={{-8,80},{12,100}})));
  Modelica.Blocks.Interfaces.BooleanOutput ECS_out(start=true)
    "ON = true, OFF = false"
    annotation (Placement(transformation(extent={{212,110},{252,150}}),
        iconTransformation(extent={{212,110},{252,150}})));
  Modelica.Blocks.Interfaces.BooleanOutput CHAUFF_out[n](each start=true)
    "ON = true, OFF = false" annotation (Placement(transformation(extent={{214,-140},
            {254,-100}}), iconTransformation(extent={{214,-140},{254,-100}})));
  Modelica.Blocks.Logical.Greater Tambiant_min_greater_test[n]
    "Tambiant_min is greater than threshold"
    annotation (Placement(transformation(extent={{-10,0},{10,20}})));
  Modelica.Blocks.Sources.RealExpression compare1(y=T4 + 11 - 8*V3V_extra_real.y)
    annotation (Placement(transformation(extent={{-60,112},{-40,132}})));
  Modelica.Blocks.Sources.RealExpression compare2(y=273.15 + 40 -
        V3V_extra_real.y)
    annotation (Placement(transformation(extent={{-60,66},{-40,86}})));
  Modelica.Blocks.Sources.RealExpression compare3[n](y=Tinstruction)
    annotation (Placement(transformation(extent={{-100,-30},{-80,-10}})));
  Modelica.Blocks.Math.Add add1[n](each k1=1, each k2=-1)
    "TsolarInstruction + 0.2"
    annotation (Placement(transformation(extent={{-58,-12},{-46,0}})));
  Modelica.Blocks.Sources.RealExpression cst[n](each y=0.1)
    annotation (Placement(transformation(extent={{-100,-46},{-80,-26}})));
  Buildings.Utilities.Math.BooleanReplicator opposite_ECS_replicator(nout=n)
    annotation (Placement(transformation(extent={{-22,-118},{-2,-98}})));
  Modelica.Blocks.Logical.And open_chauff_cond[n]
    "If S4 flow rate is minimum and deltaT_test false, it's false otherwise it's true "
    annotation (Placement(transformation(extent={{32,-130},{52,-110}})));
  Modelica.Blocks.Logical.And open_chauff_cond1[n]
    "If S4 flow rate is minimum and deltaT_test false, it's false otherwise it's true "
    annotation (Placement(transformation(extent={{68,-128},{88,-108}})));
  Modelica.Blocks.Logical.And open_chauff_cond2[n]
    "If S4 flow rate is minimum and deltaT_test false, it's false otherwise it's true "
    annotation (Placement(transformation(extent={{108,-128},{128,-108}})));
  Buildings.Utilities.Math.BooleanReplicator deltaT1_T7_replicator(nout=n)
    annotation (Placement(transformation(extent={{74,-98},{94,-78}})));
  Modelica.Blocks.Math.BooleanToReal V3V_extra_real annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={170,-32})));
  Modelica.Blocks.Logical.Pre breaker "Cut loop to avoid infinite loop"
    annotation (Placement(transformation(extent={{220,-42},{200,-22}})));
  Modelica.Blocks.MathBoolean.And on_conditions2(nu=3)
    annotation (Placement(transformation(extent={{60,120},{80,140}})));
  Modelica.Blocks.Logical.And on_conditions1
    annotation (Placement(transformation(extent={{60,60},{80,80}})));
  Modelica.Blocks.Logical.Not opposite_CHAUFF
    "Return the opposite CHAUFF state"
    annotation (Placement(transformation(extent={{-26,-158},{-6,-138}})));
  Modelica.Blocks.MathBoolean.Or all_on(nu=3)
    annotation (Placement(transformation(extent={{120,-10},{140,10}})));
  Utilities.Logical.BoolSwitch switch_ECS "Keep current state if false"
    annotation (Placement(transformation(extent={{180,120},{200,140}})));
  Utilities.Logical.BoolSwitch switch_CHAUFF[n] "Keep current state if false"
    annotation (Placement(transformation(extent={{180,-130},{200,-110}})));
  Modelica.Blocks.Logical.Not change_ECS
    annotation (Placement(transformation(extent={{120,140},{140,160}})));
  Modelica.Blocks.Logical.Not change_CHAUFF[n]
    annotation (Placement(transformation(extent={{150,-100},{170,-80}})));
  Utilities.Logical.True_seeker any_CHAUFF(n=n)
    annotation (Placement(transformation(extent={{-70,-160},{-48,-138}})));
  Utilities.Logical.True_seeker on_conditions4(n=n) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={90,-30})));
equation
  connect(T1, delta_T1.u[1]) annotation (Line(
      points={{-100,150},{-68,150},{-68,53.5},{-60,53.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7, delta_T1.u[2]) annotation (Line(
      points={{-100,70},{-73,70},{-73,46.5},{-60,46.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_T1.y, delta_T1_greater_test1.u) annotation (Line(
      points={{-38.3,50},{-12,50}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T1, delta_T1_greater_test.u1) annotation (Line(
      points={{-100,150},{-68,150},{-68,140},{-12,140}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(compare1.y, delta_T1_greater_test.u2) annotation (Line(
      points={{-39,122},{-20,122},{-20,132},{-12,132}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(compare2.y, T4_greaterEqual_test.u2) annotation (Line(
      points={{-39,76},{-22,76},{-22,82},{-10,82}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, T4_greaterEqual_test.u1) annotation (Line(
      points={{-100,110},{-64,110},{-64,90},{-10,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst.y, add1.u2) annotation (Line(
      points={{-79,-36},{-70,-36},{-70,-9.6},{-59.2,-9.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(compare3.y, add1.u1) annotation (Line(
      points={{-79,-20},{-74,-20},{-74,-2.4},{-59.2,-2.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add1.y, Tambiant_min_greater_test.u2) annotation (Line(
      points={{-45.4,-6},{-40,-6},{-40,2},{-12,2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant, Tambiant_min_greater_test.u1) annotation (Line(
      points={{-100,10},{-12,10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ECS, opposite_ECS.u) annotation (Line(
      points={{-100,-90},{-62,-90}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(opposite_ECS.y, opposite_ECS_replicator.u) annotation (Line(
      points={{-39,-90},{-24,-90},{-24,-108}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(opposite_ECS_replicator.y, open_chauff_cond.u1) annotation (Line(
      points={{-1,-108},{18,-108},{18,-120},{30,-120}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(CHAUFF, open_chauff_cond.u2) annotation (Line(
      points={{-100,-130},{-42,-130},{-42,-128},{30,-128}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(open_chauff_cond.y, open_chauff_cond1.u2) annotation (Line(
      points={{53,-120},{58,-120},{58,-126},{66,-126}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(open_chauff_cond1.y, open_chauff_cond2.u2) annotation (Line(
      points={{89,-118},{96,-118},{96,-126},{106,-126}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(delta_T1_greater_test1.y, deltaT1_T7_replicator.u) annotation (Line(
      points={{11,50},{24,50},{24,-88},{72,-88}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tambiant_min_greater_test.y, open_chauff_cond1.u1) annotation (Line(
      points={{11,10},{20,10},{20,-98},{58,-98},{58,-116},{62,-116},{62,-118},{66,
          -118}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(deltaT1_T7_replicator.y, open_chauff_cond2.u1) annotation (Line(
      points={{95,-88},{98,-88},{98,-118},{106,-118}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_extra, breaker.u) annotation (Line(
      points={{232,0},{240,0},{240,-32},{222,-32}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(breaker.y, V3V_extra_real.u) annotation (Line(
      points={{199,-32},{182,-32}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(opposite_ECS.y, on_conditions1.u1) annotation (Line(
      points={{-39,-90},{-24,-90},{-24,-46},{42,-46},{42,70},{58,70}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(all_on.y, V3V_extra) annotation (Line(
      points={{141.5,0},{232,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(delta_T1_greater_test.y, on_conditions2.u[1]) annotation (Line(
      points={{11,140},{36,140},{36,134.667},{60,134.667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T4_greaterEqual_test.y, on_conditions2.u[2]) annotation (Line(
      points={{13,90},{35.5,90},{35.5,130},{60,130}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(ECS, on_conditions2.u[3]) annotation (Line(
      points={{-100,-90},{-72,-90},{-72,-40},{40,-40},{40,125.333},{60,125.333}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(on_conditions1.y, all_on.u[1]) annotation (Line(
      points={{81,70},{100,70},{100,4.66667},{120,4.66667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on_conditions2.y, all_on.u[2]) annotation (Line(
      points={{81.5,130},{90,130},{90,0},{120,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(opposite_CHAUFF.y, on_conditions1.u2) annotation (Line(
      points={{-5,-148},{6,-148},{6,-50},{44,-50},{44,62},{58,62}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(switch_ECS.y, ECS_out) annotation (Line(
      points={{201,130},{232,130}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(switch_CHAUFF.y, CHAUFF_out) annotation (Line(
      points={{201,-120},{234,-120}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(open_chauff_cond2.y, switch_CHAUFF.u2) annotation (Line(
      points={{129,-118},{160,-118},{160,-120},{178,-120}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on_conditions2.y, switch_ECS.u2) annotation (Line(
      points={{81.5,130},{178,130}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(ECS, switch_ECS.u3) annotation (Line(
      points={{-100,-90},{-72,-90},{-72,-40},{40,-40},{40,100},{100,100},{100,122},
          {178,122}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(CHAUFF, switch_CHAUFF.u3) annotation (Line(
      points={{-100,-130},{14,-130},{14,-158},{154,-158},{154,-128},{178,-128}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(change_ECS.y, switch_ECS.u1) annotation (Line(
      points={{141,150},{160.5,150},{160.5,138},{178,138}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on_conditions2.y, change_ECS.u) annotation (Line(
      points={{81.5,130},{90,130},{90,150},{118,150}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(open_chauff_cond2.y, change_CHAUFF.u) annotation (Line(
      points={{129,-118},{140,-118},{140,-90},{148,-90}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(change_CHAUFF.y, switch_CHAUFF.u1) annotation (Line(
      points={{171,-90},{174,-90},{174,-112},{176,-112},{176,-112},{178,-112},{178,
          -112}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(any_CHAUFF.y, opposite_CHAUFF.u) annotation (Line(
      points={{-48,-149},{-38,-149},{-38,-148},{-28,-148}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(CHAUFF, any_CHAUFF.u) annotation (Line(
      points={{-100,-130},{-85,-130},{-85,-149},{-70,-149}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(open_chauff_cond2.y, on_conditions4.u) annotation (Line(
      points={{129,-118},{140,-118},{140,-48},{90,-48},{90,-40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on_conditions4.y, all_on.u[3]) annotation (Line(
      points={{90,-20},{90,-4.66667},{120,-4.66667}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-180},{220,180}},
          preserveAspectRatio=false),graphics), Icon(coordinateSystem(extent={{-100,
            -180},{220,180}}, preserveAspectRatio=true), graphics={
        Text(
          extent={{-60,72},{188,-58}},
          lineColor={0,128,0},
          textString="Extra
valve control"),
        Polygon(
          points={{-100,180},{240,180},{240,-180},{-100,-180},{-100,180},
              {-100,180},{-100,180}},
          lineColor={255,0,255},
          smooth=Smooth.None)}),
    Documentation(info="<html>
<p>This algorithm must be used only when system use only one room (2 room use but one fake room). Have to use a fake room due to pump algorithm control ...</p>
</html>"));
end ExtraValve_mod;
