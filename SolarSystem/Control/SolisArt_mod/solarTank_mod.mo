within SolarSystem.Control.SolisArt_mod;
model solarTank_mod "SolisArt S5 pump control"

  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-120,50},{-80,90}})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-120,10},{-80,50}})));
  Modelica.Blocks.Interfaces.BooleanInput V3V_solar
    "Open to solar panels = true, Open to heat tank = false"
    annotation (Placement(transformation(extent={{-120,-90},{-80,-50}})));
  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S5(start=true)
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{120,-40},{160,0}})));
  Modelica.Blocks.Math.MultiSum delta_T(nu=2, k={1,-1}) "T1 - T3"
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  Modelica.Blocks.Logical.Hysteresis deltaT_hys_test(
    y(start=false),
    uLow=4,
    uHigh=10) "Test T1 - T3"
    annotation (Placement(transformation(extent={{-20,40},{0,60}})));
  Modelica.Blocks.Logical.OnOffController onOffController_T3(bandwidth=5,
      pre_y_start=true)
    annotation (Placement(transformation(extent={{-20,0},{0,20}})));
  Modelica.Blocks.MathBoolean.And conditions(nu=3)
    "true if all true, else false"
    annotation (Placement(transformation(extent={{42,-28},{58,-12}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=273.15 + 90)
    annotation (Placement(transformation(extent={{-60,6},{-40,26}})));
equation
  connect(delta_T.y, deltaT_hys_test.u)
                                    annotation (Line(
      points={{-38.3,50},{-22,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, delta_T.u[1]) annotation (Line(
      points={{-100,70},{-80,70},{-80,53.5},{-60,53.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, delta_T.u[2]) annotation (Line(
      points={{-100,30},{-80,30},{-80,46.5},{-60,46.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, onOffController_T3.u) annotation (Line(
      points={{-100,30},{-80,30},{-80,4},{-22,4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(deltaT_hys_test.y, conditions.u[1])
                                          annotation (Line(
      points={{1,50},{20,50},{20,-16.2667},{42,-16.2667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(onOffController_T3.y, conditions.u[2]) annotation (Line(
      points={{1,10},{10,10},{10,-20},{42,-20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_solar, conditions.u[3])   annotation (Line(
      points={{-100,-70},{20,-70},{20,-23.7333},{42,-23.7333}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(conditions.y, pumpControl_S5) annotation (Line(
      points={{59.2,-20},{140,-20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(realExpression.y, onOffController_T3.reference) annotation (Line(
      points={{-39,16},{-22,16}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-120},{140,120}},
          preserveAspectRatio=false),graphics), Icon(coordinateSystem(
          extent={{-100,-120},{140,120}}, preserveAspectRatio=true),
        graphics={
        Text(
          extent={{-74,62},{100,-62}},
          lineColor={0,128,0},
          textString="Solar Tank
pump control"),
        Polygon(
          points={{-98,120},{140,120},{140,-120},{-100,-120},{-100,118},
              {-100,120},{-98,120}},
          lineColor={255,0,255},
          smooth=Smooth.None)}));
end solarTank_mod;
