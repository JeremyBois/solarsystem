within SolarSystem.Control.SolisArt_mod;
model Heating_mod

  import SI = Modelica.SIunits;

parameter SI.ThermodynamicTemperature Tinstruction[n] = {293.15, 293.15}
    "Inside desired temperature";
parameter Integer n = 2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states : true = ON, false = OFF)";
  parameter SI.ThermodynamicTemperature Tambiant_out_start[n]={291.15,291.15}
    "T. in the room"                                                              annotation (Dialog(group="Initialization"));

  Modelica.Blocks.Interfaces.RealInput Tambiant[n] "T. in the room"
    annotation (Placement(transformation(extent={{-120,110},{-80,150}}),
        iconTransformation(extent={{-112,104},{-80,136}})));
  Modelica.Blocks.Interfaces.RealInput  DTeco "Return DTeco value"
    annotation (Placement(transformation(extent={{-120,70},{-80,110}}),
        iconTransformation(extent={{-112,64},{-80,96}})));
  Modelica.Blocks.Interfaces.BooleanInput V3V_extra
    "Open to solar panels = true, Open to extra tank = false"
    annotation (Placement(transformation(extent={{-120,-10},{-80,30}}),
        iconTransformation(extent={{-114,-36},{-80,-2}})));
  Modelica.Blocks.Interfaces.BooleanInput V3V_solar
    "Open to solar panels = true, Open to storage tank = false"
    annotation (Placement(transformation(extent={{-120,-50},{-80,-10}}),
        iconTransformation(extent={{-114,-78},{-80,-44}})));
  Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-122,-270},{-82,-230}}),
        iconTransformation(extent={{-112,-258},{-78,-224}})));
  Modelica.Blocks.Interfaces.RealInput T8
    "T. at heating start point (just before extra exchanger input)"
    annotation (Placement(transformation(extent={{-122,-240},{-82,-200}}),
        iconTransformation(extent={{-112,-306},{-78,-272}})));
  Modelica.Blocks.Interfaces.RealInput TsolarInstruction[n] annotation (
      Placement(transformation(extent={{-120,30},{-80,70}}), iconTransformation(
          extent={{-112,24},{-80,56}})));

  Modelica.Blocks.Logical.And on1[n]
    annotation (Placement(transformation(extent={{140,80},{160,100}})));
  Buildings.Utilities.Math.BooleanReplicator replicator3(nout=n)
                                                        annotation (
      Placement(transformation(extent={{-60,0},{-40,20}})));
  Modelica.Blocks.MathBoolean.Or all_on[n](each nu=3)
    annotation (Placement(transformation(extent={{180,40},{200,60}})));
  Modelica.Blocks.Math.Add3 deltaT_2[n](
    each k1=1,
    each k2=1,
    each k3=-1) "Tinstruction + 0.1 - DTeco"
    annotation (Placement(transformation(extent={{20,38},{38,56}})));
  Modelica.Blocks.Logical.Not not_extra[n] "V3V to tank"
    annotation (Placement(transformation(extent={{-6,-26},{6,-14}})));
  Modelica.Blocks.MathBoolean.And on2[n](each nu=4)
    annotation (Placement(transformation(extent={{140,40},{160,60}})));
  Modelica.Blocks.Logical.Less deltaT_2_test[n]
    annotation (Placement(transformation(extent={{60,40},{80,60}})));
  Modelica.Blocks.Logical.Less delta_T3[n]
    annotation (Placement(transformation(extent={{60,0},{80,20}})));
  Modelica.Blocks.MathBoolean.And on3[n](each nu=4)
    annotation (Placement(transformation(extent={{140,0},{160,20}})));
  Buildings.Utilities.Math.BooleanReplicator
                                     replicator4(
                                                nout=n) annotation (
      Placement(transformation(extent={{-60,-40},{-40,-20}})));
  Modelica.Blocks.Sources.RealExpression Tinstruction_kelvin2[n](y=Tinstruction)
    "Minimal temperature inside each rooms"
    annotation (Placement(transformation(extent={{-88,-196},{-60,-164}})));
  Modelica.Blocks.Math.Add3 deltaT_4[n](
    each k1=1,
    each k2=1,
    each k3=-2) "Tinstruction + 0.2 - 2*DTeco"
    annotation (Placement(transformation(extent={{-8,-160},{10,-142}})));
  Modelica.Blocks.Logical.Greater deltaT_3_test[n]
    annotation (Placement(transformation(extent={{60,-168},{80,-148}})));
  Modelica.Blocks.Logical.And off1[n]
    annotation (Placement(transformation(extent={{140,-160},{160,-140}})));
  Modelica.Blocks.Logical.Not not_off1[n]
    annotation (Placement(transformation(extent={{180,-160},{200,-140}})));
  Modelica.Blocks.Logical.Greater deltaT_test[n]
    annotation (Placement(transformation(extent={{60,-208},{80,-188}})));
  Modelica.Blocks.Logical.Or or1[n]
    annotation (Placement(transformation(extent={{100,-208},{120,-188}})));
  Modelica.Blocks.Math.MultiSum deltaT_5(k={1,-1}, nu=2) "T8 - T7"
    annotation (Placement(transformation(extent={{-60,-240},{-40,-220}})));
  Modelica.Blocks.Logical.LessThreshold deltaT_4_test(threshold=2.5)
    annotation (Placement(transformation(extent={{-10,-240},{10,-220}})));
  Buildings.Utilities.Math.BooleanReplicator
                                     replicator9(
                                                nout=n) annotation (
      Placement(transformation(extent={{60,-240},{80,-220}})));
  Modelica.Blocks.Logical.Not not_off2[n]
    annotation (Placement(transformation(extent={{180,-208},{200,-188}})));
  Modelica.Blocks.MathBoolean.And off2[n](each nu=3)
    annotation (Placement(transformation(extent={{140,-208},{160,-188}})));
  Modelica.Blocks.Logical.Not not_solar[
                                       n] "V3V to tank"
    annotation (Placement(transformation(extent={{-6,-48},{6,-36}})));
  Modelica.Blocks.MathBoolean.And final_state[n](each nu=4)
    annotation (Placement(transformation(extent={{240,-72},{264,-48}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold T5_test[n](each threshold=
        273.15 + 28)
    annotation (Placement(transformation(extent={{60,-80},{80,-60}})));
  Modelica.Blocks.Logical.Greater          T1_test[n]
    annotation (Placement(transformation(extent={{60,-120},{80,-100}})));
  Modelica.Blocks.Routing.Replicator replicator5(
                                                nout=n) annotation (
      Placement(transformation(extent={{20,-80},{40,-60}})));
  Modelica.Blocks.Routing.Replicator replicator6(
                                                nout=n) annotation (
      Placement(transformation(extent={{20,-104},{36,-88}})));
  Modelica.Blocks.Logical.Or or2[n]
    annotation (Placement(transformation(extent={{100,-280},{120,-260}})));
  Modelica.Blocks.Math.Add add2[n](each k1=1, each k2=1)
    "TsolarInstruction + 0.2"
    annotation (Placement(transformation(extent={{-10,-288},{10,-268}})));
  Modelica.Blocks.Sources.RealExpression cst2[n](each y=0.3)
    "Minimal temperature inside each rooms"
    annotation (Placement(transformation(extent={{-62,-272},{-44,-254}})));
  Modelica.Blocks.Logical.Greater add_test[n]
    annotation (Placement(transformation(extent={{58,-288},{78,-268}})));
  Modelica.Blocks.MathBoolean.And off3[n](each nu=3)
    annotation (Placement(transformation(extent={{140,-280},{160,-260}})));
  Modelica.Blocks.Logical.Not not_off3[n]
    annotation (Placement(transformation(extent={{180,-280},{200,-260}})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-120,-90},{-80,-50}}),
        iconTransformation(extent={{-112,-206},{-80,-174}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-120,-120},{-80,-80}}),
        iconTransformation(extent={{-112,-164},{-80,-132}})));

  Modelica.Blocks.Routing.BooleanPassThrough V3V_extra_pass
    "Just pass the signal"
    annotation (Placement(transformation(extent={{240,20},{260,40}})));
  Modelica.Blocks.Routing.RealPassThrough Tambiant_pass[n]
    "Just pass the signal"
    annotation (Placement(transformation(extent={{240,120},{260,140}})));
  Modelica.Blocks.Interfaces.RealOutput Tambiant_out[n](start=
        Tambiant_out_start) "T. in the room"
    annotation (Placement(transformation(extent={{308,110},{348,150}}),
        iconTransformation(extent={{300,102},{338,140}})));
  Utilities.Timers.Wait_for wait_for "T8 - T7 timer"
    annotation (Placement(transformation(extent={{20,-240},{40,-220}})));
  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S5(start=true)
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{312,-230},{352,-190}})));

  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_Sj[
                                                         n](each start=true)
    "ON = true, OFF = false (pump Sj)"
    annotation (Placement(transformation(extent={{310,-80},{350,-40}}),
        iconTransformation(extent={{300,-90},{340,-50}})));

  Modelica.Blocks.Interfaces.BooleanOutput V3V_extra_out(start=false)
    "Open to solar panels = true, Open to extra tank = false"
    annotation (Placement(transformation(extent={{310,10},{350,50}}),
        iconTransformation(extent={{300,42},{338,80}})));
  Utilities.Logical.True_seeker true_seeker(n=n)
    annotation (Placement(transformation(extent={{252,-226},{292,-194}})));

  Modelica.Blocks.Interfaces.BooleanInput CHAUFF[n] "ON = true, OFF = false"
    annotation (Placement(transformation(extent={{-40,78},{0,118}}),
        iconTransformation(extent={{-114,-116},{-80,-82}})));
  Modelica.Blocks.Routing.Replicator replicator1(
                                                nout=n) annotation (
      Placement(transformation(extent={{-64,80},{-44,100}})));
  Modelica.Blocks.Sources.RealExpression cst1[n](each y=0.2)
    annotation (Placement(transformation(extent={{-6,42},{6,52}})));
  Modelica.Blocks.Math.Add add1[n](each k1=1, each k2=1)
    "TsolarInstruction + 0.2"
    annotation (Placement(transformation(extent={{18,-200},{38,-180}})));
  Modelica.Blocks.Logical.Pre breaker[n](each pre_u_start=true)
    "Break algebraic loop to avoid infinite loop" annotation (Placement(
        transformation(
        extent={{-8,-8},{8,8}},
        rotation=0,
        origin={100,98})));
  Modelica.Blocks.Math.BooleanToReal CHAUFF_real[n] annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={22,80})));
  Modelica.Blocks.Math.Add add3[n](           each k2=1, each k1=0.25)
    "TsolarInstruction + 0.2"
    annotation (Placement(transformation(extent={{22,-156},{42,-136}})));
  Utilities.Timers.Stay_on stay_on[n](each pause=60, out_value(each start=false))
    annotation (Placement(transformation(extent={{280,-68},{296,-52}})));
  Modelica.Blocks.Math.Max max_28_T7
    "Return the minimal value between T3 and T4"
    annotation (Placement(transformation(extent={{-70,-132},{-52,-114}})));
  Modelica.Blocks.Sources.RealExpression cst3(each y=28 + 273.15)
    annotation (Placement(transformation(extent={{-100,-126},{-78,-110}})));
  Modelica.Blocks.Math.Add add4(each k2=1, each k1=1) "TsolarInstruction + 0.2"
    annotation (Placement(transformation(extent={{-14,-128},{2,-112}})));
  Modelica.Blocks.Sources.RealExpression cst4(each y=5)
    annotation (Placement(transformation(extent={{-88,-166},{-66,-150}})));
  Modelica.Blocks.Routing.Replicator replicator2(
                                                nout=n) annotation (
      Placement(transformation(extent={{20,-128},{36,-112}})));
equation
  connect(V3V_extra, replicator3.u)
                                   annotation (Line(
      points={{-100,10},{-62,10}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator3.y, not_extra.u)
                                     annotation (Line(
      points={{-39,10},{-30,10},{-30,-20},{-7.2,-20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(not_extra.y, on1.u2)     annotation (Line(
      points={{6.6,-20},{120,-20},{120,82},{138,82}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tambiant, deltaT_2_test.u1)
                                 annotation (Line(
      points={{-100,130},{48,130},{48,50},{58,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant,delta_T3. u1) annotation (Line(
      points={{-100,130},{48,130},{48,10},{58,10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_T3.y, on3.u[1])      annotation (Line(
      points={{81,10},{106,10},{106,15.25},{140,15.25}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator3.y, on3.u[2])      annotation (Line(
      points={{-39,10},{-30,10},{-30,-8},{116,-8},{116,11.75},{140,
          11.75}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_solar, replicator4.u)   annotation (Line(
      points={{-100,-30},{-62,-30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator4.y, on3.u[3])      annotation (Line(
      points={{-39,-30},{128,-30},{128,8.25},{140,8.25}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on1.y, all_on.u[1])        annotation (Line(
      points={{161,90},{168,90},{168,54.6667},{180,54.6667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on2.y, all_on.u[2])        annotation (Line(
      points={{161.5,50},{180,50}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on3.y, all_on.u[3])        annotation (Line(
      points={{161.5,10},{168,10},{168,45.3333},{180,45.3333}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tambiant, deltaT_3_test.u1)
                                  annotation (Line(
      points={{-100,130},{48,130},{48,-158},{58,-158}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(deltaT_3_test.y, off1.u2)  annotation (Line(
      points={{81,-158},{138,-158}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(off1.y, not_off1.u)       annotation (Line(
      points={{161,-150},{178,-150}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tambiant, deltaT_test.u1)
                                  annotation (Line(
      points={{-100,130},{48,130},{48,-198},{58,-198}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(deltaT_test.y, or1.u1)     annotation (Line(
      points={{81,-198},{98,-198}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(deltaT_5.y, deltaT_4_test.u)
                                    annotation (Line(
      points={{-38.3,-230},{-12,-230}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T8, deltaT_5.u[1])  annotation (Line(
      points={{-102,-220},{-70,-220},{-70,-226.5},{-60,-226.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7, deltaT_5.u[2])  annotation (Line(
      points={{-102,-250},{-68,-250},{-68,-234},{-60,-234},{-60,-233.5}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(replicator9.y, or1.u2)       annotation (Line(
      points={{81,-230},{92,-230},{92,-206},{98,-206}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator3.y, off2.u[1])      annotation (Line(
      points={{-39,10},{-30,10},{-30,-50},{120,-50},{120,-172},{130,-172},{130,
          -193.333},{140,-193.333}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator4.y,not_solar. u) annotation (Line(
      points={{-39,-30},{-22,-30},{-22,-42},{-7.2,-42}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(not_solar.y, off2.u[2])      annotation (Line(
      points={{6.6,-42},{116,-42},{116,-176},{128,-176},{128,-198},{140,
          -198}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(or1.y, off2.u[3])            annotation (Line(
      points={{121,-198},{124,-198},{124,-202.667},{140,-202.667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(off2.y, not_off2.u)       annotation (Line(
      points={{161.5,-198},{178,-198}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(all_on.y, final_state.u[1]) annotation (Line(
      points={{201.5,50},{220,50},{220,-53.7},{240,-53.7}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(not_off1.y, final_state.u[2])
                                       annotation (Line(
      points={{201,-150},{216,-150},{216,-58},{234,-58},{234,-57.9},{240,-57.9}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(not_off2.y, final_state.u[3])
                                       annotation (Line(
      points={{201,-198},{226,-198},{226,-62},{240,-62},{240,-62.1}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(deltaT_2_test.y, on2.u[1]) annotation (Line(
      points={{81,50},{112,50},{112,55.25},{140,55.25}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator3.y, on2.u[2])      annotation (Line(
      points={{-39,10},{-30,10},{-30,-8},{116,-8},{116,51.75},{140,
          51.75}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(not_solar.y, on2.u[3])      annotation (Line(
      points={{6.6,-42},{124,-42},{124,48.25},{140,48.25}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator5.y, T5_test.u)   annotation (Line(
      points={{41,-70},{58,-70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5_test.y, on2.u[4])        annotation (Line(
      points={{81,-70},{132,-70},{132,44.75},{140,44.75}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T1_test.y, on3.u[4])        annotation (Line(
      points={{81,-110},{136,-110},{136,4.75},{140,4.75}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator9.y, or2.u1)       annotation (Line(
      points={{81,-230},{92,-230},{92,-270},{98,-270}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tambiant, add_test.u1)  annotation (Line(
      points={{-100,130},{48,130},{48,-278},{56,-278}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add_test.y, or2.u2)        annotation (Line(
      points={{79,-278},{98,-278}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator3.y, off3.u[1])      annotation (Line(
      points={{-39,10},{-30,10},{-30,-50},{120,-50},{120,-172},{130,-172},{130,
          -265.333},{140,-265.333}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(replicator4.y, off3.u[2])      annotation (Line(
      points={{-39,-30},{128,-30},{128,-270},{140,-270}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(or2.y, off3.u[3])            annotation (Line(
      points={{121,-270},{130,-270},{130,-274.667},{140,-274.667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(off3.y, not_off3.u)       annotation (Line(
      points={{161.5,-270},{178,-270}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(not_off3.y, final_state.u[4])
                                       annotation (Line(
      points={{201,-270},{232,-270},{232,-66.3},{240,-66.3}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T5,replicator5. u) annotation (Line(
      points={{-100,-70},{18,-70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(V3V_extra_pass.y, V3V_extra_out) annotation (Line(
      points={{261,30},{330,30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_extra, V3V_extra_pass.u) annotation (Line(
      points={{-100,10},{-74,10},{-74,30},{238,30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Tambiant, Tambiant_pass.u) annotation (Line(
      points={{-100,130},{238,130}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tambiant_pass.y, Tambiant_out) annotation (Line(
      points={{261,130},{328,130}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(deltaT_4_test.y, wait_for.in_value) annotation (Line(
      points={{11,-230},{20,-230}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(wait_for.out_value, replicator9.u) annotation (Line(
      points={{41.4,-230},{58,-230}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(true_seeker.y, pumpControl_S5) annotation (Line(
      points={{292,-210},{332,-210}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on3.y, true_seeker.u) annotation (Line(
      points={{161.5,10},{210,10},{210,-210},{252,-210}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(TsolarInstruction, delta_T3.u2) annotation (Line(
      points={{-100,50},{-26,50},{-26,2},{58,2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add2.y, add_test.u2) annotation (Line(
      points={{11,-278},{31.5,-278},{31.5,-286},{56,-286}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst2.y, add2.u2) annotation (Line(
      points={{-43.1,-263},{-30,-263},{-30,-284},{-12,-284}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TsolarInstruction, add2.u1) annotation (Line(
      points={{-100,50},{-26,50},{-26,-272},{-12,-272}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(deltaT_2.y, deltaT_2_test.u2) annotation (Line(
      points={{38.9,47},{44,47},{44,42},{58,42}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(DTeco, replicator1.u) annotation (Line(
      points={{-100,90},{-66,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(replicator1.y, deltaT_2.u3) annotation (Line(
      points={{-43,90},{-34,90},{-34,39.8},{18.2,39.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction_kelvin2.y, deltaT_2.u1) annotation (Line(
      points={{-58.6,-180},{-20,-180},{-20,54.2},{18.2,54.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst1.y, deltaT_2.u2) annotation (Line(
      points={{6.6,47},{18.2,47}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction_kelvin2.y,deltaT_4. u1) annotation (Line(
      points={{-58.6,-180},{-20,-180},{-20,-144},{-14,-144},{-14,-143.8},{-9.8,-143.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(replicator1.y,deltaT_4. u3) annotation (Line(
      points={{-43,90},{-34,90},{-34,-158.2},{-9.8,-158.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst2.y,deltaT_4. u2) annotation (Line(
      points={{-43.1,-263},{-30,-263},{-30,-151},{-9.8,-151}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinstruction_kelvin2.y, add1.u1) annotation (Line(
      points={{-58.6,-180},{-24,-180},{-24,-184},{16,-184}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst2.y, add1.u2) annotation (Line(
      points={{-43.1,-263},{-30,-263},{-30,-196},{16,-196}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add1.y, deltaT_test.u2) annotation (Line(
      points={{39,-190},{44,-190},{44,-206},{58,-206}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(breaker.y, on1.u1) annotation (Line(
      points={{108.8,98},{124,98},{124,90},{138,90}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(CHAUFF, breaker.u) annotation (Line(
      points={{-20,98},{90.4,98}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(not_extra.y, off1.u1) annotation (Line(
      points={{6.6,-20},{100,-20},{100,-150},{138,-150}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(CHAUFF, CHAUFF_real.u) annotation (Line(
      points={{-20,98},{0,98},{0,80},{10,80}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(add3.y, deltaT_3_test.u2) annotation (Line(
      points={{43,-146},{44,-146},{44,-166},{58,-166}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(CHAUFF_real.y, add3.u1) annotation (Line(
      points={{33,80},{42,80},{42,64},{8,64},{8,-130},{14,-130},{14,-140},{20,-140}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(deltaT_4.y, add3.u2) annotation (Line(
      points={{10.9,-151},{14.45,-151},{14.45,-152},{20,-152}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(final_state.y, stay_on.in_value) annotation (Line(
      points={{265.8,-60},{280,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(stay_on.out_value, pumpControl_Sj) annotation (Line(
      points={{297.6,-60},{330,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T7, max_28_T7.u2) annotation (Line(
      points={{-102,-250},{-100,-250},{-100,-128.4},{-71.8,-128.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst3.y, max_28_T7.u1) annotation (Line(
      points={{-76.9,-118},{-74,-118},{-74,-117.6},{-71.8,-117.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(max_28_T7.y, add4.u1) annotation (Line(
      points={{-51.1,-123},{-46,-123},{-46,-115.2},{-15.6,-115.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cst4.y, add4.u2) annotation (Line(
      points={{-64.9,-158},{-60,-158},{-60,-134},{-40,-134},{-40,-124.8},{-15.6,
          -124.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(replicator6.y, T1_test.u1) annotation (Line(
      points={{36.8,-96},{48,-96},{48,-110},{58,-110}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(replicator2.y, T1_test.u2) annotation (Line(
      points={{36.8,-120},{48,-120},{48,-118},{58,-118}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add4.y, replicator2.u) annotation (Line(
      points={{2.8,-120},{18.4,-120}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, replicator6.u) annotation (Line(
      points={{-100,-100},{13,-100},{13,-96},{18.4,-96}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-300},{320,140}},
          preserveAspectRatio=false),  graphics), Icon(coordinateSystem(extent={{-100,
            -300},{320,140}}, preserveAspectRatio=false),  graphics={
        Text(
          extent={{-58,24},{284,-236}},
          lineColor={0,128,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          textString="Nb Room
%n"),   Polygon(
          points={{-100,140},{320,140},{320,-300},{-100,-300},{-100,140},{-100,140},
              {-100,140}},
          lineColor={255,0,255},
          smooth=Smooth.None)}));
end Heating_mod;
