within SolarSystem.Control.SolisArt_mod;
model SolarValve_mod
  "Lock or unlock the solar valve (true = open to solar, false = open to storage tank)"

  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-120,70},{-80,110}})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-120,10},{-80,50}})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
    annotation (Placement(transformation(extent={{-120,-50},{-80,-10}})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-120,-110},{-80,-70}})));
  Modelica.Blocks.Interfaces.BooleanOutput V3V_solar(start=true)
    "Open to solar panels = true, Open to storage tank = false"
    annotation (Placement(transformation(extent={{80,-20},{120,20}})));
  Modelica.Blocks.Math.Min min_T3_T4
    "Return the minimal value between T3 and T4"
    annotation (Placement(transformation(extent={{-40,0},{-20,20}})));
  Modelica.Blocks.Logical.GreaterEqual T1_greaterEqual_test
    "T1 is greater or equal to (T3 or T4)"
    annotation (Placement(transformation(extent={{20,40},{40,60}})));
  Modelica.Blocks.Logical.Or conditions "true if one input is true"
    annotation (Placement(transformation(extent={{54,-10},{74,10}})));
  Modelica.Blocks.Logical.And pre_conditions
    "If S4 flow rate is minimum and deltaT_test false, it's false otherwise it's true "
    annotation (Placement(transformation(extent={{20,-60},{40,-40}})));
  Modelica.Blocks.Logical.Greater T1_greater_test
    "T1 is greater or equal to (T3 or T4)"
    annotation (Placement(transformation(extent={{-12,-92},{8,-72}})));
  Modelica.Blocks.Logical.GreaterEqual          T3_greaterEqual_test
    "Test if T3 is greater or equal to threshold"
    annotation (Placement(transformation(extent={{-12,-50},{8,-30}})));
  Modelica.Blocks.Math.BooleanToReal V3V_solar_real annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={10,80})));
  Modelica.Blocks.Math.MultiSum compare1(nu=2, k={1,-2})
    annotation (Placement(transformation(extent={{-8,-6},{8,10}})));
  Modelica.Blocks.Math.MultiSum compare2(nu=2, k={1,-2})
    annotation (Placement(transformation(extent={{-48,-108},{-32,-92}})));
  Modelica.Blocks.Sources.RealExpression compare3(y=273.15 + 30 - 2*
        V3V_solar_real.y)
    annotation (Placement(transformation(extent={{-50,-64},{-30,-44}})));
  Modelica.Blocks.Logical.Pre breaker "Cut loop to avoid infinite loop"
    annotation (Placement(transformation(extent={{72,70},{52,90}})));
equation
  connect(T3, min_T3_T4.u1)
                       annotation (Line(
      points={{-100,30},{-72,30},{-72,16},{-42,16}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, min_T3_T4.u2)
                       annotation (Line(
      points={{-100,-30},{-50,-30},{-50,4},{-42,4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, T1_greaterEqual_test.u1)
                              annotation (Line(
      points={{-100,90},{-60,90},{-60,50},{18,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1_greaterEqual_test.y, conditions.u1)
                                         annotation (Line(
      points={{41,50},{46,50},{46,0},{52,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(conditions.y, V3V_solar)    annotation (Line(
      points={{75,0},{100,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pre_conditions.y, conditions.u2)
                                         annotation (Line(
      points={{41,-50},{46,-50},{46,-8},{52,-8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T1, T1_greater_test.u1)
                              annotation (Line(
      points={{-100,90},{-60,90},{-60,-82},{-14,-82}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1_greater_test.y, pre_conditions.u2)
                                         annotation (Line(
      points={{9,-82},{12,-82},{12,-58},{18,-58}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T3_greaterEqual_test.y, pre_conditions.u1)
                                         annotation (Line(
      points={{9,-40},{12,-40},{12,-50},{18,-50}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(min_T3_T4.y, compare1.u[1]) annotation (Line(
      points={{-19,10},{-12,10},{-12,4.8},{-8,4.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(V3V_solar_real.y, compare1.u[2]) annotation (Line(
      points={{-1,80},{-16,80},{-16,-0.8},{-8,-0.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(compare1.y, T1_greaterEqual_test.u2) annotation (Line(
      points={{9.36,2},{14,2},{14,42},{18,42}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(compare2.y, T1_greater_test.u2) annotation (Line(
      points={{-30.64,-100},{-20,-100},{-20,-90},{-14,-90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, compare2.u[1]) annotation (Line(
      points={{-100,-90},{-74,-90},{-74,-97.2},{-48,-97.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(V3V_solar_real.y, compare2.u[2]) annotation (Line(
      points={{-1,80},{-66,80},{-66,-102.8},{-48,-102.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, T3_greaterEqual_test.u1) annotation (Line(
      points={{-100,30},{-72,30},{-72,-34},{-22,-34},{-22,-40},{-14,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(compare3.y, T3_greaterEqual_test.u2) annotation (Line(
      points={{-29,-54},{-22,-54},{-22,-48},{-14,-48}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(breaker.y, V3V_solar_real.u) annotation (Line(
      points={{51,80},{22,80}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_solar, breaker.u) annotation (Line(
      points={{100,0},{140,0},{140,80},{74,80}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-120},{100,120}},
          preserveAspectRatio=false),
                      graphics), Icon(coordinateSystem(extent={{-100,
            -120},{100,120}}),        graphics={
        Polygon(
          points={{-98,120},{100,120},{100,-120},{-100,-120},{-100,118},
              {-100,120},{-98,120}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-84,48},{84,-54}},
          lineColor={0,128,0},
          textString="Solar
valve control")}));
end SolarValve_mod;
