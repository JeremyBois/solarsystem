within SolarSystem.Control.SolisArt_mod;
model UserGuide "What can you expect from this package"
  extends Modelica.Icons.Information;
  annotation (Documentation(info="<html>
<p></font><font style=\"font-size: 7pt; \">&nbsp; </p>
<h4><span style=\"color:#008000\">Pilotage du chauffage solaire collectif : Solis Confort </span></h4>
<p><b>&nbsp;</b> </p>
<p><b></font><font style=\"font-size: 10pt; \">Zone &agrave; chauffer</b> (Sj avec j=1 ou 2): prendre des radiateurs basse temp&eacute;rature pour simplifier. </p>
<p><b>Sj :</b> ON Si CHAUFF=1 et V3Vappoint orient&eacute;e vers l&rsquo;appoint </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ON Si (Tambiante&LT;Tconsigne+0.2-DTeco) et V3Vappoint orient&eacute;e vers le solaire et V3Vsolaire </p>
<p>orient&eacute;e vers le ballon chauffage et T5&GT;=28&deg;C et anti court cycle Sj pendant 1mn. </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ON Si (Tambiante&LT;Tconsigne solaire) et V3Vappoint et solaire orient&eacute;es vers les capteurs </p>
<p>solaires et T1&GT;28&deg;C et anti court cycle Sj, S3 et S5 pendant 1mn </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OFF Si (Tambiante&GT;Tconsigne+0.3&deg;C-2*DTeco) et V3Vappoint orient&eacute;e vers l&rsquo;appoint </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OFF Si (Tambiante&GT;Tconsigne+0.3&deg;C ou T8-T7&LT;2,5&deg;C pendant 1,5mn) et V3Vappoint orient&eacute;e </p>
<p>vers le solaire et V3Vsolaire orient&eacute;e vers le ballon chauffage </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OFF Si (Tambiante&GT;Tconsigne solaire+0.3&deg;C ou T8-T7&LT;2,5&deg;C pendant 1,5mn) et V3Vappoint </p>
<p>orient&eacute;e vers le solaire et V3Vsolaire orient&eacute;e vers le solaire </p>
<p>&nbsp; </p>
<p>DTeco=0.3 si T1&GT;=T7+5-15*DTeco. Sinon DTeco=0 </p>
<p>Tconsigne solaire= Tconsigne+ 3-(Text mini +Text)/18 + Ski&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (avec Ski=0.3 si Si&GT;0 et Ski=0 sinon) </p>
<p>CHAUFF=1 si Tambiante&LT;Tconsigne-DTeco+CHAUFF/4 et ECS=0 </p>
<p>CHAUFF=0 sinon. </p>
<p>ECS=1 (priorit&eacute; ECS) si T4&LT;=55&deg;C+5*ECS et 0 sinon </p>
<p>ECS=0 aussi si T3&GT;54&deg;C+2*ECS </p>
<p>&nbsp; </p>
<p>Si plancher chauffant&nbsp;: La T. max de d&eacute;part dans les planchers est de 42&deg;C. </p>
<p>&nbsp; </p>
<p><b>V3V</b> </p>
<p><b>V3Vsolaire</b>=1 (ouverte sur le solaire) si T1&GT;=MIN(T3,T4)-2*V3Vsolaire ou si (T1&GT;T5-2*V3Vsolaire et T3&GT;=30&deg;C-2*V3Vsolaire) </p>
<p>=ouverte vers le ballon chauffage sinon </p>
<p><b>V3Vappoint</b>=0 vers l&rsquo;appoint si ECS=1 ou CHAUFF=1 </p>
<p>=1 vers le solaire si (ECS=0 et CHAUFF=0) </p>
<p>=1 vers le solaire et ECS=0 si (ECS=1 et T1&GT;T4+11-8*V3Vappoint et T4&GT;=55-V3Vappoint) </p>
<p>=1 vers le solaire et CHAUFF=0 si (ECS=0 et CHAUFF=1 et T1&GT;T7+10 et </p>
<p>&nbsp;&nbsp; Tamb&GT;Tconsigne-0,1&deg;C) </p>
<p><b>V3Vbouclage</b>=bascule cot&eacute; ballon appoint si T14&LT;=T16+2&deg;C ou si T10&GT;=60&deg;C </p>
<p>=bascule cot&eacute; ballon solaire si T14&GT;=T16+5&deg;C et si T10&LT;=58&deg;C </p>
<p>&nbsp; </p>
<p>&nbsp; </p>
<p><b>Ballon appoint :</b> </p>
<p><b>S4</b> ON si T1-T4&GT;=10 et V3Vappoint ouverte &agrave; 100&percnt; vers le solaire </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ON si ECS=1 </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OFF si T1-T4&LT;=4 et vitesse circulateur=d&eacute;bit mini&nbsp;&nbsp;&nbsp;&nbsp; </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;OFF et ECS=0 si ECS=1 et (T4&GT;=60 ou (T3&GT;=50 et T4&GT;56)) </p>
<p>&nbsp; </p>
<p><b>Ballon solaire :</b> </p>
<p><b>S5</b> : ON si T1-T3&GT;=10 et V3V solaire ouverte &agrave; 100&percnt; vers les capteurs, </p>
<p>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OFF si T1-T3&LT;=4 ou si T3&GT;=90&deg;C </p>
<p>&nbsp; </p>
<p><b>Ballon stockage :</b> </p>
<p><b>S6 </b>: ON si T1-T5&GT;=6 et T5&LT;100&deg;C et T3&GT;=30&deg;C </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OFF si T1-T5&LT;=2.5 ou si T5&GT;=105 ou si T3&LT;=28&deg;C </p>
<p>&nbsp; </p>
<p><b>Circulateur du retour de bouclage sanitaire</b> </p>
<p><b>S7</b> : en d&eacute;bit variable pour avoir T16=50&deg;C </p>
<p>&nbsp; </p>
<p><b>Chaudi&egrave;re Appoint</b> </p>
<p>ON : (si ECS=1 ou si (CHAUFF=1 et T8&LT;=30)) et V3Vappoint orient&eacute;e vers l&rsquo;appoint </p>
<p>OFF: (si ECS=0 et (CHAUFF=0 ou (CHAUFF=1 et T8&GT;=35)) ou si V3Vappoint orient&eacute;e </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; vers le solaire </p>
<p><br><img src=\"modelica://SolarSystem/../../../Dymola/SolisArt/Doc/SolisConfort.png\"/></p>
<p><br>--------------------------------------------------------------------------------------------------------------------------------------------------------------------</p>
<h4><span style=\"color:#008000\">Enclenchement des pompes sur le chauffage:</span></h4>
<p><br><h4>Deux cas de fonctionnement :</h4></p>
<p>1. Utilisation de l&apos;appoint (avec ou sans utilisation des panneaux solaires)</p>
<p>----&GT; V3V appoint ouverte vers l&apos;appoint</p>
<p>2. Utilisation des panneaux solaires uniquement</p>
<p>----&GT; V3V appoint ouverte vers les panneaux solaires</p>
<p><br><h4>Cas 1 :</h4></p>
<p>Le d&eacute;bit nominal de chaques pompes est de 40 l/h/m2 auxquels s&apos;ajoutent 20 l/h/m2</p>
<p>pour chaque nouvelle pompe en fonctionnement.</p>
<p>Ce d&eacute;bit est ensuite r&eacute;parti &eacute;quitablement entre les diff&eacute;rentes pompes. On obtient</p>
<p>ainsi le d&eacute;bit maximum pour chaque pompe.</p>
<p>Ex : 40 l/h/m2 si une pompe en fonctionnement</p>
<p>30 l/h/m2 si deux pompes en fonctionnement</p>
<p>Chaque pompe est limit&eacute;e &agrave; 50 &percnt; de ce d&eacute;bit maximum au d&eacute;marrage durant 3min.</p>
<p><br><h4>Cas 2 :</h4></p>
<p>Le d&eacute;bit nominal de chaques pompes est de 70 l/h/m2 auxquels s&apos;ajoutent 35 l/h/m2</p>
<p>pour chaque nouvelle pompe en fonctionnement, &agrave; l&apos;exception des pompes S6 et S5 qui</p>
<p>elles conservent le fonctionnement du cas 1.</p>
<p>Comme pour le cas 1 on r&eacute;partie les d&eacute;bits sur les diff&eacute;rentes pompes en</p>
<p>fonctionnement afin d&apos;obtenir le d&eacute;bit maximum pour chaque pompe.</p>
<p>Il n&apos; y plus de limitation de d&eacute;bit au d&eacute;marrage des pompes &agrave; l&apos;exception des pompes</p>
<p>S6 et S5 qui elles conservent la modulation du d&eacute;bit.</p>
<p><br><h4>Cas 1 et cas 2 :</h4></p>
<p>Dans les 2 cas les pompes de chauffage s&apos;enchenchent en escalier.</p>
<p>La pompe suivante ne d&eacute;marre que si la pompe pr&eacute;c&eacute;dente est en fonctionnement</p>
<p>depuis au minimum 5min, et que les conditions pour d&eacute;marrer la seconde pompe sont</p>
<p>toujours atteintes.</p>
<p>Apr&egrave;s avoir d&eacute;termin&eacute; le d&eacute;bit maximum pour chaque pompe, le d&eacute;bit est r&eacute;gul&eacute; gr&acirc;ce</p>
<p>&agrave; un PID.</p>
</html>"));
end UserGuide;
