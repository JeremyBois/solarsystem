within SolarSystem.Control.SolisArt_mod;
model storageTank_mod "SolisArt S6 pump control"

  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-120,50},{-80,90}})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-120,-50},{-80,-10}}),
        iconTransformation(extent={{-120,-50},{-80,-10}})));
  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S6(start=false)
    "ON = true, OFF = false (pump S6)"
    annotation (Placement(transformation(extent={{120,-40},{160,0}})));
  Modelica.Blocks.Math.MultiSum delta_T(nu=2, k={1,-1}) "T1 - T5"
    annotation (Placement(transformation(extent={{-52,40},{-32,60}})));
  Modelica.Blocks.Logical.Hysteresis deltaT_hys_test(
    y(start=false),
    uLow=2.5,
    uHigh=10) "Test T1 - T5"
    annotation (Placement(transformation(extent={{-20,40},{0,60}})));
  Modelica.Blocks.Logical.OnOffController onOffController_T5(bandwidth=6)
    annotation (Placement(transformation(extent={{-20,0},{0,20}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=273.15 + 105)
    annotation (Placement(transformation(extent={{-52,6},{-32,26}})));
  Modelica.Blocks.MathBoolean.And conditions(nu=3)
    "true if all true, else false"
    annotation (Placement(transformation(extent={{42,-28},{58,-12}})));
  Modelica.Blocks.Logical.Hysteresis T3_hys_test(uLow=273.15 + 28, uHigh=273.15
         + 30)
    annotation (Placement(transformation(extent={{-20,-40},{0,-20}})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-120,0},{-80,40}}),
        iconTransformation(extent={{-120,0},{-80,40}})));
equation
  connect(delta_T.y, deltaT_hys_test.u)
                                    annotation (Line(
      points={{-30.3,50},{-22,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, delta_T.u[1]) annotation (Line(
      points={{-100,70},{-80,70},{-80,53.5},{-52,53.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, delta_T.u[2]) annotation (Line(
      points={{-100,-30},{-80,-30},{-80,46.5},{-52,46.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(realExpression.y, onOffController_T5.reference) annotation (
      Line(
      points={{-31,16},{-22,16}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(conditions.y, pumpControl_S6) annotation (Line(
      points={{59.2,-20},{140,-20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T3, T3_hys_test.u)        annotation (Line(
      points={{-100,20},{-80,20},{-80,-30},{-22,-30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5, onOffController_T5.u) annotation (Line(
      points={{-100,-30},{-80,-30},{-80,4},{-22,4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(deltaT_hys_test.y, conditions.u[1])
                                          annotation (Line(
      points={{1,50},{20,50},{20,-16.2667},{42,-16.2667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(onOffController_T5.y, conditions.u[2]) annotation (Line(
      points={{1,10},{16,10},{16,-20},{42,-20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T3_hys_test.y, conditions.u[3])        annotation (Line(
      points={{1,-30},{20,-30},{20,-23.7333},{42,-23.7333}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-60},{140,120}},
          preserveAspectRatio=false), graphics), Icon(coordinateSystem(
          extent={{-100,-60},{140,120}},  preserveAspectRatio=false),
        graphics={
        Text(
          extent={{-66,88},{106,-32}},
          lineColor={0,128,0},
          textString="Storage Tank
pump control"),
        Polygon(
          points={{-100,120},{140,120},{140,-60},{-100,-60},{-100,118},
              {-100,120},{-100,120}},
          lineColor={255,0,255},
          smooth=Smooth.None)}));
end storageTank_mod;
