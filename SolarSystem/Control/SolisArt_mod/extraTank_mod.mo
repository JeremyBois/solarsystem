within SolarSystem.Control.SolisArt_mod;
model extraTank_mod "SolisArt S4 pump control"

  Modelica.Blocks.Interfaces.RealInput T1 "T. after solar panels"
    annotation (Placement(transformation(extent={{-120,50},{-80,90}})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
    annotation (Placement(transformation(extent={{-120,-10},{-80,30}})));
  Modelica.Blocks.Interfaces.BooleanOutput pumpControl_S4(start=true)
    "ON = true, OFF = false (pump S4)"
    annotation (Placement(transformation(extent={{320,20},{360,60}})));
  Modelica.Blocks.Math.MultiSum delta_T(nu=2, k={1,-1}) "T1 - T4"
    annotation (Placement(transformation(extent={{-56,40},{-36,60}})));
  Modelica.Blocks.Logical.Hysteresis deltaT_hys_test(
    y(start=false),
    uLow=4,
    uHigh=9.99) "Test T1 - T4"
    annotation (Placement(transformation(extent={{-20,40},{0,60}})));
  Modelica.Blocks.Interfaces.BooleanInput V3V_extra
    "Open to solar panels = true, Open to extra tank = false"
    annotation (Placement(transformation(extent={{-120,-150},{-80,-110}})));
  Modelica.Blocks.Interfaces.BooleanOutput ECS(start=true)
    "ON = true, OFF = false"
    annotation (Placement(transformation(extent={{320,-52},{360,-12}})));
  Modelica.Blocks.Interfaces.RealInput flow_S4 "S4 pump flow rate"
    annotation (Placement(transformation(extent={{-120,150},{-80,190}})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-120,90},{-80,130}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold
                                          onOffController_T4(threshold=273.15
         + 45)
    "True at start to always try to turn on the pump. Limits are 58 and 60.01"
    annotation (Placement(transformation(extent={{-20,14},{0,34}})));
  Modelica.Blocks.Logical.GreaterEqual     Test_T3_greater
    "T3 greater than threshold ?"
    annotation (Placement(transformation(extent={{-20,100},{0,120}})));
  Modelica.Blocks.Logical.Greater            flowMini_S4
    "S4 flow rate is flow rate minimum"
    annotation (Placement(transformation(extent={{-40,160},{-20,180}})));
  Modelica.Blocks.Logical.LessEqual          Test_T4_lowerEqual
    "T4 lower or equal to threshold ?"
    annotation (Placement(transformation(extent={{-20,-50},{0,-30}})));
  Modelica.Blocks.Logical.Pre breaker "Cut loop to avoid infinite loop"
    annotation (Placement(transformation(extent={{20,160},{40,180}})));
  Modelica.Blocks.MathBoolean.And on_conditions(nu=3)
    annotation (Placement(transformation(extent={{142,80},{162,100}})));
  Modelica.Blocks.Logical.Or S4_state
    annotation (Placement(transformation(extent={{280,30},{300,50}})));
  Modelica.Blocks.Logical.Or ECS_off_alt "Catch first or second ON"
    annotation (Placement(transformation(extent={{100,-80},{120,-60}})));
  Modelica.Blocks.Logical.And ECS_state "Catch last off"
    annotation (Placement(transformation(extent={{232,-42},{252,-22}})));
  Modelica.Blocks.Logical.Not ECS_opposite
    annotation (Placement(transformation(extent={{180,-80},{200,-60}})));
  Modelica.Blocks.Interfaces.RealInput mini_flow_S4 "S4 mini pump flow rate"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={-70,200})));
  Modelica.Blocks.Math.BooleanToReal ECS_real annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={130,-150})));
  Modelica.Blocks.Logical.Pre breaker1 "Cut loop to avoid infinite loop"
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=0,
        origin={310,-72})));
  Modelica.Blocks.Math.Gain          booleanToReal2(k=5)
                                                    annotation (Placement(
        transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={90,-150})));
  Modelica.Blocks.Math.MultiSum delta_ECS(k={1,1}, nu=2) "5*ECS + 55"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-50,-90})));
  Modelica.Blocks.Sources.RealExpression max_T4(y=273.15 + 40)
    "Max temp for T4" annotation (Placement(transformation(
        extent={{-10,-8},{10,8}},
        rotation=90,
        origin={-60,-150})));
  Modelica.Blocks.Sources.RealExpression compare(y=273.15 + 39 + 2*ECS_real.y)
    annotation (Placement(transformation(extent={{-74,86},{-36,106}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold
                                          onOffController_T1(threshold=273.15
         + 35)
    "True at start to always try to turn on the pump. Limits are 58 and 60.01"
    annotation (Placement(transformation(extent={{-20,128},{0,148}})));
  Modelica.Blocks.Logical.GreaterThreshold onOffController_T2(threshold=273.15
         + 41)
    "True at start to always try to turn on the pump. Limits are 58 and 60.01"
    annotation (Placement(transformation(extent={{-20,-18},{0,2}})));
  Modelica.Blocks.Logical.And on_ECS
    annotation (Placement(transformation(extent={{100,-42},{120,-22}})));
  Modelica.Blocks.Logical.Not Opposite2 "Reverse boolean to catch T4 >= 60"
    annotation (Placement(transformation(extent={{20,100},{40,120}})));
  Modelica.Blocks.Logical.And off1_conditions1 "Catch last off"
    annotation (Placement(transformation(extent={{60,-100},{80,-80}})));
  Modelica.Blocks.Logical.And ECS_off "Catch last off"
    annotation (Placement(transformation(extent={{140,-80},{160,-60}})));
equation
  connect(delta_T.y, deltaT_hys_test.u)
                                    annotation (Line(
      points={{-34.3,50},{-22,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, delta_T.u[1]) annotation (Line(
      points={{-100,70},{-80,70},{-80,53.5},{-56,53.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, delta_T.u[2]) annotation (Line(
      points={{-100,10},{-70,10},{-70,46.5},{-56,46.5}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(flowMini_S4.y, breaker.u)  annotation (Line(
      points={{-19,170},{18,170}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(flow_S4, flowMini_S4.u1) annotation (Line(
      points={{-100,170},{-42,170}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(mini_flow_S4, flowMini_S4.u2) annotation (Line(
      points={{-70,200},{-70,162},{-42,162}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(breaker1.y, ECS_real.u) annotation (Line(
      points={{299,-72},{280,-72},{280,-100},{160,-100},{160,-150},{142,
          -150}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(ECS, breaker1.u) annotation (Line(
      points={{340,-32},{340,-72},{322,-72}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T4, Test_T4_lowerEqual.u1) annotation (Line(
      points={{-100,10},{-70,10},{-70,-40},{-22,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ECS_real.y, booleanToReal2.u) annotation (Line(
      points={{119,-150},{102,-150}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(max_T4.y, delta_ECS.u[1]) annotation (Line(
      points={{-60,-139},{-60,-120},{-53.5,-120},{-53.5,-100}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(booleanToReal2.y, delta_ECS.u[2]) annotation (Line(
      points={{79,-150},{-46.5,-150},{-46.5,-100}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_ECS.y, Test_T4_lowerEqual.u2) annotation (Line(
      points={{-50,-78.3},{-50,-48},{-22,-48}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, Test_T3_greater.u1) annotation (Line(
      points={{-100,110},{-22,110}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(compare.y, Test_T3_greater.u2) annotation (Line(
      points={{-34.1,96},{-28,96},{-28,102},{-22,102}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, onOffController_T4.u) annotation (Line(
      points={{-100,10},{-70,10},{-70,24},{-22,24}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, onOffController_T1.u) annotation (Line(
      points={{-100,110},{-60,110},{-60,138},{-22,138}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, onOffController_T2.u) annotation (Line(
      points={{-100,10},{-70,10},{-70,-8},{-22,-8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Opposite2.u, Test_T3_greater.y) annotation (Line(
      points={{18,110},{1,110}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on_ECS.y, ECS_state.u1) annotation (Line(
      points={{121,-32},{230,-32}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(onOffController_T1.y, off1_conditions1.u1) annotation (Line(
      points={{1,138},{10,138},{10,-90},{58,-90}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(onOffController_T2.y, off1_conditions1.u2) annotation (Line(
      points={{1,-8},{10,-8},{10,-98},{58,-98}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(off1_conditions1.y, ECS_off_alt.u2) annotation (Line(
      points={{81,-90},{90,-90},{90,-78},{98,-78}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(onOffController_T4.y, ECS_off_alt.u1) annotation (Line(
      points={{1,24},{20,24},{20,-70},{98,-70}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(ECS_off_alt.y, ECS_off.u1) annotation (Line(
      points={{121,-70},{138,-70}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(ECS_off.y, ECS_opposite.u) annotation (Line(
      points={{161,-70},{178,-70}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(ECS_opposite.y, ECS_state.u2) annotation (Line(
      points={{201,-70},{220,-70},{220,-40},{230,-40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(ECS_state.y, ECS) annotation (Line(
      points={{253,-32},{340,-32}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(breaker.y, on_conditions.u[1]) annotation (Line(
      points={{41,170},{100,170},{100,94.6667},{142,94.6667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(deltaT_hys_test.y, on_conditions.u[2]) annotation (Line(
      points={{1,50},{80,50},{80,90},{142,90}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_extra, on_conditions.u[3]) annotation (Line(
      points={{-100,-130},{40,-130},{40,32},{100,32},{100,85.3333},{142,85.3333}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(ECS_state.y, S4_state.u2) annotation (Line(
      points={{253,-32},{260,-32},{260,32},{278,32}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(on_conditions.y, S4_state.u1) annotation (Line(
      points={{163.5,90},{260,90},{260,40},{278,40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(S4_state.y, pumpControl_S4) annotation (Line(
      points={{301,40},{340,40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Opposite2.y, on_ECS.u1) annotation (Line(
      points={{41,110},{60,110},{60,-32},{98,-32}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Test_T4_lowerEqual.y, on_ECS.u2) annotation (Line(
      points={{1,-40},{98,-40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(breaker1.y, ECS_off.u2) annotation (Line(
      points={{299,-72},{280,-72},{280,-100},{128,-100},{128,-78},{138,
          -78}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-160},{340,200}},
          preserveAspectRatio=false),graphics), Icon(coordinateSystem(extent={{-100,
            -160},{340,200}}, preserveAspectRatio=true), graphics={
        Text(
          extent={{-46,102},{260,-86}},
          lineColor={0,128,0},
          textString="Extra Tank
pump control"),
        Polygon(
          points={{-98,200},{340,200},{340,-160},{-100,-160},{-100,198},
              {-100,200},{-98,200}},
          lineColor={255,0,255},
          smooth=Smooth.None)}));
end extraTank_mod;
