within SolarSystem.Control.SolisArt_mod.Examples;
model extraTank_mod_TEST "

Test en cours"
  extends Modelica.Icons.Example;
  extraTank_mod extraTank_mod1(
    ECS(start=false),
    pumpControl_S4(start=false))
                               annotation (Placement(transformation(
          extent={{40,-20},{100,40}})));
  Modelica.Blocks.Sources.Ramp T4_mod(
    height=75,
    duration=800,
    offset=273.15 + 25) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-40,-32},{-20,-12}})));
  Modelica.Blocks.Sources.Ramp T1_mod(
    offset=273.15 + 25,
    height=75,
    duration=500) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-40,0},{-20,20}})));
  Modelica.Blocks.Sources.BooleanStep V3Vextra(startTime=500)
    annotation (Placement(transformation(extent={{-40,-66},{-20,-46}})));
  Modelica.Blocks.Sources.RealExpression T1(y=0.1) "Temp after solar panels"
    annotation (Placement(transformation(extent={{68,60},{48,80}})));
  Modelica.Blocks.Sources.Sine flow_mod(
    freqHz=1/500,
    offset=0,
    amplitude=0.2,
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-40,70},{-20,90}})));
  Modelica.Blocks.Sources.Ramp T3_mod(
    duration=800,
    offset=273.15 + 20,
    height=40) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-40,34},{-20,54}})));
equation
  connect(T4_mod.y, extraTank_mod1.T4) annotation (Line(
      points={{-19,-22},{20,-22},{20,8.33333},{40,8.33333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1_mod.y, extraTank_mod1.T1) annotation (Line(
      points={{-19,10},{0,10},{0,18.3333},{40,18.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(V3Vextra.y, extraTank_mod1.V3V_extra) annotation (Line(
      points={{-19,-56},{20,-56},{20,-15},{40,-15}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T1.y, extraTank_mod1.mini_flow_S4) annotation (Line(
      points={{47,70},{44.0909,70},{44.0909,40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(flow_mod.y, extraTank_mod1.flow_S4) annotation (Line(
      points={{-19,80},{20,80},{20,35},{40,35}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3_mod.y, extraTank_mod1.T3) annotation (Line(
      points={{-19,44},{0,44},{0,25},{40,25}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                      graphics),
    experiment(StopTime=1000),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end extraTank_mod_TEST;
