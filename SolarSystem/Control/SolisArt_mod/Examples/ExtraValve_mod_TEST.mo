within SolarSystem.Control.SolisArt_mod.Examples;
model ExtraValve_mod_TEST "

Test ok"
  extends Modelica.Icons.Example;

  ExtraValve_mod_one
                 V3Vextra_mod1(n=2, Tinstruction={293.15,293.15})
    annotation (Placement(transformation(extent={{40,-20},{94,40}})));
  Modelica.Blocks.Sources.Ramp T4_mod(
    height=75,
    duration=800,
    offset=273.15 + 25) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-40,50},{-20,70}})));
  Modelica.Blocks.Sources.Ramp T1_mod(
    offset=273.15 + 25,
    duration=500,
    height=75) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-40,80},{-20,100}})));
  Modelica.Blocks.Sources.BooleanPulse ECS(
    period=100,
    width=50,
    startTime=300)
    annotation (Placement(transformation(extent={{-40,-40},{-20,-20}})));
  Modelica.Blocks.Sources.Ramp T7_mod(
    duration=800,
    offset=273.15 + 20,
    height=40) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-40,20},{-20,40}})));
  Modelica.Blocks.Sources.BooleanStep CHAUFF1(startTime=600)
    annotation (Placement(transformation(extent={{-40,-70},{-20,-50}})));
  Modelica.Blocks.Sources.BooleanStep CHAUFF2(startTime=400, startValue=true)
    annotation (Placement(transformation(extent={{-40,-100},{-20,-80}})));
  Modelica.Blocks.Sources.Sine Tamb_mod[2](
    startTime={0,600},
    amplitude={10,10},
    offset={273.15 + 15,273.15 + 15},
    freqHz={1/250,1/250},
    phase={1.5707963267949,1.5707963267949})
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
equation
  connect(ECS.y, V3Vextra_mod1.ECS) annotation (Line(
      points={{-19,-30},{8,-30},{8,-5},{40,-5}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(CHAUFF1.y, V3Vextra_mod1.CHAUFF[1]) annotation (Line(
      points={{-19,-60},{28,-60},{28,-14.6667},{40,-14.6667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(CHAUFF2.y, V3Vextra_mod1.CHAUFF[2]) annotation (Line(
      points={{-19,-90},{20,-90},{20,-11.3333},{40,-11.3333}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T1_mod.y, V3Vextra_mod1.T1) annotation (Line(
      points={{-19,90},{20,90},{20,35},{40,35}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4_mod.y, V3Vextra_mod1.T4) annotation (Line(
      points={{-19,60},{10.5,60},{10.5,28.3333},{40,28.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7_mod.y, V3Vextra_mod1.T7) annotation (Line(
      points={{-19,30},{0,30},{0,16.6667},{40,16.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb_mod.y, V3Vextra_mod1.Tambiant) annotation (Line(
      points={{-19,0},{10,0},{10,3.33333},{40,3.33333}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                      graphics),
    experiment(StopTime=1000),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end ExtraValve_mod_TEST;
