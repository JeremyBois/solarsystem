within SolarSystem.Control.SolisArt_mod.Examples;
model SolarValve_mod_TEST "

Test en cours"
  extends Modelica.Icons.Example;

  SolarValve_mod V3Vsolar_mod1
    annotation (Placement(transformation(extent={{40,-24},{100,36}})));
  Modelica.Blocks.Sources.Ramp T5_mod(
    height=75,
    duration=800,
    offset=273.15 + 25) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-60,-70},{-40,-50}})));
  Modelica.Blocks.Sources.Ramp T4_mod(
    offset=273.15 + 25,
    duration=500,
    height=95) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-60,-30},{-40,-10}})));
  Modelica.Blocks.Sources.Sine T1_mod(
    freqHz=1/250,
    amplitude=60,
    offset=273.15 + 60,
    phase=1.5707963267949)
    annotation (Placement(transformation(extent={{-60,50},{-40,70}})));
  Modelica.Blocks.Sources.Ramp T3_mod(
    duration=800,
    offset=273.15 + 20,
    height=100) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-60,10},{-40,30}})));
equation
  connect(T1_mod.y, V3Vsolar_mod1.T1) annotation (Line(
      points={{-39,60},{20,60},{20,28.5},{40,28.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3_mod.y, V3Vsolar_mod1.T3) annotation (Line(
      points={{-39,20},{0,20},{0,13.5},{40,13.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4_mod.y, V3Vsolar_mod1.T4) annotation (Line(
      points={{-39,-20},{0,-20},{0,-1.5},{40,-1.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5_mod.y, V3Vsolar_mod1.T5) annotation (Line(
      points={{-39,-60},{20,-60},{20,-16.5},{40,-16.5}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                      graphics),
    experiment(StopTime=1000),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end SolarValve_mod_TEST;
