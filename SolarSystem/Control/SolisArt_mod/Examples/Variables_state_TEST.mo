within SolarSystem.Control.SolisArt_mod.Examples;
model Variables_state_TEST
  extends Modelica.Icons.Example;
  Variables_state variables_state
    annotation (Placement(transformation(extent={{18,-8},{82,26}})));
  Modelica.Blocks.Sources.RealExpression Text_mini(y=273.15 - 5)
                                                        annotation (
      Placement(transformation(extent={{-60,-30},{-40,-10}})));
  Modelica.Blocks.Sources.Ramp T1_mod(
    height=75,
    duration=800,
    offset=273.15 + 10) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
  Modelica.Blocks.Sources.Ramp T7_mod(
    offset=273.15 + 20,
    height=50,
    duration=600) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-60,30},{-40,50}})));
  Modelica.Blocks.Sources.Sine Text_mod(
    amplitude=15,
    freqHz=1/250,
    phase=1.5707963267949,
    offset=273.15 + 10,
    startTime=0)
    annotation (Placement(transformation(extent={{-60,0},{-40,20}})));
equation
  connect(Text_mini.y, variables_state.Text_mini) annotation (Line(
      points={{-39,-20},{-10,-20},{-10,-5.08571},{18,-5.08571}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1_mod.y, variables_state.T1) annotation (Line(
      points={{-39,70},{-12,70},{-12,23.0857},{18,23.0857}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7_mod.y, variables_state.T7) annotation (Line(
      points={{-39,40},{-12,40},{-12,13.3714},{18,13.3714}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text_mod.y, variables_state.Text) annotation (Line(
      points={{-39,10},{-12,10},{-12,4.14286},{18,4.14286}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                      graphics));
end Variables_state_TEST;
