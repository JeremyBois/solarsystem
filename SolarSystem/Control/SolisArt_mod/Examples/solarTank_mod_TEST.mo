within SolarSystem.Control.SolisArt_mod.Examples;
model solarTank_mod_TEST "

Test ok"
  extends Modelica.Icons.Example;
  solarTank_mod solarTank_mod1
    annotation (Placement(transformation(extent={{20,-20},{80,40}})));
  Modelica.Blocks.Sources.BooleanStep booleanStep(startTime=500)
    annotation (Placement(transformation(extent={{-80,-20},{-60,0}})));
  Modelica.Blocks.Sources.Ramp T3_mod(
    height=75,
    duration=800,
    offset=273.15 + 25) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-80,20},{-60,40}})));
  Modelica.Blocks.Sources.Ramp T1_mod(
    offset=273.15 + 25,
    height=75,
    duration=500) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-80,60},{-60,80}})));
equation
  connect(T1_mod.y, solarTank_mod1.T1) annotation (Line(
      points={{-59,70},{0,70},{0,27.5},{20,27.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3_mod.y, solarTank_mod1.T3) annotation (Line(
      points={{-59,30},{-20,30},{-20,17.5},{20,17.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(booleanStep.y, solarTank_mod1.V3V_solar) annotation (Line(
      points={{-59,-10},{0,-10},{0,-7.5},{20,-7.5}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                      graphics),
    experiment(StopTime=1000),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end solarTank_mod_TEST;
