within SolarSystem.Control.SolisArt_mod.Examples;
model BackupHeater_mod_TEST "

Test ok"
  extends Modelica.Icons.Example;

  BackupHeater_mod backup_mod1
    annotation (Placement(transformation(extent={{40,-24},{100,50}})));
  Modelica.Blocks.Sources.Ramp T8_mod(
    height=20,
    duration=800,
    offset=273.15 + 20) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-40,40},{-20,60}})));
  Modelica.Blocks.Sources.BooleanPulse ECS(
    period=100,
    width=50,
    startTime=300)
    annotation (Placement(transformation(extent={{-40,-40},{-20,-20}})));
  Modelica.Blocks.Sources.BooleanStep V3Vextra(startTime=600)
    annotation (Placement(transformation(extent={{-40,0},{-20,20}})));
  Modelica.Blocks.Sources.BooleanStep CHAUFF(startTime=400, startValue=true)
    annotation (Placement(transformation(extent={{-40,-80},{-20,-60}})));
equation
  connect(T8_mod.y, backup_mod1.T8) annotation (Line(
      points={{-19,50},{0,50},{0,38.6154},{40,38.6154}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(V3Vextra.y, backup_mod1.V3V_extra) annotation (Line(
      points={{-19,10},{0,10},{0,21.5385},{40,21.5385}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(ECS.y, backup_mod1.ECS) annotation (Line(
      points={{-19,-30},{10,-30},{10,4.46154},{40,4.46154}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(CHAUFF.y, backup_mod1.CHAUFF) annotation (Line(
      points={{-19,-70},{20,-70},{20,-12.6154},{40,-12.6154}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                      graphics),
    experiment(StopTime=1000),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end BackupHeater_mod_TEST;
