within SolarSystem.Control.SolisArt_mod.Examples;
model Flow_max_TEST
  extends Modelica.Icons.Example;

  parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states : true = ON, false = OFF)";
  Modelica.Blocks.Logical.LessThreshold Condition[n](each threshold=
        2.5) "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-40,-88},{-20,-68}})));
  Modelica.Blocks.Sources.Sine sine(
    amplitude=5,
    freqHz=1/500,
    offset=0,
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
  Modelica.Blocks.Sources.Pulse sine1(
    amplitude=5,
    offset=0,
    startTime=0,
    period=100)
    annotation (Placement(transformation(extent={{-100,22},{-80,42}})));
  Modelica.Blocks.Sources.Constant sine2(k=1)
    annotation (Placement(transformation(extent={{-100,-48},{-80,-28}})));
  Modelica.Blocks.Sources.BooleanStep booleanStep(startTime=500)
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Modelica.Blocks.Sources.Constant sine3(k=1)
    annotation (Placement(transformation(extent={{-100,52},{-80,72}})));
  Modelica.Blocks.Sources.Sine sine4(
    amplitude=5,
    freqHz=1/500,
    offset=0,
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-100,-88},{-80,-68}})));
  Modelica.Blocks.Sources.Constant sine5(k=3)
    annotation (Placement(transformation(extent={{-100,-120},{-80,-100}})));
  parameter Modelica.SIunits.MassFlowRate base[2]={0.0110,0.0192}
    "Base mass flow rate : [1] >> flow to panel, [2]  >> flow to backup";
  parameter Modelica.SIunits.MassFlowRate add[2]={0.0055,0.0096}
    "If more than one pump on : [1] >> flow to panel, [2]  >> flow to backup";
  parameter Integer nb_panel=1 "How many collector";
  parameter Modelica.SIunits.Area area_panel=1 "Collecting area";
  SolarSystem.Control.SolisArt_mod.Flow_max flow_out(
    base={1,2},
    add={0.5,1},
    temporizations={50},
    modulations={100,30,180,180,180},
    n=n,
    nb_panel=nb_panel,
    area_panel=area_panel)
    annotation (Placement(transformation(extent={{24,-18},{54,2}})));
  Modelica.Blocks.Logical.LessThreshold Condition1[3](each threshold=
        2.5) "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-40,22},{-20,42}})));
equation

  connect(Condition.y, flow_out.in_value) annotation (Line(
      points={{-19,-78},{0,-78},{0,-16},{24,-16}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(booleanStep.y, flow_out.V3V_extra) annotation (Line(
      points={{-79,90},{8,90},{8,0},{24,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(sine3.y, Condition1[1].u) annotation (Line(
      points={{-79,62},{-68,62},{-68,32},{-42,32}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Condition1.y, flow_out.in_value_other) annotation (Line(
      points={{-19,32},{0,32},{0,-8},{24,-8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(sine5.y, Condition[2].u) annotation (Line(
      points={{-79,-110},{-68,-110},{-68,-78},{-42,-78}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine3.y, Condition1[2].u) annotation (Line(
      points={{-79,62},{-60,62},{-60,32},{-42,32}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine2.y, Condition[1].u) annotation (Line(
      points={{-79,-38},{-60,-38},{-60,-78},{-42,-78}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine5.y, Condition1[3].u) annotation (Line(
      points={{-79,-110},{-68,-110},{-68,32},{-42,32}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-120},{140,100}},
          preserveAspectRatio=false),
                      graphics),
    Icon(coordinateSystem(extent={{-100,-120},{140,100}})),
    experiment(StopTime=1000),
    __Dymola_experimentSetupOutput);
end Flow_max_TEST;
