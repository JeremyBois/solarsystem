within SolarSystem.Control.SolisArt_mod.Examples;
model Chauff_state_TEST
  extends Modelica.Icons.Example;

  Chauff_state chauff_state(Tinstruction={293.15,303.15})
    annotation (Placement(transformation(extent={{40,-20},{100,40}})));
  Modelica.Blocks.Sources.Step DTeco_mod(
    height=0.3,
    offset=0,
    startTime=500) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-40,40},{-20,60}})));
  Modelica.Blocks.Sources.Sine Tamb_mod[2](
    startTime={0,600},
    amplitude={10,10},
    offset={273.15 + 15,273.15 + 15},
    freqHz={1/250,1/250},
    phase={1.5707963267949,1.5707963267949})
    annotation (Placement(transformation(extent={{-40,0},{-20,20}})));
  Modelica.Blocks.Sources.BooleanPulse ECS(
    period=100,
    width=50,
    startTime=300)
    annotation (Placement(transformation(extent={{-40,-40},{-20,-20}})));
equation
  connect(DTeco_mod.y, chauff_state.DTeco) annotation (Line(
      points={{-19,50},{0,50},{0,26.6667},{40,26.6667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb_mod.y, chauff_state.Tambiant) annotation (Line(
      points={{-19,10},{20,10},{20,6.66667},{40,6.66667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ECS.y, chauff_state.ECS) annotation (Line(
      points={{-19,-30},{0,-30},{0,-13.3333},{40,-13.3333}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                      graphics));
end Chauff_state_TEST;
