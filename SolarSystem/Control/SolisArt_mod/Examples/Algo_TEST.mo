within SolarSystem.Control.SolisArt_mod.Examples;
model Algo_TEST
  extends Modelica.Icons.Example;

  Modelica.Blocks.Sources.RealExpression T7(y=273.15 + 20)
    "Temp heat transmitters return"                  annotation (
      Placement(transformation(extent={{-100,8},{-80,28}})));
  Modelica.Blocks.Sources.RealExpression T1(y=273.15 + 100)
    "Temp after solar panels"
    annotation (Placement(transformation(extent={{-76,82},{-56,102}})));
  Modelica.Blocks.Sources.RealExpression Text(y=20 + 273.15)
    "outdoor temperature"
    annotation (Placement(transformation(extent={{-100,-112},{-80,-92}})));
  Modelica.Blocks.Sources.RealExpression Tamb_1(y=60 + 273.15) "Temp room 1"
    annotation (Placement(transformation(extent={{-100,-80},{-80,-60}})));
  Modelica.Blocks.Sources.RealExpression Tamb_2(y=10 + 273.15) "Temp room 2"
    annotation (Placement(transformation(extent={{-100,-96},{-80,-76}})));
  Modelica.Blocks.Sources.Ramp T7_mod(
    duration=1,
    height=15,
    offset=273.15 + 45) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-72,8},{-52,28}})));
  Modelica.Blocks.Sources.RealExpression T3(y=273.15 + 60)
    annotation (Placement(transformation(extent={{-76,70},{-56,90}})));
  Modelica.Blocks.Sources.RealExpression T4(y=273.15 + 60) "Tinside_extraTank"
    annotation (Placement(transformation(extent={{-100,48},{-80,68}})));
  Modelica.Blocks.Sources.Ramp T4_mod(
    duration=1,
    height=10.01,
    offset=273.15 + 50) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-76,50},{-56,70}})));
  Modelica.Blocks.Sources.RealExpression T5(y=273.15 + 110)
    "T. inside storage tank"
    annotation (Placement(transformation(extent={{-100,28},{-80,48}})));
  Modelica.Blocks.Sources.RealExpression T8(y=273.15 + 11)
    "T. at heating start point (just before extra exchanger input)"
    annotation (Placement(transformation(extent={{-100,-26},{-80,-6}})));
  Modelica.Blocks.Sources.Ramp Tamb_2_mod(
    duration=1,
    height=10,
    offset=273.15 + 28) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-72,-26},{-52,-6}})));
  Modelica.Blocks.Sources.RealExpression flow_S4(y=0.1) "S4_flowRate"
                                                     annotation (
      Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-12,130})));
  Algo algo(n=2)
            annotation (Placement(transformation(extent={{-34,-106},{
            126,100}})));
  pump_algo             pump_algo1(temporizations={300}, n=2)
    annotation (Placement(transformation(extent={{180,-68},{300,42}})));
  Modelica.Blocks.Math.RealToBoolean real_solar
                                           annotation (Placement(transformation(
        extent={{-8,8},{8,-8}},
        rotation=0,
        origin={146,88})));
  Modelica.Blocks.Sources.RealExpression flow_S1(y=0.2) "S4_flowRate"
                                                     annotation (
      Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={14,130})));
equation
  connect(flow_S4.y, algo.flow_S4) annotation (Line(
      points={{-12,119},{-12,100.606},{-11.92,100.606}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1.y, algo.T1) annotation (Line(
      points={{-55,92},{-46,92},{-46,80.0059},{-33.68,80.0059}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3.y, algo.T3) annotation (Line(
      points={{-55,80},{-46,80},{-46,61.8294},{-33.68,61.8294}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4_mod.y, algo.T4) annotation (Line(
      points={{-55,60},{-44,60},{-44,43.6529},{-33.68,43.6529}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5.y, algo.T5) annotation (Line(
      points={{-79,38},{-50,38},{-50,40},{-42,40},{-42,26.6882},{-33.68,26.6882}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7_mod.y, algo.T7) annotation (Line(
      points={{-51,18},{-44,18},{-44,8.51176},{-33.68,8.51176}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb_2_mod.y, algo.T8) annotation (Line(
      points={{-51,-16},{-46,-16},{-46,4},{-40,4},{-40,-9.66471},{-33.68,
          -9.66471}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text.y, algo.Text) annotation (Line(
      points={{-79,-102},{-50,-102},{-50,-78.1294},{-32.72,-78.1294}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(algo.S6_outter, pump_algo1.pumpControl_S6) annotation (Line(
      points={{127.6,26.6882},{145.8,26.6882},{145.8,9.55},{181.059,9.55}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(algo.S5_outter, pump_algo1.pumpControl_S5) annotation (Line(
      points={{127.6,1.24118},{145.8,1.24118},{145.8,-12.45},{181.059,-12.45}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(algo.S4_outter, pump_algo1.pumpControl_S4) annotation (Line(
      points={{127.6,-22.9941},{145.8,-22.9941},{145.8,-34.45},{181.059,-34.45}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(algo.Sj_outter, pump_algo1.pumpControl_Sj) annotation (Line(
      points={{127.6,-47.2294},{146.8,-47.2294},{146.8,-56.45},{181.059,-56.45}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(algo.V3V_extra_outter, real_solar.u) annotation (Line(
      points={{127.6,88.4882},{131.8,88.4882},{131.8,88},{136.4,88}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(real_solar.y, pump_algo1.V3V_extra) annotation (Line(
      points={{154.8,88},{160,88},{160,31.55},{181.059,31.55}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T1.y, pump_algo1.T1) annotation (Line(
      points={{-55,92},{-48,92},{-48,164},{194.118,164},{194.118,39.8}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T3.y, pump_algo1.T3) annotation (Line(
      points={{-55,80},{-48,80},{-48,164},{208.235,164},{208.235,39.8}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T4_mod.y, pump_algo1.T4) annotation (Line(
      points={{-55,60},{-48,60},{-48,164},{222.353,164},{222.353,39.8}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T5.y, pump_algo1.T5) annotation (Line(
      points={{-79,38},{-48,38},{-48,164},{236.471,164},{236.471,39.8}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T7_mod.y, pump_algo1.T7) annotation (Line(
      points={{-51,18},{-48,18},{-48,164},{250.588,164},{250.588,39.8}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(Tamb_2_mod.y, pump_algo1.T8) annotation (Line(
      points={{-51,-16},{-48,-16},{-48,164},{264.706,164},{264.706,39.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb_1.y, algo.Tambiant[1]) annotation (Line(
      points={{-79,-70},{-58,-70},{-58,-56.6206},{-34.32,-56.6206}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb_1.y, algo.Tambiant[2]) annotation (Line(
      points={{-79,-70},{-56,-70},{-56,-49.9559},{-34.32,-49.9559}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(flow_S1.y, algo.mini_flow_S4) annotation (Line(
      points={{14,119},{14,114},{4.08,114},{4.08,100.606}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-140},{300,140}},
          preserveAspectRatio=false),
                      graphics),
    Icon(coordinateSystem(extent={{-100,-140},{300,140}})),
    experiment(StopTime=1000, Interval=120));
end Algo_TEST;
