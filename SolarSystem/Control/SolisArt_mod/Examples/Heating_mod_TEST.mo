within SolarSystem.Control.SolisArt_mod.Examples;
model Heating_mod_TEST "

Test ok"
  extends Modelica.Icons.Example;

  Heating_mod heating_mod(                           n=n, Tinstruction=
        Tinstruction)          annotation (Placement(transformation(
          extent={{90,-26},{150,48}})));
  Modelica.Blocks.Sources.RealExpression T8(y=273.15 + 20)
    "T. at heating start point (just before extra exchanger input)"
    annotation (Placement(transformation(extent={{8,-88},{28,-68}})));
  Modelica.Blocks.Sources.BooleanExpression V3V_extra(y=true)
                                                      annotation (
      Placement(transformation(extent={{28,-6},{48,14}})));
  Variables_state variables_state(Tinstruction=
        Tinstruction)
    annotation (Placement(transformation(extent={{-24,22},{30,48}})));
  Modelica.Blocks.Sources.RealExpression Tamb_1(y=1 + 273.15) "Temp room 1"
    annotation (Placement(transformation(extent={{8,62},{28,82}})));
  Modelica.Blocks.Sources.RealExpression Tamb_2(y=19.9 + 273.15) "Temp room 2"
    annotation (Placement(transformation(extent={{8,46},{28,66}})));
  Modelica.Blocks.Sources.BooleanExpression V3V_solaire(y=false)
                                                        annotation (
      Placement(transformation(extent={{30,6},{50,26}})));
  Modelica.Blocks.Sources.RealExpression T1(y=273.15 + 100)
    "Temp after solar panels"
    annotation (Placement(transformation(extent={{-96,-22},{-76,-2}})));
  Modelica.Blocks.Sources.RealExpression T5(y=273.15 + 110)
    "T. inside storage tank"
    annotation (Placement(transformation(extent={{8,-38},{28,-18}})));
  Modelica.Blocks.Sources.RealExpression T7(y=273.15 + 19)
    "Temp heat transmitters return"                  annotation (
      Placement(transformation(extent={{-74,-60},{-54,-40}})));
  Modelica.Blocks.Sources.Ramp T7_mod(
    duration=1,
    height=15,
    offset=273.15 + 50) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-100,-60},{-80,-40}})));
  Modelica.Blocks.Sources.RealExpression Text(y=6 + 273.15)
    "outdoor temperature"
    annotation (Placement(transformation(extent={{-100,18},{-80,38}})));
  parameter Modelica.SIunits.Temp_C Tinstruction[n]={293.15, 293.15}
    "Inside desired temperature (�C)";
  parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states : true = ON, false = OFF)";
  Modelica.Blocks.Sources.BooleanExpression CHAUFF[2](y={true,false})
    annotation (Placement(transformation(extent={{-30,-16},{-10,4}})));
  Modelica.Blocks.Sources.RealExpression Text_mini(y=3 + 273.15)
    "outdoor temperature"
    annotation (Placement(transformation(extent={{-94,0},{-74,20}})));
equation
  connect(variables_state.DTeco, heating_mod.DTeco) annotation (Line(
      points={{30.3857,40.5714},{47.1929,40.5714},{47.1929,37.9091},{90.5714,
          37.9091}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(variables_state.TsolarInstruction, heating_mod.TsolarInstruction)
    annotation (Line(
      points={{30.3857,29.4286},{47.1929,29.4286},{47.1929,31.1818},{90.5714,
          31.1818}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb_1.y, heating_mod.Tambiant[1]) annotation (Line(
      points={{29,72},{48,72},{48,44.6364},{90.5714,44.6364}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb_2.y, heating_mod.Tambiant[2]) annotation (Line(
      points={{29,56},{48,56},{48,44.6364},{90.5714,44.6364}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T5.y, heating_mod.T5) annotation (Line(
      points={{29,-28},{48,-28},{48,-7.5},{90.5714,-7.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T8.y, heating_mod.T8) annotation (Line(
      points={{29,-78},{56,-78},{56,-24.15},{90.7143,-24.15}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1.y, variables_state.T1) annotation (Line(
      points={{-75,-12},{-68,-12},{-68,45.7714},{-24,45.7714}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7.y, variables_state.T7) annotation (Line(
      points={{-53,-50},{-40,-50},{-40,38},{-24,38},{-24,38.3429}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text.y, variables_state.Text) annotation (Line(
      points={{-79,28},{-52.5,28},{-52.5,31.2857},{-24,31.2857}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(V3V_solaire.y, heating_mod.V3V_solar) annotation (Line(
      points={{51,16},{58,16},{58,14.1955},{90.4286,14.1955}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_extra.y, heating_mod.V3V_extra) annotation (Line(
      points={{49,4},{59.5,4},{59.5,21.2591},{90.4286,21.2591}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(variables_state.T1_out, heating_mod.T1) annotation (Line(
      points={{-0.857143,21.2571},{-0.857143,-12},{48,-12},{48,-6},{76,-6},{76,
          -0.436364},{90.5714,-0.436364}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(variables_state.T7_out, heating_mod.T7) annotation (Line(
      points={{10.7143,21.2571},{10.7143,-16.0773},{90.7143,-16.0773}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(CHAUFF.y, heating_mod.CHAUFF) annotation (Line(
      points={{-9,-6},{30,-6},{30,-2},{70,-2},{70,7.80455},{90.4286,7.80455}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(Text_mini.y, variables_state.Text_mini) annotation (Line(
      points={{-73,10},{-50,10},{-50,24.2286},{-24,24.2286}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-100},{140,100}},
          preserveAspectRatio=false),
                      graphics),
    Icon(coordinateSystem(extent={{-100,-100},{140,100}})),
    experiment(StopTime=1000),
    __Dymola_experimentSetupOutput);
end Heating_mod_TEST;
