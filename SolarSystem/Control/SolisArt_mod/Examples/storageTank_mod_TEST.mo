within SolarSystem.Control.SolisArt_mod.Examples;
model storageTank_mod_TEST "

Test ok"
  extends Modelica.Icons.Example;
  storageTank_mod storageTank_mod1 annotation (Placement(
        transformation(extent={{40,-20},{100,40}})));
  Modelica.Blocks.Sources.Ramp T3_mod(
    height=75,
    duration=800,
    offset=273.15 + 20) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-60,20},{-40,40}})));
  Modelica.Blocks.Sources.Ramp T1_mod(
    offset=273.15 + 25,
    height=75,
    duration=500) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
  Modelica.Blocks.Sources.Ramp T5_mod(
    duration=800,
    offset=273.15 + 25,
    height=85) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-60,-18},{-40,2}})));
equation
  connect(T1_mod.y, storageTank_mod1.T1) annotation (Line(
      points={{-39,70},{20,70},{20,23.3333},{40,23.3333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3_mod.y, storageTank_mod1.T3) annotation (Line(
      points={{-39,30},{-2,30},{-2,6.66667},{40,6.66667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5_mod.y, storageTank_mod1.T5) annotation (Line(
      points={{-39,-8},{0,-8},{0,-10},{40,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                      graphics));
end storageTank_mod_TEST;
