within SolarSystem.Control.SolisArt_mod;
model Flow_max
  "Compute max mass flow rate for each pumps according to algorithm "
  parameter Integer n=4 "Number of heating pump (Sj)";
  parameter Modelica.SIunits.Time temporizations[n-1]={300, 300, 300}
    "How much time we have to wait before the outter value can be true (n-1 values)";

  parameter Modelica.SIunits.Time modulations[n+3]={180, 180, 180, 180, 180, 180, 180}
    "How much time we have to wait before the outter value can be true (n+3 values)";
  parameter Integer nb_panel=6 "How many collector ?" annotation (Dialog(group="Mass flow control"));
  parameter Modelica.SIunits.Area area_panel=2.32 "Collecting area" annotation (Dialog(group="Mass flow control"));

  parameter Modelica.SIunits.MassFlowRate base[2]={0.0110, 0.0192}
    "Base mass flow rate : [1] >> flow to panel, [2]  >> flow to backup" annotation (Dialog(group="Mass flow control"));
  parameter Modelica.SIunits.MassFlowRate add[2]={0.0055, 0.0096}
    "If more than one pump on : [1] >> flow to panel, [2]  >> flow to backup" annotation (Dialog(group="Mass flow control"));
  parameter Modelica.SIunits.MassFlowRate m_flow_mini=0.0
    "Value of Real output" annotation (Dialog(group="Mass flow control"));
  Modelica.Blocks.Interfaces.BooleanInput V3V_extra
    "Open to solar panels = true, Open to extra tank = false"
    annotation (Placement(transformation(extent={{-126,50},{-86,90}}),
        iconTransformation(extent={{-120,60},{-80,100}})));
  Modelica.Blocks.Logical.Switch flow_limit[n + 3]
    "Limit to 50 per cent if pump just start"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=0,
        origin={122,-56})));
  Modelica.Blocks.Sources.RealExpression hundred[n + 3](each y=1)
    annotation (Placement(transformation(extent={{60,-46},{80,-26}})));
  Modelica.Blocks.Sources.RealExpression fifty[n + 3](each y=0.5)
    annotation (Placement(transformation(extent={{60,-94},{80,-74}})));
  Utilities.Timers.Wait_for wait_for[n + 3](wait_for=modulations)
    annotation (Placement(transformation(extent={{-6,-50},{18,-30}})));
  Modelica.Blocks.Math.Product product[n + 3]
    annotation (Placement(transformation(extent={{144,-34},{164,-14}})));
  Modelica.Blocks.Routing.Multiplex2 concatenation(n1=2, n2=n + 1)
    "Concatenation of all flows rate"                        annotation (
      Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=-90,
        origin={80,18})));
  Modelica.Blocks.Routing.Replicator replicator1(nout=n + 1)
    "replicate for S4 and Sj"
    annotation (Placement(transformation(extent={{-6,-5},{6,5}},
        rotation=-90,
        origin={95,46})));
  Utilities.SolisArt.Counter_double counter(n=3 + n)
    annotation (Placement(transformation(extent={{-6,42},{12,58}})));
  Utilities.SolisArt.split_input splitter(
    base=base,
    add=add,
    nb_panel=nb_panel,
    area_panel=area_panel) annotation (Placement(transformation(extent={{30,70},{76,94}})));
  Modelica.Blocks.Logical.Or or_limit[n + 3]
    annotation (Placement(transformation(extent={{42,-64},{58,-48}})));
  Buildings.Utilities.Math.BooleanReplicator booRep(nout=n + 1)
    annotation (Placement(transformation(extent={{-28,-100},{-14,-86}})));
  Utilities.Logical.Multiplex2_boolean multiplex_boolean(n1=3, n2=n)
    annotation (Placement(transformation(extent={{-34,-8},{-18,8}})));
  Modelica.Blocks.Logical.Not not_onSolar
    annotation (Placement(transformation(extent={{-66,-100},{-52,-86}})));
  Modelica.Blocks.Sources.BooleanExpression always_tempo[2]
    annotation (Placement(transformation(extent={{-30,-74},{-10,-54}})));
  Utilities.Logical.Multiplex2_boolean multiplex_boolean1(n1=2, n2=n + 1)
    "Used to keep modulation for S6 and S5 even if V3Vextra close to solar collector"
    annotation (Placement(transformation(extent={{4,-88},{20,-72}})));
  Modelica.Blocks.Interfaces.BooleanInput in_value_other[3] "[S6, S5, S4]"
    annotation (Placement(transformation(extent={{-124,-20},{-84,20}}),
        iconTransformation(extent={{-120,-20},{-80,20}})));
  Modelica.Blocks.Interfaces.BooleanInput in_value[n] "[Sj]*n"
    annotation (Placement(transformation(extent={{-126,-80},{-86,-40}}),
        iconTransformation(extent={{-120,-100},{-80,-60}})));

  Utilities.Timers.Wait_for_tempo tempo(n=n, wait_for=temporizations)
    annotation (Placement(transformation(extent={{-68,-68},{-48,-52}})));
  Modelica.Blocks.Logical.Switch flow_split[n + 3]
    "If pump on transfer flow rate"
    annotation (Placement(transformation(extent={{110,-10},{130,10}})));
  Modelica.Blocks.Interfaces.RealOutput out_value[3 + n] "[S6, S5, S4, Sj*n]"
    annotation (Placement(transformation(extent={{192,-20},{232,20}}),
        iconTransformation(extent={{192,-20},{232,20}})));
  Modelica.Blocks.Sources.RealExpression m_flow_else[n + 3](each y=m_flow_mini)
    annotation (Placement(transformation(extent={{60,-26},{80,-6}})));
  Modelica.Blocks.Routing.Replicator replicator(nout=2)
    "replicate for S6 and S5"
    annotation (Placement(transformation(extent={{-6,-5},{6,5}},
        rotation=-90,
        origin={65,46})));

  Modelica.Blocks.Interfaces.BooleanOutput out_stateValue[3 + n]
    "[S6, S5, S4, Sj*n] states"
    annotation (Placement(transformation(extent={{192,-80},{232,-40}}),
        iconTransformation(extent={{192,-80},{232,-40}})));
  Modelica.Blocks.Interfaces.RealOutput S4_flow_mini[3 + n] "S4_mini"
    annotation (Placement(transformation(extent={{192,30},{232,70}}),
        iconTransformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={140,-110})));
  Modelica.Blocks.Interfaces.BooleanOutput pumps_state[3 + n]
    "[S6, S5, S4, Sj*n] states"
    annotation (Placement(transformation(extent={{192,70},{232,110}}),
        iconTransformation(extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={2,-110})));
equation
  connect(m_flow_else.y, flow_split.u3) annotation (Line(
      points={{81,-16},{100,-16},{100,-8},{108,-8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(hundred.y, flow_limit.u1)
                                  annotation (Line(
      points={{81,-36},{100,-36},{100,-48},{110,-48}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(fifty.y, flow_limit.u3)      annotation (Line(
      points={{81,-84},{100,-84},{100,-64},{110,-64}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(product.y, out_value) annotation (Line(
      points={{165,-24},{180,-24},{180,0},{212,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(flow_split.y, product.u1) annotation (Line(
      points={{131,0},{136,0},{136,-18},{142,-18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(flow_limit.y, product.u2)  annotation (Line(
      points={{133,-56},{136,-56},{136,-30},{142,-30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(or_limit.y, flow_limit.u2)  annotation (Line(
      points={{58.8,-56},{110,-56}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(splitter.out_value[1], replicator.u) annotation (Line(
      points={{76,80.8},{88,80.8},{88,58},{65,58},{65,53.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(splitter.out_value[2], replicator1.u) annotation (Line(
      points={{76,83.2},{95,83.2},{95,53.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(wait_for.out_value, or_limit.u1) annotation (Line(
      points={{19.68,-40},{24,-40},{24,-56},{40.4,-56}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(concatenation.y, flow_split.u1)
                                        annotation (Line(
      points={{80,11.4},{80,8},{108,8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(V3V_extra, splitter.V3V_extra) annotation (Line(
      points={{-106,70},{-80,70},{-80,89.92},{30,89.92}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(tempo.out_value, multiplex_boolean.u2) annotation (Line(
      points={{-46.6,-60},{-40.44,-60},{-40.44,-4.8},{-35.6,-4.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(multiplex_boolean.y, wait_for.in_value) annotation (Line(
      points={{-17.2,0},{-12,0},{-12,-40},{-6,-40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_extra, not_onSolar.u) annotation (Line(
      points={{-106,70},{-80,70},{-80,-93},{-67.4,-93}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(multiplex_boolean.y, counter.in_value) annotation (Line(
      points={{-17.2,0},{-12,0},{-12,50},{-6,50}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(multiplex_boolean.y, flow_split.u2) annotation (Line(
      points={{-17.2,0},{108,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(counter.out_value, splitter.in_value) annotation (Line(
      points={{12,50},{20,50},{20,73.84},{30,73.84}},
      color={255,127,0},
      smooth=Smooth.None));
  connect(replicator.y, concatenation.u1) annotation (Line(
      points={{65,39.4},{65,36},{76.4,36},{76.4,25.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(replicator1.y, concatenation.u2) annotation (Line(
      points={{95,39.4},{95,36},{83.6,36},{83.6,25.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(not_onSolar.y, booRep.u) annotation (Line(
      points={{-51.3,-93},{-29.4,-93}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(multiplex_boolean1.y, or_limit.u2) annotation (Line(
      points={{20.8,-80},{24,-80},{24,-62.4},{40.4,-62.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(always_tempo.y, multiplex_boolean1.u1) annotation (Line(
      points={{-9,-64},{-4,-64},{-4,-75.2},{2.4,-75.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(booRep.y, multiplex_boolean1.u2) annotation (Line(
      points={{-13.3,-93},{-4,-93},{-4,-84.8},{2.4,-84.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(in_value_other, multiplex_boolean.u1) annotation (Line(
      points={{-104,0},{-40,0},{-40,4.8},{-35.6,4.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(in_value, tempo.in_value) annotation (Line(
      points={{-106,-60},{-68,-60}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(or_limit.y, out_stateValue) annotation (Line(
      points={{58.8,-56},{80,-56},{80,-72},{180,-72},{180,-60},{212,-60}},
      color={255,0,255},
      smooth=Smooth.None));

  connect(concatenation.y, S4_flow_mini) annotation (Line(
      points={{80,11.4},{80,8},{100,8},{100,20},{180,20},{180,50},{212,
          50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiplex_boolean.y, pumps_state) annotation (Line(
      points={{-17.2,0},{-12,0},{-12,100},{174,100},{174,90},{212,90}},
      color={255,0,255},
      smooth=Smooth.None));

                                                                                      annotation (Dialog(group="Temporization"), Icon(
        coordinateSystem(extent={{-100,-100},{200,100}}, preserveAspectRatio=true),
        graphics),
    Documentation(info="<html>
<p>Inputs and outputs order :</p>
<p>S6, S5, S4, Sj (heating pumps)</p>
<p>Heating pump order is important. Used to temporizations</p>
<p>n = heating pumps number</p>
<p>Each pumps start at 50&percnt; except if V3V extra open to backup (always a modulation on S5 and S6 yet)</p>
<p>Must have two heating pump at least due to temporization algorithm.</p>
</html>"),    Diagram(coordinateSystem(extent={{-100,-100},{200,100}},
          preserveAspectRatio=true),
                      graphics),defaultComponentName = "flow_out");
end Flow_max;
