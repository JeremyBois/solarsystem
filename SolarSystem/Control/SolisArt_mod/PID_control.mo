within SolarSystem.Control.SolisArt_mod;
model PID_control "Adjust output"

  parameter Real day_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)";

  parameter Modelica.Blocks.Types.SimpleController controllerType=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(group="PID"));
  parameter Real k=1 "Gain of controller" annotation (Dialog(group="PID"));
  parameter Modelica.SIunits.Time Ti=1 "Time constant of Integrator block" annotation (Dialog(group="PID"));
  parameter Modelica.SIunits.Time Td=1 "Time constant of Derivative block" annotation (Dialog(group="PID"));
  parameter Real yMax=1 "Upper limit of output" annotation (Dialog(group="PID"));
  parameter Real yMin=0 "Lower limit of output" annotation (Dialog(group="PID"));
  parameter Real wp=1 "Set-point weight for Proportional block (0..1)" annotation (Dialog(group="PID"));
  parameter Real wd=0 "Set-point weight for Derivative block (0..1)" annotation (Dialog(group="PID"));
  parameter Modelica.Blocks.Types.InitPID initType=Modelica.Blocks.Types.InitPID.DoNotUse_InitialIntegratorState
    "Type of initialization (1: no init, 2: steady state, 3: initial state, 4: initial output)"
    annotation (Dialog(group="Initialization"));
  parameter Boolean limitsAtInit=true
    "= false, if limits are ignored during initializiation"
    annotation (Dialog(group="Initialization"));
  Modelica.Blocks.Interfaces.BooleanInput pumpControl
    "ON = true, OFF = false (pump)"
    annotation (Placement(transformation(extent={{-120,-110},{-80,-70}})));
  Modelica.Blocks.Interfaces.RealInput signal
    "Connector of measurement input signal"
    annotation (Placement(transformation(extent={{-120,70},{-80,110}})));
  Buildings.Controls.Continuous.LimPID limPID(
    k=k,
    Ti=Ti,
    Td=Td,
    yMax=yMax,
    yMin=yMin,
    wp=wp,
    wd=wd,
    controllerType=controllerType,
    reverseAction=true,
    initType=initType,
    limitsAtInit=limitsAtInit)
          annotation (Placement(transformation(extent={{-24,-28},{12,8}})));
  Modelica.Blocks.Interfaces.RealOutput value_outer(start=0.5)
    annotation (Placement(transformation(extent={{140,-20},{180,20}}),
        iconTransformation(extent={{140,-20},{180,20}})));

  Modelica.Blocks.Logical.Switch modifier_switch
    annotation (Placement(transformation(extent={{60,-24},{86,2}})));
  Modelica.Blocks.Sources.RealExpression coef(y=0) "pump is off"
    annotation (Placement(transformation(extent={{84,-62},{64,-42}})));

  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule instructions(table=
        day_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{-100,-16},{-68,16}})));

equation
  connect(pumpControl, modifier_switch.u2)    annotation (Line(
      points={{-100,-90},{40,-90},{40,-11},{57.4,-11}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(limPID.y, modifier_switch.u1) annotation (Line(
      points={{13.8,-10},{36,-10},{36,-0.6},{57.4,-0.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(coef.y, modifier_switch.u3)       annotation (Line(
      points={{63,-52},{48,-52},{48,-21.4},{57.4,-21.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(modifier_switch.y, value_outer)  annotation (Line(
      points={{87.3,-11},{115.65,-11},{115.65,0},{160,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(instructions.y[1], limPID.u_s)
                                     annotation (Line(
      points={{-66.4,0},{-54.45,0},{-54.45,-10},{-27.6,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(signal, limPID.u_m) annotation (Line(
      points={{-100,90},{-42,90},{-42,-46},{-6,-46},{-6,-31.6}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-140},{160,140}},
          preserveAspectRatio=true), graphics), Icon(coordinateSystem(
          extent={{-100,-140},{160,140}}), graphics={
        Text(
          extent={{-58,-52},{124,-150}},
          lineColor={0,128,0},
          textString="PID"),
        Line(points={{-60,-52},{-60,78},{-60,8},{50,88},{100,88}},  color={0,
              0,127}),
        Line(points={{-60,106},{-60,-62}},color={192,192,192}),
        Line(points={{-64,-60},{108,-60}},color={192,192,192}),
        Polygon(
          points={{-60,118},{-68,96},{-52,96},{-60,118}},
          lineColor={192,192,192},
          fillColor={192,192,192},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{0,11},{-8,-11},{8,-11},{0,11}},
          lineColor={192,192,192},
          fillColor={192,192,192},
          fillPattern=FillPattern.Solid,
          origin={114,-59},
          rotation=-90),
        Polygon(
          points={{-100,140},{160,140},{160,-140},{-100,-140},{-100,140},
              {-100,140},{-100,140}},
          lineColor={255,0,255},
          smooth=Smooth.None)}),
    Documentation(info="<html>
<p>This models return a modifier coefficient according to pump state and temperature difference between inputs.</p>
</html>"));
end PID_control;
