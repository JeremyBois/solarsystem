within SolarSystem.Control.SolisArt_mod;
model BackupHeater_mod "Control when we need to turn on the backup heater"

  Modelica.Blocks.Interfaces.BooleanInput CHAUFF "ON = true, OFF = false"
    annotation (Placement(transformation(extent={{-120,-120},{-80,-80}}),
        iconTransformation(extent={{-120,-120},{-80,-80}})));
  Modelica.Blocks.Interfaces.BooleanInput  ECS "ON = true, OFF = false"
    annotation (Placement(transformation(extent={{-120,-70},{-80,-30}}),
        iconTransformation(extent={{-120,-60},{-80,-20}})));
  Modelica.Blocks.Interfaces.BooleanInput V3V_extra
    "Open to solar panels = true, Open to extra tank = false"
    annotation (Placement(transformation(extent={{-120,-30},{-80,10}}),
        iconTransformation(extent={{-120,0},{-80,40}})));
  Modelica.Blocks.Interfaces.RealInput T8
    "T. at heating start point (just before extra exchanger input)"
    annotation (Placement(transformation(extent={{-120,60},{-80,100}}),
        iconTransformation(extent={{-120,60},{-80,100}})));
  Modelica.Blocks.Interfaces.BooleanOutput BackupHeater(start=false)
    "true = needed, false otherwise"
    annotation (Placement(transformation(extent={{150,-20},{190,20}}),
        iconTransformation(extent={{150,-20},{190,20}})));
  Modelica.Blocks.Logical.Or alternatives "true if one input is true"
    annotation (Placement(transformation(extent={{60,-40},{80,-20}})));
  Modelica.Blocks.Logical.And inter_conditions "true if all inputs are true"
    annotation (Placement(transformation(extent={{20,20},{40,40}})));
  Modelica.Blocks.Logical.Hysteresis T8_hys(
    uLow=273.15 + 30,
    uHigh=273.15 + 35,
    pre_y_start=true,
    y(start=false)) "true if T8 >=35 and false if T8 <=30"
    annotation (Placement(transformation(extent={{-60,70},{-40,90}})));
  Modelica.Blocks.Logical.Not T8_hys_opposite "Return the input opposite"
    annotation (Placement(transformation(extent={{-20,70},{0,90}})));
  Modelica.Blocks.Logical.And conditions "true if all inputs are true"
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
  Modelica.Blocks.Logical.Not V3V_extra_opposite "Return the input opposite"
    annotation (Placement(transformation(extent={{-20,-20},{0,0}})));
equation
  connect(T8, T8_hys.u) annotation (Line(
      points={{-100,80},{-62,80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T8_hys.y, T8_hys_opposite.u) annotation (Line(
      points={{-39,80},{-22,80}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(T8_hys_opposite.y, inter_conditions.u1) annotation (Line(
      points={{1,80},{8,80},{8,30},{18,30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(CHAUFF, inter_conditions.u2) annotation (Line(
      points={{-100,-100},{8,-100},{8,22},{18,22}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(inter_conditions.y, alternatives.u1) annotation (Line(
      points={{41,30},{50,30},{50,-30},{58,-30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(ECS, alternatives.u2) annotation (Line(
      points={{-100,-50},{50,-50},{50,-38},{58,-38}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(alternatives.y, conditions.u2) annotation (Line(
      points={{81,-30},{88,-30},{88,-8},{98,-8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(conditions.y, BackupHeater) annotation (Line(
      points={{121,0},{170,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_extra, V3V_extra_opposite.u) annotation (Line(
      points={{-100,-10},{-22,-10}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(V3V_extra_opposite.y, conditions.u1) annotation (Line(
      points={{1,-10},{84,-10},{84,0},{98,0}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-140},{160,120}},
          preserveAspectRatio=true), graphics), Icon(coordinateSystem(
          extent={{-100,-140},{160,120}}), graphics={
        Polygon(
          points={{-100,120},{160,120},{160,-140},{-100,-140},{-100,120},
              {-100,120},{-100,120}},
          lineColor={255,0,255},
          smooth=Smooth.None),
        Text(
          extent={{-72,54},{140,-64}},
          lineColor={0,128,0},
          textString="Backup Heater
control")}));
end BackupHeater_mod;
