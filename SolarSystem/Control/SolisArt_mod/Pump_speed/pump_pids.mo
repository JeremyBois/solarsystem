within SolarSystem.Control.SolisArt_mod.Pump_speed;
model pump_pids "flow control for each pump"

  // How many heated rooms
  parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

  // Flow Control
  parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm right_pumps=1
    "Prescribed rotational speed for pumps of right extra valve side (unit=1/min)";
  parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm left_pumps=1
    "Prescribed rotational speed for pumps of left extra valve side";
  parameter Modelica.SIunits.Time temporizations[n - 1]={300}
    "How much time we have to wait before the outter value can be true (n-1 values)";
  parameter Modelica.SIunits.Time modulations[2]={180,180}
    "How much time we have to wait before the outter value can be true (2 values)";

  // S6
  parameter Modelica.Blocks.Types.SimpleController S6_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S6"));
  parameter Real S6_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S6"));
  parameter Real S6_k=0.1 "Gain of controller" annotation (Dialog(tab="S6"));
  parameter Modelica.SIunits.Time S6_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S6"));
  parameter Modelica.SIunits.Time S6_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S6"));
  parameter Real S6_yMax=1 "Upper limit of output" annotation (Dialog(tab="S6"));
  parameter Real S6_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S6"));
  parameter Real S6_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S6"));
  parameter Real S6_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S6"));

  // S5
  parameter Modelica.Blocks.Types.SimpleController S5_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S5"));
  parameter Real S5_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S5"));
  parameter Real S5_k=0.1 "Gain of controller" annotation (Dialog(tab="S5"));
  parameter Modelica.SIunits.Time S5_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S5"));
  parameter Modelica.SIunits.Time S5_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S5"));
  parameter Real S5_yMax=1 "Upper limit of output" annotation (Dialog(tab="S5"));
  parameter Real S5_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S5"));
  parameter Real S5_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S5"));
  parameter Real S5_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S5"));

  // S4
  parameter Modelica.Blocks.Types.SimpleController S4_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S4"));
  parameter Real S4_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S4"));
  parameter Real S4_k=0.1 "Gain of controller" annotation (Dialog(tab="S4"));
  parameter Modelica.SIunits.Time S4_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S4"));
  parameter Modelica.SIunits.Time S4_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S4"));
  parameter Real S4_yMax=1 "Upper limit of output" annotation (Dialog(tab="S4"));
  parameter Real S4_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S4"));
  parameter Real S4_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S4"));
  parameter Real S4_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S4"));

  // Sj
  parameter Modelica.Blocks.Types.SimpleController Sj_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="Sj"));
  parameter Real Sj_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="Sj"));
  parameter Real Sj_k=0.1 "Gain of controller" annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Ti=60 "Time constant of Integrator block" annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMax=1 "Upper limit of output" annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="Sj"));
  parameter Real Sj_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Sj"));
  parameter Real Sj_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Sj"));

  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_Sj[
                                                         n]
    "ON = true, OFF = false (pump Sj)"
    annotation (Placement(transformation(extent={{-120,-110},{-80,-70}}),
        iconTransformation(extent={{-114,-96},{-80,-62}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S4
    "ON = true, OFF = false (pump S4)"
    annotation (Placement(transformation(extent={{-120,-62},{-80,-22}}),
        iconTransformation(extent={{-114,-56},{-80,-22}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S5
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{-120,-22},{-80,18}}),
        iconTransformation(extent={{-114,-16},{-80,18}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S6
    "ON = true, OFF = false (pump S6)"
    annotation (Placement(transformation(extent={{-120,18},{-80,58}}),
        iconTransformation(extent={{-114,24},{-80,58}})));
  Modelica.Blocks.Interfaces.RealOutput S6_outter(start=0)
    "ON = 1, OFF = 0 (pump S6)"
    annotation (Placement(transformation(extent={{222,-8},{254,24}}),
        iconTransformation(extent={{226,-48},{258,-16}})));
  Modelica.Blocks.Interfaces.RealOutput S5_outter(start=0)
    "ON = 1, OFF = 0 (pump S5)"
    annotation (Placement(transformation(extent={{222,-48},{254,-16}}),
        iconTransformation(extent={{228,12},{260,44}})));
  Modelica.Blocks.Interfaces.RealOutput S4_outter(start=0)
    "ON = 1, OFF = 0 (pump S4)"
    annotation (Placement(transformation(extent={{222,-88},{254,-56}}),
        iconTransformation(extent={{228,64},{260,96}})));

  Modelica.Blocks.Interfaces.RealOutput Sj_outter[n](each start=0)
    "ON = 1, OFF = 0 (pump Sj)"
    annotation (Placement(transformation(extent={{224,64},{256,96}}),
        iconTransformation(extent={{228,-100},{260,-68}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_Sj(table=
        Sj_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,80},{100,100}})));
  Buildings.Controls.Continuous.LimPID pid_Sj[n](each k=Sj_k, each Td=Sj_Td,
    each wp=Sj_wp,
    each wd=Sj_wd,
    each controllerType=Sj_controller,
    each Ti=Sj_Ti,
    each yMax=Sj_yMax,
    each yMin=Sj_yMin,
    each reverseAction=Sj_reverseAction,
    each Nd=Sj_Nd)
    annotation (Placement(transformation(extent={{140,80},{160,100}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S6(table=
        S6_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,10},{100,30}})));
  Buildings.Controls.Continuous.LimPID pid_S6(
    controllerType=S6_controller,
    k=S6_k,
    Ti=S6_Ti,
    Td=S6_Td,
    wp=S6_wp,
    wd=S6_wd,
    yMax=S6_yMax,
    yMin=S6_yMin,
    reverseAction=true)
    annotation (Placement(transformation(extent={{142,10},{162,30}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S5(table=
        S5_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,-36},{100,-16}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule control_S4(table=
        S4_schedule) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{80,-74},{100,-54}})));
  Buildings.Controls.Continuous.LimPID pid_S5(
    controllerType=S5_controller,
    k=S5_k,
    Ti=S5_Ti,
    Td=S5_Td,
    wp=S5_wp,
    wd=S5_wd,
    yMax=S5_yMax,
    yMin=S5_yMin,
    reverseAction=true)
    annotation (Placement(transformation(extent={{140,-36},{160,-16}})));
  Buildings.Controls.Continuous.LimPID pid_S4(
    controllerType=S4_controller,
    k=S4_k,
    Ti=S4_Ti,
    Td=S4_Td,
    wp=S4_wp,
    wd=S4_wd,
    yMax=S4_yMax,
    yMin=S4_yMin,
    reverseAction=true)
    annotation (Placement(transformation(extent={{140,-74},{160,-54}})));
  Modelica.Blocks.Interfaces.RealInput T7 "T. at heat transmitters return"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={40,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={100,96})));
  Modelica.Blocks.Interfaces.RealInput T8 "T. at heat transmitters start"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={70,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={140,96})));
  Modelica.Blocks.Math.MultiSum delta_heat(k={1,-1}, nu=2) "T8 - T7"
    annotation (Placement(transformation(extent={{104,68},{116,80}})));
  Modelica.Blocks.Routing.Replicator delta_replicator(nout=n)
                                                        annotation (
      Placement(transformation(extent={{124,68},{134,80}})));
  Modelica.Blocks.Math.MultiSum delta_storage(k={1,-1}, nu=2) "T1 - T5"
    annotation (Placement(transformation(extent={{126,2},{134,10}})));
  Modelica.Blocks.Math.MultiSum delta_solar(k={1,-1}, nu=2) "T1 - T3"
    annotation (Placement(transformation(extent={{126,-44},{134,-36}})));
  Modelica.Blocks.Math.MultiSum delta_extra(k={1,-1}, nu=2) "T8 - T4"
    annotation (Placement(transformation(extent={{126,-82},{134,-74}})));
  Modelica.Blocks.Interfaces.RealInput T1 "T. after collector"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-80,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={-60,96})));
  Modelica.Blocks.Interfaces.RealInput T3 "T. inside solar tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-50,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={-20,96})));
  Modelica.Blocks.Interfaces.RealInput T4 "T. inside extra tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={-20,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={20,96})));
  Modelica.Blocks.Interfaces.RealInput T5 "T. inside storage tank"
    annotation (Placement(transformation(extent={{-16,16},{16,-16}},
        rotation=-90,
        origin={10,100}),
        iconTransformation(extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={60,96})));
  Modelica.Blocks.Routing.Replicator control_replicator(nout=n)
                                                        annotation (
      Placement(transformation(extent={{116,84},{126,96}})));

  Modelica.Blocks.Logical.Switch S6_if_max
    annotation (Placement(transformation(extent={{180,4},{188,12}})));
  Modelica.Blocks.Logical.Switch S5_if_max
    annotation (Placement(transformation(extent={{180,-40},{188,-32}})));
  Modelica.Blocks.Sources.RealExpression S6_static(each y=0.5)
    annotation (Placement(transformation(extent={{156,-8},{170,6}})));
  Modelica.Blocks.Sources.RealExpression S5_static(each y=0.5)
    annotation (Placement(transformation(extent={{158,-48},{172,-36}})));
  Modelica.Blocks.Interfaces.RealOutput    pumps_state[n + 3]
    "[S6, S5, S4, Sj*n] states" annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=-90,
        origin={20,-106}),  iconTransformation(
        extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={0,-104})));
  parameter Real Sj_Nd=10 "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Sj"));
  parameter Boolean Sj_reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Sj"));

  Pump_control pumps_state_algo(
    n=n,
    temporizations=temporizations,
    modulations=modulations)
    annotation (Placement(transformation(extent={{-44,-46},{6,-10}})));
  Modelica.Blocks.Math.Gain Sj_speed[n](each k=right_pumps)
    annotation (Placement(transformation(extent={{190,86},{198,94}})));
  Modelica.Blocks.Math.Gain S6_speed(k=left_pumps)
    annotation (Placement(transformation(extent={{194,-4},{202,4}})));
  Modelica.Blocks.Math.Gain S5_speed(k=left_pumps)
    annotation (Placement(transformation(extent={{194,-46},{202,-38}})));
  Modelica.Blocks.Math.Gain S4_speed(k=right_pumps)
    annotation (Placement(transformation(extent={{196,-84},{204,-76}})));
  Modelica.Blocks.Math.Product S6_speed_state
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{210,4},{218,12}})));
  Modelica.Blocks.Math.BooleanToReal to_real[n + 3] annotation (Placement(
        transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={176,50})));
  Modelica.Blocks.Math.Product Sj_speed_state[n]
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{210,76},{218,84}})));
  Modelica.Blocks.Math.Product S5_speed_state
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{210,-36},{218,-28}})));
  Modelica.Blocks.Math.Product S5_speed_state1
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{210,-76},{218,-68}})));
  Modelica.Blocks.Math.BooleanToReal Pumps_state_ToReal[n + 3] annotation (
      Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=-90,
        origin={20,-58})));
equation
  connect(control_S6.y[1], pid_S6.u_s) annotation (Line(
      points={{101,20},{140,20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(control_S5.y[1], pid_S5.u_s) annotation (Line(
      points={{101,-26},{138,-26}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(control_S4.y[1], pid_S4.u_s) annotation (Line(
      points={{101,-64},{138,-64}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T8, delta_heat.u[1]) annotation (Line(
      points={{70,100},{70,76.1},{104,76.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7, delta_heat.u[2]) annotation (Line(
      points={{40,100},{40,71.9},{104,71.9}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_heat.y, delta_replicator.u) annotation (Line(
      points={{117.02,74},{123,74}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_replicator.y, pid_Sj.u_m) annotation (Line(
      points={{134.5,74},{150,74},{150,78}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_solar.y, pid_S5.u_m) annotation (Line(
      points={{134.68,-40},{140,-40},{140,-44},{150,-44},{150,-38}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_storage.y, pid_S6.u_m) annotation (Line(
      points={{134.68,6},{140,6},{140,2},{152,2},{152,8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_extra.y, pid_S4.u_m) annotation (Line(
      points={{134.68,-78},{140,-78},{140,-82},{150,-82},{150,-76}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1, delta_storage.u[1]) annotation (Line(
      points={{-80,100},{-80,74},{-38,74},{-38,28},{60,28},{60,7.4},{126,7.4}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T1, delta_solar.u[1]) annotation (Line(
      points={{-80,100},{-80,74},{-38,74},{-38,28},{60,28},{60,-38.6},{126,-38.6}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T5, delta_storage.u[2]) annotation (Line(
      points={{10,100},{10,80},{20,80},{20,60},{72,60},{72,4.6},{126,4.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3, delta_solar.u[2]) annotation (Line(
      points={{-50,100},{-50,76},{-20,76},{-20,40},{64,40},{64,-41.4},{126,-41.4}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T8, delta_extra.u[1]) annotation (Line(
      points={{70,100},{70,64},{110,64},{110,-76.6},{126,-76.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T4, delta_extra.u[2]) annotation (Line(
      points={{-20,100},{-20,80},{0,80},{0,54},{68,54},{68,-79.4},{126,-79.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(control_Sj.y[1], control_replicator.u) annotation (Line(
      points={{101,90},{115,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(control_replicator.y, pid_Sj.u_s) annotation (Line(
      points={{126.5,90},{138,90}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(S6_static.y, S6_if_max.u3) annotation (Line(
      points={{170.7,-1},{176.2,-1},{176.2,4.8},{179.2,4.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_static.y, S5_if_max.u3) annotation (Line(
      points={{172.7,-42},{175.15,-42},{175.15,-39.2},{179.2,-39.2}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(pid_S5.y, S5_if_max.u1) annotation (Line(
      points={{161,-26},{172,-26},{172,-32.8},{179.2,-32.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_S6.y, S6_if_max.u1) annotation (Line(
      points={{163,20},{172,20},{172,11.2},{179.2,11.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumpControl_S6, pumps_state_algo.pumps_system[1]) annotation (Line(
      points={{-100,38},{-74,38},{-74,-23.2},{-44.4167,-23.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumpControl_S5, pumps_state_algo.pumps_system[2]) annotation (Line(
      points={{-100,-2},{-74,-2},{-74,-19},{-44.4167,-19}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumpControl_S4, pumps_state_algo.pumps_system[3]) annotation (Line(
      points={{-100,-42},{-64,-42},{-64,-14.8},{-44.4167,-14.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_modulation[1], S6_if_max.u2) annotation (Line(
      points={{4.33333,-39.7},{4,-39.7},{4,-36},{50,-36},{50,-4},{174,-4},{174,
          8},{179.2,8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_modulation[2], S5_if_max.u2) annotation (Line(
      points={{4.33333,-34.3},{4,-34.3},{4,-36},{50,-36},{50,-4},{174,-4},{174,
          -36},{179.2,-36}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(S6_if_max.y, S6_speed.u) annotation (Line(
      points={{188.4,8},{190,8},{190,0},{193.2,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_if_max.y, S5_speed.u) annotation (Line(
      points={{188.4,-36},{190,-36},{190,-42},{193.2,-42}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_S4.y, S4_speed.u) annotation (Line(
      points={{161,-64},{180,-64},{180,-80},{195.2,-80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid_Sj.y, Sj_speed.u) annotation (Line(
      points={{161,90},{189.2,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumpControl_Sj, pumps_state_algo.pumps_heat) annotation (Line(
      points={{-100,-90},{-56,-90},{-56,-37},{-44.4167,-37}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_state, to_real.u) annotation (Line(
      points={{4.33333,-19},{20,-19},{20,50},{171.2,50}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(to_real[1].y, S6_speed_state.u1) annotation (Line(
      points={{180.4,50},{204,50},{204,10.4},{209.2,10.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S6_speed.y, S6_speed_state.u2) annotation (Line(
      points={{202.4,0},{206,0},{206,5.6},{209.2,5.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Sj_speed_state.y, Sj_outter) annotation (Line(
      points={{218.4,80},{240,80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Sj_speed.y, Sj_speed_state.u1) annotation (Line(
      points={{198.4,90},{200,90},{200,82.4},{209.2,82.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_speed.y, S5_speed_state.u2) annotation (Line(
      points={{202.4,-42},{206,-42},{206,-34.4},{209.2,-34.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S4_speed.y, S5_speed_state1.u2) annotation (Line(
      points={{204.4,-80},{206,-80},{206,-74.4},{209.2,-74.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(to_real[2].y, S5_speed_state.u1) annotation (Line(
      points={{180.4,50},{204,50},{204,-29.6},{209.2,-29.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(to_real[3].y, S5_speed_state1.u1) annotation (Line(
      points={{180.4,50},{204,50},{204,-69.6},{209.2,-69.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S6_speed_state.y, S6_outter) annotation (Line(
      points={{218.4,8},{238,8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_speed_state.y, S5_outter) annotation (Line(
      points={{218.4,-32},{238,-32}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_speed_state1.y, S4_outter) annotation (Line(
      points={{218.4,-72},{238,-72}},
      color={0,0,127},
      smooth=Smooth.None));

      // Connect each heating pumps (first three pump are system wide pumps)
connect(to_real[4:(3+n)].y, Sj_speed_state[:].u2) annotation (Line(
       points={{180.4,50},{204,50},{204,77.6},{209.2,77.6}},
       color={0,0,127},
       smooth=Smooth.None));

  connect(pumps_state_algo.pumps_state, Pumps_state_ToReal.u) annotation (Line(
      points={{4.33333,-19},{20,-19},{20,-48.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Pumps_state_ToReal.y, pumps_state) annotation (Line(
      points={{20,-66.8},{20,-106}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-100},{240,100}},
          preserveAspectRatio=false), graphics, defaultComponentName = "pump_control"), Icon(coordinateSystem(extent={{-100,
            -100},{240,100}}, preserveAspectRatio=false), graphics={
                  Rectangle(
          extent={{-100,100},{240,-100}},
          lineColor={90,150,90},
          fillPattern=FillPattern.CrossDiag,
          fillColor={0,127,127}),
        Text(
          extent={{-100,62},{238,16}},
          lineColor={0,0,0},
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="PID"),
        Text(
          extent={{-96,-20},{240,-52}},
          lineColor={0,0,0},
          textString="Flow switch",
          fontName="Consolas",
          textStyle={TextStyle.Bold})}),
    Documentation(info="<html>
<p>Control all pumps according to pump state from algo.</p>
</html>"),
    experiment(
      StopTime=2e+006,
      Interval=240,
      __Dymola_Algorithm="esdirk23a"),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end pump_pids;
