within SolarSystem.Control.SolisArt_mod.Pump_speed;
model pump_pids_TOR "flow control for each pump"

  // How many heated rooms
  parameter Integer n=2
    "Number of heated rooms (used to sizing vector holding the rooms pumps states)";

  // Flow Control
  parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm right_pumps=1
    "Prescribed rotational speed for pumps of right extra valve side (unit=1/min)";
  parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm left_pumps=1
    "Prescribed rotational speed for pumps of left extra valve side";
  parameter Modelica.SIunits.Time temporizations[n - 1]={300}
    "How much time we have to wait before the outter value can be true (n-1 values)";
  parameter Modelica.SIunits.Time modulations[2]={180,180}
    "How much time we have to wait before the outter value can be true (2 values)";

  // S6
  parameter Modelica.Blocks.Types.SimpleController S6_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S6"));
  parameter Real S6_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S6"));
  parameter Real S6_k=0.1 "Gain of controller" annotation (Dialog(tab="S6"));
  parameter Modelica.SIunits.Time S6_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S6"));
  parameter Modelica.SIunits.Time S6_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S6"));
  parameter Real S6_yMax=1 "Upper limit of output" annotation (Dialog(tab="S6"));
  parameter Real S6_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S6"));
  parameter Real S6_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S6"));
  parameter Real S6_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S6"));

  // S5
  parameter Modelica.Blocks.Types.SimpleController S5_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S5"));
  parameter Real S5_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S5"));
  parameter Real S5_k=0.1 "Gain of controller" annotation (Dialog(tab="S5"));
  parameter Modelica.SIunits.Time S5_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S5"));
  parameter Modelica.SIunits.Time S5_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S5"));
  parameter Real S5_yMax=1 "Upper limit of output" annotation (Dialog(tab="S5"));
  parameter Real S5_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S5"));
  parameter Real S5_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S5"));
  parameter Real S5_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S5"));

  // S4
  parameter Modelica.Blocks.Types.SimpleController S4_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="S4"));
  parameter Real S4_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="S4"));
  parameter Real S4_k=0.1 "Gain of controller" annotation (Dialog(tab="S4"));
  parameter Modelica.SIunits.Time S4_Ti=60 "Time constant of Integrator block"
    annotation (Dialog(tab="S4"));
  parameter Modelica.SIunits.Time S4_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="S4"));
  parameter Real S4_yMax=1 "Upper limit of output" annotation (Dialog(tab="S4"));
  parameter Real S4_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="S4"));
  parameter Real S4_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="S4"));
  parameter Real S4_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="S4"));

  // Sj
  parameter Modelica.Blocks.Types.SimpleController Sj_controller=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller" annotation (Dialog(tab="Sj"));
  parameter Real Sj_schedule[:,:]=[0, 10]
    "Scheduled consigne setup (repeat every days)"
    annotation (Dialog(tab="Sj"));
  parameter Real Sj_k=0.1 "Gain of controller" annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Ti=60 "Time constant of Integrator block" annotation (Dialog(tab="Sj"));
  parameter Modelica.SIunits.Time Sj_Td=60 "Time constant of Derivative block"
    annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMax=1 "Upper limit of output" annotation (Dialog(tab="Sj"));
  parameter Real Sj_yMin=0.2 "Lower limit of output" annotation (Dialog(tab="Sj"));
  parameter Real Sj_wp=1 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Sj"));
  parameter Real Sj_wd=0 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Sj"));

  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_Sj[
                                                         n]
    "ON = true, OFF = false (pump Sj)"
    annotation (Placement(transformation(extent={{-120,-110},{-80,-70}}),
        iconTransformation(extent={{-114,-96},{-80,-62}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S4
    "ON = true, OFF = false (pump S4)"
    annotation (Placement(transformation(extent={{-120,-80},{-80,-40}}),
        iconTransformation(extent={{-114,-74},{-80,-40}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S5
    "ON = true, OFF = false (pump S5)"
    annotation (Placement(transformation(extent={{-120,-40},{-80,0}}),
        iconTransformation(extent={{-114,-34},{-80,0}})));
  Modelica.Blocks.Interfaces.BooleanInput  pumpControl_S6
    "ON = true, OFF = false (pump S6)"
    annotation (Placement(transformation(extent={{-120,0},{-80,40}}),
        iconTransformation(extent={{-114,6},{-80,40}})));
  Modelica.Blocks.Interfaces.RealOutput S6_outter(start=0)
    "ON = 1, OFF = 0 (pump S6)"
    annotation (Placement(transformation(extent={{222,-8},{254,24}}),
        iconTransformation(extent={{226,-48},{258,-16}})));
  Modelica.Blocks.Interfaces.RealOutput S5_outter(start=0)
    "ON = 1, OFF = 0 (pump S5)"
    annotation (Placement(transformation(extent={{222,-48},{254,-16}}),
        iconTransformation(extent={{228,12},{260,44}})));
  Modelica.Blocks.Interfaces.RealOutput S4_outter(start=0)
    "ON = 1, OFF = 0 (pump S4)"
    annotation (Placement(transformation(extent={{222,-88},{254,-56}}),
        iconTransformation(extent={{228,64},{260,96}})));

  Modelica.Blocks.Interfaces.RealOutput Sj_outter[n](each start=0)
    "ON = 1, OFF = 0 (pump Sj)"
    annotation (Placement(transformation(extent={{224,64},{256,96}}),
        iconTransformation(extent={{228,-100},{260,-68}})));

  Modelica.Blocks.Sources.RealExpression right(each y=right_pumps)
    annotation (Placement(transformation(extent={{166,-70},{180,-56}})));
  Modelica.Blocks.Sources.RealExpression left(each y=left_pumps)
    annotation (Placement(transformation(extent={{166,-20},{180,-8}})));
  Modelica.Blocks.Interfaces.RealOutput    pumps_state[n + 3]
    "[S6, S5, S4, Sj*n] states" annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=-90,
        origin={20,-106}),  iconTransformation(
        extent={{-16,-16},{16,16}},
        rotation=-90,
        origin={0,-104})));
  parameter Real Sj_Nd=10 "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Sj"));
  parameter Boolean Sj_reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Sj"));

  Pump_control pumps_state_algo(
    n=n,
    temporizations=temporizations,
    modulations=modulations)
    annotation (Placement(transformation(extent={{-44,-46},{6,-10}})));
  Modelica.Blocks.Math.Product S6_speed_state
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{210,4},{218,12}})));
  Modelica.Blocks.Math.BooleanToReal to_real[n + 3] annotation (Placement(
        transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={176,50})));
  Modelica.Blocks.Math.Product Sj_speed_state[n]
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{210,76},{218,84}})));
  Modelica.Blocks.Math.Product S5_speed_state
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{210,-36},{218,-28}})));
  Modelica.Blocks.Math.Product S5_speed_state1
    "Another multiplicator to check pump state"
    annotation (Placement(transformation(extent={{210,-76},{218,-68}})));
  Modelica.Blocks.Math.BooleanToReal Pumps_state_ToReal[n + 3] annotation (
      Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=-90,
        origin={20,-58})));
  Modelica.Blocks.Sources.RealExpression right_n[n](each y=right_pumps)
    annotation (Placement(transformation(extent={{166,76},{180,90}})));
equation

  connect(pumpControl_S6, pumps_state_algo.pumps_system[1]) annotation (Line(
      points={{-100,20},{-74,20},{-74,-23.2},{-44.4167,-23.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumpControl_S5, pumps_state_algo.pumps_system[2]) annotation (Line(
      points={{-100,-20},{-74,-20},{-74,-19},{-44.4167,-19}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumpControl_S4, pumps_state_algo.pumps_system[3]) annotation (Line(
      points={{-100,-60},{-64,-60},{-64,-14.8},{-44.4167,-14.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumpControl_Sj, pumps_state_algo.pumps_heat) annotation (Line(
      points={{-100,-90},{-56,-90},{-56,-37},{-44.4167,-37}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pumps_state_algo.pumps_state, to_real.u) annotation (Line(
      points={{4.33333,-19},{20,-19},{20,50},{171.2,50}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(to_real[1].y, S6_speed_state.u1) annotation (Line(
      points={{180.4,50},{204,50},{204,10.4},{209.2,10.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Sj_speed_state.y, Sj_outter) annotation (Line(
      points={{218.4,80},{240,80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(to_real[2].y, S5_speed_state.u1) annotation (Line(
      points={{180.4,50},{204,50},{204,-29.6},{209.2,-29.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(to_real[3].y, S5_speed_state1.u1) annotation (Line(
      points={{180.4,50},{204,50},{204,-69.6},{209.2,-69.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S6_speed_state.y, S6_outter) annotation (Line(
      points={{218.4,8},{238,8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_speed_state.y, S5_outter) annotation (Line(
      points={{218.4,-32},{238,-32}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(S5_speed_state1.y, S4_outter) annotation (Line(
      points={{218.4,-72},{238,-72}},
      color={0,0,127},
      smooth=Smooth.None));

      // Connect each heating pumps (first three pump are system wide pumps)
connect(to_real[4:(3+n)].y, Sj_speed_state[:].u2) annotation (Line(
       points={{180.4,50},{204,50},{204,77.6},{209.2,77.6}},
       color={0,0,127},
       smooth=Smooth.None));

  connect(pumps_state_algo.pumps_state, Pumps_state_ToReal.u) annotation (Line(
      points={{4.33333,-19},{20,-19},{20,-48.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Pumps_state_ToReal.y, pumps_state) annotation (Line(
      points={{20,-66.8},{20,-106}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(right_n.y, Sj_speed_state.u1) annotation (Line(
      points={{180.7,83},{195.35,83},{195.35,82.4},{209.2,82.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(right.y, S5_speed_state1.u2) annotation (Line(
      points={{180.7,-63},{194.35,-63},{194.35,-74.4},{209.2,-74.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(left.y, S5_speed_state.u2) annotation (Line(
      points={{180.7,-14},{194,-14},{194,-34.4},{209.2,-34.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(left.y, S6_speed_state.u2) annotation (Line(
      points={{180.7,-14},{194,-14},{194,5.6},{209.2,5.6}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-100},{240,100}},
          preserveAspectRatio=false), graphics, defaultComponentName = "pump_control"), Icon(coordinateSystem(extent={{-100,
            -100},{240,100}}, preserveAspectRatio=true),  graphics={
                  Rectangle(
          extent={{-100,100},{240,-100}},
          lineColor={90,150,90},
          fillPattern=FillPattern.CrossDiag,
          fillColor={0,127,127}),
        Text(
          extent={{-100,62},{238,16}},
          lineColor={0,0,0},
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="PID"),
        Text(
          extent={{-96,-20},{240,-52}},
          lineColor={0,0,0},
          textString="Flow switch",
          fontName="Consolas",
          textStyle={TextStyle.Bold})}),
    Documentation(info="<html>
<p>Control all pumps according to pump state from algo.</p>
</html>"));
end pump_pids_TOR;
