within SolarSystem.Control;
package Electrical_control
  "Describe a full electrical control for a solar combi system using air as heat vector."
  extends Modelica.Icons.VariantsPackage;

  model Equipements_control_monozone
    "Describe the control algorithm for solar air system with electrical auxiliary"
    import SI = Modelica.SIunits;

    extends Data.System_parameters_Monozone;


  //
  // General inputs
  //
    Modelica.Blocks.Interfaces.RealInput T1(unit="K")
      "Temperature inside collector"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-20,-20},{20,20}},
          origin={-1488,410}),
          iconTransformation(extent={{-18,-18},{18,18}},
          origin={-1478,440})));
    Modelica.Blocks.Interfaces.RealInput T3(unit="K")
      "Temperature inside DHW tank next to solar exchanger (bottom)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,360},{-1468,400}}), iconTransformation(
            extent={{-1496,382},{-1460,418}})));
    Modelica.Blocks.Interfaces.RealInput T4(unit="K")
      "Temperature inside DHW tank next to drawing up (top)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,332},{-1468,372}}), iconTransformation(
            extent={{-1496,302},{-1460,338}})));
    Modelica.Blocks.Interfaces.RealInput T5(unit="K")
      "Temperature inside solar tank (top)"
       annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,304},{-1468,344}}), iconTransformation(
            extent={{-1496,342},{-1460,378}})));
    Modelica.Blocks.Interfaces.RealInput T7(unit="K")
      "Temperature of water after terminal emitter (only one for all rooms)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1506,264},{-1466,304}}), iconTransformation(
            extent={{-1496,262},{-1460,298}})));
    Modelica.Blocks.Interfaces.RealInput       Tamb(each unit="K")
      "Vector of temperature inside rooms areas"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1506,
              228},{-1466,268}}),                                                                                       iconTransformation(
            extent={{-1496,186},{-1468,214}})));
    Modelica.Blocks.Interfaces.RealInput       Texch_out(each unit="K")
      "Vector of air temperature just after exchanger"      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(extent={{-1506,150},{-1466,190}}),
                                                      iconTransformation(extent={{-1496,152},{-1468,
              180}})));
    Modelica.Blocks.Interfaces.RealInput       Tsouff(each unit="K")
      "Vector of air temperature blowing in each room"   annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(extent={{-1506,190},{-1466,230}}),
                                                      iconTransformation(extent={{-1496,120},{-1468,
              148}})));
    Modelica.Blocks.Interfaces.RealInput Text(unit="K")
      "Temperature outside the room (environnement)"
                                              annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(
          extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-1486,132}),
                            iconTransformation(extent={{-18,-18},{18,18}},
          rotation=0,
          origin={-1478,240})));

  //
  // Describe the control of the solar valve (V3V)
  //
    Modelica.Blocks.Logical.OnOffController need_solar_DHW(pre_y_start=true, bandwidth=2)
      "Return True if solar must be used and False if solar must not be used"
      annotation (Placement(transformation(extent={{-1396,410},{-1384,422}})));

    Modelica.Blocks.Logical.OnOffController need_solar_Storage(pre_y_start=true,
        bandwidth=2)
      "Return True if solar must be used to load the storage tank"
      annotation (Placement(transformation(extent={{-1396,392},{-1384,404}})));
    Modelica.Blocks.Logical.Hysteresis enough_power_in_DHW(uLow=temp_min_DHW - 2, uHigh=
          temp_min_DHW)
      "Test if we have enough power inside the DHW before authorize temperature inside the storage tank"
      annotation (Placement(transformation(extent={{-1396,374},{-1384,386}})));
     Modelica.Blocks.Interfaces.BooleanOutput V3V_solar
      "State of the solar valve" annotation (__Dymola_tag={"Valve"}, Placement(transformation(
            extent={{-1344,398},{-1316,426}}), iconTransformation(extent={{-14,-14},
              {14,14}},
          rotation=90,
          origin={-1140,474})));

  //
  // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
  //
    Boolean S5_state_temp
      "Temporary state of the S5 pump, see S2 control part to get final state";
    Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T3(
      y(start=false),
      uLow=4,
      uHigh=solar_delta_min)
                "Check if T1 - T3 is larger enough"
      annotation (Placement(transformation(extent={{-1278,412},{-1266,424}})));
    Modelica.Blocks.Logical.OnOffController max_DHW_tank(bandwidth=5, pre_y_start=true)
      "Test if the temperature inside the tank is lower than reference"
      annotation (Placement(transformation(extent={{-1278,392},{-1266,404}})));

  //
  // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
  //
    Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T5(
      y(start=false),
      uLow=2.5,
      uHigh=solar_delta_min)
                "Check if T1 - T5 is larger enough"
      annotation (Placement(transformation(extent={{-1278,352},{-1268,362}})));
    Modelica.Blocks.Logical.OnOffController max_Storage_tank(bandwidth=5)
      "Test if the temperature inside the tank is lower than reference"
      annotation (Placement(transformation(extent={{-1278,336},{-1268,346}})));
     Modelica.Blocks.Interfaces.BooleanOutput S6_state
      "State of the solar pump S6." annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(
            extent={{-1224,342},{-1196,370}}), iconTransformation(extent={{-14,-14},
              {14,14}},
          rotation=-90,
          origin={-1140,106})));

  //
  // Describe the control of the electrical extra heating for DHW
  //
    Buildings.Controls.Continuous.LimPID pip_DHW_elec(
      controllerType=elecDHW_controllerType,
      k=elecDHW_k,
      Ti=elecDHW_Ti,
      Td=elecDHW_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "Controller for electrical heat to DHW"
      annotation (Placement(transformation(extent={{-1414,286},{-1404,296}})));
    Modelica.Blocks.Continuous.Integrator integrator_DHW_elec
      "Integration of the power used to keep domestic hot water at the setpoint temperature"
      annotation (Placement(transformation(extent={{-1392,284},{-1380,296}})));
    Modelica.Blocks.Interfaces.RealOutput power_DHW_elec( unit="W")
      "Output the power needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-1344,294},{-1316,322}}), iconTransformation(extent={{-940,216},
              {-916,240}})));
    Modelica.Blocks.Interfaces.RealOutput energy_DHW_elec( unit="J")
      "Output the cumulated energy needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-1344,264},{-1316,292}}), iconTransformation(extent={{-940,192},
              {-916,216}})));

  //
  // Describe how to control S2 and S5 pumps (check heat/overheat | direct/indirect solar energy)
  //
    Boolean direct_solar_state
      "Vector of Sn pump state on direct solar energy (one for each room)";
    Boolean indirect_solar_state
      "Vector of Sn pump state on indirect solar energy (one for each room)";
    Modelica.Blocks.Logical.OnOffController need_overheat(each bandwidth=1)
      annotation (Placement(transformation(extent={{-1410,190},{-1390,210}})));
    Modelica.Blocks.Logical.OnOffController need_heat(each bandwidth=0.5)
      annotation (Placement(transformation(extent={{-1410,220},{-1390,240}})));
    Modelica.Blocks.Logical.OnOffController can_heat_with_directSolar(bandwidth=
          solar_delta_min/2)
      "We can use direct solar energy"
      annotation (Placement(transformation(extent={{-1376,220},{-1356,240}})));
    Modelica.Blocks.Logical.Hysteresis enough_energy_storage(uLow=
          temp_min_storage - 5, uHigh=temp_min_storage)
      annotation (Placement(transformation(extent={{-1378,190},{-1358,210}})));
    Utilities.Timers.OnDelay_pause
                             S2_state_timer(each delayTime=60, y(each
          start=false)) "Return the current state of S2 pump (vector of state)"
      annotation (Placement(transformation(extent={{-1348,222},{-1332,238}})));
    Utilities.Timers.OnDelay_pause
                             S5_state_timer(each delayTime=60, y(each start=false))
      "Return the current state of S5 pump."
      annotation (Placement(transformation(extent={{-1348,192},{-1332,208}})));
  public
    Modelica.Blocks.Interfaces.BooleanOutput S5_state "Control S5 state"
      annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(extent={{-1324,184},{-1292,216}}), iconTransformation(
            extent={{-13,-13},{13,13}},
          rotation=-90,
          origin={-1113,107})));

  //
  // Describe how to compute temporization and flow modulation for S2, S6, S5
  //
    Modelica.Blocks.Interfaces.BooleanOutput S2_state
      "Final S2 pumps states after temporization check" annotation (__Dymola_tag={"Pump", "state"}, Placement(
          transformation(extent={{-1324,216},{-1296,244}}), iconTransformation(
          extent={{-14,-14},{14,14}},
          rotation=-90,
          origin={-1168,106})));

  //
  // Describe pid pump output construction for S6 and S5
  //
    Buildings.Controls.Continuous.LimPID pid_S6(
      controllerType=S6_controller,
      k=S6_k,
      Ti=S6_Ti,
      Td=S6_Td,
      yMax=S6_yMax,
      yMin=S6_yMin,
      reverseAction=true)
      annotation (Placement(transformation(extent={{-1118,226},{-1098,246}})));
    Buildings.Controls.Continuous.LimPID pid_S5(
      controllerType=S5_controller,
      k=S5_k,
      Ti=S5_Ti,
      Td=S5_Td,
      yMax=S5_yMax,
      yMin=S5_yMin,
      reverseAction=true)
      annotation (Placement(transformation(extent={{-1148,200},{-1128,220}})));

    Modelica.Blocks.Interfaces.RealOutput S5_speed( unit="1/min")
      "Final speed of S5 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
        Placement(transformation(extent={{-1086,226},{-1058,254}}), iconTransformation(extent={{-940,
              280},{-914,306}})));
    Modelica.Blocks.Interfaces.RealOutput S6_speed( unit="1/min")
      "Final speed of S6 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
        Placement(transformation(extent={{-1086,194},{-1058,222}}), iconTransformation(extent={{-940,
              306},{-914,332}})));

  //
  //Control of blowing flow rate/temperature
  //
    Boolean need_limit_flowRate
      "Boolean used to check if we need or not to reduce the blowing flow rate";
    Real temperature_instruction_souff
      "Computed temperature instruction [K]";
    Modelica.Blocks.Interfaces.RealOutput minimal_flowRate(each unit="m3/s")
      "Minimal sanitary volumic flow rate [m3/s]"                        annotation (Placement(
          transformation(extent={{-1094,412},{-1074,432}}), iconTransformation(extent={{-940,
              408},{-916,432}})));
    Modelica.Blocks.Interfaces.RealOutput fan_flowRate(each unit="m3/s")
      "Computed fan flow rate [m3/s]"                                              annotation (
        Placement(transformation(extent={{-1094,390},{-1074,410}}), iconTransformation(extent={{-940,
              436},{-916,460}})));
    Modelica.Blocks.Logical.Hysteresis need_higher_flow(uLow=Tsouff_max - 1.1, uHigh=Tsouff_max - 0.1)
      "Test if we need a higher flow rate to cover the heating demand"
      annotation (Placement(transformation(extent={{-1172,412},{-1158,426}})));
    Utilities.Timers.OnDelay_risingEdge
                              need_increase_flowRate(delayTime=tempo_algo_flowRate, y(start=
            false))
      "Time to wait before increasing blowing flow rate"
      annotation (Placement(transformation(extent={{-1144,412},{-1130,426}})));

    Modelica.Blocks.Logical.OnOffController flow_is_minimal(each bandwidth=10/3600)
      annotation (Placement(transformation(extent={{-1172,386},{-1158,400}})));
    Buildings.Controls.Continuous.LimPID pid_fan(
      controllerType=fan_controllerType,
      k=fan_k,
      Ti=fan_Ti,
      Td=fan_Td,
      each yMax=1,
      each yMin=0,
      reverseAction=false)
                   "PID used to control blowing flow rate for each room"
                  annotation (Placement(transformation(extent={{-1114,388},{
              -1102,400}})));
     Buildings.Controls.Continuous.LimPID pid_Tsouff(
      controllerType=Tsouff_controllerType,
      k=Tsouff_k,
      Ti=Tsouff_Ti,
      Td=Tsouff_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "PID which control the blowing temperature instruction"
      annotation (Placement(transformation(extent={{-1116,414},{-1104,426}})));

  //
  //Control of S2 pumps speed
  //
     Buildings.Controls.Continuous.LimPID pid_S2(
      controllerType=S2_controllerType,
      k=S2_k,
      Ti=S2_Ti,
      Td=S2_Td,
      yMax=S2_yMax,
      yMin=S2_yMin,
      reverseAction=false)
      "Compute correct factor in order to modulate S2 speed"
      annotation (Placement(transformation(extent={{-1176,312},{-1160,328}})));

    Utilities.Timers.OnDelay_risingEdge
                              need_electric_heater(delayTime=tempo_algo_elec)
      "Check if we still need electric heater after `wait_for` time."
      annotation (Placement(transformation(extent={{-1138,290},{-1124,304}})));
    Modelica.Blocks.Logical.OnOffController need_elec_to_air(each bandwidth=2)
      "Check if solar energy gives enough power to the blowing air source"
      annotation (Placement(transformation(extent={{-1164,290},{-1154,300}})));
    Modelica.Blocks.Interfaces.RealOutput S2_speed(each unit="1/min")
      "Output of each pump S2 speed"
      annotation (__Dymola_tag={"Pump", "speed"}, Placement(transformation(extent={{-1096,308},{-1076,328}}),
          iconTransformation(extent={{-940,332},{-914,358}})));

  //
  //Control of blowing electric heater
  //
      Buildings.Controls.Continuous.LimPID pid_heater_elec(
        controllerType=elecHeat_controllerType,
        k=elecHeat_k,
        Ti=elecHeat_Ti,
        Td=elecHeat_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "Controller for electric heater on blowing air for each room"
        annotation (Placement(transformation(extent={{-1048,308},{-1032,324}})));

    Modelica.Blocks.Continuous.Integrator integrator_heating_elec
      "Integration of the power used to keep room air at the setpoint temperature"
      annotation (Placement(transformation(extent={{-1014,294},{-998,310}})));
    Modelica.Blocks.Interfaces.RealOutput power_air_elec(each unit="W")
      "Output the power needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-976,316},{-948,344}}),   iconTransformation(extent={{-940,144},
              {-916,168}})));
    Modelica.Blocks.Interfaces.RealOutput energy_air_elec(each unit="J")
      "Output the cumulated energy needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-976,288},{-948,316}}),   iconTransformation(extent={{-940,120},
              {-916,144}})));
     Modelica.Blocks.Sources.CombiTimeTable             Schedules(
      tableOnFile=tableOnFile,
      table=table,
      tableName=tableName,
      fileName=fileName,
      verboseRead=verboseRead,
      columns=columns,
      each smoothness=smoothness,
      each extrapolation=extrapolation,
      offset=offset,
      startTime=startTime)
      "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
       annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(extent={{-1480,428},{-1448,460}})));

  equation
  //
  // Describe the control of the solar valve (V3V)
  //
    //Connect minimal between T4 and T3 for test with T1 (`+1` used to shift bandwidth upper)
    need_solar_DHW.u = Buildings.Utilities.Math.Functions.smoothMin(x1=T4,
                                                                    x2=T3,
                                                                    deltaX=0.1)+1
      "[T1 >= min(T3, T4)]";
    need_solar_Storage.u = T5+1 "[T1 >= T5";
    // V3V open to solar collector only if we have
    V3V_solar = (enough_power_in_DHW.y and need_solar_Storage.y) or need_solar_DHW.y or can_heat_with_directSolar.y
      "[T1 >= T5 and T3 >= 32�C] or [T1 >= min(T3, T4)] or [T1 >= max(T7, T_heatMini)]";

  //
  // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
  //
    enoughDelta_T1_T3.u = T1 - T3 "[T1 - T3 >= 10]";
    max_DHW_tank.reference = temp_max_DHW "[T3 < temp_max_DHW]";
    // Compute S5 pump state according to temperature difference
    S5_state_temp = V3V_solar and max_DHW_tank.y and enoughDelta_T1_T3.y
      "[T1 - T3 >= 10 and [T3 > temp_max_DHW] and V3V_solar]";

  //
  // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
  //
    enoughDelta_T1_T5.u = T1 - T5 "[T1 - T5 >= 10]";
    max_Storage_tank.reference = temp_max_storage-2.5 "[T5 > temp_max_DHW]";
    // Compute S6 pump state
    S6_state = enoughDelta_T1_T5.y and enough_power_in_DHW.y and max_Storage_tank.y
      "[T1 - T5 >= 10 and min(T3, T4) >= 30 and T5 > temp_max_DHW]";

  //
  // Describe the control of the electrical extra heating for DHW
  //
    pip_DHW_elec.u_s = DHW_setpoint "Set DHW temperature setpoint for PID";
    power_DHW_elec = DHW_elec_power * pip_DHW_elec.y "Power compute using PID";
    integrator_DHW_elec.u = power_DHW_elec
      "Same thing, only here to be used outside the model";

  //
  // Describe how to control S2 and S5 pumps (check heat/overheat | direct/indirect solar energy)
  //
  // Test temperature difference between T1 and max(T7 or minimal value to use direct solar)
    can_heat_with_directSolar.u = Buildings.Utilities.Math.Functions.smoothMax(x1=temp_min_collector,
                                                                             x2=T7,
                                                                             deltaX=0.1);
    // Connect T1 to solar_direct bloc OnOffController
    can_heat_with_directSolar.reference = T1 - (can_heat_with_directSolar.bandwidth / 2)
      "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";
    // S5 pump state according to heating demande control
    S5_state_timer.u = S5_state_temp
      "State of S5 pump according to heating demands";

  //
  // Describe pid pump output construction for S6 and S5
  //
    // Connection to S6 pid
    pid_S6.u_s = solar_delta_min;
    pid_S6.u_m = T1 - T5;
    // Only need to compute output if pump S5 is On
    S5_speed = smooth(1, (if S5_state then (S5_speed_max * pid_S5.y) else 0));

    // Connection to S5 pid
    pid_S5.u_s = solar_delta_min;
    pid_S5.u_m = T1 - T3;
    // Only need to compute output if pump S5 is On
    S6_speed = smooth(1, (if S6_state then (S6_speed_max * pid_S6.y) else 0));

  //
  //Control of S2 pumps speed and Control of blowing electric heater
  //
    // Connection to pids
    pid_S2.u_s = temperature_instruction_souff;
    pid_heater_elec.u_s = temperature_instruction_souff;
    need_elec_to_air.reference = temperature_instruction_souff - (need_elec_to_air.bandwidth / 2) - 0.1
      "Shift down reference temperature because exchanger outlet temperature cannot be higher than maximal blowing temperature value";
    integrator_heating_elec.u = power_air_elec
      "Connect power computed to integrator";



  //
  // Describe how to control S2 and S5 pumps (check heat/overheat | direct/indirect solar energy)
  //
      // Overheat connections
      need_overheat.reference = Schedules.y[2];
      need_overheat.u = Tamb - (need_overheat.bandwidth / 2)
        "bandwidth used to shift down minimal value";

      // Heat connection
      need_heat.reference = Schedules.y[1];
      need_heat.u = Tamb - (need_heat.bandwidth / 2)
        "bandwidth used to shift down minimal value";

      // We need to activate the S2 pump using direct solar energy from collectors
      direct_solar_state = V3V_solar and can_heat_with_directSolar.y and (need_heat.y or need_overheat.y);

      // We need to activate the S2 pump using indirect solar energy from tank
      indirect_solar_state = (not V3V_solar) and enough_energy_storage.y and need_heat.y;

      // Final state of the pump for the room j
      S2_state_timer.u = indirect_solar_state or direct_solar_state;
    //
    //Control of blowing flow rate/temperature
    //
      //Minimal flow rate reference
      flow_is_minimal.reference = Schedules.y[3] * 1.5;
      flow_is_minimal.u = fan_flowRate
        "RECURSIVE call to computed volumic flow rate";
      need_higher_flow.u = temperature_instruction_souff
        "RECURSIVE call to computed temperature instruction";
      // Check if we need to increase blowing flow rate
      need_increase_flowRate.u = need_heat.y and need_higher_flow.y;
      // Check if we need to limit the blowing flow rate or not
      need_limit_flowRate = not need_increase_flowRate.y;

    //
    // BLOWING VOLUMIC FLOW RATE COMPUTATION
    //
      // PID for fans volumic flow rate (one for each room)
      pid_fan.u_s = Schedules.y[1] "Reference";
      pid_fan.u_m = Tamb "Temperature from room air";
      // Compute volumic flow rate as max(minimal flow rate, (maximal flow rate time PID factor)
      fan_flowRate = Buildings.Utilities.Math.Functions.smoothMax(x1=fan_flowRate_max * (if pre(need_limit_flowRate) then 0 else pid_fan.y),
                                                                     x2=Schedules.y[3],
                                                                     deltaX=0.001);
      minimal_flowRate = Schedules.y[3]
        "Just used as getter for external uses";
    //
    // BLOWING TEMPERATURE INSTRUCTION COMPUTATION
    //
      // PID for temperature of blowing instruction (one for each room)
      pid_Tsouff.u_s = smooth(1, if ((not need_heat.y) and need_overheat.y) then Schedules.y[2]
                                else Schedules.y[1])
        "Reference depend if we need overheating or just heating";
      pid_Tsouff.u_m = Tamb "Temperature from room air";
      // Compute the blowing temperature.
      // We first check the difference between max blowing temperature and environment temperature
      // Then we apply the PID factor to it.
      // So we compute blowing temperature as Text + PID factor * delta(max - ext)
      temperature_instruction_souff = smooth(1, if pre(need_limit_flowRate) then (Text + pid_Tsouff.y * (Tsouff_max - Text)) else Tsouff_max);

    //
    //Control of S2 pumps speed and Control of blowing electric heater
    //
      // Compute speed for each pump (for each room) S2
      S2_speed = smooth(1, if S2_state then (S2_speed_max * pid_S2.y) else 0);
      // Compute boolean to check if we need or not the electric heater
      need_electric_heater.u = need_heat.y and need_elec_to_air.y;
      // Compute electric power needed to get blowing temperature instruction
      power_air_elec = smooth(1, if need_electric_heater.y then (heater_elec_power * pid_heater_elec.y) else 0);

    connect(T1, need_solar_DHW.reference) annotation (Line(
        points={{-1488,410},{-1440,410},{-1440,419.6},{-1397.2,419.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T1, need_solar_Storage.reference) annotation (Line(
        points={{-1488,410},{-1440,410},{-1440,401.6},{-1397.2,401.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T3, enough_power_in_DHW.u) annotation (Line(
        points={{-1488,380},{-1397.2,380}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T3, max_DHW_tank.u) annotation (Line(
        points={{-1488,380},{-1440,380},{-1440,360},{-1340,360},{-1340,394.4},{-1279.2,
            394.4}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T5, max_Storage_tank.u) annotation (Line(
        points={{-1488,324},{-1386,324},{-1386,338},{-1279,338}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T4, pip_DHW_elec.u_m) annotation (Line(
        points={{-1488,352},{-1458,352},{-1458,280},{-1409,280},{-1409,285}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator_DHW_elec.y, energy_DHW_elec) annotation (Line(
        points={{-1379.4,290},{-1354,290},{-1354,278},{-1330,278}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S5_state_timer.y, S5_state) annotation (Line(
        points={{-1331.2,200},{-1308,200}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T5, enough_energy_storage.u) annotation (Line(
        points={{-1488,324},{-1440,324},{-1440,188},{-1384,188},{-1384,200},{-1380,200}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Texch_out, pid_S2.u_m) annotation (Line(
        points={{-1486,170},{-1290,170},{-1290,310.4},{-1168,310.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Texch_out, need_elec_to_air.u) annotation (Line(
        points={{-1486,170},{-1290,170},{-1290,292},{-1165,292}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tsouff, pid_heater_elec.u_m) annotation (Line(
        points={{-1486,210},{-1466,210},{-1466,170},{-1040,170},{-1040,306.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator_heating_elec.y, energy_air_elec) annotation (Line(
        points={{-997.2,302},{-962,302}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S2_state_timer.y, S2_state) annotation (Line(points={{-1331.2,230},{-1326,
            230},{-1326,230},{-1310,230}}, color={255,0,255}));
    annotation (Diagram(coordinateSystem(extent={{-1480,120},{-940,460}},
            preserveAspectRatio=false), graphics={
          Rectangle(
            extent={{-1060,340},{-976,290}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-1180,342},{-1096,286}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-1420,260},{-1324,180}},
            lineColor={0,0,255},
            fillColor={255,170,85},
            fillPattern=FillPattern.Solid),       Rectangle(
            extent={{-1420,440},{-1344,372}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={85,170,255}),              Text(
            extent={{-1420,438},{-1344,432}},
            lineColor={0,0,255},
            textString="Control of the solar valve (V3V)"),
                                                  Rectangle(
            extent={{-1300,440},{-1224,388}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={255,170,85}),              Text(
            extent={{-1300,438},{-1224,432}},
            lineColor={0,0,255},
            textString="Control of S5 (DHW tank pump)"),
          Rectangle(
            extent={{-1300,380},{-1224,334}},
            lineColor={0,0,255},
            fillColor={255,170,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1300,376},{-1224,370}},
            lineColor={0,0,255},
            textString="Control of S6 (Storage tank pump)"),
          Rectangle(
            extent={{-1420,320},{-1344,270}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1420,318},{-1344,312}},
            lineColor={0,0,255},
            textString="Control of DHW electrical power"),
                                                  Text(
            extent={{-1410,258},{-1334,252}},
            lineColor={0,0,255},
            textString="Control of S2 pumps (emitters)"),
          Rectangle(
            extent={{-1160,260},{-1086,194}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1162,258},{-1086,252}},
            lineColor={0,0,255},
            textString="Pid controller for S5 and S6"),
          Rectangle(
            extent={{-1180,440},{-1094,380}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1176,436},{-1100,430}},
            lineColor={0,0,255},
            textString="Control of blowing flow and temperature"),
                                                  Text(
            extent={{-1178,340},{-1102,334}},
            lineColor={0,0,255},
            textString="PID controller for S2 (each room)"),
                                                  Text(
            extent={{-1058,338},{-982,332}},
            lineColor={0,0,255},
            textString="Control of electric air blowing heating")}),
                                                   Icon(coordinateSystem(extent={{-1480,
              120},{-940,460}},
                     preserveAspectRatio=false), graphics={
          Rectangle(
            extent={{-1480,462},{-940,122}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={255,85,85}),               Text(
            extent={{-1430,396},{-978,324}},
            lineColor={0,0,0},
            textString="Control of solar system
(pumps, valves, setpoint, ...)"),                 Text(
            extent={{-1444,242},{-972,162}},
            lineColor={0,0,0},
            textString="Control of air blowing system
(fans, setpoints, ...)")}),
      Documentation(info="<html>
<h4><span style=\"color: #00aa00\">Mise � jour: 2016-01-06</span></h4>
<p>Un sc&eacute;nario de consigne/consigne solaire diff&eacute;rent peut &ecirc;tre utilis&eacute; pour chaque pi&egrave;ce.</p>
<p><br><br><b><span style=\"color: #00aa00;\">Mise &agrave; jour: 2016-06-22</span></b></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">La pompe S5 ne se d&eacute;clenche plus lorsque le chauffage solaire passe en mode chauffage solaire direct (`direct_solar_state`)</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">La modulation du d&eacute;bit de soufflage est maintenant correctement pris en compte (suppression des contradictions entre sont activation et d&eacute;sactivation)</span></p>
<p><br>La modulation du d&eacute;bit &agrave; 50&percnt; a &eacute;t&eacute; vir&eacute; car le PID le fait tr&egrave;s bien tout seul. On conserve le smooth entre pid *max (state=True) et 0 (state=False)</p>
<p><br><b><span style=\"color: #00aa00;\">Mise &agrave; jour: 2016-07-01</span></b></p>
<p><br>Add a parameter to tweak the minimal temperature difference to turn on direct solar energy for S5, S6, and S2.</p>
<p><span style=\"color: #00ff00;\"></p><p><b></span><span style=\"color: #00aa00;\">Mise &agrave; jour: 2016-08-26</span></b></p>
<p>Mod&egrave;le pour monozone uniquement.</p>
<p>Possible de faire uniquement du chauffage solaire m&ecirc;me si la temp&eacute;rature des ballons est trop importante.</p>
<p><br><br><h4><span style=\"color: #008000\">Control of the solar valve (V3V)</span></h4></p>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of S5 (DHW tank pump)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of S6 (pump for Storage tank exchanger)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of the electrical extra heating for DHW</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control if we need heat or overheat</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of pump temporization and modulation</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of system pumps speed (S5 and S6)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of blowing instruction (temperature and fan volumic flow rate)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of Electrical heater for heating demand</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of solar pump for each water/air exchanger (one fo each room)</span></h4>
<p>Fonctionnel</p>
</html>"));
  end Equipements_control_monozone;

  model Equipements_control_monozone_2
    "Describe the control algorithm for solar air system with electrical auxiliary"
    import SI = Modelica.SIunits;

    extends Data.System_parameters_Monozone_2;

  //
  // General inputs
  //
    Modelica.Blocks.Interfaces.RealInput T1(unit="K")
      "Temperature inside collector"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-20,-20},{20,20}},
          origin={-1488,410}),
          iconTransformation(extent={{-18,-18},{18,18}},
          origin={-1478,440})));
    Modelica.Blocks.Interfaces.RealInput T3(unit="K")
      "Temperature inside DHW tank next to solar exchanger (bottom)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,360},{-1468,400}}), iconTransformation(
            extent={{-1496,382},{-1460,418}})));
    Modelica.Blocks.Interfaces.RealInput T4(unit="K")
      "Temperature inside DHW tank next to drawing up (top)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,332},{-1468,372}}), iconTransformation(
            extent={{-1496,302},{-1460,338}})));
    Modelica.Blocks.Interfaces.RealInput T5(unit="K")
      "Temperature inside solar tank (top)"
       annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,304},{-1468,344}}), iconTransformation(
            extent={{-1496,342},{-1460,378}})));
    Modelica.Blocks.Interfaces.RealInput T7(unit="K")
      "Temperature of water after terminal emitter (only one for all rooms)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1506,264},{-1466,304}}), iconTransformation(
            extent={{-1496,262},{-1460,298}})));
    Modelica.Blocks.Interfaces.RealInput       Tamb(each unit="K")
      "Vector of temperature inside rooms areas"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1506,
              228},{-1466,268}}),                                                                                       iconTransformation(
            extent={{-1496,186},{-1468,214}})));
    Modelica.Blocks.Interfaces.RealInput       Texch_out(each unit="K")
      "Vector of air temperature just after exchanger"      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(extent={{-1506,150},{-1466,190}}),
                                                      iconTransformation(extent={{-1496,152},{-1468,
              180}})));
    Modelica.Blocks.Interfaces.RealInput       Tsouff(each unit="K")
      "Vector of air temperature blowing in each room"   annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(extent={{-1506,190},{-1466,230}}),
                                                      iconTransformation(extent={{-1496,120},{-1468,
              148}})));
    Modelica.Blocks.Interfaces.RealInput Text(unit="K")
      "Temperature outside the room (environnement)"
                                              annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(
          extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-1486,132}),
                            iconTransformation(extent={{-18,-18},{18,18}},
          rotation=0,
          origin={-1478,240})));

  //
  // Describe the control of the solar valve (V3V)
  //
    Modelica.Blocks.Logical.OnOffController need_solar_DHW(pre_y_start=true,
        bandwidth=4)
      "Return True if solar must be used and False if solar must not be used"
      annotation (Placement(transformation(extent={{-1396,410},{-1384,422}})));

    Modelica.Blocks.Logical.OnOffController need_solar_Storage(pre_y_start=true,
        bandwidth=4)
      "Return True if solar must be used to load the storage tank"
      annotation (Placement(transformation(extent={{-1396,392},{-1384,404}})));
    Modelica.Blocks.Logical.Hysteresis enough_power_in_DHW(uLow=temp_min_DHW - 2, uHigh=
          temp_min_DHW)
      "Test if we have enough power inside the DHW before authorize temperature inside the storage tank"
      annotation (Placement(transformation(extent={{-1396,374},{-1384,386}})));
     Modelica.Blocks.Interfaces.BooleanOutput V3V_solar
      "State of the solar valve" annotation (__Dymola_tag={"Valve"}, Placement(transformation(
            extent={{-1344,398},{-1316,426}}), iconTransformation(extent={{-14,-14},
              {14,14}},
          rotation=90,
          origin={-1140,474})));

  //
  // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
  //
    Boolean S5_state_temp
      "Temporary state of the S5 pump, see S2 control part to get final state";
    Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T3(
      y(start=false),
      uLow=4,
      uHigh=solar_delta_min)
                "Check if T1 - T3 is larger enough"
      annotation (Placement(transformation(extent={{-1278,412},{-1266,424}})));
    Modelica.Blocks.Logical.OnOffController max_DHW_tank(bandwidth=5, pre_y_start=true)
      "Test if the temperature inside the tank is lower than reference"
      annotation (Placement(transformation(extent={{-1278,392},{-1266,404}})));

  //
  // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
  //
    Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T5(
      y(start=false),
      uLow=2.5,
      uHigh=solar_delta_min)
                "Check if T1 - T5 is larger enough"
      annotation (Placement(transformation(extent={{-1278,352},{-1268,362}})));
    Modelica.Blocks.Logical.OnOffController max_Storage_tank(bandwidth=5)
      "Test if the temperature inside the tank is lower than reference"
      annotation (Placement(transformation(extent={{-1278,336},{-1268,346}})));
     Modelica.Blocks.Interfaces.BooleanOutput S6_state
      "State of the solar pump S6." annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(
            extent={{-1224,342},{-1196,370}}), iconTransformation(extent={{-14,-14},
              {14,14}},
          rotation=-90,
          origin={-1140,106})));

  //
  // Describe the control of the electrical extra heating for DHW
  //
    Buildings.Controls.Continuous.LimPID pip_DHW_elec(
      controllerType=elecDHW_controllerType,
      k=elecDHW_k,
      Ti=elecDHW_Ti,
      Td=elecDHW_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "Controller for electrical heat to DHW"
      annotation (Placement(transformation(extent={{-1414,286},{-1404,296}})));
    Modelica.Blocks.Continuous.Integrator integrator_DHW_elec
      "Integration of the power used to keep domestic hot water at the setpoint temperature"
      annotation (Placement(transformation(extent={{-1392,284},{-1380,296}})));
    Modelica.Blocks.Interfaces.RealOutput power_DHW_elec( unit="W")
      "Output the power needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-1344,294},{-1316,322}}), iconTransformation(extent={{-940,216},
              {-916,240}})));
    Modelica.Blocks.Interfaces.RealOutput energy_DHW_elec( unit="J")
      "Output the cumulated energy needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-1344,264},{-1316,292}}), iconTransformation(extent={{-940,192},
              {-916,216}})));

  //
  // Describe how to control S2 and S5 pumps (check heat/overheat | direct/indirect solar energy)
  //
    Boolean direct_solar_state
      "Vector of Sn pump state on direct solar energy (one for each room)";
    Boolean indirect_solar_state
      "Vector of Sn pump state on indirect solar energy (one for each room)";
    Modelica.Blocks.Logical.OnOffController need_overheat(each bandwidth=1)
      annotation (Placement(transformation(extent={{-1410,190},{-1390,210}})));
    Modelica.Blocks.Logical.OnOffController need_heat(each bandwidth=0.5)
      annotation (Placement(transformation(extent={{-1410,218},{-1390,238}})));
    Modelica.Blocks.Logical.OnOffController can_heat_with_directSolar(bandwidth=5)
      "We can use direct solar energy"
      annotation (Placement(transformation(extent={{-1376,220},{-1356,240}})));
    Modelica.Blocks.Logical.OnOffController can_heat_with_indirectSolar(bandwidth=
         5)
      annotation (Placement(transformation(extent={{-1378,188},{-1358,208}})));
    Utilities.Timers.OnDelay_pause
                             S2_state_timer(each delayTime=60, y(each
          start=false)) "Return the current state of S2 pump (vector of state)"
      annotation (Placement(transformation(extent={{-1348,222},{-1332,238}})));
    Utilities.Timers.OnDelay_pause
                             S5_state_timer(each delayTime=60, y(each start=false))
      "Return the current state of S5 pump."
      annotation (Placement(transformation(extent={{-1348,192},{-1332,208}})));
  public
    Modelica.Blocks.Interfaces.BooleanOutput S5_state "Control S5 state"
      annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(extent={{-1324,184},{-1292,216}}), iconTransformation(
            extent={{-13,-13},{13,13}},
          rotation=-90,
          origin={-1113,107})));

  //
  // Describe how to compute temporization and flow modulation for S2, S6, S5
  //
    Modelica.Blocks.Interfaces.BooleanOutput S2_state
      "Final S2 pumps states after temporization check" annotation (__Dymola_tag={"Pump", "state"}, Placement(
          transformation(extent={{-1324,216},{-1296,244}}), iconTransformation(
          extent={{-14,-14},{14,14}},
          rotation=-90,
          origin={-1168,106})));

  //
  // Describe pid pump output construction for S6 and S5
  //
    Buildings.Controls.Continuous.LimPID pid_S6(
      controllerType=S6_controller,
      k=S6_k,
      Ti=S6_Ti,
      Td=S6_Td,
      yMax=S6_yMax,
      yMin=S6_yMin,
      reverseAction=true)
      annotation (Placement(transformation(extent={{-1118,226},{-1098,246}})));
    Buildings.Controls.Continuous.LimPID pid_S5(
      controllerType=S5_controller,
      k=S5_k,
      Ti=S5_Ti,
      Td=S5_Td,
      yMax=S5_yMax,
      yMin=S5_yMin,
      reverseAction=true)
      annotation (Placement(transformation(extent={{-1148,200},{-1128,220}})));

    Modelica.Blocks.Interfaces.RealOutput S5_speed( unit="1/min")
      "Final speed of S5 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
        Placement(transformation(extent={{-1086,226},{-1058,254}}), iconTransformation(extent={{-940,
              280},{-914,306}})));
    Modelica.Blocks.Interfaces.RealOutput S6_speed( unit="1/min")
      "Final speed of S6 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
        Placement(transformation(extent={{-1086,194},{-1058,222}}), iconTransformation(extent={{-940,
              306},{-914,332}})));

  //
  //Control of blowing flow rate/temperature
  //
    Boolean need_limit_flowRate
      "Boolean used to check if we need or not to reduce the blowing flow rate";
    Real temperature_instruction_souff
      "Computed temperature instruction [K]";
    Modelica.Blocks.Interfaces.RealOutput minimal_flowRate(each unit="m3/s")
      "Minimal sanitary volumic flow rate [m3/s]"                        annotation (Placement(
          transformation(extent={{-1094,412},{-1074,432}}), iconTransformation(extent={{-940,
              408},{-916,432}})));
    Modelica.Blocks.Interfaces.RealOutput fan_flowRate(each unit="m3/s")
      "Computed fan flow rate [m3/s]"                                              annotation (
        Placement(transformation(extent={{-1094,390},{-1074,410}}), iconTransformation(extent={{-940,
              436},{-916,460}})));
    Modelica.Blocks.Logical.Hysteresis need_higher_flow(uLow=Tsouff_max - 1.1, uHigh=Tsouff_max - 0.1)
      "Test if we need a higher flow rate to cover the heating demand"
      annotation (Placement(transformation(extent={{-1172,412},{-1158,426}})));
    Utilities.Timers.OnDelay_risingEdge
                              need_increase_flowRate(delayTime=tempo_algo_flowRate, y(start=
            false))
      "Time to wait before increasing blowing flow rate"
      annotation (Placement(transformation(extent={{-1144,412},{-1130,426}})));

    Modelica.Blocks.Logical.OnOffController flow_is_minimal(each bandwidth=10/3600)
      annotation (Placement(transformation(extent={{-1172,386},{-1158,400}})));
    Buildings.Controls.Continuous.LimPID pid_fan(
      controllerType=fan_controllerType,
      k=fan_k,
      Ti=fan_Ti,
      Td=fan_Td,
      each yMax=1,
      each yMin=0,
      reverseAction=false)
                   "PID used to control blowing flow rate for each room"
                  annotation (Placement(transformation(extent={{-1114,388},{
              -1102,400}})));
     Buildings.Controls.Continuous.LimPID pid_Tsouff(
      controllerType=Tsouff_controllerType,
      k=Tsouff_k,
      Ti=Tsouff_Ti,
      Td=Tsouff_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "PID which control the blowing temperature instruction"
      annotation (Placement(transformation(extent={{-1116,414},{-1104,426}})));

  //
  //Control of S2 pumps speed
  //
     Buildings.Controls.Continuous.LimPID pid_S2(
      controllerType=S2_controllerType,
      k=S2_k,
      Ti=S2_Ti,
      Td=S2_Td,
      yMax=S2_yMax,
      yMin=S2_yMin,
      reverseAction=false)
      "Compute correct factor in order to modulate S2 speed"
      annotation (Placement(transformation(extent={{-1176,312},{-1160,328}})));

    Utilities.Timers.OnDelay_risingEdge
                              need_electric_heater(delayTime=tempo_algo_elec)
      "Check if we still need electric heater after `wait_for` time."
      annotation (Placement(transformation(extent={{-1138,290},{-1124,304}})));
    Modelica.Blocks.Logical.OnOffController need_elec_to_air(each bandwidth=2)
      "Check if solar energy gives enough power to the blowing air source"
      annotation (Placement(transformation(extent={{-1164,290},{-1154,300}})));
    Modelica.Blocks.Interfaces.RealOutput S2_speed(each unit="1/min")
      "Output of each pump S2 speed"
      annotation (__Dymola_tag={"Pump", "speed"}, Placement(transformation(extent={{-1096,308},{-1076,328}}),
          iconTransformation(extent={{-940,332},{-914,358}})));

  //
  //Control of blowing electric heater
  //
      Buildings.Controls.Continuous.LimPID pid_heater_elec(
        controllerType=elecHeat_controllerType,
        k=elecHeat_k,
        Ti=elecHeat_Ti,
        Td=elecHeat_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "Controller for electric heater on blowing air for each room"
        annotation (Placement(transformation(extent={{-1048,308},{-1032,324}})));

    Modelica.Blocks.Continuous.Integrator integrator_heating_elec
      "Integration of the power used to keep room air at the setpoint temperature"
      annotation (Placement(transformation(extent={{-1014,294},{-998,310}})));
    Modelica.Blocks.Interfaces.RealOutput power_air_elec(each unit="W")
      "Output the power needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-976,316},{-948,344}}),   iconTransformation(extent={{-940,144},
              {-916,168}})));
    Modelica.Blocks.Interfaces.RealOutput energy_air_elec(each unit="J")
      "Output the cumulated energy needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-976,288},{-948,316}}),   iconTransformation(extent={{-940,120},
              {-916,144}})));
     Modelica.Blocks.Sources.CombiTimeTable             Schedules(
      tableOnFile=tableOnFile,
      table=table,
      tableName=tableName,
      fileName=fileName,
      verboseRead=verboseRead,
      columns=columns,
      each smoothness=smoothness,
      each extrapolation=extrapolation,
      offset=offset,
      startTime=startTime)
      "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
       annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(extent={{-1480,428},{-1448,460}})));

    Modelica.Blocks.Interfaces.RealInput Tmix(each unit="K")
      "Vector of air temperature just after exchanger" annotation (__Dymola_tag={"Temperature",
          "Computed"}, Placement(transformation(extent={{-1506,78},{-1466,118}}),
          iconTransformation(extent={{-1496,76},{-1468,104}})));
  equation
  //
  // Describe the control of the solar valve (V3V)
  //
    //Connect minimal between T4 and T3 for test with T1 (`+1` used to shift bandwidth upper)
    need_solar_DHW.u = Buildings.Utilities.Math.Functions.smoothMin(x1=T4,
                                                                    x2=T3,
                                                                    deltaX=0.1)+2
      "[T1 >= min(T3, T4)]";
    need_solar_Storage.u = T5+2 "[T1 >= T5";
    // V3V open to solar collector only if we have
    V3V_solar = (enough_power_in_DHW.y and need_solar_Storage.y) or need_solar_DHW.y or can_heat_with_directSolar.y
      "[T1 >= T5 and T3 >= 32�C] or [T1 >= min(T3, T4)] or [T1 >= max(T7, T_heatMini)]";

  //
  // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
  //
    enoughDelta_T1_T3.u = T1 - T3 "[T1 - T3 >= 10]";
    max_DHW_tank.reference = temp_max_DHW "[T3 < temp_max_DHW]";
    // Compute S5 pump state according to temperature difference
    S5_state_temp = V3V_solar and max_DHW_tank.y and enoughDelta_T1_T3.y
      "[T1 - T3 >= 10 and [T3 > temp_max_DHW] and V3V_solar]";

  //
  // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
  //
    enoughDelta_T1_T5.u = T1 - T5 "[T1 - T5 >= 10]";
    max_Storage_tank.reference = temp_max_storage-2.5 "[T5 > temp_max_DHW]";
    // Compute S6 pump state
    S6_state = enoughDelta_T1_T5.y and enough_power_in_DHW.y and max_Storage_tank.y
      "[T1 - T5 >= 10 and min(T3, T4) >= 30 and T5 > temp_max_DHW]";

  //
  // Describe the control of the electrical extra heating for DHW
  //
    pip_DHW_elec.u_s = DHW_setpoint "Set DHW temperature setpoint for PID";
    power_DHW_elec = DHW_elec_power * pip_DHW_elec.y "Power compute using PID";
    integrator_DHW_elec.u = power_DHW_elec
      "Same thing, only here to be used outside the model";

  //
  // Describe how to control S2 and S5 pumps (check heat/overheat | direct/indirect solar energy)
  //
  // Test temperature difference between T1 and max(T7 or minimal value to use direct solar)
    can_heat_with_directSolar.u = Tmix;
    // Connect T1 to solar_direct bloc OnOffController
    can_heat_with_directSolar.reference = T1 - DeltaMinCol - (can_heat_with_directSolar.bandwidth / 2)
      "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";
    // S5 pump state according to heating demande control
    S5_state_timer.u = S5_state_temp
      "State of S5 pump according to heating demands";


  // Test temperature difference between Tbefore exchanger and T5
    can_heat_with_indirectSolar.u = Tmix;
    // Connect T5 to solar_indirect bloc OnOffController
    can_heat_with_indirectSolar.reference = T5 -  DeltaMinBuffer - (can_heat_with_indirectSolar.bandwidth / 2)
      "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";
    // S5 pump state according to heating demande control
  //
  // Describe pid pump output construction for S6 and S5
  //
    // Connection to S6 pid
    pid_S6.u_s = solar_delta_min;
    pid_S6.u_m = T1 - T5;
    // Only need to compute output if pump S5 is On
    S5_speed = smooth(1, (if S5_state then (S5_speed_max * pid_S5.y) else 0));

    // Connection to S5 pid
    pid_S5.u_s = solar_delta_min;
    pid_S5.u_m = T1 - T3;
    // Only need to compute output if pump S5 is On
    S6_speed = smooth(1, (if S6_state then (S6_speed_max * pid_S6.y) else 0));

  //
  //Control of S2 pumps speed and Control of blowing electric heater
  //
    // Connection to pids
    pid_S2.u_s = temperature_instruction_souff;
    pid_heater_elec.u_s = temperature_instruction_souff;
    need_elec_to_air.reference = temperature_instruction_souff - (need_elec_to_air.bandwidth / 2) - 0.1
      "Shift down reference temperature because exchanger outlet temperature cannot be higher than maximal blowing temperature value";
    integrator_heating_elec.u = power_air_elec
      "Connect power computed to integrator";

  //
  // Describe how to control S2 and S5 pumps (check heat/overheat | direct/indirect solar energy)
  //
      // Overheat connections
      need_overheat.reference = Schedules.y[2];
      need_overheat.u = Tamb - (need_overheat.bandwidth / 2)
        "bandwidth used to shift down minimal value";

      // Heat connection
      need_heat.reference = Schedules.y[1];
      need_heat.u = Tamb - (need_heat.bandwidth / 2)
        "bandwidth used to shift down minimal value";

      // We need to activate the S2 pump using direct solar energy from collectors
      direct_solar_state = V3V_solar and can_heat_with_directSolar.y and (need_heat.y or need_overheat.y);

      // We need to activate the S2 pump using indirect solar energy from tank
      indirect_solar_state =(not V3V_solar) and can_heat_with_indirectSolar.y
       and need_heat.y;

      // Final state of the pump for the room j
      S2_state_timer.u = indirect_solar_state or direct_solar_state;
    //
    //Control of blowing flow rate/temperature
    //
      //Minimal flow rate reference
      flow_is_minimal.reference = Schedules.y[3] * 1.5;
      flow_is_minimal.u = fan_flowRate
        "RECURSIVE call to computed volumic flow rate";
      need_higher_flow.u = temperature_instruction_souff
        "RECURSIVE call to computed temperature instruction";
      // Check if we need to increase blowing flow rate
      need_increase_flowRate.u = need_heat.y and need_higher_flow.y;
      // Check if we need to limit the blowing flow rate or not
      need_limit_flowRate = not need_increase_flowRate.y;

    //
    // BLOWING VOLUMIC FLOW RATE COMPUTATION
    //
      // PID for fans volumic flow rate (one for each room)
      pid_fan.u_s = Schedules.y[1] "Reference";
      pid_fan.u_m = Tamb "Temperature from room air";
      // Compute volumic flow rate as max(minimal flow rate, (maximal flow rate time PID factor)
      fan_flowRate = Buildings.Utilities.Math.Functions.smoothMax(x1=fan_flowRate_max * (if pre(need_limit_flowRate) then 0 else pid_fan.y),
                                                                     x2=Schedules.y[3],
                                                                     deltaX=0.001);
      minimal_flowRate = Schedules.y[3]
        "Just used as getter for external uses";
    //
    // BLOWING TEMPERATURE INSTRUCTION COMPUTATION
    //
      // PID for temperature of blowing instruction (one for each room)
      pid_Tsouff.u_s = smooth(1, if ((not need_heat.y) and need_overheat.y) then Schedules.y[2]
                                else Schedules.y[1])
        "Reference depend if we need overheating or just heating";
      pid_Tsouff.u_m = Tamb "Temperature from room air";
      // Compute the blowing temperature.
      // We first check the difference between max blowing temperature and environment temperature
      // Then we apply the PID factor to it.
      // So we compute blowing temperature as Text + PID factor * delta(max - ext)
      temperature_instruction_souff = smooth(1, if pre(need_limit_flowRate) then (Text + pid_Tsouff.y * (Tsouff_max - Text)) else Tsouff_max);

    //
    //Control of S2 pumps speed and Control of blowing electric heater
    //
      // Compute speed for each pump (for each room) S2
      S2_speed = smooth(1, if S2_state then (S2_speed_max * pid_S2.y) else 0);
      // Compute boolean to check if we need or not the electric heater
      need_electric_heater.u = need_heat.y and need_elec_to_air.y;
      // Compute electric power needed to get blowing temperature instruction
      power_air_elec = smooth(1, if need_electric_heater.y then (heater_elec_power * pid_heater_elec.y) else 0);

    connect(T1, need_solar_DHW.reference) annotation (Line(
        points={{-1488,410},{-1440,410},{-1440,419.6},{-1397.2,419.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T1, need_solar_Storage.reference) annotation (Line(
        points={{-1488,410},{-1440,410},{-1440,401.6},{-1397.2,401.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T3, enough_power_in_DHW.u) annotation (Line(
        points={{-1488,380},{-1397.2,380}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T3, max_DHW_tank.u) annotation (Line(
        points={{-1488,380},{-1440,380},{-1440,360},{-1340,360},{-1340,394.4},{-1279.2,
            394.4}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T5, max_Storage_tank.u) annotation (Line(
        points={{-1488,324},{-1386,324},{-1386,338},{-1279,338}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T4, pip_DHW_elec.u_m) annotation (Line(
        points={{-1488,352},{-1458,352},{-1458,280},{-1409,280},{-1409,285}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator_DHW_elec.y, energy_DHW_elec) annotation (Line(
        points={{-1379.4,290},{-1354,290},{-1354,278},{-1330,278}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S5_state_timer.y, S5_state) annotation (Line(
        points={{-1331.2,200},{-1308,200}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Texch_out, pid_S2.u_m) annotation (Line(
        points={{-1486,170},{-1290,170},{-1290,310.4},{-1168,310.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Texch_out, need_elec_to_air.u) annotation (Line(
        points={{-1486,170},{-1290,170},{-1290,292},{-1165,292}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tsouff, pid_heater_elec.u_m) annotation (Line(
        points={{-1486,210},{-1466,210},{-1466,170},{-1040,170},{-1040,306.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator_heating_elec.y, energy_air_elec) annotation (Line(
        points={{-997.2,302},{-962,302}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S2_state_timer.y, S2_state) annotation (Line(points={{-1331.2,230},{-1326,
            230},{-1326,230},{-1310,230}}, color={255,0,255}));
    annotation (Diagram(coordinateSystem(extent={{-1480,120},{-940,460}},
            preserveAspectRatio=false), graphics={
          Rectangle(
            extent={{-1060,340},{-976,290}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-1180,342},{-1096,286}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-1420,260},{-1324,180}},
            lineColor={0,0,255},
            fillColor={255,170,85},
            fillPattern=FillPattern.Solid),       Rectangle(
            extent={{-1420,440},{-1344,372}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={85,170,255}),              Text(
            extent={{-1420,438},{-1344,432}},
            lineColor={0,0,255},
            textString="Control of the solar valve (V3V)"),
                                                  Rectangle(
            extent={{-1300,440},{-1224,388}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={255,170,85}),              Text(
            extent={{-1300,438},{-1224,432}},
            lineColor={0,0,255},
            textString="Control of S5 (DHW tank pump)"),
          Rectangle(
            extent={{-1300,380},{-1224,334}},
            lineColor={0,0,255},
            fillColor={255,170,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1300,376},{-1224,370}},
            lineColor={0,0,255},
            textString="Control of S6 (Storage tank pump)"),
          Rectangle(
            extent={{-1420,320},{-1344,270}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1420,318},{-1344,312}},
            lineColor={0,0,255},
            textString="Control of DHW electrical power"),
                                                  Text(
            extent={{-1410,258},{-1334,252}},
            lineColor={0,0,255},
            textString="Control of S2 pumps (emitters)"),
          Rectangle(
            extent={{-1160,260},{-1086,194}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1162,258},{-1086,252}},
            lineColor={0,0,255},
            textString="Pid controller for S5 and S6"),
          Rectangle(
            extent={{-1180,440},{-1094,380}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1176,436},{-1100,430}},
            lineColor={0,0,255},
            textString="Control of blowing flow and temperature"),
                                                  Text(
            extent={{-1178,340},{-1102,334}},
            lineColor={0,0,255},
            textString="PID controller for S2 (each room)"),
                                                  Text(
            extent={{-1058,338},{-982,332}},
            lineColor={0,0,255},
            textString="Control of electric air blowing heating")}),
                                                   Icon(coordinateSystem(extent={{-1480,
              120},{-940,460}},
                     preserveAspectRatio=false), graphics={
          Rectangle(
            extent={{-1480,462},{-940,122}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={255,85,85}),               Text(
            extent={{-1430,396},{-978,324}},
            lineColor={0,0,0},
            textString="Control of solar system
(pumps, valves, setpoint, ...)"),                 Text(
            extent={{-1444,242},{-972,162}},
            lineColor={0,0,0},
            textString="Control of air blowing system
(fans, setpoints, ...)")}),
      Documentation(info="<html>
<h4><span style=\"color: #00aa00\">Mise � jour: 2016-01-06</span></h4>
<p>Un sc&eacute;nario de consigne/consigne solaire diff&eacute;rent peut &ecirc;tre utilis&eacute; pour chaque pi&egrave;ce.</p>
<p><br><br><b><span style=\"color: #00aa00;\">Mise &agrave; jour: 2016-06-22</span></b></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">La pompe S5 ne se d&eacute;clenche plus lorsque le chauffage solaire passe en mode chauffage solaire direct (`direct_solar_state`)</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">La modulation du d&eacute;bit de soufflage est maintenant correctement pris en compte (suppression des contradictions entre sont activation et d&eacute;sactivation)</span></p>
<p><br>La modulation du d&eacute;bit &agrave; 50&percnt; a &eacute;t&eacute; vir&eacute; car le PID le fait tr&egrave;s bien tout seul. On conserve le smooth entre pid *max (state=True) et 0 (state=False)</p>
<p><br><b><span style=\"color: #00aa00;\">Mise &agrave; jour: 2016-07-01</span></b></p>
<p><br>Add a parameter to tweak the minimal temperature difference to turn on direct solar energy for S5, S6, and S2.</p>
<p><span style=\"color: #00ff00;\"></p><p><b></span><span style=\"color: #00aa00;\">Mise &agrave; jour: 2016-08-26</span></b></p>
<p>Mod&egrave;le pour monozone uniquement.</p>
<p>Possible de faire uniquement du chauffage solaire m&ecirc;me si la temp&eacute;rature des ballons est trop importante.</p>
<p><br><br><h4><span style=\"color: #008000\">Control of the solar valve (V3V)</span></h4></p>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of S5 (DHW tank pump)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of S6 (pump for Storage tank exchanger)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of the electrical extra heating for DHW</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control if we need heat or overheat</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of pump temporization and modulation</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of system pumps speed (S5 and S6)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of blowing instruction (temperature and fan volumic flow rate)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of Electrical heater for heating demand</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of solar pump for each water/air exchanger (one fo each room)</span></h4>
<p>Fonctionnel</p>
</html>"));
  end Equipements_control_monozone_2;

  model ReferenceControl_28
    "Controller for solar system 28 reference system using only electrical energy."
    import SI = Modelica.SIunits;

    extends Data.System_parameters_Monozone_2;

     Modelica.Blocks.Sources.CombiTimeTable             Schedules(
      tableOnFile=tableOnFile,
      table=table,
      tableName=tableName,
      fileName=fileName,
      verboseRead=verboseRead,
      columns=columns,
      each smoothness=smoothness,
      each extrapolation=extrapolation,
      offset=offset,
      startTime=startTime)
      "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
       annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(extent={{-658,
              168},{-626,200}})));
    Modelica.Blocks.Logical.Hysteresis need_higher_flow(uLow=Tsouff_max - 1.1, uHigh=Tsouff_max - 0.1)
      "Test if we need a higher flow rate to cover the heating demand"
      annotation (Placement(transformation(extent={{-486,158},{-472,172}})));
    Utilities.Timers.OnDelay_risingEdge
                              need_increase_flowRate(delayTime=tempo_algo_flowRate, y(start=
            false))
      "Time to wait before increasing blowing flow rate"
      annotation (Placement(transformation(extent={{-456,134},{-442,148}})));
    Modelica.Blocks.Logical.OnOffController flow_is_minimal(each bandwidth=10/3600)
      annotation (Placement(transformation(extent={{-484,100},{-470,114}})));
    Buildings.Controls.Continuous.LimPID pid_fan(
      controllerType=fan_controllerType,
      k=fan_k,
      Ti=fan_Ti,
      Td=fan_Td,
      each yMax=1,
      each yMin=0,
      reverseAction=false)
                   "PID used to control blowing flow rate for each room"
                  annotation (Placement(transformation(extent={{-428,106},{-416,118}})));
     Buildings.Controls.Continuous.LimPID pid_Tsouff(
      controllerType=Tsouff_controllerType,
      k=Tsouff_k,
      Ti=Tsouff_Ti,
      Td=Tsouff_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "PID which control the blowing temperature instruction"
      annotation (Placement(transformation(extent={{-422,160},{-410,172}})));

  //
  //Control of blowing flow rate/temperature
  //
    Boolean need_limit_flowRate
      "Boolean used to check if we need or not to reduce the blowing flow rate";
    Real temperature_instruction_souff
      "Computed temperature instruction [K]";

    Modelica.Blocks.Interfaces.RealOutput minimal_flowRate(each unit="m3/s")
      "Minimal sanitary volumic flow rate [m3/s]"                        annotation (Placement(
          transformation(extent={{-284,164},{-264,184}}),   iconTransformation(extent={{-288,90},
              {-264,114}})));
    Modelica.Blocks.Interfaces.RealOutput fan_flowRate(each unit="m3/s")
      "Computed fan flow rate [m3/s]"             annotation (
        Placement(transformation(extent={{-284,142},{-264,162}}),   iconTransformation(extent={{-288,
              118},{-264,142}})));

  //
  //Control of blowing electric heater
  //
      Buildings.Controls.Continuous.LimPID pid_heater_elec(
        controllerType=elecHeat_controllerType,
        k=elecHeat_k,
        Ti=elecHeat_Ti,
        Td=elecHeat_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "Controller for electric heater on blowing air for each room"
        annotation (Placement(transformation(extent={{-476,6},{-460,22}})));
    Modelica.Blocks.Continuous.Integrator integrator_heating_elec
      "Integration of the power used to keep room air at the setpoint temperature"
      annotation (Placement(transformation(extent={{-476,-38},{-460,-22}})));
    Modelica.Blocks.Interfaces.RealOutput power_air_elec(each unit="W")
      "Output the power needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-284,-16},{-256,12}}),    iconTransformation(extent={{-288,-174},
              {-264,-150}})));
    Modelica.Blocks.Interfaces.RealOutput energy_air_elec(each unit="J")
      "Output the cumulated energy needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-284,-44},{-256,-16}}),   iconTransformation(extent={{-288,-198},
              {-264,-174}})));
    Modelica.Blocks.Interfaces.RealInput       Tamb(each unit="K")
      "Vector of temperature inside rooms areas"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-680,
              -10},{-640,30}}),                                                                                         iconTransformation(
            extent={{-672,-106},{-634,-68}})));
    Modelica.Blocks.Interfaces.RealInput       Tsouff(each unit="K")
      "Vector of air temperature blowing in each room"   annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(extent={{-680,-48},{-640,-8}}),
                                                      iconTransformation(extent={{-670,
              -220},{-634,-184}})));
    Modelica.Blocks.Interfaces.RealInput Text(unit="K")
      "Temperature outside the room (environnement)"
                                              annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(
          extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-660,146}),
                            iconTransformation(extent={{-18,-18},{18,18}},
          rotation=0,
          origin={-654,-34})));

  //
  // Describe the control of the electrical extra heating for DHW
  //
    Buildings.Controls.Continuous.LimPID pip_DHW_elec(
      controllerType=elecDHW_controllerType,
      k=elecDHW_k,
      Ti=elecDHW_Ti,
      Td=elecDHW_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "Controller for electrical heat to DHW"
      annotation (Placement(transformation(extent={{-498,-186},{-488,-176}})));
    Modelica.Blocks.Continuous.Integrator integrator_DHW_elec
      "Integration of the power used to keep domestic hot water at the setpoint temperature"
      annotation (Placement(transformation(extent={{-456,-188},{-444,-176}})));
    Modelica.Blocks.Interfaces.RealOutput power_DHW_elec( unit="W")
      "Output the power needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-280,-166},{-252,-138}}), iconTransformation(extent={{-292,-20},
              {-268,4}})));
    Modelica.Blocks.Interfaces.RealOutput energy_DHW_elec( unit="J")
      "Output the cumulated energy needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-280,-196},{-252,-168}}), iconTransformation(extent={{-292,-44},
              {-268,-20}})));
    Modelica.Blocks.Interfaces.RealInput T4(unit="K")
      "Temperature inside DHW tank next to drawing up (top)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-680,
              -180},{-640,-140}}),                                                                                      iconTransformation(
            extent={{-670,56},{-634,92}})));
    Modelica.Blocks.Interfaces.RealInput T3(unit="K")
      "Temperature inside DHW tank next to solar exchanger (bottom)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-680,
              -150},{-640,-110}}),                                                                                      iconTransformation(
            extent={{-670,110},{-634,146}})));
    Modelica.Blocks.Logical.OnOffController need_heat(each bandwidth=0.5)
      annotation (Placement(transformation(extent={{-588,84},{-568,104}})));

  equation

  //
  // Describe the control of the electrical extra heating for DHW
  //
    pip_DHW_elec.u_s = DHW_setpoint "Set DHW temperature setpoint for PID";
    power_DHW_elec = DHW_elec_power * pip_DHW_elec.y "Power compute using PID";
    integrator_DHW_elec.u = power_DHW_elec
      "Same thing, only here to be used outside the model";

  //
  //Control of blowing electric heater
  //
    // Connection to pid
    pid_heater_elec.u_s = temperature_instruction_souff;
    integrator_heating_elec.u = power_air_elec
      "Connect power computed to integrator";

      // Heat connection
      need_heat.reference = Schedules.y[1];
      need_heat.u = Tamb - (need_heat.bandwidth / 2)
        "bandwidth used to shift down minimal value";
    //
    //Control of blowing flow rate/temperature
    //
      //Minimal flow rate reference
      flow_is_minimal.reference = Schedules.y[3] * 1.5;
      flow_is_minimal.u = fan_flowRate
        "RECURSIVE call to computed volumic flow rate";
      need_higher_flow.u = temperature_instruction_souff
        "RECURSIVE call to computed temperature instruction";
      // Check if we need to increase blowing flow rate
      need_increase_flowRate.u = need_heat.y and need_higher_flow.y;
      // Check if we need to limit the blowing flow rate or not
      need_limit_flowRate = not need_increase_flowRate.y;

    //
    // BLOWING VOLUMIC FLOW RATE COMPUTATION
    //
      // PID for fans volumic flow rate (one for each room)
      pid_fan.u_s = Schedules.y[1] "Reference";
      pid_fan.u_m = Tamb "Temperature from room air";
      // Compute volumic flow rate as max(minimal flow rate, (maximal flow rate time PID factor)
      fan_flowRate = Buildings.Utilities.Math.Functions.smoothMax(x1=fan_flowRate_max * (if pre(need_limit_flowRate) then 0 else pid_fan.y),
                                                                     x2=Schedules.y[3],
                                                                     deltaX=0.001);
      minimal_flowRate = Schedules.y[3]
        "Just used as getter for external uses";
    //
    // BLOWING TEMPERATURE INSTRUCTION COMPUTATION
    //
      // PID for temperature of blowing instruction (one for each room)
      pid_Tsouff.u_s = Schedules.y[1]
        "Reference depend if we need overheating or just heating";
      pid_Tsouff.u_m = Tamb "Temperature from room air";
      // Compute the blowing temperature.
      // We first check the difference between max blowing temperature and environment temperature
      // Then we apply the PID factor to it.
      // So we compute blowing temperature as Text + PID factor * delta(max - ext)
      temperature_instruction_souff = smooth(1, if pre(need_limit_flowRate) then (Text + pid_Tsouff.y * (Tsouff_max - Text)) else Tsouff_max);

    //
    //Control of S2 pumps speed and Control of blowing electric heater
    //
      // Compute electric power needed to get blowing temperature instruction
      power_air_elec = smooth(1, if need_heat.y then (heater_elec_power * pid_heater_elec.y) else 0);

    connect(integrator_heating_elec.y,energy_air_elec)  annotation (Line(
        points={{-459.2,-30},{-270,-30}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator_DHW_elec.y,energy_DHW_elec)  annotation (Line(
        points={{-443.4,-182},{-266,-182}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T4, pip_DHW_elec.u_m) annotation (Line(points={{-660,-160},{-580,-160},
            {-580,-208},{-493,-208},{-493,-187}}, color={0,0,127}));
    connect(Tsouff, pid_heater_elec.u_m) annotation (Line(points={{-660,-28},{-520,
            -28},{-520,-2},{-468,-2},{-468,4.4}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-660,-240},
              {-280,200}}), graphics={
          Rectangle(
            extent={{-660,200},{-282,-238}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={255,85,85}),               Text(
            extent={{-636,52},{-308,-88}},
            lineColor={0,0,0},
            textString="Electrical controller")}),
                             Diagram(coordinateSystem(preserveAspectRatio=false,
            extent={{-660,-240},{-280,200}}), graphics={
          Rectangle(
            extent={{-492,192},{-402,90}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-488,188},{-412,182}},
            lineColor={0,0,255},
            textString="Control of blowing flow and temperature"),
          Rectangle(
            extent={{-496,44},{-408,-56}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-494,42},{-418,36}},
            lineColor={0,0,255},
            textString="Control of electric air blowing heating"),
          Rectangle(
            extent={{-512,-132},{-428,-202}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-508,-134},{-432,-140}},
            lineColor={0,0,255},
            textString="Control of DHW electrical power")}));
  end ReferenceControl_28;

  model Equipements_control_monozone_3
    "Describe the control algorithm for solar air system with electrical auxiliary"
    import SI = Modelica.SIunits;

    extends Data.System_parameters_Monozone_2;

  //
  // General inputs
  //
    Modelica.Blocks.Interfaces.RealInput T1(unit="K")
      "Temperature inside collector"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-20,-20},{20,20}},
          origin={-1488,410}),
          iconTransformation(extent={{-18,-18},{18,18}},
          origin={-1478,440})));
    Modelica.Blocks.Interfaces.RealInput T3(unit="K")
      "Temperature inside DHW tank next to solar exchanger (bottom)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,360},{-1468,400}}), iconTransformation(
            extent={{-1496,382},{-1460,418}})));
    Modelica.Blocks.Interfaces.RealInput T4(unit="K")
      "Temperature inside DHW tank next to drawing up (top)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,332},{-1468,372}}), iconTransformation(
            extent={{-1496,302},{-1460,338}})));
    Modelica.Blocks.Interfaces.RealInput T5(unit="K")
      "Temperature inside solar tank (top)"
       annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,304},{-1468,344}}), iconTransformation(
            extent={{-1496,342},{-1460,378}})));
    Modelica.Blocks.Interfaces.RealInput T7(unit="K")
      "Temperature of water after terminal emitter (only one for all rooms)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1506,264},{-1466,304}}), iconTransformation(
            extent={{-1496,262},{-1460,298}})));
    Modelica.Blocks.Interfaces.RealInput       Tamb(each unit="K")
      "Vector of temperature inside rooms areas"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1506,
              228},{-1466,268}}),                                                                                       iconTransformation(
            extent={{-1496,186},{-1468,214}})));
    Modelica.Blocks.Interfaces.RealInput       Texch_out(each unit="K")
      "Vector of air temperature just after exchanger"      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(extent={{-1506,150},{-1466,190}}),
                                                      iconTransformation(extent={{-1496,152},{-1468,
              180}})));
    Modelica.Blocks.Interfaces.RealInput       Tsouff(each unit="K")
      "Vector of air temperature blowing in each room"   annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(extent={{-1506,190},{-1466,230}}),
                                                      iconTransformation(extent={{-1496,120},{-1468,
              148}})));
    Modelica.Blocks.Interfaces.RealInput Text(unit="K")
      "Temperature outside the room (environnement)"
                                              annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(
          extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-1486,132}),
                            iconTransformation(extent={{-18,-18},{18,18}},
          rotation=0,
          origin={-1478,240})));

  //
  // Describe the control of the solar valve (V3V)
  //
    Modelica.Blocks.Logical.OnOffController need_solar_DHW(pre_y_start=true, bandwidth=2)
      "Return True if solar must be used and False if solar must not be used"
      annotation (Placement(transformation(extent={{-1396,410},{-1384,422}})));

    Modelica.Blocks.Logical.OnOffController need_solar_Storage(pre_y_start=true,
        bandwidth=2)
      "Return True if solar must be used to load the storage tank"
      annotation (Placement(transformation(extent={{-1396,392},{-1384,404}})));
    Modelica.Blocks.Logical.Hysteresis enough_power_in_DHW(uLow=temp_min_DHW - 2, uHigh=
          temp_min_DHW)
      "Test if we have enough power inside the DHW before authorize temperature inside the storage tank"
      annotation (Placement(transformation(extent={{-1396,374},{-1384,386}})));
     Modelica.Blocks.Interfaces.BooleanOutput V3V_solar
      "State of the solar valve" annotation (__Dymola_tag={"Valve"}, Placement(transformation(
            extent={{-1344,398},{-1316,426}}), iconTransformation(extent={{-14,-14},
              {14,14}},
          rotation=90,
          origin={-1140,474})));

  //
  // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
  //
    Boolean S5_state_temp
      "Temporary state of the S5 pump, see S2 control part to get final state";
    Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T3(
      y(start=false),
      uLow=4,
      uHigh=solar_delta_min)
                "Check if T1 - T3 is larger enough"
      annotation (Placement(transformation(extent={{-1278,412},{-1266,424}})));
    Modelica.Blocks.Logical.OnOffController max_DHW_tank(bandwidth=5, pre_y_start=true)
      "Test if the temperature inside the tank is lower than reference"
      annotation (Placement(transformation(extent={{-1278,392},{-1266,404}})));

  //
  // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
  //
    Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T5(
      y(start=false),
      uLow=2.5,
      uHigh=solar_delta_min)
                "Check if T1 - T5 is larger enough"
      annotation (Placement(transformation(extent={{-1278,352},{-1268,362}})));
    Modelica.Blocks.Logical.OnOffController max_Storage_tank(bandwidth=5)
      "Test if the temperature inside the tank is lower than reference"
      annotation (Placement(transformation(extent={{-1278,336},{-1268,346}})));
     Modelica.Blocks.Interfaces.BooleanOutput S6_state
      "State of the solar pump S6." annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(
            extent={{-1224,342},{-1196,370}}), iconTransformation(extent={{-14,-14},
              {14,14}},
          rotation=-90,
          origin={-1140,106})));

  //
  // Describe the control of the electrical extra heating for DHW
  //
    Buildings.Controls.Continuous.LimPID pip_DHW_elec(
      controllerType=elecDHW_controllerType,
      k=elecDHW_k,
      Ti=elecDHW_Ti,
      Td=elecDHW_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "Controller for electrical heat to DHW"
      annotation (Placement(transformation(extent={{-1414,286},{-1404,296}})));
    Modelica.Blocks.Continuous.Integrator integrator_DHW_elec
      "Integration of the power used to keep domestic hot water at the setpoint temperature"
      annotation (Placement(transformation(extent={{-1392,284},{-1380,296}})));
    Modelica.Blocks.Interfaces.RealOutput power_DHW_elec( unit="W")
      "Output the power needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-1344,294},{-1316,322}}), iconTransformation(extent={{-940,216},
              {-916,240}})));
    Modelica.Blocks.Interfaces.RealOutput energy_DHW_elec( unit="J")
      "Output the cumulated energy needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-1344,264},{-1316,292}}), iconTransformation(extent={{-940,192},
              {-916,216}})));

  //
  // Describe how to control S2 and S5 pumps (check heat/overheat | direct/indirect solar energy)
  //
    Boolean direct_solar_state
      "Vector of Sn pump state on direct solar energy (one for each room)";
    Boolean indirect_solar_state
      "Vector of Sn pump state on indirect solar energy (one for each room)";
    Modelica.Blocks.Logical.OnOffController need_overheat(each bandwidth=1)
      annotation (Placement(transformation(extent={{-1410,190},{-1390,210}})));
    Modelica.Blocks.Logical.OnOffController need_heat(each bandwidth=0.5)
      annotation (Placement(transformation(extent={{-1410,220},{-1390,240}})));
    Modelica.Blocks.Logical.OnOffController can_heat_with_directSolar(bandwidth=
          solar_delta_min/2)
      "We can use direct solar energy"
      annotation (Placement(transformation(extent={{-1376,220},{-1356,240}})));
    Modelica.Blocks.Logical.OnOffController can_heat_with_indirectSolar(bandwidth=
         5)
      annotation (Placement(transformation(extent={{-1378,188},{-1358,208}})));
    Utilities.Timers.OnDelay_pause
                             S2_state_timer(each delayTime=60, y(each
          start=false)) "Return the current state of S2 pump (vector of state)"
      annotation (Placement(transformation(extent={{-1348,222},{-1332,238}})));
    Utilities.Timers.OnDelay_pause
                             S5_state_timer(each delayTime=60, y(each start=false))
      "Return the current state of S5 pump."
      annotation (Placement(transformation(extent={{-1348,192},{-1332,208}})));
  public
    Modelica.Blocks.Interfaces.BooleanOutput S5_state "Control S5 state"
      annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(extent={{-1324,184},{-1292,216}}), iconTransformation(
            extent={{-13,-13},{13,13}},
          rotation=-90,
          origin={-1113,107})));

  //
  // Describe how to compute temporization and flow modulation for S2, S6, S5
  //
    Modelica.Blocks.Interfaces.BooleanOutput S2_state
      "Final S2 pumps states after temporization check" annotation (__Dymola_tag={"Pump", "state"}, Placement(
          transformation(extent={{-1324,216},{-1296,244}}), iconTransformation(
          extent={{-14,-14},{14,14}},
          rotation=-90,
          origin={-1168,106})));

  //
  // Describe pid pump output construction for S6 and S5
  //
    Buildings.Controls.Continuous.LimPID pid_S6(
      controllerType=S6_controller,
      k=S6_k,
      Ti=S6_Ti,
      Td=S6_Td,
      yMax=S6_yMax,
      yMin=S6_yMin,
      reverseAction=true)
      annotation (Placement(transformation(extent={{-1118,226},{-1098,246}})));
    Buildings.Controls.Continuous.LimPID pid_S5(
      controllerType=S5_controller,
      k=S5_k,
      Ti=S5_Ti,
      Td=S5_Td,
      yMax=S5_yMax,
      yMin=S5_yMin,
      reverseAction=true)
      annotation (Placement(transformation(extent={{-1148,200},{-1128,220}})));

    Modelica.Blocks.Interfaces.RealOutput S5_speed( unit="1/min")
      "Final speed of S5 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
        Placement(transformation(extent={{-1086,226},{-1058,254}}), iconTransformation(extent={{-940,
              280},{-914,306}})));
    Modelica.Blocks.Interfaces.RealOutput S6_speed( unit="1/min")
      "Final speed of S6 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
        Placement(transformation(extent={{-1086,194},{-1058,222}}), iconTransformation(extent={{-940,
              306},{-914,332}})));

  //
  //Control of blowing flow rate/temperature
  //
    Real temperature_instruction_souff
      "Computed temperature instruction [K]";
    Modelica.Blocks.Interfaces.RealOutput minimal_flowRate(each unit="m3/s")
      "Minimal sanitary volumic flow rate [m3/s]"                        annotation (Placement(
          transformation(extent={{-1094,412},{-1074,432}}), iconTransformation(extent={{-940,
              408},{-916,432}})));
    Modelica.Blocks.Interfaces.RealOutput fan_flowRate(each unit="m3/s")
      "Computed fan flow rate [m3/s]"                                              annotation (
        Placement(transformation(extent={{-1094,390},{-1074,410}}), iconTransformation(extent={{-940,
              436},{-916,460}})));

    Buildings.Controls.Continuous.LimPID pid_fan(
      controllerType=fan_controllerType,
      k=fan_k,
      Ti=fan_Ti,
      Td=fan_Td,
      each yMax=1,
      each yMin=0,
      reverseAction=false)
                   "PID used to control blowing flow rate for each room"
                  annotation (Placement(transformation(extent={{-1146,400},{-1134,
              412}})));

  //
  //Control of S2 pumps speed
  //
     Buildings.Controls.Continuous.LimPID pid_S2(
      controllerType=S2_controllerType,
      k=S2_k,
      Ti=S2_Ti,
      Td=S2_Td,
      yMax=S2_yMax,
      yMin=S2_yMin,
      reverseAction=false)
      "Compute correct factor in order to modulate S2 speed"
      annotation (Placement(transformation(extent={{-1176,312},{-1160,328}})));

    Utilities.Timers.OnDelay_risingEdge
                              need_electric_heater(delayTime=tempo_algo_elec)
      "Check if we still need electric heater after `wait_for` time."
      annotation (Placement(transformation(extent={{-1138,290},{-1124,304}})));
    Modelica.Blocks.Logical.OnOffController need_elec_to_air(each bandwidth=2)
      "Check if solar energy gives enough power to the blowing air source"
      annotation (Placement(transformation(extent={{-1164,290},{-1154,300}})));
    Modelica.Blocks.Interfaces.RealOutput S2_speed(each unit="1/min")
      "Output of each pump S2 speed"
      annotation (__Dymola_tag={"Pump", "speed"}, Placement(transformation(extent={{-1096,308},{-1076,328}}),
          iconTransformation(extent={{-940,332},{-914,358}})));

  //
  //Control of blowing electric heater
  //
      Buildings.Controls.Continuous.LimPID pid_heater_elec(
        controllerType=elecHeat_controllerType,
        k=elecHeat_k,
        Ti=elecHeat_Ti,
        Td=elecHeat_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "Controller for electric heater on blowing air for each room"
        annotation (Placement(transformation(extent={{-1048,308},{-1032,324}})));

    Modelica.Blocks.Continuous.Integrator integrator_heating_elec
      "Integration of the power used to keep room air at the setpoint temperature"
      annotation (Placement(transformation(extent={{-1014,294},{-998,310}})));
    Modelica.Blocks.Interfaces.RealOutput power_air_elec(each unit="W")
      "Output the power needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-976,316},{-948,344}}),   iconTransformation(extent={{-940,144},
              {-916,168}})));
    Modelica.Blocks.Interfaces.RealOutput energy_air_elec(each unit="J")
      "Output the cumulated energy needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-976,288},{-948,316}}),   iconTransformation(extent={{-940,120},
              {-916,144}})));
     Modelica.Blocks.Sources.CombiTimeTable             Schedules(
      tableOnFile=tableOnFile,
      table=table,
      tableName=tableName,
      fileName=fileName,
      verboseRead=verboseRead,
      columns=columns,
      each smoothness=smoothness,
      each extrapolation=extrapolation,
      offset=offset,
      startTime=startTime)
      "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
       annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(extent={{-1480,428},{-1448,460}})));

    Modelica.Blocks.Interfaces.RealInput Tmix(each unit="K")
      "Vector of air temperature just after exchanger" annotation (__Dymola_tag={"Temperature",
          "Computed"}, Placement(transformation(extent={{-1506,78},{-1466,118}}),
          iconTransformation(extent={{-1496,76},{-1468,104}})));
  equation
  //
  // Describe the control of the solar valve (V3V)
  //
    //Connect minimal between T4 and T3 for test with T1 (`+1` used to shift bandwidth upper)
    need_solar_DHW.u = Buildings.Utilities.Math.Functions.smoothMin(x1=T4,
                                                                    x2=T3,
                                                                    deltaX=0.1)+1
      "[T1 >= min(T3, T4)]";
    need_solar_Storage.u = T5+1 "[T1 >= T5";
    // V3V open to solar collector only if we have
    V3V_solar = (enough_power_in_DHW.y and need_solar_Storage.y) or need_solar_DHW.y or can_heat_with_directSolar.y
      "[T1 >= T5 and T3 >= 32�C] or [T1 >= min(T3, T4)] or [T1 >= max(T7, T_heatMini)]";

  //
  // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
  //
    enoughDelta_T1_T3.u = T1 - T3 "[T1 - T3 >= 10]";
    max_DHW_tank.reference = temp_max_DHW "[T3 < temp_max_DHW]";
    // Compute S5 pump state according to temperature difference
    S5_state_temp = V3V_solar and max_DHW_tank.y and enoughDelta_T1_T3.y
      "[T1 - T3 >= 10 and [T3 > temp_max_DHW] and V3V_solar]";

  //
  // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
  //
    enoughDelta_T1_T5.u = T1 - T5 "[T1 - T5 >= 10]";
    max_Storage_tank.reference = temp_max_storage-2.5 "[T5 > temp_max_DHW]";
    // Compute S6 pump state
    S6_state = enoughDelta_T1_T5.y and enough_power_in_DHW.y and max_Storage_tank.y
      "[T1 - T5 >= 10 and min(T3, T4) >= 30 and T5 > temp_max_DHW]";

  //
  // Describe the control of the electrical extra heating for DHW
  //
    pip_DHW_elec.u_s = DHW_setpoint "Set DHW temperature setpoint for PID";
    power_DHW_elec = DHW_elec_power * pip_DHW_elec.y "Power compute using PID";
    integrator_DHW_elec.u = power_DHW_elec
      "Same thing, only here to be used outside the model";

  //
  // Describe how to control S2 and S5 pumps (check heat/overheat | direct/indirect solar energy)
  //
  // Test temperature difference between T1 and max(T7 or minimal value to use direct solar)
    can_heat_with_directSolar.u = Tmix;
    // Connect T1 to solar_direct bloc OnOffController
    can_heat_with_directSolar.reference = T1 - DeltaMinCol - (can_heat_with_directSolar.bandwidth / 2)
      "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";
    // S5 pump state according to heating demande control
    S5_state_timer.u = S5_state_temp
      "State of S5 pump according to heating demands";

  // Test temperature difference between Tbefore exchanger and T5
    can_heat_with_indirectSolar.u = Tmix;
    // Connect T5 to solar_indirect bloc OnOffController
    can_heat_with_indirectSolar.reference = T5 -  DeltaMinBuffer - (can_heat_with_indirectSolar.bandwidth / 2)
      "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";
    // S5 pump state according to heating demande control
  //
  // Describe pid pump output construction for S6 and S5
  //
    // Connection to S6 pid
    pid_S6.u_s = solar_delta_min;
    pid_S6.u_m = T1 - T5;
    // Only need to compute output if pump S5 is On
    S5_speed = smooth(1, (if S5_state then (S5_speed_max * pid_S5.y) else 0));

    // Connection to S5 pid
    pid_S5.u_s = solar_delta_min;
    pid_S5.u_m = T1 - T3;
    // Only need to compute output if pump S5 is On
    S6_speed = smooth(1, (if S6_state then (S6_speed_max * pid_S6.y) else 0));

  //
  //Control of S2 pumps speed and Control of blowing electric heater
  //
    // Connection to pids
    pid_S2.u_s = temperature_instruction_souff;
    pid_heater_elec.u_s = temperature_instruction_souff;
    need_elec_to_air.reference = temperature_instruction_souff - (need_elec_to_air.bandwidth / 2) - 0.1
      "Shift down reference temperature because exchanger outlet temperature cannot be higher than maximal blowing temperature value";
    integrator_heating_elec.u = power_air_elec
      "Connect power computed to integrator";

  //
  // Describe how to control S2 and S5 pumps (check heat/overheat | direct/indirect solar energy)
  //
      // Overheat connections
      need_overheat.reference = Schedules.y[2];
      need_overheat.u = Tamb - (need_overheat.bandwidth / 2)
        "bandwidth used to shift down minimal value";

      // Heat connection
      need_heat.reference = Schedules.y[1];
      need_heat.u = Tamb - (need_heat.bandwidth / 2)
        "bandwidth used to shift down minimal value";

      // We need to activate the S2 pump using direct solar energy from collectors
      direct_solar_state = V3V_solar and can_heat_with_directSolar.y and (need_heat.y or need_overheat.y);

      // We need to activate the S2 pump using indirect solar energy from tank
      indirect_solar_state =(not V3V_solar) and can_heat_with_indirectSolar.y
       and need_heat.y;

      // Final state of the pump for the room j
      S2_state_timer.u = indirect_solar_state or direct_solar_state;


    //
    // BLOWING VOLUMIC FLOW RATE COMPUTATION
    //
      // PID for fans volumic flow rate (one for each room)
      pid_fan.u_s = Schedules.y[1] "Reference";
      pid_fan.u_m = Tamb "Temperature from room air";
      // Compute volumic flow rate as max(minimal flow rate, (maximal flow rate time PID factor)
      fan_flowRate = minimal_flowRate + smooth(1,  if need_heat.y then (pid_fan.y * (fan_flowRate_max - minimal_flowRate)) else 0);
      minimal_flowRate = Schedules.y[3]
        "Just used as getter for external uses";
    //
    // BLOWING TEMPERATURE INSTRUCTION COMPUTATION
    //

      // Compute the blowing temperature as Tmax if need heat or overheat and 16 if not.
      // No activated because solar pump on only if need_heat or overheat
      temperature_instruction_souff = smooth(1, if (need_heat.y or need_overheat.y) then Tsouff_max else 16);

    //
    //Control of S2 pumps speed and Control of blowing electric heater
    //
      // Compute speed for each pump (for each room) S2
      S2_speed = smooth(1, if S2_state then (S2_speed_max * pid_S2.y) else 0);
      // Compute boolean to check if we need or not the electric heater
      need_electric_heater.u = need_heat.y and need_elec_to_air.y;
      // Compute electric power needed to get blowing temperature instruction
      power_air_elec = smooth(1, if need_electric_heater.y then (heater_elec_power * pid_heater_elec.y) else 0);

    connect(T1, need_solar_DHW.reference) annotation (Line(
        points={{-1488,410},{-1440,410},{-1440,419.6},{-1397.2,419.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T1, need_solar_Storage.reference) annotation (Line(
        points={{-1488,410},{-1440,410},{-1440,401.6},{-1397.2,401.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T3, enough_power_in_DHW.u) annotation (Line(
        points={{-1488,380},{-1397.2,380}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T3, max_DHW_tank.u) annotation (Line(
        points={{-1488,380},{-1440,380},{-1440,360},{-1340,360},{-1340,394.4},{-1279.2,
            394.4}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T5, max_Storage_tank.u) annotation (Line(
        points={{-1488,324},{-1386,324},{-1386,338},{-1279,338}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T4, pip_DHW_elec.u_m) annotation (Line(
        points={{-1488,352},{-1458,352},{-1458,280},{-1409,280},{-1409,285}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator_DHW_elec.y, energy_DHW_elec) annotation (Line(
        points={{-1379.4,290},{-1354,290},{-1354,278},{-1330,278}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S5_state_timer.y, S5_state) annotation (Line(
        points={{-1331.2,200},{-1308,200}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(Texch_out, pid_S2.u_m) annotation (Line(
        points={{-1486,170},{-1290,170},{-1290,310.4},{-1168,310.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Texch_out, need_elec_to_air.u) annotation (Line(
        points={{-1486,170},{-1290,170},{-1290,292},{-1165,292}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tsouff, pid_heater_elec.u_m) annotation (Line(
        points={{-1486,210},{-1466,210},{-1466,170},{-1040,170},{-1040,306.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator_heating_elec.y, energy_air_elec) annotation (Line(
        points={{-997.2,302},{-962,302}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(S2_state_timer.y, S2_state) annotation (Line(points={{-1331.2,230},{-1326,
            230},{-1326,230},{-1310,230}}, color={255,0,255}));
    annotation (Diagram(coordinateSystem(extent={{-1480,120},{-940,460}},
            preserveAspectRatio=false), graphics={
          Rectangle(
            extent={{-1060,340},{-976,290}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-1180,342},{-1096,286}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-1420,260},{-1324,180}},
            lineColor={0,0,255},
            fillColor={255,170,85},
            fillPattern=FillPattern.Solid),       Rectangle(
            extent={{-1420,440},{-1344,372}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={85,170,255}),              Text(
            extent={{-1420,438},{-1344,432}},
            lineColor={0,0,255},
            textString="Control of the solar valve (V3V)"),
                                                  Rectangle(
            extent={{-1300,440},{-1224,388}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={255,170,85}),              Text(
            extent={{-1300,438},{-1224,432}},
            lineColor={0,0,255},
            textString="Control of S5 (DHW tank pump)"),
          Rectangle(
            extent={{-1300,380},{-1224,334}},
            lineColor={0,0,255},
            fillColor={255,170,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1300,376},{-1224,370}},
            lineColor={0,0,255},
            textString="Control of S6 (Storage tank pump)"),
          Rectangle(
            extent={{-1420,320},{-1344,270}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1420,318},{-1344,312}},
            lineColor={0,0,255},
            textString="Control of DHW electrical power"),
                                                  Text(
            extent={{-1410,258},{-1334,252}},
            lineColor={0,0,255},
            textString="Control of S2 pumps (emitters)"),
          Rectangle(
            extent={{-1160,260},{-1086,194}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1162,258},{-1086,252}},
            lineColor={0,0,255},
            textString="Pid controller for S5 and S6"),
          Rectangle(
            extent={{-1180,440},{-1094,380}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1176,436},{-1100,430}},
            lineColor={0,0,255},
            textString="Control of blowing flow and temperature"),
                                                  Text(
            extent={{-1178,340},{-1102,334}},
            lineColor={0,0,255},
            textString="PID controller for S2 (each room)"),
                                                  Text(
            extent={{-1058,338},{-982,332}},
            lineColor={0,0,255},
            textString="Control of electric air blowing heating")}),
                                                   Icon(coordinateSystem(extent={{-1480,
              120},{-940,460}},
                     preserveAspectRatio=false), graphics={
          Rectangle(
            extent={{-1480,462},{-940,122}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={255,85,85}),               Text(
            extent={{-1430,396},{-978,324}},
            lineColor={0,0,0},
            textString="Control of solar system
(pumps, valves, setpoint, ...)"),                 Text(
            extent={{-1444,242},{-972,162}},
            lineColor={0,0,0},
            textString="Control of air blowing system
(fans, setpoints, ...)")}),
      Documentation(info="<html>
<h4><span style=\"color: #00aa00\">Mise � jour: 2016-01-06</span></h4>
<p>Un sc&eacute;nario de consigne/consigne solaire diff&eacute;rent peut &ecirc;tre utilis&eacute; pour chaque pi&egrave;ce.</p>
<p><br><br><b><span style=\"color: #00aa00;\">Mise &agrave; jour: 2016-06-22</span></b></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">La pompe S5 ne se d&eacute;clenche plus lorsque le chauffage solaire passe en mode chauffage solaire direct (`direct_solar_state`)</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">La modulation du d&eacute;bit de soufflage est maintenant correctement pris en compte (suppression des contradictions entre sont activation et d&eacute;sactivation)</span></p>
<p><br>La modulation du d&eacute;bit &agrave; 50&percnt; a &eacute;t&eacute; vir&eacute; car le PID le fait tr&egrave;s bien tout seul. On conserve le smooth entre pid *max (state=True) et 0 (state=False)</p>
<p><br><b><span style=\"color: #00aa00;\">Mise &agrave; jour: 2016-07-01</span></b></p>
<p><br>Add a parameter to tweak the minimal temperature difference to turn on direct solar energy for S5, S6, and S2.</p>
<p><span style=\"color: #00ff00;\"></p><p><b></span><span style=\"color: #00aa00;\">Mise &agrave; jour: 2016-08-26</span></b></p>
<p>Mod&egrave;le pour monozone uniquement.</p>
<p>Possible de faire uniquement du chauffage solaire m&ecirc;me si la temp&eacute;rature des ballons est trop importante.</p>
<p><br><br><h4><span style=\"color: #008000\">Control of the solar valve (V3V)</span></h4></p>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of S5 (DHW tank pump)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of S6 (pump for Storage tank exchanger)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of the electrical extra heating for DHW</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control if we need heat or overheat</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of pump temporization and modulation</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of system pumps speed (S5 and S6)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of blowing instruction (temperature and fan volumic flow rate)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of Electrical heater for heating demand</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of solar pump for each water/air exchanger (one fo each room)</span></h4>
<p>Fonctionnel</p>
</html>"));
  end Equipements_control_monozone_3;

  model ReferenceControl_29
    "Controller for solar system 29 reference system using only electrical energy."
    import SI = Modelica.SIunits;

    extends Data.System_parameters_Monozone_2;

     Modelica.Blocks.Sources.CombiTimeTable             Schedules(
      tableOnFile=tableOnFile,
      table=table,
      tableName=tableName,
      fileName=fileName,
      verboseRead=verboseRead,
      columns=columns,
      each smoothness=smoothness,
      each extrapolation=extrapolation,
      offset=offset,
      startTime=startTime)
      "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
       annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(extent={{-658,
              168},{-626,200}})));
    Buildings.Controls.Continuous.LimPID pid_fan(
      controllerType=fan_controllerType,
      k=fan_k,
      Ti=fan_Ti,
      Td=fan_Td,
      each yMax=1,
      each yMin=0,
      reverseAction=false)
                   "PID used to control blowing flow rate for each room"
                  annotation (Placement(transformation(extent={{-428,106},{-416,118}})));

  //
  //Control of blowing flow rate/temperature
  //
    Real temperature_instruction_souff
      "Computed temperature instruction [K]";

    Modelica.Blocks.Interfaces.RealOutput minimal_flowRate(each unit="m3/s")
      "Minimal sanitary volumic flow rate [m3/s]"                        annotation (Placement(
          transformation(extent={{-284,164},{-264,184}}),   iconTransformation(extent={{-288,90},
              {-264,114}})));
    Modelica.Blocks.Interfaces.RealOutput fan_flowRate(each unit="m3/s")
      "Computed fan flow rate [m3/s]"             annotation (
        Placement(transformation(extent={{-284,142},{-264,162}}),   iconTransformation(extent={{-288,
              118},{-264,142}})));

  //
  //Control of blowing electric heater
  //
      Buildings.Controls.Continuous.LimPID pid_heater_elec(
        controllerType=elecHeat_controllerType,
        k=elecHeat_k,
        Ti=elecHeat_Ti,
        Td=elecHeat_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "Controller for electric heater on blowing air for each room"
        annotation (Placement(transformation(extent={{-476,6},{-460,22}})));
    Modelica.Blocks.Continuous.Integrator integrator_heating_elec
      "Integration of the power used to keep room air at the setpoint temperature"
      annotation (Placement(transformation(extent={{-476,-38},{-460,-22}})));
    Modelica.Blocks.Interfaces.RealOutput power_air_elec(each unit="W")
      "Output the power needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-284,-16},{-256,12}}),    iconTransformation(extent={{-288,-174},
              {-264,-150}})));
    Modelica.Blocks.Interfaces.RealOutput energy_air_elec(each unit="J")
      "Output the cumulated energy needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-284,-44},{-256,-16}}),   iconTransformation(extent={{-288,-198},
              {-264,-174}})));
    Modelica.Blocks.Interfaces.RealInput       Tamb(each unit="K")
      "Vector of temperature inside rooms areas"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-680,
              -10},{-640,30}}),                                                                                         iconTransformation(
            extent={{-672,-106},{-634,-68}})));
    Modelica.Blocks.Interfaces.RealInput       Tsouff(each unit="K")
      "Vector of air temperature blowing in each room"   annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(extent={{-680,-48},{-640,-8}}),
                                                      iconTransformation(extent={{-670,
              -220},{-634,-184}})));
    Modelica.Blocks.Interfaces.RealInput Text(unit="K")
      "Temperature outside the room (environnement)"
                                              annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(
          extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-660,146}),
                            iconTransformation(extent={{-18,-18},{18,18}},
          rotation=0,
          origin={-654,-34})));

  //
  // Describe the control of the electrical extra heating for DHW
  //
    Buildings.Controls.Continuous.LimPID pip_DHW_elec(
      controllerType=elecDHW_controllerType,
      k=elecDHW_k,
      Ti=elecDHW_Ti,
      Td=elecDHW_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "Controller for electrical heat to DHW"
      annotation (Placement(transformation(extent={{-498,-186},{-488,-176}})));
    Modelica.Blocks.Continuous.Integrator integrator_DHW_elec
      "Integration of the power used to keep domestic hot water at the setpoint temperature"
      annotation (Placement(transformation(extent={{-456,-188},{-444,-176}})));
    Modelica.Blocks.Interfaces.RealOutput power_DHW_elec( unit="W")
      "Output the power needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-280,-166},{-252,-138}}), iconTransformation(extent={{-292,-20},
              {-268,4}})));
    Modelica.Blocks.Interfaces.RealOutput energy_DHW_elec( unit="J")
      "Output the cumulated energy needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-280,-196},{-252,-168}}), iconTransformation(extent={{-292,-44},
              {-268,-20}})));
    Modelica.Blocks.Interfaces.RealInput T4(unit="K")
      "Temperature inside DHW tank next to drawing up (top)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-680,
              -180},{-640,-140}}),                                                                                      iconTransformation(
            extent={{-670,56},{-634,92}})));
    Modelica.Blocks.Interfaces.RealInput T3(unit="K")
      "Temperature inside DHW tank next to solar exchanger (bottom)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-680,
              -150},{-640,-110}}),                                                                                      iconTransformation(
            extent={{-670,110},{-634,146}})));
    Modelica.Blocks.Logical.OnOffController need_heat(each bandwidth=0.5)
      annotation (Placement(transformation(extent={{-588,84},{-568,104}})));

  equation

  //
  // Describe the control of the electrical extra heating for DHW
  //
    pip_DHW_elec.u_s = DHW_setpoint "Set DHW temperature setpoint for PID";
    power_DHW_elec = DHW_elec_power * pip_DHW_elec.y "Power compute using PID";
    integrator_DHW_elec.u = power_DHW_elec
      "Same thing, only here to be used outside the model";

  //
  //Control of blowing electric heater
  //
    // Connection to pid
    pid_heater_elec.u_s = temperature_instruction_souff;
    integrator_heating_elec.u = power_air_elec
      "Connect power computed to integrator";

      // Heat connection
      need_heat.reference = Schedules.y[1];
      need_heat.u = Tamb - (need_heat.bandwidth / 2)
        "bandwidth used to shift down minimal value";

    //
    // BLOWING VOLUMIC FLOW RATE COMPUTATION
    //
      // PID for fans volumic flow rate (one for each room)
      pid_fan.u_s = Schedules.y[1] "Reference";
      pid_fan.u_m = Tamb "Temperature from room air";
      // Compute volumic flow rate as max(minimal flow rate, (maximal flow rate time PID factor)
      fan_flowRate = minimal_flowRate + smooth(1,  if need_heat.y then (pid_fan.y * (fan_flowRate_max - minimal_flowRate)) else 0);
      minimal_flowRate = Schedules.y[3]
        "Just used as getter for external uses";
    //
    // BLOWING TEMPERATURE INSTRUCTION COMPUTATION
    //

      // Compute the blowing temperature as Tmax if need heat or overheat and 16 if not.
      // No activated because solar pump on only if need_heat or overheat
      temperature_instruction_souff = smooth(1, if need_heat.y then Tsouff_max else 16);

    //
    //Control of S2 pumps speed and Control of blowing electric heater
    //
      // Compute electric power needed to get blowing temperature instruction
      power_air_elec = smooth(1, if need_heat.y then (heater_elec_power * pid_heater_elec.y) else 0);

    connect(integrator_heating_elec.y,energy_air_elec)  annotation (Line(
        points={{-459.2,-30},{-270,-30}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator_DHW_elec.y,energy_DHW_elec)  annotation (Line(
        points={{-443.4,-182},{-266,-182}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T4, pip_DHW_elec.u_m) annotation (Line(points={{-660,-160},{-580,-160},
            {-580,-208},{-493,-208},{-493,-187}}, color={0,0,127}));
    connect(Tsouff, pid_heater_elec.u_m) annotation (Line(points={{-660,-28},{-520,
            -28},{-520,-2},{-468,-2},{-468,4.4}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-660,-240},
              {-280,200}}), graphics={
          Rectangle(
            extent={{-660,200},{-282,-238}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={255,85,85}),               Text(
            extent={{-636,52},{-308,-88}},
            lineColor={0,0,0},
            textString="Electrical controller")}),
                             Diagram(coordinateSystem(preserveAspectRatio=false,
            extent={{-660,-240},{-280,200}}), graphics={
          Rectangle(
            extent={{-492,192},{-402,90}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-488,188},{-412,182}},
            lineColor={0,0,255},
            textString="Control of blowing flow and temperature"),
          Rectangle(
            extent={{-496,44},{-408,-56}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-494,42},{-418,36}},
            lineColor={0,0,255},
            textString="Control of electric air blowing heating"),
          Rectangle(
            extent={{-512,-132},{-428,-202}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-508,-134},{-432,-140}},
            lineColor={0,0,255},
            textString="Control of DHW electrical power")}));
  end ReferenceControl_29;

  package Data
    extends Modelica.Icons.MaterialPropertiesPackage;
    record PID_parameters "Record for all PID inside the system to control"

      extends System_parameters;

      // S6 PID parameters
      parameter Modelica.Blocks.Types.SimpleController S6_controller=Modelica.Blocks.Types.SimpleController.PI
        "Type of controller" annotation (Dialog(tab="S6", group="PID controller"));
      parameter Real S6_k=2.5 "Gain of controller" annotation (Dialog(tab="S6", group="PID controller"));
      parameter Modelica.SIunits.Time S6_Ti=175
        "Time constant of Integrator block"
        annotation (Dialog(tab="S6", group="PID controller"));
      parameter Modelica.SIunits.Time S6_Td=0.1
        "Time constant of Derivative block"
        annotation (Dialog(tab="S6", group="PID controller"));
         parameter Real S6_yMax=1 "Upper limit of output" annotation (Dialog(tab="S6", group="PID controller"));
      parameter Real S6_yMin=0.25 "Lower limit of output" annotation (Dialog(tab="S6", group="PID controller"));
      parameter Real S6_wp=1 "Set-point weight for Proportional block (0..1)"
        annotation (Dialog(tab="S6", group="PID controller"));
      parameter Real S6_wd=0 "Set-point weight for Derivative block (0..1)"
        annotation (Dialog(tab="S6", group="PID controller"));

    // S5 PID parameters
      parameter Modelica.Blocks.Types.SimpleController S5_controller=Modelica.Blocks.Types.SimpleController.PID
        "Type of controller" annotation (Dialog(tab="S5", group="PID controller"));
      parameter Real S5_k=5 "Gain of controller" annotation (Dialog(tab="S5", group="PID controller"));
      parameter Modelica.SIunits.Time S5_Ti=200
        "Time constant of Integrator block"
        annotation (Dialog(tab="S5", group="PID controller"));
      parameter Modelica.SIunits.Time S5_Td=20
        "Time constant of Derivative block"
        annotation (Dialog(tab="S5", group="PID controller"));
      parameter Real S5_yMax=1 "Upper limit of output" annotation (Dialog(tab="S5", group="PID controller"));
      parameter Real S5_yMin=0.25 "Lower limit of output" annotation (Dialog(tab="S5", group="PID controller"));
      parameter Real S5_wp=1 "Set-point weight for Proportional block (0..1)"
        annotation (Dialog(tab="S5", group="PID controller"));
      parameter Real S5_wd=0 "Set-point weight for Derivative block (0..1)"
        annotation (Dialog(tab="S5", group="PID controller"));

      // SJ PUMPS PID parameters
        parameter Modelica.Blocks.Types.SimpleController Sj_controllerType[n]=fill(Modelica.Blocks.Types.SimpleController.PI, n)
        "Type of controller"
       annotation (Dialog(tab="Sj", group="PID controller (per room)"));
      parameter Real Sj_k[n]=fill(10, n) "Gain of controller"
       annotation (Dialog(tab="Sj", group="PID controller (per room)"));
      parameter Modelica.SIunits.Time Sj_Ti[n]=fill(300, n)
        "Time constant of Integrator block"
       annotation (Dialog(tab="Sj", group="PID controller (per room)"));
      parameter Modelica.SIunits.Time Sj_Td[n]=fill(0.1, n)
        "Time constant of Derivative block"
       annotation (Dialog(tab="Sj", group="PID controller (per room)"));
      parameter Real Sj_yMax[n]=fill(1, n) "Upper limit of speed"
       annotation (Dialog(tab="Sj", group="PID controller (per room)"));
      parameter Real Sj_yMin[n]=fill(0.11, n) "Lower limit of speed"
       annotation (Dialog(tab="Sj", group="PID controller (per room)"));
      parameter Boolean Sj_reverseAction[n]=fill(false, n)
        "Set to true for throttling the water flow rate through a cooling coil controller"
         annotation (Dialog(tab="Sj", group="PID controller (per room)"));

      // FANS PID parameters
      parameter Modelica.Blocks.Types.SimpleController fan_controllerType[n]=fill(.Modelica.Blocks.Types.SimpleController.PID, n)
        "Type of controller" annotation (Dialog(tab="Fans", group="PID controller (per room)"));
      parameter Real fan_k[n]=fill(10, n) "Gain of controller" annotation (Dialog(tab="Fans", group="PID controller (per room)"));
      parameter Modelica.SIunits.Time fan_Ti[n]=fill(60, n)
        "Time constant of Integrator block" annotation (Dialog(tab="Fans", group="PID controller (per room)"));
      parameter Modelica.SIunits.Time fan_Td[n]=fill(60, n)
        "Time constant of Derivative block" annotation (Dialog(tab="Fans", group="PID controller (per room)"));
      parameter Real fan_wp[n]=fill(1, n)
        "Set-point weight for Proportional block (0..1)"                                   annotation (Dialog(tab="Fans", group="PID controller (per room)"));
      parameter Real fan_wd[n]=fill(0, n)
        "Set-point weight for Derivative block (0..1)"                                   annotation (Dialog(tab="Fans", group="PID controller (per room)"));

      // BLOWING TEMPERATURES INSTRUCTION PID parameters
      parameter Modelica.Blocks.Types.SimpleController Tsouff_controllerType[n]=fill(.Modelica.Blocks.Types.SimpleController.PI, n)
        "Type of controller"
        annotation (Dialog(tab="Blowing Temperatures", group="PID controller (per room)"));
      parameter Real Tsouff_k[n]=fill(10, n) "Gain of controller"
      annotation (Dialog(tab="Blowing Temperatures", group="PID controller (per room)"));
      parameter Modelica.SIunits.Time Tsouff_Ti[n]=fill(400, n)
        "Time constant of Integrator block"
        annotation (Dialog(tab="Blowing Temperatures", group="PID controller (per room)"));
      parameter Modelica.SIunits.Time Tsouff_Td[n]=fill(0.1, n)
        "Time constant of Derivative block"
        annotation (Dialog(tab="Blowing Temperatures", group="PID controller (per room)"));
      parameter Real Tsouff_yMax[n]=fill(1, n) "Upper limit of output"
      annotation (Dialog(tab="Blowing Temperatures", group="PID controller (per room)"));
      parameter Real Tsouff_yMin[n]=fill(0, n) "Lower limit of output"
      annotation (Dialog(tab="Blowing Temperatures", group="PID controller (per room)"));
      parameter Boolean Tsouff_reverseAction[n]=fill(false, n)
        "Set to true for throttling the water flow rate through a cooling coil controller"
         annotation (Dialog(tab="Blowing Temperatures", group="PID controller (per room)"));

      // ELECTRICAL HEATER PID parameters
     // BLowing air control
     parameter Modelica.Blocks.Types.SimpleController elecHeat_controllerType[n]=fill(Modelica.Blocks.Types.SimpleController.PI, n)
        "Type of controller" annotation (Dialog(tab="Electric heaters", group="PID_air (per room)"));
      parameter Real elecHeat_k[n]=fill(2.5, n) "Gain of controller" annotation (Dialog(tab="Electric heaters", group="PID_air (per room)"));
      parameter Modelica.SIunits.Time elecHeat_Ti[n]=fill(200, n)
        "Time constant of Integrator block" annotation (Dialog(tab="Electric heaters", group="PID_air (per room)"));
      parameter Modelica.SIunits.Time elecHeat_Td[n]=fill(0.1, n)
        "Time constant of Derivative block" annotation (Dialog(tab="Electric heaters", group="PID_air (per room)"));
      parameter Real elecHeat_yMax[n]=fill(1, n) "Upper limit of output" annotation (Dialog(tab="Electric heaters", group="PID_air (per room)"));
      parameter Real elecHeat_yMin[n]=fill(0, n) "Lower limit of output" annotation (Dialog(tab="Electric heaters", group="PID_air (per room)"));
      parameter Boolean elecHeat_reverseAction[n]=fill(false, n)
        "Set to true for throttling the water flow rate through a cooling coil controller"
         annotation (Dialog(tab="Electric heaters", group="PID_air (per room)"));

      // DHW tank electrical control
      parameter Modelica.Blocks.Types.SimpleController elecDHW_controllerType=Modelica.Blocks.Types.SimpleController.P
        "Type of controller" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Real elecDHW_k=10 "Gain of controller" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Modelica.SIunits.Time elecDHW_Ti=0.1
        "Time constant of Integrator block" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Modelica.SIunits.Time elecDHW_Td=0.1
        "Time constant of Derivative block" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Real elecDHW_yMax=1 "Upper limit of output" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Real elecDHW_yMin=0 "Lower limit of output" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Boolean elecDHW_reverseAction=false
        "Set to true for throttling the water flow rate through a cooling coil controller"
         annotation (Dialog(tab="Electric heaters", group="PID_DHW"));

    end PID_parameters;

    record System_parameters "Group of parameters system temperature control"

      parameter Integer n=1
        "Number of room to control (must be greater than 0)";

    //
    // Schedules for temperature setpoint, solar setpoint and minimal volumic flow rate
    //
       parameter Boolean tableOnFile[n]=fill(false, n)
        "= true, if table is defined on file or in function usertab"
         annotation (Dialog(tab="Schedules", group="Tables data definition"));
         parameter Real table[:, :, :] = fill(fill(0.0, 0, 3), n)
        "Setpoint for the algorithm control (first=time). 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s])"
           annotation (Dialog(tab="Schedules",group="Tables data definition",enable=not tableOnFile));
       parameter String tableName[n]=fill("NoName", n)
        "Tables names on file or in function usertab (see docu)"
         annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
       parameter String fileName[n]=fill("NoName", n)
        "Files where matrix are stored"
         annotation (Dialog(tab="Schedules",
           group="Tables data definition",
           enable=tableOnFile,
           loadSelector(filter="Text files (*.txt);;MATLAB MAT-files (*.mat)",
               caption="Open file in which table is present")));
       parameter Boolean verboseRead[n]=fill(true, n)
        "= true, if info message that file is loading is to be printed"
         annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
        parameter Integer columns[:, :]=fill(2:3+1, n)
        "Columns of table to be interpolated"
          annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.Blocks.Types.Smoothness smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments
        "Smoothness of table interpolation"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.Blocks.Types.Extrapolation extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic
        "Extrapolation of data outside the definition range"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Real offset[:, :]=fill(fill(0, 3), n)
        "Offsets of output signals"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.SIunits.Time startTime[n]=fill(0, n)
        "Output = offset for time < startTime"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));

      annotation (Diagram(coordinateSystem(extent={{-1480,360},{-1340,460}})), Icon(coordinateSystem(
              extent={{-1480,360},{-1340,460}})));
    end System_parameters;

    record System_parameters_Monozone
      "Group of parameters system temperature control"

      import SI = Modelica.SIunits;

      extends PID_parameters_Monozone;

    //
    // Schedules for temperature setpoint, solar setpoint and minimal volumic flow rate
    //
       parameter Boolean tableOnFile=false
        "= true, if table is defined on file or in function usertab"
         annotation (Dialog(tab="Schedules", group="Tables data definition"));
         parameter Real table[:, :] = fill(0.0, 0, 3)
        "Setpoint for the algorithm control (first=time). 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s])"
           annotation (Dialog(tab="Schedules",group="Tables data definition",enable=not tableOnFile));
       parameter String tableName="NoName"
        "Tables names on file or in function usertab (see docu)"
         annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
       parameter String fileName="NoName"
        "Files where matrix are stored"
         annotation (Dialog(tab="Schedules",
           group="Tables data definition",
           enable=tableOnFile,
           loadSelector(filter="Text files (*.txt);;MATLAB MAT-files (*.mat)",
               caption="Open file in which table is present")));
       parameter Boolean verboseRead=true
        "= true, if info message that file is loading is to be printed"
         annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
        parameter Integer columns[:]=2:3+1
        "Columns of table to be interpolated"
          annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.Blocks.Types.Smoothness smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments
        "Smoothness of table interpolation"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.Blocks.Types.Extrapolation extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic
        "Extrapolation of data outside the definition range"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Real offset[:]=fill(0, 3)
        "Offsets of output signals"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.SIunits.Time startTime=0
        "Output = offset for time < startTime"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));


    //
    // Control solar bandwidth
    //
    parameter Real solar_delta_min=10 "Minimal temperature difference to activate direct solar energy (shared among all pumps)"
                                                                                                                               annotation (Dialog(group="Instructions"));


    //
    // Describe the control of the electrical extra heating for DHW
    //
      parameter SI.ThermodynamicTemperature DHW_setpoint = 328.15
        "Temperature setpoint for DHW" annotation (Dialog(group="Instructions"));
      parameter SI.Power DHW_elec_power = 3500
        "Maximum power of the extra electrical heater for DHW" annotation (Dialog(tab="Electric heaters"));

    //
    // Solar valve (V3V)
    //

      parameter SI.ThermodynamicTemperature temp_min_collector = 313.15
        "Minimum collector temperature in order to use direct solar energy for heating" annotation (Dialog(group="Lower limit"));

    //
    // Pump S5 (pump for DHW tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_DHW = 363.15
        "Maximum admissible value for DHW tank" annotation (Dialog(group="Higher limit"));

    //
    // Pump S6 (pump for Storage tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_storage = 378.15
        "Maximum admissible value for Storage tank" annotation (Dialog(group="Higher limit"));
      parameter SI.ThermodynamicTemperature temp_min_DHW = 303.15
        "Minimum admissible value for DHW tank before allowing Storage tank loading" annotation (Dialog(group="Lower limit"));
      parameter SI.ThermodynamicTemperature temp_min_storage = 318.15
        "Minimum storage tank temperature in order to use indirect solar energy" annotation (Dialog(group="Lower limit"));

      //
      //Control of blowing flow rate/temperature
      //
      parameter SI.ThermodynamicTemperature Tsouff_max= 305.15
        "Maximum value of blowing temperature for the room"
        annotation (Dialog(tab="Blowing Temperatures"));
      parameter Real fan_flowRate_max=900/3600
        "Upper limit of fan flow rate." annotation (Dialog(tab="Fans"));
      parameter SI.Time tempo_algo_flowRate= 600
        "How much time we have to wait before switching between Temperature or flow regulation"
        annotation (Dialog(group="Instructions"));

      //
      //Control of blowing electric heater
      //
        parameter Modelica.SIunits.Power heater_elec_power=3000
        "Maximum power of the extra electrical heater for air blowing heating"
        annotation (Dialog(tab="Electric heaters"));
        parameter SI.Time tempo_algo_elec= 180
        "How much time we have to wait before using electric heater"
        annotation (Dialog(group="Instructions"));

    //
    // Max Speed for S2, S6 and S5
    //
      parameter SI.Conversions.NonSIunits.AngularVelocity_rpm S6_speed_max=3000
        "Maximum value for S6 speed"
        annotation (Dialog(tab="S6"));
      parameter SI.Conversions.NonSIunits.AngularVelocity_rpm S5_speed_max=3000
        "Maximum value for S5 speed"
        annotation (Dialog(tab="S5"));
      parameter SI.Conversions.NonSIunits.AngularVelocity_rpm S2_speed_max= 3000
        "Maximum value for S2 speed"
        annotation (Dialog(tab="S2"));



      annotation (Diagram(coordinateSystem(extent={{-1480,360},{-1340,460}})), Icon(coordinateSystem(
              extent={{-1480,360},{-1340,460}})));
    end System_parameters_Monozone;

    record System_parameters_Monozone_2
      "Group of parameters system temperature control"

      import SI = Modelica.SIunits;

      extends PID_parameters_Monozone;

    //
    // Schedules for temperature setpoint, solar setpoint and minimal volumic flow rate
    //
       parameter Boolean tableOnFile=false
        "= true, if table is defined on file or in function usertab"
         annotation (Dialog(tab="Schedules", group="Tables data definition"));
         parameter Real table[:, :] = fill(0.0, 0, 3)
        "Setpoint for the algorithm control (first=time). 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s])"
           annotation (Dialog(tab="Schedules",group="Tables data definition",enable=not tableOnFile));
       parameter String tableName="NoName"
        "Tables names on file or in function usertab (see docu)"
         annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
       parameter String fileName="NoName"
        "Files where matrix are stored"
         annotation (Dialog(tab="Schedules",
           group="Tables data definition",
           enable=tableOnFile,
           loadSelector(filter="Text files (*.txt);;MATLAB MAT-files (*.mat)",
               caption="Open file in which table is present")));
       parameter Boolean verboseRead=true
        "= true, if info message that file is loading is to be printed"
         annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
        parameter Integer columns[:]=2:3+1
        "Columns of table to be interpolated"
          annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.Blocks.Types.Smoothness smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments
        "Smoothness of table interpolation"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.Blocks.Types.Extrapolation extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic
        "Extrapolation of data outside the definition range"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Real offset[:]=fill(0, 3)
        "Offsets of output signals"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.SIunits.Time startTime=0
        "Output = offset for time < startTime"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));

    //
    // Control solar bandwidth
    //
    parameter Real solar_delta_min=10 "Minimal temperature difference to activate direct solar energy (shared among all pumps)"
                                                                                                                               annotation (Dialog(group="Instructions"));

    //
    // Describe the control of the electrical extra heating for DHW
    //
      parameter SI.ThermodynamicTemperature DHW_setpoint = 328.15
        "Temperature setpoint for DHW" annotation (Dialog(group="Instructions"));
      parameter SI.Power DHW_elec_power = 3500
        "Maximum power of the extra electrical heater for DHW" annotation (Dialog(tab="Electric heaters"));

    //
    // Solar valve (V3V)
    //

      parameter Real DeltaMinCol = 2
        "Minimum difference temperature between col and exchanger inlet in order to use direct solar energy for heating" annotation (Dialog(group="Lower limit"));

    //
    // Pump S5 (pump for DHW tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_DHW = 363.15
        "Maximum admissible value for DHW tank" annotation (Dialog(group="Higher limit"));

    //
    // Pump S6 (pump for Storage tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_storage = 378.15
        "Maximum admissible value for Storage tank" annotation (Dialog(group="Higher limit"));
      parameter SI.ThermodynamicTemperature temp_min_DHW = 303.15
        "Minimum admissible value for DHW tank before allowing Storage tank loading" annotation (Dialog(group="Lower limit"));
      parameter Real DeltaMinBuffer = 2
        "Minimum temperature difference between buffer and exchanger inlet in order to use indirect solar energy" annotation (Dialog(group="Lower limit"));

      //
      //Control of blowing flow rate/temperature
      //
      parameter SI.ThermodynamicTemperature Tsouff_max= 305.15
        "Maximum value of blowing temperature for the room"
        annotation (Dialog(tab="Blowing Temperatures"));
      parameter Real fan_flowRate_max=400/3600
        "Upper limit of fan flow rate." annotation (Dialog(tab="Fans"));
      parameter SI.Time tempo_algo_flowRate= 600
        "How much time we have to wait before switching between Temperature or flow regulation"
        annotation (Dialog(group="Instructions"));

      //
      //Control of blowing electric heater
      //
        parameter Modelica.SIunits.Power heater_elec_power=3000
        "Maximum power of the extra electrical heater for air blowing heating"
        annotation (Dialog(tab="Electric heaters"));
        parameter SI.Time tempo_algo_elec= 180
        "How much time we have to wait before using electric heater"
        annotation (Dialog(group="Instructions"));

    //
    // Max Speed for S2, S6 and S5
    //
      parameter SI.Conversions.NonSIunits.AngularVelocity_rpm S6_speed_max=3000
        "Maximum value for S6 speed"
        annotation (Dialog(tab="S6"));
      parameter SI.Conversions.NonSIunits.AngularVelocity_rpm S5_speed_max=3000
        "Maximum value for S5 speed"
        annotation (Dialog(tab="S5"));
      parameter SI.Conversions.NonSIunits.AngularVelocity_rpm S2_speed_max= 3000
        "Maximum value for S2 speed"
        annotation (Dialog(tab="S2"));

      annotation (Diagram(coordinateSystem(extent={{-1480,360},{-1340,460}})), Icon(coordinateSystem(
              extent={{-1480,360},{-1340,460}})));
    end System_parameters_Monozone_2;

    record PID_parameters_Monozone
      "Record for all PID inside the system to control"

      import SI = Modelica.SIunits;


    // S6 PID parameters
          parameter Modelica.Blocks.Types.SimpleController S6_controller=Modelica.Blocks.Types.SimpleController.PI
        "Type of controller" annotation (Dialog(tab="S6", group="PID controller"));
      parameter Real S6_k=2.5 "Gain of controller" annotation (Dialog(tab="S6", group="PID controller"));
      parameter SI.Time S6_Ti=175
        "Time constant of Integrator block"
        annotation (Dialog(tab="S6", group="PID controller"));
      parameter SI.Time S6_Td=0.1
        "Time constant of Derivative block"
        annotation (Dialog(tab="S6", group="PID controller"));
         parameter Real S6_yMax=1 "Upper limit of output" annotation (Dialog(tab="S6", group="PID controller"));
      parameter Real S6_yMin=0.25 "Lower limit of output" annotation (Dialog(tab="S6", group="PID controller"));


    // S5 PID parameters
      parameter Modelica.Blocks.Types.SimpleController S5_controller=Modelica.Blocks.Types.SimpleController.PID
        "Type of controller" annotation (Dialog(tab="S5", group="PID controller"));
      parameter Real S5_k=5 "Gain of controller" annotation (Dialog(tab="S5", group="PID controller"));
      parameter SI.Time S5_Ti=200
        "Time constant of Integrator block"
        annotation (Dialog(tab="S5", group="PID controller"));
      parameter SI.Time S5_Td=20
        "Time constant of Derivative block"
        annotation (Dialog(tab="S5", group="PID controller"));
      parameter Real S5_yMax=1 "Upper limit of output" annotation (Dialog(tab="S5", group="PID controller"));
      parameter Real S5_yMin=0.25 "Lower limit of output" annotation (Dialog(tab="S5", group="PID controller"));


    // S2 PUMPS PID parameters
        parameter Modelica.Blocks.Types.SimpleController S2_controllerType=Modelica.Blocks.Types.SimpleController.PI
        "Type of controller"
       annotation (Dialog(tab="S2", group="PID controller"));
      parameter Real S2_k=10 "Gain of controller"
       annotation (Dialog(tab="S2", group="PID controller"));
      parameter SI.Time S2_Ti=300
        "Time constant of Integrator block"
       annotation (Dialog(tab="S2", group="PID controller"));
      parameter SI.Time S2_Td=0.1
        "Time constant of Derivative block"
       annotation (Dialog(tab="S2", group="PID controller"));
      parameter Real S2_yMax=1 "Upper limit of speed"
       annotation (Dialog(tab="S2", group="PID controller"));
      parameter Real S2_yMin=0.11 "Lower limit of speed"
       annotation (Dialog(tab="S2", group="PID controller"));


    // DHW tank electrical control
      parameter Modelica.Blocks.Types.SimpleController elecDHW_controllerType=Modelica.Blocks.Types.SimpleController.P
        "Type of controller" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Real elecDHW_k=10 "Gain of controller" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Modelica.SIunits.Time elecDHW_Ti=0.1
        "Time constant of Integrator block" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Modelica.SIunits.Time elecDHW_Td=0.1
        "Time constant of Derivative block" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));


    // FANS PID parameters
      parameter Modelica.Blocks.Types.SimpleController fan_controllerType=Modelica.Blocks.Types.SimpleController.PID
        "Type of controller" annotation (Dialog(tab="Fans", group="PID controller"));
      parameter Real fan_k=10 "Gain of controller" annotation (Dialog(tab="Fans", group="PID controller"));
      parameter Modelica.SIunits.Time fan_Ti=60
        "Time constant of Integrator block" annotation (Dialog(tab="Fans", group="PID controller"));
      parameter Modelica.SIunits.Time fan_Td=60
        "Time constant of Derivative block" annotation (Dialog(tab="Fans", group="PID controller"));


    // BLOWING TEMPERATURES INSTRUCTION PID parameters
      parameter Modelica.Blocks.Types.SimpleController Tsouff_controllerType=Modelica.Blocks.Types.SimpleController.PI
        "Type of controller"
        annotation (Dialog(tab="Blowing Temperatures", group="PID controller"));
      parameter Real Tsouff_k=10 "Gain of controller"
      annotation (Dialog(tab="Blowing Temperatures", group="PID controller"));
      parameter Modelica.SIunits.Time Tsouff_Ti=400
        "Time constant of Integrator block"
        annotation (Dialog(tab="Blowing Temperatures", group="PID controller"));
      parameter Modelica.SIunits.Time Tsouff_Td=0.1
        "Time constant of Derivative block"
        annotation (Dialog(tab="Blowing Temperatures", group="PID controller"));


    // ELECTRICAL HEATER PID parameters
       parameter Modelica.Blocks.Types.SimpleController elecHeat_controllerType=Modelica.Blocks.Types.SimpleController.PI
        "Type of controller" annotation (Dialog(tab="Electric heaters", group="PID_air"));
        parameter Real elecHeat_k=2.5 "Gain of controller" annotation (Dialog(tab="Electric heaters", group="PID_air"));
        parameter Modelica.SIunits.Time elecHeat_Ti=200
        "Time constant of Integrator block" annotation (Dialog(tab="Electric heaters", group="PID_air"));
        parameter Modelica.SIunits.Time elecHeat_Td=0.1
        "Time constant of Derivative block" annotation (Dialog(tab="Electric heaters", group="PID_air"));


    end PID_parameters_Monozone;
  end Data;

  package Backup "Older version in case of too fancy code ..."
    model Equipements_control_20151210
      "Describe the control algorithm for pumps S6 and S5; V3V cot� solaire; Electrical power pull inside the DHW tank."
      import SI = Modelica.SIunits;

      extends Data.PID_parameters;

    //
    // General inputs and outputs
    //
      Modelica.Blocks.Interfaces.RealInput T1 "Temperature inside collector"
        annotation (Placement(transformation(extent={{-20,-20},{20,20}},
            origin={-1488,410}),
            iconTransformation(extent={{-18,-18},{18,18}},
            origin={-1478,440})));
      Modelica.Blocks.Interfaces.RealInput T3
        "Temperature inside solar tank (top)"
        annotation (Placement(transformation(extent={{-1508,360},{-1468,400}}), iconTransformation(
              extent={{-1496,382},{-1460,418}})));
      Modelica.Blocks.Interfaces.RealInput T4
        "Temperature inside DHW tank next to drawing up (top)"
        annotation (Placement(transformation(extent={{-1508,332},{-1468,372}}), iconTransformation(
              extent={{-1496,302},{-1460,338}})));
      Modelica.Blocks.Interfaces.RealInput T5
        "Temperature inside DHW tank next to solar exchanger (bottom)"
         annotation (Placement(transformation(extent={{-1508,304},{-1468,344}}), iconTransformation(
              extent={{-1496,342},{-1460,378}})));
      Modelica.Blocks.Interfaces.RealInput T7
        "Temperature of water after terminal emitter (only one for all rooms)"
        annotation (Placement(transformation(extent={{-1506,264},{-1466,304}}), iconTransformation(
              extent={{-1496,262},{-1460,298}})));
      Modelica.Blocks.Interfaces.RealVectorInput Tamb[n]
        "Vector of temperature inside rooms areas"
        annotation (Placement(transformation(extent={{-1500,228},{-1460,268}}), iconTransformation(
              extent={{-1496,186},{-1468,214}})));
      Modelica.Blocks.Interfaces.RealVectorInput Texch_out[n]
        "Vector of air temperature just after exchanger"      annotation (Placement(
            transformation(extent={{-1500,150},{-1460,190}}),
                                                        iconTransformation(extent={{-1496,152},{-1468,
                180}})));
      Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n]
        "Vector of air temperature blowing in each room"   annotation (Placement(
            transformation(extent={{-1500,190},{-1460,230}}),
                                                        iconTransformation(extent={{-1496,120},{-1468,
                148}})));
      Modelica.Blocks.Interfaces.RealInput Text
        "Temperature outside the room (environnement)"
                                                annotation (Placement(
            transformation(
            extent={{-20,-20},{20,20}},
            rotation=0,
            origin={-1486,132}),
                              iconTransformation(extent={{-18,-18},{18,18}},
            rotation=0,
            origin={-1478,240})));
      Modelica.Blocks.Interfaces.BooleanOutput Sj_state_final[n]
        "Final Sj pumps states after temporization check" annotation (Placement(transformation(
              extent={{-1206,210},{-1178,238}}), iconTransformation(extent={{-1196,414},{-1156,454}})));

    //
    // Describe the control of the solar valve (V3V)
    //
      Modelica.Blocks.Logical.OnOffController need_solar_DHW(pre_y_start=true, bandwidth=2)
        "Return True if solar must not be used and False if solar must be used"
        annotation (Placement(transformation(extent={{-1396,410},{-1384,422}})));

      Modelica.Blocks.Logical.OnOffController need_solar_Storage(pre_y_start=true,
          bandwidth=2)
        "Return True if solar must be used to load the storage tank"
        annotation (Placement(transformation(extent={{-1396,392},{-1384,404}})));
      Modelica.Blocks.Logical.Hysteresis enough_power_in_DHW(uLow=temp_min_DHW - 2, uHigh=
            temp_min_DHW)
        "Test if we have enough power inside the DHW before authorize temperature inside the storage tank"
        annotation (Placement(transformation(extent={{-1396,374},{-1384,386}})));
       Modelica.Blocks.Interfaces.BooleanOutput V3V_solar
        "State of the solar valve"
         annotation (Placement(transformation(extent={{-1344,396},{-1324,416}})));

    //
    // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_DHW = 363.15
        "Maximum admissible value for DHW tank" annotation (Dialog(group="Higher limit"));
      Boolean S5_state_temp
        "Temporary state of the S5 pump, see Sj control part to get final state";
      Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T3(
        y(start=false),
        uLow=4,
        uHigh=10) "Check if T1 - T3 is larger enough"
        annotation (Placement(transformation(extent={{-1278,404},{-1266,416}})));
      Modelica.Blocks.Logical.OnOffController max_DHW_tank(bandwidth=5, pre_y_start=true)
        "Test if the temperature inside the tank is lower than reference"
        annotation (Placement(transformation(extent={{-1278,384},{-1266,396}})));

    //
    // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_storage = 378.15
        "Maximum admissible value for Storage tank" annotation (Dialog(group="Higher limit"));
      parameter SI.ThermodynamicTemperature temp_min_DHW = 303.15
        "Minimum admissible value for DHW tank before allowing Storage tank loading" annotation (Dialog(group="Lower limit"));
      Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T5(
        y(start=false),
        uHigh=10,
        uLow=2.5) "Check if T1 - T5 is larger enough"
        annotation (Placement(transformation(extent={{-1278,338},{-1268,348}})));
      Modelica.Blocks.Logical.OnOffController max_Storage_tank(bandwidth=5)
        "Test if the temperature inside the tank is lower than reference"
        annotation (Placement(transformation(extent={{-1278,322},{-1268,332}})));
       Modelica.Blocks.Interfaces.BooleanOutput S6_state
        "State of the solar pump S6."
        annotation (Placement(transformation(extent={{-1224,334},{-1204,354}})));

    //
    // Describe the control of the electrical extra heating for DHW
    //
      parameter SI.Power DHW_elec_power = 6000
        "Maximum power of the extra electrical heater for DHW";
      parameter SI.ThermodynamicTemperature DHW_setpoint = 313.15
        "Temperature setpoint for DHW" annotation (Dialog(group="Instructions"));
      Buildings.Controls.Continuous.LimPID pip_DHW_elec(
        controllerType=elecDHW_controllerType,
        k=elecDHW_k,
        Ti=elecDHW_Ti,
        Td=elecDHW_Td,
        yMax=elecDHW_yMax,
        yMin=elecDHW_yMin,
        reverseAction=elecDHW_reverseAction)
        "Controller for electrical heat to DHW"
        annotation (Placement(transformation(extent={{-1414,286},{-1404,296}})));
      Modelica.Blocks.Continuous.Integrator integrator_DHW_elec
        "Integration of the power used to keep domestic hot water at the setpoint temperature"
        annotation (Placement(transformation(extent={{-1392,284},{-1380,296}})));
      Modelica.Blocks.Interfaces.RealOutput power_DHW_elec
        "Output the power needed to keep water temperature higher than setpoint"
        annotation (Placement(transformation(extent={{-1344,290},{-1324,310}})));
      Modelica.Blocks.Interfaces.RealOutput energy_DHW_elec
        "Output the cumulated energy needed to keep water temperature higher than setpoint"
        annotation (Placement(transformation(extent={{-1344,270},{-1324,290}})));

    //
    // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
    //
      parameter SI.ThermodynamicTemperature temp_min_collector = 313.15
        "Minimum collector temperature in order to use direct solar energy" annotation (Dialog(group="Lower limit"));
      parameter SI.ThermodynamicTemperature temp_min_storage = 318.15
        "Minimum storage tank temperature in order to use indirect solar energy" annotation (Dialog(group="Lower limit"));
      Boolean direct_solar_state[n]
        "Vector of Sn pump state on direct solar energy (one for each room)";
      Boolean indirect_solar_state[n]
        "Vector of Sn pump state on indirect solar energy (one for each room)";
      Modelica.Blocks.Logical.OnOffController need_overheat[n](each bandwidth=1)
        annotation (Placement(transformation(extent={{-1410,190},{-1390,210}})));
      Modelica.Blocks.Logical.OnOffController need_heat[n](each bandwidth=0.5)
        annotation (Placement(transformation(extent={{-1410,220},{-1390,240}})));
      Modelica.Blocks.Logical.OnOffController enough_energy_collector(bandwidth=5)
        "We can use direct solar energy"
        annotation (Placement(transformation(extent={{-1376,220},{-1356,240}})));
      Modelica.Blocks.Logical.Hysteresis enough_energy_storage(uLow=
            temp_min_storage - 5, uHigh=temp_min_storage)
        annotation (Placement(transformation(extent={{-1378,190},{-1358,210}})));
      Utilities.Timers.Stay_on Sj_state_timer[n](each pause=60, out_value(each
            start=false))
        "Return the current state of Sj pump (vector of state)"
        annotation (Placement(transformation(extent={{-1348,222},{-1332,238}})));
      Utilities.Timers.Stay_on S5_state_timer(each pause=60, out_value(each start=false))
        "Return the current state of S5 pump."
        annotation (Placement(transformation(extent={{-1348,192},{-1332,208}})));
      Modelica.Blocks.Interfaces.BooleanOutput Sj_state[n]
        "Control Sj pump behavior" annotation (Placement(transformation(extent={{-1324,214},{-1292,
                246}}),             iconTransformation(extent={{-1196,414},{-1156,454}})));
      Modelica.Blocks.Interfaces.BooleanOutput S5_state "Control S5 state"
        annotation (Placement(transformation(extent={{-1324,184},{-1292,216}}), iconTransformation(
              extent={{-1194,368},{-1154,408}})));

    //
    // Describe how to compute temporization and flow modulation for Sj, S6, S5
    //
         Utilities.Timers.Wait_for_tempo_crack temporization_Sj(n=n) if n > 1
        "Used to add a temporization to Sj pumps. Avoid all pump to start at full speed at the same time and consuming all energy"
        annotation (Placement(transformation(extent={{-1266,216},{-1222,240}})));
      Utilities.Timers.Wait_for system_pump_mod[2]
        "Modulation of pumps for the system (S6 and S5 currently)"
        annotation (Placement(transformation(extent={{-1266,188},{-1222,212}})));

    //
    // Describe pid pump output construction for S6 and S5
    //
      // S6 PID parameters
      parameter Real S6_delta_min=10
        "Minimal temperature difference between collector and storage tank"
        annotation (Dialog(tab="S6"));
      parameter Real S6_speed_max=3000 "Maximum value for S6 speed"
        annotation (Dialog(tab="S6"));

      // S5 PID parameters
      parameter Real S5_delta_min=10
        "Minimal temperature difference between collector and DHW tank"
        annotation (Dialog(tab="S5"));
      parameter Real S5_speed_max=3000 "Maximum value for S5 speed"
        annotation (Dialog(tab="S5"));
      Buildings.Controls.Continuous.LimPID pid_S6(
        controllerType=S6_controller,
        k=S6_k,
        Ti=S6_Ti,
        Td=S6_Td,
        wp=S6_wp,
        wd=S6_wd,
        yMax=S6_yMax,
        yMin=S6_yMin,
        reverseAction=true)
        annotation (Placement(transformation(extent={{-1078,220},{-1058,240}})));
      Buildings.Controls.Continuous.LimPID pid_S5(
        controllerType=S5_controller,
        k=S5_k,
        Ti=S5_Ti,
        Td=S5_Td,
        wp=S5_wp,
        wd=S5_wd,
        yMax=S5_yMax,
        yMin=S5_yMin,
        reverseAction=true)
        annotation (Placement(transformation(extent={{-1108,194},{-1088,214}})));

      Modelica.Blocks.Interfaces.RealOutput S5_speed "Final speed of S5 pump" annotation (
          Placement(transformation(extent={{-1046,220},{-1018,248}}), iconTransformation(extent={{-1196,
                414},{-1156,454}})));
      Modelica.Blocks.Interfaces.RealOutput S6_speed "Final speed of S6 pump" annotation (
          Placement(transformation(extent={{-1046,188},{-1018,216}}), iconTransformation(extent={{-1196,
                414},{-1156,454}})));

    //
    //Control of blowing flow rate/temperature
    //
      parameter SI.ThermodynamicTemperature Tsouff_max[n]= fill(273.15 + 32, n)
        "Maximum value of blowing temperature for each room"
        annotation (Dialog(tab="Blowing Temperatures"));
      parameter SI.Time tempo_algo_flowRate[n]= fill(600, n)
        "How much time we have to wait before switching between Temperature or flow regulation"
        annotation (Dialog(group="Instructions"));
      parameter Real fan_flowRate_max[n]=fill(900/3600, n)
        "Upper limit of fan flow rate." annotation (Dialog(tab="Fans"));
      Boolean need_limit_flowRate[n]
        "Boolean used to check if we need or not to reduce the blowing flow rate";
      Real temperature_instruction_souff[n]
        "Computed temperature instruction [K]";
      Modelica.Blocks.Interfaces.RealOutput minimal_flowRate[n]
        "Minimal sanitary volumic flow rate [m3/s]"                        annotation (Placement(
            transformation(extent={{-1034,410},{-1014,430}}), iconTransformation(extent={{-1196,414},
                {-1156,454}})));
      Modelica.Blocks.Interfaces.RealOutput fan_flowRate[n]
        "Computed fan flow rate [m3/s]"                                              annotation (
          Placement(transformation(extent={{-1034,390},{-1014,410}}), iconTransformation(extent={{
                -1196,414},{-1156,454}})));
      Modelica.Blocks.Logical.Hysteresis need_higher_flow[n](uLow=Tsouff_max - fill(1.1, size(
            Tsouff_max, 1)), uHigh=Tsouff_max - fill(0.1, size(Tsouff_max, 1)))
        "Test if we need a higher flow rate to cover the heating demand"
        annotation (Placement(transformation(extent={{-1114,416},{-1106,424}})));
      Utilities.Timers.Wait_for need_increase_flowRate[n](wait_for=tempo_algo_flowRate)
        "Time to wait before increasing blowing flow rate"
        annotation (Placement(transformation(extent={{-1096,416},{-1064,424}})));
      Utilities.Timers.Wait_for need_decrease_flowRate[n](wait_for=tempo_algo_flowRate)
        "Time to wait before decreasing blowing flow rate to minimal flow rate"
        annotation (Placement(transformation(extent={{-1096,396},{-1064,404}})));

      Modelica.Blocks.Logical.OnOffController flow_is_minimal[n](each bandwidth=10/3600)
        annotation (Placement(transformation(extent={{-1114,396},{-1106,404}})));
      Buildings.Controls.Continuous.LimPID pid_fan[n](
        controllerType=fan_controllerType,
        k=fan_k,
        Ti=fan_Ti,
        Td=fan_Td,
        wp=fan_wp,
        wd=fan_wd,
        each yMax=1,
        each yMin=0) "PID used to control blowing flow rate for each room"
                    annotation (Placement(transformation(extent={{-1052,396},{-1044,404}})));
       Buildings.Controls.Continuous.LimPID pid_Tsouff[n](
        controllerType=Tsouff_controllerType,
        k=Tsouff_k,
        Ti=Tsouff_Ti,
        Td=Tsouff_Td,
        yMax=Tsouff_yMax,
        yMin=Tsouff_yMin,
        reverseAction=Tsouff_reverseAction)
        "PID which control the blowing temperature instruction"
        annotation (Placement(transformation(extent={{-1052,416},{-1044,424}})));

    //
    //Control of Sj pumps speed
    //
      parameter SI.Time tempo_algo_elec[n]= fill(180, n)
        "How much time we have to wait before using electric heater"
        annotation (Dialog(group="Instructions"));
        parameter Real Sj_speed_max[n]= fill(3000, n)
        "Maximum value for Sj speed of each pump"
        annotation (Dialog(tab="Sj"));
       Buildings.Controls.Continuous.LimPID pid_Sj[n](
        controllerType=Sj_controllerType,
        k=Sj_k,
        Ti=Sj_Ti,
        Td=Sj_Td,
        yMax=Sj_yMax,
        yMin=Sj_yMin,
        reverseAction=Sj_reverseAction)
        "Compute correct factor in order to modulate Sj speed"
        annotation (Placement(transformation(extent={{-1116,320},{-1100,336}})));

      Utilities.Timers.Wait_for need_electric_heater[n](wait_for=tempo_algo_elec)
        "Check if we still need electric heater after `wait_for` time."
        annotation (Placement(transformation(extent={{-1086,298},{-1056,308}})));
      Modelica.Blocks.Logical.OnOffController need_elec_to_air[n](each bandwidth=2)
        "Check if solar energy gives enough power to the blowing air source"
        annotation (Placement(transformation(extent={{-1104,298},{-1094,308}})));
      Modelica.Blocks.Interfaces.RealOutput Sj_speed[n]
        "Output of each pump Sj speed"
        annotation (Placement(transformation(extent={{-1036,316},{-1016,336}}),
            iconTransformation(extent={{-1196,414},{-1156,454}})));

    //
    //Control of blowing electric heater
    //
      parameter Modelica.SIunits.Power heater_elec_power[n]=fill(5000, n)
        "Maximum power of the extra electrical heater for air blowing heating";
        Buildings.Controls.Continuous.LimPID pid_heater_elec[n](
          controllerType=elecHeat_controllerType,
          k=elecHeat_k,
          Ti=elecHeat_Ti,
          Td=elecHeat_Td,
          yMax=elecHeat_yMax,
          yMin=elecHeat_yMin)
        "Controller for electric heater on blowing air for each room"
          annotation (Placement(transformation(extent={{-988,318},{-972,334}})));

      Modelica.Blocks.Continuous.Integrator integrator_heating_elec[n]
        "Integration of the power used to keep room air at the setpoint temperature"
        annotation (Placement(transformation(extent={{-954,304},{-938,320}})));
      Modelica.Blocks.Interfaces.RealOutput power_air_elec[n]
        "Output the power needed to keep room air temperature at setpoint"
        annotation (Placement(transformation(extent={{-916,318},{-896,338}})));
      Modelica.Blocks.Interfaces.RealOutput energy_storage_elec[n]
        "Output the cumulated energy needed to keep room air temperature at setpoint"
        annotation (Placement(transformation(extent={{-916,298},{-896,318}})));
       Modelica.Blocks.Sources.CombiTimeTable             Schedules[n](
        tableOnFile=tableOnFile,
        table=table,
        tableName=tableName,
        fileName=fileName,
        verboseRead=verboseRead,
        columns=columns,
        each smoothness=smoothness,
        each extrapolation=extrapolation,
        offset=offset,
        startTime=startTime)
        "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
         annotation (Placement(transformation(extent={{-1480,428},{-1448,460}})));

    //
    // Schedules for temperature setpoint, solar setpoint and minimal volumic flow rate
    //
       parameter Boolean tableOnFile[n]=fill(false, n)
        "= true, if table is defined on file or in function usertab"
         annotation (Dialog(tab="Schedules", group="Tables data definition"));
         parameter Real table[:, :, :] = fill(fill(0.0, 0, 3), n)
        "Setpoint for the algorithm control (first=time). 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s])"
           annotation (Dialog(tab="Schedules",group="Tables data definition",enable=not tableOnFile));
       parameter String tableName[n]=fill("NoName", n)
        "Tables names on file or in function usertab (see docu)"
         annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
       parameter String fileName[n]=fill("NoName", n)
        "Files where matrix are stored"
         annotation (Dialog(tab="Schedules",
           group="Tables data definition",
           enable=tableOnFile,
           loadSelector(filter="Text files (*.txt);;MATLAB MAT-files (*.mat)",
               caption="Open file in which table is present")));
       parameter Boolean verboseRead[n]=fill(true, n)
        "= true, if info message that file is loading is to be printed"
         annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
        parameter Integer columns[:, :]=fill(2:3+1, n)
        "Columns of table to be interpolated"
          annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.Blocks.Types.Smoothness smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments
        "Smoothness of table interpolation"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.Blocks.Types.Extrapolation extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic
        "Extrapolation of data outside the definition range"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Real offset[:, :]=fill(fill(0, 3), n)
        "Offsets of output signals"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.SIunits.Time startTime[n]=fill(0, n)
        "Output = offset for time < startTime"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));

    equation
    //
    // Describe the control of the solar valve (V3V)
    //
      //Connect minimal between T4 and T3 for test with T1 (`+1` used to shift bandwidth upper)
      need_solar_DHW.u = min(T4, T3)+1 "[T1 >= min(T3, T4)]";
      need_solar_Storage.u = T5+1 "[T1 >= T5";
      // V3V open to solar collector only if we have
      V3V_solar = (enough_power_in_DHW.y and need_solar_Storage.y) or need_solar_DHW.y
        "[T1 >= T5 and T3 >= 32�C] or [T1 >= min(T3, T4)]";

    //
    // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
    //
      enoughDelta_T1_T3.u = T1 - T3 "[T1 - T3 >= 10]";
      max_DHW_tank.reference = temp_max_DHW "[T3 > temp_max_DHW]";
      // Compute S5 pump state according to temperature difference
      S5_state_temp = V3V_solar and max_DHW_tank.y and enoughDelta_T1_T3.y
        "[T1 - T3 >= 10 and [T1 > temp_max_DHW] and V3V_solar]";

    //
    // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
    //
      enoughDelta_T1_T5.u = T1 - T5 "[T1 - T5 >= 10]";
      max_Storage_tank.reference = temp_max_storage-2.5 "[T5 > temp_max_DHW]";
      // Compute S6 pump state
      S6_state = enoughDelta_T1_T5.y and enough_power_in_DHW.y and max_Storage_tank.y
        "[T1 - T5 >= 10 and T5 > temp_max_DHW]";

    //
    // Describe the control of the electrical extra heating for DHW
    //
      pip_DHW_elec.u_s = DHW_setpoint "Set DHW temperature setpoint for PID";
      power_DHW_elec = DHW_elec_power * pip_DHW_elec.y
        "Power compute using PID";
      integrator_DHW_elec.u = power_DHW_elec
        "Same thing, only here to be used outside the model";

    //
    // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
    //
    // Test temperature difference between T1 and max(T7 or minimal value to use direct solar)
      enough_energy_collector.u = max(temp_min_collector, T7);
      // Connect T1 to solar_direct bloc OnOffController
      enough_energy_collector.reference = T1 - (enough_energy_collector.bandwidth / 2)
        "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";
      // S5 pump state according to heating demande control
      S5_state_timer.in_value = Modelica.Math.BooleanVectors.anyTrue(direct_solar_state) or S5_state_temp
        "State of S5 pump according to heating demands";

      // Loop only once over each room
      for j in 1:n loop
        // Overheat connections
        need_overheat[j].reference = Schedules[j].y[2];
        need_overheat[j].u = Tamb[j] - (need_overheat[j].bandwidth / 2)
          "bandwidth used to shift down minimal value";

        // Heat connection
        need_heat[j].reference = Schedules[j].y[1];
        need_heat[j].u = Tamb[j] - (need_heat[j].bandwidth / 2)
          "bandwidth used to shift down minimal value";

        // We need to activate the Sj pump using direct solar energy from collectors
        direct_solar_state[j] = V3V_solar and enough_energy_collector.y and (need_heat[j].y or need_overheat[j].y);

        // We need to activate the Sj pump using indirect solar energy from tank
        indirect_solar_state[j] = (not V3V_solar) and enough_energy_storage.y and need_heat[j].y;

        // Final state of the pump for the room j
        Sj_state_timer[j].in_value = indirect_solar_state[j] or direct_solar_state[j];
      end for;

    //
    // Describe how to compute temporization and flow modulation for Sj, S6, S5
    //
      // Connect pump S6 and S5 to modulation process
      system_pump_mod[1].in_value = S6_state;
      system_pump_mod[2].in_value = S5_state;
      // Connection of Sj pumps to temporization if at least 2 pumps else directly to Sj_final_state
      if n > 1 then
        connect(Sj_state, temporization_Sj.in_value);
        connect(temporization_Sj.out_value, Sj_state_final);
      else
        Sj_state_final = Sj_state;
      end if;

    //
    // Describe pid pump output construction for S6 and S5
    //
      // Connection to S6 pid
      pid_S6.u_s = S6_delta_min;
      pid_S6.u_m = T1 - T5;
       // Only need to compute output if pump S5 is On
       if S5_state then
         // Check if S5 pump must be modulated to find multiplicator
         S5_speed = S5_speed_max * (if system_pump_mod[2].out_value then pid_S5.y else 0.5);
       else
         S5_speed = 0;
       end if;
       // Connection to S5 pid
       pid_S5.u_s = S5_delta_min;
       pid_S5.u_m = T1 - T3;
       // Only need to compute output if pump S5 is On
       if S6_state then
         // Check if S5 pump must be modulated to find multiplicator
         S6_speed = S6_speed_max * (if system_pump_mod[1].out_value then pid_S6.y else 0.5);
       else
         S6_speed = 0;
       end if;

    //
    //Control of blowing flow rate/temperature
    //
      // Loop only once over each room
      for j in 1:n loop
        //Minimal flow rate reference
        flow_is_minimal[j].reference = Schedules[j].y[3] * 1.5;
        flow_is_minimal[j].u = fan_flowRate[j]
          "RECURSIVE call to computed volumic flow rate";
        need_higher_flow[j].u = temperature_instruction_souff[j]
          "RECURSIVE call to computed temperature instruction";
        // Check if we need to increase blowing flow rate
        need_increase_flowRate[j].in_value = need_heat[j].y and need_higher_flow[j].y;
        // Check if we need to decrease flow rate
        need_decrease_flowRate[j].in_value = flow_is_minimal[j].y and need_increase_flowRate[j].out_value;
        // Check if we need to limit the blowing flow rate or not
        need_limit_flowRate[j] = need_decrease_flowRate[j].out_value or (not need_increase_flowRate[j].out_value);

        //
        // BLOWING VOLUMIC FLOW RATE COMPUTATION
        //
        // PID for fans volumic flow rate (one for each room)
        pid_fan[j].u_s = Schedules[j].y[1] "Reference";
        pid_fan[j].u_m = Tamb[j] "Temperature from room air";
        // Compute volumic flow rate as max(minimal flow rate, (maximal flow rate time PID factor)
        fan_flowRate[j] = max(fan_flowRate_max[j] * (if pre(need_limit_flowRate[j]) then 0 else pid_fan[j].y), Schedules[j].y[3]);
        minimal_flowRate[j] = Schedules[j].y[3]
          "Just used as getter for external uses";
        //
        // BLOWING TEMPERATURE INSTRUCTION COMPUTATION
        //
        // PID for temperature of blowing instruction (one for each room)
        pid_Tsouff[j].u_s = if ((not need_heat[j].y) and need_overheat[j].y) then Schedules[j].y[2]
                                  else Schedules[j].y[1]
          "Reference depend if we need overheating or just heating";
        pid_Tsouff[j].u_m = Tamb[j] "Temperature from room air";
        // Compute the blowing temperature.
        // We first check the difference between max blowing temperature and environment temperature
        // Then we apply the PID factor to it.
        // So we compute blowing temperature as Text + PID factor * delta(max - ext)
        temperature_instruction_souff[j] = if pre(need_limit_flowRate[j]) then (Text + pid_Tsouff[j].y * (Tsouff_max[j] - Text)) else Tsouff_max[j];
      end for;

    //
    //Control of Sj pumps speed and Control of blowing electric heater
    //
      // Connection to pids
      pid_Sj.u_s = temperature_instruction_souff;
      pid_heater_elec.u_s = temperature_instruction_souff;
      need_elec_to_air.reference = temperature_instruction_souff - (need_elec_to_air.bandwidth / 2)
        "Shift down reference temperature because exchanger outlet temperature cannot be higher than maximal blowing temperature value";
      integrator_heating_elec.u = power_air_elec
        "Connect power computed to integrator";
      for j in 1:n loop
        // Compute speed for each pump (for each room) Sj
        Sj_speed[j] = if Sj_state_final[j] then (Sj_speed_max[j] * pid_Sj[j].y) else 0;
        // Compute boolean to check if we need or not the electric heater
        need_electric_heater[j].in_value = need_heat[j].y and need_elec_to_air[j].y;
        // Compute electric power needed to get blowing temperature instruction
        power_air_elec[j] = if need_electric_heater[j].out_value then (heater_elec_power[j] * pid_heater_elec[j].y) else 0;
      end for;

      connect(T1, need_solar_DHW.reference) annotation (Line(
          points={{-1488,410},{-1488,410},{-1440,410},{-1440,410},{-1440,410},{-1440,419.6},{-1397.2,
              419.6}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T1, need_solar_Storage.reference) annotation (Line(
          points={{-1488,410},{-1440,410},{-1440,401.6},{-1397.2,401.6}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T3, enough_power_in_DHW.u) annotation (Line(
          points={{-1488,380},{-1397.2,380}},
          color={0,0,127},
          smooth=Smooth.None));

      connect(T3, max_DHW_tank.u) annotation (Line(
          points={{-1488,380},{-1440,380},{-1440,360},{-1340,360},{-1340,386.4},{-1279.2,386.4}},
          color={0,0,127},
          smooth=Smooth.None));

      connect(T5, max_Storage_tank.u) annotation (Line(
          points={{-1488,324},{-1279,324}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T4, pip_DHW_elec.u_m) annotation (Line(
          points={{-1488,352},{-1458,352},{-1458,280},{-1409,280},{-1409,285}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(integrator_DHW_elec.y, energy_DHW_elec) annotation (Line(
          points={{-1379.4,290},{-1356,290},{-1356,280},{-1334,280}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Sj_state_timer.out_value, Sj_state) annotation (Line(
          points={{-1330.4,230},{-1308,230}},
          color={255,0,255},
          smooth=Smooth.None));
      connect(S5_state_timer.out_value, S5_state) annotation (Line(
          points={{-1330.4,200},{-1308,200}},
          color={255,0,255},
          smooth=Smooth.None));
      connect(T5, enough_energy_storage.u) annotation (Line(
          points={{-1488,324},{-1440,324},{-1440,188},{-1384,188},{-1384,200},{-1380,200}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Texch_out, pid_Sj.u_m) annotation (Line(
          points={{-1480,170},{-1140,170},{-1140,300},{-1108,300},{-1108,318.4}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Texch_out, need_elec_to_air.u) annotation (Line(
          points={{-1480,170},{-1140,170},{-1140,300},{-1105,300}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Tsouff, pid_heater_elec.u_m) annotation (Line(
          points={{-1480,210},{-1460,210},{-1460,170},{-980,170},{-980,316.4}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(integrator_heating_elec.y, energy_storage_elec) annotation (Line(
          points={{-937.2,312},{-926,312},{-926,308},{-906,308}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(extent={{-1480,120},{-880,460}},
              preserveAspectRatio=false), graphics={
            Rectangle(
              extent={{-1000,350},{-916,300}},
              lineColor={0,0,255},
              fillColor={255,85,85},
              fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{-1120,350},{-1036,294}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{-1420,260},{-1324,180}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Rectangle(
              extent={{-1420,438},{-1344,372}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={85,170,255}),              Text(
              extent={{-1420,436},{-1344,430}},
              lineColor={0,0,255},
              textString="Control of the solar valve (V3V)"),
                                                    Rectangle(
              extent={{-1300,432},{-1224,380}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={255,170,85}),              Text(
              extent={{-1300,430},{-1224,424}},
              lineColor={0,0,255},
              textString="Control of S5 (DHW tank pump)"),
            Rectangle(
              extent={{-1300,366},{-1224,320}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1300,362},{-1224,356}},
              lineColor={0,0,255},
              textString="Control of S6 (Storage tank pump)"),
            Rectangle(
              extent={{-1420,320},{-1344,270}},
              lineColor={0,0,255},
              fillColor={255,85,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1420,318},{-1344,312}},
              lineColor={0,0,255},
              textString="Control of DHW electrical power"),
                                                    Text(
              extent={{-1410,258},{-1334,252}},
              lineColor={0,0,255},
              textString="Control of Sj pumps (emitters)"),
            Rectangle(
              extent={{-1280,256},{-1206,184}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1278,252},{-1202,246}},
              lineColor={0,0,255},
              textString="Control of pumps activation
(Sj, S6, S5)"),
            Rectangle(
              extent={{-1120,254},{-1046,188}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1122,252},{-1046,246}},
              lineColor={0,0,255},
              textString="Pid controller for S5 and S6"),
            Rectangle(
              extent={{-1120,440},{-1034,380}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1116,436},{-1040,430}},
              lineColor={0,0,255},
              textString="Control of blowing flow and temperature"),
                                                    Text(
              extent={{-1118,348},{-1042,342}},
              lineColor={0,0,255},
              textString="PID controller for Sj (each room)"),
                                                    Text(
              extent={{-998,348},{-922,342}},
              lineColor={0,0,255},
              textString="Control of electric air blowing heating")}),
                                                     Icon(coordinateSystem(extent={{-1480,120},{-880,
                460}}, preserveAspectRatio=false), graphics),
        Documentation(info="<html>
<h4>Mise &agrave; jour: 2015-12-08</h4>
<p>Un sc&eacute;nario de consigne/consigne solaire diff&eacute;rent peut &ecirc;tre utilis&eacute; pour chaque pi&egrave;ce.</p>
<p>Note: Trouver un moyen d&rsquo;ajouter facilement les sc&eacute;nario au mod&egrave;le de l&rsquo;ext&eacute;rieur</p>
<h4><span style=\"color:#008000\">Control of the solar valve (V3V)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of S5 (DHW tank pump)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of S6 (pump for Storage tank exchanger)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of the electrical extra heating for DHW</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control if we need heat or overheat</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of pump temporization and modulation</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of system pumps speed (S5 and S6)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of blowing instruction (temperature and fan volumic flow rate)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of Electrical heater for heating demand</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of solar pump for each water/air exchanger (one fo each room)</span></h4>
<p>Fonctionnel</p>
<p><br><h4>N&eacute;cessite des tests en fonctionnement r&eacute;el pour mieux appr&eacute;cier le comportement des PID.</h4></p>
</html>"));
    end Equipements_control_20151210;

    model Equipements_control
      "Describe the control algorithm for pumps S6 and S5; V3V cot� solaire; Electrical power pull inside the DHW tank."
      import SI = Modelica.SIunits;

      extends Data.PID_parameters;

    //
    // General inputs
    //
      Modelica.Blocks.Interfaces.RealInput T1(unit="K")
        "Temperature inside collector"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-20,-20},{20,20}},
            origin={-1488,410}),
            iconTransformation(extent={{-18,-18},{18,18}},
            origin={-1478,440})));
      Modelica.Blocks.Interfaces.RealInput T3(unit="K")
        "Temperature inside solar tank (top)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,360},{-1468,400}}), iconTransformation(
              extent={{-1496,382},{-1460,418}})));
      Modelica.Blocks.Interfaces.RealInput T4(unit="K")
        "Temperature inside DHW tank next to drawing up (top)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,332},{-1468,372}}), iconTransformation(
              extent={{-1496,302},{-1460,338}})));
      Modelica.Blocks.Interfaces.RealInput T5(unit="K")
        "Temperature inside DHW tank next to solar exchanger (bottom)"
         annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,304},{-1468,344}}), iconTransformation(
              extent={{-1496,342},{-1460,378}})));
      Modelica.Blocks.Interfaces.RealInput T7(unit="K")
        "Temperature of water after terminal emitter (only one for all rooms)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1506,264},{-1466,304}}), iconTransformation(
              extent={{-1496,262},{-1460,298}})));
      Modelica.Blocks.Interfaces.RealVectorInput Tamb[n](each unit="K")
        "Vector of temperature inside rooms areas"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1500,228},{-1460,268}}), iconTransformation(
              extent={{-1496,186},{-1468,214}})));
      Modelica.Blocks.Interfaces.RealVectorInput Texch_out[n](each unit="K")
        "Vector of air temperature just after exchanger"      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
            transformation(extent={{-1500,150},{-1460,190}}),
                                                        iconTransformation(extent={{-1496,152},{-1468,
                180}})));
      Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n](each unit="K")
        "Vector of air temperature blowing in each room"   annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
            transformation(extent={{-1500,190},{-1460,230}}),
                                                        iconTransformation(extent={{-1496,120},{-1468,
                148}})));
      Modelica.Blocks.Interfaces.RealInput Text(unit="K")
        "Temperature outside the room (environnement)"
                                                annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
            transformation(
            extent={{-20,-20},{20,20}},
            rotation=0,
            origin={-1486,132}),
                              iconTransformation(extent={{-18,-18},{18,18}},
            rotation=0,
            origin={-1478,240})));

    //
    // Describe the control of the solar valve (V3V)
    //
      Modelica.Blocks.Logical.OnOffController need_solar_DHW(pre_y_start=true, bandwidth=2)
        "Return True if solar must not be used and False if solar must be used"
        annotation (Placement(transformation(extent={{-1396,410},{-1384,422}})));

      Modelica.Blocks.Logical.OnOffController need_solar_Storage(pre_y_start=true,
          bandwidth=2)
        "Return True if solar must be used to load the storage tank"
        annotation (Placement(transformation(extent={{-1396,392},{-1384,404}})));
      Modelica.Blocks.Logical.Hysteresis enough_power_in_DHW(uLow=temp_min_DHW - 2, uHigh=
            temp_min_DHW)
        "Test if we have enough power inside the DHW before authorize temperature inside the storage tank"
        annotation (Placement(transformation(extent={{-1396,374},{-1384,386}})));
       Modelica.Blocks.Interfaces.BooleanOutput V3V_solar
        "State of the solar valve" annotation (__Dymola_tag={"Valve"}, Placement(transformation(
              extent={{-1344,398},{-1316,426}}), iconTransformation(extent={{-14,-14},
                {14,14}},
            rotation=90,
            origin={-1140,474})));

    //
    // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_DHW = 363.15
        "Maximum admissible value for DHW tank" annotation (Dialog(group="Higher limit"));
      Boolean S5_state_temp
        "Temporary state of the S5 pump, see Sj control part to get final state";
      Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T3(
        y(start=false),
        uLow=4,
        uHigh=10) "Check if T1 - T3 is larger enough"
        annotation (Placement(transformation(extent={{-1278,412},{-1266,424}})));
      Modelica.Blocks.Logical.OnOffController max_DHW_tank(bandwidth=5, pre_y_start=true)
        "Test if the temperature inside the tank is lower than reference"
        annotation (Placement(transformation(extent={{-1278,392},{-1266,404}})));

    //
    // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_storage = 378.15
        "Maximum admissible value for Storage tank" annotation (Dialog(group="Higher limit"));
      parameter SI.ThermodynamicTemperature temp_min_DHW = 303.15
        "Minimum admissible value for DHW tank before allowing Storage tank loading" annotation (Dialog(group="Lower limit"));
      Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T5(
        y(start=false),
        uHigh=10,
        uLow=2.5) "Check if T1 - T5 is larger enough"
        annotation (Placement(transformation(extent={{-1278,352},{-1268,362}})));
      Modelica.Blocks.Logical.OnOffController max_Storage_tank(bandwidth=5)
        "Test if the temperature inside the tank is lower than reference"
        annotation (Placement(transformation(extent={{-1278,336},{-1268,346}})));
       Modelica.Blocks.Interfaces.BooleanOutput S6_state
        "State of the solar pump S6." annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(
              extent={{-1224,342},{-1196,370}}), iconTransformation(extent={{-14,-14},
                {14,14}},
            rotation=-90,
            origin={-1140,106})));

    //
    // Describe the control of the electrical extra heating for DHW
    //
      parameter SI.Power DHW_elec_power = 6000
        "Maximum power of the extra electrical heater for DHW";
      parameter SI.ThermodynamicTemperature DHW_setpoint = 313.15
        "Temperature setpoint for DHW" annotation (Dialog(group="Instructions"));
      Buildings.Controls.Continuous.LimPID pip_DHW_elec(
        controllerType=elecDHW_controllerType,
        k=elecDHW_k,
        Ti=elecDHW_Ti,
        Td=elecDHW_Td,
        yMax=elecDHW_yMax,
        yMin=elecDHW_yMin,
        reverseAction=elecDHW_reverseAction)
        "Controller for electrical heat to DHW"
        annotation (Placement(transformation(extent={{-1414,286},{-1404,296}})));
      Modelica.Blocks.Continuous.Integrator integrator_DHW_elec
        "Integration of the power used to keep domestic hot water at the setpoint temperature"
        annotation (Placement(transformation(extent={{-1392,284},{-1380,296}})));
      Modelica.Blocks.Interfaces.RealOutput power_DHW_elec( unit="W")
        "Output the power needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
              extent={{-1344,294},{-1316,322}}), iconTransformation(extent={{-940,216},
                {-916,240}})));
      Modelica.Blocks.Interfaces.RealOutput energy_DHW_elec( unit="J")
        "Output the cumulated energy needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
              extent={{-1344,264},{-1316,292}}), iconTransformation(extent={{-940,192},
                {-916,216}})));

    //
    // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
    //
      parameter SI.ThermodynamicTemperature temp_min_collector = 313.15
        "Minimum collector temperature in order to use direct solar energy" annotation (Dialog(group="Lower limit"));
      parameter SI.ThermodynamicTemperature temp_min_storage = 318.15
        "Minimum storage tank temperature in order to use indirect solar energy" annotation (Dialog(group="Lower limit"));
      Boolean direct_solar_state[n]
        "Vector of Sn pump state on direct solar energy (one for each room)";
      Boolean indirect_solar_state[n]
        "Vector of Sn pump state on indirect solar energy (one for each room)";
      Modelica.Blocks.Logical.OnOffController need_overheat[n](each bandwidth=1)
        annotation (Placement(transformation(extent={{-1410,190},{-1390,210}})));
      Modelica.Blocks.Logical.OnOffController need_heat[n](each bandwidth=0.5)
        annotation (Placement(transformation(extent={{-1410,220},{-1390,240}})));
      Modelica.Blocks.Logical.OnOffController enough_energy_collector(bandwidth=5)
        "We can use direct solar energy"
        annotation (Placement(transformation(extent={{-1376,220},{-1356,240}})));
      Modelica.Blocks.Logical.Hysteresis enough_energy_storage(uLow=
            temp_min_storage - 5, uHigh=temp_min_storage)
        annotation (Placement(transformation(extent={{-1378,190},{-1358,210}})));
      Utilities.Timers.Stay_on Sj_state_timer[n](each pause=60, out_value(each
            start=false))
        "Return the current state of Sj pump (vector of state)"
        annotation (Placement(transformation(extent={{-1348,222},{-1332,238}})));
      Utilities.Timers.Stay_on S5_state_timer(each pause=60, out_value(each start=false))
        "Return the current state of S5 pump."
        annotation (Placement(transformation(extent={{-1348,192},{-1332,208}})));
    protected
      Modelica.Blocks.Interfaces.BooleanOutput Sj_state_temp[n]
        "Control Sj pump behavior" annotation (Placement(transformation(extent={{-1324,
                214},{-1292,246}}), iconTransformation(extent={{-1300,232},{-1272,260}})));
    public
      Modelica.Blocks.Interfaces.BooleanOutput S5_state "Control S5 state"
        annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(extent={{-1324,184},{-1292,216}}), iconTransformation(
              extent={{-13,-13},{13,13}},
            rotation=-90,
            origin={-1113,107})));

    //
    // Describe how to compute temporization and flow modulation for Sj, S6, S5
    //
         Utilities.Timers.Wait_for_tempo_crack temporization_Sj(n=n) if n > 1
        "Used to add a temporization to Sj pumps. Avoid all pump to start at full speed at the same time and consuming all energy"
        annotation (Placement(transformation(extent={{-1266,216},{-1222,240}})));
      Utilities.Timers.Wait_for system_pump_mod[2]
        "Modulation of pumps for the system (S6 and S5 currently)"
        annotation (Placement(transformation(extent={{-1266,188},{-1222,212}})));
      Modelica.Blocks.Interfaces.BooleanOutput Sj_state[n]
        "Final Sj pumps states after temporization check" annotation (__Dymola_tag={"Pump", "state"}, Placement(
            transformation(extent={{-1206,206},{-1178,234}}), iconTransformation(
            extent={{-14,-14},{14,14}},
            rotation=-90,
            origin={-1168,106})));
    //
    // Describe pid pump output construction for S6 and S5
    //
      // S6 PID parameters
      parameter Real S6_delta_min=10
        "Minimal temperature difference between collector and storage tank"
        annotation (Dialog(tab="S6"));
      parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm S6_speed_max=3000
        "Maximum value for S6 speed"
        annotation (Dialog(tab="S6"));

      // S5 PID parameters
      parameter Real S5_delta_min=10
        "Minimal temperature difference between collector and DHW tank"
        annotation (Dialog(tab="S5"));
      parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm S5_speed_max=3000
        "Maximum value for S5 speed"
        annotation (Dialog(tab="S5"));
      Buildings.Controls.Continuous.LimPID pid_S6(
        controllerType=S6_controller,
        k=S6_k,
        Ti=S6_Ti,
        Td=S6_Td,
        wp=S6_wp,
        wd=S6_wd,
        yMax=S6_yMax,
        yMin=S6_yMin,
        reverseAction=true)
        annotation (Placement(transformation(extent={{-1118,226},{-1098,246}})));
      Buildings.Controls.Continuous.LimPID pid_S5(
        controllerType=S5_controller,
        k=S5_k,
        Ti=S5_Ti,
        Td=S5_Td,
        wp=S5_wp,
        wd=S5_wd,
        yMax=S5_yMax,
        yMin=S5_yMin,
        reverseAction=true)
        annotation (Placement(transformation(extent={{-1148,200},{-1128,220}})));

      Modelica.Blocks.Interfaces.RealOutput S5_speed( unit="1/min")
        "Final speed of S5 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
          Placement(transformation(extent={{-1086,226},{-1058,254}}), iconTransformation(extent={{-940,
                280},{-914,306}})));
      Modelica.Blocks.Interfaces.RealOutput S6_speed( unit="1/min")
        "Final speed of S6 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
          Placement(transformation(extent={{-1086,194},{-1058,222}}), iconTransformation(extent={{-940,
                306},{-914,332}})));

    //
    //Control of blowing flow rate/temperature
    //
      parameter SI.ThermodynamicTemperature Tsouff_max[n]= fill(273.15 + 32, n)
        "Maximum value of blowing temperature for each room"
        annotation (Dialog(tab="Blowing Temperatures"));
      parameter SI.Time tempo_algo_flowRate[n]= fill(600, n)
        "How much time we have to wait before switching between Temperature or flow regulation"
        annotation (Dialog(group="Instructions"));
      parameter Real fan_flowRate_max[n]=fill(900/3600, n)
        "Upper limit of fan flow rate." annotation (Dialog(tab="Fans"));
      Boolean need_limit_flowRate[n]
        "Boolean used to check if we need or not to reduce the blowing flow rate";
      Real temperature_instruction_souff[n]
        "Computed temperature instruction [K]";
      Modelica.Blocks.Interfaces.RealOutput minimal_flowRate[n](each unit="m3/s")
        "Minimal sanitary volumic flow rate [m3/s]"                        annotation (Placement(
            transformation(extent={{-1094,412},{-1074,432}}), iconTransformation(extent={{-940,
                408},{-916,432}})));
      Modelica.Blocks.Interfaces.RealOutput fan_flowRate[n](each unit="m3/s")
        "Computed fan flow rate [m3/s]"                                              annotation (
          Placement(transformation(extent={{-1094,390},{-1074,410}}), iconTransformation(extent={{-940,
                436},{-916,460}})));
      Modelica.Blocks.Logical.Hysteresis need_higher_flow[n](uLow=Tsouff_max - fill(1.1, size(
            Tsouff_max, 1)), uHigh=Tsouff_max - fill(0.1, size(Tsouff_max, 1)))
        "Test if we need a higher flow rate to cover the heating demand"
        annotation (Placement(transformation(extent={{-1174,416},{-1166,424}})));
      Utilities.Timers.Wait_for need_increase_flowRate[n](wait_for=tempo_algo_flowRate)
        "Time to wait before increasing blowing flow rate"
        annotation (Placement(transformation(extent={{-1156,416},{-1124,424}})));
      Utilities.Timers.Wait_for need_decrease_flowRate[n](wait_for=tempo_algo_flowRate)
        "Time to wait before decreasing blowing flow rate to minimal flow rate"
        annotation (Placement(transformation(extent={{-1156,396},{-1124,404}})));

      Modelica.Blocks.Logical.OnOffController flow_is_minimal[n](each bandwidth=10/3600)
        annotation (Placement(transformation(extent={{-1174,396},{-1166,404}})));
      Buildings.Controls.Continuous.LimPID pid_fan[n](
        controllerType=fan_controllerType,
        k=fan_k,
        Ti=fan_Ti,
        Td=fan_Td,
        wp=fan_wp,
        wd=fan_wd,
        each yMax=1,
        each yMin=0) "PID used to control blowing flow rate for each room"
                    annotation (Placement(transformation(extent={{-1112,396},{-1104,
                404}})));
       Buildings.Controls.Continuous.LimPID pid_Tsouff[n](
        controllerType=Tsouff_controllerType,
        k=Tsouff_k,
        Ti=Tsouff_Ti,
        Td=Tsouff_Td,
        yMax=Tsouff_yMax,
        yMin=Tsouff_yMin,
        reverseAction=Tsouff_reverseAction)
        "PID which control the blowing temperature instruction"
        annotation (Placement(transformation(extent={{-1112,416},{-1104,424}})));

    //
    //Control of Sj pumps speed
    //
      parameter SI.Time tempo_algo_elec[n]= fill(180, n)
        "How much time we have to wait before using electric heater"
        annotation (Dialog(group="Instructions"));
        parameter Real Sj_speed_max[n]= fill(3000, n)
        "Maximum value for Sj speed of each pump"
        annotation (Dialog(tab="Sj"));
       Buildings.Controls.Continuous.LimPID pid_Sj[n](
        controllerType=Sj_controllerType,
        k=Sj_k,
        Ti=Sj_Ti,
        Td=Sj_Td,
        yMax=Sj_yMax,
        yMin=Sj_yMin,
        reverseAction=Sj_reverseAction)
        "Compute correct factor in order to modulate Sj speed"
        annotation (Placement(transformation(extent={{-1176,312},{-1160,328}})));

      Utilities.Timers.Wait_for need_electric_heater[n](wait_for=tempo_algo_elec)
        "Check if we still need electric heater after `wait_for` time."
        annotation (Placement(transformation(extent={{-1146,290},{-1116,300}})));
      Modelica.Blocks.Logical.OnOffController need_elec_to_air[n](each bandwidth=2)
        "Check if solar energy gives enough power to the blowing air source"
        annotation (Placement(transformation(extent={{-1164,290},{-1154,300}})));
      Modelica.Blocks.Interfaces.RealOutput Sj_speed[n](each unit="1/min")
        "Output of each pump Sj speed"
        annotation (__Dymola_tag={"Pump", "speed"}, Placement(transformation(extent={{-1096,308},{-1076,328}}),
            iconTransformation(extent={{-940,332},{-914,358}})));

    //
    //Control of blowing electric heater
    //
      parameter Modelica.SIunits.Power heater_elec_power[n]=fill(5000, n)
        "Maximum power of the extra electrical heater for air blowing heating";
        Buildings.Controls.Continuous.LimPID pid_heater_elec[n](
          controllerType=elecHeat_controllerType,
          k=elecHeat_k,
          Ti=elecHeat_Ti,
          Td=elecHeat_Td,
          yMax=elecHeat_yMax,
          yMin=elecHeat_yMin)
        "Controller for electric heater on blowing air for each room"
          annotation (Placement(transformation(extent={{-1048,308},{-1032,324}})));

      Modelica.Blocks.Continuous.Integrator integrator_heating_elec[n]
        "Integration of the power used to keep room air at the setpoint temperature"
        annotation (Placement(transformation(extent={{-1014,294},{-998,310}})));
      Modelica.Blocks.Interfaces.RealOutput power_air_elec[n](each unit="W")
        "Output the power needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
              extent={{-976,316},{-948,344}}),   iconTransformation(extent={{-940,144},
                {-916,168}})));
      Modelica.Blocks.Interfaces.RealOutput energy_air_elec[n](each unit="J")
        "Output the cumulated energy needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
              extent={{-976,288},{-948,316}}),   iconTransformation(extent={{-940,120},
                {-916,144}})));
       Modelica.Blocks.Sources.CombiTimeTable             Schedules[n](
        tableOnFile=tableOnFile,
        table=table,
        tableName=tableName,
        fileName=fileName,
        verboseRead=verboseRead,
        columns=columns,
        each smoothness=smoothness,
        each extrapolation=extrapolation,
        offset=offset,
        startTime=startTime)
        "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
         annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(extent={{-1480,428},{-1448,460}})));

    equation
    //
    // Describe the control of the solar valve (V3V)
    //
      //Connect minimal between T4 and T3 for test with T1 (`+1` used to shift bandwidth upper)
      need_solar_DHW.u = min(T4, T3)+1 "[T1 >= min(T3, T4)]";
      need_solar_Storage.u = T5+1 "[T1 >= T5";
      // V3V open to solar collector only if we have
      V3V_solar = (enough_power_in_DHW.y and need_solar_Storage.y) or need_solar_DHW.y
        "[T1 >= T5 and T3 >= 32�C] or [T1 >= min(T3, T4)]";

    //
    // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
    //
      enoughDelta_T1_T3.u = T1 - T3 "[T1 - T3 >= 10]";
      max_DHW_tank.reference = temp_max_DHW "[T3 > temp_max_DHW]";
      // Compute S5 pump state according to temperature difference
      S5_state_temp = V3V_solar and max_DHW_tank.y and enoughDelta_T1_T3.y
        "[T1 - T3 >= 10 and [T1 > temp_max_DHW] and V3V_solar]";

    //
    // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
    //
      enoughDelta_T1_T5.u = T1 - T5 "[T1 - T5 >= 10]";
      max_Storage_tank.reference = temp_max_storage-2.5 "[T5 > temp_max_DHW]";
      // Compute S6 pump state
      S6_state = enoughDelta_T1_T5.y and enough_power_in_DHW.y and max_Storage_tank.y
        "[T1 - T5 >= 10 and T5 > temp_max_DHW]";

    //
    // Describe the control of the electrical extra heating for DHW
    //
      pip_DHW_elec.u_s = DHW_setpoint "Set DHW temperature setpoint for PID";
      power_DHW_elec = DHW_elec_power * pip_DHW_elec.y
        "Power compute using PID";
      integrator_DHW_elec.u = power_DHW_elec
        "Same thing, only here to be used outside the model";

    //
    // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
    //
    // Test temperature difference between T1 and max(T7 or minimal value to use direct solar)
      enough_energy_collector.u = max(temp_min_collector, T7);
      // Connect T1 to solar_direct bloc OnOffController
      enough_energy_collector.reference = T1 - (enough_energy_collector.bandwidth / 2)
        "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";
      // S5 pump state according to heating demande control
      S5_state_timer.in_value = Modelica.Math.BooleanVectors.anyTrue(direct_solar_state) or S5_state_temp
        "State of S5 pump according to heating demands";

    //
    // Describe how to compute temporization and flow modulation for Sj, S6, S5
    //
      // Connect pump S6 and S5 to modulation process
      system_pump_mod[1].in_value = S6_state;
      system_pump_mod[2].in_value = S5_state;
      // Connection of Sj pumps to temporization if at least 2 pumps else directly to Sj_final_state
      if n > 1 then
        connect(Sj_state_temp, temporization_Sj.in_value);
        connect(temporization_Sj.out_value, Sj_state);
      else
        Sj_state = Sj_state_temp;
      end if;

    //
    // Describe pid pump output construction for S6 and S5
    //
      // Connection to S6 pid
      pid_S6.u_s = S6_delta_min;
      pid_S6.u_m = T1 - T5;
       // Only need to compute output if pump S5 is On
       if S5_state then
         // Check if S5 pump must be modulated to find multiplicator
         S5_speed = S5_speed_max * (if system_pump_mod[2].out_value then pid_S5.y else 0.5);
       else
         S5_speed = 0;
       end if;
       // Connection to S5 pid
       pid_S5.u_s = S5_delta_min;
       pid_S5.u_m = T1 - T3;
       // Only need to compute output if pump S5 is On
       if S6_state then
         // Check if S5 pump must be modulated to find multiplicator
         S6_speed = S6_speed_max * (if system_pump_mod[1].out_value then pid_S6.y else 0.5);
       else
         S6_speed = 0;
       end if;

    //
    //Control of Sj pumps speed and Control of blowing electric heater
    //
      // Connection to pids
      pid_Sj.u_s = temperature_instruction_souff;
      pid_heater_elec.u_s = temperature_instruction_souff;
      need_elec_to_air.reference = temperature_instruction_souff - (need_elec_to_air.bandwidth / 2)
        "Shift down reference temperature because exchanger outlet temperature cannot be higher than maximal blowing temperature value";
      integrator_heating_elec.u = power_air_elec
        "Connect power computed to integrator";

    //
    // Loop only once over each room
    //

    for j in 1:n loop
      //
      // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
      //
        // Overheat connections
        need_overheat[j].reference = Schedules[j].y[2];
        need_overheat[j].u = Tamb[j] - (need_overheat[j].bandwidth / 2)
          "bandwidth used to shift down minimal value";

        // Heat connection
        need_heat[j].reference = Schedules[j].y[1];
        need_heat[j].u = Tamb[j] - (need_heat[j].bandwidth / 2)
          "bandwidth used to shift down minimal value";

        // We need to activate the Sj pump using direct solar energy from collectors
        direct_solar_state[j] = V3V_solar and enough_energy_collector.y and (need_heat[j].y or need_overheat[j].y);

        // We need to activate the Sj pump using indirect solar energy from tank
        indirect_solar_state[j] = (not V3V_solar) and enough_energy_storage.y and need_heat[j].y;

        // Final state of the pump for the room j
        Sj_state_timer[j].in_value = indirect_solar_state[j] or direct_solar_state[j];
      //
      //Control of blowing flow rate/temperature
      //
        //Minimal flow rate reference
        flow_is_minimal[j].reference = Schedules[j].y[3] * 1.5;
        flow_is_minimal[j].u = fan_flowRate[j]
          "RECURSIVE call to computed volumic flow rate";
        need_higher_flow[j].u = temperature_instruction_souff[j]
          "RECURSIVE call to computed temperature instruction";
        // Check if we need to increase blowing flow rate
        need_increase_flowRate[j].in_value = need_heat[j].y and need_higher_flow[j].y;
        // Check if we need to decrease flow rate
        need_decrease_flowRate[j].in_value = flow_is_minimal[j].y and need_increase_flowRate[j].out_value;
        // Check if we need to limit the blowing flow rate or not
        need_limit_flowRate[j] = need_decrease_flowRate[j].out_value or (not need_increase_flowRate[j].out_value);

      //
      // BLOWING VOLUMIC FLOW RATE COMPUTATION
      //
        // PID for fans volumic flow rate (one for each room)
        pid_fan[j].u_s = Schedules[j].y[1] "Reference";
        pid_fan[j].u_m = Tamb[j] "Temperature from room air";
        // Compute volumic flow rate as max(minimal flow rate, (maximal flow rate time PID factor)
        fan_flowRate[j] = max(fan_flowRate_max[j] * (if pre(need_limit_flowRate[j]) then 0 else pid_fan[j].y), Schedules[j].y[3]);
        minimal_flowRate[j] = Schedules[j].y[3]
          "Just used as getter for external uses";
      //
      // BLOWING TEMPERATURE INSTRUCTION COMPUTATION
      //
        // PID for temperature of blowing instruction (one for each room)
        pid_Tsouff[j].u_s = if ((not need_heat[j].y) and need_overheat[j].y) then Schedules[j].y[2]
                                  else Schedules[j].y[1]
          "Reference depend if we need overheating or just heating";
        pid_Tsouff[j].u_m = Tamb[j] "Temperature from room air";
        // Compute the blowing temperature.
        // We first check the difference between max blowing temperature and environment temperature
        // Then we apply the PID factor to it.
        // So we compute blowing temperature as Text + PID factor * delta(max - ext)
        temperature_instruction_souff[j] = if pre(need_limit_flowRate[j]) then (Text + pid_Tsouff[j].y * (Tsouff_max[j] - Text)) else Tsouff_max[j];
      //
      //Control of Sj pumps speed and Control of blowing electric heater
      //
        // Compute speed for each pump (for each room) Sj
        Sj_speed[j] = if Sj_state[j] then (Sj_speed_max[j] * pid_Sj[j].y) else 0;
        // Compute boolean to check if we need or not the electric heater
        need_electric_heater[j].in_value = need_heat[j].y and need_elec_to_air[j].y;
        // Compute electric power needed to get blowing temperature instruction
        power_air_elec[j] = if need_electric_heater[j].out_value then (heater_elec_power[j] * pid_heater_elec[j].y) else 0;
    end for;

      connect(T1, need_solar_DHW.reference) annotation (Line(
          points={{-1488,410},{-1488,410},{-1440,410},{-1440,410},{-1440,410},{-1440,419.6},{-1397.2,
              419.6}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T1, need_solar_Storage.reference) annotation (Line(
          points={{-1488,410},{-1440,410},{-1440,401.6},{-1397.2,401.6}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T3, enough_power_in_DHW.u) annotation (Line(
          points={{-1488,380},{-1397.2,380}},
          color={0,0,127},
          smooth=Smooth.None));

      connect(T3, max_DHW_tank.u) annotation (Line(
          points={{-1488,380},{-1440,380},{-1440,360},{-1340,360},{-1340,394.4},{-1279.2,
              394.4}},
          color={0,0,127},
          smooth=Smooth.None));

      connect(T5, max_Storage_tank.u) annotation (Line(
          points={{-1488,324},{-1386,324},{-1386,338},{-1279,338}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T4, pip_DHW_elec.u_m) annotation (Line(
          points={{-1488,352},{-1458,352},{-1458,280},{-1409,280},{-1409,285}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(integrator_DHW_elec.y, energy_DHW_elec) annotation (Line(
          points={{-1379.4,290},{-1354,290},{-1354,278},{-1330,278}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Sj_state_timer.out_value, Sj_state_temp) annotation (Line(
          points={{-1330.4,230},{-1308,230}},
          color={255,0,255},
          smooth=Smooth.None));
      connect(S5_state_timer.out_value, S5_state) annotation (Line(
          points={{-1330.4,200},{-1308,200}},
          color={255,0,255},
          smooth=Smooth.None));
      connect(T5, enough_energy_storage.u) annotation (Line(
          points={{-1488,324},{-1440,324},{-1440,188},{-1384,188},{-1384,200},{-1380,200}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Texch_out, pid_Sj.u_m) annotation (Line(
          points={{-1480,170},{-1290,170},{-1290,310.4},{-1168,310.4}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Texch_out, need_elec_to_air.u) annotation (Line(
          points={{-1480,170},{-1290,170},{-1290,292},{-1165,292}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Tsouff, pid_heater_elec.u_m) annotation (Line(
          points={{-1480,210},{-1466,210},{-1466,170},{-1040,170},{-1040,306.4}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(integrator_heating_elec.y, energy_air_elec) annotation (Line(
          points={{-997.2,302},{-962,302}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(extent={{-1480,120},{-940,460}},
              preserveAspectRatio=false), graphics={
            Rectangle(
              extent={{-1060,340},{-976,290}},
              lineColor={0,0,255},
              fillColor={255,85,85},
              fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{-1180,342},{-1096,286}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{-1420,260},{-1324,180}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Rectangle(
              extent={{-1420,438},{-1344,372}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={85,170,255}),              Text(
              extent={{-1420,436},{-1344,430}},
              lineColor={0,0,255},
              textString="Control of the solar valve (V3V)"),
                                                    Rectangle(
              extent={{-1300,440},{-1224,388}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={255,170,85}),              Text(
              extent={{-1300,438},{-1224,432}},
              lineColor={0,0,255},
              textString="Control of S5 (DHW tank pump)"),
            Rectangle(
              extent={{-1300,380},{-1224,334}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1300,376},{-1224,370}},
              lineColor={0,0,255},
              textString="Control of S6 (Storage tank pump)"),
            Rectangle(
              extent={{-1420,320},{-1344,270}},
              lineColor={0,0,255},
              fillColor={255,85,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1420,318},{-1344,312}},
              lineColor={0,0,255},
              textString="Control of DHW electrical power"),
                                                    Text(
              extent={{-1410,258},{-1334,252}},
              lineColor={0,0,255},
              textString="Control of Sj pumps (emitters)"),
            Rectangle(
              extent={{-1280,256},{-1206,184}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1278,252},{-1202,246}},
              lineColor={0,0,255},
              textString="Control of pumps activation
(Sj, S6, S5)"),
            Rectangle(
              extent={{-1160,260},{-1086,194}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1162,258},{-1086,252}},
              lineColor={0,0,255},
              textString="Pid controller for S5 and S6"),
            Rectangle(
              extent={{-1180,440},{-1094,380}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1176,436},{-1100,430}},
              lineColor={0,0,255},
              textString="Control of blowing flow and temperature"),
                                                    Text(
              extent={{-1178,340},{-1102,334}},
              lineColor={0,0,255},
              textString="PID controller for Sj (each room)"),
                                                    Text(
              extent={{-1058,338},{-982,332}},
              lineColor={0,0,255},
              textString="Control of electric air blowing heating")}),
                                                     Icon(coordinateSystem(extent={{-1480,
                120},{-940,460}},
                       preserveAspectRatio=false), graphics={
            Rectangle(
              extent={{-1480,460},{-940,120}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={255,85,85}),               Text(
              extent={{-1430,396},{-978,324}},
              lineColor={0,0,0},
              textString="Control of solar system
(pumps, valves, setpoint, ...)"),                   Text(
              extent={{-1444,242},{-972,162}},
              lineColor={0,0,0},
              textString="Control of air blowing system
(fans, setpoints, ...)")}),
        Documentation(info="<html>
<h4>Mise &agrave; jour: 2016-01-06</h4>
<p>Un sc&eacute;nario de consigne/consigne solaire diff&eacute;rent peut &ecirc;tre utilis&eacute; pour chaque pi&egrave;ce.</p>
<h4><span style=\"color:#008000\">Control of the solar valve (V3V)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of S5 (DHW tank pump)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of S6 (pump for Storage tank exchanger)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of the electrical extra heating for DHW</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control if we need heat or overheat</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of pump temporization and modulation</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of system pumps speed (S5 and S6)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of blowing instruction (temperature and fan volumic flow rate)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of Electrical heater for heating demand</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of solar pump for each water/air exchanger (one fo each room)</span></h4>
<p>Fonctionnel</p>
</html>"));
    end Equipements_control;

    model Equipements_control_smooth
      "Describe the control algorithm for pumps S6 and S5; V3V cot� solaire; Electrical power pull inside the DHW tank."
      import SI = Modelica.SIunits;

      extends Data.PID_parameters;

    //
    // General inputs
    //
      Modelica.Blocks.Interfaces.RealInput T1(unit="K")
        "Temperature inside collector"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-20,-20},{20,20}},
            origin={-1488,410}),
            iconTransformation(extent={{-18,-18},{18,18}},
            origin={-1478,440})));
      Modelica.Blocks.Interfaces.RealInput T3(unit="K")
        "Temperature inside solar tank (top)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,360},{-1468,400}}), iconTransformation(
              extent={{-1496,382},{-1460,418}})));
      Modelica.Blocks.Interfaces.RealInput T4(unit="K")
        "Temperature inside DHW tank next to drawing up (top)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,332},{-1468,372}}), iconTransformation(
              extent={{-1496,302},{-1460,338}})));
      Modelica.Blocks.Interfaces.RealInput T5(unit="K")
        "Temperature inside DHW tank next to solar exchanger (bottom)"
         annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,304},{-1468,344}}), iconTransformation(
              extent={{-1496,342},{-1460,378}})));
      Modelica.Blocks.Interfaces.RealInput T7(unit="K")
        "Temperature of water after terminal emitter (only one for all rooms)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1506,264},{-1466,304}}), iconTransformation(
              extent={{-1496,262},{-1460,298}})));
      Modelica.Blocks.Interfaces.RealVectorInput Tamb[n](each unit="K")
        "Vector of temperature inside rooms areas"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1500,228},{-1460,268}}), iconTransformation(
              extent={{-1496,186},{-1468,214}})));
      Modelica.Blocks.Interfaces.RealVectorInput Texch_out[n](each unit="K")
        "Vector of air temperature just after exchanger"      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
            transformation(extent={{-1500,150},{-1460,190}}),
                                                        iconTransformation(extent={{-1496,152},{-1468,
                180}})));
      Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n](each unit="K")
        "Vector of air temperature blowing in each room"   annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
            transformation(extent={{-1500,190},{-1460,230}}),
                                                        iconTransformation(extent={{-1496,120},{-1468,
                148}})));
      Modelica.Blocks.Interfaces.RealInput Text(unit="K")
        "Temperature outside the room (environnement)"
                                                annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
            transformation(
            extent={{-20,-20},{20,20}},
            rotation=0,
            origin={-1486,132}),
                              iconTransformation(extent={{-18,-18},{18,18}},
            rotation=0,
            origin={-1478,240})));

    //
    // Describe the control of the solar valve (V3V)
    //
      Modelica.Blocks.Logical.OnOffController need_solar_DHW(pre_y_start=true, bandwidth=2)
        "Return True if solar must not be used and False if solar must be used"
        annotation (Placement(transformation(extent={{-1396,410},{-1384,422}})));

      Modelica.Blocks.Logical.OnOffController need_solar_Storage(pre_y_start=true,
          bandwidth=2)
        "Return True if solar must be used to load the storage tank"
        annotation (Placement(transformation(extent={{-1396,392},{-1384,404}})));
      Modelica.Blocks.Logical.Hysteresis enough_power_in_DHW(uLow=temp_min_DHW - 2, uHigh=
            temp_min_DHW)
        "Test if we have enough power inside the DHW before authorize temperature inside the storage tank"
        annotation (Placement(transformation(extent={{-1396,374},{-1384,386}})));
       Modelica.Blocks.Interfaces.BooleanOutput V3V_solar
        "State of the solar valve" annotation (__Dymola_tag={"Valve"}, Placement(transformation(
              extent={{-1344,398},{-1316,426}}), iconTransformation(extent={{-14,-14},
                {14,14}},
            rotation=90,
            origin={-1140,474})));

    //
    // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_DHW = 363.15
        "Maximum admissible value for DHW tank" annotation (Dialog(group="Higher limit"));
      Boolean S5_state_temp
        "Temporary state of the S5 pump, see Sj control part to get final state";
      Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T3(
        y(start=false),
        uLow=4,
        uHigh=10) "Check if T1 - T3 is larger enough"
        annotation (Placement(transformation(extent={{-1278,412},{-1266,424}})));
      Modelica.Blocks.Logical.OnOffController max_DHW_tank(bandwidth=5, pre_y_start=true)
        "Test if the temperature inside the tank is lower than reference"
        annotation (Placement(transformation(extent={{-1278,392},{-1266,404}})));

    //
    // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_storage = 378.15
        "Maximum admissible value for Storage tank" annotation (Dialog(group="Higher limit"));
      parameter SI.ThermodynamicTemperature temp_min_DHW = 303.15
        "Minimum admissible value for DHW tank before allowing Storage tank loading" annotation (Dialog(group="Lower limit"));
      Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T5(
        y(start=false),
        uHigh=10,
        uLow=2.5) "Check if T1 - T5 is larger enough"
        annotation (Placement(transformation(extent={{-1278,352},{-1268,362}})));
      Modelica.Blocks.Logical.OnOffController max_Storage_tank(bandwidth=5)
        "Test if the temperature inside the tank is lower than reference"
        annotation (Placement(transformation(extent={{-1278,336},{-1268,346}})));
       Modelica.Blocks.Interfaces.BooleanOutput S6_state
        "State of the solar pump S6." annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(
              extent={{-1224,342},{-1196,370}}), iconTransformation(extent={{-14,-14},
                {14,14}},
            rotation=-90,
            origin={-1140,106})));

    //
    // Describe the control of the electrical extra heating for DHW
    //
      parameter SI.Power DHW_elec_power = 6000
        "Maximum power of the extra electrical heater for DHW";
      parameter SI.ThermodynamicTemperature DHW_setpoint = 313.15
        "Temperature setpoint for DHW" annotation (Dialog(group="Instructions"));
      Buildings.Controls.Continuous.LimPID pip_DHW_elec(
        controllerType=elecDHW_controllerType,
        k=elecDHW_k,
        Ti=elecDHW_Ti,
        Td=elecDHW_Td,
        yMax=elecDHW_yMax,
        yMin=elecDHW_yMin,
        reverseAction=elecDHW_reverseAction)
        "Controller for electrical heat to DHW"
        annotation (Placement(transformation(extent={{-1414,286},{-1404,296}})));
      Modelica.Blocks.Continuous.Integrator integrator_DHW_elec
        "Integration of the power used to keep domestic hot water at the setpoint temperature"
        annotation (Placement(transformation(extent={{-1392,284},{-1380,296}})));
      Modelica.Blocks.Interfaces.RealOutput power_DHW_elec( unit="W")
        "Output the power needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
              extent={{-1344,294},{-1316,322}}), iconTransformation(extent={{-940,216},
                {-916,240}})));
      Modelica.Blocks.Interfaces.RealOutput energy_DHW_elec( unit="J")
        "Output the cumulated energy needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
              extent={{-1344,264},{-1316,292}}), iconTransformation(extent={{-940,192},
                {-916,216}})));

    //
    // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
    //
      parameter SI.ThermodynamicTemperature temp_min_collector = 313.15
        "Minimum collector temperature in order to use direct solar energy" annotation (Dialog(group="Lower limit"));
      parameter SI.ThermodynamicTemperature temp_min_storage = 318.15
        "Minimum storage tank temperature in order to use indirect solar energy" annotation (Dialog(group="Lower limit"));
      Boolean direct_solar_state[n]
        "Vector of Sn pump state on direct solar energy (one for each room)";
      Boolean indirect_solar_state[n]
        "Vector of Sn pump state on indirect solar energy (one for each room)";
      Modelica.Blocks.Logical.OnOffController need_overheat[n](each bandwidth=1)
        annotation (Placement(transformation(extent={{-1410,190},{-1390,210}})));
      Modelica.Blocks.Logical.OnOffController need_heat[n](each bandwidth=0.5)
        annotation (Placement(transformation(extent={{-1410,220},{-1390,240}})));
      Modelica.Blocks.Logical.OnOffController enough_energy_collector(bandwidth=5)
        "We can use direct solar energy"
        annotation (Placement(transformation(extent={{-1376,220},{-1356,240}})));
      Modelica.Blocks.Logical.Hysteresis enough_energy_storage(uLow=
            temp_min_storage - 5, uHigh=temp_min_storage)
        annotation (Placement(transformation(extent={{-1378,190},{-1358,210}})));
      Utilities.Timers.Stay_on Sj_state_timer[n](each pause=60, out_value(each
            start=false))
        "Return the current state of Sj pump (vector of state)"
        annotation (Placement(transformation(extent={{-1348,222},{-1332,238}})));
      Utilities.Timers.Stay_on S5_state_timer(each pause=60, out_value(each start=false))
        "Return the current state of S5 pump."
        annotation (Placement(transformation(extent={{-1348,192},{-1332,208}})));
    protected
      Modelica.Blocks.Interfaces.BooleanOutput Sj_state_temp[n]
        "Control Sj pump behavior" annotation (Placement(transformation(extent={{-1324,
                214},{-1292,246}}), iconTransformation(extent={{-1300,232},{-1272,260}})));
    public
      Modelica.Blocks.Interfaces.BooleanOutput S5_state "Control S5 state"
        annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(extent={{-1324,184},{-1292,216}}), iconTransformation(
              extent={{-13,-13},{13,13}},
            rotation=-90,
            origin={-1113,107})));

    //
    // Describe how to compute temporization and flow modulation for Sj, S6, S5
    //
         Utilities.Timers.Wait_for_tempo_crack temporization_Sj(n=n) if n > 1
        "Used to add a temporization to Sj pumps. Avoid all pump to start at full speed at the same time and consuming all energy"
        annotation (Placement(transformation(extent={{-1266,216},{-1222,240}})));
      Utilities.Timers.Wait_for system_pump_mod[2]
        "Modulation of pumps for the system (S6 and S5 currently)"
        annotation (Placement(transformation(extent={{-1266,188},{-1222,212}})));
      Modelica.Blocks.Interfaces.BooleanOutput Sj_state[n]
        "Final Sj pumps states after temporization check" annotation (__Dymola_tag={"Pump", "state"}, Placement(
            transformation(extent={{-1206,206},{-1178,234}}), iconTransformation(
            extent={{-14,-14},{14,14}},
            rotation=-90,
            origin={-1168,106})));
    //
    // Describe pid pump output construction for S6 and S5
    //
      // S6 PID parameters
      parameter Real S6_delta_min=10
        "Minimal temperature difference between collector and storage tank"
        annotation (Dialog(tab="S6"));
      parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm S6_speed_max=3000
        "Maximum value for S6 speed"
        annotation (Dialog(tab="S6"));

      // S5 PID parameters
      parameter Real S5_delta_min=10
        "Minimal temperature difference between collector and DHW tank"
        annotation (Dialog(tab="S5"));
      parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm S5_speed_max=3000
        "Maximum value for S5 speed"
        annotation (Dialog(tab="S5"));
      Buildings.Controls.Continuous.LimPID pid_S6(
        controllerType=S6_controller,
        k=S6_k,
        Ti=S6_Ti,
        Td=S6_Td,
        wp=S6_wp,
        wd=S6_wd,
        yMax=S6_yMax,
        yMin=S6_yMin,
        reverseAction=true)
        annotation (Placement(transformation(extent={{-1118,226},{-1098,246}})));
      Buildings.Controls.Continuous.LimPID pid_S5(
        controllerType=S5_controller,
        k=S5_k,
        Ti=S5_Ti,
        Td=S5_Td,
        wp=S5_wp,
        wd=S5_wd,
        yMax=S5_yMax,
        yMin=S5_yMin,
        reverseAction=true)
        annotation (Placement(transformation(extent={{-1148,200},{-1128,220}})));

      Modelica.Blocks.Interfaces.RealOutput S5_speed( unit="1/min")
        "Final speed of S5 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
          Placement(transformation(extent={{-1086,226},{-1058,254}}), iconTransformation(extent={{-940,
                280},{-914,306}})));
      Modelica.Blocks.Interfaces.RealOutput S6_speed( unit="1/min")
        "Final speed of S6 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
          Placement(transformation(extent={{-1086,194},{-1058,222}}), iconTransformation(extent={{-940,
                306},{-914,332}})));

    //
    //Control of blowing flow rate/temperature
    //
      parameter SI.ThermodynamicTemperature Tsouff_max[n]= fill(273.15 + 32, n)
        "Maximum value of blowing temperature for each room"
        annotation (Dialog(tab="Blowing Temperatures"));
      parameter SI.Time tempo_algo_flowRate[n]= fill(600, n)
        "How much time we have to wait before switching between Temperature or flow regulation"
        annotation (Dialog(group="Instructions"));
      parameter Real fan_flowRate_max[n]=fill(900/3600, n)
        "Upper limit of fan flow rate." annotation (Dialog(tab="Fans"));
      Boolean need_limit_flowRate[n]
        "Boolean used to check if we need or not to reduce the blowing flow rate";
      Real temperature_instruction_souff[n]
        "Computed temperature instruction [K]";
      Modelica.Blocks.Interfaces.RealOutput minimal_flowRate[n](each unit="m3/s")
        "Minimal sanitary volumic flow rate [m3/s]"                        annotation (Placement(
            transformation(extent={{-1094,412},{-1074,432}}), iconTransformation(extent={{-940,
                408},{-916,432}})));
      Modelica.Blocks.Interfaces.RealOutput fan_flowRate[n](each unit="m3/s")
        "Computed fan flow rate [m3/s]"                                              annotation (
          Placement(transformation(extent={{-1094,390},{-1074,410}}), iconTransformation(extent={{-940,
                436},{-916,460}})));
      Modelica.Blocks.Logical.Hysteresis need_higher_flow[n](uLow=Tsouff_max - fill(1.1, size(
            Tsouff_max, 1)), uHigh=Tsouff_max - fill(0.1, size(Tsouff_max, 1)))
        "Test if we need a higher flow rate to cover the heating demand"
        annotation (Placement(transformation(extent={{-1174,416},{-1166,424}})));
      Utilities.Timers.Wait_for need_increase_flowRate[n](wait_for=tempo_algo_flowRate)
        "Time to wait before increasing blowing flow rate"
        annotation (Placement(transformation(extent={{-1156,416},{-1124,424}})));
      Utilities.Timers.Wait_for need_decrease_flowRate[n](wait_for=tempo_algo_flowRate)
        "Time to wait before decreasing blowing flow rate to minimal flow rate"
        annotation (Placement(transformation(extent={{-1156,396},{-1124,404}})));

      Modelica.Blocks.Logical.OnOffController flow_is_minimal[n](each bandwidth=10/3600)
        annotation (Placement(transformation(extent={{-1174,396},{-1166,404}})));
      Buildings.Controls.Continuous.LimPID pid_fan[n](
        controllerType=fan_controllerType,
        k=fan_k,
        Ti=fan_Ti,
        Td=fan_Td,
        wp=fan_wp,
        wd=fan_wd,
        each yMax=1,
        each yMin=0) "PID used to control blowing flow rate for each room"
                    annotation (Placement(transformation(extent={{-1112,396},{-1104,
                404}})));
       Buildings.Controls.Continuous.LimPID pid_Tsouff[n](
        controllerType=Tsouff_controllerType,
        k=Tsouff_k,
        Ti=Tsouff_Ti,
        Td=Tsouff_Td,
        yMax=Tsouff_yMax,
        yMin=Tsouff_yMin,
        reverseAction=Tsouff_reverseAction)
        "PID which control the blowing temperature instruction"
        annotation (Placement(transformation(extent={{-1112,416},{-1104,424}})));

    //
    //Control of Sj pumps speed
    //
      parameter SI.Time tempo_algo_elec[n]= fill(180, n)
        "How much time we have to wait before using electric heater"
        annotation (Dialog(group="Instructions"));
        parameter Real Sj_speed_max[n]= fill(3000, n)
        "Maximum value for Sj speed of each pump"
        annotation (Dialog(tab="Sj"));
       Buildings.Controls.Continuous.LimPID pid_Sj[n](
        controllerType=Sj_controllerType,
        k=Sj_k,
        Ti=Sj_Ti,
        Td=Sj_Td,
        yMax=Sj_yMax,
        yMin=Sj_yMin,
        reverseAction=Sj_reverseAction)
        "Compute correct factor in order to modulate Sj speed"
        annotation (Placement(transformation(extent={{-1176,312},{-1160,328}})));

      Utilities.Timers.Wait_for need_electric_heater[n](wait_for=tempo_algo_elec)
        "Check if we still need electric heater after `wait_for` time."
        annotation (Placement(transformation(extent={{-1146,290},{-1116,300}})));
      Modelica.Blocks.Logical.OnOffController need_elec_to_air[n](each bandwidth=2)
        "Check if solar energy gives enough power to the blowing air source"
        annotation (Placement(transformation(extent={{-1164,290},{-1154,300}})));
      Modelica.Blocks.Interfaces.RealOutput Sj_speed[n](each unit="1/min")
        "Output of each pump Sj speed"
        annotation (__Dymola_tag={"Pump", "speed"}, Placement(transformation(extent={{-1096,308},{-1076,328}}),
            iconTransformation(extent={{-940,332},{-914,358}})));

    //
    //Control of blowing electric heater
    //
      parameter Modelica.SIunits.Power heater_elec_power[n]=fill(5000, n)
        "Maximum power of the extra electrical heater for air blowing heating";
        Buildings.Controls.Continuous.LimPID pid_heater_elec[n](
          controllerType=elecHeat_controllerType,
          k=elecHeat_k,
          Ti=elecHeat_Ti,
          Td=elecHeat_Td,
          yMax=elecHeat_yMax,
          yMin=elecHeat_yMin)
        "Controller for electric heater on blowing air for each room"
          annotation (Placement(transformation(extent={{-1048,308},{-1032,324}})));

      Modelica.Blocks.Continuous.Integrator integrator_heating_elec[n]
        "Integration of the power used to keep room air at the setpoint temperature"
        annotation (Placement(transformation(extent={{-1014,294},{-998,310}})));
      Modelica.Blocks.Interfaces.RealOutput power_air_elec[n](each unit="W")
        "Output the power needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
              extent={{-976,316},{-948,344}}),   iconTransformation(extent={{-940,144},
                {-916,168}})));
      Modelica.Blocks.Interfaces.RealOutput energy_air_elec[n](each unit="J")
        "Output the cumulated energy needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
              extent={{-976,288},{-948,316}}),   iconTransformation(extent={{-940,120},
                {-916,144}})));
       Modelica.Blocks.Sources.CombiTimeTable             Schedules[n](
        tableOnFile=tableOnFile,
        table=table,
        tableName=tableName,
        fileName=fileName,
        verboseRead=verboseRead,
        columns=columns,
        each smoothness=smoothness,
        each extrapolation=extrapolation,
        offset=offset,
        startTime=startTime)
        "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
         annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(extent={{-1480,428},{-1448,460}})));

    equation
    //
    // Describe the control of the solar valve (V3V)
    //
      //Connect minimal between T4 and T3 for test with T1 (`+1` used to shift bandwidth upper)
      need_solar_DHW.u = Buildings.Utilities.Math.Functions.smoothMin(x1=T4,
                                                                      x2=T3,
                                                                      deltaX=0.1)+1
        "[T1 >= min(T3, T4)]";
      need_solar_Storage.u = T5+1 "[T1 >= T5";
      // V3V open to solar collector only if we have
      V3V_solar = (enough_power_in_DHW.y and need_solar_Storage.y) or need_solar_DHW.y
        "[T1 >= T5 and T3 >= 32�C] or [T1 >= min(T3, T4)]";

    //
    // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
    //
      enoughDelta_T1_T3.u = T1 - T3 "[T1 - T3 >= 10]";
      max_DHW_tank.reference = temp_max_DHW "[T3 > temp_max_DHW]";
      // Compute S5 pump state according to temperature difference
      S5_state_temp = V3V_solar and max_DHW_tank.y and enoughDelta_T1_T3.y
        "[T1 - T3 >= 10 and [T1 > temp_max_DHW] and V3V_solar]";

    //
    // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
    //
      enoughDelta_T1_T5.u = T1 - T5 "[T1 - T5 >= 10]";
      max_Storage_tank.reference = temp_max_storage-2.5 "[T5 > temp_max_DHW]";
      // Compute S6 pump state
      S6_state = enoughDelta_T1_T5.y and enough_power_in_DHW.y and max_Storage_tank.y
        "[T1 - T5 >= 10 and T5 > temp_max_DHW]";

    //
    // Describe the control of the electrical extra heating for DHW
    //
      pip_DHW_elec.u_s = DHW_setpoint "Set DHW temperature setpoint for PID";
      power_DHW_elec = DHW_elec_power * pip_DHW_elec.y
        "Power compute using PID";
      integrator_DHW_elec.u = power_DHW_elec
        "Same thing, only here to be used outside the model";

    //
    // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
    //
    // Test temperature difference between T1 and max(T7 or minimal value to use direct solar)
      enough_energy_collector.u = Buildings.Utilities.Math.Functions.smoothMax(x1=temp_min_collector,
                                                                               x2=T7,
                                                                               deltaX=0.1);
      // Connect T1 to solar_direct bloc OnOffController
      enough_energy_collector.reference = T1 - (enough_energy_collector.bandwidth / 2)
        "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";
      // S5 pump state according to heating demande control
      S5_state_timer.in_value = Modelica.Math.BooleanVectors.anyTrue(direct_solar_state) or S5_state_temp
        "State of S5 pump according to heating demands";

    //
    // Describe how to compute temporization and flow modulation for Sj, S6, S5
    //
      // Connect pump S6 and S5 to modulation process
      system_pump_mod[1].in_value = S6_state;
      system_pump_mod[2].in_value = S5_state;
      // Connection of Sj pumps to temporization if at least 2 pumps else directly to Sj_final_state
      if n > 1 then
        connect(Sj_state_temp, temporization_Sj.in_value);
        connect(temporization_Sj.out_value, Sj_state);
      else
        Sj_state = Sj_state_temp;
      end if;

    //
    // Describe pid pump output construction for S6 and S5
    //
      // Connection to S6 pid
      pid_S6.u_s = S6_delta_min;
      pid_S6.u_m = T1 - T5;
       // Only need to compute output if pump S5 is On
       if S5_state then
         // Check if S5 pump must be modulated to find multiplicator
         S5_speed = S5_speed_max * (if system_pump_mod[2].out_value then pid_S5.y else 0.5);
       else
         S5_speed = 0;
       end if;
       // Connection to S5 pid
       pid_S5.u_s = S5_delta_min;
       pid_S5.u_m = T1 - T3;
       // Only need to compute output if pump S5 is On
       if S6_state then
         // Check if S5 pump must be modulated to find multiplicator
         S6_speed = S6_speed_max * (if system_pump_mod[1].out_value then pid_S6.y else 0.5);
       else
         S6_speed = 0;
       end if;

    //
    //Control of Sj pumps speed and Control of blowing electric heater
    //
      // Connection to pids
      pid_Sj.u_s = temperature_instruction_souff;
      pid_heater_elec.u_s = temperature_instruction_souff;
      need_elec_to_air.reference = temperature_instruction_souff - (need_elec_to_air.bandwidth / 2)
        "Shift down reference temperature because exchanger outlet temperature cannot be higher than maximal blowing temperature value";
      integrator_heating_elec.u = power_air_elec
        "Connect power computed to integrator";

    //
    // Loop only once over each room
    //

    for j in 1:n loop
      //
      // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
      //
        // Overheat connections
        need_overheat[j].reference = Schedules[j].y[2];
        need_overheat[j].u = Tamb[j] - (need_overheat[j].bandwidth / 2)
          "bandwidth used to shift down minimal value";

        // Heat connection
        need_heat[j].reference = Schedules[j].y[1];
        need_heat[j].u = Tamb[j] - (need_heat[j].bandwidth / 2)
          "bandwidth used to shift down minimal value";

        // We need to activate the Sj pump using direct solar energy from collectors
        direct_solar_state[j] = V3V_solar and enough_energy_collector.y and (need_heat[j].y or need_overheat[j].y);

        // We need to activate the Sj pump using indirect solar energy from tank
        indirect_solar_state[j] = (not V3V_solar) and enough_energy_storage.y and need_heat[j].y;

        // Final state of the pump for the room j
        Sj_state_timer[j].in_value = indirect_solar_state[j] or direct_solar_state[j];
      //
      //Control of blowing flow rate/temperature
      //
        //Minimal flow rate reference
        flow_is_minimal[j].reference = Schedules[j].y[3] * 1.5;
        flow_is_minimal[j].u = fan_flowRate[j]
          "RECURSIVE call to computed volumic flow rate";
        need_higher_flow[j].u = temperature_instruction_souff[j]
          "RECURSIVE call to computed temperature instruction";
        // Check if we need to increase blowing flow rate
        need_increase_flowRate[j].in_value = need_heat[j].y and need_higher_flow[j].y;
        // Check if we need to decrease flow rate
        need_decrease_flowRate[j].in_value = flow_is_minimal[j].y and need_increase_flowRate[j].out_value;
        // Check if we need to limit the blowing flow rate or not
        need_limit_flowRate[j] = need_decrease_flowRate[j].out_value or (not need_increase_flowRate[j].out_value);

      //
      // BLOWING VOLUMIC FLOW RATE COMPUTATION
      //
        // PID for fans volumic flow rate (one for each room)
        pid_fan[j].u_s = Schedules[j].y[1] "Reference";
        pid_fan[j].u_m = Tamb[j] "Temperature from room air";
        // Compute volumic flow rate as max(minimal flow rate, (maximal flow rate time PID factor)
        fan_flowRate[j] = Buildings.Utilities.Math.Functions.smoothMax(x1=fan_flowRate_max[j] * (if pre(need_limit_flowRate[j]) then 0 else pid_fan[j].y),
                                                                       x2=Schedules[j].y[3],
                                                                       deltaX=0.001);
        minimal_flowRate[j] = Schedules[j].y[3]
          "Just used as getter for external uses";
      //
      // BLOWING TEMPERATURE INSTRUCTION COMPUTATION
      //
        // PID for temperature of blowing instruction (one for each room)
        pid_Tsouff[j].u_s = if ((not need_heat[j].y) and need_overheat[j].y) then Schedules[j].y[2]
                                  else Schedules[j].y[1]
          "Reference depend if we need overheating or just heating";
        pid_Tsouff[j].u_m = Tamb[j] "Temperature from room air";
        // Compute the blowing temperature.
        // We first check the difference between max blowing temperature and environment temperature
        // Then we apply the PID factor to it.
        // So we compute blowing temperature as Text + PID factor * delta(max - ext)
        temperature_instruction_souff[j] = if pre(need_limit_flowRate[j]) then (Text + pid_Tsouff[j].y * (Tsouff_max[j] - Text)) else Tsouff_max[j];
      //
      //Control of Sj pumps speed and Control of blowing electric heater
      //
        // Compute speed for each pump (for each room) Sj
        Sj_speed[j] = if Sj_state[j] then (Sj_speed_max[j] * pid_Sj[j].y) else 0;
        // Compute boolean to check if we need or not the electric heater
        need_electric_heater[j].in_value = need_heat[j].y and need_elec_to_air[j].y;
        // Compute electric power needed to get blowing temperature instruction
        power_air_elec[j] = if need_electric_heater[j].out_value then (heater_elec_power[j] * pid_heater_elec[j].y) else 0;
    end for;

      connect(T1, need_solar_DHW.reference) annotation (Line(
          points={{-1488,410},{-1440,410},{-1440,419.6},{-1397.2,419.6}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T1, need_solar_Storage.reference) annotation (Line(
          points={{-1488,410},{-1440,410},{-1440,401.6},{-1397.2,401.6}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T3, enough_power_in_DHW.u) annotation (Line(
          points={{-1488,380},{-1397.2,380}},
          color={0,0,127},
          smooth=Smooth.None));

      connect(T3, max_DHW_tank.u) annotation (Line(
          points={{-1488,380},{-1440,380},{-1440,360},{-1340,360},{-1340,394.4},{-1279.2,
              394.4}},
          color={0,0,127},
          smooth=Smooth.None));

      connect(T5, max_Storage_tank.u) annotation (Line(
          points={{-1488,324},{-1386,324},{-1386,338},{-1279,338}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T4, pip_DHW_elec.u_m) annotation (Line(
          points={{-1488,352},{-1458,352},{-1458,280},{-1409,280},{-1409,285}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(integrator_DHW_elec.y, energy_DHW_elec) annotation (Line(
          points={{-1379.4,290},{-1354,290},{-1354,278},{-1330,278}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Sj_state_timer.out_value, Sj_state_temp) annotation (Line(
          points={{-1330.4,230},{-1308,230}},
          color={255,0,255},
          smooth=Smooth.None));
      connect(S5_state_timer.out_value, S5_state) annotation (Line(
          points={{-1330.4,200},{-1308,200}},
          color={255,0,255},
          smooth=Smooth.None));
      connect(T5, enough_energy_storage.u) annotation (Line(
          points={{-1488,324},{-1440,324},{-1440,188},{-1384,188},{-1384,200},{-1380,200}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Texch_out, pid_Sj.u_m) annotation (Line(
          points={{-1480,170},{-1290,170},{-1290,310.4},{-1168,310.4}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Texch_out, need_elec_to_air.u) annotation (Line(
          points={{-1480,170},{-1290,170},{-1290,292},{-1165,292}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Tsouff, pid_heater_elec.u_m) annotation (Line(
          points={{-1480,210},{-1466,210},{-1466,170},{-1040,170},{-1040,306.4}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(integrator_heating_elec.y, energy_air_elec) annotation (Line(
          points={{-997.2,302},{-962,302}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(extent={{-1480,120},{-940,460}},
              preserveAspectRatio=false), graphics={
            Rectangle(
              extent={{-1060,340},{-976,290}},
              lineColor={0,0,255},
              fillColor={255,85,85},
              fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{-1180,342},{-1096,286}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{-1420,260},{-1324,180}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Rectangle(
              extent={{-1420,438},{-1344,372}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={85,170,255}),              Text(
              extent={{-1420,436},{-1344,430}},
              lineColor={0,0,255},
              textString="Control of the solar valve (V3V)"),
                                                    Rectangle(
              extent={{-1300,440},{-1224,388}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={255,170,85}),              Text(
              extent={{-1300,438},{-1224,432}},
              lineColor={0,0,255},
              textString="Control of S5 (DHW tank pump)"),
            Rectangle(
              extent={{-1300,380},{-1224,334}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1300,376},{-1224,370}},
              lineColor={0,0,255},
              textString="Control of S6 (Storage tank pump)"),
            Rectangle(
              extent={{-1420,320},{-1344,270}},
              lineColor={0,0,255},
              fillColor={255,85,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1420,318},{-1344,312}},
              lineColor={0,0,255},
              textString="Control of DHW electrical power"),
                                                    Text(
              extent={{-1410,258},{-1334,252}},
              lineColor={0,0,255},
              textString="Control of Sj pumps (emitters)"),
            Rectangle(
              extent={{-1280,256},{-1206,184}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1278,252},{-1202,246}},
              lineColor={0,0,255},
              textString="Control of pumps activation
(Sj, S6, S5)"),
            Rectangle(
              extent={{-1160,260},{-1086,194}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1162,258},{-1086,252}},
              lineColor={0,0,255},
              textString="Pid controller for S5 and S6"),
            Rectangle(
              extent={{-1180,440},{-1094,380}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1176,436},{-1100,430}},
              lineColor={0,0,255},
              textString="Control of blowing flow and temperature"),
                                                    Text(
              extent={{-1178,340},{-1102,334}},
              lineColor={0,0,255},
              textString="PID controller for Sj (each room)"),
                                                    Text(
              extent={{-1058,338},{-982,332}},
              lineColor={0,0,255},
              textString="Control of electric air blowing heating")}),
                                                     Icon(coordinateSystem(extent={{-1480,
                120},{-940,460}},
                       preserveAspectRatio=false), graphics={
            Rectangle(
              extent={{-1480,460},{-940,120}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={255,85,85}),               Text(
              extent={{-1430,396},{-978,324}},
              lineColor={0,0,0},
              textString="Control of solar system
(pumps, valves, setpoint, ...)"),                   Text(
              extent={{-1444,242},{-972,162}},
              lineColor={0,0,0},
              textString="Control of air blowing system
(fans, setpoints, ...)")}),
        Documentation(info="<html>
<h4>Mise &agrave; jour: 2016-01-06</h4>
<p>Un sc&eacute;nario de consigne/consigne solaire diff&eacute;rent peut &ecirc;tre utilis&eacute; pour chaque pi&egrave;ce.</p>
<h4><span style=\"color:#008000\">Control of the solar valve (V3V)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of S5 (DHW tank pump)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of S6 (pump for Storage tank exchanger)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of the electrical extra heating for DHW</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control if we need heat or overheat</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of pump temporization and modulation</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of system pumps speed (S5 and S6)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of blowing instruction (temperature and fan volumic flow rate)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of Electrical heater for heating demand</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of solar pump for each water/air exchanger (one fo each room)</span></h4>
<p>Fonctionnel</p>
</html>"));
    end Equipements_control_smooth;

    model Equipements_control_smooth_enhanced_old
      "Describe the control algorithm for pumps S6 and S5; V3V cot� solaire; Electrical power pull inside the DHW tank."
      import SI = Modelica.SIunits;

      extends Data.PID_parameters;

    //
    // General inputs
    //
      Modelica.Blocks.Interfaces.RealInput T1(unit="K")
        "Temperature inside collector"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-20,-20},{20,20}},
            origin={-1488,410}),
            iconTransformation(extent={{-18,-18},{18,18}},
            origin={-1478,440})));
      Modelica.Blocks.Interfaces.RealInput T3(unit="K")
        "Temperature inside DHW tank next to solar exchanger (bottom)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,360},{-1468,400}}), iconTransformation(
              extent={{-1496,382},{-1460,418}})));
      Modelica.Blocks.Interfaces.RealInput T4(unit="K")
        "Temperature inside DHW tank next to drawing up (top)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,332},{-1468,372}}), iconTransformation(
              extent={{-1496,302},{-1460,338}})));
      Modelica.Blocks.Interfaces.RealInput T5(unit="K")
        "Temperature inside solar tank (top)"
         annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,304},{-1468,344}}), iconTransformation(
              extent={{-1496,342},{-1460,378}})));
      Modelica.Blocks.Interfaces.RealInput T7(unit="K")
        "Temperature of water after terminal emitter (only one for all rooms)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1506,264},{-1466,304}}), iconTransformation(
              extent={{-1496,262},{-1460,298}})));
      Modelica.Blocks.Interfaces.RealVectorInput Tamb[n](each unit="K")
        "Vector of temperature inside rooms areas"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1500,228},{-1460,268}}), iconTransformation(
              extent={{-1496,186},{-1468,214}})));
      Modelica.Blocks.Interfaces.RealVectorInput Texch_out[n](each unit="K")
        "Vector of air temperature just after exchanger"      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
            transformation(extent={{-1500,150},{-1460,190}}),
                                                        iconTransformation(extent={{-1496,152},{-1468,
                180}})));
      Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n](each unit="K")
        "Vector of air temperature blowing in each room"   annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
            transformation(extent={{-1500,190},{-1460,230}}),
                                                        iconTransformation(extent={{-1496,120},{-1468,
                148}})));
      Modelica.Blocks.Interfaces.RealInput Text(unit="K")
        "Temperature outside the room (environnement)"
                                                annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
            transformation(
            extent={{-20,-20},{20,20}},
            rotation=0,
            origin={-1486,132}),
                              iconTransformation(extent={{-18,-18},{18,18}},
            rotation=0,
            origin={-1478,240})));

    //
    // Describe the control of the solar valve (V3V)
    //
      Modelica.Blocks.Logical.OnOffController need_solar_DHW(pre_y_start=true, bandwidth=2)
        "Return True if solar must be used and False if solar must not be used"
        annotation (Placement(transformation(extent={{-1396,410},{-1384,422}})));

      Modelica.Blocks.Logical.OnOffController need_solar_Storage(pre_y_start=true,
          bandwidth=2)
        "Return True if solar must be used to load the storage tank"
        annotation (Placement(transformation(extent={{-1396,392},{-1384,404}})));
      Modelica.Blocks.Logical.Hysteresis enough_power_in_DHW(uLow=temp_min_DHW - 2, uHigh=
            temp_min_DHW)
        "Test if we have enough power inside the DHW before authorize temperature inside the storage tank"
        annotation (Placement(transformation(extent={{-1396,374},{-1384,386}})));
       Modelica.Blocks.Interfaces.BooleanOutput V3V_solar
        "State of the solar valve" annotation (__Dymola_tag={"Valve"}, Placement(transformation(
              extent={{-1344,398},{-1316,426}}), iconTransformation(extent={{-14,-14},
                {14,14}},
            rotation=90,
            origin={-1140,474})));

    //
    // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_DHW = 363.15
        "Maximum admissible value for DHW tank" annotation (Dialog(group="Higher limit"));
      Boolean S5_state_temp
        "Temporary state of the S5 pump, see Sj control part to get final state";
      Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T3(
        y(start=false),
        uLow=4,
        uHigh=10) "Check if T1 - T3 is larger enough"
        annotation (Placement(transformation(extent={{-1278,412},{-1266,424}})));
      Modelica.Blocks.Logical.OnOffController max_DHW_tank(bandwidth=5, pre_y_start=true)
        "Test if the temperature inside the tank is lower than reference"
        annotation (Placement(transformation(extent={{-1278,392},{-1266,404}})));

    //
    // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_storage = 378.15
        "Maximum admissible value for Storage tank" annotation (Dialog(group="Higher limit"));
      parameter SI.ThermodynamicTemperature temp_min_DHW = 303.15
        "Minimum admissible value for DHW tank before allowing Storage tank loading" annotation (Dialog(group="Lower limit"));
      Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T5(
        y(start=false),
        uHigh=10,
        uLow=2.5) "Check if T1 - T5 is larger enough"
        annotation (Placement(transformation(extent={{-1278,352},{-1268,362}})));
      Modelica.Blocks.Logical.OnOffController max_Storage_tank(bandwidth=5)
        "Test if the temperature inside the tank is lower than reference"
        annotation (Placement(transformation(extent={{-1278,336},{-1268,346}})));
       Modelica.Blocks.Interfaces.BooleanOutput S6_state
        "State of the solar pump S6." annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(
              extent={{-1224,342},{-1196,370}}), iconTransformation(extent={{-14,-14},
                {14,14}},
            rotation=-90,
            origin={-1140,106})));

    //
    // Describe the control of the electrical extra heating for DHW
    //
      parameter SI.Power DHW_elec_power = 6000
        "Maximum power of the extra electrical heater for DHW";
      parameter SI.ThermodynamicTemperature DHW_setpoint = 313.15
        "Temperature setpoint for DHW" annotation (Dialog(group="Instructions"));
      Buildings.Controls.Continuous.LimPID pip_DHW_elec(
        controllerType=elecDHW_controllerType,
        k=elecDHW_k,
        Ti=elecDHW_Ti,
        Td=elecDHW_Td,
        yMax=elecDHW_yMax,
        yMin=elecDHW_yMin,
        reverseAction=elecDHW_reverseAction)
        "Controller for electrical heat to DHW"
        annotation (Placement(transformation(extent={{-1414,286},{-1404,296}})));
      Modelica.Blocks.Continuous.Integrator integrator_DHW_elec
        "Integration of the power used to keep domestic hot water at the setpoint temperature"
        annotation (Placement(transformation(extent={{-1392,284},{-1380,296}})));
      Modelica.Blocks.Interfaces.RealOutput power_DHW_elec( unit="W")
        "Output the power needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
              extent={{-1344,294},{-1316,322}}), iconTransformation(extent={{-940,216},
                {-916,240}})));
      Modelica.Blocks.Interfaces.RealOutput energy_DHW_elec( unit="J")
        "Output the cumulated energy needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
              extent={{-1344,264},{-1316,292}}), iconTransformation(extent={{-940,192},
                {-916,216}})));

    //
    // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
    //
      parameter SI.ThermodynamicTemperature temp_min_collector = 313.15
        "Minimum collector temperature in order to use direct solar energy" annotation (Dialog(group="Lower limit"));
      parameter SI.ThermodynamicTemperature temp_min_storage = 318.15
        "Minimum storage tank temperature in order to use indirect solar energy" annotation (Dialog(group="Lower limit"));
      Boolean direct_solar_state[n]
        "Vector of Sn pump state on direct solar energy (one for each room)"
                                                                            annotation (__Dymola_tag={"algo", "state"});
      Boolean indirect_solar_state[n]
        "Vector of Sn pump state on indirect solar energy (one for each room)"
                                                                              annotation (__Dymola_tag={"algo", "state"});
      Modelica.Blocks.Logical.OnOffController need_overheat[n](each bandwidth=1)
        annotation (Placement(transformation(extent={{-1410,190},{-1390,210}})));
      Modelica.Blocks.Logical.OnOffController need_heat[n](each bandwidth=0.5)
        annotation (Placement(transformation(extent={{-1410,220},{-1390,240}})));
      Modelica.Blocks.Logical.OnOffController enough_energy_collector(bandwidth=5)
        "We can use direct solar energy"
        annotation (Placement(transformation(extent={{-1376,220},{-1356,240}})));
      Modelica.Blocks.Logical.Hysteresis enough_energy_storage(uLow=
            temp_min_storage - 5, uHigh=temp_min_storage)
        annotation (Placement(transformation(extent={{-1378,190},{-1358,210}})));
      Utilities.Timers.OnDelay_pause
                               Sj_state_timer[n](each delayTime=60, y(each
            start=false))
        "Return the current state of Sj pump (vector of state)"
        annotation (Placement(transformation(extent={{-1348,222},{-1332,238}})));
      Utilities.Timers.OnDelay_pause
                               S5_state_timer(each delayTime=60, y(each start=false))
        "Return the current state of S5 pump."
        annotation (Placement(transformation(extent={{-1348,192},{-1332,208}})));
    protected
      Modelica.Blocks.Interfaces.BooleanOutput Sj_state_temp[n]
        "Control Sj pump behavior" annotation (Placement(transformation(extent={{-1324,
                214},{-1292,246}}), iconTransformation(extent={{-1300,232},{-1272,260}})));
    public
      Modelica.Blocks.Interfaces.BooleanOutput S5_state "Control S5 state"
        annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(extent={{-1324,184},{-1292,216}}), iconTransformation(
              extent={{-13,-13},{13,13}},
            rotation=-90,
            origin={-1113,107})));

    //
    // Describe how to compute temporization and flow modulation for Sj, S6, S5
    //
         Utilities.Timers.Wait_for_tempo_crack temporization_Sj(n=n) if n > 1
        "Used to add a temporization to Sj pumps. Avoid all pump to start at full speed at the same time and consuming all energy"
        annotation (Placement(transformation(extent={{-1266,216},{-1222,240}})));
      Utilities.Timers.OnDelay_risingEdge
                                system_pump_mod[2](each delayTime=90)
        "Modulation of pumps for the system (S6 and S5 currently)"
        annotation (Placement(transformation(extent={{-1254,186},{-1232,208}})));
      Modelica.Blocks.Interfaces.BooleanOutput Sj_state[n]
        "Final Sj pumps states after temporization check" annotation (__Dymola_tag={"Pump", "state"}, Placement(
            transformation(extent={{-1206,206},{-1178,234}}), iconTransformation(
            extent={{-14,-14},{14,14}},
            rotation=-90,
            origin={-1168,106})));
    //
    // Describe pid pump output construction for S6 and S5
    //
      // S6 PID parameters
      parameter Real S6_delta_min=10
        "Minimal temperature difference between collector and storage tank"
        annotation (Dialog(tab="S6"));
      parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm S6_speed_max=3000
        "Maximum value for S6 speed"
        annotation (Dialog(tab="S6"));

      // S5 PID parameters
      parameter Real S5_delta_min=10
        "Minimal temperature difference between collector and DHW tank"
        annotation (Dialog(tab="S5"));
      parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm S5_speed_max=3000
        "Maximum value for S5 speed"
        annotation (Dialog(tab="S5"));
      Buildings.Controls.Continuous.LimPID pid_S6(
        controllerType=S6_controller,
        k=S6_k,
        Ti=S6_Ti,
        Td=S6_Td,
        wp=S6_wp,
        wd=S6_wd,
        yMax=S6_yMax,
        yMin=S6_yMin,
        reverseAction=true)
        annotation (Placement(transformation(extent={{-1118,226},{-1098,246}})));
      Buildings.Controls.Continuous.LimPID pid_S5(
        controllerType=S5_controller,
        k=S5_k,
        Ti=S5_Ti,
        Td=S5_Td,
        wp=S5_wp,
        wd=S5_wd,
        yMax=S5_yMax,
        yMin=S5_yMin,
        reverseAction=true)
        annotation (Placement(transformation(extent={{-1148,200},{-1128,220}})));

      Modelica.Blocks.Interfaces.RealOutput S5_speed( unit="1/min")
        "Final speed of S5 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
          Placement(transformation(extent={{-1086,226},{-1058,254}}), iconTransformation(extent={{-940,
                280},{-914,306}})));
      Modelica.Blocks.Interfaces.RealOutput S6_speed( unit="1/min")
        "Final speed of S6 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
          Placement(transformation(extent={{-1086,194},{-1058,222}}), iconTransformation(extent={{-940,
                306},{-914,332}})));

    //
    //Control of blowing flow rate/temperature
    //
      parameter SI.ThermodynamicTemperature Tsouff_max[n]= fill(273.15 + 32, n)
        "Maximum value of blowing temperature for each room"
        annotation (Dialog(tab="Blowing Temperatures"));
      parameter SI.Time tempo_algo_flowRate[n]= fill(600, n)
        "How much time we have to wait before switching between Temperature or flow regulation"
        annotation (Dialog(group="Instructions"));
      parameter Real fan_flowRate_max[n]=fill(900/3600, n)
        "Upper limit of fan flow rate." annotation (Dialog(tab="Fans"));
      Boolean need_limit_flowRate[n]
        "Boolean used to check if we need or not to reduce the blowing flow rate";
      Real temperature_instruction_souff[n]
        "Computed temperature instruction [K]";
      Modelica.Blocks.Interfaces.RealOutput minimal_flowRate[n](each unit="m3/s")
        "Minimal sanitary volumic flow rate [m3/s]"                        annotation (Placement(
            transformation(extent={{-1094,412},{-1074,432}}), iconTransformation(extent={{-940,
                408},{-916,432}})));
      Modelica.Blocks.Interfaces.RealOutput fan_flowRate[n](each unit="m3/s")
        "Computed fan flow rate [m3/s]"                                              annotation (
          Placement(transformation(extent={{-1094,390},{-1074,410}}), iconTransformation(extent={{-940,
                436},{-916,460}})));
      Modelica.Blocks.Logical.Hysteresis need_higher_flow[n](uLow=Tsouff_max - fill(1.1, size(
            Tsouff_max, 1)), uHigh=Tsouff_max - fill(0.1, size(Tsouff_max, 1)))
        "Test if we need a higher flow rate to cover the heating demand"
        annotation (Placement(transformation(extent={{-1172,412},{-1158,426}})));
      Utilities.Timers.OnDelay_risingEdge
                                need_increase_flowRate[n](delayTime=tempo_algo_flowRate)
        "Time to wait before increasing blowing flow rate"
        annotation (Placement(transformation(extent={{-1144,412},{-1130,426}})));
      Utilities.Timers.OnDelay_risingEdge
                                need_decrease_flowRate[n](delayTime=tempo_algo_flowRate)
        "Time to wait before decreasing blowing flow rate to minimal flow rate"
        annotation (Placement(transformation(extent={{-1144,386},{-1130,400}})));

      Modelica.Blocks.Logical.OnOffController flow_is_minimal[n](each bandwidth=10/3600)
        annotation (Placement(transformation(extent={{-1172,386},{-1158,400}})));
      Buildings.Controls.Continuous.LimPID pid_fan[n](
        controllerType=fan_controllerType,
        k=fan_k,
        Ti=fan_Ti,
        Td=fan_Td,
        wp=fan_wp,
        wd=fan_wd,
        each yMax=1,
        each yMin=0) "PID used to control blowing flow rate for each room"
                    annotation (Placement(transformation(extent={{-1114,388},{
                -1102,400}})));
       Buildings.Controls.Continuous.LimPID pid_Tsouff[n](
        controllerType=Tsouff_controllerType,
        k=Tsouff_k,
        Ti=Tsouff_Ti,
        Td=Tsouff_Td,
        yMax=Tsouff_yMax,
        yMin=Tsouff_yMin,
        reverseAction=Tsouff_reverseAction)
        "PID which control the blowing temperature instruction"
        annotation (Placement(transformation(extent={{-1116,414},{-1104,426}})));

    //
    //Control of Sj pumps speed
    //
      parameter SI.Time tempo_algo_elec[n]= fill(180, n)
        "How much time we have to wait before using electric heater"
        annotation (Dialog(group="Instructions"));
        parameter Real Sj_speed_max[n]= fill(3000, n)
        "Maximum value for Sj speed of each pump"
        annotation (Dialog(tab="Sj"));
       Buildings.Controls.Continuous.LimPID pid_Sj[n](
        controllerType=Sj_controllerType,
        k=Sj_k,
        Ti=Sj_Ti,
        Td=Sj_Td,
        yMax=Sj_yMax,
        yMin=Sj_yMin,
        reverseAction=Sj_reverseAction)
        "Compute correct factor in order to modulate Sj speed"
        annotation (Placement(transformation(extent={{-1176,312},{-1160,328}})));

      Utilities.Timers.OnDelay_risingEdge
                                need_electric_heater[n](delayTime=tempo_algo_elec)
        "Check if we still need electric heater after `wait_for` time."
        annotation (Placement(transformation(extent={{-1138,290},{-1124,304}})));
      Modelica.Blocks.Logical.OnOffController need_elec_to_air[n](each bandwidth=2)
        "Check if solar energy gives enough power to the blowing air source"
        annotation (Placement(transformation(extent={{-1164,290},{-1154,300}})));
      Modelica.Blocks.Interfaces.RealOutput Sj_speed[n](each unit="1/min")
        "Output of each pump Sj speed"
        annotation (__Dymola_tag={"Pump", "speed"}, Placement(transformation(extent={{-1096,308},{-1076,328}}),
            iconTransformation(extent={{-940,332},{-914,358}})));

    //
    //Control of blowing electric heater
    //
      parameter Modelica.SIunits.Power heater_elec_power[n]=fill(5000, n)
        "Maximum power of the extra electrical heater for air blowing heating";
        Buildings.Controls.Continuous.LimPID pid_heater_elec[n](
          controllerType=elecHeat_controllerType,
          k=elecHeat_k,
          Ti=elecHeat_Ti,
          Td=elecHeat_Td,
          yMax=elecHeat_yMax,
          yMin=elecHeat_yMin)
        "Controller for electric heater on blowing air for each room"
          annotation (Placement(transformation(extent={{-1048,308},{-1032,324}})));

      Modelica.Blocks.Continuous.Integrator integrator_heating_elec[n]
        "Integration of the power used to keep room air at the setpoint temperature"
        annotation (Placement(transformation(extent={{-1014,294},{-998,310}})));
      Modelica.Blocks.Interfaces.RealOutput power_air_elec[n](each unit="W")
        "Output the power needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
              extent={{-976,316},{-948,344}}),   iconTransformation(extent={{-940,144},
                {-916,168}})));
      Modelica.Blocks.Interfaces.RealOutput energy_air_elec[n](each unit="J")
        "Output the cumulated energy needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
              extent={{-976,288},{-948,316}}),   iconTransformation(extent={{-940,120},
                {-916,144}})));
       Modelica.Blocks.Sources.CombiTimeTable             Schedules[n](
        tableOnFile=tableOnFile,
        table=table,
        tableName=tableName,
        fileName=fileName,
        verboseRead=verboseRead,
        columns=columns,
        each smoothness=smoothness,
        each extrapolation=extrapolation,
        offset=offset,
        startTime=startTime)
        "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
         annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(extent={{-1480,428},{-1448,460}})));

    equation
    //
    // Describe the control of the solar valve (V3V)
    //
      //Connect minimal between T4 and T3 for test with T1 (`+1` used to shift bandwidth upper)
      need_solar_DHW.u = Buildings.Utilities.Math.Functions.smoothMin(x1=T4,
                                                                      x2=T3,
                                                                      deltaX=0.1)+1
        "[T1 >= min(T3, T4)]";
      need_solar_Storage.u = T5+1 "[T1 >= T5";
      // V3V open to solar collector only if we have
      V3V_solar = (enough_power_in_DHW.y and need_solar_Storage.y) or need_solar_DHW.y
        "[T1 >= T5 and T3 >= 32�C] or [T1 >= min(T3, T4)]";

    //
    // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
    //
      enoughDelta_T1_T3.u = T1 - T3 "[T1 - T3 >= 10]";
      max_DHW_tank.reference = temp_max_DHW "[T3 > temp_max_DHW]";
      // Compute S5 pump state according to temperature difference
      S5_state_temp = V3V_solar and max_DHW_tank.y and enoughDelta_T1_T3.y
        "[T1 - T3 >= 10 and [T3 > temp_max_DHW] and V3V_solar]";

    //
    // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
    //
      enoughDelta_T1_T5.u = T1 - T5 "[T1 - T5 >= 10]";
      max_Storage_tank.reference = temp_max_storage-2.5 "[T5 > temp_max_DHW]";
      // Compute S6 pump state
      S6_state = enoughDelta_T1_T5.y and enough_power_in_DHW.y and max_Storage_tank.y
        "[T1 - T5 >= 10 and min(T3, T4) >= 30 and T5 > temp_max_DHW]";

    //
    // Describe the control of the electrical extra heating for DHW
    //
      pip_DHW_elec.u_s = DHW_setpoint "Set DHW temperature setpoint for PID";
      power_DHW_elec = DHW_elec_power * pip_DHW_elec.y
        "Power compute using PID";
      integrator_DHW_elec.u = power_DHW_elec
        "Same thing, only here to be used outside the model";

    //
    // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
    //
    // Test temperature difference between T1 and max(T7 or minimal value to use direct solar)
      enough_energy_collector.u = Buildings.Utilities.Math.Functions.smoothMax(x1=temp_min_collector,
                                                                               x2=T7,
                                                                               deltaX=0.1);
      // Connect T1 to solar_direct bloc OnOffController
      enough_energy_collector.reference = T1 - (enough_energy_collector.bandwidth / 2)
        "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";
      // S5 pump state according to heating demande control
      S5_state_timer.u = Modelica.Math.BooleanVectors.anyTrue(direct_solar_state) or S5_state_temp
        "State of S5 pump according to heating demands";

    //
    // Describe how to compute temporization and flow modulation for Sj, S6, S5
    //
      // Connect pump S6 and S5 to modulation process
      system_pump_mod[1].u = S6_state;
      system_pump_mod[2].u = S5_state;
      // Connection of Sj pumps to temporization if at least 2 pumps else directly to Sj_final_state
      if n > 1 then
        connect(Sj_state_temp, temporization_Sj.in_value);
        connect(temporization_Sj.out_value, Sj_state);
      else
        Sj_state = Sj_state_temp;
      end if;

    //
    // Describe pid pump output construction for S6 and S5
    //
      // Connection to S6 pid
      pid_S6.u_s = S6_delta_min;
      pid_S6.u_m = T1 - T5;
       // Only need to compute output if pump S5 is On
       if S5_state then
         // Check if S5 pump must be modulated to find multiplicator
         S5_speed = smooth(1, S5_speed_max * (if system_pump_mod[2].y then pid_S5.y else 0.5));
       else
         S5_speed = 0;
       end if;
       // Connection to S5 pid
       pid_S5.u_s = S5_delta_min;
       pid_S5.u_m = T1 - T3;
       // Only need to compute output if pump S5 is On
       if S6_state then
         // Check if S5 pump must be modulated to find multiplicator
         S6_speed = smooth(1, S6_speed_max * (if system_pump_mod[1].y then pid_S6.y else 0.5));
       else
         S6_speed = 0;
       end if;

    //
    //Control of Sj pumps speed and Control of blowing electric heater
    //
      // Connection to pids
      pid_Sj.u_s = temperature_instruction_souff;
      pid_heater_elec.u_s = temperature_instruction_souff;
      need_elec_to_air.reference = temperature_instruction_souff - (need_elec_to_air.bandwidth / 2)
        "Shift down reference temperature because exchanger outlet temperature cannot be higher than maximal blowing temperature value";
      integrator_heating_elec.u = power_air_elec
        "Connect power computed to integrator";

    //
    // Loop only once over each room
    //

    for j in 1:n loop
      //
      // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
      //
        // Overheat connections
        need_overheat[j].reference = Schedules[j].y[2];
        need_overheat[j].u = Tamb[j] - (need_overheat[j].bandwidth / 2)
          "bandwidth used to shift down minimal value";

        // Heat connection
        need_heat[j].reference = Schedules[j].y[1];
        need_heat[j].u = Tamb[j] - (need_heat[j].bandwidth / 2)
          "bandwidth used to shift down minimal value";

        // We need to activate the Sj pump using direct solar energy from collectors
        direct_solar_state[j] = V3V_solar and enough_energy_collector.y and (need_heat[j].y or need_overheat[j].y);

        // We need to activate the Sj pump using indirect solar energy from tank
        indirect_solar_state[j] = (not V3V_solar) and enough_energy_storage.y and need_heat[j].y;

        // Final state of the pump for the room j
        Sj_state_timer[j].u = indirect_solar_state[j] or direct_solar_state[j];
      //
      //Control of blowing flow rate/temperature
      //
        //Minimal flow rate reference
        flow_is_minimal[j].reference = Schedules[j].y[3] * 1.5;
        flow_is_minimal[j].u = fan_flowRate[j]
          "RECURSIVE call to computed volumic flow rate";
        need_higher_flow[j].u = temperature_instruction_souff[j]
          "RECURSIVE call to computed temperature instruction";
        // Check if we need to increase blowing flow rate
        need_increase_flowRate[j].u = need_heat[j].y and need_higher_flow[j].y;
        // Check if we need to decrease flow rate
        need_decrease_flowRate[j].u = flow_is_minimal[j].y and need_increase_flowRate[j].y;
        // Check if we need to limit the blowing flow rate or not
        need_limit_flowRate[j] = need_decrease_flowRate[j].u or (not need_increase_flowRate[j].y);

      //
      // BLOWING VOLUMIC FLOW RATE COMPUTATION
      //
        // PID for fans volumic flow rate (one for each room)
        pid_fan[j].u_s = Schedules[j].y[1] "Reference";
        pid_fan[j].u_m = Tamb[j] "Temperature from room air";
        // Compute volumic flow rate as max(minimal flow rate, (maximal flow rate time PID factor)
        fan_flowRate[j] = Buildings.Utilities.Math.Functions.smoothMax(x1=fan_flowRate_max[j] * (if pre(need_limit_flowRate[j]) then 0 else pid_fan[j].y),
                                                                       x2=Schedules[j].y[3],
                                                                       deltaX=0.001);
        minimal_flowRate[j] = Schedules[j].y[3]
          "Just used as getter for external uses";
      //
      // BLOWING TEMPERATURE INSTRUCTION COMPUTATION
      //
        // PID for temperature of blowing instruction (one for each room)
        pid_Tsouff[j].u_s = smooth(1, if ((not need_heat[j].y) and need_overheat[j].y) then Schedules[j].y[2]
                                  else Schedules[j].y[1])
          "Reference depend if we need overheating or just heating";
        pid_Tsouff[j].u_m = Tamb[j] "Temperature from room air";
        // Compute the blowing temperature.
        // We first check the difference between max blowing temperature and environment temperature
        // Then we apply the PID factor to it.
        // So we compute blowing temperature as Text + PID factor * delta(max - ext)
        temperature_instruction_souff[j] = smooth(1, if pre(need_limit_flowRate[j]) then (Text + pid_Tsouff[j].y * (Tsouff_max[j] - Text)) else Tsouff_max[j]);
      //
      //Control of Sj pumps speed and Control of blowing electric heater
      //
        // Compute speed for each pump (for each room) Sj
        Sj_speed[j] = smooth(1, if Sj_state[j] then (Sj_speed_max[j] * pid_Sj[j].y) else 0);
        // Compute boolean to check if we need or not the electric heater
        need_electric_heater[j].u = need_heat[j].y and need_elec_to_air[j].y;
        // Compute electric power needed to get blowing temperature instruction
        power_air_elec[j] = smooth(1, if need_electric_heater[j].y then (heater_elec_power[j] * pid_heater_elec[j].y) else 0);
    end for;

      connect(T1, need_solar_DHW.reference) annotation (Line(
          points={{-1488,410},{-1440,410},{-1440,419.6},{-1397.2,419.6}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T1, need_solar_Storage.reference) annotation (Line(
          points={{-1488,410},{-1440,410},{-1440,401.6},{-1397.2,401.6}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T3, enough_power_in_DHW.u) annotation (Line(
          points={{-1488,380},{-1397.2,380}},
          color={0,0,127},
          smooth=Smooth.None));

      connect(T3, max_DHW_tank.u) annotation (Line(
          points={{-1488,380},{-1440,380},{-1440,360},{-1340,360},{-1340,394.4},{-1279.2,
              394.4}},
          color={0,0,127},
          smooth=Smooth.None));

      connect(T5, max_Storage_tank.u) annotation (Line(
          points={{-1488,324},{-1386,324},{-1386,338},{-1279,338}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T4, pip_DHW_elec.u_m) annotation (Line(
          points={{-1488,352},{-1458,352},{-1458,280},{-1409,280},{-1409,285}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(integrator_DHW_elec.y, energy_DHW_elec) annotation (Line(
          points={{-1379.4,290},{-1354,290},{-1354,278},{-1330,278}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Sj_state_timer.y, Sj_state_temp) annotation (Line(
          points={{-1331.2,230},{-1308,230}},
          color={255,0,255},
          smooth=Smooth.None));
      connect(S5_state_timer.y, S5_state) annotation (Line(
          points={{-1331.2,200},{-1308,200}},
          color={255,0,255},
          smooth=Smooth.None));
      connect(T5, enough_energy_storage.u) annotation (Line(
          points={{-1488,324},{-1440,324},{-1440,188},{-1384,188},{-1384,200},{-1380,200}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Texch_out, pid_Sj.u_m) annotation (Line(
          points={{-1480,170},{-1290,170},{-1290,310.4},{-1168,310.4}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Texch_out, need_elec_to_air.u) annotation (Line(
          points={{-1480,170},{-1290,170},{-1290,292},{-1165,292}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Tsouff, pid_heater_elec.u_m) annotation (Line(
          points={{-1480,210},{-1466,210},{-1466,170},{-1040,170},{-1040,306.4}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(integrator_heating_elec.y, energy_air_elec) annotation (Line(
          points={{-997.2,302},{-962,302}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(extent={{-1480,120},{-940,460}},
              preserveAspectRatio=false), graphics={
            Rectangle(
              extent={{-1060,340},{-976,290}},
              lineColor={0,0,255},
              fillColor={255,85,85},
              fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{-1180,342},{-1096,286}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),
            Rectangle(
              extent={{-1420,260},{-1324,180}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Rectangle(
              extent={{-1420,438},{-1344,372}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={85,170,255}),              Text(
              extent={{-1420,436},{-1344,430}},
              lineColor={0,0,255},
              textString="Control of the solar valve (V3V)"),
                                                    Rectangle(
              extent={{-1300,440},{-1224,388}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={255,170,85}),              Text(
              extent={{-1300,438},{-1224,432}},
              lineColor={0,0,255},
              textString="Control of S5 (DHW tank pump)"),
            Rectangle(
              extent={{-1300,380},{-1224,334}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1300,376},{-1224,370}},
              lineColor={0,0,255},
              textString="Control of S6 (Storage tank pump)"),
            Rectangle(
              extent={{-1420,320},{-1344,270}},
              lineColor={0,0,255},
              fillColor={255,85,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1420,318},{-1344,312}},
              lineColor={0,0,255},
              textString="Control of DHW electrical power"),
                                                    Text(
              extent={{-1410,258},{-1334,252}},
              lineColor={0,0,255},
              textString="Control of Sj pumps (emitters)"),
            Rectangle(
              extent={{-1280,256},{-1206,184}},
              lineColor={0,0,255},
              fillColor={255,170,85},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1278,252},{-1202,246}},
              lineColor={0,0,255},
              textString="Control of pumps activation
(Sj, S6, S5)"),
            Rectangle(
              extent={{-1160,260},{-1086,194}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1162,258},{-1086,252}},
              lineColor={0,0,255},
              textString="Pid controller for S5 and S6"),
            Rectangle(
              extent={{-1180,440},{-1094,380}},
              lineColor={0,0,255},
              fillColor={215,215,215},
              fillPattern=FillPattern.Solid),       Text(
              extent={{-1176,436},{-1100,430}},
              lineColor={0,0,255},
              textString="Control of blowing flow and temperature"),
                                                    Text(
              extent={{-1178,340},{-1102,334}},
              lineColor={0,0,255},
              textString="PID controller for Sj (each room)"),
                                                    Text(
              extent={{-1058,338},{-982,332}},
              lineColor={0,0,255},
              textString="Control of electric air blowing heating")}),
                                                     Icon(coordinateSystem(extent={{-1480,
                120},{-940,460}},
                       preserveAspectRatio=false), graphics={
            Rectangle(
              extent={{-1480,460},{-940,120}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={255,85,85}),               Text(
              extent={{-1430,396},{-978,324}},
              lineColor={0,0,0},
              textString="Control of solar system
(pumps, valves, setpoint, ...)"),                   Text(
              extent={{-1444,242},{-972,162}},
              lineColor={0,0,0},
              textString="Control of air blowing system
(fans, setpoints, ...)")}),
        Documentation(info="<html>
<h4>Mise &agrave; jour: 2016-01-06</h4>
<p>Un sc&eacute;nario de consigne/consigne solaire diff&eacute;rent peut &ecirc;tre utilis&eacute; pour chaque pi&egrave;ce.</p>
<h4><span style=\"color:#008000\">Control of the solar valve (V3V)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of S5 (DHW tank pump)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of S6 (pump for Storage tank exchanger)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of the electrical extra heating for DHW</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control if we need heat or overheat</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of pump temporization and modulation</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of system pumps speed (S5 and S6)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of blowing instruction (temperature and fan volumic flow rate)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of Electrical heater for heating demand</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color:#008000\">Control of solar pump for each water/air exchanger (one fo each room)</span></h4>
<p>Fonctionnel</p>
</html>"));
    end Equipements_control_smooth_enhanced_old;
  end Backup;

  package Examples "Tests and examples"
    extends Modelica.Icons.Package;
    model test_control "Test new control behavior"

      parameter Integer n=2
        "Number of room to control (must be greater than 0)";

      Backup.Equipements_control_smooth equipements_control(
        n=2,
        tempo_algo_flowRate={20,30},
        tempo_algo_elec={10,20},
        table={[0,273.15 + 20,273.15 + 22,90/3600; 100,273.15 + 20,273.15 + 22,
            90/3600; 300,273.15 + 10,273.15 + 22,20/3600; 500,273.15 + 10,
            273.15 + 22,20/3600; 800,273.15 + 20,273.15 + 22,20/3600; 1000,
            273.15 + 20,273.15 + 22,90/3600],[0,273.15 + 20,273.15 + 22,90/3600;
            100,273.15 + 20,273.15 + 22,90/3600; 300,273.15 + 10,273.15 + 22,20
            /3600; 500,273.15 + 10,273.15 + 22,20/3600; 800,273.15 + 20,273.15
             + 22,20/3600; 1000,273.15 + 20,273.15 + 22,90/3600]})
        annotation (Placement(transformation(extent={{2,0},{62,34}})));
      Modelica.Blocks.Sources.Ramp T5_mod(
        height=75,
        duration=800,
        offset=273.15 + 25) "Allow to show ECS interactions"
        annotation (Placement(transformation(extent={{-100,-46},{-80,-26}})));
      Modelica.Blocks.Sources.Ramp T4_mod(
        offset=273.15 + 25,
        duration=500,
        height=1000) "Allow to show ECS interactions"
        annotation (Placement(transformation(extent={{-100,-14},{-80,6}})));
      Modelica.Blocks.Sources.Sine T1_mod(
        freqHz=1/250,
        amplitude=60,
        phase=1.5707963267949,
        offset=273.15 + 60)
        annotation (Placement(transformation(extent={{-100,60},{-80,80}})));
      Modelica.Blocks.Sources.Ramp T3_mod(
        offset=273.15 + 20,
        duration=800,
        height=30) "Allow to show ECS interactions"
        annotation (Placement(transformation(extent={{-100,20},{-80,40}})));
      Modelica.Blocks.Sources.Sine Tamb[n](
        each freqHz=1/500,
        each amplitude=25,
        each offset=273.15 + 10,
        each phase=1.5707963267949) annotation (Placement(transformation(extent={{-100,-120},{-80,-100}})));
      Modelica.Blocks.Sources.Ramp T7_mod(
        height=75,
        duration=800,
        offset=273.15 + 25) annotation (Placement(transformation(extent={{-100,-80},{-80,-60}})));
      Modelica.Blocks.Sources.RealExpression Tsoufflage[n](y=fill(273.15 + 30, n))
        annotation (Placement(transformation(extent={{-32,-14},{-16,2}})));
      Modelica.Blocks.Sources.RealExpression Text(y=273.15 + 10)
        annotation (Placement(transformation(extent={{-32,-26},{-16,-10}})));
      Modelica.Blocks.Sources.Ramp Texchanger[n](
        each duration=800,
        each height=20,
        each offset=273.15 + 20) annotation (Placement(transformation(extent={{-32,-52},{-12,-32}})));
    equation
      connect(T4_mod.y, equipements_control.T4) annotation (Line(
          points={{-79,-4},{-56,-4},{-56,20},{2.22222,20}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T5_mod.y, equipements_control.T5) annotation (Line(
          points={{-79,-36},{-46,-36},{-46,24},{2.22222,24}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T3_mod.y, equipements_control.T3) annotation (Line(
          points={{-79,30},{-64,30},{-64,28},{2.22222,28}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T1_mod.y, equipements_control.T1) annotation (Line(
          points={{-79,70},{-64,70},{-64,32},{2.22222,32}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T7_mod.y, equipements_control.T7) annotation (Line(
          points={{-79,-70},{-40,-70},{-40,16},{2.22222,16}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Text.y, equipements_control.Text) annotation (Line(
          points={{-15.2,-18},{-12,-18},{-12,12},{2.22222,12}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Tamb.y, equipements_control.Tamb) annotation (Line(
          points={{-79,-110},{-34,-110},{-34,8},{1.77778,8}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Tsoufflage.y, equipements_control.Tsouff) annotation (Line(
          points={{-15.2,-6},{-6,-6},{-6,1.4},{1.77778,1.4}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Texchanger.y, equipements_control.Texch_out) annotation (Line(
          points={{-11,-42},{-4,-42},{-4,4.6},{1.77778,4.6}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-120},{100,100}}),
                            graphics),
        Icon(coordinateSystem(extent={{-100,-120},{100,100}})),
        experiment(StopTime=1000),
        __Dymola_experimentSetupOutput);
    end test_control;

    model test_Equipments_control_smooth_enhanced "Test new control behavior"

      parameter Integer n=2
        "Number of room to control (must be greater than 0)";

      Backup.Equipements_control_smooth_enhanced_old equipements_control(
        n=2,
        tempo_algo_flowRate={20,30},
        tempo_algo_elec={10,20},
        table={[0,273.15 + 20,273.15 + 22,90/3600; 100,273.15 + 20,273.15 + 22,
            90/3600; 300,273.15 + 10,273.15 + 22,20/3600; 500,273.15 + 10,
            273.15 + 22,20/3600; 800,273.15 + 20,273.15 + 22,20/3600; 1000,
            273.15 + 20,273.15 + 22,90/3600],[0,273.15 + 20,273.15 + 22,90/3600;
            100,273.15 + 20,273.15 + 22,90/3600; 300,273.15 + 10,273.15 + 22,20
            /3600; 500,273.15 + 10,273.15 + 22,20/3600; 800,273.15 + 20,273.15
             + 22,20/3600; 1000,273.15 + 20,273.15 + 22,90/3600]})
        annotation (Placement(transformation(extent={{2,0},{62,34}})));
      Modelica.Blocks.Sources.Ramp T5_mod(
        height=75,
        duration=800,
        offset=273.15 + 25) "Allow to show ECS interactions"
        annotation (Placement(transformation(extent={{-100,-46},{-80,-26}})));
      Modelica.Blocks.Sources.Ramp T4_mod(
        offset=273.15 + 25,
        duration=500,
        height=1000) "Allow to show ECS interactions"
        annotation (Placement(transformation(extent={{-100,-14},{-80,6}})));
      Modelica.Blocks.Sources.Sine T1_mod(
        freqHz=1/250,
        amplitude=60,
        phase=1.5707963267949,
        offset=273.15 + 60)
        annotation (Placement(transformation(extent={{-100,60},{-80,80}})));
      Modelica.Blocks.Sources.Ramp T3_mod(
        offset=273.15 + 20,
        duration=800,
        height=30) "Allow to show ECS interactions"
        annotation (Placement(transformation(extent={{-100,20},{-80,40}})));
      Modelica.Blocks.Sources.Sine Tamb[n](
        each freqHz=1/500,
        each amplitude=25,
        each offset=273.15 + 10,
        each phase=1.5707963267949) annotation (Placement(transformation(extent={{-100,-120},{-80,-100}})));
      Modelica.Blocks.Sources.Ramp T7_mod(
        height=75,
        duration=800,
        offset=273.15 + 25) annotation (Placement(transformation(extent={{-100,-80},{-80,-60}})));
      Modelica.Blocks.Sources.RealExpression Tsoufflage[n](y=fill(273.15 + 30, n))
        annotation (Placement(transformation(extent={{-32,-14},{-16,2}})));
      Modelica.Blocks.Sources.RealExpression Text(y=273.15 + 10)
        annotation (Placement(transformation(extent={{-32,-26},{-16,-10}})));
      Modelica.Blocks.Sources.Ramp Texchanger[n](
        each duration=800,
        each height=20,
        each offset=273.15 + 20) annotation (Placement(transformation(extent={{-32,-52},{-12,-32}})));
    equation
      connect(T4_mod.y, equipements_control.T4) annotation (Line(
          points={{-79,-4},{-56,-4},{-56,20},{2.22222,20}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T5_mod.y, equipements_control.T5) annotation (Line(
          points={{-79,-36},{-46,-36},{-46,24},{2.22222,24}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T3_mod.y, equipements_control.T3) annotation (Line(
          points={{-79,30},{-64,30},{-64,28},{2.22222,28}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T1_mod.y, equipements_control.T1) annotation (Line(
          points={{-79,70},{-64,70},{-64,32},{2.22222,32}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(T7_mod.y, equipements_control.T7) annotation (Line(
          points={{-79,-70},{-40,-70},{-40,16},{2.22222,16}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Text.y, equipements_control.Text) annotation (Line(
          points={{-15.2,-18},{-12,-18},{-12,12},{2.22222,12}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Tamb.y, equipements_control.Tamb) annotation (Line(
          points={{-79,-110},{-34,-110},{-34,8},{1.77778,8}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Tsoufflage.y, equipements_control.Tsouff) annotation (Line(
          points={{-15.2,-6},{-6,-6},{-6,1.4},{1.77778,1.4}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(Texchanger.y, equipements_control.Texch_out) annotation (Line(
          points={{-11,-42},{-4,-42},{-4,4.6},{1.77778,4.6}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-120},{100,100}}),
                            graphics),
        Icon(coordinateSystem(extent={{-100,-120},{100,100}})),
        experiment(StopTime=1000),
        __Dymola_experimentSetupOutput);
    end test_Equipments_control_smooth_enhanced;
  end Examples;

  model Equipements_control_smooth_enhanced
    "Describe the control algorithm for pumps S6 and S5; V3V cot� solaire; Electrical power pull inside the DHW tank."
    import SI = Modelica.SIunits;

    extends Data.PID_parameters;

  //
  // Control solar bandwidth
  //
  parameter Real solar_delta_min=10 "Minimal temperature difference to activate direct solar energy (shared among all pumps)";

  //
  // General inputs
  //
    Modelica.Blocks.Interfaces.RealInput T1(unit="K")
      "Temperature inside collector"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-20,-20},{20,20}},
          origin={-1488,410}),
          iconTransformation(extent={{-18,-18},{18,18}},
          origin={-1478,440})));
    Modelica.Blocks.Interfaces.RealInput T3(unit="K")
      "Temperature inside DHW tank next to solar exchanger (bottom)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,360},{-1468,400}}), iconTransformation(
            extent={{-1496,382},{-1460,418}})));
    Modelica.Blocks.Interfaces.RealInput T4(unit="K")
      "Temperature inside DHW tank next to drawing up (top)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,332},{-1468,372}}), iconTransformation(
            extent={{-1496,302},{-1460,338}})));
    Modelica.Blocks.Interfaces.RealInput T5(unit="K")
      "Temperature inside solar tank (top)"
       annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1508,304},{-1468,344}}), iconTransformation(
            extent={{-1496,342},{-1460,378}})));
    Modelica.Blocks.Interfaces.RealInput T7(unit="K")
      "Temperature of water after terminal emitter (only one for all rooms)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1506,264},{-1466,304}}), iconTransformation(
            extent={{-1496,262},{-1460,298}})));
    Modelica.Blocks.Interfaces.RealVectorInput Tamb[n](each unit="K")
      "Vector of temperature inside rooms areas"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1500,228},{-1460,268}}), iconTransformation(
            extent={{-1496,186},{-1468,214}})));
    Modelica.Blocks.Interfaces.RealVectorInput Texch_out[n](each unit="K")
      "Vector of air temperature just after exchanger"      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(extent={{-1500,150},{-1460,190}}),
                                                      iconTransformation(extent={{-1496,152},{-1468,
              180}})));
    Modelica.Blocks.Interfaces.RealVectorInput Tsouff[n](each unit="K")
      "Vector of air temperature blowing in each room"   annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(extent={{-1500,190},{-1460,230}}),
                                                      iconTransformation(extent={{-1496,120},{-1468,
              148}})));
    Modelica.Blocks.Interfaces.RealInput Text(unit="K")
      "Temperature outside the room (environnement)"
                                              annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(
          extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-1486,132}),
                            iconTransformation(extent={{-18,-18},{18,18}},
          rotation=0,
          origin={-1478,240})));

  //
  // Describe the control of the solar valve (V3V)
  //
    Modelica.Blocks.Logical.OnOffController need_solar_DHW(pre_y_start=true, bandwidth=2)
      "Return True if solar must be used and False if solar must not be used"
      annotation (Placement(transformation(extent={{-1396,410},{-1384,422}})));

    Modelica.Blocks.Logical.OnOffController need_solar_Storage(pre_y_start=true,
        bandwidth=2)
      "Return True if solar must be used to load the storage tank"
      annotation (Placement(transformation(extent={{-1396,392},{-1384,404}})));
    Modelica.Blocks.Logical.Hysteresis enough_power_in_DHW(uLow=temp_min_DHW - 2, uHigh=
          temp_min_DHW)
      "Test if we have enough power inside the DHW before authorize temperature inside the storage tank"
      annotation (Placement(transformation(extent={{-1396,374},{-1384,386}})));
     Modelica.Blocks.Interfaces.BooleanOutput V3V_solar
      "State of the solar valve" annotation (__Dymola_tag={"Valve"}, Placement(transformation(
            extent={{-1344,398},{-1316,426}}), iconTransformation(extent={{-14,-14},
              {14,14}},
          rotation=90,
          origin={-1140,474})));

  //
  // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
  //
    parameter SI.ThermodynamicTemperature temp_max_DHW = 363.15
      "Maximum admissible value for DHW tank" annotation (Dialog(group="Higher limit"));
    Boolean S5_state_temp
      "Temporary state of the S5 pump, see Sj control part to get final state";
    Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T3(
      y(start=false),
      uLow=4,
      uHigh=solar_delta_min)
                "Check if T1 - T3 is larger enough"
      annotation (Placement(transformation(extent={{-1278,412},{-1266,424}})));
    Modelica.Blocks.Logical.OnOffController max_DHW_tank(bandwidth=5, pre_y_start=true)
      "Test if the temperature inside the tank is lower than reference"
      annotation (Placement(transformation(extent={{-1278,392},{-1266,404}})));

  //
  // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
  //
    parameter SI.ThermodynamicTemperature temp_max_storage = 378.15
      "Maximum admissible value for Storage tank" annotation (Dialog(group="Higher limit"));
    parameter SI.ThermodynamicTemperature temp_min_DHW = 303.15
      "Minimum admissible value for DHW tank before allowing Storage tank loading" annotation (Dialog(group="Lower limit"));
    Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T5(
      y(start=false),
      uLow=2.5,
      uHigh=solar_delta_min)
                "Check if T1 - T5 is larger enough"
      annotation (Placement(transformation(extent={{-1278,352},{-1268,362}})));
    Modelica.Blocks.Logical.OnOffController max_Storage_tank(bandwidth=5)
      "Test if the temperature inside the tank is lower than reference"
      annotation (Placement(transformation(extent={{-1278,336},{-1268,346}})));
     Modelica.Blocks.Interfaces.BooleanOutput S6_state
      "State of the solar pump S6." annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(
            extent={{-1224,342},{-1196,370}}), iconTransformation(extent={{-14,-14},
              {14,14}},
          rotation=-90,
          origin={-1140,106})));

  //
  // Describe the control of the electrical extra heating for DHW
  //
    parameter SI.Power DHW_elec_power = 6000
      "Maximum power of the extra electrical heater for DHW";
    parameter SI.ThermodynamicTemperature DHW_setpoint = 313.15
      "Temperature setpoint for DHW" annotation (Dialog(group="Instructions"));
    Buildings.Controls.Continuous.LimPID pip_DHW_elec(
      controllerType=elecDHW_controllerType,
      k=elecDHW_k,
      Ti=elecDHW_Ti,
      Td=elecDHW_Td,
      yMax=elecDHW_yMax,
      yMin=elecDHW_yMin,
      reverseAction=elecDHW_reverseAction)
      "Controller for electrical heat to DHW"
      annotation (Placement(transformation(extent={{-1414,286},{-1404,296}})));
    Modelica.Blocks.Continuous.Integrator integrator_DHW_elec
      "Integration of the power used to keep domestic hot water at the setpoint temperature"
      annotation (Placement(transformation(extent={{-1392,284},{-1380,296}})));
    Modelica.Blocks.Interfaces.RealOutput power_DHW_elec( unit="W")
      "Output the power needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-1344,294},{-1316,322}}), iconTransformation(extent={{-940,216},
              {-916,240}})));
    Modelica.Blocks.Interfaces.RealOutput energy_DHW_elec( unit="J")
      "Output the cumulated energy needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-1344,264},{-1316,292}}), iconTransformation(extent={{-940,192},
              {-916,216}})));

  //
  // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
  //
    parameter SI.ThermodynamicTemperature temp_min_collector = 313.15
      "Minimum collector temperature in order to use direct solar energy" annotation (Dialog(group="Lower limit"));
    parameter SI.ThermodynamicTemperature temp_min_storage = 318.15
      "Minimum storage tank temperature in order to use indirect solar energy" annotation (Dialog(group="Lower limit"));
    Boolean direct_solar_state[n]
      "Vector of Sn pump state on direct solar energy (one for each room)";
    Boolean indirect_solar_state[n]
      "Vector of Sn pump state on indirect solar energy (one for each room)";
    Modelica.Blocks.Logical.OnOffController need_overheat[n](each bandwidth=1)
      annotation (Placement(transformation(extent={{-1410,190},{-1390,210}})));
    Modelica.Blocks.Logical.OnOffController need_heat[n](each bandwidth=0.5)
      annotation (Placement(transformation(extent={{-1410,220},{-1390,240}})));
    Modelica.Blocks.Logical.OnOffController enough_energy_collector(bandwidth=
          solar_delta_min/2)
      "We can use direct solar energy"
      annotation (Placement(transformation(extent={{-1376,220},{-1356,240}})));
    Modelica.Blocks.Logical.Hysteresis enough_energy_storage(uLow=
          temp_min_storage - 5, uHigh=temp_min_storage)
      annotation (Placement(transformation(extent={{-1378,190},{-1358,210}})));
    Utilities.Timers.OnDelay_pause
                             Sj_state_timer[n](each delayTime=60, y(each
          start=false)) "Return the current state of Sj pump (vector of state)"
      annotation (Placement(transformation(extent={{-1348,222},{-1332,238}})));
    Utilities.Timers.OnDelay_pause
                             S5_state_timer(each delayTime=60, y(each start=false))
      "Return the current state of S5 pump."
      annotation (Placement(transformation(extent={{-1348,192},{-1332,208}})));
  protected
    Modelica.Blocks.Interfaces.BooleanOutput Sj_state_temp[n]
      "Control Sj pump behavior" annotation (Placement(transformation(extent={{-1324,
              214},{-1292,246}}), iconTransformation(extent={{-1300,232},{-1272,260}})));
  public
    Modelica.Blocks.Interfaces.BooleanOutput S5_state "Control S5 state"
      annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(extent={{-1324,184},{-1292,216}}), iconTransformation(
            extent={{-13,-13},{13,13}},
          rotation=-90,
          origin={-1113,107})));

  //
  // Describe how to compute temporization and flow modulation for Sj, S6, S5
  //
       Utilities.Timers.Wait_for_tempo_crack temporization_Sj(n=n) if n > 1
      "Used to add a temporization to Sj pumps. Avoid all pump to start at full speed at the same time and consuming all energy"
      annotation (Placement(transformation(extent={{-1266,216},{-1222,240}})));

    Modelica.Blocks.Interfaces.BooleanOutput Sj_state[n]
      "Final Sj pumps states after temporization check" annotation (__Dymola_tag={"Pump", "state"}, Placement(
          transformation(extent={{-1206,206},{-1178,234}}), iconTransformation(
          extent={{-14,-14},{14,14}},
          rotation=-90,
          origin={-1168,106})));
  //
  // Describe pid pump output construction for S6 and S5
  //
    parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm S6_speed_max=3000
      "Maximum value for S6 speed"
      annotation (Dialog(tab="S6"));

    parameter Modelica.SIunits.Conversions.NonSIunits.AngularVelocity_rpm S5_speed_max=3000
      "Maximum value for S5 speed"
      annotation (Dialog(tab="S5"));
    Buildings.Controls.Continuous.LimPID pid_S6(
      controllerType=S6_controller,
      k=S6_k,
      Ti=S6_Ti,
      Td=S6_Td,
      wp=S6_wp,
      wd=S6_wd,
      yMax=S6_yMax,
      yMin=S6_yMin,
      reverseAction=true)
      annotation (Placement(transformation(extent={{-1118,226},{-1098,246}})));
    Buildings.Controls.Continuous.LimPID pid_S5(
      controllerType=S5_controller,
      k=S5_k,
      Ti=S5_Ti,
      Td=S5_Td,
      wp=S5_wp,
      wd=S5_wd,
      yMax=S5_yMax,
      yMin=S5_yMin,
      reverseAction=true)
      annotation (Placement(transformation(extent={{-1148,200},{-1128,220}})));

    Modelica.Blocks.Interfaces.RealOutput S5_speed( unit="1/min")
      "Final speed of S5 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
        Placement(transformation(extent={{-1086,226},{-1058,254}}), iconTransformation(extent={{-940,
              280},{-914,306}})));
    Modelica.Blocks.Interfaces.RealOutput S6_speed( unit="1/min")
      "Final speed of S6 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
        Placement(transformation(extent={{-1086,194},{-1058,222}}), iconTransformation(extent={{-940,
              306},{-914,332}})));

  //
  //Control of blowing flow rate/temperature
  //
    parameter SI.ThermodynamicTemperature Tsouff_max[n]= fill(273.15 + 32, n)
      "Maximum value of blowing temperature for each room"
      annotation (Dialog(tab="Blowing Temperatures"));
    parameter SI.Time tempo_algo_flowRate[n]= fill(600, n)
      "How much time we have to wait before switching between Temperature or flow regulation"
      annotation (Dialog(group="Instructions"));
    parameter Real fan_flowRate_max[n]=fill(900/3600, n)
      "Upper limit of fan flow rate." annotation (Dialog(tab="Fans"));
    Boolean need_limit_flowRate[n]
      "Boolean used to check if we need or not to reduce the blowing flow rate";
    Real temperature_instruction_souff[n]
      "Computed temperature instruction [K]";
    Modelica.Blocks.Interfaces.RealOutput minimal_flowRate[n](each unit="m3/s")
      "Minimal sanitary volumic flow rate [m3/s]"                        annotation (Placement(
          transformation(extent={{-1094,412},{-1074,432}}), iconTransformation(extent={{-940,
              408},{-916,432}})));
    Modelica.Blocks.Interfaces.RealOutput fan_flowRate[n](each unit="m3/s")
      "Computed fan flow rate [m3/s]"                                              annotation (
        Placement(transformation(extent={{-1094,390},{-1074,410}}), iconTransformation(extent={{-940,
              436},{-916,460}})));
    Modelica.Blocks.Logical.Hysteresis need_higher_flow[n](uLow=Tsouff_max - fill(1.1, size(
          Tsouff_max, 1)), uHigh=Tsouff_max - fill(0.1, size(Tsouff_max, 1)))
      "Test if we need a higher flow rate to cover the heating demand"
      annotation (Placement(transformation(extent={{-1172,412},{-1158,426}})));
    Utilities.Timers.OnDelay_risingEdge
                              need_increase_flowRate[n](delayTime=tempo_algo_flowRate)
      "Time to wait before increasing blowing flow rate"
      annotation (Placement(transformation(extent={{-1144,412},{-1130,426}})));

    Modelica.Blocks.Logical.OnOffController flow_is_minimal[n](each bandwidth=10/3600)
      annotation (Placement(transformation(extent={{-1172,386},{-1158,400}})));
    Buildings.Controls.Continuous.LimPID pid_fan[n](
      controllerType=fan_controllerType,
      k=fan_k,
      Ti=fan_Ti,
      Td=fan_Td,
      wp=fan_wp,
      wd=fan_wd,
      each yMax=1,
      each yMin=0) "PID used to control blowing flow rate for each room"
                  annotation (Placement(transformation(extent={{-1114,388},{
              -1102,400}})));
     Buildings.Controls.Continuous.LimPID pid_Tsouff[n](
      controllerType=Tsouff_controllerType,
      k=Tsouff_k,
      Ti=Tsouff_Ti,
      Td=Tsouff_Td,
      yMax=Tsouff_yMax,
      yMin=Tsouff_yMin,
      reverseAction=Tsouff_reverseAction)
      "PID which control the blowing temperature instruction"
      annotation (Placement(transformation(extent={{-1116,414},{-1104,426}})));

  //
  //Control of Sj pumps speed
  //
    parameter SI.Time tempo_algo_elec[n]= fill(180, n)
      "How much time we have to wait before using electric heater"
      annotation (Dialog(group="Instructions"));
      parameter Real Sj_speed_max[n]= fill(3000, n)
      "Maximum value for Sj speed of each pump"
      annotation (Dialog(tab="Sj"));
     Buildings.Controls.Continuous.LimPID pid_Sj[n](
      controllerType=Sj_controllerType,
      k=Sj_k,
      Ti=Sj_Ti,
      Td=Sj_Td,
      yMax=Sj_yMax,
      yMin=Sj_yMin,
      reverseAction=Sj_reverseAction)
      "Compute correct factor in order to modulate Sj speed"
      annotation (Placement(transformation(extent={{-1176,312},{-1160,328}})));

    Utilities.Timers.OnDelay_risingEdge
                              need_electric_heater[n](delayTime=tempo_algo_elec)
      "Check if we still need electric heater after `wait_for` time."
      annotation (Placement(transformation(extent={{-1138,290},{-1124,304}})));
    Modelica.Blocks.Logical.OnOffController need_elec_to_air[n](each bandwidth=2)
      "Check if solar energy gives enough power to the blowing air source"
      annotation (Placement(transformation(extent={{-1164,290},{-1154,300}})));
    Modelica.Blocks.Interfaces.RealOutput Sj_speed[n](each unit="1/min")
      "Output of each pump Sj speed"
      annotation (__Dymola_tag={"Pump", "speed"}, Placement(transformation(extent={{-1096,308},{-1076,328}}),
          iconTransformation(extent={{-940,332},{-914,358}})));

  //
  //Control of blowing electric heater
  //
    parameter Modelica.SIunits.Power heater_elec_power[n]=fill(5000, n)
      "Maximum power of the extra electrical heater for air blowing heating";
      Buildings.Controls.Continuous.LimPID pid_heater_elec[n](
        controllerType=elecHeat_controllerType,
        k=elecHeat_k,
        Ti=elecHeat_Ti,
        Td=elecHeat_Td,
        yMax=elecHeat_yMax,
        yMin=elecHeat_yMin)
      "Controller for electric heater on blowing air for each room"
        annotation (Placement(transformation(extent={{-1048,308},{-1032,324}})));

    Modelica.Blocks.Continuous.Integrator integrator_heating_elec[n]
      "Integration of the power used to keep room air at the setpoint temperature"
      annotation (Placement(transformation(extent={{-1014,294},{-998,310}})));
    Modelica.Blocks.Interfaces.RealOutput power_air_elec[n](each unit="W")
      "Output the power needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-976,316},{-948,344}}),   iconTransformation(extent={{-940,144},
              {-916,168}})));
    Modelica.Blocks.Interfaces.RealOutput energy_air_elec[n](each unit="J")
      "Output the cumulated energy needed to keep room air temperature at setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-976,288},{-948,316}}),   iconTransformation(extent={{-940,120},
              {-916,144}})));
     Modelica.Blocks.Sources.CombiTimeTable             Schedules[n](
      tableOnFile=tableOnFile,
      table=table,
      tableName=tableName,
      fileName=fileName,
      verboseRead=verboseRead,
      columns=columns,
      each smoothness=smoothness,
      each extrapolation=extrapolation,
      offset=offset,
      startTime=startTime)
      "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
       annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(extent={{-1480,428},{-1448,460}})));

  equation
  //
  // Describe the control of the solar valve (V3V)
  //
    //Connect minimal between T4 and T3 for test with T1 (`+1` used to shift bandwidth upper)
    need_solar_DHW.u = Buildings.Utilities.Math.Functions.smoothMin(x1=T4,
                                                                    x2=T3,
                                                                    deltaX=0.1)+1
      "[T1 >= min(T3, T4)]";
    need_solar_Storage.u = T5+1 "[T1 >= T5";
    // V3V open to solar collector only if we have
    V3V_solar = (enough_power_in_DHW.y and need_solar_Storage.y) or need_solar_DHW.y
      "[T1 >= T5 and T3 >= 32�C] or [T1 >= min(T3, T4)]";

  //
  // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
  //
    enoughDelta_T1_T3.u = T1 - T3 "[T1 - T3 >= 10]";
    max_DHW_tank.reference = temp_max_DHW "[T3 < temp_max_DHW]";
    // Compute S5 pump state according to temperature difference
    S5_state_temp = V3V_solar and max_DHW_tank.y and enoughDelta_T1_T3.y
      "[T1 - T3 >= 10 and [T3 > temp_max_DHW] and V3V_solar]";

  //
  // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
  //
    enoughDelta_T1_T5.u = T1 - T5 "[T1 - T5 >= 10]";
    max_Storage_tank.reference = temp_max_storage-2.5 "[T5 > temp_max_DHW]";
    // Compute S6 pump state
    S6_state = enoughDelta_T1_T5.y and enough_power_in_DHW.y and max_Storage_tank.y
      "[T1 - T5 >= 10 and min(T3, T4) >= 30 and T5 > temp_max_DHW]";

  //
  // Describe the control of the electrical extra heating for DHW
  //
    pip_DHW_elec.u_s = DHW_setpoint "Set DHW temperature setpoint for PID";
    power_DHW_elec = DHW_elec_power * pip_DHW_elec.y "Power compute using PID";
    integrator_DHW_elec.u = power_DHW_elec
      "Same thing, only here to be used outside the model";

  //
  // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
  //
  // Test temperature difference between T1 and max(T7 or minimal value to use direct solar)
    enough_energy_collector.u = Buildings.Utilities.Math.Functions.smoothMax(x1=temp_min_collector,
                                                                             x2=T7,
                                                                             deltaX=0.1);
    // Connect T1 to solar_direct bloc OnOffController
    enough_energy_collector.reference = T1 - (enough_energy_collector.bandwidth / 2)
      "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";
    // S5 pump state according to heating demande control
    S5_state_timer.u = S5_state_temp
      "State of S5 pump according to heating demands";

  //
  // Describe how to compute temporization and flow modulation for Sj, S6, S5
  //

    // Connection of Sj pumps to temporization if at least 2 pumps else directly to Sj_final_state
    if n > 1 then
      connect(Sj_state_temp, temporization_Sj.in_value);
      connect(temporization_Sj.out_value, Sj_state);
    else
      Sj_state = Sj_state_temp;
    end if;

  //
  // Describe pid pump output construction for S6 and S5
  //
    // Connection to S6 pid
    pid_S6.u_s = solar_delta_min;
    pid_S6.u_m = T1 - T5;
    // Only need to compute output if pump S5 is On
    S5_speed = smooth(1, (if S5_state then (S5_speed_max * pid_S5.y) else 0));

    // Connection to S5 pid
    pid_S5.u_s = solar_delta_min;
    pid_S5.u_m = T1 - T3;
    // Only need to compute output if pump S5 is On
    S6_speed = smooth(1, (if S6_state then (S6_speed_max * pid_S6.y) else 0));

  //
  //Control of Sj pumps speed and Control of blowing electric heater
  //
    // Connection to pids
    pid_Sj.u_s = temperature_instruction_souff;
    pid_heater_elec.u_s = temperature_instruction_souff;
    need_elec_to_air.reference = temperature_instruction_souff - (need_elec_to_air.bandwidth / 2)
      "Shift down reference temperature because exchanger outlet temperature cannot be higher than maximal blowing temperature value";
    integrator_heating_elec.u = power_air_elec
      "Connect power computed to integrator";

  //
  // Loop only once over each room
  //

  for j in 1:n loop
    //
    // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
    //
      // Overheat connections
      need_overheat[j].reference = Schedules[j].y[2];
      need_overheat[j].u = Tamb[j] - (need_overheat[j].bandwidth / 2)
        "bandwidth used to shift down minimal value";

      // Heat connection
      need_heat[j].reference = Schedules[j].y[1];
      need_heat[j].u = Tamb[j] - (need_heat[j].bandwidth / 2)
        "bandwidth used to shift down minimal value";

      // We need to activate the Sj pump using direct solar energy from collectors
      direct_solar_state[j] = V3V_solar and enough_energy_collector.y and (need_heat[j].y or need_overheat[j].y);

      // We need to activate the Sj pump using indirect solar energy from tank
      indirect_solar_state[j] = (not V3V_solar) and enough_energy_storage.y and need_heat[j].y;

      // Final state of the pump for the room j
      Sj_state_timer[j].u = indirect_solar_state[j] or direct_solar_state[j];
    //
    //Control of blowing flow rate/temperature
    //
      //Minimal flow rate reference
      flow_is_minimal[j].reference = Schedules[j].y[3] * 1.5;
      flow_is_minimal[j].u = fan_flowRate[j]
        "RECURSIVE call to computed volumic flow rate";
      need_higher_flow[j].u = temperature_instruction_souff[j]
        "RECURSIVE call to computed temperature instruction";
      // Check if we need to increase blowing flow rate
      need_increase_flowRate[j].u = need_heat[j].y and need_higher_flow[j].y;
      // Check if we need to limit the blowing flow rate or not
      need_limit_flowRate[j] = not need_increase_flowRate[j].y;

    //
    // BLOWING VOLUMIC FLOW RATE COMPUTATION
    //
      // PID for fans volumic flow rate (one for each room)
      pid_fan[j].u_s = Schedules[j].y[1] "Reference";
      pid_fan[j].u_m = Tamb[j] "Temperature from room air";
      // Compute volumic flow rate as max(minimal flow rate, (maximal flow rate time PID factor)
      fan_flowRate[j] = Buildings.Utilities.Math.Functions.smoothMax(x1=fan_flowRate_max[j] * (if pre(need_limit_flowRate[j]) then 0 else pid_fan[j].y),
                                                                     x2=Schedules[j].y[3],
                                                                     deltaX=0.001);
      minimal_flowRate[j] = Schedules[j].y[3]
        "Just used as getter for external uses";
    //
    // BLOWING TEMPERATURE INSTRUCTION COMPUTATION
    //
      // PID for temperature of blowing instruction (one for each room)
      pid_Tsouff[j].u_s = smooth(1, if ((not need_heat[j].y) and need_overheat[j].y) then Schedules[j].y[2]
                                else Schedules[j].y[1])
        "Reference depend if we need overheating or just heating";
      pid_Tsouff[j].u_m = Tamb[j] "Temperature from room air";
      // Compute the blowing temperature.
      // We first check the difference between max blowing temperature and environment temperature
      // Then we apply the PID factor to it.
      // So we compute blowing temperature as Text + PID factor * delta(max - ext)
      temperature_instruction_souff[j] = smooth(1, if pre(need_limit_flowRate[j]) then (Text + pid_Tsouff[j].y * (Tsouff_max[j] - Text)) else Tsouff_max[j]);
    //
    //Control of Sj pumps speed and Control of blowing electric heater
    //
      // Compute speed for each pump (for each room) Sj
      Sj_speed[j] = smooth(1, if Sj_state[j] then (Sj_speed_max[j] * pid_Sj[j].y) else 0);
      // Compute boolean to check if we need or not the electric heater
      need_electric_heater[j].u = need_heat[j].y and need_elec_to_air[j].y;
      // Compute electric power needed to get blowing temperature instruction
      power_air_elec[j] = smooth(1, if need_electric_heater[j].y then (heater_elec_power[j] * pid_heater_elec[j].y) else 0);
  end for;

    connect(T1, need_solar_DHW.reference) annotation (Line(
        points={{-1488,410},{-1440,410},{-1440,419.6},{-1397.2,419.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T1, need_solar_Storage.reference) annotation (Line(
        points={{-1488,410},{-1440,410},{-1440,401.6},{-1397.2,401.6}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T3, enough_power_in_DHW.u) annotation (Line(
        points={{-1488,380},{-1397.2,380}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T3, max_DHW_tank.u) annotation (Line(
        points={{-1488,380},{-1440,380},{-1440,360},{-1340,360},{-1340,394.4},{-1279.2,
            394.4}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(T5, max_Storage_tank.u) annotation (Line(
        points={{-1488,324},{-1386,324},{-1386,338},{-1279,338}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(T4, pip_DHW_elec.u_m) annotation (Line(
        points={{-1488,352},{-1458,352},{-1458,280},{-1409,280},{-1409,285}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator_DHW_elec.y, energy_DHW_elec) annotation (Line(
        points={{-1379.4,290},{-1354,290},{-1354,278},{-1330,278}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Sj_state_timer.y, Sj_state_temp) annotation (Line(
        points={{-1331.2,230},{-1308,230}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(S5_state_timer.y, S5_state) annotation (Line(
        points={{-1331.2,200},{-1308,200}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(T5, enough_energy_storage.u) annotation (Line(
        points={{-1488,324},{-1440,324},{-1440,188},{-1384,188},{-1384,200},{-1380,200}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Texch_out, pid_Sj.u_m) annotation (Line(
        points={{-1480,170},{-1290,170},{-1290,310.4},{-1168,310.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Texch_out, need_elec_to_air.u) annotation (Line(
        points={{-1480,170},{-1290,170},{-1290,292},{-1165,292}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tsouff, pid_heater_elec.u_m) annotation (Line(
        points={{-1480,210},{-1466,210},{-1466,170},{-1040,170},{-1040,306.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator_heating_elec.y, energy_air_elec) annotation (Line(
        points={{-997.2,302},{-962,302}},
        color={0,0,127},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-1480,120},{-940,460}},
            preserveAspectRatio=false), graphics={
          Rectangle(
            extent={{-1060,340},{-976,290}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-1180,342},{-1096,286}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),
          Rectangle(
            extent={{-1420,260},{-1324,180}},
            lineColor={0,0,255},
            fillColor={255,170,85},
            fillPattern=FillPattern.Solid),       Rectangle(
            extent={{-1420,438},{-1344,372}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={85,170,255}),              Text(
            extent={{-1420,436},{-1344,430}},
            lineColor={0,0,255},
            textString="Control of the solar valve (V3V)"),
                                                  Rectangle(
            extent={{-1300,440},{-1224,388}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={255,170,85}),              Text(
            extent={{-1300,438},{-1224,432}},
            lineColor={0,0,255},
            textString="Control of S5 (DHW tank pump)"),
          Rectangle(
            extent={{-1300,380},{-1224,334}},
            lineColor={0,0,255},
            fillColor={255,170,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1300,376},{-1224,370}},
            lineColor={0,0,255},
            textString="Control of S6 (Storage tank pump)"),
          Rectangle(
            extent={{-1420,320},{-1344,270}},
            lineColor={0,0,255},
            fillColor={255,85,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1420,318},{-1344,312}},
            lineColor={0,0,255},
            textString="Control of DHW electrical power"),
                                                  Text(
            extent={{-1410,258},{-1334,252}},
            lineColor={0,0,255},
            textString="Control of Sj pumps (emitters)"),
          Rectangle(
            extent={{-1280,256},{-1206,184}},
            lineColor={0,0,255},
            fillColor={255,170,85},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1278,252},{-1202,246}},
            lineColor={0,0,255},
            textString="Control of pumps activation
(Sj, S6, S5)"),
          Rectangle(
            extent={{-1160,260},{-1086,194}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1162,258},{-1086,252}},
            lineColor={0,0,255},
            textString="Pid controller for S5 and S6"),
          Rectangle(
            extent={{-1180,440},{-1094,380}},
            lineColor={0,0,255},
            fillColor={215,215,215},
            fillPattern=FillPattern.Solid),       Text(
            extent={{-1176,436},{-1100,430}},
            lineColor={0,0,255},
            textString="Control of blowing flow and temperature"),
                                                  Text(
            extent={{-1178,340},{-1102,334}},
            lineColor={0,0,255},
            textString="PID controller for Sj (each room)"),
                                                  Text(
            extent={{-1058,338},{-982,332}},
            lineColor={0,0,255},
            textString="Control of electric air blowing heating")}),
                                                   Icon(coordinateSystem(extent={{-1480,
              120},{-940,460}},
                     preserveAspectRatio=false), graphics={
          Rectangle(
            extent={{-1480,460},{-940,120}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={255,85,85}),               Text(
            extent={{-1430,396},{-978,324}},
            lineColor={0,0,0},
            textString="Control of solar system
(pumps, valves, setpoint, ...)"),                 Text(
            extent={{-1444,242},{-972,162}},
            lineColor={0,0,0},
            textString="Control of air blowing system
(fans, setpoints, ...)")}),
      Documentation(info="<html>
<h4><span style=\"color: #00aa00\">Mise � jour: 2016-01-06</span></h4>
<p>Un sc&eacute;nario de consigne/consigne solaire diff&eacute;rent peut &ecirc;tre utilis&eacute; pour chaque pi&egrave;ce.</p>
<p><br><br><b><span style=\"color: #00aa00;\">Mise &agrave; jour: 2016-06-22</span></b></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">La pompe S5 ne se d&eacute;clenche plus lorsque le chauffage solaire passe en mode chauffage solaire direct (`direct_solar_state`)</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">La modulation du d&eacute;bit de soufflage est maintenant correctement pris en compte (suppression des contradictions entre sont activation et d&eacute;sactivation)</span></p>
<p><br>La modulation du d&eacute;bit &agrave; 50&percnt; a &eacute;t&eacute; vir&eacute; car le PID le fait tr&egrave;s bien tout seul. On conserve le smooth entre pid *max (state=True) et 0 (state=False)</p>
<p><br><b><span style=\"color: #00aa00;\">Mise &agrave; jour: 2016-07-01</span></b></p><p><br>Add a parameter to tweak the minimal temperature difference to turn on direct solar energy for S5, S6, and S2.</p>
<p><br><br><h4><span style=\"color: #008000\">Control of the solar valve (V3V)</span></h4></p>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of S5 (DHW tank pump)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of S6 (pump for Storage tank exchanger)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of the electrical extra heating for DHW</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control if we need heat or overheat</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of pump temporization and modulation</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of system pumps speed (S5 and S6)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of blowing instruction (temperature and fan volumic flow rate)</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of Electrical heater for heating demand</span></h4>
<p>Fonctionnel</p>
<h4><span style=\"color: #008000\">Control of solar pump for each water/air exchanger (one fo each room)</span></h4>
<p>Fonctionnel</p>
</html>"));
  end Equipements_control_smooth_enhanced;

end Electrical_control;
