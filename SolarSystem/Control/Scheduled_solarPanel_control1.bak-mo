within SolarSystem.Control;
model Scheduled_solarPanel_control1
  "Use input schedules and deltaT to manage solar panel flow rate"

parameter Integer n=16 "Schedules' lenght"
annotation(Dialog(connectorSizing=true));
parameter Integer matrixSchedule[2, n] = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21; 0, 0, 0, 0, 0, 0, 0, 30, 30, 30, 30, 30, 0, 0, 0, 0]
    "Schedule time (hour, minute) (matrixSchedule columns must be equal to matrixFlowValue rows)";
parameter Real matrixFlowValue[n] = {0.1, 0.1, 0.1, 0.12, 0.15, 0.18, 0.2, 0.25, 0.3, 0.25, 0.2, 0.18, 0.17, 0.15, 0.1, 0.1}
    "Scheduled flow (matrixFlowValue rows must be equal to matrixSchedule columns) ";

parameter Real minGlobalRadiation = 80
    "Minimal radiation to feed solar panel with water [W/m2]";

parameter Real hysteresisLow = 0
    "Minimal deltaT (T_out - T_in) value to turn off the pump";

parameter Real hysteresisHight = 4
    "Minimal deltaT (T_out - T_in) value to turn on the pump";

parameter Real flowElse = 0 "Solar flow out of schedule";

  Horloge            clock(Heure_debut=0)
    "\"Cut time into hour, minute, seconde\""
    annotation (Placement(transformation(extent={{-400,190},{-334,262}})));
  Modelica.Blocks.Interfaces.RealInput T_in "T before object"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=0,
        origin={-400,-30}), iconTransformation(extent={{-33,-33},{33,33}},
          origin={-367,47})));
  Modelica.Blocks.Interfaces.RealInput T_out "T after object"
    annotation (Placement(transformation(extent={{-20,-20},{20,20}},
        rotation=0,
        origin={-400,-70}), iconTransformation(extent={{-32,-32},{32,32}},
          origin={-368,-52})));
  Modelica.Blocks.Logical.Hysteresis enoughCold(uLow=hysteresisLow, uHigh=
        hysteresisHight,
    y(start=false))
    annotation (Placement(transformation(extent={{-320,-60},{-300,-40}})));
  Modelica.Blocks.Math.MultiSum deltaPanel(k={-1,1}, nu=2)
    annotation (Placement(transformation(extent={{-358,-60},{-338,-40}})));

  Modelica.Blocks.Logical.LessThreshold lessiH_mini(threshold=
        matrixSchedule[1, 1])
    annotation (Placement(transformation(extent={{-174,298},{-152,320}})));
  Modelica.Blocks.Logical.GreaterThreshold greaterMin_mini(threshold=
        matrixSchedule[2, 1])
    annotation (Placement(transformation(extent={{-200,260},{-180,280}})));
  Modelica.Blocks.Logical.LogicalSwitch beforeSchedules
    annotation (Placement(transformation(extent={{-122,256},{-94,284}})));
  Modelica.Blocks.Logical.LessEqualThreshold lessEqualH_mini(threshold=
        matrixSchedule[1, 1])
    annotation (Placement(transformation(extent={{-172,220},{-152,240}})));
  Modelica.Blocks.Logical.GreaterThreshold greaterH_max(threshold=
        matrixSchedule[1, n])
    annotation (Placement(transformation(extent={{-172,180},{-152,200}})));
  Modelica.Blocks.Logical.LessThreshold greaterMin_max(threshold=
        matrixSchedule[2, n])
    annotation (Placement(transformation(extent={{-200,140},{-180,160}})));
  Modelica.Blocks.Logical.LogicalSwitch afterSchedules
    annotation (Placement(transformation(extent={{-122,136},{-94,164}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualH_max(threshold=
       matrixSchedule[1, n])
    annotation (Placement(transformation(extent={{-172,100},{-152,120}})));
  Modelica.Blocks.Logical.Or beforeOrAfter "False if outside schedules"
    annotation (Placement(transformation(extent={{-56,196},{-28,224}})));
  Modelica.Blocks.Logical.Not insideSchedule "false becomes true "
    annotation (Placement(transformation(extent={{12,192},{48,228}})));
  Modelica.Blocks.Logical.And allowFlow
    "( enoughSun or EnoughCold ) and Inside schedule"
    annotation (Placement(transformation(extent={{70,86},{108,124}})));
  Modelica.Blocks.Math.IntegerToReal integerToReal "int(H) ----> real(H)"
    annotation (Placement(transformation(extent={{-300,240},{-278,262}})));
  Modelica.Blocks.Math.IntegerToReal integerToReal1
    "int(min) ---> float(min)"
    annotation (Placement(transformation(extent={{-300,198},{-276,222}})));
  Modelica.Blocks.Interfaces.RealOutput flowRate(start=0) "[Kg/s]"
    annotation (Placement(transformation(extent={{142,68},{214,140}}),
        iconTransformation(extent={{118,44},{214,140}})));

  Modelica.Blocks.Logical.Hysteresis stopCondition(
    y(start=false),
    uLow=273.15 + 90,
    uHigh=273.15 + 110)
    annotation (Placement(transformation(extent={{-194,-88},{-158,-52}})));
  Modelica.Blocks.Logical.And conditions
    "( enoughSun or EnoughCold ) and Inside schedule"
    annotation (Placement(transformation(extent={{10,-34},{48,4}})));
  Modelica.Blocks.Logical.Not NotStopCondition "false becomes true "
    annotation (Placement(transformation(extent={{-126,-88},{-90,-52}})));
algorithm
  for i in 1:size(matrixFlowValue, 1) loop

    // Flow rate inside the object is allowed
    if allowFlow.y then

      // We reach a schedule time ---> adjust the flow rate
      if clock.H == matrixSchedule[1,i] and clock.min == matrixSchedule[2,i] then
         flowRate :=matrixFlowValue[i];

      // Keep actual flow rate until we reach an other schedule time
      else
        flowRate :=flowRate;
      end if;

    // Out of schedule or (no enough sun and no enough cold) ---> default flow rate
    else
       flowRate :=flowElse;
    end if;

  end for;

equation
  connect(deltaPanel.y, enoughCold.u) annotation (Line(
      points={{-336.3,-50},{-322,-50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T_in, deltaPanel.u[1]) annotation (Line(
      points={{-400,-30},{-380,-30},{-380,-46.5},{-358,-46.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T_out, deltaPanel.u[2]) annotation (Line(
      points={{-400,-70},{-378,-70},{-378,-53.5},{-358,-53.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(greaterMin_mini.y, beforeSchedules.u2) annotation (Line(
      points={{-179,270},{-124.8,270}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(lessiH_mini.y, beforeSchedules.u1)  annotation (Line(
      points={{-150.9,309},{-132,309},{-132,281.2},{-124.8,281.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(lessEqualH_mini.y, beforeSchedules.u3)  annotation (Line(
      points={{-151,230},{-132,230},{-132,258.8},{-124.8,258.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(greaterMin_max.y, afterSchedules.u2)    annotation (Line(
      points={{-179,150},{-124.8,150}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(greaterH_max.y, afterSchedules.u1)   annotation (Line(
      points={{-151,190},{-132,190},{-132,161.2},{-124.8,161.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(greaterEqualH_max.y, afterSchedules.u3)   annotation (Line(
      points={{-151,110},{-132,110},{-132,138.8},{-124.8,138.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(beforeSchedules.y, beforeOrAfter.u1)
                                   annotation (Line(
      points={{-92.6,270},{-84,270},{-84,210},{-58.8,210}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(afterSchedules.y, beforeOrAfter.u2)
                                    annotation (Line(
      points={{-92.6,150},{-84,150},{-84,198.8},{-58.8,198.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(beforeOrAfter.y, insideSchedule.u)
                         annotation (Line(
      points={{-26.6,210},{8.4,210}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(insideSchedule.y, allowFlow.u1)
                           annotation (Line(
      points={{49.8,210},{58,210},{58,105},{66.2,105}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(clock.H, integerToReal.u) annotation (Line(
      points={{-340.6,244},{-320,244},{-320,251},{-302.2,251}},
      color={255,127,0},
      smooth=Smooth.None));
  connect(clock.min, integerToReal1.u) annotation (Line(
      points={{-340.6,229.6},{-319.3,229.6},{-319.3,210},{-302.4,210}},
      color={255,127,0},
      smooth=Smooth.None));

  connect(integerToReal1.y, greaterMin_mini.u)   annotation (Line(
      points={{-274.8,210},{-238,210},{-238,270},{-202,270}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(integerToReal.y, lessEqualH_mini.u)    annotation (Line(
      points={{-276.9,251},{-254,251},{-254,230},{-174,230}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(integerToReal1.y, greaterMin_max.u)    annotation (Line(
      points={{-274.8,210},{-238,210},{-238,150},{-202,150}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(integerToReal.y, greaterH_max.u)   annotation (Line(
      points={{-276.9,251},{-254,251},{-254,190},{-174,190}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(integerToReal.y, greaterEqualH_max.u)   annotation (Line(
      points={{-276.9,251},{-254,251},{-254,110},{-174,110}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(integerToReal.y, lessiH_mini.u)    annotation (Line(
      points={{-276.9,251},{-254,251},{-254,309},{-176.2,309}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T_out, stopCondition.u) annotation (Line(
      points={{-400,-70},{-197.6,-70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(conditions.y, allowFlow.u2) annotation (Line(
      points={{49.9,-15},{58,-15},{58,89.8},{66.2,89.8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(stopCondition.y, NotStopCondition.u) annotation (Line(
      points={{-156.2,-70},{-129.6,-70}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(NotStopCondition.y, conditions.u2) annotation (Line(
      points={{-88.2,-70},{-56,-70},{-56,-30.2},{6.2,-30.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(enoughCold.y, conditions.u1) annotation (Line(
      points={{-299,-50},{-216,-50},{-216,-15},{6.2,-15}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-400,-120},{180,320}},
          preserveAspectRatio=true), graphics), Icon(coordinateSystem(extent={{-400,
            -120},{180,320}}, preserveAspectRatio=true), graphics));
end Scheduled_solarPanel_control1;
