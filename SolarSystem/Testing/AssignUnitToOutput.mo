within SolarSystem.Testing;
model AssignUnitToOutput
  "How to assign a specific unit for internal and display."

  Modelica.Blocks.Routing.RealPassThrough heating_setpoint[2](each y(
      final quantity="ThermodynamicTemperature",
      final unit="K",
      displayUnit="degC",
      min=0)) "Heat temperature setpoint for each room"
    annotation (Placement(transformation(extent={{-24,14},{-4,34}})));
  Modelica.Blocks.Sources.Sine dumb_data[2](
    each freqHz=1/250,
    each amplitude=60,
    each offset=273.15 + 60,
    each phase=1.5707963267949)
    annotation (Placement(transformation(extent={{-86,16},{-66,36}})));
  Modelica.Blocks.Routing.RealPassThrough heating_setpoint_sans_unit[2]
    "Heat temperature setpoint for each room"
    annotation (Placement(transformation(extent={{-22,-20},{-2,0}})));
equation

  connect(dumb_data.y, heating_setpoint.u)
    annotation (Line(points={{-65,26},{-46,26},{-46,24},{-26,24}}, color={0,0,127}));
  connect(dumb_data.y, heating_setpoint_sans_unit.u) annotation (Line(points={{-65,26},{-44,26},
          {-44,-10},{-24,-10}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(coordinateSystem(
          preserveAspectRatio=false)));
end AssignUnitToOutput;
