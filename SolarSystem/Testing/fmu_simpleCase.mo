within SolarSystem.Testing;
model fmu_simpleCase "Test of an FMU implementation inside Dymola."
  Modelica.Blocks.Sources.RealExpression Consigne(y=273.15 + 20)
    annotation (Placement(transformation(extent={{-88,-8},{-72,8}})));
  Buildings.Controls.Continuous.LimPID pid_controller(controllerType=Modelica.Blocks.Types.SimpleController.PI,
      strict=false)
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
  _fmu_export_schedule_fmu _fmu_export_schedule_fmu1(fmi_StopTime=604800,
      fmi_CommunicationStepSize=360)
    annotation (Placement(transformation(extent={{0,-14},{34,14}})));
equation
  connect(Consigne.y, pid_controller.u_s)
    annotation (Line(points={{-71.2,0},{-56,0},{-42,0}}, color={0,0,127}));
  connect(pid_controller.y, _fmu_export_schedule_fmu1.Q)
    annotation (Line(points={{-19,0},{-0.68,0}}, color={0,0,127}));
  connect(_fmu_export_schedule_fmu1.TRooMea, pid_controller.u_m) annotation (
      Line(points={{37.4,0},{60,0},{60,-20},{-30,-20},{-30,-12}}, color={0,0,
          127}));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{
            100,100}})),
    experiment(StopTime=604800, __Dymola_NumberOfIntervals=1680),
    __Dymola_experimentSetupOutput(doublePrecision=true, events=false));
end fmu_simpleCase;
