within SolarSystem.Testing;
model test_schedules
  parameter Integer n=1;

  parameter Boolean tableOnFile[n]=fill(false, n)
    "= true, if table is defined on file or in function usertab"
    annotation (Dialog(tab="Schedules", group="Table data definition"));
     parameter Real table[:, :, :] = fill(fill(0.0, 0, 3), n)
    "Setpoint for the algorithm control (first=time). 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s])"
       annotation (Dialog(tab="Schedules",group="Table data definition",enable=not tableOnFile));
  parameter String tableName[n]=fill("NoName", n)
    "Table name on file or in function usertab (see docu)"
    annotation (Dialog(tab="Schedules",group="Table data definition",enable=tableOnFile));
  parameter String fileName[n]=fill("NoName", n) "File where matrix is stored"
    annotation (Dialog(tab="Schedules",
      group="Table data definition",
      enable=tableOnFile,
      loadSelector(filter="Text files (*.txt);;MATLAB MAT-files (*.mat)",
          caption="Open file in which table is present")));
  parameter Boolean verboseRead[n]=fill(true, n)
    "= true, if info message that file is loading is to be printed"
    annotation (Dialog(tab="Schedules",group="Table data definition",enable=tableOnFile));
   parameter Integer columns[:, :]=fill(2:3+1, n)
    "Columns of table to be interpolated"
     annotation (Dialog(tab="Schedules",group="Table data interpretation"));
  parameter Modelica.Blocks.Types.Smoothness smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments
    "Smoothness of table interpolation"
    annotation (Dialog(tab="Schedules",group="Table data interpretation"));
  parameter Modelica.Blocks.Types.Extrapolation extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic
    "Extrapolation of data outside the definition range"
    annotation (Dialog(tab="Schedules",group="Table data interpretation"));
   parameter Real offset[:, :]=fill(fill(0, 3), n) "Offsets of output signals"
     annotation (Dialog(tab="Schedules",group="Table data interpretation"));
  parameter Modelica.SIunits.Time startTime[n]=fill(0, n)
    "Output = offset for time < startTime"
    annotation (Dialog(tab="Schedules",group="Table data interpretation"));

   Modelica.Blocks.Sources.CombiTimeTable             Schedules[n](
    tableOnFile=tableOnFile,
    tableName=tableName,
    fileName=fileName,
    verboseRead=verboseRead,
    each smoothness=smoothness,
    each extrapolation=extrapolation,
    startTime=startTime,
    table=table,
    columns=columns,
    offset=offset)
    "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
     annotation (Placement(transformation(extent={{-100,68},{-68,100}})));
   Modelica.Blocks.Sources.CombiTimeTable Schedules_ref(
    table=[0,273.15 + 20,273.15 + 22,90/3600; 100,273.15 + 20,273.15 + 22,90/3600; 300,273.15 + 10,273.15
         + 22,20/3600; 500,273.15 + 10,273.15 + 22,20/3600; 800,273.15 + 20,273.15 + 22,20/3600; 1000,
        273.15 + 20,273.15 + 22,90/3600],
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    columns=2:size(Schedules_ref.table, 2))
    "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
    annotation (Placement(transformation(extent={{-98,-16},{-66,16}})));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
        graphics), Documentation(info="<html>
<h4>Used to find how to setup a general representation for a multiple instance combi table.</h4>
</html>"));
end test_schedules;
