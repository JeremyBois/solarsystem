within SolarSystem.Testing;
model valve_test


public
  package MediumE = SolarSystem.Media.WaterGlycol
    "Medium water model with glycol";
  package MediumDHW = Buildings.Media.Water "Medium water model";

  // Pump initialization
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal[2] = {40/3600,
                                                               70/3600}
    "Mass flow rate for left pumps [1] and right pumps [2] [l/h] to [kg/s]";

  // This relation is only True if we have 4 * dp_solaire_other + dp_V3V_solar
  // on each possible flow path for S5 and S6.
  parameter Real kv_solar = (m_flow_nominal[1] * 3.6) / (((4 * dp_solaire_other + dp_V3V_solar) / 100000)^0.5)
  "Kv factor for solar pumps (S5 and S6)";
  // Solar mass flow rate [kg/s] en [m3/s] ---> m_flow_solar / 1000
  parameter Modelica.SIunits.MassFlowRate m_flow_solar[5] = linspace(0.1, 1, 5) * m_flow_nominal[1]
  "Mass flow rate for solar pumps (S5 and S6)";
  parameter Real dp_solaire[5] = (m_flow_solar .* m_flow_solar) * (3.6*3.6*100000) / (kv_solar^2)
  "Element wise multiplication (Q[i]^2 / factor[i] for i in range(1, 11)";
  // Heat mass flow rate [kg/s] to [m3/s] ---> m_flow_solar / (3600*1000)
  parameter Modelica.SIunits.MassFlowRate m_flow_heat[5] = linspace(0.1, 1, 5) * m_flow_nominal[2]
  "Mass flow rate for heating pumps (S2)";
  // This relation is only True if we have 3 * dp_solaire_other + dp_V3V_solar (without losses inside branche with S2)
  // on each possible flow path for S2 (pressure losses in the heat branch == dp_other * 2)
  parameter Modelica.SIunits.Pressure dp_heat_other = (((m_flow_nominal[2] * 3.6)^2 / kv_solar^2) * 100000 -
                                                       (3 * dp_solaire_other + dp_V3V_solar)) / 2
    "Pressure difference for other component in the heat branch (must be added twice inside the S2 branch)";

  parameter Real dp_heat[5] = (m_flow_heat .* m_flow_heat) * (3.6*3.6*100000) / (kv_solar^2)
  "Element wise multiplication (Q[i]^2 / factor[i] for i in range(1, 11)";


protected
  parameter Modelica.SIunits.Pressure dp_V3V_solar = 10000
    "Pressure difference V3V solar (when opened)";
  parameter Modelica.SIunits.Pressure dp_solaire_other = 5000
    "Pressure difference for other component";



public
  Buildings.Fluid.Sources.Boundary_pT sou(
    redeclare package Medium = MediumE,
    nPorts=1) "Fluid source"
              annotation (Placement(transformation(extent={{-100,-2},{-80,18}})));
  Buildings.Fluid.Sources.Boundary_pT sin(redeclare package Medium = MediumE,
      nPorts=1) "Fluid sink"
    annotation (Placement(transformation(extent={{100,0},{80,20}})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM
                                      res(
    redeclare package Medium = MediumE,
    m_flow_nominal=m_flow_solar[1],
    dp_nominal=(4*dp_solaire_other + dp_V3V_solar))
    "Pressure drop component for avoiding singular system"
    annotation (Placement(transformation(extent={{20,-2},{40,18}})));
protected
  parameter Data.Parameter.Pump.SolisArt.C6_5_SolisConfort             C6_5(
    motorCooledByFluid=true,
    hydraulicEfficiency(V_flow=m_flow_solar/1000, eta=fill(1, 5)),
    motorEfficiency(V_flow=m_flow_solar/1000, eta=fill(1, 5)),
    pressure(V_flow=m_flow_solar/1000, dp=Modelica.Math.Vectors.reverse(
          dp_solaire)))
    annotation (Placement(transformation(extent={{-80,64},{-60,84}})));
public
  Buildings.Fluid.Movers.SpeedControlled_Nrpm
                                            C6(
    redeclare package Medium =
        MediumE,
    per=C6_5)       annotation (__Dymola_tag={"Pump", "flow"}, Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-28,8})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=3000)
    annotation (Placement(transformation(extent={{-62,34},{-42,54}})));
equation
  connect(C6.port_b, res.port_a)
    annotation (Line(points={{-18,8},{2,8},{20,8}}, color={0,127,255}));
  connect(res.port_b, sin.ports[1])
    annotation (Line(points={{40,8},{80,8},{80,10}}, color={0,127,255}));
  connect(sou.ports[1], C6.port_a)
    annotation (Line(points={{-80,8},{-60,8},{-38,8}}, color={0,127,255}));
  connect(realExpression.y, C6.Nrpm) annotation (Line(points={{-41,44},{-32,44},
          {-28,44},{-28,20}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end valve_test;
