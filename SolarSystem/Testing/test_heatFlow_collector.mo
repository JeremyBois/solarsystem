within SolarSystem.Testing;
model test_heatFlow_collector
  Modelica.Thermal.HeatTransfer.Sensors.HeatFlowSensor heatFlowSensor
    annotation (Placement(transformation(
        extent={{12,-12},{-12,12}},
        rotation=-90,
        origin={0,0})));
  Modelica.Thermal.HeatTransfer.Components.ThermalCollector Losses_collector2(m=
       5) "Collect all losses from tanks to the house."
    annotation (Placement(transformation(extent={{-10,22},{10,42}})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary_inverse(
    nPorts=1,
    redeclare package Medium = Buildings.Media.Water,
    m_flow=1,
    T=323.15)
    annotation (Placement(transformation(extent={{-100,50},{-80,70}})));
  Buildings.Fluid.Sources.Boundary_pT boundary_inverse2(
    nPorts=1,
    redeclare package Medium = Buildings.Media.Water,
    T=323.15) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={92,60})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{80,-60},{100,-40}})));
public
  Buildings.Fluid.FixedResistances.Pipe inverse(
    useMultipleHeatPorts=true,
    dp(start=0),
    redeclare package Medium = Buildings.Media.Water,
    nSeg=5,
    thicknessIns(displayUnit="mm") = 0.001,
    lambdaIns=1,
    length=5,
    m_flow_nominal=0.01)
    annotation (Placement(transformation(extent={{-8,54},{8,66}})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary(
    nPorts=1,
    redeclare package Medium = Buildings.Media.Water,
    m_flow=1,
    T=323.15)
    annotation (Placement(transformation(extent={{-100,-250},{-80,-230}})));
  Buildings.Fluid.Sources.Boundary_pT boundary2(     nPorts=1,
    redeclare package Medium = Buildings.Media.Water,
    T=323.15)                                                  annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={92,-240})));
public
  Buildings.Fluid.FixedResistances.Pipe normal(
    useMultipleHeatPorts=true,
    dp(start=0),
    redeclare package Medium = Buildings.Media.Water,
    nSeg=5,
    thicknessIns(displayUnit="mm") = 0.001,
    lambdaIns=1,
    length=5,
    m_flow_nominal=0.01)
    annotation (Placement(transformation(extent={{-8,-234},{8,-246}})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature Text_multi[5]
    annotation (Placement(transformation(
        extent={{12,-12},{-12,12}},
        rotation=180,
        origin={-28,-80})));
  Modelica.Blocks.Continuous.Integrator integrator_normal(k=-1)
    annotation (Placement(transformation(extent={{76,-172},{100,-148}})));
  Modelica.Thermal.HeatTransfer.Sensors.HeatFlowSensor heatFlowSensor_multi[5]
    annotation (Placement(transformation(
        extent={{-12,12},{12,-12}},
        rotation=-90,
        origin={0,-160})));
  Modelica.Blocks.Math.Sum sum_flow(nin=5)
    annotation (Placement(transformation(extent={{36,-172},{60,-148}})));
public
  Modelica.Blocks.Sources.Sine Temp_imposed[5](
    freqHz=1/500,
    phase=0,
    amplitude=30,
    offset=273.15 + 40)
    annotation (Placement(transformation(extent={{-102,-90},{-82,-70}})));
  Utilities.Other.Integrator_losses integrator_inverse
    annotation (Placement(transformation(extent={{-58,-14},{-100,14}})));
equation
  connect(boundary_inverse.ports[1], inverse.port_a) annotation (Line(
      points={{-80,60},{-8,60}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(inverse.port_b, boundary_inverse2.ports[1]) annotation (Line(
      points={{8,60},{82,60}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(inverse.heatPorts, Losses_collector2.port_a) annotation (Line(
      points={{0,57},{0,42}},
      color={127,0,0},
      smooth=Smooth.None));
  connect(Losses_collector2.port_b, heatFlowSensor.port_b) annotation (Line(
      points={{0,22},{0,12},{2.22045e-015,12}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Text_multi.port, heatFlowSensor_multi.port_a) annotation (Line(
      points={{-16,-80},{0,-80},{0,-148}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(heatFlowSensor_multi.port_b, normal.heatPorts) annotation (Line(
      points={{0,-172},{0,-237}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(boundary.ports[1], normal.port_a) annotation (Line(
      points={{-80,-240},{-8,-240}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(normal.port_b, boundary2.ports[1]) annotation (Line(
      points={{8,-240},{82,-240}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(heatFlowSensor_multi.Q_flow, sum_flow.u) annotation (Line(
      points={{12,-160},{33.6,-160}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sum_flow.y, integrator_normal.u) annotation (Line(
      points={{61.2,-160},{73.6,-160}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Temp_imposed.y, Text_multi.T) annotation (Line(
      points={{-81,-80},{-42.4,-80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text_multi[1].port, heatFlowSensor.port_a) annotation (Line(
      points={{-16,-80},{0,-80},{0,-12},{-2.22045e-015,-12}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(heatFlowSensor.Q_flow, integrator_inverse.Heat_flow) annotation (Line(
      points={{-12,2.22045e-015},{-36,2.22045e-015},{-36,0},{-61.15,0}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-260},{
            100,80}}), graphics),
    Icon(coordinateSystem(extent={{-100,-260},{100,80}})),
    Documentation(info="<html>
<p>This model is used to test how you can use the heatflow collector and how we can integrate the flow rate.</p>
<p>In this example we integrate losses and gains (negative impact on the integrator)</p>
</html>"));
end test_heatFlow_collector;
