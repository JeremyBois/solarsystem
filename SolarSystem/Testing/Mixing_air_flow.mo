within SolarSystem.Testing;
model Mixing_air_flow

  package MediumA =
      Buildings.Media.Air "Medium model";

protected
  Data.Parameter.City_Datas.Bordeaux weather_file
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Weather(
      computeWetBulbTemperature=false, filNam=weather_file.path)
    "Weather datas"
    annotation (Placement(transformation(extent={{-100,40},{-80,60}})));
public
        Buildings.Fluid.Movers.FlowControlled_m_flow fan(redeclare package
      Medium =
        MediumA,
    allowFlowReversal=false,
    m_flow_nominal=900/3600*1.2,
    dynamicBalance=true,
    redeclare Buildings.Fluid.Movers.Data.FlowControlled per,
    tau=10,
    filteredSpeed=false)
    annotation (Placement(transformation(extent={{50,-10},{70,10}})));
protected
Buildings.Fluid.MixingVolumes.MixingVolume vol(          redeclare package
      Medium = MediumA,
    m_flow_nominal=1,
    V=2,
    prescribedHeatFlowRate=false,
    nPorts=3)           annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={28,10})));
protected
  Buildings.Fluid.Sources.Boundary_pT House(
    redeclare package Medium = MediumA,
    nPorts=2,
    T=293.15) "Source model for air infiltration"
    annotation (Placement(transformation(extent={{-50,-48},{-36,-34}})));
protected
  Buildings.Fluid.Sources.MassFlowSource_T
                                  outiside_mini(
                                               redeclare package Medium =
        MediumA,
    m_flow=0.1,
    use_T_in=true,
    nPorts=1) "Source model for air infiltration"
    annotation (Placement(transformation(extent={{-46,-6},{-34,6}})));
protected
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{-68,42},{-52,58}})));
public
  Modelica.Blocks.Sources.Sine flow_imposed(
    amplitude=1,
    offset=1.1,
    freqHz=1/500,
    phase=0) annotation (Placement(transformation(extent={{4,54},{24,74}})));
public
  Buildings.Fluid.Sensors.MassFlowRate variable_flow(redeclare package Medium =
        MediumA)
    annotation (Placement(transformation(extent={{-16,-46},{-4,-34}})));
  Buildings.Fluid.Sensors.MassFlowRate fixed_flow(redeclare package Medium =
        MediumA)
    annotation (Placement(transformation(extent={{-18,-6},{-6,6}})));
  Utilities.Timers.Wait_for_tempo TT(
    out_value(start={false,false,false}),
    n=3,
    wait_for={44,44})
    annotation (Placement(transformation(extent={{76,76},{100,100}})));
  Modelica.Blocks.Sources.BooleanExpression booleanExpression
    annotation (Placement(transformation(extent={{-38,82},{-18,102}})));
equation
  connect(Weather.weaBus, weaBus) annotation (Line(
      points={{-80,50},{-60,50}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(weaBus.TDryBul,outiside_mini. T_in) annotation (Line(
      points={{-60,50},{-52,50},{-52,2.4},{-47.2,2.4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(flow_imposed.y, fan.m_flow_in) annotation (Line(
      points={{25,64},{59.8,64},{59.8,12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(outiside_mini.ports[1], fixed_flow.port_a) annotation (Line(
      points={{-34,0},{-18,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(House.ports[1], variable_flow.port_a) annotation (Line(
      points={{-36,-39.6},{-26,-39.6},{-26,-40},{-16,-40}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(fixed_flow.port_b, vol.ports[1]) annotation (Line(
      points={{-6,0},{25.3333,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(variable_flow.port_b, vol.ports[2]) annotation (Line(
      points={{-4,-40},{28,-40},{28,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(vol.ports[3], fan.port_a) annotation (Line(
      points={{30.6667,0},{50,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(fan.port_b, House.ports[2]) annotation (Line(
      points={{70,0},{80,0},{80,-60},{-30,-60},{-30,-42.4},{-36,-42.4}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(flow_imposed.y, TT.in_value) annotation (Line(
      points={{25,64},{60,64},{60,88},{76,88}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(booleanExpression.y, TT.in_value[1]) annotation (Line(
      points={{-17,92},{30,92},{30,86.4},{76,86.4}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(booleanExpression.y, TT.in_value[2]) annotation (Line(
      points={{-17,92},{30,92},{30,88},{76,88}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(booleanExpression.y, TT.in_value[3]) annotation (Line(
      points={{-17,92},{28,92},{28,89.6},{76,89.6}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Mixing_air_flow;
