within SolarSystem.Testing;
model Test_drawing_up "Test new drawing up implementation"

  Buildings.Fluid.Sources.Boundary_pT boundary(          redeclare package
      Medium = SolarSystem.Models.Validation.Tank.MediumE,
    use_T_in=true,
    nPorts=1)                                              annotation (
      Placement(transformation(
        extent={{8,-7},{-8,7}},
        rotation=180,
        origin={-38,83})));
  Modelica.Blocks.Sources.Step Tb(
    startTime=10000,
    height=50,
    offset=11 + 273.15)
    annotation (Placement(transformation(extent={{-90,70},{-70,90}})));
  Modelica.Blocks.Sources.RealExpression Te(y=10 + 273.15)
    annotation (Placement(transformation(extent={{-80,-10},{-60,10}})));
  Valves.DrawingUp.Drawing_up_mflow drawing_up_mflow(redeclare package Medium =
        SolarSystem.Models.Validation.Tank.MediumE,
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table=[0*3600,50; 1*3600,0; 2*3600,1000; 3*3600,1; 4*3600,2000; 4*3600,2000])
    annotation (Placement(transformation(extent={{40,26},{74,62}})));
protected
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{80,80},{100,100}})));
equation
  connect(Tb.y, boundary.T_in) annotation (Line(
      points={{-69,80},{-70,80},{-70,80.2},{-47.6,80.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(boundary.ports[1], drawing_up_mflow.tank_port) annotation (Line(
      points={{-30,83},{6,83},{6,44.9},{41.7,44.9}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Te.y, drawing_up_mflow.Te) annotation (Line(
      points={{-59,0},{-8,0},{-8,30.5},{41.7,30.5}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end Test_drawing_up;
