within SolarSystem.Testing;
model test_occultation_switch

  Boolean test;

  Buildings.BoundaryConditions.WeatherData.BaseClasses.ConvertTime converter annotation (Placement(transformation(extent={{-80,20},
            {-60,40}})));
  Modelica.Blocks.Sources.CombiTimeTable occultations(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    table=[0,1,1,1; 3600,1,1,1; 7200,1,1,1; 10800,1,1,1; 14400,1,1,1; 18000,1,1,
        1; 21600,1,1,1; 25200,0,0,0; 28800,0,1,0.7; 32400,0,1,0.7; 36000,0,1,0.7;
        39600,0,1,0.7; 43200,0,1,0.7; 46800,0,0.7,1; 50400,0,0.7,1; 54000,0,0.7,
        1; 57600,0,0.7,1; 61200,0,0.7,1; 64800,0,0,0; 68400,0,0,0; 72000,0,0,0;
        75600,0,0,0; 79200,1,1,1; 82800,1,1,1; 86400,1,1,1; 90000,1,1,1; 93600,1,
        1,1; 97200,1,1,1; 100800,1,1,1; 104400,1,1,1; 108000,1,1,1; 111600,0,0,0;
        115200,0,1,0.7; 118800,0,1,0.7; 122400,0,1,0.7; 126000,0,1,0.7; 129600,0,
        1,0.7; 133200,0,0.7,1; 136800,0,0.7,1; 140400,0,0.7,1; 144000,0,0.7,1; 147600,
        0,0.7,1; 151200,0,0,0; 154800,0,0,0; 158400,0,0,0; 162000,0,0,0; 165600,
        1,1,1; 169200,1,1,1; 172800,1,1,1; 176400,1,1,1; 180000,1,1,1; 183600,1,
        1,1; 187200,1,1,1; 190800,1,1,1; 194400,1,1,1; 198000,0,0,0; 201600,0,1,
        0.7; 205200,0,1,0.7; 208800,0,1,0.7; 212400,0,1,0.7; 216000,0,1,0.7; 219600,
        0,0.7,1; 223200,0,0.7,1; 226800,0,0.7,1; 230400,0,0.7,1; 234000,0,0.7,1;
        237600,0,0,0; 241200,0,0,0; 244800,0,0,0; 248400,0,0,0; 252000,1,1,1; 255600,
        1,1,1; 259200,1,1,1; 262800,1,1,1; 266400,1,1,1; 270000,1,1,1; 273600,1,
        1,1; 277200,1,1,1; 280800,1,1,1; 284400,0,0,0; 288000,0,1,0.7; 291600,0,
        1,0.7; 295200,0,1,0.7; 298800,0,1,0.7; 302400,0,1,0.7; 306000,0,0.7,1; 309600,
        0,0.7,1; 313200,0,0.7,1; 316800,0,0.7,1; 320400,0,0.7,1; 324000,0,0,0; 327600,
        0,0,0; 331200,0,0,0; 334800,0,0,0; 338400,1,1,1; 342000,1,1,1; 345600,1,
        1,1; 349200,1,1,1; 352800,1,1,1; 356400,1,1,1; 360000,1,1,1; 363600,1,1,
        1; 367200,1,1,1; 370800,0,0,0; 374400,0,1,0.7; 378000,0,1,0.7; 381600,0,
        1,0.7; 385200,0,1,0.7; 388800,0,1,0.7; 392400,0,0.7,1; 396000,0,0.7,1; 399600,
        0,0.7,1; 403200,0,0.7,1; 406800,0,0.7,1; 410400,0,0,0; 414000,0,0,0; 417600,
        0,0,0; 421200,0,0,0; 424800,1,1,1; 428400,1,1,1; 432000,1,1,1; 435600,1,
        1,1; 439200,1,1,1; 442800,1,1,1; 446400,1,1,1; 450000,1,1,1; 453600,1,1,
        1; 457200,0,0,0; 460800,0,1,0.7; 464400,0,1,0.7; 468000,0,1,0.7; 471600,
        0,1,0.7; 475200,0,1,0.7; 478800,0,0.7,1; 482400,0,0.7,1; 486000,0,0.7,1;
        489600,0,0.7,1; 493200,0,0.7,1; 496800,0,0,0; 500400,0,0,0; 504000,0,0,0;
        507600,0,0,0; 511200,1,1,1; 514800,1,1,1; 518400,1,1,1; 522000,1,1,1; 525600,
        1,1,1; 529200,1,1,1; 532800,1,1,1; 536400,1,1,1; 540000,1,1,1; 543600,0,
        0,0; 547200,0,1,0.7; 550800,0,1,0.7; 554400,0,1,0.7; 558000,0,1,0.7; 561600,
        0,1,0.7; 565200,0,0.7,1; 568800,0,0.7,1; 572400,0,0.7,1; 576000,0,0.7,1;
        579600,0,0.7,1; 583200,0,0,0; 586800,0,0,0; 590400,0,0,0; 594000,0,0,0;
        597600,1,1,1; 601200,1,1,1; 604800,1,1,1])
    annotation (Placement(transformation(extent={{-80,-20},{-60,0}})));

public
  Modelica.Blocks.Logical.Switch switch[2]
    annotation (Placement(transformation(extent={{-20,0},{0,20}})));
equation
  // Compute calendar time (reset time every year)
  converter.modTim = time;
  // Check if we are in winter or summer (summer: may 31th to september 30th)
  test = converter.calTim >= 13132800 and converter.calTim <= 23673600;
  // Connect schedules to switcher
  for i in 1:2 loop
    switch[i].u2 = test;
    connect(occultations.y[1], switch[i].u3);
  end for;

  connect(occultations.y[2], switch[1].u1) annotation (Line(
      points={{-59,-10},{-48,-10},{-48,18},{-22,18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(occultations.y[3], switch[2].u1) annotation (Line(
      points={{-59,-10},{-40,-10},{-40,18},{-22,18}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics),
    experiment(StopTime=4e+007),
    __Dymola_experimentSetupOutput);
end test_occultation_switch;
