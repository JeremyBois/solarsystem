within SolarSystem.Testing;
model test_control "Test new control behavior"

  parameter Integer n=2 "Number of room to control (must be greater than 0)";

  Control.Electrical_control.Backup.Equipements_control equipements_control(
    n=2,
    tempo_algo_flowRate={20,30},
    tempo_algo_elec={10,20},
    table={[0,273.15 + 20,273.15 + 22,90/3600; 100,273.15 + 20,273.15 + 22,90/
        3600; 300,273.15 + 10,273.15 + 22,20/3600; 500,273.15 + 10,273.15 + 22,
        20/3600; 800,273.15 + 20,273.15 + 22,20/3600; 1000,273.15 + 20,273.15
         + 22,90/3600],[0,273.15 + 20,273.15 + 22,90/3600; 100,273.15 + 20,
        273.15 + 22,90/3600; 300,273.15 + 10,273.15 + 22,20/3600; 500,273.15 +
        10,273.15 + 22,20/3600; 800,273.15 + 20,273.15 + 22,20/3600; 1000,
        273.15 + 20,273.15 + 22,90/3600]})
    annotation (Placement(transformation(extent={{2,0},{62,34}})));
  Modelica.Blocks.Sources.Ramp T5_mod(
    height=75,
    duration=800,
    offset=273.15 + 25) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-100,-46},{-80,-26}})));
  Modelica.Blocks.Sources.Ramp T4_mod(
    offset=273.15 + 25,
    duration=500,
    height=1000) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-100,-14},{-80,6}})));
  Modelica.Blocks.Sources.Sine T1_mod(
    freqHz=1/250,
    amplitude=60,
    phase=1.5707963267949,
    offset=273.15 + 60)
    annotation (Placement(transformation(extent={{-100,60},{-80,80}})));
  Modelica.Blocks.Sources.Ramp T3_mod(
    offset=273.15 + 20,
    duration=800,
    height=30) "Allow to show ECS interactions"
    annotation (Placement(transformation(extent={{-100,20},{-80,40}})));
  Modelica.Blocks.Sources.Sine Tamb[n](
    freqHz=1/500,
    amplitude=25,
    offset=273.15 + 10,
    phase=1.5707963267949) annotation (Placement(transformation(extent={{-100,-120},{-80,-100}})));
  Modelica.Blocks.Sources.Ramp T7_mod(
    height=75,
    duration=800,
    offset=273.15 + 25) annotation (Placement(transformation(extent={{-100,-80},{-80,-60}})));
  Modelica.Blocks.Sources.RealExpression Tsoufflage[n](y=fill(273.15 + 30, n))
    annotation (Placement(transformation(extent={{-32,-14},{-16,2}})));
  Modelica.Blocks.Sources.RealExpression Text(y=273.15 + 10)
    annotation (Placement(transformation(extent={{-32,-26},{-16,-10}})));
  Modelica.Blocks.Sources.Ramp Texchanger[n](
    duration=800,
    height=20,
    offset=273.15 + 20) annotation (Placement(transformation(extent={{-32,-52},{-12,-32}})));
equation
  connect(T4_mod.y, equipements_control.T4) annotation (Line(
      points={{-79,-4},{-56,-4},{-56,20},{2.22222,20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T5_mod.y, equipements_control.T5) annotation (Line(
      points={{-79,-36},{-46,-36},{-46,24},{2.22222,24}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3_mod.y, equipements_control.T3) annotation (Line(
      points={{-79,30},{-64,30},{-64,28},{2.22222,28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1_mod.y, equipements_control.T1) annotation (Line(
      points={{-79,70},{-64,70},{-64,32},{2.22222,32}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T7_mod.y, equipements_control.T7) annotation (Line(
      points={{-79,-70},{-40,-70},{-40,16},{2.22222,16}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Text.y, equipements_control.Text) annotation (Line(
      points={{-15.2,-18},{-12,-18},{-12,12},{2.22222,12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tamb.y, equipements_control.Tamb) annotation (Line(
      points={{-79,-110},{-34,-110},{-34,8},{1.77778,8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tsoufflage.y, equipements_control.Tsouff) annotation (Line(
      points={{-15.2,-6},{-6,-6},{-6,1.4},{1.77778,1.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texchanger.y, equipements_control.Texch_out) annotation (Line(
      points={{-11,-42},{-4,-42},{-4,4.6},{1.77778,4.6}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-120},{100,100}}),
                        graphics),
    Icon(coordinateSystem(extent={{-100,-120},{100,100}})),
    experiment(StopTime=1000),
    __Dymola_experimentSetupOutput);
end test_control;
