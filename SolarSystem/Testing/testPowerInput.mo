within SolarSystem.Testing;
model testPowerInput "Test how power is send from an input"
public
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow Elec_to_DHW
    "Prescribed heat flow for heating and cooling"
    annotation (Placement(transformation(extent={{24,30},{2,52}})));
public
  Modelica.Blocks.Sources.Constant PowerToEach(k=600)
    annotation (Placement(transformation(extent={{78,32},{60,50}})));
public
  Buildings.Fluid.FixedResistances.Pipe test(
    dp(start=0),
    redeclare package Medium = Buildings.Media.Water,
    m_flow_nominal=1,
    nSeg=3,
    thicknessIns(displayUnit="mm") = 0.1,
    lambdaIns=0.04,
    length=10,
    useMultipleHeatPorts=true) annotation (Placement(transformation(
        extent={{8,-8},{-8,8}},
        rotation=90,
        origin={-80,20})));
protected
  Buildings.Fluid.Sources.Boundary_pT NoFlow_StaticSource(
    nPorts=1, redeclare package Medium = Buildings.Media.Water)
              annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-80,-44})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                      NoFlow_StaticSource1(
    nPorts=1,
    redeclare package Medium = Buildings.Media.Water,
    m_flow=1) annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={-80,82})));
equation
  connect(PowerToEach.y, Elec_to_DHW.Q_flow)
    annotation (Line(points={{59.1,41},{24,41}}, color={0,0,127}));
  connect(Elec_to_DHW.port, test.heatPorts[1]) annotation (Line(points={{2,41},
          {-42,41},{-42,21.6},{-76,21.6}}, color={191,0,0}));
  connect(Elec_to_DHW.port, test.heatPorts[2]) annotation (Line(points={{2,41},
          {-38,41},{-38,20},{-76,20}}, color={191,0,0}));
  connect(Elec_to_DHW.port, test.heatPorts[3]) annotation (Line(points={{2,41},
          {-38,41},{-38,18.4},{-76,18.4}}, color={191,0,0}));
  connect(test.port_b, NoFlow_StaticSource.ports[1])
    annotation (Line(points={{-80,12},{-80,12},{-80,-38}}, color={0,127,255}));
  connect(NoFlow_StaticSource1.ports[1], test.port_a)
    annotation (Line(points={{-80,76},{-80,76},{-80,28}}, color={0,127,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end testPowerInput;
