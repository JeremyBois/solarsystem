within SolarSystem.Testing;
package FSM
  model FSM_Monozone
    "Complete FSM for the solar air system with electrical auxiliary and a monozone house."

    extends Tools.System_parameters;

    Heating_fsm heating_fsm(temp_min_storage=temp_min_storage)
      annotation (Placement(transformation(extent={{-1342,390},{-1280,440}})));
    Fan_fsm fan_fsm(
      Tsouff_max=Tsouff_max,
      fan_flowRate_max=fan_flowRate_max,
      tempo_algo_flowRate=tempo_algo_flowRate,
      fan_controllerType=fan_controllerType,
      fan_k=fan_k,
      fan_Ti=fan_Ti,
      fan_Td=fan_Td,
      Tsouff_controllerType=Tsouff_controllerType,
      Tsouff_k=Tsouff_k,
      Tsouff_Ti=Tsouff_Ti,
      Tsouff_Td=Tsouff_Td,
      heater_elec_power=heater_elec_power,
      tempo_algo_elec=tempo_algo_elec,
      elecHeat_controllerType=elecHeat_controllerType,
      elecHeat_k=elecHeat_k,
      elecHeat_Ti=elecHeat_Ti,
      elecHeat_Td=elecHeat_Td)
      annotation (Placement(transformation(extent={{-1420,218},{-1324,276}})));
     Modelica.Blocks.Sources.CombiTimeTable Schedules(
      tableOnFile=tableOnFile,
      table=table,
      tableName=tableName,
      fileName=fileName,
      verboseRead=verboseRead,
      columns=columns,
      each smoothness=smoothness,
      each extrapolation=extrapolation,
      offset=offset,
      startTime=startTime)
      "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
      annotation (__Dymola_tag={"Temperature","Consigne"}, Placement(
          transformation(extent={{-1480,438},{-1458,460}})));
    S5_S6_V3V_fsm s5_S6_V3V_fsm(
      temp_min_collector=temp_min_collector,
      temp_max_DHW=temp_max_DHW,
      temp_max_storage=temp_max_storage,
      temp_min_DHW=temp_min_DHW)
      annotation (Placement(transformation(extent={{-1420,312},{-1318,364}})));
    Modelica.Blocks.Interfaces.RealInput Tamb(unit="K")
      "Vector of temperature inside rooms areas" annotation (__Dymola_tag={"Temperature","Computed"},
        Placement(transformation(extent={{-1496,406},{-1470,432}}),
                                                               iconTransformation(
            extent={{-1488,256},{-1466,278}})));
    Modelica.Blocks.Interfaces.RealInput T5(unit="K")
      "Temperature inside storage tank"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
              {12,12}},
          origin={-1484,240}),
          iconTransformation(extent={{-11,-11},{11,11}},
          origin={-1479,343})));
    Modelica.Blocks.Interfaces.RealInput T1(unit="K")
      "Temperature inside collector"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
              {12,12}},
          origin={-1484,330}),
          iconTransformation(extent={{-11,-11},{11,11}},
          origin={-1479,439})));
    Modelica.Blocks.Interfaces.RealInput T7(unit="K")
      "Water temperature after exchanger"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
              {12,12}},
          origin={-1484,210}),
          iconTransformation(extent={{-11,-11},{11,11}},
          origin={-1479,311})));
    Modelica.Blocks.Interfaces.RealInput Text(unit="K")
      "Temperature outside the room (environnement)"
                                              annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(
          extent={{-13,-13},{13,13}},
          rotation=0,
          origin={-1483,391}),
                            iconTransformation(extent={{-11,-11},{11,11}},
          rotation=0,
          origin={-1477,235})));
    Modelica.Blocks.Interfaces.RealInput T3(unit="K")
      "Temperature inside DHW tank next to solar exchanger (bottom)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1496,
              288},{-1472,312}}),                                                                                       iconTransformation(
            extent={{-1490,396},{-1468,418}})));
    Modelica.Blocks.Interfaces.RealInput T4(unit="K")
      "Temperature inside DHW tank next to drawing up (top)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-1496,
              258},{-1472,282}}),                                                                                       iconTransformation(
            extent={{-1490,364},{-1468,386}})));
     Buildings.Controls.Continuous.LimPID pid_S2(
      controllerType=S2_controllerType,
      k=S2_k,
      Ti=S2_Ti,
      Td=S2_Td,
      yMax=S2_yMax,
      yMin=S2_yMin,
      reverseAction=false)
      "Compute correct factor in order to modulate S2 speed"
      annotation (Placement(transformation(extent={{-1050,348},{-1028,370}})));
    Buildings.Controls.Continuous.LimPID pid_S6(
      controllerType=S6_controller,
      k=S6_k,
      Ti=S6_Ti,
      Td=S6_Td,
      yMax=S6_yMax,
      yMin=S6_yMin,
      reverseAction=false)
      annotation (Placement(transformation(extent={{-1060,272},{-1040,292}})));
    Buildings.Controls.Continuous.LimPID pid_S5(
      controllerType=S5_controller,
      k=S5_k,
      Ti=S5_Ti,
      Td=S5_Td,
      yMax=S5_yMax,
      yMin=S5_yMin,
      reverseAction=false)
      annotation (Placement(transformation(extent={{-1060,310},{-1040,330}})));
    Modelica.Blocks.Interfaces.RealOutput S5_speed( unit="1/min")
      "Final speed of S5 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
        Placement(transformation(extent={{-952,308},{-928,332}}),   iconTransformation(extent={{-950,
              302},{-924,328}})));
    Modelica.Blocks.Interfaces.RealOutput S6_speed( unit="1/min")
      "Final speed of S6 pump"                                                             annotation (__Dymola_tag={"Pump", "speed"},
        Placement(transformation(extent={{-952,270},{-928,294}}),   iconTransformation(extent={{-950,
              328},{-924,354}})));
    Modelica.Blocks.Interfaces.RealOutput S2_speed(each unit="1/min")
      "Output of S2 pump" annotation (__Dymola_tag={"Pump","speed"}, Placement(
          transformation(extent={{-952,340},{-928,364}}), iconTransformation(
            extent={{-950,354},{-924,380}})));
    Modelica.Blocks.Interfaces.RealOutput fan_flowRate(each unit="m3/s")
      "Computed fan flow rate [m3/s]" annotation (Placement(transformation(extent={{-954,
              420},{-926,448}}),       iconTransformation(extent={{-950,436},{
              -926,460}})));
    Modelica.Blocks.Interfaces.RealOutput minimal_flowRate(each unit="m3/s")
      "Minimal sanitary volumic flow rate [m3/s]" annotation (Placement(
          transformation(
          extent={{-16,-16},{16,16}},
          rotation=0,
          origin={-940,400}),  iconTransformation(extent={{-950,408},{-926,432}})));
    Buildings.Controls.Continuous.LimPID pip_DHW_elec(
      controllerType=elecDHW_controllerType,
      k=elecDHW_k,
      Ti=elecDHW_Ti,
      Td=elecDHW_Td,
      reverseAction=false)
      "Controller for electrical heat to DHW"
      annotation (Placement(transformation(extent={{-1122,184},{-1102,204}})));
    Modelica.Blocks.Continuous.Integrator integrator_DHW_elec
      "Integration of the power used to keep domestic hot water at the setpoint temperature"
      annotation (Placement(transformation(extent={{-10,-10},{10,10}},
          rotation=-90,
          origin={-1192,182})));
    Modelica.Blocks.Continuous.Integrator integrator_heating_elec
      "Integration of the power used to keep room air at the setpoint temperature"
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=-90,
          origin={-1260,186})));
    Modelica.Blocks.Interfaces.RealOutput power_DHW_elec( unit="W")
      "Output the power needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Power", "Electric"}, Placement(transformation(
            extent={{-954,180},{-926,208}}),   iconTransformation(extent={{-950,
              238},{-926,262}})));
    Modelica.Blocks.Interfaces.RealOutput energy_DHW_elec( unit="J")
      "Output the cumulated energy needed to keep water temperature higher than setpoint" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(transformation(
            extent={{-14,-14},{14,14}},
          rotation=-90,
          origin={-1192,140}),                 iconTransformation(extent={{-950,
              164},{-926,188}})));
    Modelica.Blocks.Interfaces.RealOutput power_air_elec(each unit="W")
      "Output the power needed to keep room air temperature at setpoint"
      annotation (__Dymola_tag={"Power","Electric"}, Placement(transformation(
            extent={{-954,216},{-926,244}}), iconTransformation(extent={{-950,
              214},{-926,238}})));
    Modelica.Blocks.Interfaces.RealOutput energy_air_elec(each unit="J")
      "Output the cumulated energy needed to keep room air temperature at setpoint"
      annotation (__Dymola_tag={"Energy","Electric"}, Placement(transformation(
          extent={{-14,-14},{14,14}},
          rotation=-90,
          origin={-1260,140}), iconTransformation(extent={{-950,140},{-926,164}})));
    Modelica.Blocks.Interfaces.RealInput Texch_out(unit="K")
      "Temperature after the solar exchanger" annotation (__Dymola_tag={"Temperature","Computed"},
        Placement(transformation(extent={{-12,-12},{12,12}}, origin={-1484,176}),
          iconTransformation(extent={{-11,-11},{11,11}}, origin={-1479,179})));
    Modelica.Blocks.Interfaces.RealInput Tsouff(each unit="K")
      "Vector of air temperature blowing into the room" annotation (__Dymola_tag={"Temperature",
          "Computed"}, Placement(transformation(extent={{-1492,138},{-1468,162}}),
          iconTransformation(extent={{-1490,140},{-1468,162}})));
  protected
    Tools.BusAlgoController busAlgoController annotation (Placement(
          transformation(extent={{-1200,316},{-1160,356}}), iconTransformation(
            extent={{-1222,292},{-1202,312}})));
  public
    Utilities.Other.ProductWithCoef ComputeS2_speed(k=S2_speed_max)
      annotation (Placement(transformation(extent={{-998,342},{-978,362}})));
    Utilities.Other.ProductWithCoef ComputeS5_speed(k=S5_speed_max)
      annotation (Placement(transformation(extent={{-996,310},{-976,330}})));
    Utilities.Other.ProductWithCoef ComputeS6_speed(k=S6_speed_max)
      annotation (Placement(transformation(extent={{-996,272},{-976,292}})));
    Modelica.Blocks.Math.Gain ComputeDHW_elec_power(k=DHW_elec_power)
      annotation (Placement(transformation(extent={{-1052,184},{-1032,204}})));
     Modelica.Blocks.Interfaces.RealOutput    V3V_solar
      "State of the solar valve" annotation (__Dymola_tag={"Valve"}, Placement(transformation(
            extent={{-14,-14},{14,14}},
          rotation=90,
          origin={-1130,472}),                 iconTransformation(extent={{-14,-14},
              {14,14}},
          rotation=90,
          origin={-1098,464})));
     Modelica.Blocks.Interfaces.RealOutput    S6_state
      "State of the solar pump S6." annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(
            extent={{-14,-14},{14,14}},
          rotation=90,
          origin={-1222,472}),                 iconTransformation(extent={{14,-14},
              {-14,14}},
          rotation=-90,
          origin={-1206,464})));
  public
    Modelica.Blocks.Interfaces.RealOutput    S5_state "Control S5 state"
      annotation (__Dymola_tag={"Pump", "state"}, Placement(transformation(extent={{-14,-14},
              {14,14}},
          rotation=90,
          origin={-1192,472}),                                                                                iconTransformation(
            extent={{13,-13},{-13,13}},
          rotation=-90,
          origin={-1169,463})));
    Modelica.Blocks.Interfaces.RealOutput S2_state "S2 pumps state" annotation (
       __Dymola_tag={"Pump","state"}, Placement(transformation(
          extent={{-14,-14},{14,14}},
          rotation=90,
          origin={-1248,472}), iconTransformation(
          extent={{14,-14},{-14,14}},
          rotation=-90,
          origin={-1248,464})));
    Modelica.Blocks.Math.IntegerToReal S2_state_ToReal
      "Convert Integer to Real" annotation (Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=90,
          origin={-1248,434})));
    Modelica.Blocks.Math.IntegerToReal S6_state_ToReal
      "Convert Integer to Real" annotation (Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=90,
          origin={-1222,434})));
    Modelica.Blocks.Math.IntegerToReal S5_state_ToReal
      "Convert Integer to Real" annotation (Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=90,
          origin={-1192,434})));
    Modelica.Blocks.Math.BooleanToReal V3V_state_ToReal
      "Convert Boolean to Real" annotation (Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=90,
          origin={-1130,434})));
  equation


    //
    // Describe pid pump output construction for S6 and S5
    //
    // Connection to S6 pid
    pid_S6.u_s = S6_delta_min;
    pid_S6.u_m = T1 - T5;

    // Connection to S5 pid
    pid_S5.u_s = S5_delta_min;
    pid_S5.u_m = T1 - T3;

    //
    // Describe the control of the electrical extra heating for DHW
    //
    pip_DHW_elec.u_s = DHW_setpoint "Set DHW temperature setpoint for PID";

    connect(Schedules.y, heating_fsm.Schedules) annotation (Line(points={{-1456.9,
            449},{-1440.45,449},{-1440.45,435.417},{-1341.59,435.417}}, color={0,0,
            127}));
    connect(Schedules.y, fan_fsm.Schedules) annotation (Line(points={{-1456.9,449},
            {-1440,449},{-1440,346},{-1440,271.65},{-1419.36,271.65}}, color={0,0,
            127}));
    connect(fan_fsm.fan_flowRate, fan_flowRate) annotation (Line(points={{
            -1322.72,267.783},{-1282,267.783},{-1100,267.783},{-1100,434},{-940,
            434}},
          color={0,0,127}));
    connect(fan_fsm.power_air_elec, integrator_heating_elec.u) annotation (Line(
          points={{-1322.72,230.083},{-1260,230.083},{-1260,198}},   color={0,0,127}));
    connect(integrator_heating_elec.y, energy_air_elec) annotation (Line(points={{-1260,
            175},{-1260,140}},                                       color={0,0,127}));
    connect(fan_fsm.power_air_elec, power_air_elec) annotation (Line(points={{
            -1322.72,230.083},{-1160,230.083},{-1160,230},{-940,230}},
                                                              color={0,0,127}));
    connect(T1, heating_fsm.T1) annotation (Line(points={{-1484,330},{-1452,330},
            {-1452,412.5},{-1341.17,412.5}},color={0,0,127}));
    connect(T5, heating_fsm.T5) annotation (Line(points={{-1484,240},{-1446,240},
            {-1446,403.75},{-1341.17,403.75}},color={0,0,127}));
    connect(T7, heating_fsm.T7) annotation (Line(points={{-1484,210},{-1484,210},
            {-1434,210},{-1434,394.583},{-1340.76,394.583}},color={0,0,127}));
    connect(T1, s5_S6_V3V_fsm.T1) annotation (Line(points={{-1484,330},{-1452,
            330},{-1452,359.273},{-1417.28,359.273}},
                                                 color={0,0,127}));
    connect(T3, s5_S6_V3V_fsm.T3) annotation (Line(points={{-1484,300},{-1460,
            300},{-1460,346},{-1417.28,346},{-1417.28,347.927}},
                                                            color={0,0,127}));
    connect(T4, s5_S6_V3V_fsm.T4) annotation (Line(points={{-1484,270},{-1456,
            270},{-1456,336.109},{-1417.28,336.109}},
                                                 color={0,0,127}));
    connect(T5, s5_S6_V3V_fsm.T5) annotation (Line(points={{-1484,240},{-1446,
            240},{-1446,322},{-1417.28,322},{-1417.28,324.291}},
                                                            color={0,0,127}));
    connect(T7, s5_S6_V3V_fsm.T7) annotation (Line(points={{-1484,210},{-1434,
            210},{-1434,312.473},{-1417.28,312.473}},        color={0,0,127}));
    connect(Text, fan_fsm.Text) annotation (Line(points={{-1483,391},{-1462,391},
            {-1462,256.183},{-1420,256.183}},color={0,0,127}));
    connect(Tamb, fan_fsm.Tamb) annotation (Line(points={{-1483,419},{-1466,419},
            {-1466,246.517},{-1420,246.517}},color={0,0,127}));
    connect(Tamb, heating_fsm.Tamb) annotation (Line(points={{-1483,419},{-1466,
            419},{-1466,423.958},{-1341.38,423.958}},
                                                 color={0,0,127}));
    connect(Texch_out, fan_fsm.Texch_out) annotation (Line(points={{-1484,176},{-1484,
            176},{-1430,176},{-1430,231.05},{-1419.36,231.05}}, color={0,0,127}));
    connect(Tsouff, fan_fsm.Tsouff) annotation (Line(points={{-1480,150},{-1480,
            158},{-1426,158},{-1426,222.35},{-1419.36,222.35}},
                                                           color={0,0,127}));

    connect(Texch_out, pid_S2.u_m) annotation (Line(points={{-1484,176},{-1296,176},
            {-1296,242},{-1076,242},{-1076,340},{-1076,345.8},{-1039,345.8}},
                                                                            color=
           {0,0,127}));
    connect(s5_S6_V3V_fsm.busCon, busAlgoController) annotation (Line(
        points={{-1367.64,309.164},{-1367.64,300},{-1180,300},{-1180,336}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}}));
    connect(busAlgoController.Tsouff_instruction, pid_S2.u_s) annotation (Line(
        points={{-1179.9,336.1},{-1180,336.1},{-1180,300},{-1080,300},{-1080,
            359},{-1052.2,359}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(pid_S2.y, ComputeS2_speed.u1) annotation (Line(points={{-1026.9,359},{
            -1014.45,359},{-1014.45,358},{-1000,358}}, color={0,0,127}));
    connect(pid_S5.y, ComputeS5_speed.u1) annotation (Line(points={{-1039,320},{-1012.5,
            320},{-1012.5,326},{-998,326}}, color={0,0,127}));
    connect(pid_S6.y, ComputeS6_speed.u1) annotation (Line(points={{-1039,282},{-1014,
            282},{-1014,288},{-998,288}}, color={0,0,127}));
    connect(busAlgoController.S2_state, ComputeS2_speed.u2) annotation (Line(
        points={{-1179.9,336.1},{-1179.9,300},{-1080,300},{-1080,336},{-1020,
            336},{-1020,346},{-1000,346}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(busAlgoController.S6_state, ComputeS5_speed.u2) annotation (Line(
        points={{-1179.9,336.1},{-1180,336.1},{-1180,300},{-1006,300},{-1006,
            314},{-998,314}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(busAlgoController.S5_state, ComputeS6_speed.u2) annotation (Line(
        points={{-1179.9,336.1},{-1179.9,300},{-1080,300},{-1080,260},{-1006,
            260},{-1006,276},{-998,276}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(ComputeS6_speed.y, S6_speed) annotation (Line(points={{-975,282},{
            -975,282},{-940,282}},
                              color={0,0,127}));
    connect(ComputeS2_speed.y, S2_speed) annotation (Line(points={{-977,352},{
            -977,352},{-940,352}},
                              color={0,0,127}));
    connect(ComputeS5_speed.y, S5_speed) annotation (Line(points={{-975,320},{
            -975,320},{-940,320}},
                              color={0,0,127}));
    connect(busAlgoController, heating_fsm.busCon) annotation (Line(
        points={{-1180,336},{-1180,336},{-1180,300},{-1310.17,300},{-1310.17,
            391.667}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));

    connect(busAlgoController, fan_fsm.busCon) annotation (Line(
        points={{-1180,336},{-1180,336},{-1180,300},{-1300,300},{-1300,188},{
            -1370.72,188},{-1370.72,219.933}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(pip_DHW_elec.y, ComputeDHW_elec_power.u) annotation (Line(points={{-1101,
            194},{-1101,194},{-1054,194}}, color={0,0,127}));
    connect(ComputeDHW_elec_power.y, power_DHW_elec) annotation (Line(points={{-1031,
            194},{-1031,194},{-940,194}}, color={0,0,127}));
    connect(integrator_DHW_elec.y, energy_DHW_elec) annotation (Line(points={{-1192,
            171},{-1192,140}},             color={0,0,127}));
    connect(ComputeDHW_elec_power.y, integrator_DHW_elec.u) annotation (Line(
          points={{-1031,194},{-1008,194},{-1008,224},{-1192,224},{-1192,194}},
          color={0,0,127}));
    connect(T4, pip_DHW_elec.u_m) annotation (Line(points={{-1484,270},{-1432,270},
            {-1456,270},{-1456,162},{-1112,162},{-1112,182}}, color={0,0,127}));
    connect(S2_state_ToReal.y, S2_state)
      annotation (Line(points={{-1248,440.6},{-1248,472}}, color={0,0,127}));
    connect(S6_state_ToReal.y, S6_state) annotation (Line(points={{-1222,440.6},
            {-1222,440.6},{-1222,472}}, color={0,0,127}));
    connect(S5_state_ToReal.y, S5_state)
      annotation (Line(points={{-1192,440.6},{-1192,472}}, color={0,0,127}));
    connect(V3V_state_ToReal.y, V3V_solar) annotation (Line(points={{-1130,
            440.6},{-1130,440.6},{-1130,472}}, color={0,0,127}));
    connect(busAlgoController.S2_state, S2_state_ToReal.u) annotation (Line(
        points={{-1179.9,336.1},{-1180,336.1},{-1180,420},{-1248,420},{-1248,
            426.8}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(busAlgoController.S6_state, S6_state_ToReal.u) annotation (Line(
        points={{-1179.9,336.1},{-1180,336.1},{-1180,420},{-1222,420},{-1222,
            426.8}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(busAlgoController.S5_state, S5_state_ToReal.u) annotation (Line(
        points={{-1179.9,336.1},{-1179.9,420},{-1192,420},{-1192,426.8}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(busAlgoController.V3V_solar_state, V3V_state_ToReal.u) annotation (
        Line(
        points={{-1179.9,336.1},{-1179.9,420},{-1130,420},{-1130,426.8}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(Schedules.y[3], minimal_flowRate) annotation (Line(points={{-1456.9,
            449},{-1380,449},{-1380,380},{-1160,380},{-1160,400},{-940,400}},
          color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-1480,140},
              {-940,460}}), graphics={            Rectangle(
            extent={{-1482,460},{-938,140}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={253,246,227}), Text(
            extent={{-1418,396},{-1010,234}},
            lineColor={28,108,200},
            textString="%name")}),                                 Diagram(
          coordinateSystem(preserveAspectRatio=false, extent={{-1480,140},{-940,460}})));
  end FSM_Monozone;

  block S5_S6_V3V_fsm

  extends Base_fsm;
  extends Tools.S5_S6_V3V_parameters;

  import MF = Buildings.Utilities.Math.Functions;

  //
  // General inputs/outputs
  //
    Modelica.Blocks.Interfaces.RealInput T1(unit="K")
      "Temperature inside collector"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
              {12,12}},
          origin={-102,100}),
          iconTransformation(extent={{-18,-18},{18,18}},
          origin={-92,100})));
    Modelica.Blocks.Interfaces.RealInput T3(unit="K")
      "Temperature inside DHW tank next to solar exchanger (bottom)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-114,58},
              {-90,82}}),                                                                                               iconTransformation(
            extent={{-110,34},{-74,70}})));
    Modelica.Blocks.Interfaces.RealInput T4(unit="K")
      "Temperature inside DHW tank next to drawing up (top)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-114,28},
              {-90,52}}),                                                                                               iconTransformation(
            extent={{-110,-16},{-74,20}})));
    Modelica.Blocks.Interfaces.RealInput T5(unit="K")
      "Temperature inside solar tank (top)"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-114,-4},
              {-90,20}}),                                                                                               iconTransformation(
            extent={{-110,-66},{-74,-30}})));
     Modelica.Blocks.Interfaces.RealInput T7(unit="K")
      "Water temperature after exchanger"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
              {12,12}},
          origin={-102,-20}),
          iconTransformation(extent={{-18,-18},{18,18}},
          origin={-92,-98})));

  //
  // Solar valve (V3V)
  //
    Modelica.Blocks.Logical.OnOffController need_solar_Storage(pre_y_start=true,
        bandwidth=2)
      "Return True if solar must be used to load the storage tank"
      annotation (Placement(transformation(extent={{-76,54},{-64,66}})));
    Modelica.Blocks.Logical.Hysteresis enough_power_in_DHW(uLow=temp_min_DHW - 2, uHigh=
          temp_min_DHW)
      "Test if we have enough power inside the DHW before authorize temperature inside the storage tank"
      annotation (Placement(transformation(extent={{-76,36},{-64,48}})));
    Modelica.Blocks.Logical.OnOffController need_solar_DHW(pre_y_start=true, bandwidth=2)
      "Return True if solar must be used and False if solar must not be used"
      annotation (Placement(transformation(extent={{-76,82},{-64,94}})));
    Modelica.Blocks.Sources.BooleanExpression V3V_solar_state(y=(
          enough_power_in_DHW.y and need_solar_Storage.y) or need_solar_DHW.y or
          need_solar_direct_heat.y)
      "[T1 >= T5 and T3 >= 32�C] or [T1 >= min(T3, T4)]" annotation (Placement(
          transformation(
          extent={{-33,-10},{33,10}},
          rotation=-90,
          origin={-48,79})));
    Modelica.Blocks.Logical.OnOffController need_solar_direct_heat(bandwidth=5)
      "We can use direct solar energy for heating"
      annotation (Placement(transformation(extent={{-74,104},{-64,114}})));

  //
  // Pump S5 (pump for DHW tank exchanger)
  //
    Modelica_StateGraph2.Step S5_off(
      initialStep=true,
      nOut=1,
      nIn=1) "S5 state is OFF"
      annotation (Placement(transformation(extent={{138,72},{146,80}})));
    Modelica_StateGraph2.Transition S5_T_OffToOn(
      use_firePort=false,
      delayedTransition=false,
      use_conditionPort=true)
      "Transition condition to change S5 state from OFF to ON"
      annotation (Placement(transformation(extent={{138,50},{146,58}})));
    Modelica_StateGraph2.Step S5_on(
      nOut=1,
      nIn=1,
      initialStep=false,
      use_activePort=true) "S5 state is ON"
      annotation (Placement(transformation(extent={{138,28},{146,36}})));
    Modelica_StateGraph2.Transition S5_T_OnToOff(
      use_firePort=false,
      use_conditionPort=true,
      delayedTransition=true,
      waitTime=60) "Transition condition to change S5 state from ON to OFF"
      annotation (Placement(transformation(extent={{170,58},{162,50}})));
    Modelica.Blocks.Logical.Not oppositeForS5
      annotation (Placement(transformation(extent={{106,60},{122,76}})));
    Modelica.Blocks.Sources.BooleanExpression s5_cond_off_to_on(y=V3V_solar_state.y and
          max_DHW_tank.y and enoughDelta_T1_T3.y)
      "[T1 - T3 >= 10 and T3 > temp_max_DHW and V3V_solar]"
      annotation (Placement(transformation(extent={{-40,12},{60,34}})));
    Modelica.Blocks.Math.BooleanToInteger S5_State
      annotation (Placement(transformation(extent={{72,-94},{60,-82}})));

  //
  // Pump S6 (pump for Storage tank exchanger)
  //
    Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T3(
      y(start=false),
      uLow=4,
      uHigh=10) "Check if T1 - T3 is larger enough"
      annotation (Placement(transformation(extent={{-76,6},{-64,18}})));
    Modelica.Blocks.Logical.OnOffController max_DHW_tank(bandwidth=5, pre_y_start=true)
      "Test if the temperature inside the tank is lower than reference"
      annotation (Placement(transformation(extent={{-76,-18},{-64,-6}})));
    Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T5(
      y(start=false),
      uHigh=10,
      uLow=2.5) "Check if T1 - T5 is larger enough"
      annotation (Placement(transformation(extent={{-74,-62},{-64,-52}})));
    Modelica.Blocks.Logical.OnOffController max_Storage_tank(bandwidth=5)
      "Test if the temperature inside the tank is lower than reference"
      annotation (Placement(transformation(extent={{-74,-84},{-64,-74}})));
  Modelica_StateGraph2.Step S6_off(
      initialStep=true,
      nOut=1,
      nIn=1) "S6 state is OFF"
      annotation (Placement(transformation(extent={{138,-18},{146,-10}})));
    Modelica_StateGraph2.Transition S6_T_OffToOn(
      use_firePort=false,
      delayedTransition=false,
      use_conditionPort=true)
      "Transition condition to change S5 state from OFF to ON"
      annotation (Placement(transformation(extent={{138,-36},{146,-28}})));
    Modelica_StateGraph2.Step S6_on(
      nOut=1,
      nIn=1,
      initialStep=false,
      use_activePort=true) "S6 state is ON"
      annotation (Placement(transformation(extent={{138,-62},{146,-54}})));
    Modelica_StateGraph2.Transition S6_T_OnToOff(
      use_firePort=false,
      use_conditionPort=true,
      delayedTransition=true,
      waitTime=60) "Transition condition to change S5 state from ON to OFF"
      annotation (Placement(transformation(extent={{170,-28},{162,-36}})));
    Modelica.Blocks.Logical.Not oppositeForS6
      annotation (Placement(transformation(extent={{106,-62},{122,-46}})));
    Modelica.Blocks.Sources.BooleanExpression s6_cond_off_to_on(y=enoughDelta_T1_T5.y and
          enough_power_in_DHW.y and max_Storage_tank.y)
      "[T1 - T5 >= 10 and min(T3, T4) >= 30 and T5 > temp_max_DHW]"
      annotation (Placement(transformation(extent={{-40,-28},{60,-6}})));
    Modelica.Blocks.Math.BooleanToInteger S6_State
      annotation (Placement(transformation(extent={{72,-74},{60,-62}})));



  equation
  //
  // Describe the control of the solar valve (V3V)
  //
    //Connect minimal between T4 and T3 for test with T1 (`+1` used to shift bandwidth upper)
    need_solar_DHW.u = MF.smoothMin(x1=T4, x2=T3,deltaX=0.1) + 1
      "[T1 >= min(T3, T4)]";
    need_solar_Storage.u = T5 + 1 "[ T1 >= T5 ]";

    // Test temperature difference between T1 and max(T7 or minimal value to use direct solar)
    need_solar_direct_heat.u = Buildings.Utilities.Math.Functions.smoothMax(x1=temp_min_collector,
                                                                             x2=T7,
                                                                             deltaX=0.1);
    // Connect T1 to solar_direct bloc OnOffController
    need_solar_direct_heat.reference = T1 - (need_solar_direct_heat.bandwidth / 2)
      "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";


  //
  // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
  //
    enoughDelta_T1_T3.u = T1 - T3 "[T1 - T3 >= 10]";
    max_DHW_tank.reference = temp_max_DHW "[T3 < temp_max_DHW]";

  //
  // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
  //
    enoughDelta_T1_T5.u = T1 - T5 "[T1 - T5 >= 10]";
    max_Storage_tank.reference = temp_max_storage-2.5 "[T5 > temp_max_DHW]";


    connect(S5_off.outPort[1], S5_T_OffToOn.inPort)
      annotation (Line(points={{142,71.4},{142,64.7},{142,58}},
                                                             color={0,0,0}));
    connect(S5_T_OffToOn.outPort, S5_on.inPort[1])
      annotation (Line(points={{142,49},{142,42.5},{142,36}},
                                                           color={0,0,0}));
    connect(S5_on.outPort[1], S5_T_OnToOff.inPort) annotation (Line(points={{142,27.4},
            {142,27.4},{142,18},{166,18},{166,50}},   color={0,0,0}));
    connect(S5_T_OnToOff.outPort, S5_off.inPort[1]) annotation (Line(points={{166,59},
            {166,92},{142,92},{142,80}},     color={0,0,0}));
    connect(S6_off.outPort[1], S6_T_OffToOn.inPort) annotation (Line(points={{142,
            -18.6},{142,-28}},              color={0,0,0}));
    connect(S6_T_OffToOn.outPort, S6_on.inPort[1])
      annotation (Line(points={{142,-37},{142,-37},{142,-54}},color={0,0,0}));
    connect(S6_on.outPort[1], S6_T_OnToOff.inPort) annotation (Line(points={{142,-62.6},
            {142,-62.6},{142,-72},{166,-72},{166,-36}},    color={0,0,0}));
    connect(S6_T_OnToOff.outPort, S6_off.inPort[1]) annotation (Line(points={{166,-27},
            {166,2},{142,2},{142,-10}},     color={0,0,0}));
    connect(oppositeForS6.y, S6_T_OnToOff.conditionPort) annotation (Line(points={{122.8,
            -54},{126,-54},{126,-78},{174,-78},{174,-32},{171,-32}},  color={255,0,
            255},
        smooth=Smooth.Bezier));
    connect(oppositeForS5.y, S5_T_OnToOff.conditionPort) annotation (Line(points={{122.8,
            68},{126,68},{126,98},{178,98},{178,54},{171,54}},    color={255,0,255},
        smooth=Smooth.Bezier));

    connect(T1, need_solar_DHW.reference) annotation (Line(points={{-102,100},{-84,100},{-84,91.6},
            {-77.2,91.6}},                 color={0,0,127}));
    connect(T1, need_solar_Storage.reference) annotation (Line(points={{-102,100},{-102,100},{-84,
            100},{-84,63.6},{-77.2,63.6}},                color={0,0,127}));
    connect(T3, enough_power_in_DHW.u) annotation (Line(points={{-102,70},{-84,70},{-84,42},{-77.2,
            42}},                 color={0,0,127}));
    connect(T3, max_DHW_tank.u) annotation (Line(points={{-102,70},{-102,74},{-86,74},{-86,-15.6},
            {-77.2,-15.6}},             color={0,0,127}));
    connect(s6_cond_off_to_on.y, S6_T_OffToOn.conditionPort) annotation (Line(points={{65,-17},
            {90,-17},{90,-18},{114,-18},{114,-32},{137,-32}},
                                     color={255,0,255},
        smooth=Smooth.Bezier));
    connect(s6_cond_off_to_on.y, oppositeForS6.u) annotation (Line(points={{65,-17},
            {88,-17},{88,-54},{104.4,-54}},
                              color={255,0,255}));
    connect(s5_cond_off_to_on.y, oppositeForS5.u) annotation (Line(points={{65,23},
            {82,23},{82,30},{82,68},{104.4,68}},
                                color={255,0,255}));
    connect(s5_cond_off_to_on.y, S5_T_OffToOn.conditionPort)
      annotation (Line(points={{65,23},{96,23},{96,54},{137,54}},
                                                                color={255,0,255},
        smooth=Smooth.Bezier));

    connect(V3V_solar_state.y, busCon.V3V_solar_state) annotation (Line(points={{-48,
            42.7},{-48,-60},{0.1,-60},{0.1,-95.9}},
                              color={255,0,255}), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}}));
    connect(T5, max_Storage_tank.u) annotation (Line(points={{-102,8},{-98,8},{-98,
            8},{-88,8},{-88,-82},{-75,-82}}, color={0,0,127}));
    connect(S6_on.activePort, S6_State.u) annotation (Line(
        points={{146.72,-58},{154,-58},{154,-68},{73.2,-68}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(S5_on.activePort, S5_State.u) annotation (Line(
        points={{146.72,32},{164,32},{186,32},{186,-88},{73.2,-88}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(S6_State.y, busCon.S6_state) annotation (Line(
        points={{59.4,-68},{32,-68},{32,-95.9},{0.1,-95.9}},
        color={255,127,0},
        smooth=Smooth.Bezier), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}}));
    connect(S5_State.y, busCon.S5_state) annotation (Line(
        points={{59.4,-88},{32,-88},{32,-95.9},{0.1,-95.9}},
        color={255,127,0},
        smooth=Smooth.Bezier), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
              {200,120}})),       Diagram(coordinateSystem(preserveAspectRatio=
              false, extent={{-100,-100},{200,120}}), graphics={
                                                  Rectangle(
            extent={{-80,24},{-60,-20}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={238,232,213}),             Rectangle(
            extent={{-80,120},{-60,34}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={147,161,161}),             Rectangle(
            extent={{-80,-46},{-60,-86}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={253,246,227}),
          Text(
            extent={{-78,6},{-64,-2}},
            lineColor={38,139,210},
            textString="S5"),
          Text(
            extent={{-76,-62},{-62,-70}},
            lineColor={38,139,210},
            textString="S6"),
          Text(
            extent={{-76,82},{-64,70}},
            lineColor={38,139,210},
            textString="V3V")}));
  end S5_S6_V3V_fsm;

  block Heating_fsm "Handle heating and overheating needs. Define if we can use direct or indirect solar enery too.
  This block only work if the house use a unique heatport."
    extends Base_fsm;
    extends Tools.Heating_parameters;

    import SI = Modelica.SIunits;
    import MBV = Modelica.Math.BooleanVectors;
  //
  // General inputs/outputs
  //
    Utilities.Other.Shift heating_setpoint_shift(k=need_heat.bandwidth/2)
      "Heat temperature setpoint for each room to always stay upper than instruction. "
      annotation (Placement(transformation(extent={{-66,100},{-62,104}})));
    Utilities.Other.Shift overheating_setpoint_shifted(k=need_overheat.bandwidth/2)
      "Heat temperature setpoint for each room to always stay upper than instruction. "
      annotation (Placement(transformation(extent={{-66,80},{-62,84}})));

    Modelica.Blocks.Interfaces.RealInput Tamb(unit="K")
      "Vector of temperature inside rooms areas" annotation (__Dymola_tag={"Temperature","Computed"},
        Placement(transformation(extent={{-120,38},{-94,64}}), iconTransformation(
            extent={{-112,28},{-82,58}})));
    Modelica.Blocks.Interfaces.RealInput T5(unit="K")
      "Temperature inside storage tank"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
              {12,12}},
          origin={-106,10}),
          iconTransformation(extent={{-16,-16},{16,16}},
          origin={-96,-54})));
     Modelica.Blocks.Interfaces.RealInput T1(unit="K")
      "Temperature inside collector"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
              {12,12}},
          origin={-106,-24}),
          iconTransformation(extent={{-16,-16},{16,16}},
          origin={-96,-12})));
    Modelica.Blocks.Interfaces.RealInput Schedules[3]
      "Schedules (1 == temperature instruction, 2 == overheating tempearture instruction, 3 == minimal mass flow rate)"
      annotation (__Dymola_tag={"Temperature","Computed"}, Placement(
          transformation(extent={{-12,-12},{12,12}}, origin={-106,92}),
          iconTransformation(extent={{-16,-16},{16,16}}, origin={-98,98})));

    Modelica.Blocks.Interfaces.RealInput T7(unit="K")
      "Water temperature after exchanger"
      annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
              {12,12}},
          origin={-104,-52}),
          iconTransformation(extent={{-16,-16},{16,16}},
          origin={-94,-98})));

  //
  // Boolean logic
  //
    Modelica.Blocks.Logical.Not condition_no_heat "We do not need heat anymore"
      annotation (Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=0,
          origin={22,46})));
    Modelica.Blocks.Logical.Or condition_need_heat_or_overheat
      "We need heat or overheat" annotation (Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=0,
          origin={22,66})));
    Modelica.Blocks.Logical.Not condition_direct_heat_off annotation (Placement(
          transformation(
          extent={{-6,-6},{6,6}},
          rotation=90,
          origin={92,-52})));
    Modelica.Blocks.Logical.Not condition_indirect_heat_off annotation (Placement(
          transformation(
          extent={{-6,6},{6,-6}},
          rotation=90,
          origin={148,-54})));

    Modelica.Blocks.Sources.BooleanExpression condition_heat_no_overheat(y=(
          need_heat.y and not need_overheat.y) or (condition_direct_heat_off.y))
      "Need heating or no Overheating" annotation (Placement(transformation(
          extent={{-65,-10},{65,10}},
          rotation=0,
          origin={95,-116})));

    Modelica.Blocks.Sources.BooleanExpression condition_solar_indirect(y=not
          busCon.V3V_solar_state and enough_energy_storage.y)
      "Not V3V and enough energy in collectors to heat the storage tank."
      annotation (Placement(transformation(
          extent={{-46,-10},{46,10}},
          rotation=0,
          origin={76,-96})));
    Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T7(
      y(start=false),
      uLow=2,
      uHigh=6) "Check if T1 - T7 is larger enough"
      annotation (Placement(transformation(extent={{-60,-48},{-44,-32}})));

    Modelica.Blocks.Math.Add Diff_T1_T7(k1=+1, k2=-1)
      annotation (Placement(transformation(extent={{-82,-46},{-70,-34}})));
    Modelica.Blocks.Logical.And condition_solar_direct
      "[V3V_solar and enough_energy_collector]"
      annotation (Placement(transformation(extent={{70,-78},{80,-68}})));

    Modelica.Blocks.Math.BooleanToInteger S2_State annotation (Placement(
          transformation(
          extent={{6,-6},{-6,6}},
          rotation=90,
          origin={14,-56})));
    Modelica.Blocks.Logical.OnOffController need_heat(each bandwidth=0.5)
      annotation (Placement(transformation(extent={{-46,60},{-34,72}})));
    Modelica.Blocks.Logical.OnOffController need_overheat(each bandwidth=1)
      annotation (Placement(transformation(extent={{-46,40},{-34,52}})));
    Modelica.Blocks.Logical.Hysteresis enough_energy_storage(uLow=
          temp_min_storage - 5, uHigh=temp_min_storage)
      annotation (Placement(transformation(extent={{-80,0},{-60,20}})));

    Modelica.Blocks.MathBoolean.Or
                                S2_state(nu=3)
      "S2 on if at least one room need heating and we have enough solar energy" annotation (
        Placement(transformation(
          extent={{-8,-8},{8,8}},
          rotation=-90,
          origin={14,-32})));

  //
  // FSM
  //
    Modelica_StateGraph2.Step overheat_ON(
      initialStep=false,
      use_activePort=true,
      nIn=1,
      nOut=1) "We can use direct solar energy to overheat the room" annotation (
        Placement(transformation(
          extent={{4,-4},{-4,4}},
          rotation=0,
          origin={40,-6})));
    Modelica_StateGraph2.Transition use_solar_direct_energy(
      use_firePort=false,
      delayedTransition=false,
      use_conditionPort=true)
      "Transition condition from heating_on to heating_SolarDirect"
      annotation (Placement(transformation(extent={{96,30},{88,38}})));
    Modelica_StateGraph2.Step heat_OFF(
      initialStep=true,
      use_activePort=false,
      nIn=1,
      nOut=1)
             "We do not need heating anymore"
      annotation (Placement(transformation(extent={{100,100},{108,108}})));
    Modelica_StateGraph2.Transition not_need_heat(
      use_firePort=false,
      use_conditionPort=true,
      delayedTransition=false)
      "Suspender transition condition from Heat_ON_globalState"
      annotation (Placement(transformation(extent={{36,22},{44,30}})));
    Modelica_StateGraph2.Step solar_direct_ON(
      nIn=1,
      initialStep=false,
      use_activePort=true,
      nOut=1) "We can use direct solar energy to heat the room"
      annotation (Placement(transformation(extent={{96,6},{88,14}})));
    Modelica_StateGraph2.Step solar_indirect_ON(
      nIn=1,
      initialStep=false,
      use_activePort=true,
      nOut=1) "We can use storage tank energy to heat the room"
      annotation (Placement(transformation(extent={{132,2},{124,10}})));
    Modelica_StateGraph2.Transition use_solar_indirect_energy(
      use_firePort=false,
      delayedTransition=false,
      use_conditionPort=true)
      "Transition condition from heating_on to heating_SolarIndirect"
      annotation (Placement(transformation(extent={{126,30},{118,38}})));
    Modelica_StateGraph2.Transition need_heat_or_overheat(
      use_firePort=false,
      use_conditionPort=true,
      delayedTransition=true,
      waitTime=60) "Transition condition from heating_off to heating_on"
      annotation (Placement(transformation(extent={{100,82},{108,90}})));
    Modelica_StateGraph2.Parallel heat_ON_globalState(
      use_suspend=true,
      nSuspend=1,
      nIn=1,
      nEntry=1)
      "Parent state that loop until we do not need to heat the room. Each internal branche can have a state active."
      annotation (Placement(transformation(extent={{52,-6},{156,74}})));
    Modelica_StateGraph2.Step solar_OFF(
      initialStep=false,
      use_activePort=false,
      nIn=3,
      nOut=2) "We need heating but there is no enough solar energy"
      annotation (Placement(transformation(extent={{100,48},{108,56}})));
    Modelica_StateGraph2.Transition not_use_solar_direct_energy(
      use_firePort=false,
      delayedTransition=false,
      use_conditionPort=true)
      "Transition condition from solar_direct_ON to solar_OFF"
      annotation (Placement(transformation(extent={{68,38},{76,30}})));
    Modelica_StateGraph2.Transition not_use_solar_indirect_energy(
      use_firePort=false,
      delayedTransition=false,
      use_conditionPort=true)
      "Transition condition from solar_indirect_ON to solar_OFF"
      annotation (Placement(transformation(extent={{144,38},{152,30}})));
    Modelica_StateGraph2.Transition need_heat_or_notOverheat_or_no_enough_solar_energy(
      use_firePort=false,
      use_conditionPort=true,
      delayedTransition=true,
      waitTime=60)
      "Transition condition from overheat_ON to heat_OFF. We do not need to overheat or we need to heat the room."
      annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=180,
          origin={172,54})));


  equation

    connect(Tamb, need_heat.u) annotation (Line(points={{-107,51},{-56,51},{-56,60},
            {-56,62.4},{-47.2,62.4}},
          color={0,0,127}));
    connect(Tamb, need_overheat.u) annotation (Line(points={{-107,51},{-56,51},{-56,
            42.4},{-47.2,42.4}},
          color={0,0,127}));
    connect(heating_setpoint_shift.y, need_heat.reference) annotation (Line(points={{-61.8,
            102},{-52,102},{-52,100},{-52,69.6},{-47.2,69.6}},
                                           color={0,0,127}));
    connect(overheating_setpoint_shifted.y, need_overheat.reference) annotation (Line(points={{-61.8,
            82},{-61.8,82},{-54,82},{-54,49.6},{-47.2,49.6}}, color={0,0,127}));
    connect(use_solar_direct_energy.outPort, solar_direct_ON.inPort[1])
      annotation (Line(points={{92,29},{92,22},{92,14}}, color={0,0,0}));
    connect(use_solar_indirect_energy.outPort, solar_indirect_ON.inPort[1])
      annotation (Line(points={{122,29},{122,26},{128,26},{128,10}}, color={0,0,0}));
    connect(solar_indirect_ON.outPort[1], not_use_solar_indirect_energy.inPort)
      annotation (Line(points={{128,1.4},{128,1.4},{128,-4},{148,-4},{148,30}},
          color={0,0,0}));
    connect(solar_direct_ON.outPort[1], not_use_solar_direct_energy.inPort)
      annotation (Line(points={{92,5.4},{92,-4},{72,-4},{72,30}}, color={0,0,0}));
    connect(heat_ON_globalState.suspend[1], not_need_heat.inPort)
      annotation (Line(points={{49.4,56.5},{40,56.5},{40,30}}, color={0,0,0}));
    connect(not_need_heat.outPort, overheat_ON.inPort[1])
      annotation (Line(points={{40,21},{40,21},{40,-2}}, color={0,0,0}));
    connect(need_heat_or_overheat.outPort,heat_ON_globalState. inPort[1])
      annotation (Line(points={{104,81},{104,74}}, color={0,0,0}));
    connect(overheat_ON.outPort[1],
      need_heat_or_notOverheat_or_no_enough_solar_energy.inPort) annotation (
        Line(points={{40,-10.6},{40,-14},{172,-14},{172,50}}, color={0,0,0}));
    connect(solar_indirect_ON.activePort, S2_state.u[1]) annotation (Line(
        points={{123.28,6},{114,6},{114,-20},{16,-20},{16,-24},{17.7333,-24}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(solar_direct_ON.activePort, S2_state.u[2]) annotation (Line(
        points={{87.28,10},{80,10},{80,-18},{14,-18},{14,-24}},
        color={255,0,255},
        smooth=Smooth.Bezier));

    connect(condition_no_heat.y, not_need_heat.conditionPort) annotation (Line(
        points={{28.6,46},{32,46},{32,26},{35,26}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(T5, enough_energy_storage.u)
      annotation (Line(points={{-106,10},{-82,10}}, color={0,0,127}));

    connect(overheat_ON.activePort, S2_state.u[3]) annotation (Line(
        points={{35.28,-6},{35.28,-14},{12,-14},{12,-24},{10.2667,-24}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(not_use_solar_direct_energy.outPort, solar_OFF.inPort[1]) annotation (
       Line(points={{72,39},{72,39},{72,60},{102.667,60},{102.667,56}}, color={0,0,
            0}));
    connect(solar_OFF.outPort[1], use_solar_direct_energy.inPort)
      annotation (Line(points={{103,47.4},{92,47.4},{92,38}}, color={0,0,0}));
    connect(solar_OFF.outPort[2], use_solar_indirect_energy.inPort) annotation (
        Line(points={{105,47.4},{122,47.4},{122,40},{122,38}}, color={0,0,0}));
    connect(heat_ON_globalState.entry[1], solar_OFF.inPort[2])
      annotation (Line(points={{104,70},{104,56}}, color={0,0,0}));
    connect(not_use_solar_indirect_energy.outPort, solar_OFF.inPort[3])
      annotation (Line(points={{148,39},{148,39},{148,60},{105.333,60},{105.333,
            56}},
          color={0,0,0}));
    connect(condition_direct_heat_off.y, not_use_solar_direct_energy.conditionPort)
      annotation (Line(
        points={{92,-45.4},{92,-45.4},{92,-38},{62,-38},{62,34},{67,34}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(need_heat_or_notOverheat_or_no_enough_solar_energy.outPort,
      heat_OFF.inPort[1]) annotation (Line(points={{172,59},{172,59},{172,116},
            {104,116},{104,108}}, color={0,0,0}));
    connect(condition_need_heat_or_overheat.y, need_heat_or_overheat.conditionPort)
      annotation (Line(
        points={{28.6,66},{40,66},{40,86},{99,86}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(heat_OFF.outPort[1], need_heat_or_overheat.inPort) annotation (Line(
        points={{104,99.4},{104,94.7},{104,90}},
        color={0,0,0},
        smooth=Smooth.Bezier));
    connect(condition_heat_no_overheat.y,
      need_heat_or_notOverheat_or_no_enough_solar_energy.conditionPort)
      annotation (Line(
        points={{166.5,-116},{194,-116},{194,54},{177,54}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(condition_solar_indirect.y, condition_indirect_heat_off.u)
      annotation (Line(
        points={{126.6,-96},{126.6,-96},{148,-96},{148,-61.2}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(condition_solar_indirect.y, use_solar_indirect_energy.conditionPort)
      annotation (Line(
        points={{126.6,-96},{126.6,-96},{138,-96},{138,34},{127,34}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(condition_indirect_heat_off.y, not_use_solar_indirect_energy.conditionPort)
      annotation (Line(
        points={{148,-47.4},{140,-47.4},{140,34},{143,34}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(T7, Diff_T1_T7.u2) annotation (Line(points={{-104,-52},{-94,-52},{-94,
            -43.6},{-83.2,-43.6}}, color={0,0,127}));
    connect(Diff_T1_T7.y, enoughDelta_T1_T7.u)
      annotation (Line(points={{-69.4,-40},{-61.6,-40}}, color={0,0,127}));
    connect(condition_solar_direct.y, condition_direct_heat_off.u) annotation (
        Line(points={{80.5,-73},{92,-73},{92,-59.2}}, color={255,0,255}));
    connect(condition_solar_direct.y, use_solar_direct_energy.conditionPort)
      annotation (Line(
        points={{80.5,-73},{106,-73},{106,34},{97,34}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(busCon.V3V_solar_state, condition_solar_direct.u2) annotation (Line(
        points={{0.1,-95.9},{26,-95.9},{26,-78},{26,-77},{69,-77}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(enoughDelta_T1_T7.y, condition_solar_direct.u1) annotation (Line(
          points={{-43.2,-40},{-40,-40},{-40,-68},{60,-68},{60,-72},{64,-72},{64,-73},
            {69,-73}},                   color={255,0,255}));
    connect(need_overheat.y, condition_need_heat_or_overheat.u2) annotation (Line(
          points={{-33.4,46},{-20,46},{-20,48},{-20,61.2},{14.8,61.2}},
          color={255,0,255}));
    connect(need_overheat.y, busCon.need_overheat) annotation (Line(points={{-33.4,
            46},{-20,46},{-20,12},{-20,-95.9},{0.1,-95.9}}, color={255,0,255},
        smooth=Smooth.Bezier),
        Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}}));
    connect(need_heat.y, condition_no_heat.u) annotation (Line(points={{-33.4,66},
            {-14,66},{-14,46},{14.8,46}}, color={255,0,255}));
    connect(need_heat.y, condition_need_heat_or_overheat.u1) annotation (Line(
          points={{-33.4,66},{14.8,66}},           color={255,0,255}));
    connect(need_heat.y, busCon.need_heat) annotation (Line(points={{-33.4,66},{-33.4,
            66},{-14,66},{-14,-22},{-14,-95.9},{0.1,-95.9}}, color={255,0,255},
        smooth=Smooth.Bezier),
        Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}}));

    connect(T1, Diff_T1_T7.u1) annotation (Line(points={{-106,-24},{-96,-24},{-96,
            -36.4},{-83.2,-36.4}}, color={0,0,127}));
    connect(Schedules[1], heating_setpoint_shift.u) annotation (Line(points={{-106,
            84},{-106,84},{-106,92},{-104,92},{-104,92},{-98,92},{-86,92},{-86,102},
            {-66.4,102}}, color={0,0,127}));
    connect(Schedules[2], overheating_setpoint_shifted.u) annotation (Line(points=
           {{-106,92},{-86,92},{-86,82},{-66.4,82}}, color={0,0,127}));
    connect(overheat_ON.activePort, busCon.overheating_ON) annotation (Line(
          points={{35.28,-6},{-10,-6},{-10,-76},{-8,-76},{-8,-86},{0.1,-86},{0.1,-95.9}},
          color={255,0,255},
        smooth=Smooth.Bezier),Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}}));
    connect(S2_state.y, S2_State.u) annotation (Line(
        points={{14,-41.2},{14,-48.8}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(S2_State.y, busCon.S2_state) annotation (Line(
        points={{14,-62.6},{14,-68},{14,-95.9},{0.1,-95.9}},
        color={255,127,0},
        smooth=Smooth.Bezier), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}}));
    annotation (Diagram(coordinateSystem(extent={{-100,-120},{200,120}})), Icon(coordinateSystem(
            extent={{-100,-120},{200,120}})),
      Documentation(info="<html>
</html>"));
  end Heating_fsm;

  block Fan_fsm
    "FSM for the fan state (Modulation on temperature or mass flow rate)"
    extends Base_fsm;
    extends Tools.Air_PID_parameters;

  //
  // General inputs/outputs
  //
    Modelica.Blocks.Interfaces.RealInput Text(unit="K")
      "Temperature outside the room (environnement)"
                                              annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(
          transformation(
          extent={{-20,-20},{20,20}},
          rotation=0,
          origin={-104,6}), iconTransformation(extent={{-14,-14},{14,14}},
          rotation=0,
          origin={-100,38})));
    Modelica.Blocks.Interfaces.RealInput Schedules[3]
      "Schedules (1 == temperature instruction, 2 == overheating tempearture instruction, 3 == minimal mass flow rate)"
      annotation (__Dymola_tag={"Temperature","Computed"}, Placement(
          transformation(extent={{-14,-14},{14,14}}, origin={-104,90}),
          iconTransformation(extent={{-14,-14},{14,14}}, origin={-98,102})));
    Modelica.Blocks.Interfaces.RealOutput fan_flowRate(each unit="m3/s")
      "Volumic fan flow rate [m3/s]"  annotation (Placement(transformation(extent=
             {{192,66},{222,96}}), iconTransformation(extent={{192,74},{216,98}})));
      Modelica.Blocks.Interfaces.RealInput Tamb(unit="K")
      "Vector of temperature inside rooms areas" annotation (__Dymola_tag={"Temperature","Computed"},
        Placement(transformation(extent={{-120,18},{-86,52}}), iconTransformation(
            extent={{-114,-16},{-86,12}})));

    Modelica.Blocks.Interfaces.RealInput Tsouff(each unit="K")
      "Vector of air temperature blowing into the room" annotation (__Dymola_tag={"Temperature",
          "Computed"}, Placement(transformation(extent={{-124,-110},{-84,-70}}),
          iconTransformation(extent={{-112,-116},{-84,-88}})));


  //
  // Fan
  //
    Modelica.Blocks.Logical.Hysteresis need_higher_flow(uLow=Tsouff_max - 1.1,
        uHigh=Tsouff_max - 0.1)
      "Test if we need a higher flow rate to cover the heating demand"
      annotation (Placement(transformation(extent={{6,84},{20,98}})));
    Modelica.Blocks.Logical.OnOffController flow_is_minimal(each bandwidth=10/3600)
      annotation (Placement(transformation(extent={{104,94},{90,108}})));

    Modelica_StateGraph2.Step Modulation_flowRate_ON(
      nOut=1,
      initialStep=false,
      nIn=1,
      use_activePort=true)
      "We need more energy to the room so we allow to increase the fan flow rate."
      annotation (Placement(transformation(extent={{36,64},{44,72}})));
    Modelica_StateGraph2.Transition Tsouff_is_max_and_need_more_energy(
      use_firePort=false,
      use_conditionPort=true,
      delayedTransition=true,
      waitTime=tempo_algo_flowRate)
      "Transition from modulation_temperature_ON to modulation_flowRate_ON"
      annotation (Placement(transformation(extent={{36,80},{44,88}})));
    Modelica_StateGraph2.Step Modulation_temperature_ON(
      nOut=1,
      nIn=1,
      initialStep=true,
      use_activePort=true)
      "We do not need a lot of energy, so only change the blowing temperature setpoint."
      annotation (Placement(transformation(extent={{36,104},{44,112}})));
    Modelica_StateGraph2.Transition flowRate_is_minimal(
      use_firePort=false,
      use_conditionPort=true,
      delayedTransition=true,
      waitTime=tempo_algo_flowRate)
      "Transition from Modulation_flowRate_ON to modulation_temperature_ON."
      annotation (Placement(transformation(extent={{74,90},{66,82}})));

    Buildings.Controls.Continuous.LimPID pid_fan(
      controllerType=fan_controllerType,
      k=fan_k,
      Ti=fan_Ti,
      Td=fan_Td,
      each yMax=1,
      each yMin=0,
      reverseAction=false)
                   "PID used to control blowing flow rate for each room"
      annotation (Placement(transformation(extent={{112,62},{132,82}})));
     Buildings.Controls.Continuous.LimPID pid_Tsouff(
      controllerType=Tsouff_controllerType,
      k=Tsouff_k,
      Ti=Tsouff_Ti,
      Td=Tsouff_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
      "PID which control the blowing temperature instruction"
      annotation (Placement(transformation(extent={{160,44},{182,66}})));

    Modelica.Blocks.Sources.RealExpression  Tsouff_instruction(y=smooth(1, if
          Modulation_temperature_ON.activePort then (Text + pid_Tsouff.y*(
          Tsouff_max - Text)) else Tsouff_max))
      "We first check the difference between max blowing temperature and environment temperature, then we apply the PID factor to it. So we compute blowing temperature as Text + PID factor * delta(Tmax - Text)"
      annotation (Placement(transformation(extent={{-62,78},{-30,104}})));

    Modelica.Blocks.Logical.Switch Overheating_switch
      "Selector between two kind of schedule for temperature instruction"
      annotation (Placement(transformation(extent={{96,38},{112,54}})));


  //
  // Electrical
  //
    Modelica_StateGraph2.Step electrical_ON(
      nOut=1,
      initialStep=false,
      nIn=1,
      use_activePort=true) "We need heating and solar energy is not enough"
      annotation (Placement(transformation(extent={{88,-76},{80,-68}})));
    Modelica_StateGraph2.Transition NeedElectrical_auxiliary(
      use_firePort=false,
      use_conditionPort=true,
      delayedTransition=true,
      waitTime=tempo_algo_elec)
      "Transition condition from electrical_OFF to electrical_ON when solar energy is not enough"
      annotation (Placement(transformation(extent={{80,-56},{88,-48}})));
    Modelica_StateGraph2.Step electrical_OFF(
      nOut=1,
      use_activePort=false,
      nIn=1,
      initialStep=true)
      "We need heating but we need to wait to see if solar energy is enough"
      annotation (Placement(transformation(extent={{80,-40},{88,-32}})));
    Modelica_StateGraph2.Transition NotNeedElectrical_auxiliary(
      use_firePort=false,
      use_conditionPort=true,
      delayedTransition=true,
      waitTime=tempo_algo_elec)
      "Transition condition from electrical_ON to electrical_OFF."
      annotation (Placement(transformation(extent={{122,-48},{114,-56}})));
    Modelica.Blocks.Logical.And NeedElectricalHeat
      annotation (Placement(transformation(extent={{36,-44},{52,-28}})));
    Modelica.Blocks.Logical.OnOffController need_elec_to_air(each bandwidth=2)
      "Check if solar energy gives enough power to the blowing air source"
      annotation (Placement(transformation(extent={{-36,-44},{-20,-28}})));
    Utilities.Other.Shift Tinstruction_souff_shift(k=-need_elec_to_air.bandwidth/2)
      "Shift down reference temperature because exchanger outlet temperature cannot be higher than maximal blowing temperature value"
      annotation (Placement(transformation(extent={{-6,-6},{6,6}},
          rotation=-90,
          origin={-50,10})));
    Modelica.Blocks.Logical.Not NotNeedElectricalHeat
      "No need for electrical heat anymore" annotation (Placement(transformation(
          extent={{8,-8},{-8,8}},
          rotation=-90,
          origin={134,-84})));
    Modelica.Blocks.Interfaces.RealInput Texch_out(unit="K")
      "Temperature after the solar exchanger" annotation (__Dymola_tag={"Temperature","Computed"},
        Placement(transformation(extent={{-19,-19},{19,19}}, origin={-103,-41}),
          iconTransformation(extent={{-14,-14},{14,14}}, origin={-98,-66})));
      Buildings.Controls.Continuous.LimPID pid_heater_elec(
      controllerType=elecHeat_controllerType,
      k=elecHeat_k,
      Ti=elecHeat_Ti,
      Td=elecHeat_Td,
      yMax=1,
      yMin=0,
      reverseAction=false)
                          "Controller for electric heater on blowing air"
      annotation (Placement(transformation(extent={{120,-10},{140,10}})));
    Modelica.Blocks.Interfaces.RealOutput power_air_elec(each unit="W")
      "Output the power needed to keep room air temperature at setpoint"
      annotation (__Dymola_tag={"Power","Electric"}, Placement(transformation(
            extent={{188,-86},{216,-58}}), iconTransformation(extent={{192,-82},{216,
              -58}})));


  equation
    //
    //Control of blowing flow rate/temperature
    //
      //Minimal flow rate reference
      flow_is_minimal.reference = Schedules[3] * 1.5;
      flow_is_minimal.u = fan_flowRate;

    //
    // BLOWING VOLUMIC FLOW RATE COMPUTATION
    //
      // Compute volumic flow rate as max(minimal flow rate, (maximal flow rate times PID factor if modulation of flow rate ON)
      fan_flowRate = Buildings.Utilities.Math.Functions.smoothMax(x1=fan_flowRate_max * (if Modulation_flowRate_ON.activePort then pid_fan.y else 0),
                                                                  x2=Schedules[3],
                                                                  deltaX=0.001);

       // Compute electric power needed to get blowing temperature instruction
      power_air_elec = smooth(1, if electrical_ON.activePort then (heater_elec_power * pid_heater_elec.y) else 0);
    connect(Modulation_temperature_ON.outPort[1],
      Tsouff_is_max_and_need_more_energy.inPort)
      annotation (Line(points={{40,103.4},{40,95.7},{40,88}},color={0,0,0}));
    connect(Tsouff_is_max_and_need_more_energy.outPort, Modulation_flowRate_ON.inPort[
      1]) annotation (Line(points={{40,79},{40,72}},                 color={0,0,
            0}));
    connect(Modulation_flowRate_ON.outPort[1], flowRate_is_minimal.inPort)
      annotation (Line(points={{40,63.4},{40,60},{70,60},{70,82}}, color={0,0,0}));
    connect(flowRate_is_minimal.outPort, Modulation_temperature_ON.inPort[1])
      annotation (Line(points={{70,91},{70,91},{70,120},{40,120},{40,112}},
          color={0,0,0}));
    connect(need_higher_flow.y, Tsouff_is_max_and_need_more_energy.conditionPort)
      annotation (Line(
        points={{20.7,91},{24,91},{24,84},{35,84}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(Tamb, pid_Tsouff.u_m) annotation (Line(points={{-103,35},{171,35},{171,
            41.8}},  color={0,0,127}));
    connect(Schedules[1], pid_fan.u_s) annotation (Line(points={{-104,80.6667},
            {-104,80.6667},{-104,90},{-80,90},{-80,56},{80,56},{80,72},{110,72}},
                                                            color={0,0,127}));
    connect(Tamb, pid_fan.u_m) annotation (Line(points={{-103,35},{-103,35},{122,35},
            {122,60}},           color={0,0,127}));
    connect(flow_is_minimal.y, flowRate_is_minimal.conditionPort) annotation (
        Line(
        points={{89.3,101},{86.65,101},{86.65,86},{75,86}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(electrical_OFF.outPort[1], NeedElectrical_auxiliary.inPort)
      annotation (Line(points={{84,-40.6},{84,-40.6},{84,-48}}, color={0,0,0}));
    connect(NeedElectrical_auxiliary.outPort, electrical_ON.inPort[1])
      annotation (Line(points={{84,-57},{84,-68}}, color={0,0,0}));
    connect(electrical_ON.outPort[1], NotNeedElectrical_auxiliary.inPort)
      annotation (Line(points={{84,-76.6},{84,-76.6},{84,-84},{118,-84},{118,-56}},
          color={0,0,0}));
    connect(NotNeedElectrical_auxiliary.outPort, electrical_OFF.inPort[1])
      annotation (Line(points={{118,-47},{118,-47},{118,-24},{84,-24},{84,-32}},
          color={0,0,0}));
    connect(NeedElectricalHeat.y, NeedElectrical_auxiliary.conditionPort)
      annotation (Line(
        points={{52.8,-36},{52.8,-36},{66,-36},{66,-52},{79,-52}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(need_elec_to_air.y,NeedElectricalHeat. u1) annotation (Line(points={{-19.2,
            -36},{34.4,-36}},                  color={255,0,255}));
    connect(Tinstruction_souff_shift.y,need_elec_to_air. reference) annotation (
        Line(points={{-50,3.4},{-50,3.4},{-50,-31.2},{-37.6,-31.2}},
                                                                 color={0,0,127}));
    connect(NotNeedElectricalHeat.y, NotNeedElectrical_auxiliary.conditionPort)
      annotation (Line(
        points={{134,-75.2},{134,-75.2},{134,-52},{123,-52}},
        color={255,0,255},
        smooth=Smooth.Bezier));
    connect(NeedElectricalHeat.y, NotNeedElectricalHeat.u) annotation (Line(
          points={{52.8,-36},{58,-36},{58,-104},{134,-104},{134,-93.6}}, color={255,0,
            255},
        smooth=Smooth.Bezier));
    connect(Tsouff_instruction.y, need_higher_flow.u)
      annotation (Line(points={{-28.4,91},{4.6,91}}, color={0,0,127}));
    connect(Tsouff_instruction.y, Tinstruction_souff_shift.u) annotation (Line(
          points={{-28.4,91},{-4,91},{-4,20},{-50,20},{-50,17.2}},
                                                                color={0,0,127}));
    connect(Tsouff_instruction.y, pid_heater_elec.u_s) annotation (Line(points={{-28.4,
            91},{-4,91},{-4,0},{118,0}},
                                       color={0,0,127}));
    connect(Tsouff_instruction.y, busCon.Tsouff_instruction) annotation (Line(
          points={{-28.4,91},{-4,91},{-4,90},{-4,-74},{0.1,-74},{0.1,-95.9}},
                                                    color={0,0,127},
        smooth=Smooth.Bezier),                                        Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}}));
    connect(busCon.need_heat, NeedElectricalHeat.u2) annotation (Line(
        points={{0.1,-95.9},{0,-95.9},{0,-42},{34.4,-42},{34.4,-42.4}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(Tsouff, pid_heater_elec.u_m) annotation (Line(points={{-104,-90},{-76,
            -90},{-60,-90},{-60,-16},{130,-16},{130,-12}}, color={0,0,127}));
    connect(Texch_out, need_elec_to_air.u) annotation (Line(points={{-103,-41},{-70.5,
            -41},{-70.5,-40.8},{-37.6,-40.8}}, color={0,0,127}));
    connect(Schedules[2], Overheating_switch.u1) annotation (Line(points={{-104,90},
            {-104,90},{-80,90},{-80,56},{40,56},{40,52.4},{94.4,52.4}},
                                                                  color={0,0,127}));
    connect(Schedules[1], Overheating_switch.u3) annotation (Line(points={{-104,
            80.6667},{-104,80.6667},{-104,90},{-80,90},{-80,56},{40,56},{40,
            39.6},{94.4,39.6}},
          color={0,0,127}));
    connect(busCon.overheating_ON, Overheating_switch.u2) annotation (Line(
        points={{0.1,-95.9},{0.1,-40},{0,-40},{0,46},{94.4,46}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(Overheating_switch.y, pid_Tsouff.u_s) annotation (Line(points={{112.8,
            46},{148,46},{148,55},{157.8,55}}, color={0,0,127}));
    annotation (Diagram(coordinateSystem(extent={{-100,-120},{200,120}}),
          graphics={                              Rectangle(
            extent={{-80,24},{200,-68}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={147,161,161}),             Rectangle(
            extent={{32,-68},{200,-110}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={147,161,161}),             Rectangle(
            extent={{-80,120},{200,24}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={238,232,213}),
          Text(
            extent={{130,-16},{196,-48}},
            lineColor={38,139,210},
            textString="Electrical"),
          Text(
            extent={{126,116},{180,90}},
            lineColor={38,139,210},
            textString="Fan")}),                                           Icon(
          coordinateSystem(extent={{-100,-120},{200,120}})));
  end Fan_fsm;

  partial block Base_fsm "Abstract class for a functionnal state machine"
    extends Modelica.Blocks.Icons.Block;

    Tools.BusAlgoController busCon "Share all states inside a single connector"
      annotation (Placement(transformation(extent={{-20,-116},{20,-76}}),
          iconTransformation(extent={{44,-122},{64,-102}})));

    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-120},
              {200,120}}),                                        graphics={
                                                  Rectangle(
            extent={{-100,120},{200,-120}},
            lineColor={0,0,255},
            fillPattern=FillPattern.Solid,
            fillColor={253,246,227})}), Diagram(coordinateSystem(preserveAspectRatio=false, extent={
              {-100,-120},{200,120}})));
  end Base_fsm;

  package Tools
    expandable connector BusAlgoController
      "Expandable Bus controller with default fields."

      import SI = Modelica.SIunits;

      extends Modelica.Icons.SignalBus;
      // Boolean
      Boolean V3V_solar_state "Three ways valve connecting solar collectors and storage tank position (1=open to collectors)";
      Boolean need_heat "Flag used to define if we need or not to heat the house";
      Boolean need_overheat "Flag used to define if we can use direct solar energy to store more power using house inertia";
      Boolean need_electrical_heat "Flag used to activate electrical terminals if solar energy is not enough (True == ON)";
      Boolean overheating_ON "Flag True if Overheating is ON else False";
      // Real
      SI.ThermodynamicTemperature Tsouff_instruction "Temperature instruction for air.";
      // Integer
      Integer S5_state "DHW tank pump state (1 == ON)";
      Integer S6_state "Storage pump state (1 == ON)";
      Integer S2_state "Exchanger pump state (1 == ON)";
      annotation (
        Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,
                100}}), graphics={Rectangle(
              extent={{-20,2},{22,-2}},
              lineColor={255,204,51},
              lineThickness=0.5)}),
        Documentation(info="<html>
<p>
This connector defines the <code>expandable connector</code> ControlBus that
is used to connect control signals.
Note, this connector is empty. When using it, the actual content is
constructed by the signals connected to this bus.
</p>
</html>"));

    end BusAlgoController;

    record System_parameters "Group of parameters system temperature control"

      extends S5_S6_V3V_parameters;
      extends Pump_parameters;
      extends Air_PID_parameters;
      extends Heating_parameters;


      import SI = Modelica.SIunits;

    //
    // Schedules for temperature setpoint, solar setpoint and minimal volumic flow rate
    //
       parameter Boolean tableOnFile=false
        "= true, if table is defined on file or in function usertab"
         annotation (Dialog(tab="Schedules", group="Tables data definition"));
         parameter Real table[:, :] = fill(0.0, 0, 3)
        "Setpoint for the algorithm control (first=time). 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s])"
           annotation (Dialog(tab="Schedules",group="Tables data definition",enable=not tableOnFile));
       parameter String tableName="NoName"
        "Tables names on file or in function usertab (see docu)"
         annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
       parameter String fileName="NoName"
        "Files where matrix are stored"
         annotation (Dialog(tab="Schedules",
           group="Tables data definition",
           enable=tableOnFile,
           loadSelector(filter="Text files (*.txt);;MATLAB MAT-files (*.mat)",
               caption="Open file in which table is present")));
       parameter Boolean verboseRead=true
        "= true, if info message that file is loading is to be printed"
         annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
        parameter Integer columns[:]=2:3+1
        "Columns of table to be interpolated"
          annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.Blocks.Types.Smoothness smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments
        "Smoothness of table interpolation"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.Blocks.Types.Extrapolation extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic
        "Extrapolation of data outside the definition range"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Real offset[:]=fill(0, 3)
        "Offsets of output signals"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
       parameter Modelica.SIunits.Time startTime=0
        "Output = offset for time < startTime"
         annotation (Dialog(tab="Schedules",group="Tables data interpretation"));


    //
    // Describe the control of the electrical extra heating for DHW
    //
      parameter SI.ThermodynamicTemperature DHW_setpoint = 328.15
        "Temperature setpoint for DHW" annotation (Dialog(group="Instructions"));
      // DHW tank electrical control
      parameter Modelica.Blocks.Types.SimpleController elecDHW_controllerType=Modelica.Blocks.Types.SimpleController.P
        "Type of controller" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Real elecDHW_k=10 "Gain of controller" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Modelica.SIunits.Time elecDHW_Ti=0.1
        "Time constant of Integrator block" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter Modelica.SIunits.Time elecDHW_Td=0.1
        "Time constant of Derivative block" annotation (Dialog(tab="Electric heaters", group="PID_DHW"));
      parameter SI.Power DHW_elec_power = 3000
        "Maximum power of the extra electrical heater for DHW" annotation (Dialog(tab="Electric heaters"));


      annotation (Diagram(coordinateSystem(extent={{-1480,360},{-1340,460}})), Icon(coordinateSystem(
              extent={{-1480,360},{-1340,460}})));
    end System_parameters;

    record S5_S6_V3V_parameters
      "Group of parameters system temperature control"

      import SI = Modelica.SIunits;


    //
    // Solar valve (V3V)
    //

      parameter SI.ThermodynamicTemperature temp_min_collector = 313.15
        "Minimum collector temperature in order to use direct solar energy" annotation (Dialog(group="Lower limit"));

    //
    // Pump S5 (pump for DHW tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_DHW = 363.15
        "Maximum admissible value for DHW tank" annotation (Dialog(group="Higher limit"));

    //
    // Pump S6 (pump for Storage tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_storage = 378.15
        "Maximum admissible value for Storage tank" annotation (Dialog(group="Higher limit"));
      parameter SI.ThermodynamicTemperature temp_min_DHW = 303.15
        "Minimum admissible value for DHW tank before allowing Storage tank loading" annotation (Dialog(group="Lower limit"));

      annotation (Diagram(coordinateSystem(extent={{-1480,360},{-1340,460}})), Icon(coordinateSystem(
              extent={{-1480,360},{-1340,460}})));
    end S5_S6_V3V_parameters;

    record Air_PID_parameters
      "Group of parameters system for air PID definition"

      import SI = Modelica.SIunits;

      //
      //Control of blowing flow rate/temperature
      //
      parameter SI.ThermodynamicTemperature Tsouff_max= 305.15
        "Maximum value of blowing temperature for the room"
        annotation (Dialog(tab="Blowing Temperatures"));
      parameter Real fan_flowRate_max=900/3600
        "Upper limit of fan flow rate." annotation (Dialog(tab="Fans"));
      parameter SI.Time tempo_algo_flowRate= 600
        "How much time we have to wait before switching between Temperature or flow regulation"
        annotation (Dialog(group="Instructions"));



        // FANS PID parameters
      parameter Modelica.Blocks.Types.SimpleController fan_controllerType=Modelica.Blocks.Types.SimpleController.PID
        "Type of controller" annotation (Dialog(tab="Fans", group="PID controller"));
      parameter Real fan_k=10 "Gain of controller" annotation (Dialog(tab="Fans", group="PID controller"));
      parameter Modelica.SIunits.Time fan_Ti=60
        "Time constant of Integrator block" annotation (Dialog(tab="Fans", group="PID controller"));
      parameter Modelica.SIunits.Time fan_Td=60
        "Time constant of Derivative block" annotation (Dialog(tab="Fans", group="PID controller"));

      // BLOWING TEMPERATURES INSTRUCTION PID parameters
      parameter Modelica.Blocks.Types.SimpleController Tsouff_controllerType=Modelica.Blocks.Types.SimpleController.PI
        "Type of controller"
        annotation (Dialog(tab="Blowing Temperatures", group="PID controller"));
      parameter Real Tsouff_k=10 "Gain of controller"
      annotation (Dialog(tab="Blowing Temperatures", group="PID controller"));
      parameter Modelica.SIunits.Time Tsouff_Ti=400
        "Time constant of Integrator block"
        annotation (Dialog(tab="Blowing Temperatures", group="PID controller"));
      parameter Modelica.SIunits.Time Tsouff_Td=0.1
        "Time constant of Derivative block"
        annotation (Dialog(tab="Blowing Temperatures", group="PID controller"));

      //
      //Control of blowing electric heater
      //
        parameter Modelica.SIunits.Power heater_elec_power=3000
        "Maximum power of the extra electrical heater for air blowing heating"
        annotation (Dialog(tab="Electric heaters"));
        parameter SI.Time tempo_algo_elec= 180
        "How much time we have to wait before using electric heater"
        annotation (Dialog(group="Instructions"));
        // ELECTRICAL HEATER PID parameters
       parameter Modelica.Blocks.Types.SimpleController elecHeat_controllerType=Modelica.Blocks.Types.SimpleController.PI
        "Type of controller" annotation (Dialog(tab="Electric heaters", group="PID_air"));
        parameter Real elecHeat_k=2.5 "Gain of controller" annotation (Dialog(tab="Electric heaters", group="PID_air"));
        parameter Modelica.SIunits.Time elecHeat_Ti=200
        "Time constant of Integrator block" annotation (Dialog(tab="Electric heaters", group="PID_air"));
        parameter Modelica.SIunits.Time elecHeat_Td=0.1
        "Time constant of Derivative block" annotation (Dialog(tab="Electric heaters", group="PID_air"));


      annotation (Diagram(coordinateSystem(extent={{-1480,360},{-1340,460}})), Icon(coordinateSystem(
              extent={{-1480,360},{-1340,460}})));
    end Air_PID_parameters;

    record Pump_parameters
      "Group of parameters system temperature control"

      import SI=Modelica.SIunits;

    //
    // Describe pid pump output construction for S2, S6 and S5
    //
      // S6 PID parameters
      parameter Real S6_delta_min=10
        "Minimal temperature difference between collector and storage tank"
        annotation (Dialog(tab="S6"));
      parameter SI.Conversions.NonSIunits.AngularVelocity_rpm S6_speed_max=3000
        "Maximum value for S6 speed"
        annotation (Dialog(tab="S6"));

          parameter Modelica.Blocks.Types.SimpleController S6_controller=Modelica.Blocks.Types.SimpleController.PI
        "Type of controller" annotation (Dialog(tab="S6", group="PID controller"));
      parameter Real S6_k=2.5 "Gain of controller" annotation (Dialog(tab="S6", group="PID controller"));
      parameter SI.Time S6_Ti=175
        "Time constant of Integrator block"
        annotation (Dialog(tab="S6", group="PID controller"));
      parameter SI.Time S6_Td=0.1
        "Time constant of Derivative block"
        annotation (Dialog(tab="S6", group="PID controller"));
         parameter Real S6_yMax=1 "Upper limit of output" annotation (Dialog(tab="S6", group="PID controller"));
      parameter Real S6_yMin=0.25 "Lower limit of output" annotation (Dialog(tab="S6", group="PID controller"));


      // S5 PID parameters
      parameter Real S5_delta_min=10
        "Minimal temperature difference between collector and DHW tank"
        annotation (Dialog(tab="S5"));
      parameter SI.Conversions.NonSIunits.AngularVelocity_rpm S5_speed_max=3000
        "Maximum value for S5 speed"
        annotation (Dialog(tab="S5"));


      parameter Modelica.Blocks.Types.SimpleController S5_controller=Modelica.Blocks.Types.SimpleController.PID
        "Type of controller" annotation (Dialog(tab="S5", group="PID controller"));
      parameter Real S5_k=5 "Gain of controller" annotation (Dialog(tab="S5", group="PID controller"));
      parameter SI.Time S5_Ti=200
        "Time constant of Integrator block"
        annotation (Dialog(tab="S5", group="PID controller"));
      parameter SI.Time S5_Td=20
        "Time constant of Derivative block"
        annotation (Dialog(tab="S5", group="PID controller"));
      parameter Real S5_yMax=1 "Upper limit of output" annotation (Dialog(tab="S5", group="PID controller"));
      parameter Real S5_yMin=0.25 "Lower limit of output" annotation (Dialog(tab="S5", group="PID controller"));



      // S2 PUMPS PID parameters

      parameter SI.Conversions.NonSIunits.AngularVelocity_rpm S2_speed_max= 3000
        "Maximum value for S2 speed"
        annotation (Dialog(tab="S2"));

        parameter Modelica.Blocks.Types.SimpleController S2_controllerType=Modelica.Blocks.Types.SimpleController.PI
        "Type of controller"
       annotation (Dialog(tab="S2", group="PID controller"));
      parameter Real S2_k=10 "Gain of controller"
       annotation (Dialog(tab="S2", group="PID controller"));
      parameter SI.Time S2_Ti=300
        "Time constant of Integrator block"
       annotation (Dialog(tab="S2", group="PID controller"));
      parameter SI.Time S2_Td=0.1
        "Time constant of Derivative block"
       annotation (Dialog(tab="S2", group="PID controller"));
      parameter Real S2_yMax=1 "Upper limit of speed"
       annotation (Dialog(tab="S2", group="PID controller"));
      parameter Real S2_yMin=0.11 "Lower limit of speed"
       annotation (Dialog(tab="S2", group="PID controller"));

      annotation (Diagram(coordinateSystem(extent={{-1480,360},{-1340,460}})), Icon(coordinateSystem(
              extent={{-1480,360},{-1340,460}})));
    end Pump_parameters;

    record Heating_parameters "Group of parameters system temperature control"

      import SI = Modelica.SIunits;

      parameter SI.ThermodynamicTemperature temp_min_storage = 318.15
        "Minimum storage tank temperature in order to use indirect solar energy" annotation (Dialog(group="Lower limit"));

      annotation (Diagram(coordinateSystem(extent={{-1480,360},{-1340,460}})), Icon(coordinateSystem(
              extent={{-1480,360},{-1340,460}})));
    end Heating_parameters;
  end Tools;


  package Tests

    model Test_ensemble

      FSM_Monozone Algorithm_Control_Monozone
        annotation (Placement(transformation(extent={{0,4},{86,56}})));
    equation

      annotation (
        Icon(coordinateSystem(preserveAspectRatio=false)),
        Diagram(coordinateSystem(preserveAspectRatio=false)),
        experiment(StopTime=10000),
        __Dymola_experimentSetupOutput(doublePrecision=true));
    end Test_ensemble;

    model test_un
      Tools.BusAlgoController busCon annotation (Placement(transformation(
              extent={{60,-116},{100,-76}}), iconTransformation(extent={{-10,-106},
                {10,-86}})));
      Modelica_StateGraph2.Step step1(
        initialStep=true,
        nOut=1,
        nIn=1) annotation (Placement(transformation(extent={{-90,80},{-82,88}})));
      Modelica_StateGraph2.Transition T1(
        use_conditionPort=false,
        use_firePort=false,
        delayedTransition=false,
        condition=time > 600)
        annotation (Placement(transformation(extent={{-82,66},{-90,74}})));
      Modelica.Blocks.Logical.GreaterThreshold Noneedheat(threshold=18)
        annotation (Placement(transformation(extent={{40,30},{20,50}})));
      Modelica_StateGraph2.Step chauffageOn(nIn=1, nOut=1)
        annotation (Placement(transformation(extent={{-90,50},{-82,58}})));
      Modelica_StateGraph2.Transition T2(
        use_conditionPort=true,
        delayedTransition=true,
        waitTime=200)
        annotation (Placement(transformation(extent={{-82,36},{-90,44}})));
      Modelica_StateGraph2.Step chauffageOff(nIn=1, nOut=1)
        annotation (Placement(transformation(extent={{-90,14},{-82,22}})));
      Modelica_StateGraph2.Transition T3(delayedTransition=false,
          use_conditionPort=true)
        annotation (Placement(transformation(extent={{-56,64},{-64,56}})));
      Modelica.Blocks.Logical.Not needheat
        annotation (Placement(transformation(extent={{0,50},{-20,70}})));
    equation
      connect(step1.outPort[1], T1.inPort)
        annotation (Line(points={{-86,79.4},{-86,74}}, color={0,0,0}));
      connect(Noneedheat.y, T2.conditionPort)
        annotation (Line(points={{19,40},{-32,40},{-81,40}}, color={255,0,255}));
      connect(T3.outPort, step1.inPort[1]) annotation (Line(points={{-60,65},{-60,
              65},{-60,96},{-60,98},{-86,98},{-86,88}}, color={0,0,0}));
      connect(T1.outPort, chauffageOn.inPort[1])
        annotation (Line(points={{-86,65},{-86,61.5},{-86,58}}, color={0,0,0}));
      connect(chauffageOn.outPort[1], T2.inPort)
        annotation (Line(points={{-86,49.4},{-86,46},{-86,44}}, color={0,0,0}));
      connect(T2.outPort, chauffageOff.inPort[1])
        annotation (Line(points={{-86,35},{-86,35},{-86,22}}, color={0,0,0}));
      connect(chauffageOff.outPort[1], T3.inPort) annotation (Line(points={{-86,
              13.4},{-86,8},{-60,8},{-60,56}}, color={0,0,0}));
      connect(Noneedheat.y, needheat.u) annotation (Line(points={{19,40},{10,40},
              {10,60},{2,60}}, color={255,0,255}));
      connect(needheat.y, T3.conditionPort) annotation (Line(points={{-21,60},{
              -38,60},{-55,60}}, color={255,0,255}));
      connect(busCon.Variable1, Noneedheat.u) annotation (Line(
          points={{80,-96},{62,-96},{62,40},{42,40}},
          color={255,204,51},
          thickness=0.5), Text(
          string="%first",
          index=-1,
          extent={{-6,3},{-6,3}}));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
            coordinateSystem(preserveAspectRatio=false)));
    end test_un;
  end Tests;

  package Backup "Old version"

    block Heating_fsm_back2 "Accept multiple room"
      extends Base_fsm;
      extends Tools.System_parameters;

      import SI = Modelica.SIunits;
      import MBV = Modelica.Math.BooleanVectors;
    //
    // General inputs/outputs
    //
       Modelica.Blocks.Sources.CombiTimeTable             Schedules[n](
        tableOnFile=tableOnFile,
        table=table,
        tableName=tableName,
        fileName=fileName,
        verboseRead=verboseRead,
        columns=columns,
        each smoothness=smoothness,
        each extrapolation=extrapolation,
        offset=offset,
        startTime=startTime)
        "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
         annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(extent={{-100,
                106},{-86,120}})));
      Modelica.Blocks.Routing.RealPassThrough heating_setpoint[n](each y(
          final quantity="ThermodynamicTemperature",
          final unit="K",
          displayUnit="degC",
          min=0)) "Heating_setpoint for rooms temperature"
        annotation (Placement(transformation(extent={{-76,100},{-72,104}})));
      Modelica.Blocks.Routing.RealPassThrough overheating_setpoint[n](each y(
          final quantity="ThermodynamicTemperature",
          final unit="K",
          displayUnit="degC",
          min=0)) "Overheating_setpoint for rooms temperature"
        annotation (Placement(transformation(extent={{-76,80},{-72,84}})));

      Utilities.Other.Shift heating_setpoint_shift[n](k=need_heat.bandwidth/2)
        "Heat temperature setpoint for each room"
        annotation (Placement(transformation(extent={{-66,100},{-62,104}})));
      Utilities.Other.Shift overheating_setpoint_shifted[n](k=need_overheat.bandwidth/2)
        "Heat temperature setpoint for each room"
        annotation (Placement(transformation(extent={{-66,80},{-62,84}})));

      Modelica.Blocks.Interfaces.RealVectorInput Tamb[n](each unit="K")
        "Vector of temperature inside rooms areas"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-122,34},
                {-82,74}}),                                                                                               iconTransformation(
              extent={{-112,82},{-84,110}})));
      Modelica.Blocks.Interfaces.RealInput T5(unit="K")
        "Temperature inside storage tank"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
                {12,12}},
            origin={-106,10}),
            iconTransformation(extent={{-18,-18},{18,18}},
            origin={-92,2})));

    //
    // Boolean logic
    //
      parameter SI.ThermodynamicTemperature temp_min_storage = 318.15
        "Minimum storage tank temperature in order to use indirect solar energy" annotation (Dialog(group="Lower limit"));
      Modelica.Blocks.Logical.Not condition_no_heat "We do not need heat anymore"
        annotation (Placement(transformation(
            extent={{-6,-6},{6,6}},
            rotation=0,
            origin={22,46})));
      Modelica.Blocks.MathBoolean.Or condition_need_heat(nu=1) "We need to heat"
        annotation (Placement(transformation(
            extent={{-6,-6},{6,6}},
            rotation=0,
            origin={-20,66})));
      Modelica.Blocks.MathBoolean.Or condition_need_overheat(nu=1)
        "We need to overheat" annotation (Placement(transformation(
            extent={{-6,-6},{6,6}},
            rotation=0,
            origin={-20,46})));
      Modelica.Blocks.Logical.Or condition_need_heat_or_overheat
        "We need heat or overheat" annotation (Placement(transformation(
            extent={{-6,-6},{6,6}},
            rotation=0,
            origin={22,66})));
      Modelica.Blocks.Logical.Not condition_direct_heat_off annotation (Placement(
            transformation(
            extent={{-6,-6},{6,6}},
            rotation=90,
            origin={92,-52})));
      Modelica.Blocks.Logical.Not condition_indirect_heat_off annotation (Placement(
            transformation(
            extent={{-6,6},{6,-6}},
            rotation=90,
            origin={148,-54})));

      Modelica.Blocks.Sources.BooleanExpression condition_heat_no_overheat(y=(
            condition_need_heat.y and not condition_need_overheat.y) or (
            condition_direct_heat_off.y))
        "Need heating or no Overheating" annotation (Placement(transformation(
            extent={{-65,-10},{65,10}},
            rotation=0,
            origin={95,-116})));

    //
    // FSM
    //
      Modelica.Blocks.Logical.OnOffController need_heat[n](each bandwidth=0.5)
        annotation (Placement(transformation(extent={{-46,60},{-34,72}})));
      Modelica.Blocks.Logical.OnOffController need_overheat[n](each bandwidth=1)
        annotation (Placement(transformation(extent={{-46,40},{-34,52}})));
      Modelica.Blocks.Logical.Hysteresis enough_energy_storage(uLow=
            temp_min_storage - 5, uHigh=temp_min_storage)
        annotation (Placement(transformation(extent={{-80,0},{-60,20}})));

      Modelica.Blocks.MathBoolean.Or
                                  S2_state(nu=3)
        "S2 on if at least one room need heating and we have enough solar energy" annotation (
          Placement(transformation(
            extent={{-8,-8},{8,8}},
            rotation=-90,
            origin={0,-34})));

      Modelica_StateGraph2.Step overheat_ON(
        initialStep=false,
        use_activePort=true,
        nIn=1,
        nOut=1) "We can use direct solar energy to overheat the room" annotation (
          Placement(transformation(
            extent={{4,-4},{-4,4}},
            rotation=0,
            origin={40,-6})));
      Modelica_StateGraph2.Transition use_solar_direct_energy(
        use_firePort=false,
        delayedTransition=false,
        use_conditionPort=true)
        "Transition condition from heating_on to heating_SolarDirect"
        annotation (Placement(transformation(extent={{96,30},{88,38}})));
      Modelica_StateGraph2.Step heat_OFF(
        initialStep=true,
        use_activePort=false,
        nIn=1,
        nOut=1)
               "We do not need heating anymore"
        annotation (Placement(transformation(extent={{100,100},{108,108}})));
      Modelica_StateGraph2.Transition not_need_heat(
        use_firePort=false,
        use_conditionPort=true,
        delayedTransition=false)
        "Suspender transition condition from Heat_ON_globalState"
        annotation (Placement(transformation(extent={{36,22},{44,30}})));
      Modelica_StateGraph2.Step solar_direct_ON(
        nIn=1,
        initialStep=false,
        use_activePort=true,
        nOut=1) "We can use direct solar energy to heat the room"
        annotation (Placement(transformation(extent={{96,6},{88,14}})));
      Modelica_StateGraph2.Step solar_indirect_ON(
        nIn=1,
        initialStep=false,
        use_activePort=true,
        nOut=1) "We can use storage tank energy to heat the room"
        annotation (Placement(transformation(extent={{132,2},{124,10}})));
      Modelica_StateGraph2.Transition use_solar_indirect_energy(
        use_firePort=false,
        delayedTransition=false,
        use_conditionPort=true)
        "Transition condition from heating_on to heating_SolarIndirect"
        annotation (Placement(transformation(extent={{126,30},{118,38}})));
      Modelica_StateGraph2.Transition need_heat_or_overheat(
        use_firePort=false,
        use_conditionPort=true,
        delayedTransition=true,
        waitTime=60) "Transition condition from heating_off to heating_on"
        annotation (Placement(transformation(extent={{100,82},{108,90}})));
      Modelica_StateGraph2.Parallel heat_ON_globalState(
        use_suspend=true,
        nSuspend=1,
        nIn=1,
        nEntry=1)
        "Parent state that loop until we do not need to heat the room. Each internal branche can have a state active."
        annotation (Placement(transformation(extent={{52,-6},{156,74}})));
      Modelica_StateGraph2.Step solar_OFF(
        initialStep=false,
        use_activePort=false,
        nIn=3,
        nOut=2) "We need heating but there is no enough solar energy"
        annotation (Placement(transformation(extent={{100,48},{108,56}})));
      Modelica_StateGraph2.Transition not_use_solar_direct_energy(
        use_firePort=false,
        delayedTransition=false,
        use_conditionPort=true)
        "Transition condition from solar_direct_ON to solar_OFF"
        annotation (Placement(transformation(extent={{68,38},{76,30}})));
      Modelica_StateGraph2.Transition not_use_solar_indirect_energy(
        use_firePort=false,
        delayedTransition=false,
        use_conditionPort=true)
        "Transition condition from solar_indirect_ON to solar_OFF"
        annotation (Placement(transformation(extent={{144,38},{152,30}})));
      Modelica_StateGraph2.Transition need_heat_or_notOverheat_or_no_enough_solar_energy(
        use_firePort=false,
        use_conditionPort=true,
        delayedTransition=true,
        waitTime=60)
        "Transition condition from overheat_ON to heat_OFF. We do not need to overheat or we need to heat the room."
        annotation (Placement(transformation(
            extent={{-4,-4},{4,4}},
            rotation=180,
            origin={172,54})));

      Modelica.Blocks.Sources.BooleanExpression condition_solar_indirect(y=not
            busCon.V3V_solar_state and enough_energy_storage.y)
        "Not V3V and enough energy in collectors to heat the storage tank."
        annotation (Placement(transformation(
            extent={{-46,-10},{46,10}},
            rotation=0,
            origin={76,-96})));
      Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T7(
        y(start=false),
        uLow=2,
        uHigh=6) "Check if T1 - T7 is larger enough"
        annotation (Placement(transformation(extent={{-60,-48},{-44,-32}})));
      Modelica.Blocks.Interfaces.RealInput T1(unit="K")
        "Temperature inside collector"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
                {12,12}},
            origin={-104,-28}),
            iconTransformation(extent={{-18,-18},{18,18}},
            origin={-110,-34})));
      Modelica.Blocks.Interfaces.RealInput T7(unit="K")
        "Water temperature after exchanger"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
                {12,12}},
            origin={-104,-52}),
            iconTransformation(extent={{-18,-18},{18,18}},
            origin={-92,-98})));
      Modelica.Blocks.Math.Add Diff_T1_T7(k1=+1, k2=-1)
        annotation (Placement(transformation(extent={{-82,-46},{-70,-34}})));
      Modelica.Blocks.Logical.And condition_solar_direct
        "[V3V_solar and enough_energy_collector]"
        annotation (Placement(transformation(extent={{70,-78},{80,-68}})));
    equation

      connect(Tamb, need_heat.u) annotation (Line(points={{-102,54},{-56,54},{-56,
              60},{-56,62.4},{-47.2,62.4}},
            color={0,0,127}));
      connect(Tamb, need_overheat.u) annotation (Line(points={{-102,54},{-56,54},
              {-56,42.4},{-47.2,42.4}},
            color={0,0,127}));
      connect(heating_setpoint_shift.y, need_heat.reference) annotation (Line(points={{-61.8,
              102},{-52,102},{-52,100},{-52,69.6},{-47.2,69.6}},
                                             color={0,0,127}));
      connect(overheating_setpoint_shifted.y, need_overheat.reference) annotation (Line(points={{-61.8,
              82},{-61.8,82},{-54,82},{-54,49.6},{-47.2,49.6}}, color={0,0,127}));
      connect(heating_setpoint.y, heating_setpoint_shift.u)
        annotation (Line(points={{-71.8,102},{-66.4,102}}, color={0,0,127}));
      connect(Schedules.y[1], heating_setpoint.u) annotation (Line(points={{-85.3,113},
              {-80,113},{-80,102},{-76.4,102}},
                                 color={0,0,127}));
      connect(overheating_setpoint.y, overheating_setpoint_shifted.u)
        annotation (Line(points={{-71.8,82},{-66.4,82}}, color={0,0,127}));
      connect(Schedules.y[2], overheating_setpoint.u) annotation (Line(points={{-85.3,
              113},{-80,113},{-80,82},{-76.4,82}},
                                    color={0,0,127}));
      connect(use_solar_direct_energy.outPort, solar_direct_ON.inPort[1])
        annotation (Line(points={{92,29},{92,22},{92,14}}, color={0,0,0}));
      connect(use_solar_indirect_energy.outPort, solar_indirect_ON.inPort[1])
        annotation (Line(points={{122,29},{122,26},{128,26},{128,10}}, color={0,0,0}));
      connect(solar_indirect_ON.outPort[1], not_use_solar_indirect_energy.inPort)
        annotation (Line(points={{128,1.4},{128,1.4},{128,-4},{148,-4},{148,30}},
            color={0,0,0}));
      connect(solar_direct_ON.outPort[1], not_use_solar_direct_energy.inPort)
        annotation (Line(points={{92,5.4},{92,-4},{72,-4},{72,30}}, color={0,0,0}));
      connect(heat_ON_globalState.suspend[1], not_need_heat.inPort)
        annotation (Line(points={{49.4,56.5},{40,56.5},{40,30}}, color={0,0,0}));
      connect(not_need_heat.outPort, overheat_ON.inPort[1])
        annotation (Line(points={{40,21},{40,21},{40,-2}}, color={0,0,0}));
      connect(need_heat_or_overheat.outPort,heat_ON_globalState. inPort[1])
        annotation (Line(points={{104,81},{104,74}}, color={0,0,0}));
      connect(overheat_ON.outPort[1],
        need_heat_or_notOverheat_or_no_enough_solar_energy.inPort) annotation (
          Line(points={{40,-10.6},{40,-14},{172,-14},{172,50}}, color={0,0,0}));
      connect(solar_indirect_ON.activePort, S2_state.u[1]) annotation (Line(
          points={{123.28,6},{114,6},{114,-20},{4,-20},{4,-26},{3.73333,-26}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(solar_direct_ON.activePort, S2_state.u[2]) annotation (Line(
          points={{87.28,10},{80,10},{80,-18},{1.77636e-015,-18},{1.77636e-015,-26}},
          color={255,0,255},
          smooth=Smooth.Bezier));

      connect(condition_no_heat.y, not_need_heat.conditionPort) annotation (Line(
          points={{28.6,46},{32,46},{32,26},{35,26}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(need_heat.y, condition_need_heat.u[1:1]) annotation (Line(points={{-33.4,
              66},{-26,66}},          color={255,0,255}));
      connect(need_overheat.y, condition_need_overheat.u[1:1]) annotation (Line(
            points={{-33.4,46},{-33.4,46},{-26,46}},
                                                   color={255,0,255}));
      connect(condition_need_heat.y, condition_no_heat.u) annotation (Line(points={{-13.1,
              66},{6,66},{6,46},{14.8,46}},      color={255,0,255}));
      connect(condition_need_heat.y, condition_need_heat_or_overheat.u1)
        annotation (Line(points={{-13.1,66},{-13.1,66},{14.8,66}},   color={255,0,255}));
      connect(condition_need_overheat.y, condition_need_heat_or_overheat.u2)
        annotation (Line(points={{-13.1,46},{-13.1,46},{-6,46},{0,46},{0,60},{0,
              61.2},{14.8,61.2}},
            color={255,0,255}));
      connect(T5, enough_energy_storage.u)
        annotation (Line(points={{-106,10},{-82,10}}, color={0,0,127}));
      connect(S2_state.y, busCon.S2_state) annotation (Line(
          points={{-1.77636e-015,-43.2},{0.1,-43.2},{0.1,-95.9}},
          color={255,0,255}),    Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));

      connect(overheat_ON.activePort, S2_state.u[3]) annotation (Line(
          points={{35.28,-6},{-2,-6},{-2,-24},{-2,-26},{-3.73333,-26}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(not_use_solar_direct_energy.outPort, solar_OFF.inPort[1]) annotation (
         Line(points={{72,39},{72,39},{72,60},{102.667,60},{102.667,56}}, color={0,0,
              0}));
      connect(solar_OFF.outPort[1], use_solar_direct_energy.inPort)
        annotation (Line(points={{103,47.4},{92,47.4},{92,38}}, color={0,0,0}));
      connect(solar_OFF.outPort[2], use_solar_indirect_energy.inPort) annotation (
          Line(points={{105,47.4},{122,47.4},{122,40},{122,38}}, color={0,0,0}));
      connect(heat_ON_globalState.entry[1], solar_OFF.inPort[2])
        annotation (Line(points={{104,70},{104,56}}, color={0,0,0}));
      connect(not_use_solar_indirect_energy.outPort, solar_OFF.inPort[3])
        annotation (Line(points={{148,39},{148,39},{148,60},{105.333,60},{105.333,
              56}},
            color={0,0,0}));
      connect(condition_direct_heat_off.y, not_use_solar_direct_energy.conditionPort)
        annotation (Line(
          points={{92,-45.4},{92,-45.4},{92,-38},{62,-38},{62,34},{67,34}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(need_heat_or_notOverheat_or_no_enough_solar_energy.outPort,
        heat_OFF.inPort[1]) annotation (Line(points={{172,59},{172,59},{172,116},
              {104,116},{104,108}}, color={0,0,0}));
      connect(condition_need_heat_or_overheat.y, need_heat_or_overheat.conditionPort)
        annotation (Line(
          points={{28.6,66},{40,66},{40,86},{99,86}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(heat_OFF.outPort[1], need_heat_or_overheat.inPort) annotation (Line(
          points={{104,99.4},{104,94.7},{104,90}},
          color={0,0,0},
          smooth=Smooth.Bezier));
      connect(condition_heat_no_overheat.y,
        need_heat_or_notOverheat_or_no_enough_solar_energy.conditionPort)
        annotation (Line(
          points={{166.5,-116},{194,-116},{194,54},{177,54}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(condition_solar_indirect.y, condition_indirect_heat_off.u)
        annotation (Line(
          points={{126.6,-96},{126.6,-96},{148,-96},{148,-61.2}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(condition_solar_indirect.y, use_solar_indirect_energy.conditionPort)
        annotation (Line(
          points={{126.6,-96},{126.6,-96},{138,-96},{138,34},{127,34}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(condition_indirect_heat_off.y, not_use_solar_indirect_energy.conditionPort)
        annotation (Line(
          points={{148,-47.4},{140,-47.4},{140,34},{143,34}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(condition_need_heat.y, busCon.need_heat) annotation (Line(points={{
              -13.1,66},{6,66},{6,0},{-18,0},{-18,-95.9},{0.1,-95.9}}, color={255,
              0,255}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));
      connect(condition_need_overheat.y, busCon.need_overheat) annotation (Line(
            points={{-13.1,46},{0,46},{0,20},{-20,20},{-20,-95.9},{0.1,-95.9}},
            color={255,0,255}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));
      connect(T1, Diff_T1_T7.u1) annotation (Line(points={{-104,-28},{-94,-28},{
              -94,-36.4},{-83.2,-36.4}}, color={0,0,127}));
      connect(T7, Diff_T1_T7.u2) annotation (Line(points={{-104,-52},{-94,-52},{
              -94,-43.6},{-83.2,-43.6}}, color={0,0,127}));
      connect(Diff_T1_T7.y, enoughDelta_T1_T7.u)
        annotation (Line(points={{-69.4,-40},{-61.6,-40}}, color={0,0,127}));
      connect(condition_solar_direct.y, condition_direct_heat_off.u) annotation (
          Line(points={{80.5,-73},{92,-73},{92,-59.2}}, color={255,0,255}));
      connect(condition_solar_direct.y, use_solar_direct_energy.conditionPort)
        annotation (Line(
          points={{80.5,-73},{106,-73},{106,34},{97,34}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(busCon.V3V_solar_state, condition_solar_direct.u2) annotation (Line(
          points={{0.1,-95.9},{26,-95.9},{26,-78},{26,-78},{26,-77},{69,-77}},
          color={255,204,51},
          thickness=0.5), Text(
          string="%first",
          index=-1,
          extent={{-6,3},{-6,3}}));
      connect(enoughDelta_T1_T7.y, condition_solar_direct.u1) annotation (Line(
            points={{-43.2,-40},{-42,-40},{-42,-68},{48,-68},{64,-68},{64,-72},{
              64,-72},{64,-73},{66,-73},{69,-73}}, color={255,0,255}));
      annotation (Diagram(coordinateSystem(extent={{-100,-120},{200,120}})), Icon(coordinateSystem(
              extent={{-100,-120},{200,120}})),
        Documentation(info="<html>
</html>"));
    end Heating_fsm_back2;

    block S5_S6_V3V_fsm_back
      "V3V cannot be open if temperature inside storage and DHW tank is too small"

    extends Base_fsm;

    import MF = Buildings.Utilities.Math.Functions;
    import SI = Modelica.SIunits;

    //
    // General inputs/outputs
    //
      Modelica.Blocks.Interfaces.RealInput T1(unit="K")
        "Temperature inside collector"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
                {12,12}},
            origin={-102,100}),
            iconTransformation(extent={{-18,-18},{18,18}},
            origin={-92,100})));
      Modelica.Blocks.Interfaces.RealInput T3(unit="K")
        "Temperature inside DHW tank next to solar exchanger (bottom)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-114,58},
                {-90,82}}),                                                                                               iconTransformation(
              extent={{-110,24},{-74,60}})));
      Modelica.Blocks.Interfaces.RealInput T4(unit="K")
        "Temperature inside DHW tank next to drawing up (top)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-114,28},
                {-90,52}}),                                                                                               iconTransformation(
              extent={{-108,-96},{-72,-60}})));
    //   Tools.BusCon busCon "Share all states inside a single connector"
    //     annotation (Placement(transformation(extent={{-20,-116},{20,-76}}),
    //         iconTransformation(extent={{-10,-106},{10,-86}})));

    //
    // Solar valve (V3V)
    //
      Modelica.Blocks.Logical.OnOffController need_solar_Storage(pre_y_start=true,
          bandwidth=2)
        "Return True if solar must be used to load the storage tank"
        annotation (Placement(transformation(extent={{-76,54},{-64,66}})));
      Modelica.Blocks.Logical.Hysteresis enough_power_in_DHW(uLow=temp_min_DHW - 2, uHigh=
            temp_min_DHW)
        "Test if we have enough power inside the DHW before authorize temperature inside the storage tank"
        annotation (Placement(transformation(extent={{-76,36},{-64,48}})));
      Modelica.Blocks.Logical.OnOffController need_solar_DHW(pre_y_start=true, bandwidth=2)
        "Return True if solar must be used and False if solar must not be used"
        annotation (Placement(transformation(extent={{-76,82},{-64,94}})));
      Modelica.Blocks.Sources.BooleanExpression V3V_solar_state(y=(enough_power_in_DHW.y and
            need_solar_Storage.y) or need_solar_DHW.y)
        "[T1 >= T5 and T3 >= 32�C] or [T1 >= min(T3, T4)]"
        annotation (Placement(transformation(extent={{-16,-10},{16,10}},
            rotation=-90,
            origin={-48,62})));

    //
    // Pump S5 (pump for DHW tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_DHW = 363.15
        "Maximum admissible value for DHW tank" annotation (Dialog(group="Higher limit"));
      Modelica_StateGraph2.Step S5_off(
        initialStep=true,
        nOut=1,
        nIn=1) "S5 state is OFF"
        annotation (Placement(transformation(extent={{52,74},{60,82}})));
      Modelica_StateGraph2.Transition S5_T_OffToOn(
        use_firePort=false,
        delayedTransition=false,
        use_conditionPort=true)
        "Transition condition to change S5 state from OFF to ON"
        annotation (Placement(transformation(extent={{52,52},{60,60}})));
      Modelica_StateGraph2.Step S5_on(
        nOut=1,
        nIn=1,
        initialStep=false,
        use_activePort=true) "S5 state is ON"
        annotation (Placement(transformation(extent={{52,30},{60,38}})));
      Modelica_StateGraph2.Transition S5_T_OnToOff(
        use_firePort=false,
        use_conditionPort=true,
        delayedTransition=true,
        waitTime=60) "Transition condition to change S5 state from ON to OFF"
        annotation (Placement(transformation(extent={{84,60},{76,52}})));
      Modelica.Blocks.Logical.Not oppositeForS5
        annotation (Placement(transformation(extent={{20,62},{36,78}})));
      Modelica.Blocks.Sources.BooleanExpression s5_cond_off_to_on(y=V3V_solar_state.y and
            max_DHW_tank.y and enoughDelta_T1_T3.y)
        "[T1 - T3 >= 10 and T3 > temp_max_DHW and V3V_solar]"
        annotation (Placement(transformation(extent={{-40,12},{0,34}})));

    //
    // Pump S6 (pump for Storage tank exchanger)
    //
      parameter SI.ThermodynamicTemperature temp_max_storage = 378.15
        "Maximum admissible value for Storage tank" annotation (Dialog(group="Higher limit"));
      parameter SI.ThermodynamicTemperature temp_min_DHW = 303.15
        "Minimum admissible value for DHW tank before allowing Storage tank loading" annotation (Dialog(group="Lower limit"));
      Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T3(
        y(start=false),
        uLow=4,
        uHigh=10) "Check if T1 - T3 is larger enough"
        annotation (Placement(transformation(extent={{-76,6},{-64,18}})));
      Modelica.Blocks.Logical.OnOffController max_DHW_tank(bandwidth=5, pre_y_start=true)
        "Test if the temperature inside the tank is lower than reference"
        annotation (Placement(transformation(extent={{-76,-18},{-64,-6}})));
      Modelica.Blocks.Logical.Hysteresis enoughDelta_T1_T5(
        y(start=false),
        uHigh=10,
        uLow=2.5) "Check if T1 - T5 is larger enough"
        annotation (Placement(transformation(extent={{-74,-62},{-64,-52}})));
      Modelica.Blocks.Logical.OnOffController max_Storage_tank(bandwidth=5)
        "Test if the temperature inside the tank is lower than reference"
        annotation (Placement(transformation(extent={{-74,-84},{-64,-74}})));
    Modelica_StateGraph2.Step S6_off(
        initialStep=true,
        nOut=1,
        nIn=1) "S6 state is OFF"
        annotation (Placement(transformation(extent={{52,-16},{60,-8}})));
      Modelica_StateGraph2.Transition S6_T_OffToOn(
        use_firePort=false,
        delayedTransition=false,
        use_conditionPort=true)
        "Transition condition to change S5 state from OFF to ON"
        annotation (Placement(transformation(extent={{52,-34},{60,-26}})));
      Modelica_StateGraph2.Step S6_on(
        nOut=1,
        nIn=1,
        initialStep=false,
        use_activePort=true) "S6 state is ON"
        annotation (Placement(transformation(extent={{52,-60},{60,-52}})));
      Modelica_StateGraph2.Transition S6_T_OnToOff(
        use_firePort=false,
        use_conditionPort=true,
        delayedTransition=true,
        waitTime=60) "Transition condition to change S5 state from ON to OFF"
        annotation (Placement(transformation(extent={{84,-26},{76,-34}})));
      Modelica.Blocks.Logical.Not oppositeForS6
        annotation (Placement(transformation(extent={{20,-60},{36,-44}})));
      Modelica.Blocks.Sources.BooleanExpression s6_cond_off_to_on(y=enoughDelta_T1_T5.y and
            enough_power_in_DHW.y and max_Storage_tank.y)
        "[T1 - T5 >= 10 and min(T3, T4) >= 30 and T5 > temp_max_DHW]"
        annotation (Placement(transformation(extent={{-40,-28},{2,-6}})));

      Modelica.Blocks.Interfaces.RealInput T5(unit="K")
        "Temperature inside solar tank (top)"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-114,-4},
                {-90,20}}),                                                                                               iconTransformation(
              extent={{-108,-96},{-72,-60}})));
    equation
    //
    // Describe the control of the solar valve (V3V)
    //
      //Connect minimal between T4 and T3 for test with T1 (`+1` used to shift bandwidth upper)
      need_solar_DHW.u = MF.smoothMin(x1=T4, x2=T3,deltaX=0.1) + 1
        "[T1 >= min(T3, T4)]";
      need_solar_Storage.u = T5 + 1 "[ T1 >= T5 ]";

    //
    // Describe the control of the solar pump S5 (pump for DHW tank exchanger)
    //
      enoughDelta_T1_T3.u = T1 - T3 "[T1 - T3 >= 10]";
      max_DHW_tank.reference = temp_max_DHW "[T3 < temp_max_DHW]";

    //
    // Describe the control of the solar pump S6 (pump for Storage tank exchanger)
    //
      enoughDelta_T1_T5.u = T1 - T5 "[T1 - T5 >= 10]";
      max_Storage_tank.reference = temp_max_storage-2.5 "[T5 > temp_max_DHW]";

      connect(S5_off.outPort[1], S5_T_OffToOn.inPort)
        annotation (Line(points={{56,73.4},{56,66.7},{56,60}}, color={0,0,0}));
      connect(S5_T_OffToOn.outPort, S5_on.inPort[1])
        annotation (Line(points={{56,51},{56,44.5},{56,38}}, color={0,0,0}));
      connect(S5_on.outPort[1], S5_T_OnToOff.inPort) annotation (Line(points={{56,
              29.4},{56,29.4},{56,20},{80,20},{80,52}}, color={0,0,0}));
      connect(S5_T_OnToOff.outPort, S5_off.inPort[1]) annotation (Line(points={{
              80,61},{80,94},{56,94},{56,82}}, color={0,0,0}));
      connect(S6_off.outPort[1], S6_T_OffToOn.inPort) annotation (Line(points={{56,-16.6},
              {56,-26}},                      color={0,0,0}));
      connect(S6_T_OffToOn.outPort, S6_on.inPort[1])
        annotation (Line(points={{56,-35},{56,-35},{56,-52}},   color={0,0,0}));
      connect(S6_on.outPort[1], S6_T_OnToOff.inPort) annotation (Line(points={{56,-60.6},
              {56,-60.6},{56,-70},{80,-70},{80,-34}},        color={0,0,0}));
      connect(S6_T_OnToOff.outPort, S6_off.inPort[1]) annotation (Line(points={{80,-25},
              {80,4},{56,4},{56,-8}},         color={0,0,0}));
      connect(oppositeForS6.y, S6_T_OnToOff.conditionPort) annotation (Line(points={{36.8,
              -52},{40,-52},{40,-76},{88,-76},{88,-30},{85,-30}},       color={255,0,
              255},
          smooth=Smooth.Bezier));
      connect(oppositeForS5.y, S5_T_OnToOff.conditionPort) annotation (Line(points={
              {36.8,70},{40,70},{40,100},{92,100},{92,56},{85,56}}, color={255,0,255},
          smooth=Smooth.Bezier));

      connect(T1, need_solar_DHW.reference) annotation (Line(points={{-102,100},{-84,100},{-84,91.6},
              {-77.2,91.6}},                 color={0,0,127}));
      connect(T1, need_solar_Storage.reference) annotation (Line(points={{-102,100},{-102,100},{-84,
              100},{-84,63.6},{-77.2,63.6}},                color={0,0,127}));
      connect(T3, enough_power_in_DHW.u) annotation (Line(points={{-102,70},{-84,70},{-84,42},{-77.2,
              42}},                 color={0,0,127}));
      connect(T3, max_DHW_tank.u) annotation (Line(points={{-102,70},{-102,74},{-86,74},{-86,-15.6},
              {-77.2,-15.6}},             color={0,0,127}));
      connect(s6_cond_off_to_on.y, S6_T_OffToOn.conditionPort) annotation (Line(points={{4.1,-17},
              {4,-17},{4,-16},{28,-16},{28,-30},{51,-30}},
                                       color={255,0,255},
          smooth=Smooth.Bezier));
      connect(s6_cond_off_to_on.y, oppositeForS6.u) annotation (Line(points={{4.1,-17},
              {6,-17},{6,-52},{18.4,-52}},
                                color={255,0,255}));
      connect(s5_cond_off_to_on.y, oppositeForS5.u) annotation (Line(points={{2,23},{
              4,23},{4,32},{4,70},{18.4,70}},
                                  color={255,0,255}));
      connect(s5_cond_off_to_on.y, S5_T_OffToOn.conditionPort)
        annotation (Line(points={{2,23},{10,23},{16,56},{51,56}}, color={255,0,255},
          smooth=Smooth.Bezier));

      connect(V3V_solar_state.y, busCon.V3V_solar_state) annotation (Line(points={{-48,
              44.4},{-48,-60},{0.1,-60},{0.1,-95.9}},
                                color={255,0,255}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));
      connect(S5_on.activePort, busCon.S5_state) annotation (Line(points={{60.72,
              34},{100,34},{100,-82},{0.1,-82},{0.1,-95.9}},
                                         color={255,0,255},
          smooth=Smooth.Bezier),                             Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));
      connect(S6_on.activePort, busCon.S6_state) annotation (Line(points={{60.72,
              -56},{70,-56},{70,-80},{0.1,-80},{0.1,-95.9}},
                                color={255,0,255},
          smooth=Smooth.Bezier),                    Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));
      connect(T5, max_Storage_tank.u) annotation (Line(points={{-102,8},{-98,8},{-98,
              8},{-88,8},{-88,-82},{-75,-82}}, color={0,0,127}));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
                -100},{100,120}})), Diagram(coordinateSystem(preserveAspectRatio=
                false, extent={{-100,-100},{100,120}}), graphics={
                                                    Rectangle(
              extent={{-80,24},{-60,-20}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={238,232,213}),             Rectangle(
              extent={{-80,100},{-60,34}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={147,161,161}),             Rectangle(
              extent={{-80,-46},{-60,-86}},
              lineColor={0,0,255},
              fillPattern=FillPattern.Solid,
              fillColor={253,246,227}),
            Text(
              extent={{-78,6},{-64,-2}},
              lineColor={38,139,210},
              textString="S5"),
            Text(
              extent={{-76,-62},{-62,-70}},
              lineColor={38,139,210},
              textString="S6"),
            Text(
              extent={{-76,82},{-64,70}},
              lineColor={38,139,210},
              textString="V3V")}));
    end S5_S6_V3V_fsm_back;

    block Heating_fsm_back
      "Cannot used direct heating when solar can only provide energy for heating"
      extends Base_fsm;
      extends Tools.System_parameters;

      import SI = Modelica.SIunits;
      import MBV = Modelica.Math.BooleanVectors;
    //
    // General inputs/outputs
    //
       Modelica.Blocks.Sources.CombiTimeTable             Schedules[n](
        tableOnFile=tableOnFile,
        table=table,
        tableName=tableName,
        fileName=fileName,
        verboseRead=verboseRead,
        columns=columns,
        each smoothness=smoothness,
        each extrapolation=extrapolation,
        offset=offset,
        startTime=startTime)
        "Describe setpoint for the algorithm control. 1=Temperature setpoint [K], 2=Temperature for solar setpoint [K], 3=Minimal air flow rate [m3/s]"
         annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(extent={{-100,
                106},{-86,120}})));
      Modelica.Blocks.Routing.RealPassThrough heating_setpoint[n](each y(
          final quantity="ThermodynamicTemperature",
          final unit="K",
          displayUnit="degC",
          min=0)) "Heating_setpoint for rooms temperature"
        annotation (Placement(transformation(extent={{-76,100},{-72,104}})));
      Modelica.Blocks.Routing.RealPassThrough overheating_setpoint[n](each y(
          final quantity="ThermodynamicTemperature",
          final unit="K",
          displayUnit="degC",
          min=0)) "Overheating_setpoint for rooms temperature"
        annotation (Placement(transformation(extent={{-76,80},{-72,84}})));

      Utilities.Other.Shift heating_setpoint_shift[n](k=need_heat.bandwidth/2)
        "Heat temperature setpoint for each room"
        annotation (Placement(transformation(extent={{-66,100},{-62,104}})));
      Utilities.Other.Shift overheating_setpoint_shifted[n](k=need_overheat.bandwidth/2)
        "Heat temperature setpoint for each room"
        annotation (Placement(transformation(extent={{-66,80},{-62,84}})));

      Modelica.Blocks.Interfaces.RealInput T1(unit="K")
        "Temperature inside collector"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
                {12,12}},
            origin={-104,-50}),
            iconTransformation(extent={{-18,-18},{18,18}},
            origin={-92,50})));
      Modelica.Blocks.Interfaces.RealVectorInput Tamb[n](each unit="K")
        "Vector of temperature inside rooms areas"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-122,34},
                {-82,74}}),                                                                                               iconTransformation(
              extent={{-112,82},{-84,110}})));
      Modelica.Blocks.Interfaces.RealInput T5(unit="K")
        "Temperature inside storage tank"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
                {12,12}},
            origin={-106,10}),
            iconTransformation(extent={{-18,-18},{18,18}},
            origin={-92,2})));
      Modelica.Blocks.Interfaces.RealInput T7(unit="K")
        "Water temperature after exchanger"
        annotation (__Dymola_tag={"Temperature", "Computed"}, Placement(transformation(extent={{-12,-12},
                {12,12}},
            origin={-104,-76}),
            iconTransformation(extent={{-18,-18},{18,18}},
            origin={-92,-52})));

    //
    // Boolean logic
    //
      parameter SI.ThermodynamicTemperature temp_min_collector = 313.15
        "Minimum collector temperature in order to use direct solar energy" annotation (Dialog(group="Lower limit"));
      parameter SI.ThermodynamicTemperature temp_min_storage = 318.15
        "Minimum storage tank temperature in order to use indirect solar energy" annotation (Dialog(group="Lower limit"));
      Modelica.Blocks.Logical.Not condition_no_heat "We do not need heat anymore"
        annotation (Placement(transformation(
            extent={{-6,-6},{6,6}},
            rotation=0,
            origin={22,46})));
      Modelica.Blocks.MathBoolean.Or condition_need_heat(nu=1) "We need to heat"
        annotation (Placement(transformation(
            extent={{-6,-6},{6,6}},
            rotation=0,
            origin={-20,66})));
      Modelica.Blocks.MathBoolean.Or condition_need_overheat(nu=1)
        "We need to overheat" annotation (Placement(transformation(
            extent={{-6,-6},{6,6}},
            rotation=0,
            origin={-20,46})));
      Modelica.Blocks.Logical.Or condition_need_heat_or_overheat
        "We need heat or overheat" annotation (Placement(transformation(
            extent={{-6,-6},{6,6}},
            rotation=0,
            origin={22,66})));
      Modelica.Blocks.Logical.Not condition_direct_heat_off annotation (Placement(
            transformation(
            extent={{-6,-6},{6,6}},
            rotation=90,
            origin={92,-54})));
      Modelica.Blocks.Logical.Not condition_indirect_heat_off annotation (Placement(
            transformation(
            extent={{-6,6},{6,-6}},
            rotation=90,
            origin={148,-54})));

      Modelica.Blocks.Logical.And condition_direct_heat
        "[V3V_solar and enough_energy_collector]" annotation (Placement(
            transformation(
            extent={{-6,-6},{6,6}},
            rotation=0,
            origin={82,-74})));
      Modelica.Blocks.Sources.BooleanExpression condition_heat_no_overheat(y=(
            condition_need_heat.y and not condition_need_overheat.y) or (
            condition_direct_heat_off.y))
        "Need heating or no Overheating" annotation (Placement(transformation(
            extent={{-65,-10},{65,10}},
            rotation=0,
            origin={95,-116})));

    //
    // FSM
    //
      Modelica.Blocks.Logical.OnOffController need_heat[n](each bandwidth=0.5)
        annotation (Placement(transformation(extent={{-46,60},{-34,72}})));
      Modelica.Blocks.Logical.OnOffController need_overheat[n](each bandwidth=1)
        annotation (Placement(transformation(extent={{-46,40},{-34,52}})));
      Modelica.Blocks.Logical.Hysteresis enough_energy_storage(uLow=
            temp_min_storage - 5, uHigh=temp_min_storage)
        annotation (Placement(transformation(extent={{-80,0},{-60,20}})));
      Modelica.Blocks.Logical.OnOffController enough_energy_collector(bandwidth=5)
        "We can use direct solar energy"
        annotation (Placement(transformation(extent={{-80,-70},{-60,-50}})));

      Modelica.Blocks.MathBoolean.Or
                                  S2_state(nu=3)
        "S2 on if at least one room need heating and we have enough solar energy" annotation (
          Placement(transformation(
            extent={{-8,-8},{8,8}},
            rotation=-90,
            origin={0,-34})));

      Modelica_StateGraph2.Step overheat_ON(
        initialStep=false,
        use_activePort=true,
        nIn=1,
        nOut=1) "We can use direct solar energy to overheat the room" annotation (
          Placement(transformation(
            extent={{4,-4},{-4,4}},
            rotation=0,
            origin={40,-6})));
      Modelica_StateGraph2.Transition use_solar_direct_energy(
        use_firePort=false,
        delayedTransition=false,
        use_conditionPort=true)
        "Transition condition from heating_on to heating_SolarDirect"
        annotation (Placement(transformation(extent={{96,30},{88,38}})));
      Modelica_StateGraph2.Step heat_OFF(
        initialStep=true,
        use_activePort=false,
        nIn=1,
        nOut=1)
               "We do not need heating anymore"
        annotation (Placement(transformation(extent={{100,100},{108,108}})));
      Modelica_StateGraph2.Transition not_need_heat(
        use_firePort=false,
        use_conditionPort=true,
        delayedTransition=false)
        "Suspender transition condition from Heat_ON_globalState"
        annotation (Placement(transformation(extent={{36,22},{44,30}})));
      Modelica_StateGraph2.Step solar_direct_ON(
        nIn=1,
        initialStep=false,
        use_activePort=true,
        nOut=1) "We can use direct solar energy to heat the room"
        annotation (Placement(transformation(extent={{96,6},{88,14}})));
      Modelica_StateGraph2.Step solar_indirect_ON(
        nIn=1,
        initialStep=false,
        use_activePort=true,
        nOut=1) "We can use storage tank energy to heat the room"
        annotation (Placement(transformation(extent={{132,2},{124,10}})));
      Modelica_StateGraph2.Transition use_solar_indirect_energy(
        use_firePort=false,
        delayedTransition=false,
        use_conditionPort=true)
        "Transition condition from heating_on to heating_SolarIndirect"
        annotation (Placement(transformation(extent={{126,30},{118,38}})));
      Modelica_StateGraph2.Transition need_heat_or_overheat(
        use_firePort=false,
        use_conditionPort=true,
        delayedTransition=true,
        waitTime=60) "Transition condition from heating_off to heating_on"
        annotation (Placement(transformation(extent={{100,82},{108,90}})));
      Modelica_StateGraph2.Parallel heat_ON_globalState(
        use_suspend=true,
        nSuspend=1,
        nIn=1,
        nEntry=1)
        "Parent state that loop until we do not need to heat the room. Each internal branche can have a state active."
        annotation (Placement(transformation(extent={{52,-6},{156,74}})));
      Modelica_StateGraph2.Step solar_OFF(
        initialStep=false,
        use_activePort=false,
        nIn=3,
        nOut=2) "We need heating but there is no enough solar energy"
        annotation (Placement(transformation(extent={{100,48},{108,56}})));
      Modelica_StateGraph2.Transition not_use_solar_direct_energy(
        use_firePort=false,
        delayedTransition=false,
        use_conditionPort=true)
        "Transition condition from solar_direct_ON to solar_OFF"
        annotation (Placement(transformation(extent={{68,38},{76,30}})));
      Modelica_StateGraph2.Transition not_use_solar_indirect_energy(
        use_firePort=false,
        delayedTransition=false,
        use_conditionPort=true)
        "Transition condition from solar_indirect_ON to solar_OFF"
        annotation (Placement(transformation(extent={{144,38},{152,30}})));
      Modelica_StateGraph2.Transition need_heat_or_notOverheat_or_no_enough_solar_energy(
        use_firePort=false,
        use_conditionPort=true,
        delayedTransition=true,
        waitTime=60)
        "Transition condition from overheat_ON to heat_OFF. We do not need to overheat or we need to heat the room."
        annotation (Placement(transformation(
            extent={{-4,-4},{4,4}},
            rotation=180,
            origin={172,54})));

      Modelica.Blocks.Sources.BooleanExpression condition_solar_indirect(y=not
            busCon.V3V_solar_state and enough_energy_storage.y)
        "Not V3V and enough energy in collectors to heat the storage tank."
        annotation (Placement(transformation(
            extent={{-46,-10},{46,10}},
            rotation=0,
            origin={76,-96})));
    equation
    //
    // Describe how to control Sj and S5 pumps (check heat/overheat | direct/indirect solar energy)
    //
    // Test temperature difference between T1 and max(T7 or minimal value to use direct solar)
      enough_energy_collector.u = Buildings.Utilities.Math.Functions.smoothMax(x1=temp_min_collector,
                                                                               x2=T7,
                                                                               deltaX=0.1);
      // Connect T1 to solar_direct bloc OnOffController
      enough_energy_collector.reference = T1 - (enough_energy_collector.bandwidth / 2)
        "bandwidth used to shift down T1 value (more safe). Wait more before turn true and wait less before turn off";

      connect(Tamb, need_heat.u) annotation (Line(points={{-102,54},{-56,54},{-56,
              60},{-56,62.4},{-47.2,62.4}},
            color={0,0,127}));
      connect(Tamb, need_overheat.u) annotation (Line(points={{-102,54},{-56,54},
              {-56,42.4},{-47.2,42.4}},
            color={0,0,127}));
      connect(heating_setpoint_shift.y, need_heat.reference) annotation (Line(points={{-61.8,
              102},{-52,102},{-52,100},{-52,69.6},{-47.2,69.6}},
                                             color={0,0,127}));
      connect(overheating_setpoint_shifted.y, need_overheat.reference) annotation (Line(points={{-61.8,
              82},{-61.8,82},{-54,82},{-54,49.6},{-47.2,49.6}}, color={0,0,127}));
      connect(heating_setpoint.y, heating_setpoint_shift.u)
        annotation (Line(points={{-71.8,102},{-66.4,102}}, color={0,0,127}));
      connect(Schedules.y[1], heating_setpoint.u) annotation (Line(points={{-85.3,113},
              {-80,113},{-80,102},{-76.4,102}},
                                 color={0,0,127}));
      connect(overheating_setpoint.y, overheating_setpoint_shifted.u)
        annotation (Line(points={{-71.8,82},{-66.4,82}}, color={0,0,127}));
      connect(Schedules.y[2], overheating_setpoint.u) annotation (Line(points={{-85.3,
              113},{-80,113},{-80,82},{-76.4,82}},
                                    color={0,0,127}));
      connect(use_solar_direct_energy.outPort, solar_direct_ON.inPort[1])
        annotation (Line(points={{92,29},{92,22},{92,14}}, color={0,0,0}));
      connect(use_solar_indirect_energy.outPort, solar_indirect_ON.inPort[1])
        annotation (Line(points={{122,29},{122,26},{128,26},{128,10}}, color={0,0,0}));
      connect(solar_indirect_ON.outPort[1], not_use_solar_indirect_energy.inPort)
        annotation (Line(points={{128,1.4},{128,1.4},{128,-4},{148,-4},{148,30}},
            color={0,0,0}));
      connect(solar_direct_ON.outPort[1], not_use_solar_direct_energy.inPort)
        annotation (Line(points={{92,5.4},{92,-4},{72,-4},{72,30}}, color={0,0,0}));
      connect(heat_ON_globalState.suspend[1], not_need_heat.inPort)
        annotation (Line(points={{49.4,56.5},{40,56.5},{40,30}}, color={0,0,0}));
      connect(not_need_heat.outPort, overheat_ON.inPort[1])
        annotation (Line(points={{40,21},{40,21},{40,-2}}, color={0,0,0}));
      connect(need_heat_or_overheat.outPort,heat_ON_globalState. inPort[1])
        annotation (Line(points={{104,81},{104,74}}, color={0,0,0}));
      connect(overheat_ON.outPort[1],
        need_heat_or_notOverheat_or_no_enough_solar_energy.inPort) annotation (
          Line(points={{40,-10.6},{40,-14},{172,-14},{172,50}}, color={0,0,0}));
      connect(solar_indirect_ON.activePort, S2_state.u[1]) annotation (Line(
          points={{123.28,6},{114,6},{114,-20},{4,-20},{4,-26},{3.73333,-26}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(solar_direct_ON.activePort, S2_state.u[2]) annotation (Line(
          points={{87.28,10},{80,10},{80,-18},{1.77636e-015,-18},{1.77636e-015,-26}},
          color={255,0,255},
          smooth=Smooth.Bezier));

      connect(condition_no_heat.y, not_need_heat.conditionPort) annotation (Line(
          points={{28.6,46},{32,46},{32,26},{35,26}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(need_heat.y, condition_need_heat.u[1:1]) annotation (Line(points={{-33.4,
              66},{-26,66}},          color={255,0,255}));
      connect(need_overheat.y, condition_need_overheat.u[1:1]) annotation (Line(
            points={{-33.4,46},{-33.4,46},{-26,46}},
                                                   color={255,0,255}));
      connect(condition_need_heat.y, condition_no_heat.u) annotation (Line(points={{-13.1,
              66},{6,66},{6,46},{14.8,46}},      color={255,0,255}));
      connect(condition_need_heat.y, condition_need_heat_or_overheat.u1)
        annotation (Line(points={{-13.1,66},{-13.1,66},{14.8,66}},   color={255,0,255}));
      connect(condition_need_overheat.y, condition_need_heat_or_overheat.u2)
        annotation (Line(points={{-13.1,46},{-13.1,46},{-6,46},{0,46},{0,60},{0,
              61.2},{14.8,61.2}},
            color={255,0,255}));
      connect(T5, enough_energy_storage.u)
        annotation (Line(points={{-106,10},{-82,10}}, color={0,0,127}));
      connect(condition_direct_heat.y, condition_direct_heat_off.u) annotation (
          Line(points={{88.6,-74},{92,-74},{92,-61.2}}, color={255,0,255}));
      connect(busCon.V3V_solar_state, condition_direct_heat.u2) annotation (Line(
          points={{0.1,-95.9},{16,-95.9},{16,-78.8},{30,-78.8},{74.8,-78.8}},
          color={255,204,51},
          thickness=0.5), Text(
          string="%first",
          index=-1,
          extent={{-6,3},{-6,3}}));
      connect(S2_state.y, busCon.S2_state) annotation (Line(
          points={{-1.77636e-015,-43.2},{0.1,-43.2},{0.1,-95.9}},
          color={255,0,255}),    Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));

      connect(overheat_ON.activePort, S2_state.u[3]) annotation (Line(
          points={{35.28,-6},{-2,-6},{-2,-24},{-2,-26},{-3.73333,-26}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(not_use_solar_direct_energy.outPort, solar_OFF.inPort[1]) annotation (
         Line(points={{72,39},{72,39},{72,60},{102.667,60},{102.667,56}}, color={0,0,
              0}));
      connect(solar_OFF.outPort[1], use_solar_direct_energy.inPort)
        annotation (Line(points={{103,47.4},{92,47.4},{92,38}}, color={0,0,0}));
      connect(solar_OFF.outPort[2], use_solar_indirect_energy.inPort) annotation (
          Line(points={{105,47.4},{122,47.4},{122,40},{122,38}}, color={0,0,0}));
      connect(heat_ON_globalState.entry[1], solar_OFF.inPort[2])
        annotation (Line(points={{104,70},{104,56}}, color={0,0,0}));
      connect(not_use_solar_indirect_energy.outPort, solar_OFF.inPort[3])
        annotation (Line(points={{148,39},{148,39},{148,60},{105.333,60},{105.333,
              56}},
            color={0,0,0}));
      connect(condition_direct_heat.y, use_solar_direct_energy.conditionPort)
        annotation (Line(
          points={{88.6,-74},{106,-74},{106,34},{97,34}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(condition_direct_heat_off.y, not_use_solar_direct_energy.conditionPort)
        annotation (Line(
          points={{92,-47.4},{92,-47.4},{92,-38},{62,-38},{62,34},{67,34}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(enough_energy_collector.y, condition_direct_heat.u1) annotation (Line(
            points={{-59,-60},{42,-60},{42,-74},{74.8,-74}}, color={255,0,255}));
      connect(need_heat_or_notOverheat_or_no_enough_solar_energy.outPort,
        heat_OFF.inPort[1]) annotation (Line(points={{172,59},{172,59},{172,116},
              {104,116},{104,108}}, color={0,0,0}));
      connect(condition_need_heat_or_overheat.y, need_heat_or_overheat.conditionPort)
        annotation (Line(
          points={{28.6,66},{40,66},{40,86},{99,86}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(heat_OFF.outPort[1], need_heat_or_overheat.inPort) annotation (Line(
          points={{104,99.4},{104,94.7},{104,90}},
          color={0,0,0},
          smooth=Smooth.Bezier));
      connect(condition_heat_no_overheat.y,
        need_heat_or_notOverheat_or_no_enough_solar_energy.conditionPort)
        annotation (Line(
          points={{166.5,-116},{194,-116},{194,54},{177,54}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(condition_solar_indirect.y, condition_indirect_heat_off.u)
        annotation (Line(
          points={{126.6,-96},{126.6,-96},{148,-96},{148,-61.2}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(condition_solar_indirect.y, use_solar_indirect_energy.conditionPort)
        annotation (Line(
          points={{126.6,-96},{126.6,-96},{138,-96},{138,34},{127,34}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(condition_indirect_heat_off.y, not_use_solar_indirect_energy.conditionPort)
        annotation (Line(
          points={{148,-47.4},{140,-47.4},{140,34},{143,34}},
          color={255,0,255},
          smooth=Smooth.Bezier));
      connect(condition_need_heat.y, busCon.need_heat) annotation (Line(points={{
              -13.1,66},{6,66},{6,0},{-18,0},{-18,-95.9},{0.1,-95.9}}, color={255,
              0,255}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));
      connect(condition_need_overheat.y, busCon.need_overheat) annotation (Line(
            points={{-13.1,46},{0,46},{0,20},{-20,20},{-20,-95.9},{0.1,-95.9}},
            color={255,0,255}), Text(
          string="%second",
          index=1,
          extent={{6,3},{6,3}}));
      annotation (Diagram(coordinateSystem(extent={{-100,-120},{200,120}})), Icon(coordinateSystem(
              extent={{-100,-120},{200,120}})),
        Documentation(info="<html>
</html>"));
    end Heating_fsm_back;
  end Backup;
end FSM;
