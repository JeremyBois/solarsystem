within SolarSystem.Testing;
model TestDrawingUp
    Modelica.Blocks.Sources.CombiTimeTable Schedules[5](
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    table={matin33.schedule,reparti33.schedule,soir33.schedule,eN12977_33.schedule,
        aDEME_33.schedule},
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments)
    "Volume of water must be in liters"
    annotation (Placement(transformation(extent={{8,14},{34,40}})));
  Data.Parameter.ParametricStudy201611.DrawingUp.Matin33 matin33
    annotation (Placement(transformation(extent={{-80,72},{-60,92}})));
  Data.Parameter.ParametricStudy201611.DrawingUp.Reparti33 reparti33
    annotation (Placement(transformation(extent={{-80,42},{-60,62}})));
  Data.Parameter.ParametricStudy201611.DrawingUp.Soir33 soir33
    annotation (Placement(transformation(extent={{-80,16},{-60,36}})));
  Data.Parameter.ParametricStudy201611.DrawingUp.EN12977_33 eN12977_33
    annotation (Placement(transformation(extent={{-80,-12},{-60,8}})));
  Data.Parameter.ParametricStudy201611.DrawingUp.ADEME_33 aDEME_33
    annotation (Placement(transformation(extent={{-80,-40},{-60,-20}})));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)),
    experiment(StartTime=2.26368e+007, StopTime=4.1904e+007),
    __Dymola_experimentSetupOutput(doublePrecision=true),
    __Dymola_experimentFlags(
      Advanced(GenerateVariableDependencies=false, OutputModelicaCode=false),
      Evaluate=true,
      OutputCPUtime=true,
      OutputFlatModelica=false));
end TestDrawingUp;
