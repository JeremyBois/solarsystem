within SolarSystem.Testing;
model PowerTo_testCase

  parameter Real n=2 "Number of room to control (must be greater than 0)";
  Real using_operator;
  Real manual_operator;

equation

  manual_operator = (n * 1000 / 3600) ^ 2;
  using_operator = (n * 1000 / 3600) * (n * 1000 / 3600);

  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false)),
    Diagram(coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<p><b><span style=\"font-size: 18pt;\">Error</span></b></p>
<p>Compiling and linking the model (Visual C++). </p>
<p>dsmodel.c</p>
<p>LINK : fatal error LNK1181: impossible d&apos;ouvrir le fichier en entr&eacute;e &apos;ModelicaMatIO.lib&apos;</p>
<p>Error generating Dymosim. </p>
<p><b><span style=\"font-size: 18pt;\">Solution ?</span></b></p>
<p>https://github.com/svn2github/Modelica_StandardLibrary/commit/ab69b8c1c2de2b7406d2a79f9a850c98020aca90 </p>
</html>"));
end PowerTo_testCase;
