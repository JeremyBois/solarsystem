within SolarSystem.Testing;
model WetDry_test
  Buildings.Utilities.Psychrometrics.X_pTphi x_pTphi
    annotation (Placement(transformation(extent={{-40,-20},{-20,0}})));
protected
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{-68,32},{-52,48}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Weather(
                                       filNam=weather_file.path,
      computeWetBulbTemperature=false) "Weather datas"
    annotation (Placement(transformation(extent={{-100,30},{-80,50}})));
protected
  Data.Parameter.City_Datas.Bordeaux weather_file
    annotation (Placement(transformation(extent={{-100,70},{-80,90}})));
public
  Buildings.Utilities.Psychrometrics.ToTotalAir
             toTotalAir
    annotation (Placement(transformation(extent={{0,-20},{20,0}})));
equation
  connect(Weather.weaBus, weaBus) annotation (Line(
      points={{-80,40},{-60,40}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(weaBus.pAtm, x_pTphi.p_in) annotation (Line(
      points={{-60,40},{-60,-4},{-42,-4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.TDryBul, x_pTphi.T) annotation (Line(
      points={{-60,40},{-60,-10},{-42,-10}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.relHum, x_pTphi.phi) annotation (Line(
      points={{-60,40},{-60,-16},{-42,-16}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(x_pTphi.X[1], toTotalAir.XiDry) annotation (Line(
      points={{-19,-10},{-1,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end WetDry_test;
