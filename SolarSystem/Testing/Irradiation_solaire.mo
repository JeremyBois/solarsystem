within SolarSystem.Testing;
model Irradiation_solaire
  "Compute solar irradiation on a tilded surface using weather data input"

  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(filNam="D:/Github/solarsystem/Meteo/Bordeaux/FRA_Bordeaux.075100_IWEC2.mos",
      computeWetBulbTemperature=false) annotation (Placement(transformation(extent={{-60,20},{-40,40}})));
  Buildings.BoundaryConditions.SolarIrradiation.DiffuseIsotropic HDifTilIso(til=til)
    annotation (Placement(transformation(extent={{0,40},{20,60}})));
  Buildings.BoundaryConditions.SolarIrradiation.DirectTiltedSurface HDirTil(
    til=til,
    azi=azi,
    lat(displayUnit="rad") = lat)
             annotation (Placement(transformation(extent={{0,0},{20,20}})));
  parameter Modelica.SIunits.Angle til=0.78539816339745 "Surface tilt";
  parameter Modelica.SIunits.Angle lat=0.8 "Latitude";
  parameter Modelica.SIunits.Angle azi=0 "Surface azimuth";
equation
  connect(weaDat.weaBus, HDirTil.weaBus) annotation (Line(
      points={{-40,30},{-20,30},{-20,10},{0,10}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, HDifTilIso.weaBus) annotation (Line(
      points={{-40,30},{-20,30},{-20,50},{0,50}},
      color={255,204,51},
      thickness=0.5));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(coordinateSystem(
          preserveAspectRatio=false)),
    experiment(StopTime=3.1536e+007, Interval=1800),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end Irradiation_solaire;
