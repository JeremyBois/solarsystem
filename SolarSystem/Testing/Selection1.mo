within SolarSystem.Testing;
model Selection1

Real chat=33. annotation(__Dymola_tag={"carnivore"});
Boolean souris=true annotation(__Dymola_tag={"carni"});
Boolean b1[3] = {false, false, false};
Boolean b2[3] = {false, true, false};
Boolean r1, r2;

equation
  r1 = Modelica.Math.BooleanVectors.anyTrue(b1);
  r2 = Modelica.Math.BooleanVectors.anyTrue(b2);

  annotation(__Dymola_selections={
             Selection(name="Tags", match={MatchVariable(name="*", tag={"carni*"}, newName="%path%truite")}),
             Selection(name="Variables", match={MatchVariable(name="*.phi|tau", newName="%path%chat")})});
end Selection1;
