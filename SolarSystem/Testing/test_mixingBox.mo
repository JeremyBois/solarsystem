within SolarSystem.Testing;
model test_mixingBox

  Buildings.Fluid.Actuators.Dampers.MixingBoxMinimumFlow eco(filteredOpening=false,
      redeclare package Medium = Medium)
    annotation (Placement(transformation(extent={{-10,0},{10,20}})));
  Buildings.Fluid.Sources.MassFlowSource_T outside(
    m_flow=1,
    redeclare package Medium = Medium,
    T=283.15,
    nPorts=2) annotation (Placement(transformation(extent={{-92,12},{-72,32}})));
protected
  Buildings.Fluid.Sources.Boundary_pT useless(
    nPorts=1,
    redeclare package Medium = Medium,
    use_T_in=false,
    T=313.15) annotation (Placement(transformation(
        extent={{6,-6},{-6,6}},
        rotation=180,
        origin={-80,-40})));
public
  Buildings.Fluid.Sources.MassFlowSource_T boundary_inverse1(
    nPorts=1,
    redeclare package Medium = Medium,
    m_flow=-1.5)
    annotation (Placement(transformation(extent={{90,50},{70,70}})));
protected
  Buildings.Fluid.Sources.Boundary_pT houseair(
    nPorts=1,
    redeclare package Medium = Medium,
    use_T_in=false,
    T=313.15) annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={80,-40})));
public
  replaceable package Medium = Buildings.Media.Air constrainedby
    Modelica.Media.Interfaces.PartialMedium annotation (
      __Dymola_choicesAllMatching=true);
    Modelica.Blocks.Sources.Constant yDamMin(k=1)
      annotation (Placement(transformation(extent={{-60,30},{-40,50}})));
    Modelica.Blocks.Sources.Step yDam(
    height=0.1,
    offset=0.45,
    startTime=60)
                 annotation (Placement(transformation(extent={{-60,60},{-40,80}})));
equation
  connect(eco.port_Sup, boundary_inverse1.ports[1]) annotation (Line(points={{10,
          16},{34,16},{34,60},{70,60}}, color={0,127,255}));
  connect(outside.ports[1], eco.port_OutMin) annotation (Line(points={{-72,24},{
          -72,20},{-14,20},{-10,20}}, color={0,127,255}));
  connect(houseair.ports[1], eco.port_Ret) annotation (Line(points={{74,-40},{42,
          -40},{42,4},{10,4}}, color={0,127,255}));
  connect(eco.port_Exh, useless.ports[1]) annotation (Line(points={{-10,4},{-34,
          4},{-50,4},{-50,-40},{-74,-40}}, color={0,127,255}));
  connect(outside.ports[2], eco.port_Out) annotation (Line(points={{-72,20},{-20,
          20},{-20,16},{-10,16}}, color={0,127,255}));
  connect(yDamMin.y, eco.yOutMin) annotation (Line(points={{-39,40},{-26,40},{-6,
          40},{-6,22}}, color={0,0,127}));
  connect(yDam.y, eco.y) annotation (Line(points={{-39,70},{-14,70},{0,70},{0,22}},
        color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end test_mixingBox;
