within SolarSystem.Testing;
model PVPanels_production
  "This example illustrates how to use PV panel models for my purpose"

  Buildings.Electrical.AC.OnePhase.Loads.Inductive Loads(V_nominal=120, mode=
        Buildings.Electrical.Types.Load.VariableZ_P_input)
    "Load taht consumes the power generted by the PVs"
    annotation (Placement(transformation(extent={{30,-42},{50,-22}})));
  Buildings.Electrical.AC.OnePhase.Sources.Grid grid(f=60, V=120)
    "Electrical grid model"
           annotation (Placement(transformation(extent={{-4,-54},{24,-82}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weather(
      computeWetBulbTemperature=false, filNam=weather_file.path)
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));
  Buildings.Electrical.AC.OnePhase.Sources.PVSimpleOriented pv_WEST(
    V_nominal=120,
    lat=weather_file.lattitude,
    A=5,
    til=0.57595865315813,
    azi=1.5707963267949) "PV array oriented"
    annotation (Placement(transformation(extent={{4,6},{24,26}})));
public
  Modelica.Blocks.Math.Sum sum_gains(nin=4, k={98.4,1,1,98.4})
    annotation (__Dymola_tag={"Power", "Internal"}, Placement(transformation(extent={{40,66},
            {52,78}})));
public
  Modelica.Blocks.Routing.Multiplex4 electrical_needs
    annotation (Placement(transformation(extent={{12,64},{28,80}})));
public
  Modelica.Blocks.Sources.CombiTimeTable schedules(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    table=[0,390,0,1.14,1,1,1; 3600,390,0,1.14,1,1,1; 7200,390,0,1.14,1,1,1;
        10800,390,0,1.14,1,1,1; 14400,390,0,1.14,1,1,1; 18000,390,0,1.14,1,1,1;
        21600,390,0,5.7,1,1,1; 25200,390,1.4,5.7,0,0,0; 28800,390,1.4,5.7,0,1,
        0.7; 32400,390,0,5.7,0,1,0.7; 36000,0,0,1.14,0,1,0.7; 39600,0,0,1.14,0,
        1,0.7; 43200,0,0,1.14,0,1,0.7; 46800,0,0,1.14,0,0.7,1; 50400,0,0,1.14,0,
        0.7,1; 54000,0,0,1.14,0,0.7,1; 57600,0,0,1.14,0,0.7,1; 61200,0,0,1.14,0,
        0.7,1; 64800,390,0,5.7,0,0,0; 68400,390,1.4,5.7,0,0,0; 72000,390,1.4,
        5.7,0,0,0; 75600,390,1.4,5.7,0,0,0; 79200,390,0,1.14,1,1,1; 82800,390,0,
        1.14,1,1,1; 86400,390,0,1.14,1,1,1; 90000,390,0,1.14,1,1,1; 93600,390,0,
        1.14,1,1,1; 97200,390,0,1.14,1,1,1; 100800,390,0,1.14,1,1,1; 104400,390,
        0,1.14,1,1,1; 108000,390,0,5.7,1,1,1; 111600,390,1.4,5.7,0,0,0; 115200,
        390,1.4,5.7,0,1,0.7; 118800,390,0,5.7,0,1,0.7; 122400,0,0,1.14,0,1,0.7;
        126000,0,0,1.14,0,1,0.7; 129600,0,0,1.14,0,1,0.7; 133200,0,0,1.14,0,0.7,
        1; 136800,0,0,1.14,0,0.7,1; 140400,0,0,1.14,0,0.7,1; 144000,0,0,1.14,0,
        0.7,1; 147600,0,0,1.14,0,0.7,1; 151200,390,0,5.7,0,0,0; 154800,390,1.4,
        5.7,0,0,0; 158400,390,1.4,5.7,0,0,0; 162000,390,1.4,5.7,0,0,0; 165600,
        390,0,1.14,1,1,1; 169200,390,0,1.14,1,1,1; 172800,390,0,1.14,1,1,1;
        176400,390,0,1.14,1,1,1; 180000,390,0,1.14,1,1,1; 183600,390,0,1.14,1,1,
        1; 187200,390,0,1.14,1,1,1; 190800,390,0,1.14,1,1,1; 194400,390,0,5.7,1,
        1,1; 198000,390,1.4,5.7,0,0,0; 201600,390,1.4,5.7,0,1,0.7; 205200,390,0,
        5.7,0,1,0.7; 208800,0,0,1.14,0,1,0.7; 212400,0,0,1.14,0,1,0.7; 216000,0,
        0,1.14,0,1,0.7; 219600,0,0,1.14,0,0.7,1; 223200,390,0,5.7,0,0.7,1;
        226800,390,0,5.7,0,0.7,1; 230400,390,0,5.7,0,0.7,1; 234000,390,0,5.7,0,
        0.7,1; 237600,390,0,5.7,0,0,0; 241200,390,1.4,5.7,0,0,0; 244800,390,1.4,
        5.7,0,0,0; 248400,390,1.4,5.7,0,0,0; 252000,390,0,1.14,1,1,1; 255600,
        390,0,1.14,1,1,1; 259200,390,0,1.14,1,1,1; 262800,390,0,1.14,1,1,1;
        266400,390,0,1.14,1,1,1; 270000,390,0,1.14,1,1,1; 273600,390,0,1.14,1,1,
        1; 277200,390,0,1.14,1,1,1; 280800,390,0,5.7,1,1,1; 284400,390,1.4,5.7,
        0,0,0; 288000,390,1.4,5.7,0,1,0.7; 291600,390,0,5.7,0,1,0.7; 295200,0,0,
        1.14,0,1,0.7; 298800,0,0,1.14,0,1,0.7; 302400,0,0,1.14,0,1,0.7; 306000,
        0,0,1.14,0,0.7,1; 309600,0,0,1.14,0,0.7,1; 313200,0,0,1.14,0,0.7,1;
        316800,0,0,1.14,0,0.7,1; 320400,0,0,1.14,0,0.7,1; 324000,390,0,5.7,0,0,
        0; 327600,390,1.4,5.7,0,0,0; 331200,390,1.4,5.7,0,0,0; 334800,390,1.4,
        5.7,0,0,0; 338400,390,0,1.14,1,1,1; 342000,390,0,1.14,1,1,1; 345600,390,
        0,1.14,1,1,1; 349200,390,0,1.14,1,1,1; 352800,390,0,1.14,1,1,1; 356400,
        390,0,1.14,1,1,1; 360000,390,0,1.14,1,1,1; 363600,390,0,1.14,1,1,1;
        367200,390,0,5.7,1,1,1; 370800,390,1.4,5.7,0,0,0; 374400,390,1.4,5.7,0,
        1,0.7; 378000,390,0,5.7,0,1,0.7; 381600,0,0,1.14,0,1,0.7; 385200,0,0,
        1.14,0,1,0.7; 388800,0,0,1.14,0,1,0.7; 392400,0,0,1.14,0,0.7,1; 396000,
        0,0,1.14,0,0.7,1; 399600,0,0,1.14,0,0.7,1; 403200,0,0,1.14,0,0.7,1;
        406800,0,0,1.14,0,0.7,1; 410400,390,0,5.7,0,0,0; 414000,390,1.4,5.7,0,0,
        0; 417600,390,1.4,5.7,0,0,0; 421200,390,1.4,5.7,0,0,0; 424800,390,0,
        1.14,1,1,1; 428400,390,0,1.14,1,1,1; 432000,390,0,1.14,1,1,1; 435600,
        390,0,1.14,1,1,1; 439200,390,0,1.14,1,1,1; 442800,390,0,1.14,1,1,1;
        446400,390,0,1.14,1,1,1; 450000,390,0,1.14,1,1,1; 453600,390,1.4,5.7,1,
        1,1; 457200,390,1.4,5.7,0,0,0; 460800,390,0.7,5.7,0,1,0.7; 464400,390,
        0.7,5.7,0,1,0.7; 468000,390,0.7,5.7,0,1,0.7; 471600,390,0.7,5.7,0,1,0.7;
        475200,390,0.7,5.7,0,1,0.7; 478800,390,0.7,5.7,0,0.7,1; 482400,390,0.7,
        5.7,0,0.7,1; 486000,390,0.7,5.7,0,0.7,1; 489600,390,0.7,5.7,0,0.7,1;
        493200,390,0.7,5.7,0,0.7,1; 496800,390,0.7,5.7,0,0,0; 500400,390,1.4,
        5.7,0,0,0; 504000,390,1.4,5.7,0,0,0; 507600,390,1.4,5.7,0,0,0; 511200,
        390,0,1.14,1,1,1; 514800,390,0,1.14,1,1,1; 518400,390,0,1.14,1,1,1;
        522000,390,0,1.14,1,1,1; 525600,390,0,1.14,1,1,1; 529200,390,0,1.14,1,1,
        1; 532800,390,0,1.14,1,1,1; 536400,390,0,1.14,1,1,1; 540000,390,1.4,5.7,
        1,1,1; 543600,390,1.4,5.7,0,0,0; 547200,390,0.7,5.7,0,1,0.7; 550800,390,
        0.7,5.7,0,1,0.7; 554400,390,0.7,5.7,0,1,0.7; 558000,390,0.7,5.7,0,1,0.7;
        561600,390,0.7,5.7,0,1,0.7; 565200,390,0.7,5.7,0,0.7,1; 568800,390,0.7,
        5.7,0,0.7,1; 572400,390,0.7,5.7,0,0.7,1; 576000,390,0.7,5.7,0,0.7,1;
        579600,390,0.7,5.7,0,0.7,1; 583200,390,0.7,5.7,0,0,0; 586800,390,1.4,
        5.7,0,0,0; 590400,390,1.4,5.7,0,0,0; 594000,390,1.4,5.7,0,0,0; 597600,
        390,0,1.14,1,1,1; 601200,390,0,1.14,1,1,1; 604800,390,0,1.14,1,1,1])
    "1 = Occupation, 2 = lumi�res, 3 = equipements, 4=occultation hiver, 5=occultation ete est, 6=occultation ete ouest"
    annotation (Placement(transformation(extent={{-100,-28},{-80,-8}})));
  Modelica.Blocks.Sources.Constant power_DHW_elec(k=200) "Elec DHW"
    annotation (Placement(transformation(extent={{-60,-20},{-40,0}})));
  Modelica.Blocks.Sources.Constant power_air_elec(k=100) "Elec heating"
    annotation (Placement(transformation(extent={{-60,-60},{-40,-40}})));
  Modelica.Blocks.Math.Gain to_negative(k=-1)
    "Power load negative to simulate a load" annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={68,44})));
public
  replaceable Data.Parameter.City_Datas.Bordeaux weather_file constrainedby
    Data.Parameter.City_Datas.City_Data
    annotation (Placement(transformation(extent={{-66,56},{-54,68}})));
public
  Modelica.Blocks.Continuous.Integrator Electric_needs(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{84,64},{100,80}})));
public
  Modelica.Blocks.Continuous.Integrator pv_production(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{84,8},{100,24}})));
public
  Modelica.Blocks.Continuous.Integrator Energy_from_grid(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{56,-80},{84,-52}})));
  Buildings.Electrical.AC.OnePhase.Sources.PVSimpleOriented pv_EAST(
    V_nominal=120,
    lat=weather_file.lattitude,
    A=5,
    til=0.57595865315813,
    azi=-1.5707963267949) "PV array oriented East"
    annotation (Placement(transformation(extent={{4,-20},{24,0}})));
  Buildings.Electrical.AC.OnePhase.Sources.PVSimpleOriented pv_SOUTH(
    V_nominal=120,
    lat=weather_file.lattitude,
    azi=0,
    A=0,
    til=0.57595865315813) "PV array oriented South"
    annotation (Placement(transformation(extent={{4,32},{24,52}})));
public
  Modelica.Blocks.Math.Sum sum_gains1(nin=3, k={1,1,1})
    annotation (__Dymola_tag={"Power", "Internal"}, Placement(transformation(extent={{44,10},
            {56,22}})));
equation

  Energy_from_grid.u = grid.P.real;
  connect(grid.terminal, Loads.terminal) annotation (Line(
      points={{10,-54},{10,-32},{30,-32}},
      color={0,120,120},
      smooth=Smooth.None));
  connect(weather.weaBus, pv_WEST.weaBus) annotation (Line(
      points={{-80,90},{-14,90},{-14,28},{14,28},{14,25}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(power_DHW_elec.y, electrical_needs.u2[1]) annotation (Line(points={{-39,
          -10},{-28,-10},{-28,74.4},{10.4,74.4}}, color={0,0,127}));
  connect(power_air_elec.y, electrical_needs.u3[1]) annotation (Line(points={{-39,
          -50},{-24,-50},{-24,69.6},{10.4,69.6}}, color={0,0,127}));
  connect(electrical_needs.y, sum_gains.u)
    annotation (Line(points={{28.8,72},{34,72},{38.8,72}}, color={0,0,127}));
  connect(sum_gains.y, to_negative.u)
    annotation (Line(points={{52.6,72},{68,72},{68,51.2}}, color={0,0,127}));
  connect(to_negative.y, Loads.Pow) annotation (Line(points={{68,37.4},{68,37.4},
          {68,-32},{50,-32}}, color={0,0,127}));
  connect(schedules.y[2], electrical_needs.u1[1]) annotation (Line(points={{-79,
          -18},{-72,-18},{-72,40},{-40,40},{-40,79.2},{10.4,79.2}}, color={0,0,127}));
  connect(schedules.y[3], electrical_needs.u4[1]) annotation (Line(points={{-79,
          -18},{-72,-18},{-72,-80},{-20,-80},{-20,64.8},{10.4,64.8}}, color={0,0,
          127}));
  connect(sum_gains.y, Electric_needs.u) annotation (Line(points={{52.6,72},{
          82.4,72}},              color={0,0,127}));
  connect(pv_SOUTH.P, sum_gains1.u[1]) annotation (Line(points={{25,49},{38,49},
          {38,15.2},{42.8,15.2}}, color={0,0,127}));
  connect(pv_WEST.P, sum_gains1.u[2]) annotation (Line(points={{25,23},{38,23},
          {38,16},{42.8,16}}, color={0,0,127}));
  connect(pv_EAST.P, sum_gains1.u[3]) annotation (Line(points={{25,-3},{38,-3},
          {38,16.8},{42.8,16.8}}, color={0,0,127}));
  connect(sum_gains1.y, pv_production.u)
    annotation (Line(points={{56.6,16},{82.4,16}}, color={0,0,127}));
  connect(pv_SOUTH.terminal, Loads.terminal) annotation (Line(points={{4,42},{
          -4,42},{-4,-32},{30,-32}}, color={0,120,120}));
  connect(pv_WEST.terminal, Loads.terminal) annotation (Line(points={{4,16},{-2,
          16},{-2,-32},{30,-32}}, color={0,120,120}));
  connect(pv_EAST.terminal, Loads.terminal) annotation (Line(points={{4,-10},{0,
          -10},{0,-32},{30,-32}}, color={0,120,120}));
  connect(weather.weaBus, pv_SOUTH.weaBus) annotation (Line(
      points={{-80,90},{-40,90},{-14,90},{-14,56},{14,56},{14,51}},
      color={255,204,51},
      thickness=0.5));
  connect(weather.weaBus, pv_EAST.weaBus) annotation (Line(
      points={{-80,90},{-42,90},{-14,90},{-14,4},{14,4},{14,-1}},
      color={255,204,51},
      thickness=0.5));
  annotation (experiment(
      StopTime=3.1536e+007,
      Interval=1600,
      Tolerance=1e-006,
      __Dymola_Algorithm="Cvode"),
    __Dymola_Commands(file=
          "modelica://Buildings/Resources/Scripts/Dymola/Electrical/AC/OnePhase/Sources/Examples/PVpanels.mos"
        "Simulate and plot"),
    Documentation(revisions="<html>
<ul>
<li>
August 5, 2014, by Marco Bonvini:<br/>
Revised model and documentation.
</li>
</ul>
</html>", info="<html>
<p>
This example shows how to use a simple PV model without orientation
as well a PV model with orientation. The power produced by the PV is
partially consumed by the load while the remaining part is fed into
the grid.
</p>
</html>"),
    __Dymola_experimentFlags(
      Advanced(GenerateVariableDependencies=false, OutputModelicaCode=false),
      Evaluate=true,
      OutputCPUtime=false,
      OutputFlatModelica=false));
end PVPanels_production;
