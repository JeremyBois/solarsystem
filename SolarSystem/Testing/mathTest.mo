within SolarSystem.Testing;
model mathTest

  Real a = 3;
  Real b;


equation

  b = exp(a);

  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end mathTest;
