within SolarSystem.Testing;
model schedules

  Data.Parameter.ParametricStudy201611.InternalLoads.RT2012 rT2012_1
    annotation (Placement(transformation(extent={{-68,24},{-48,44}})));
  Modelica.Blocks.Sources.CombiTimeTable occultations(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    table=rT2012_1.schedule)
    annotation (Placement(transformation(extent={{-4,16},{16,36}})));
  Data.Parameter.ParametricStudy201611.Instructions.Consigne19_hygroB
    consigne19_hygroB
    annotation (Placement(transformation(extent={{-72,-26},{-52,-6}})));
  Modelica.Blocks.Sources.CombiTimeTable occultations1(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    table=consigne19_hygroB.schedule)
    annotation (Placement(transformation(extent={{-6,-26},{14,-6}})));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
inner parameter Modelica.SIunits.Temperature SolarTempInstruction = 283.15;

end schedules;
