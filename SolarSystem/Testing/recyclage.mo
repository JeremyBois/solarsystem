within SolarSystem.Testing;
model recyclage
  Buildings.Fluid.Storage.StratifiedEnhanced tan(
    redeclare package Medium = Buildings.Media.Water,
    m_flow_nominal=0.1,
    VTan=1,
    hTan=1,
    dIns(displayUnit="mm") = 0.1)
    annotation (Placement(transformation(extent={{-42,2},{6,50}})));
  Buildings.Fluid.Sources.Boundary_pT source(
    nPorts=1,
    use_T_in=false,
    redeclare package Medium = Buildings.Media.Water,
    T=293.15) annotation (__Dymola_tag={"Temperature"}, Placement(
        transformation(
        extent={{6,-6},{-6,6}},
        rotation=0,
        origin={52,-36})));
  Buildings.Fluid.Sources.MassFlowSource_T puisage(
    nPorts=1,
    use_T_in=false,
    redeclare package Medium = Buildings.Media.Water,
    m_flow=-0.2,
    T=293.15) annotation (__Dymola_tag={"Temperature"}, Placement(
        transformation(
        extent={{6,-6},{-6,6}},
        rotation=90,
        origin={-70,86})));
public
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM HealthTank_SolarTopSplitter(
    redeclare package Medium = Buildings.Media.Water,
    m_flow_nominal=0.3*{1,-0.33,-0.66},
    dp_nominal={0,0,0})
             "Flow splitter"                                      annotation (
      Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={-70,26})));
  Buildings.Fluid.Movers.FlowControlled_m_flow pompeBouclage(
    redeclare package Medium = Buildings.Media.Water,
    m_flow_nominal=0.1,
    inputType=Buildings.Fluid.Types.InputType.Constant,
    nominalValuesDefineDefaultPressureCurve=true) annotation (__Dymola_tag={
        "Temperature"}, Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=180,
        origin={-54,-32})));
protected
  Buildings.Fluid.FixedResistances.Pipe perteBouclage(
    redeclare package Medium = MediumE,
    T_start=T_start,
    dp(start=0),
    length=10,
    from_dp=false,
    linearizeFlowResistance=false,
    m_flow_nominal=0.2,
    dp_nominal=0,
    nSeg=10,
    lambdaIns=0.04,
    v_nominal=0.3,
    useMultipleHeatPorts=false)
    annotation (Placement(transformation(extent={{-28,-38},{-12,-26}})));
equation
  connect(source.ports[1], tan.port_b) annotation (Line(points={{46,-36},{28,
          -36},{28,26},{6,26}}, color={0,127,255}));
  connect(tan.port_a, HealthTank_SolarTopSplitter.port_1)
    annotation (Line(points={{-42,26},{-64,26}}, color={0,127,255}));
  connect(HealthTank_SolarTopSplitter.port_3, puisage.ports[1])
    annotation (Line(points={{-70,32},{-70,80}}, color={0,127,255}));
  connect(HealthTank_SolarTopSplitter.port_2, pompeBouclage.port_a) annotation (
     Line(points={{-76,26},{-80,26},{-80,-32},{-60,-32}}, color={0,127,255}));
  connect(pompeBouclage.port_b, perteBouclage.port_a)
    annotation (Line(points={{-48,-32},{-28,-32}}, color={0,127,255}));
  connect(perteBouclage.port_b, tan.port_b)
    annotation (Line(points={{-12,-32},{6,-32},{6,26}}, color={0,127,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end recyclage;
