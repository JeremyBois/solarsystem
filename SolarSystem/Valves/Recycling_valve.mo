within SolarSystem.Valves;
model Recycling_valve

  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM
                                                  thermo_valve(
    redeclare package Medium = Medium,
    deltaM=deltaM,
    m_flow_nominal=m_flow_nominal,
    dp_nominal=dp_nominal)           annotation (Placement(transformation(
        extent={{24,22},{-24,-22}},
        rotation=90,
        origin={-66,0})));
  Modelica.Fluid.Interfaces.FluidPort_a port_1(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{0,88},{20,108}}),
        iconTransformation(extent={{0,88},{20,108}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_2(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{0,-108},{20,-88}}),
        iconTransformation(extent={{0,-108},{20,-88}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_3(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{-106,-10},{-86,10}}),
        iconTransformation(extent={{-106,-10},{-86,10}})));
  replaceable package Medium =
    Modelica.Media.Interfaces.PartialMedium "Medium in the component"
      annotation (choicesAllMatching = true);
  SolarSystem.Utilities.Other.Schedule control_Sj(table=
        control) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{8,-8},{-8,8}},
        rotation=-90,
        origin={-86,-68})));
  Buildings.Controls.Continuous.LimPID pid(
    controllerType=controllerType,
    k=k,
    Ti=Ti,
    Td=Td,
    wp=wp,
    wd=wd,
    Ni=Ni,
    Nd=Nd,
    initType=initType,
    limitsAtInit=limitsAtInit,
    xi_start=xi_start,
    xd_start=xd_start,
    y_start=y_start,
    reverseAction=reverseAction,
    yMax=yMax,
    yMin=yMin)
    annotation (Placement(transformation(extent={{-54,-50},{-34,-30}})));
  Modelica.Blocks.Interfaces.RealInput Tin "Temperature before heating slab"
                                      annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={60,100}), iconTransformation(
        extent={{-14,-14},{14,14}},
        rotation=180,
        origin={62,40})));

  parameter Real control[:,:]=[0.0,311.15]
    "Table matrix (time = first column; temperature to get = second column)"
                                                                            annotation (Dialog(group="Control"));

  parameter Modelica.Blocks.Types.SimpleController controllerType=Modelica.Blocks.Types.SimpleController.PI
    "Type of controller" annotation (Dialog(tab="Controller"));
  parameter Real k=0.2 "Gain of controller" annotation (Dialog(tab="Controller"));
  parameter Modelica.SIunits.Time Ti=100 "Time constant of Integrator block"
    annotation (Dialog(tab="Controller"));
  parameter Modelica.SIunits.Time Td=200 "Time constant of Derivative block"
    annotation (Dialog(tab="Controller"));
  parameter Real yMax=1 "Upper limit of output"
    annotation (Dialog(tab="Controller"));
  parameter Real yMin=0 "Lower limit of output"
    annotation (Dialog(tab="Controller"));
  parameter Real wp=0.8 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Controller"));
  parameter Real wd=0.2 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Controller"));
  parameter Real Ni=0.9 "Ni*Ti is time constant of anti-windup compensation"
    annotation (Dialog(tab="Controller"));
  parameter Real Nd=10 "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Controller"));
  parameter Boolean reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Controller"));
  parameter Modelica.Blocks.Types.InitPID initType=Modelica.Blocks.Types.InitPID.DoNotUse_InitialIntegratorState
    "Type of initialization (1: no init, 2: steady state, 3: initial state, 4: initial output)"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Boolean limitsAtInit=true
    "= false, if limits are ignored during initializiation"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Real xi_start=0
    "Initial or guess value value for integrator output (= integrator state)"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Real xd_start=0
    "Initial or guess value for state of derivative block"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Real y_start=0 "Initial value of output"
    annotation (Dialog(tab="Controller", group="Initialization"));
  Modelica.Blocks.Interfaces.RealOutput Sj_loop
    "Mass flow rate for recycled water" annotation (Placement(transformation(
        extent={{20,-20},{-20,20}},
        rotation=180,
        origin={94,24}),  iconTransformation(
        extent={{-14,-14},{14,14}},
        rotation=180,
        origin={-66,-80})));
  Modelica.Blocks.Interfaces.RealOutput Sj_out "Mass flow rate for heat pump"
    annotation (Placement(transformation(
        extent={{20,-20},{-20,20}},
        rotation=180,
        origin={92,64}),  iconTransformation(
        extent={{-14,-14},{14,14}},
        rotation=180,
        origin={-66,80})));
  Modelica.Blocks.Math.MultiSum gain_Sj(nu=2, k={1,-1}) annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={40,64})));
  Modelica.Blocks.Math.MultiProduct gain_Sj_loop(nu=2) annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={0,36})));
  Modelica.Blocks.Interfaces.RealInput Sj_flow "Sj mass flow rate" annotation (
      Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={-74,100}), iconTransformation(
        extent={{-14,-14},{14,14}},
        rotation=180,
        origin={62,-38})));
  parameter Modelica.SIunits.Pressure dp_nominal[3] = {1000,0,1000}
    "Pressure. Set negative at outflowing ports.";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal[3] = 0.153*1.7*{1,-1,-1}
    "Mass flow rate. Set negative at outflowing ports.";
  parameter Real deltaM=0.3
    "Fraction of nominal mass flow rate where transition to turbulent occurs";
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-10,-4})));
  Modelica.Blocks.Logical.Greater greater
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={30,-36})));
  Modelica.Blocks.Sources.RealExpression ceiling(y=PID_start)
    annotation (Placement(transformation(extent={{76,-86},{56,-66}})));
  Modelica.Blocks.Sources.RealExpression no_modulation(y=0) annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=-90,
        origin={-2,-40})));
  parameter Modelica.Blocks.Interfaces.RealOutput PID_start=0.0
    "If Tin > PID_start compute PID control else pump Sj_out to max flow"
                                                                         annotation (Dialog(group="Control"));
equation
  connect(control_Sj.y[1], pid.u_s) annotation (Line(
      points={{-86,-59.2},{-86,-40},{-56,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tin, pid.u_m) annotation (Line(
      points={{60,100},{60,-60},{-44,-60},{-44,-52}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(thermo_valve.port_3, port_3) annotation (Line(
      points={{-88,1.33227e-015},{-88,0},{-96,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(port_1, thermo_valve.port_1) annotation (Line(
      points={{10,98},{10,60},{-66,60},{-66,24}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(thermo_valve.port_2, port_2) annotation (Line(
      points={{-66,-24},{-66,-80},{10,-80},{10,-98}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Sj_flow, gain_Sj_loop.u[1]) annotation (Line(
      points={{-74,100},{-74,38.1},{-6,38.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Sj_flow, gain_Sj.u[1]) annotation (Line(
      points={{-74,100},{-74,66.1},{34,66.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gain_Sj_loop.y, gain_Sj.u[2]) annotation (Line(
      points={{7.02,36},{20,36},{20,61.9},{34,61.9}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid.y, switch1.u1) annotation (Line(
      points={{-33,-40},{-18,-40},{-18,-16}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(greater.y, switch1.u2) annotation (Line(
      points={{30,-25},{30,-20},{-10,-20},{-10,-16}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(ceiling.y, greater.u2) annotation (Line(
      points={{55,-76},{38,-76},{38,-48}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(switch1.y, gain_Sj_loop.u[2]) annotation (Line(
      points={{-10,7},{-10,33.9},{-6,33.9}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(no_modulation.y, switch1.u3) annotation (Line(
      points={{-2,-29},{-2,-16}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tin, greater.u1) annotation (Line(
      points={{60,100},{60,-56},{30,-56},{30,-48}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gain_Sj.y, Sj_out) annotation (Line(
      points={{47.02,64},{92,64}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gain_Sj_loop.y, Sj_loop) annotation (Line(
      points={{7.02,36},{40,36},{40,24},{94,24}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{80,100}}),  graphics), Icon(coordinateSystem(extent={{-100,-100},
            {80,100}},             preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{100,40},{-100,-40}},
          lineColor={0,0,0},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={192,192,192},
          origin={8,0},
          rotation=90),
        Rectangle(
          extent={{100,23},{-100,-23}},
          lineColor={0,0,0},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={0,127,255},
          origin={8,1},
          rotation=-90),
        Polygon(
          points={{2,-2},{-80,60},{-80,-58},{2,-2}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{12,-8},{-50,-90},{70,-90},{12,-8}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{39,-2},{-39,60},{-39,-60},{39,-2}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          origin={11,52},
          rotation=-90)}),
    Documentation(info="<html>
<p>This model can be used to limit the fluid temperature (<b>port_2</b>) by mixing two fluid together.</p>
<p><b>Port_1</b> and <b>port_3</b> fluid mass flow rate are compute not to exceed the<b> control </b>temperature value.</p>
<p>Control output fluid temperature only if <b>port_1</b> fluid temperature reachs <b>PID_start value</b>, else <b>port_3</b> mass flow rate equal to zero.</p>
</html>"));
end Recycling_valve;
