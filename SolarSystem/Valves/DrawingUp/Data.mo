within SolarSystem.Valves.DrawingUp;
package Data
  extends Modelica.Icons.MaterialPropertiesPackage;
  record combitable_parameters "Group of parameters for combitable"

  //
  // Schedules for temperature setpoint, solar setpoint and minimal volumic flow rate
  //
     parameter Boolean tableOnFile=false
      "= true, if table is defined on file or in function usertab"
       annotation (Dialog(tab="Schedules", group="Tables data definition"));
       parameter Real table[:, :] = fill(0.0, 0, 2)
      "Setpoint for the algorithm control (first=time). 1=DHW volume to draw [l])"
         annotation (Dialog(tab="Schedules",group="Tables data definition",enable=not tableOnFile));
     parameter String tableName="NoName"
      "Tables names on file or in function usertab (see docu)"
       annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
     parameter String fileName="NoName" "Files where matrix are stored"
       annotation (Dialog(tab="Schedules",
         group="Tables data definition",
         enable=tableOnFile,
         loadSelector(filter="Text files (*.txt);;MATLAB MAT-files (*.mat)",
             caption="Open file in which table is present")));
     parameter Boolean verboseRead=true
      "= true, if info message that file is loading is to be printed"
       annotation (Dialog(tab="Schedules",group="Tables data definition",enable=tableOnFile));
      parameter Integer columns[:]=2:size(table, 2)
      "Columns of table to be interpolated"
        annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
     parameter Modelica.Blocks.Types.Smoothness smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments
      "Smoothness of table interpolation"
       annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
     parameter Modelica.Blocks.Types.Extrapolation extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic
      "Extrapolation of data outside the definition range"
       annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
     parameter Real offset[:]={0} "Offsets of output signals"
       annotation (Dialog(tab="Schedules",group="Tables data interpretation"));
     parameter Modelica.SIunits.Time startTime=0
      "Output = offset for time < startTime"
       annotation (Dialog(tab="Schedules",group="Tables data interpretation"));

    annotation (Diagram(coordinateSystem(extent={{-1480,360},{-1340,460}})), Icon(coordinateSystem(
            extent={{-1480,360},{-1340,460}})),
      Documentation(info="<html>
</html>"));
  end combitable_parameters;
end Data;
