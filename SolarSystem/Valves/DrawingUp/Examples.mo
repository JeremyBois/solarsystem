within SolarSystem.Valves.DrawingUp;
package Examples
  extends Modelica.Icons.ExamplesPackage;
  model Test_Drawing_up_mflow "Test of drawing up implementation"
    extends Modelica.Icons.Example;

    Buildings.Fluid.Sources.Boundary_pT boundary(          redeclare package
        Medium = SolarSystem.Models.Validation.Tank.MediumE,
      use_T_in=true,
      nPorts=1)                                              annotation (
        Placement(transformation(
          extent={{8,-8},{-8,8}},
          rotation=180,
          origin={-50,20})));
    Modelica.Blocks.Sources.Step Tb(
      startTime=10000,
      height=50,
      offset=11 + 273.15)
      annotation (Placement(transformation(extent={{-100,62},{-80,82}})));
    Modelica.Blocks.Sources.RealExpression Te(y=10 + 273.15)
      annotation (Placement(transformation(extent={{-100,-86},{-80,-66}})));
    Valves.DrawingUp.Drawing_up_mflow drawing_up_mflow(redeclare package Medium =
          SolarSystem.Models.Validation.Tank.MediumE,
      smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
      extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
      table=[0,0; 3600,0; 7200,0; 10800,0; 14400,0; 18000,0; 21600,16.5; 25200,16.5;
          28800,16.5; 32400,0; 36000,0; 39600,0; 43200,0; 46800,16.5; 50400,16.5;
          54000,16.5; 57600,0; 61200,0; 64800,58.6575; 68400,58.6575; 72000,58.6575;
          75600,0; 79200,0; 82800,0; 86400,0; 90000,0; 93600,0; 97200,0; 100800,0;
          104400,0; 108000,16.5; 111600,16.5; 115200,16.5; 118800,0; 122400,0; 126000,
          0; 129600,0; 133200,16.5; 136800,16.5; 140400,16.5; 144000,0; 147600,0;
          151200,58.6575; 154800,58.6575; 158400,58.6575; 162000,0; 165600,0; 169200,
          0; 172800,0; 176400,0; 180000,0; 183600,0; 187200,0; 190800,0; 194400,16.5;
          198000,16.5; 201600,16.5; 205200,0; 208800,0; 212400,0; 216000,0; 219600,
          16.5; 223200,16.5; 226800,16.5; 230400,0; 234000,0; 237600,58.6575; 241200,
          58.6575; 244800,58.6575; 248400,0; 252000,0; 255600,0; 259200,0; 262800,
          0; 266400,0; 270000,0; 273600,0; 277200,0; 280800,16.5; 284400,16.5; 288000,
          16.5; 291600,0; 295200,0; 298800,0; 302400,0; 306000,16.5; 309600,16.5;
          313200,16.5; 316800,0; 320400,0; 324000,58.6575; 327600,58.6575; 331200,
          58.6575; 334800,0; 338400,0; 342000,0; 345600,0; 349200,0; 352800,0; 356400,
          0; 360000,0; 363600,0; 367200,16.5; 370800,16.5; 374400,16.5; 378000,0;
          381600,0; 385200,0; 388800,0; 392400,16.5; 396000,16.5; 399600,16.5; 403200,
          0; 406800,0; 410400,58.6575; 414000,58.6575; 417600,58.6575; 421200,0; 424800,
          0; 428400,0; 432000,0; 435600,0; 439200,0; 442800,0; 446400,0; 450000,0;
          453600,16.5; 457200,16.5; 460800,16.5; 464400,0; 468000,0; 471600,0; 475200,
          0; 478800,16.5; 482400,16.5; 486000,16.5; 489600,0; 493200,0; 496800,58.6575;
          500400,58.6575; 504000,58.6575; 507600,0; 511200,0; 514800,0; 518400,0;
          522000,0; 525600,0; 529200,0; 532800,0; 536400,0; 540000,16.5; 543600,16.5;
          547200,16.5; 550800,0; 554400,0; 558000,0; 561600,0; 565200,16.5; 568800,
          16.5; 572400,16.5; 576000,0; 579600,0; 583200,58.6575; 586800,58.6575; 590400,
          58.6575; 594000,0; 597600,0; 601200,0; 604800,0])
      annotation (Placement(transformation(extent={{20,-22},{80,42}})));
  protected
    inner Modelica.Fluid.System system
      annotation (Placement(transformation(extent={{80,80},{100,100}})));
  equation
    connect(Tb.y, boundary.T_in) annotation (Line(
        points={{-79,72},{-70,72},{-70,16.8},{-59.6,16.8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(boundary.ports[1], drawing_up_mflow.tank_port) annotation (Line(
        points={{-42,20},{0,20},{0,11.6},{23,11.6}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Te.y, drawing_up_mflow.Te) annotation (Line(
        points={{-79,-76},{0,-76},{0,-14},{23,-14}},
        color={0,0,127},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
              -100},{100,100}}), graphics), Documentation(info="<html>
<p>Test drawing up implementation using a daily schedule.</p>
<p>Be aware that only tank water is extracted not a mix between cold and hot water. </p>
</html>"));
  end Test_Drawing_up_mflow;

  model Test_Drawing_up_mflow_smooth "Test of drawing up implementation"
    extends Modelica.Icons.Example;

    Buildings.Fluid.Sources.Boundary_pT boundary(          redeclare package
        Medium = SolarSystem.Models.Validation.Tank.MediumE,
      use_T_in=true,
      nPorts=1)                                              annotation (
        Placement(transformation(
          extent={{8,-8},{-8,8}},
          rotation=180,
          origin={-50,20})));
    Modelica.Blocks.Sources.Step Tb(
      startTime=10000,
      height=50,
      offset=11 + 273.15)
      annotation (Placement(transformation(extent={{-100,62},{-80,82}})));
    Modelica.Blocks.Sources.RealExpression Te(y=10 + 273.15)
      annotation (Placement(transformation(extent={{-100,-86},{-80,-66}})));
    Drawing_up_mflow_smooth           drawing_up_mflow(redeclare package Medium =
          SolarSystem.Models.Validation.Tank.MediumE,
      smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
      extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
      table=[0,0; 3600,0; 7200,0; 10800,0; 14400,0; 18000,0; 21600,16.5; 25200,16.5;
          28800,16.5; 32400,0; 36000,0; 39600,0; 43200,0; 46800,16.5; 50400,16.5;
          54000,16.5; 57600,0; 61200,0; 64800,58.6575; 68400,58.6575; 72000,58.6575;
          75600,0; 79200,0; 82800,0; 86400,0; 90000,0; 93600,0; 97200,0; 100800,0;
          104400,0; 108000,16.5; 111600,16.5; 115200,16.5; 118800,0; 122400,0; 126000,
          0; 129600,0; 133200,16.5; 136800,16.5; 140400,16.5; 144000,0; 147600,0;
          151200,58.6575; 154800,58.6575; 158400,58.6575; 162000,0; 165600,0; 169200,
          0; 172800,0; 176400,0; 180000,0; 183600,0; 187200,0; 190800,0; 194400,16.5;
          198000,16.5; 201600,16.5; 205200,0; 208800,0; 212400,0; 216000,0; 219600,
          16.5; 223200,16.5; 226800,16.5; 230400,0; 234000,0; 237600,58.6575; 241200,
          58.6575; 244800,58.6575; 248400,0; 252000,0; 255600,0; 259200,0; 262800,
          0; 266400,0; 270000,0; 273600,0; 277200,0; 280800,16.5; 284400,16.5; 288000,
          16.5; 291600,0; 295200,0; 298800,0; 302400,0; 306000,16.5; 309600,16.5;
          313200,16.5; 316800,0; 320400,0; 324000,58.6575; 327600,58.6575; 331200,
          58.6575; 334800,0; 338400,0; 342000,0; 345600,0; 349200,0; 352800,0; 356400,
          0; 360000,0; 363600,0; 367200,16.5; 370800,16.5; 374400,16.5; 378000,0;
          381600,0; 385200,0; 388800,0; 392400,16.5; 396000,16.5; 399600,16.5; 403200,
          0; 406800,0; 410400,58.6575; 414000,58.6575; 417600,58.6575; 421200,0; 424800,
          0; 428400,0; 432000,0; 435600,0; 439200,0; 442800,0; 446400,0; 450000,0;
          453600,16.5; 457200,16.5; 460800,16.5; 464400,0; 468000,0; 471600,0; 475200,
          0; 478800,16.5; 482400,16.5; 486000,16.5; 489600,0; 493200,0; 496800,58.6575;
          500400,58.6575; 504000,58.6575; 507600,0; 511200,0; 514800,0; 518400,0;
          522000,0; 525600,0; 529200,0; 532800,0; 536400,0; 540000,16.5; 543600,16.5;
          547200,16.5; 550800,0; 554400,0; 558000,0; 561600,0; 565200,16.5; 568800,
          16.5; 572400,16.5; 576000,0; 579600,0; 583200,58.6575; 586800,58.6575; 590400,
          58.6575; 594000,0; 597600,0; 601200,0; 604800,0])
      annotation (Placement(transformation(extent={{20,-22},{80,42}})));
  protected
    inner Modelica.Fluid.System system
      annotation (Placement(transformation(extent={{80,80},{100,100}})));
  equation
    connect(Tb.y, boundary.T_in) annotation (Line(
        points={{-79,72},{-70,72},{-70,16.8},{-59.6,16.8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(boundary.ports[1], drawing_up_mflow.tank_port) annotation (Line(
        points={{-42,20},{0,20},{0,11.6},{23,11.6}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Te.y, drawing_up_mflow.Te) annotation (Line(
        points={{-79,-76},{0,-76},{0,-14},{23,-14}},
        color={0,0,127},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
              -100},{100,100}}), graphics), Documentation(info="<html>
<p>Test drawing up implementation using a daily schedule.</p>
<p>Be aware that only tank water is extracted not a mix between cold and hot water. </p>
</html>"));
  end Test_Drawing_up_mflow_smooth;
end Examples;
