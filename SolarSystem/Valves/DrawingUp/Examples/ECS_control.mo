within SolarSystem.Valves.DrawingUp.Examples;
model ECS_control
  extends Modelica.Icons.Example;
  Buildings.Fluid.Sources.FixedBoundary
                                 boundary2(
    redeclare package Medium =
        MediumA, nPorts=1)
    annotation (Placement(transformation(extent={{100,0},{80,20}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        MediumA,
    nPorts=1,
    m_flow=2,
    use_T_in=true)
    annotation (Placement(transformation(extent={{-86,-2},{-66,18}})));
  Buildings.Fluid.MixingVolumes.MixingVolume VolPanel(
    nPorts=3,
    redeclare package Medium =
        MediumA,
    V(displayUnit="l") = 1,
    m_flow_nominal=2)
              annotation (Placement(transformation(extent={{-50,20},{
            -14,44}})));
  Water_Drawing_up_control water_Drawing_model(
    table=[0,0,0; 5.99,0,0; 6,250,0.04; 8.99,250,0.04; 9,120,0.06; 9.99,120,
        0.06; 10,0,0; 10.99,0,0; 11,0,0; 11.99,0,0; 12,250,0.07; 13.49,250,
        0.07; 13.5,0,0; 14.49,0,0; 14.5,0,0; 15.49,0,0; 15.5,0,0; 16.49,0,0;
        16.5,0,0; 17.49,0,0; 17.5,0,0; 17.99,0,0; 18,0,0; 18.99,0,0; 19,200,
        0.05; 21,200,0.05; 21.01,0,0; 24,0,0],
    redeclare package Medium =
        MediumA,
    f_cut=1/120,
    gain=0.7)
    annotation (Placement(transformation(extent={{68,70},{98,100}})));
  Modelica.Blocks.Sources.Step step(
    height=40,
    offset=293.15,
    startTime=10000)
    annotation (Placement(transformation(extent={{-94,58},{-74,78}})));
equation
  connect(boundary1.ports[1], VolPanel.ports[1]) annotation (Line(
      points={{-66,8},{-38,8},{-38,20},{-36.8,20}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(VolPanel.ports[2], boundary2.ports[1]) annotation (Line(
      points={{-32,20},{-32,6},{80,6},{80,10}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(VolPanel.ports[3], water_Drawing_model.port_a) annotation (
      Line(
      points={{-27.2,20},{-34,20},{-34,18},{-10,18},{-10,46},{83,46},{
          83,70.1875}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(step.y, boundary1.T_in) annotation (Line(
      points={{-73,68},{-68,68},{-68,34},{-94,34},{-94,12},{-88,12}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(graphics), experiment(StopTime=1e+006, Interval=
          120),
    __Dymola_experimentSetupOutput);
end ECS_control;
