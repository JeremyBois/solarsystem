within SolarSystem.Valves.DrawingUp.Examples;
model ECS_massFlow
  extends Modelica.Icons.Example;
  Buildings.Fluid.Sources.FixedBoundary
                                 boundary2(
    redeclare package Medium =
        MediumA, nPorts=1)
    annotation (Placement(transformation(extent={{100,0},{80,20}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        MediumA,
    nPorts=1,
    m_flow=2,
    use_T_in=true)
    annotation (Placement(transformation(extent={{-84,-4},{-64,16}})));
  Buildings.Fluid.MixingVolumes.MixingVolume VolPanel(
    nPorts=3,
    redeclare package Medium =
        MediumA,
    V(displayUnit="l") = 1,
    m_flow_nominal=2)
              annotation (Placement(transformation(extent={{-50,20},{
            -14,44}})));
  SolarSystem.Valves.DrawingUp.Water_Drawing_massFlow water_Drawing_model(m_flow_nominal=0.05,
      redeclare package Medium =
        MediumA)
    annotation (Placement(transformation(extent={{60,60},{100,100}})));
  Modelica.Blocks.Sources.Step step(
    height=40,
    offset=293.15,
    startTime=10000)
    annotation (Placement(transformation(extent={{-100,60},{-80,80}})));
equation
  connect(boundary1.ports[1], VolPanel.ports[1]) annotation (Line(
      points={{-64,6},{-38,6},{-38,20},{-36.8,20}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(VolPanel.ports[2], boundary2.ports[1]) annotation (Line(
      points={{-32,20},{-32,6},{80,6},{80,10}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(VolPanel.ports[3], water_Drawing_model.port_a) annotation (
      Line(
      points={{-27.2,20},{-34,20},{-34,18},{-10,18},{-10,46},{80,46},{
          80,60.25}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(step.y, boundary1.T_in) annotation (Line(
      points={{-79,70},{-72,70},{-72,28},{-94,28},{-94,10},{-86,10}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(graphics), experiment(StopTime=100000, Interval=
          120),
    __Dymola_experimentSetupOutput);
end ECS_massFlow;
