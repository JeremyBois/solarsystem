within SolarSystem.Valves.DrawingUp;
package Old "Old implementation of drawing up control"
  extends Modelica.Icons.ObsoleteModel;
  model Drawing_up

    parameter Real table[:,:]=[0,0; 5.99,0; 6,250; 8.99,250; 9,120; 9.99,120; 10,0; 10.99,0; 11,
          0; 11.99,0; 12,250; 13.49,250; 13.5,0; 14.49,0; 14.5,0; 15.49,0;
          15.5,0; 16.49,0; 16.5,0; 17.49,0; 17.5,0; 17.99,0; 18,0; 18.99,0;
          19,200; 21,200; 21.01,0; 24,0]
      "table matrix (col1 = decimal hour ; col2 = Element"
      annotation (Dialog(group="Table", enable = not tableOnFile));

    parameter Boolean tableOnFile=false
      "true, if table is defined on file or in function usertab"
      annotation (Dialog(group="Table"));

    parameter String fileName="No name" "file where matrix is stored"
      annotation(Dialog(group="Table", enable = tableOnFile,
                           __Dymola_loadSelector(filter="Text files (*.txt);;Matlab files (*.mat)",
                           caption="Open file in which table is present")));

    Modelica.Blocks.Tables.CombiTable1D combiTable1D(
      tableOnFile=tableOnFile,
      table=table,
      fileName=fileName)
      annotation (Placement(transformation(extent={{-58,48},{-42,64}})));
    Modelica.Blocks.Sources.RealExpression minOfDay(y=(time - Tjour)/3600)
      "Transform time in secondes to hours"
      annotation (Placement(transformation(extent={{-100,48},{-78,64}})));
    Modelica.Blocks.Interfaces.RealOutput State(start=1) "1 = Open, 0 = Close"
      annotation (Placement(transformation(extent={{114,-20},{154,20}}),
          iconTransformation(extent={{110,-14},{138,14}})));

    Modelica.Blocks.Sources.RealExpression Conso_model(y=Conso_tot)
      "Transform time in secondes to hours"
      annotation (Placement(transformation(extent={{-100,24},{-78,40}})));
    Modelica.Blocks.Math.IntegerChange new_step "Change if new step begins"
      annotation (Placement(transformation(extent={{22,-98},{38,-82}})));
    Modelica.Blocks.Math.RealToInteger realToInteger
      annotation (Placement(transformation(extent={{-38,-98},{-22,-82}})));
    Modelica.Blocks.Logical.Switch switch
      annotation (Placement(transformation(extent={{42,-10},{62,10}})));
    Modelica.Blocks.Sources.RealExpression Min(y=0)
      "Transform time in secondes to hours"
      annotation (Placement(transformation(extent={{24,-16},{34,0}})));
    Modelica.Blocks.Sources.RealExpression Max(y=1)
      "Transform time in secondes to hours"
      annotation (Placement(transformation(extent={{24,0},{34,16}})));
      // public
  Modelica.Blocks.Logical.Less     compare
      annotation (Placement(transformation(extent={{-32,-10},{-12,10}})));
    Modelica.Blocks.Interfaces.RealInput Conso "Actual drawing up"
      annotation (Placement(transformation(extent={{-120,-20},{-80,20}}),
          iconTransformation(extent={{-114,-14},{-86,14}})));
  Real Conso_tot;

  discrete Real Tjour;

  equation
  // Keep trace of actual element level
    der(Conso_tot)=der(Conso);

  //Every 24 hours, update Tjour
    when mod(time, 3600*24) < 1 then
      Tjour = time;
    end when;

  // New schedule
    when new_step.y then
      reinit(Conso_tot,0);
    end when;

    connect(minOfDay.y, combiTable1D.u[1]) annotation (Line(
        points={{-76.9,56},{-59.6,56}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(combiTable1D.y[1], realToInteger.u) annotation (Line(
        points={{-41.2,56},{-34,56},{-34,26},{-64,26},{-64,-90},{-39.6,-90}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Max.y, switch.u1) annotation (Line(
        points={{34.5,8},{40,8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Min.y, switch.u3) annotation (Line(
        points={{34.5,-8},{40,-8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(combiTable1D.y[1], compare.u2) annotation (Line(
        points={{-41.2,56},{-34,56},{-34,26},{-64,26},{-64,-8},{-34,-8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(switch.y, State)
                            annotation (Line(
        points={{63,0},{134,0}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(realToInteger.y, new_step.u) annotation (Line(
        points={{-21.2,-90},{20.4,-90}},
        color={255,127,0},
        smooth=Smooth.None));
    connect(Conso_model.y, compare.u1) annotation (Line(
        points={{-76.9,32},{-44,32},{-44,0},{-34,0}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(compare.y, switch.u2) annotation (Line(
        points={{-11,0},{40,0}},
        color={255,0,255},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-100,-100},{120,100}},
            preserveAspectRatio=true), defaultComponentName = "Consumption",
                        graphics), Icon(coordinateSystem(extent={{-100,-100},{120,
              100}},
            preserveAspectRatio=true),  graphics),
      experiment(Interval=120),
      Documentation(info="<html>
<p>This model makes it possible to define a drawing up according to a time profile.</p>
<p>As long as the desired quantity is not reached one continues.</p>
<p>Nevertheless, if the desired quantity is not reached before the end of the current step, the quantity is reset for the next step</p>
</html>"));
  end Drawing_up;

  model Drawing_up_control

    parameter Real table[:,:]=[0,0,0; 5.99,
          0,0; 6,250,0.05; 8.99,250,0.05; 9,120,0.05; 9.99,120,0.05; 10,0,0; 10.99,
          0,0; 11,0,0; 11.99,0,0; 12,250,0.05; 13.49,250,0.05; 13.5,0,0; 14.49,0,0;
          14.5,0,0; 15.49,0,0; 15.5,0,0; 16.49,0,0; 16.5,0,0; 17.49,0,0; 17.5,0,0;
          17.99,0,0; 18,0,0; 18.99,0,0; 19,200,0.05; 21,200,0.05; 21.01,0,0; 24,0,
          0]
      "table matrix (col1 = decimal hour ; col2 = Element ; col3 = Element2"
      annotation (Dialog(group="Table", enable = not tableOnFile));

    parameter Boolean tableOnFile=false
      "true, if table is defined on file or in function usertab"
      annotation (Dialog(group="Table"));

    parameter String fileName="No name" "file where matrix is stored"
      annotation(Dialog(group="Table", enable = tableOnFile,
                           __Dymola_loadSelector(filter="Text files (*.txt);;Matlab files (*.mat)",
                           caption="Open file in which table is present")));

    parameter Modelica.SIunits.Time start_value=0 "Start day of simulation"
      annotation (Dialog(group="Table"));

    Modelica.Blocks.Tables.CombiTable1Ds combiTable1D(
      tableOnFile=tableOnFile,
      table=table,
      fileName=fileName)
      annotation (Placement(transformation(extent={{-58,48},{-42,64}})));
    Modelica.Blocks.Sources.RealExpression minOfDay(y=(time - Tjour)/3600)
      "Transform time in secondes to hours"
      annotation (Placement(transformation(extent={{-100,48},{-78,64}})));

    Modelica.Blocks.Interfaces.RealOutput State(start=1) "1 = Open, 0 = Close"
      annotation (Placement(transformation(extent={{114,-20},{154,20}}),
          iconTransformation(extent={{110,-14},{138,14}})));

    Modelica.Blocks.Sources.RealExpression Conso_model(y=Conso_tot)
      "Transform time in secondes to hours"
      annotation (Placement(transformation(extent={{-100,24},{-78,40}})));
    Modelica.Blocks.Math.IntegerChange new_step "Change if new step begins"
      annotation (Placement(transformation(extent={{22,-98},{38,-82}})));
    Modelica.Blocks.Math.RealToInteger realToInteger
      annotation (Placement(transformation(extent={{-38,-98},{-22,-82}})));
    Modelica.Blocks.Logical.Switch switch
      annotation (Placement(transformation(extent={{42,-10},{62,10}})));
    Modelica.Blocks.Sources.RealExpression Min(y=0)
      "Transform time in secondes to hours"
      annotation (Placement(transformation(extent={{24,-16},{34,0}})));
    Modelica.Blocks.Sources.RealExpression Max(y=1)
      "Transform time in secondes to hours"
      annotation (Placement(transformation(extent={{24,0},{34,16}})));

  Modelica.Blocks.Logical.Less   compare
      annotation (Placement(transformation(extent={{-32,-10},{-12,10}})));
    Modelica.Blocks.Interfaces.RealInput Conso "Actual drawing up"
      annotation (Placement(transformation(extent={{-120,-20},{-80,20}}),
          iconTransformation(extent={{-114,-14},{-86,14}})));
  Real Conso_tot;

  discrete Real Tjour(start=start_value);

    Modelica.Blocks.Interfaces.RealOutput control(start=0)
      "Flow for each drawing_up (allow to draw more or less quicker during schedule)"
      annotation (Placement(transformation(extent={{112,60},{152,100}}),
          iconTransformation(extent={{110,58},{138,86}})));
  equation
  // Keep trace of actual element level
    der(Conso_tot)=der(Conso);

  //Every 24 hours, update Tjour
    when mod(time, 3600*24) < 1 then
      Tjour = time;
    end when;

  // New schedule
    when new_step.y then
      reinit(Conso_tot,0);
    end when;

    connect(combiTable1D.y[1], realToInteger.u) annotation (Line(
        points={{-41.2,56},{-34,56},{-34,26},{-64,26},{-64,-90},{-39.6,-90}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Max.y, switch.u1) annotation (Line(
        points={{34.5,8},{40,8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Min.y, switch.u3) annotation (Line(
        points={{34.5,-8},{40,-8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(combiTable1D.y[1], compare.u2) annotation (Line(
        points={{-41.2,56},{-34,56},{-34,26},{-64,26},{-64,-8},{-34,-8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(switch.y, State)
                            annotation (Line(
        points={{63,0},{134,0}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(realToInteger.y, new_step.u) annotation (Line(
        points={{-21.2,-90},{20.4,-90}},
        color={255,127,0},
        smooth=Smooth.None));
    connect(Conso_model.y, compare.u1) annotation (Line(
        points={{-76.9,32},{-44,32},{-44,0},{-34,0}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(compare.y, switch.u2) annotation (Line(
        points={{-11,0},{40,0}},
        color={255,0,255},
        smooth=Smooth.None));
    connect(combiTable1D.y[2], control)
                                       annotation (Line(
        points={{-41.2,56},{38,56},{38,80},{132,80}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(minOfDay.y, combiTable1D.u) annotation (Line(
        points={{-76.9,56},{-59.6,56}},
        color={0,0,127},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-100,-100},{120,100}},
            preserveAspectRatio=false),defaultComponentName = "Consumption",
                        graphics), Icon(coordinateSystem(extent={{-100,-100},{120,
              100}},
            preserveAspectRatio=true),  graphics),
      experiment(Interval=120),
      Documentation(info="<html>
<p>This model makes it possible to define a drawing up according to a time profile.</p>
<p>As long as the desired quantity is not reached one continues.</p>
<p>Nevertheless, if the desired quantity is not reached before the end of the current step, the quantity is reset for the next step</p>
</html>"));
  end Drawing_up_control;

  model Water_Drawing_pressure

    replaceable package Medium =
      Modelica.Media.Interfaces.PartialMedium "Medium in the component"
        annotation (choicesAllMatching = true);
    parameter Modelica.Media.Interfaces.PartialMedium.MassFlowRate m_flow_nominal=0.1
      "Nominal mass flowrate at full opening";
    parameter Modelica.SIunits.AbsolutePressure dp_nominal=10000
      "Nominal pressure drop at full opening";
    parameter Real table[:,:]=[0,0; 5.99,0; 6,250; 8.99,250; 9,120; 9.99,120; 10,0; 10.99,0; 11,
          0; 11.99,0; 12,250; 13.49,250; 13.5,0; 14.49,0; 14.5,0; 15.49,0;
          15.5,0; 16.49,0; 16.5,0; 17.49,0; 17.5,0; 17.99,0; 18,0; 18.99,0;
          19,200; 21,200; 21.01,0; 24,0]
      "table matrix (col1 = decimal hour ; col2 = Drawing up in liter (Each value must be per pairs)"
                                                                                                  annotation(Dialog(group="Drawing up",enable = not tableOnFile));
    parameter Boolean tableOnFile=false
      "true, if table is defined on file or in function usertab" annotation (Dialog(group="Drawing up"));
    parameter String fileName="No name" "file where matrix is stored" annotation (Dialog(group="Drawing up",enable = tableOnFile,
                           __Dymola_loadSelector(filter="Text files (*.txt);;Matlab files (*.mat)",
                           caption="Open file in which table is present")));
    parameter Real k=1 "Gain" annotation (Dialog(group="Valve specifications"));
    parameter Modelica.SIunits.Time T=20 "Time Constant"
      annotation (Dialog(group="Valve specifications"));

  protected
    Modelica.Fluid.Sensors.MassFlowRate massFlowRate(redeclare package Medium =
          Medium)
      annotation (Placement(transformation(extent={{-13,-11},{13,11}},
          rotation=90,
          origin={61,-5})));
    Modelica.Blocks.Continuous.Integrator integrator  annotation (Placement(
          transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={28,24})));
    Modelica.Blocks.Continuous.FirstOrder firstOrder(
      initType=Modelica.Blocks.Types.Init.InitialState,
      y_start=0,
      k=k,
      T=T) annotation (Placement(transformation(extent={{-6,-10},{10,6}})));
    Modelica.Fluid.Valves.ValveLinear valveDiscrete(
      redeclare package Medium =
          Medium,
      allowFlowReversal=false,
      dp_nominal=dp_nominal,
      m_flow_nominal=m_flow_nominal)
      annotation (Placement(transformation(extent={{6,-40},{30,-16}})));
    Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium =
          Medium) annotation (
        Placement(transformation(rotation=0, extent={{-12,-90},{12,-68}}),
          iconTransformation(extent={{-12,-90},{12,-68}})));

    Modelica.Blocks.Math.Gain conversion(k=1.017)
      "Kilogrammes to litre (60�C)"
      annotation (Placement(transformation(extent={{-40,14},{-50,24}})));
    Buildings.Fluid.Sources.Boundary_pT boundary(
      redeclare package Medium = Medium,
      nPorts=1,
      use_T_in=true,
      p=100000) annotation (Placement(transformation(
          extent={{-7.5,-7.5},{7.5,7.5}},
          rotation=270,
          origin={60.5,28.5})));
    Modelica.Fluid.Sensors.TemperatureTwoPort Tdrawing_up(redeclare package
        Medium = Medium)
      annotation (Placement(transformation(extent={{-8,8},{8,-8}},
          rotation=90,
          origin={-2,-50})));

  // public
  public
    Drawing_up                              Consumption(table=table)
      annotation (Placement(transformation(extent={{-40,28},{-10,54}})));

  equation
    connect(valveDiscrete.port_b, massFlowRate.port_a) annotation (Line(
        points={{30,-28},{61,-28},{61,-18}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(firstOrder.y, valveDiscrete.opening) annotation (Line(
        points={{10.8,-2},{18,-2},{18,-18.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(massFlowRate.m_flow, integrator.u)  annotation (Line(
        points={{48.9,-5},{44,-5},{44,24},{37.6,24}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Consumption.State, firstOrder.u)  annotation (Line(
        points={{-9.45455,41},{-2,41},{-2,16},{-20,16},{-20,-2},{-7.6,-2}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator.y, conversion.u) annotation (Line(
        points={{19.2,24},{-10,24},{-10,19},{-39,19}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(conversion.y, Consumption.Conso) annotation (Line(
        points={{-50.5,19},{-54,19},{-54,41},{-40,41}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(massFlowRate.port_b, boundary.ports[1]) annotation (Line(
        points={{61,8},{60.5,8},{60.5,21}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(port_a, Tdrawing_up.port_a) annotation (Line(
        points={{0,-79},{-2,-79},{-2,-58}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Tdrawing_up.port_b, valveDiscrete.port_a) annotation (Line(
        points={{-2,-42},{-2,-28},{6,-28}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Tdrawing_up.T, boundary.T_in) annotation (Line(
        points={{6.8,-50},{80,-50},{80,44},{63.5,44},{63.5,37.5}},
        color={0,0,127},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-80,-80},{80,80}},
            preserveAspectRatio=true),
                        graphics),defaultComponentName = "WaterConsump", Icon(coordinateSystem(extent={{-80,-80},{80,80}},
                        preserveAspectRatio=true), graphics={
          Rectangle(
            extent={{-80,80},{80,-80}},
            lineColor={0,0,255},
            fillColor={170,255,255},
            fillPattern=FillPattern.Solid),
          Text(
            extent={{-70,42},{66,18}},
            lineColor={0,128,0},
            textString="Drawing_up"),
          Text(
            extent={{-80,-16},{80,-30}},
            lineColor={0,0,255},
            fillColor={170,255,255},
            fillPattern=FillPattern.Solid,
            textString="dp [Pa] = %dp_nominal")}),
      Documentation(info="<html>
<p>This model makes it possible to define a heat water drawing up (volume in liter at 60&deg;C) according to a time profile.</p>
<p>As long as the desired volume is not reached one continues.</p>
<p>Nevertheless, if the desired heat water quantity is not reached before the end of the current step, the quantity is reset for the next step.</p>
</html>"));
  end Water_Drawing_pressure;

  model Water_Drawing_massFlow

    replaceable package Medium =
      Modelica.Media.Interfaces.PartialMedium "Medium in the component"
        annotation (choicesAllMatching = true);
    parameter Modelica.Media.Interfaces.PartialMedium.MassFlowRate m_flow_nominal=0.1
      "Nominal mass flowrate at full opening";
    parameter Modelica.SIunits.AbsolutePressure dp_nominal=10000
      "Nominal pressure drop at full opening";
    parameter Real table[:,:]=[0,0; 5.99,0; 6,250; 8.99,250; 9,120; 9.99,120; 10,0; 10.99,0; 11,
          0; 11.99,0; 12,250; 13.49,250; 13.5,0; 14.49,0; 14.5,0; 15.49,0;
          15.5,0; 16.49,0; 16.5,0; 17.49,0; 17.5,0; 17.99,0; 18,0; 18.99,0;
          19,200; 21,200; 21.01,0; 24,0]
      "table matrix (col1 = decimal hour ; col2 = Drawing up in liter (Each value must be per pairs)"
                                                                                                  annotation(Dialog(group="Drawing up",enable = not tableOnFile));
    parameter Boolean tableOnFile=false
      "true, if table is defined on file or in function usertab" annotation (Dialog(group="Drawing up"));
    parameter String fileName="No name" "file where matrix is stored" annotation (Dialog(group="Drawing up",enable = tableOnFile,
                           __Dymola_loadSelector(filter="Text files (*.txt);;Matlab files (*.mat)",
                           caption="Open file in which table is present")));
    parameter Real k=1 "Gain" annotation (Dialog(group="Valve specifications"));
    parameter Modelica.SIunits.Time T=20 "Time Constant"
      annotation (Dialog(group="Valve specifications"));

  protected
    Modelica.Fluid.Sensors.MassFlowRate massFlowRate(redeclare package Medium =
          Medium)
      annotation (Placement(transformation(extent={{-13,-11},{13,11}},
          rotation=90,
          origin={61,-5})));
    Modelica.Blocks.Continuous.Integrator integrator  annotation (Placement(
          transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={28,24})));
    Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium =
          Medium) annotation (
        Placement(transformation(rotation=0, extent={{-12,-90},{12,-68}}),
          iconTransformation(extent={{-12,-90},{12,-68}})));

    Modelica.Blocks.Math.Gain conversion(k=1.017)
      "Kilogrammes to litre (60�C)"
      annotation (Placement(transformation(extent={{-40,14},{-50,24}})));
    Buildings.Fluid.Sources.MassFlowSource_T
                                        boundary(
      redeclare package Medium = Medium,
      nPorts=1,
      use_T_in=true,
      m_flow=-m_flow_nominal,
      use_m_flow_in=true)
                annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={61,33})));
    Modelica.Fluid.Sensors.TemperatureTwoPort Tdrawing_up(redeclare package
        Medium = Medium)
      annotation (Placement(transformation(extent={{-8,8},{8,-8}},
          rotation=90,
          origin={-2,-50})));
    Modelica.Blocks.Math.Gain conversion1(k=-m_flow_nominal)
      "Kilogrammes to litre (60�C)"
      annotation (Placement(transformation(extent={{44,48},{52,56}})));
    Modelica.Blocks.Continuous.FirstOrder firstOrder1(
      initType=Modelica.Blocks.Types.Init.InitialState,
      y_start=0,
      k=k,
      T=T) annotation (Placement(transformation(extent={{10,44},{26,60}})));
  // public
  public
    Drawing_up                              Consumption(table=table)
      annotation (Placement(transformation(extent={{-40,28},{-10,54}})));
  equation
    connect(massFlowRate.m_flow, integrator.u)  annotation (Line(
        points={{48.9,-5},{44,-5},{44,24},{37.6,24}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator.y, conversion.u) annotation (Line(
        points={{19.2,24},{-10,24},{-10,19},{-39,19}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(conversion.y, Consumption.Conso) annotation (Line(
        points={{-50.5,19},{-54,19},{-54,41},{-40,41}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(massFlowRate.port_b, boundary.ports[1]) annotation (Line(
        points={{61,8},{61,26}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(port_a, Tdrawing_up.port_a) annotation (Line(
        points={{0,-79},{-2,-79},{-2,-58}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Tdrawing_up.T, boundary.T_in) annotation (Line(
        points={{6.8,-50},{80,-50},{80,44},{63.8,44},{63.8,41.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(conversion1.y, boundary.m_flow_in) annotation (Line(
        points={{52.4,52},{66,52},{66,40},{66.6,40}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(firstOrder1.y, conversion1.u) annotation (Line(
        points={{26.8,52},{43.2,52}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Consumption.State, firstOrder1.u) annotation (Line(
        points={{-9.45455,41},{0,41},{0,52},{8.4,52}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tdrawing_up.port_b, massFlowRate.port_a) annotation (Line(
        points={{-2,-42},{-2,-36},{61,-36},{61,-18}},
        color={0,127,255},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-80,-80},{80,80}},
            preserveAspectRatio=true),
                        graphics),defaultComponentName = "WaterConsump", Icon(coordinateSystem(extent={{-80,-80},{80,80}},
                        preserveAspectRatio=true), graphics={
          Rectangle(
            extent={{-80,80},{80,-80}},
            lineColor={0,0,255},
            fillColor={170,255,255},
            fillPattern=FillPattern.Solid),
          Text(
            extent={{-70,42},{66,18}},
            lineColor={0,128,0},
            textString="Drawing_up"),
          Text(
            extent={{-80,-20},{80,-34}},
            lineColor={0,0,255},
            fillColor={170,255,255},
            fillPattern=FillPattern.Solid,
            textString="m_flow [Kg/s] = %m_flow_nominal")}),
      Documentation(info="<html>
<p>This model makes it possible to define a heat water drawing up (volume in liter at 60&deg;C) according to a time profile.</p>
<p>As long as the desired volume is not reached one continues.</p>
<p>Nevertheless, if the desired heat water quantity is not reached before the end of the current step, the quantity is reset for the next step.</p>
</html>"));
  end Water_Drawing_massFlow;

  model Water_Drawing_up_control
    "Can choose how much water as you want and how speed the flow rate must be for each schedule time"
    import SolarSystem;
    replaceable package Medium =
      Modelica.Media.Interfaces.PartialMedium "Medium in the component"
        annotation (choicesAllMatching = true);
    parameter Real table[:,:]=[0,0,0; 5.99,
          0,0; 6,250,0.05; 8.99,250,0.05; 9,120,0.05; 9.99,120,0.05; 10,0,0; 10.99,
          0,0; 11,0,0; 11.99,0,0; 12,250,0.05; 13.49,250,0.05; 13.5,0,0; 14.49,0,0;
          14.5,0,0; 15.49,0,0; 15.5,0,0; 16.49,0,0; 16.5,0,0; 17.49,0,0; 17.5,0,0;
          17.99,0,0; 18,0,0; 18.99,0,0; 19,200,0.05; 21,200,0.05; 21.01,0,0; 24,0,
          0]
      "table matrix (col1 = decimal hour ; col2 = Drawing up in liter ; col3 = Mass flow rate in Kg/s (Each value must be per pairs)"
                                                                                                  annotation(Dialog(group="Drawing up",enable = not tableOnFile));
    parameter Boolean tableOnFile=false
      "true, if table is defined on file or in function usertab" annotation (Dialog(group="Drawing up"));
    parameter String fileName="No name" "file where matrix is stored" annotation (Dialog(group="Drawing up",enable = tableOnFile,
                           __Dymola_loadSelector(filter="Text files (*.txt);;Matlab files (*.mat)",
                           caption="Open file in which table is present")));
    parameter Modelica.SIunits.Time start_value=0 "Start day of simulation"
      annotation (Dialog(group="Drawing up"));
    parameter Modelica.SIunits.Frequency f_cut=1/180 "Cut-off frequency"
      annotation (Dialog(group="Smooth"));
    parameter Real gain=1.0
      "Gain (= amplitude of frequency response at zero frequency)"
      annotation (Dialog(group="Smooth"));

    Modelica.Fluid.Sensors.MassFlowRate massFlowRate(redeclare package Medium =
          Medium)
      annotation (Placement(transformation(extent={{-13,-11},{13,11}},
          rotation=90,
          origin={61,-5})));
    Modelica.Blocks.Continuous.Integrator integrator  annotation (Placement(
          transformation(
          extent={{-8,-8},{8,8}},
          rotation=180,
          origin={28,24})));
    Modelica.Fluid.Interfaces.FluidPort_a port_a(redeclare package Medium =
          Medium) annotation (
        Placement(transformation(rotation=0, extent={{-12,-90},{12,-68}}),
          iconTransformation(extent={{-12,-90},{12,-68}})));

    Modelica.Blocks.Math.Gain conversion(k=1.017)
      "Kilogrammes to litre (60�C)"
      annotation (Placement(transformation(extent={{-40,14},{-50,24}})));
    Buildings.Fluid.Sources.MassFlowSource_T
                                        boundary(
      redeclare package Medium = Medium,
      nPorts=1,
      use_T_in=true,
      use_m_flow_in=true)
                annotation (Placement(transformation(
          extent={{-7,-7},{7,7}},
          rotation=270,
          origin={61,33})));
    Modelica.Fluid.Sensors.TemperatureTwoPort Tdrawing_up(redeclare package
        Medium = Medium)
      annotation (Placement(transformation(extent={{-8,8},{8,-8}},
          rotation=90,
          origin={-2,-50})));
    Modelica.Blocks.Math.MultiProduct smooth_flow(nu=2)
      "Kilogrammes to litre (60�C)"
      annotation (Placement(transformation(extent={{44,54},{56,66}})));
    Modelica.Blocks.Continuous.Filter     firstOrder1(
      y_start=0,
      analogFilter=Modelica.Blocks.Types.AnalogFilter.Bessel,
      filterType=Modelica.Blocks.Types.FilterType.LowPass,
      f_cut=f_cut,
      gain=gain)
           annotation (Placement(transformation(extent={{10,44},{26,60}})));
    Modelica.Blocks.Math.Gain flip(k=-1) "Change flow sign"
      annotation (Placement(transformation(extent={{-24,60},{-12,72}})));

  public
    SolarSystem.Valves.DrawingUp.Old.Drawing_up_control Consumption(table=table,
        start_value=start_value)
      annotation (Placement(transformation(extent={{-72,34},{-42,60}})));

  equation
    connect(massFlowRate.m_flow, integrator.u)  annotation (Line(
        points={{48.9,-5},{44,-5},{44,24},{37.6,24}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(integrator.y, conversion.u) annotation (Line(
        points={{19.2,24},{-10,24},{-10,19},{-39,19}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(conversion.y, Consumption.Conso) annotation (Line(
        points={{-50.5,19},{-76,19},{-76,47},{-72,47}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(massFlowRate.port_b, boundary.ports[1]) annotation (Line(
        points={{61,8},{61,26}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(port_a, Tdrawing_up.port_a) annotation (Line(
        points={{0,-79},{-2,-79},{-2,-58}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Tdrawing_up.T, boundary.T_in) annotation (Line(
        points={{6.8,-50},{80,-50},{80,54},{63.8,54},{63.8,41.4}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(smooth_flow.y, boundary.m_flow_in) annotation (Line(
        points={{57.02,60},{72,60},{72,50},{66,50},{66,40},{66.6,40}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Consumption.State, firstOrder1.u) annotation (Line(
        points={{-41.4545,47},{0,47},{0,52},{8.4,52}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Tdrawing_up.port_b, massFlowRate.port_a) annotation (Line(
        points={{-2,-42},{-2,-36},{61,-36},{61,-18}},
        color={0,127,255},
        smooth=Smooth.None));

    connect(flip.y, smooth_flow.u[1]) annotation (Line(
        points={{-11.4,66},{38,66},{38,62.1},{44,62.1}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(firstOrder1.y, smooth_flow.u[2]) annotation (Line(
        points={{26.8,52},{36,52},{36,57.9},{44,57.9}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(Consumption.control, flip.u) annotation (Line(
        points={{-41.4545,56.36},{-33.7272,56.36},{-33.7272,66},{-25.2,66}},
        color={0,0,127},
        smooth=Smooth.None));
    annotation (Diagram(coordinateSystem(extent={{-80,-80},{80,80}},
            preserveAspectRatio=false),
                        graphics),defaultComponentName = "WaterConsump", Icon(coordinateSystem(extent={{-80,-80},{80,80}},
                        preserveAspectRatio=false), graphics={
          Rectangle(
            extent={{-80,80},{80,-80}},
            lineColor={0,0,255},
            fillColor={170,255,255},
            fillPattern=FillPattern.Solid),
          Text(
            extent={{-70,42},{66,18}},
            lineColor={0,128,0},
            textString="Drawing_up
control"),Text(
            extent={{-80,-26},{80,-46}},
            lineColor={0,0,255},
            textString="controled flow
")}), Documentation(info="<html>
<p>This model makes it possible to define a heat water drawing up (volume in liter at 60&deg;C) according to a time profile.</p>
<p>As long as the desired volume is not reached one continues.</p>
<p>Nevertheless, if the desired heat water quantity is not reached before the end of the current step, the quantity is reset for the next step.</p>
</html>"));
  end Water_Drawing_up_control;

  package Examples
    extends Modelica.Icons.ExamplesPackage;

    package MediumA = Buildings.Media.Water "Medium model";
    model ECS_pressure
      extends Modelica.Icons.Example;

      Buildings.Fluid.Sources.FixedBoundary
                                     boundary2(
        redeclare package Medium =
            MediumA, nPorts=1)
        annotation (Placement(transformation(extent={{100,0},{80,20}})));
      Buildings.Fluid.Sources.MassFlowSource_T
                                     boundary1(
        redeclare package Medium =
            MediumA,
        nPorts=1,
        m_flow=2,
        use_T_in=true)
        annotation (Placement(transformation(extent={{-80,-4},{-60,16}})));
      Buildings.Fluid.MixingVolumes.MixingVolume VolPanel(
        nPorts=3,
        redeclare package Medium =
            MediumA,
        V(displayUnit="l") = 1,
        m_flow_nominal=2)
                  annotation (Placement(transformation(extent={{-50,20},{
                -14,44}})));
      Water_Drawing_pressure water_Drawing_model(m_flow_nominal=0.5, redeclare
          package Medium = MediumA)
        annotation (Placement(transformation(extent={{60,60},{100,100}})));
      Modelica.Blocks.Sources.Step step(
        height=40,
        offset=293.15,
        startTime=10000)
        annotation (Placement(transformation(extent={{-100,62},{-80,82}})));
    equation
      connect(boundary1.ports[1], VolPanel.ports[1]) annotation (Line(
          points={{-60,6},{-38,6},{-38,20},{-36.8,20}},
          color={0,127,255},
          smooth=Smooth.None));
      connect(VolPanel.ports[2], boundary2.ports[1]) annotation (Line(
          points={{-32,20},{-32,6},{80,6},{80,10}},
          color={0,127,255},
          smooth=Smooth.None));
      connect(step.y, boundary1.T_in) annotation (Line(
          points={{-79,72},{-68,72},{-68,32},{-94,32},{-94,10},{-82,10}},
          color={0,0,127},
          smooth=Smooth.None));
      connect(VolPanel.ports[3], water_Drawing_model.port_a) annotation (
          Line(
          points={{-27.2,20},{-27.2,12},{66,12},{66,52},{80,52},{80,60.25}},
          color={0,127,255},
          smooth=Smooth.None));

      annotation (Diagram(graphics), experiment(Interval=120));
    end ECS_pressure;

    model ECS_massFlow
      extends Modelica.Icons.Example;
      Buildings.Fluid.Sources.FixedBoundary
                                     boundary2(
        redeclare package Medium =
            MediumA, nPorts=1)
        annotation (Placement(transformation(extent={{100,0},{80,20}})));
      Buildings.Fluid.Sources.MassFlowSource_T
                                     boundary1(
        redeclare package Medium =
            MediumA,
        nPorts=1,
        m_flow=2,
        use_T_in=true)
        annotation (Placement(transformation(extent={{-84,-4},{-64,16}})));
      Buildings.Fluid.MixingVolumes.MixingVolume VolPanel(
        nPorts=3,
        redeclare package Medium =
            MediumA,
        V(displayUnit="l") = 1,
        m_flow_nominal=2)
                  annotation (Placement(transformation(extent={{-50,20},{
                -14,44}})));
      Water_Drawing_massFlow                              water_Drawing_model(m_flow_nominal=0.05,
          redeclare package Medium =
            MediumA)
        annotation (Placement(transformation(extent={{60,60},{100,100}})));
      Modelica.Blocks.Sources.Step step(
        height=40,
        offset=293.15,
        startTime=10000)
        annotation (Placement(transformation(extent={{-100,60},{-80,80}})));
    equation
      connect(boundary1.ports[1], VolPanel.ports[1]) annotation (Line(
          points={{-64,6},{-38,6},{-38,20},{-36.8,20}},
          color={0,127,255},
          smooth=Smooth.None));
      connect(VolPanel.ports[2], boundary2.ports[1]) annotation (Line(
          points={{-32,20},{-32,6},{80,6},{80,10}},
          color={0,127,255},
          smooth=Smooth.None));
      connect(VolPanel.ports[3], water_Drawing_model.port_a) annotation (
          Line(
          points={{-27.2,20},{-34,20},{-34,18},{-10,18},{-10,46},{80,46},{
              80,60.25}},
          color={0,127,255},
          smooth=Smooth.None));
      connect(step.y, boundary1.T_in) annotation (Line(
          points={{-79,70},{-72,70},{-72,28},{-94,28},{-94,10},{-86,10}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(graphics), experiment(StopTime=100000, Interval=
              120),
        __Dymola_experimentSetupOutput);
    end ECS_massFlow;

    model ECS_control
      extends Modelica.Icons.Example;
      Buildings.Fluid.Sources.FixedBoundary
                                     boundary2(
        redeclare package Medium =
            MediumA, nPorts=1)
        annotation (Placement(transformation(extent={{100,0},{80,20}})));
      Buildings.Fluid.Sources.MassFlowSource_T
                                     boundary1(
        redeclare package Medium =
            MediumA,
        nPorts=1,
        m_flow=2,
        use_T_in=true)
        annotation (Placement(transformation(extent={{-86,-2},{-66,18}})));
      Buildings.Fluid.MixingVolumes.MixingVolume VolPanel(
        nPorts=3,
        redeclare package Medium =
            MediumA,
        V(displayUnit="l") = 1,
        m_flow_nominal=2)
                  annotation (Placement(transformation(extent={{-50,20},{
                -14,44}})));
      Water_Drawing_up_control water_Drawing_model(
        table=[0,0,0; 5.99,0,0; 6,250,0.04; 8.99,250,0.04; 9,120,0.06; 9.99,120,
            0.06; 10,0,0; 10.99,0,0; 11,0,0; 11.99,0,0; 12,250,0.07; 13.49,250,
            0.07; 13.5,0,0; 14.49,0,0; 14.5,0,0; 15.49,0,0; 15.5,0,0; 16.49,0,0;
            16.5,0,0; 17.49,0,0; 17.5,0,0; 17.99,0,0; 18,0,0; 18.99,0,0; 19,200,
            0.05; 21,200,0.05; 21.01,0,0; 24,0,0],
        redeclare package Medium = MediumA,
        f_cut=1/120,
        gain=0.7)
        annotation (Placement(transformation(extent={{68,70},{98,100}})));
      Modelica.Blocks.Sources.Step step(
        height=40,
        offset=293.15,
        startTime=10000)
        annotation (Placement(transformation(extent={{-94,58},{-74,78}})));
    equation
      connect(boundary1.ports[1], VolPanel.ports[1]) annotation (Line(
          points={{-66,8},{-38,8},{-38,20},{-36.8,20}},
          color={0,127,255},
          smooth=Smooth.None));
      connect(VolPanel.ports[2], boundary2.ports[1]) annotation (Line(
          points={{-32,20},{-32,6},{80,6},{80,10}},
          color={0,127,255},
          smooth=Smooth.None));
      connect(VolPanel.ports[3], water_Drawing_model.port_a) annotation (
          Line(
          points={{-27.2,20},{-34,20},{-34,18},{-10,18},{-10,46},{83,46},{
              83,70.1875}},
          color={0,127,255},
          smooth=Smooth.None));
      connect(step.y, boundary1.T_in) annotation (Line(
          points={{-73,68},{-68,68},{-68,34},{-94,34},{-94,12},{-88,12}},
          color={0,0,127},
          smooth=Smooth.None));
      annotation (Diagram(graphics), experiment(StopTime=1e+006, Interval=
              120),
        __Dymola_experimentSetupOutput);
    end ECS_control;
  end Examples;
end Old;
