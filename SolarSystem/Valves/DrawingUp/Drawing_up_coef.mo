within SolarSystem.Valves.DrawingUp;
model Drawing_up_coef
  "Accept a volume of water schedule to fetch and compute the corresponding mass flow rate."
  import SI = Modelica.SIunits;
  extends Data.combitable_parameters;

  replaceable package Medium =
    Modelica.Media.Interfaces.PartialMedium "Medium in the component" annotation (choicesAllMatching = true, Dialog(group="Water specs"));

  parameter SI.Temperature Tinstruction = 313.15
    "Temperature instruction for drawing up";
  parameter SI.Density density = 1000 "Water density" annotation (Dialog(group="Water specs"));
  parameter SI.SpecificHeatCapacity Cp=4180 "Specific heat capacity for water" annotation (Dialog(group="Water specs"));

  parameter SI.Time time_step = 3600
    "Time step between two different DHW need. Must match combitable time step" annotation (Dialog(tab="Schedules"));

  parameter Real coef_schedules[:, :] = [0, 1; 2678400, 1; 5097600, 1; 7776000, 1; 10368000, 1; 13046400, 1; 15638400, 1; 18316800, 1; 20995200, 1; 23587200, 1; 26265600, 1; 28857600, 1]
    "Setpoint for the algorithm control (first=time). 1=DHW volume to draw [l])"
       annotation (Dialog(tab="Schedules",group="Coefficient schedules used to size drawn volume"));

  SI.MassFlowRate m_flow_b
    "Mass flow rate of DHW tank water part for drawing up";
  SI.MassFlowRate m_flow_p "Mass flow rate of drawing up";

    Modelica.Blocks.Sources.CombiTimeTable Schedules(
    tableOnFile=tableOnFile,
    tableName=tableName,
    fileName=fileName,
    verboseRead=verboseRead,
    columns=columns,
    smoothness=smoothness,
    extrapolation=extrapolation,
    offset=offset,
    startTime=startTime,
    table=table)         "Volume of water must be in liters"
    annotation (Placement(transformation(extent={{-80,70},{-70,80}})));
  Modelica.Blocks.Interfaces.RealInput Te "Cold water temperature [K]" annotation (
      Placement(transformation(extent={{-104,20},{-64,60}}), iconTransformation(
          extent={{-86,0},{-66,20}})));

  Modelica.Fluid.Interfaces.FluidPort_a tank_port(redeclare package Medium =
        Medium) "DHW tank fluid port for drawing up." annotation (Placement(
        transformation(rotation=0, extent={{-92,-2},{-68,20}}),
        iconTransformation(extent={{-82,36},{-70,48}})));
  Buildings.Fluid.Sources.MassFlowSource_T pump_tank(
    redeclare package Medium = Medium,
    use_m_flow_in=true,
    nPorts=1,
    use_T_in=true) "Mass flow rate to extract from tank"  annotation (Placement(
        transformation(
        extent={{-7,-7},{7,7}},
        rotation=270,
        origin={-39,67})));

  Modelica.Fluid.Sensors.TemperatureTwoPort Tb(redeclare package Medium =
        Medium) "Temperature of DHW tank." annotation (Placement(transformation(
        extent={{-8,8},{8,-8}},
        rotation=90,
        origin={-40,40})));
  Modelica.Blocks.Continuous.Integrator integrator(k=1)
                                                    annotation (Placement(
        transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-20,10})));
public
  Modelica.Blocks.Interfaces.RealOutput Energy( each unit="J")
    "Energy evolution"
    annotation (__Dymola_tag={"Energy"}, Placement(transformation(extent={{-10,20},
            {30,60}}),
        iconTransformation(extent={{-6,0},{14,20}})));
  Modelica.Blocks.Math.Gain gain(k=-1) annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=90,
        origin={-14,70})));
    Modelica.Blocks.Sources.CombiTimeTable coefs(
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    startTime=0,
    table=coef_schedules)
    "Monthly coefficients used to adjust water volume draw to inhabitants needs"
    annotation (Placement(transformation(extent={{-80,56},{-70,66}})));
equation
  // Resolve mass flow rate for each branch.
  m_flow_p = (Schedules.y[1] * coefs.y[1] * density) / (1000 * time_step);
  // `Te` must be different from `Tb` to avoid a zero division error.
  // Add a small shift to avoid zero division error.
  m_flow_b = min(m_flow_p * (Tinstruction - Te) / (Tb.T - Te + 0.00001), m_flow_p);
  gain.u = m_flow_b;
  integrator.u = m_flow_p * Cp * (Tinstruction - Te);

  connect(tank_port, Tb.port_a) annotation (Line(
      points={{-80,9},{-80,8},{-60,8},{-60,20},{-40,20},{-40,32}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tb.port_b, pump_tank.ports[1]) annotation (Line(
      points={{-40,48},{-40,60},{-39,60}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(integrator.y, Energy) annotation (Line(
      points={{-9,10},{0,10},{0,26},{-20,26},{-20,40},{10,40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tb.T, pump_tank.T_in) annotation (Line(
      points={{-31.2,40},{-24,40},{-24,80},{-36.2,80},{-36.2,75.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gain.y, pump_tank.m_flow_in) annotation (Line(points={{-14,74.4},{-14,74},{-14,80},{-33.4,80},{-33.4,
          74}}, color={0,0,127}));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-80,0},
            {0,80}})),                 Icon(coordinateSystem(extent={{-80,0},{0,
            80}},  preserveAspectRatio=false), graphics={Bitmap(
          extent={{-80,-2},{0,82}},
          imageSource="iVBORw0KGgoAAAANSUhEUgAAALIAAACyCAYAAAGRjaX+AAAAHnRFWHRTUFJEAGZpc2gwMy5zcHJlYWRzaGlydC5leHRlcm6wg8MKAAAykklEQVR42u1dZ7BdV3U+lNBhEhggEDAhQ4AhoQ38YBJKIBAIECABMgnBhkBIGFqGGQwkJq6yXFQsF1k2VpdVLKvLRe6SLFuyei+W1axe3lOzKf5zcvfx28frrLPWt9Y+976nJ0s/9nu3nrvPPuus/a32rey3v/1tTsdvfvOb4v/vfve7yvPwP4z4+tNPP115nX4ufiYLHwojvhlHeC278OJyDHxofvk4vPeJUaMr79Pvx8dZPHD8Hx8Xb5IvW4POPo6Mn1o8kzDeeNWg/K5Nm8rZzFi3vnagn8+7J9/d1VU+p9/P+HrFWfM1Lb/QOnh8n18Peqyw7tnvf//74kH4zy8gXzK65vws44WsHTyO+CYdTz31VPHh8D8enF44PuPKmocD0vHCiy7J93Z315bj5iWPlQf/9WNL883796siHCeZxSUJQ5KA8IVlTz4pSkg8cDxr+mPFsqCDewc9cHxcHjz+QNODxxGForwJm940fNz3+NbKRT558uRztz+VBrrmXA3wNS9OX/hOcbzwC+GBdPD4ISrL/G6m8k+/U8w8/KE/wA8YH3PZp8+lgxeTjb8Sf4C+GW8ePmM+6MHjd0+cOPHsweMPxMd05tLgNxh9HI91/PjxZ5cl/AqdOZ09XZp4FnzJwv+7Nm7K6SoUM6dP6FnQH+A/zCcQhYEevJh5+BMOTA9OD0j/S2dFRTUeNIxjx449e/D4A9q6S9eBng29oOE44XhHjx6tzjzOPj6mPxYeRzHkF4/OPB5cnLm0HPRs4mt0S4ufCceJS1McnEMJrjqpvuY7DRVLvotlfNePb3xv5qzyVN857Lp8f1i/1uMNe/flE1aurN2R9MCFAuQ3QZxZiiYcu3xF7eCZtO/Fg39nxszKHUihRBiP7dyVv/u662tQolgKvjbSRku1nbQJ89u+mDHf66Jyil+Kz1eQPZMDHboM5UZMIQQ9IFc8fDPga8rRQsZ3aLpjxC9S6BBGeC6JKd0ny9393GnTa7OS9LEGG+IoDxx/5bzpMxpvvOJSxBm3c+CISaghkMWp86VoOvPyYkdt9c3bp4lrPGrpMjhLFSZErXX1goXipmlBBf6d8sB8m9p1+LBrc9WwR2XGGjyQ9j4040379j23CWhKXto16A4uWQ7x/WJzRTuFtMZ89nxPLCFBnDHf6/gspZnTjTUeOO59mbRb0+d05vR06ft0t64cOH4ovEhPCS1JnMTG1gULB71u0SPlQYsDUwgQD6jt0hGAS8iI4ovywHHWXEKkC8rf4xMpgQtX2BKMpYZp/Lz0GjVI+WelnZzapui3RQTAZZ5/mHs02lWEfLz44kufVTfC7/IJV7Qyn7j0RUn9dHLwRaNzifJY8bAgxCJdQkmZ8OfctcNNNOm3qdhwESxREd/C+WNpstomQJEVvSJczfJJatCZi2UJXin+iJCDTpx/UVPNlvhQpIAGsgEqMk0RGZUhPkmkUTRIyQGGdDzusuRGCsVeJaCj7rHw4oz1G0wYxGW+6Q0nTZCKJ7WsKq5OOnEPcOQ30HtvGN4IXHLRpCiWGgkiwqUf6gTabRcp8xPg90O5I9IPnMpJf3v6zIoNwW/eAjNICJCj9FMxXnbpgNqeUXGxccxsTVrzJ3HrQFKNkubQfkdy0JQeTQl5jV+xoqK+Pj9+Qj522XJxkhSzUwBLHyMdbWkZ/puZBBPRQJ47jvokR4Wkq9FKS1C1YmJJK85dvBLW4L5NJDISvvnDgVeK42uTp9QmXfGPoolLJgUfUsxM2wE1uef+b+7MLa2L+AI3XyTDVrLetAny/5phTJ///O55FbGgVgv16ZbWYXyBu2C5ExmdALoSXOa5iM3btFnUHHRRS29r+EMnykXEculyl4NmT2v3ieTIDuOzY8fn1I4P84yjND4ly5YOydnNHTDoaqCT6A6XnEx42c6dNWd5XOXSI8/jCVrwgr8mTY461KXwAd/d+Psb9+6trHBc5WiJF5O2gDgF4NzioLhFw9vSsahlwm9MzQSruMy1sLG2MSDXAX1MXQn8+Frig2aOVfIRJHuPG5cVM0fZub4+5bb8jo0b8wvvf6CmAVbv3pP/+I4789ddcZX43RM9YmHF3TMO9dDkw3jntdd3HMVNXLW65kunK11JEJAsab6jVb7QC7AzXjUpxiEGqqStlN5U9MyXKwkInXbM8HB1adVLNwp3E3D88K1pM/KP3jKqSOLZcuCAKHdxEnu6u8X3n2odb3QL0gbw8wcXX1q7IemEK8iPOzw0FxhXT9o2zV0F/Co9JdxckoNGjcEh8C35zngMTfNXSNlMyJWmQdPaPcTjbdJkNHeXlBbB3V0aYkMbjCSipR6W3F38EkouXSmPTMtSCW56LT1MykvjJ1ALA3LHouTOlZ7zuxi5ujT/nKS+pO08zLMWwn3mmWdq6WZou+YrpMFGKXNMumL0ZuROmFIk4pnz9CeetqaFByTx0DYH6/Jzf7M0Mu2NkyxVKxzoaAvuabpy3Z49RYybx71p+lwYHPzQxdIciWp6Hc+Ee8FFl9R+ODhj0EaRkrip5ZVwxzzdE+CEpRFMc1HdOCe7pnUlpCwD6SpLJ5DxyXonTBN2UzHDX7e2deTZ1E6g0YR7Ywx+eJE6QXoiqk/5VHk3n+zqhhOuRKP6w4TDmLVhQy5tZuXNLamQUzXZkz3aRrrhSh3P7/ZTOeEw7uzxCGmbWObFAn09JAhQTFjyBVtqjaOt1MnctmZNqRpfPWAgnDCfXya5ljwTlmLXUmhBSi32ZBws3rFD9CyJHno04Y+NHAX9ZygekhJWCBPmV15Mvo7jqvkL8kM9KZ3RGTJowULoufcmTXlCCnTCFZHgKTDc1XqCpS5pqxlhKI91aBC0yQrXgjZSPo+0alruheaZ50Ysd3ZbMlyJffAcLRS00dykmoEqhcv4So9bsUKc8K/uva8mv2Kwhie0SyuiDS7XNLVL84C+4apBaoRJDIlRRzWXYS1/nb6uOam1FB2UYKItAk19N2N4WjBGeo0mNSNzXpuklipYiSrxCUsJjVx5W6Exuvpo9fgJvGXwEPG3KkmSNChDYx38MqeWmmjWt1Y5ErXFMSaeYrCRRoto1EgKwtDkTClUpjkKJTmn98ktLYtaqvzhwdBMCjDSD9GDekK6UokMd5tKx+EhBrpolRVGAUYUUNSSRTX5jicuLUBXCBzymAeLzlYKXLS0We2GpGdvFRx5HovWB5NfMSKKEoe1GxAlGKMoKL0f6EQ/POLmWpZznF+lhCjmEFshXAkoeZJFtM8UNxKZ8Oyealz6u6JI0ElLIsLDu2jS3hWOx7tv8xZRhtUJS4Va0mpyrYFS8j2T5ie7pAehxd/hi1kpWaMTlrQGv8P5aqfmE0lahKem0wB5edNZOFbziqOoD/d1aEl26HcsZ7oWLU01HrTfRUX+kudVer2S/Sht/1qmOsI09MpLIb0pq1bn350xszAPQ3n+up54jQRFtHTkwy1p+cjNt5T3Z6zTk0CgVEvAIXkI7o5fsTK/oGUz/POU22r1C9IxpGQDlKmc8WiblN+NcrPRYsfPhJr8/uBTQuPllw4oH7/isstr50DLESSBgL4BybGgpf1JxZUo6b4/Oe28VXzx8eSVqyoGrmZpo7qUWtzZqibQynmsTIDw+LVKCk1/G9TBc6C1w2q+FskdpxG8VBJU0IZgbRbaxkgn8I5h1zZK8A769z9nzc6HL15SIlFpTF69Oj/S2tXnbtiYX/bgQ/nfj5uQv+nqwc1CL0eOuMtW0LlXUj7Qh1NrZRAK0TLZJFWDSmCt7CFtk7MKl7RaUi2nV8sDroVhOdRC4Xu020obgebE8CRqpHjFJR6llGOjOxWV03nWpxLllxbYwpFWlZm2WWr1eCippFIdCQI7Wlq7lmfEUzk5Okq5s3kOai0diEo0yhTQJECrpPAkplgbbDxGyEf50dw7aov8ozlzi6iHdJElxCMZTeiio1IW6Q7l+WAZrdKjj61cEnSrIYtMuv1SGYq0EfJrPFYqynJGlGLSQqLsJZgBQhddy3mRSmtQrafXqElJ5UJcG5ogWIuOalalzECUdaUuMl1cmmjEJdyzQaENCCVhS7i0SaaAtYDWQmkSKxEdogSsLGQ5atIs6Wspf+4nd9yZf2HCrfmhY8fMHdsyy7WYZuoiIzQjCYlEUKepTmnvoqqRO4ky/iW+wOEixAvxpkGDK/pv0fbttZN825BrikBXfP4iklgu6TO+CF+6dWLHLbl/uW2quelJt7umAmgKLsohruQ98gWWVEQ7DD8hyIw2lyYS27TcwVILmpRq+tZK1q4xWXIdTEe7i6zBNzpe1MMm0hsjlGogxCQttlbyr7FSSKq1ksErSXMc4dZop8L+lZddnt+ydFkxQp50/E9HXzmBRi5bXs4l0G0GehjO0sbvZK6jJemVhFUlfNU2wFNJY9DX47zWXSttfpIQWsiipi7QYneCde10HLM3blT3KS01XUUX0m1yJi9yyBR7FUmC3d+Cp5Jxhtg7a/nSaFNqF12criMscjj311959bM+krl3QLWgZiB7vUv9gaXlVI2/uP45QqJftzZMi2WsloLuTf1rZ5FpTZEWH+tLAqMoecdbC/Cay69oFKby8ElXUtMRj1unFhkFJCPWHLd8hVip1okxZfUaFSMfYvQT1nji4EEzW7hCK2Qx8HR6kbVCB4sZ1Mr+1dIRLB6mJqWFMXPecw4lG6yWk2UVXwx48CHXpLpaksJPXCvGsBKCrRQqT4Snqb9aq6dAaV6VwhDEgoReCywFGiu/J+XQStRHkQrkyrRCZvx7TRdZylnha5l5KODRreBZPA85ksbx68nL9zrekcu1XUnWVIZak8UrHBCjslaEI1G0WRlKKASvRWK0XR4ds13vXywOQvqYZppmFgOWNwM59daXNkJP+QzP3eOVKdpGqel52lUpdWjqlCdIZ9YCelO8pStrVch41Ien7gipF02irQCwdvcgGjVeFFBW4kioQtsIrWocfgJoU0DVDSnNZLSYYApZiJZfot0RVlkKJ/XLeJGC9NxKpfdIuCQZWqkfIpvUTljjdkTpvbw0kb8/d/0GE0Zq60bLFyCJJS/dsmCd9bpFx+jBxigAK6kRdHGlC8lTfoOTyFPSwBc78v3VCvd4gYbU5cBTPuldeOsO4Lcm0o10A7WS0z2uBLrBfWD4CLX8mXNsci7QSt8EXkbkkXCue1Bxl4ZerEo0DzetZ4+QJJhfxNLXICCJEEW34BovexJLn6iIWwVmKRAPbRKoR4hVz4gQTFOj6MUgmHvhfferpbFcC8S1rCxyLIeSKG/jc6k6H6kFSz9rdfHa5mlJuhcSclUUj/HFCbea2HjnoUNidaFEGyxWziILDyEOjyRZJ4+qblNLLyWEovl6PewjL7rokiKlQCqSpWqCa4UaM69WV23RZ/PNkVZTanWsEnKxLqBns5KsR0+lffjMA1seVxf5Z3fdXUiwtsjahlep7pT0Cj8Al3LNgJEqmKXFtKRc4tRHG57029oxvK7c0lXbksgYQfnkqDGqBPNK1LIiVar35YsqwTxtQ7Rq4D2boeWdQ+oEGU8eF+tjO3aICx0W+QSbIxdIririHpdJG51UAa5JK39NUwFWqwGLV6KJntb0sHczlgrLORiQVEXUxSLxuFZ0Y4W/reRo9H2tqblVl2GlMfBUVpo8iNJ3Jee/lYVvpVWITVqkUmBUqK6Vj0n9rhBrsVaAiZw86HWLV9cqfUDnqxVHmgXslhSh4klpobRwEOLQkfq6cK7T+NjKC0ZlcVIdofQ7GnU1Kj+r5cJ5FtSiOZCwKTdbw+tfmzQl/+W8e/LRS5flC7dty4/0BFhRrwI++YHzF5SYNTDyL96x0/QVa+UXe7q6ip4fQxY+nP9gztyCEd2r7qzmPvRCZsgd6GVRpwTLnYgG/9HAK/NPjh6T//cdd+Zjli8vGguHY2g5GW8fOqy8kKGk4t4tW/KrFiwsMuzfc/0N+QsTcjm+2hIElJWfQphe61YgbRiSfuTV8FZYKfx/3WlQwP7DOc/VB27et99VSOSh0q/oZBQ1sNqOSG0ceruNRG/SMISMTlQHqO01WmPLTPNOaXpS60HK6ajj4H2O+utYs3dv/tM77xJ7SHtQDtLNGUoU0XywVg4DvZJzNmw4LRY5UDksJtYe6qGNNmYJzWRWxahVG41gTFEPbXDr9pcR6lYC8ok8G5phYsFTaT2zlHJcD0uAtPC9WdXUG2P62nVmJJsHcDmxVaV/AdehqAmNp+mHdBccTkxNpdDsHydNzi+6/4HixFGj6/A7oaNV4MwMTC8fvPGmig/YO0Kxp4V7LcmtoQtkmyO/QEqSSJhcWOhwG773hhuLhft569Yc2TJKlrSMia4TJ9RJc914TmsR6KJ8Zuy4JF6K8JknDh1q7RUbCywdLsjfjBpTwMwv3zpJVZF0U/ewbtd0Mmo9g3qYab2wpeZuVo941BqKnsD7ht9Y40tP5bOzfC8WrNXSEDTqnUzDeWgTtKQXMW5ZHEPcTOd8GNIia7nJEkryOLokiZS6/1ksirU6PknCNMvGw/gnOYK0BfCkxMbv80X++MjRudeg8qZ4eTkxrLu1Yow0YdLilp40QW/vMeSq5L8pLbLlLk3JV9bQlGcttDrtTNoxrdvFQ0iHMuM1vh+PR+v9w0fU1IXFnOUlseIZSR73Luq4V0qyVTGP2E0QiapFmuRJdeXN8rSNr91sT8/gbWSRiuGvZVatMH296UZoMVKhE6fHnrx6TeEGpYv8xqsH5dPWrBUTEj0cdzwspYWikGqTLgplGMhQHzBOUSBdLYoSkF739BhBURqL7zN4zqTva3cWqkGRgg9W2AkxbmUeujIpbufxc2iWI3K+SMdbu2evy1rbfviwaTxZ0EyCdxICk/iZTDYt1KYPhWA8vmZrw0NFjeGx1syRj6kttZGKkDxtaTVplfzJnC2yQr6nUZd5uDw9vHAezKldHC+zy9S1azvOC4eAAY9Sa5KdSUQZkS1Koy/z7tQeTIpKxeL7KYusme6WU8vyGWs0ORoKo7+daVJsUcJY+QxWhNtbdBOGV12ErniW/vdsVB7yPSS9tX6LiBuNm8gSFItpTFLzAcvh77lQTXSyhdEtZ5EnqUUDCtKFggyHniu7aveewlU4dNEic/PwNB2QFsWrLqIkWxEbyYfuWUQEcRGLbaZ9UVvkg8eOlQe4vaUD6Um+dfDQ8j1OKUmDrRqHZ3w8ZtnyskV0GN+e5mPyCgkq4YLE741dvgJyJdPncVE1Fy5vQ+0BBzWipyjRXA/H10NPEHpCgxY+LJ5oSEShz29cssTV5KVp02Uvu6HU0iKVQpKvidS5100jKSGOdnMaPFbgL3oCmZ0elz803xVOQmEmKaOUk6FK5nytRzllNOQGSid4fyzrL3Su6Y1FnrFuHaTh1agjNStPgrwSNq847ame0SS63f4dnrTb8Dg4fDq5wG/p2SdQpo+14aMu5BqmrhgjmmOo04vMSav55K595NHezatYutR031Lvmcb8qHH8owuZIX7OTrWI55mfUhCgr+jKEPGrJq0IdSGDpSbJkjcuXtmgo9s5uVtXriqZXiXG2fPvntcni3zJAw+Wc5i0ek2R/4Y8ahoZqoafXYvMsSKFLM93FsMPjbg539XVVaOVl+AmoqJX+ZM1DuW4wGfCIlN6+C0HD7pZZi2XcNkxB2HlCOfONH7OT48ZZ0aMUNObpEXuBLo4XUdofii1BkGbpst3oamOM5VxNnLha4tr+dAr/MmaDjpTF3kAoTILmZ5eo0Ry5Waetjpn4iK/+7obiqYC8fmEFgyVmgKgmpKyMF4Lx/BFPxNVxY4jR/K9oTaaGTOWmqgxgWtJJ1zCz1SdTM/9p3fd7cot4SnAmac3U1+Zvf1xfHfmLOi21TJQKzwaFtXi6dYWubdHqBjwhNLURUbkoe164VBi4po9e/pkgUKGEb3dr1n0SPIxQhmGlatcW2SNYJSTdnTC1Ykyhvqq6pRHP1YlXuBzBg8173qR0x5xxHdiETycE2H8JWk10cnxgRtHwKTBYNk1OR8vj2dm0fSWMKQDi6zx2qML7SGzRsXzVrvk8D+wCTQJzHqIqkUmcO1kOyXJks5CBH4ofcuqlKK1L+gOSu08gSRX7M7gpX5sZ5ERD6dF1iSVc1ls3xadTruLbO1htUXWuNz4iV69YGHlhz544whxAj9hfaX/6tcjzd4fqKm41VeEJqJbhEzae00XWWMKrwmoxgQufXhfV1d+wT335kt6uhPQLl9Uavd3d+e/uve+fPmuXW4aX55pj1SBRJOA6BAs9u921AXiCi3RBeJ7s1hdu1nNdCDhR4vhaTTg4af38HB4GpnHi9nuIlt7S+bhsEc0vuFHNu/fL3IiI7RgsQ2mNBbnVaupVVCdkmRTXWj0ix5Wb86HiXZ/xOjtpV9H9SVWYX1fLTKEcE0o0i2p9OJgrRs7UhmI2UBiopU22SaLrKlQicM0QzhPouT1LC5q0CJ9z1PFaiEMqycU8jd0UidLvJ6ZxDWv6Wh0BTUjQ0pb1RbaKlhEBoVWf2dVWe1pIaEAM1MWORCXWJterc+I1lIntRuDtxWRdNt6+ohoeFuCfBbWbtcn8x8zZ0HGWsq+m2kUvd52RKi/noaDEVMX0uWI8pfrc4uFJj7+cct4Ck0ZU0YoPT5y7JirKUKxyJz839v3KQXCoP4gFgKxWsahBi9W57KmvZ9QpwiJUzmTNjmJYNqLOqyNCW16KQ1YNE+eVd2Kaqs9fm9r05NIrDPE7i0xgKdgWcQt75Vay6dhlauhx6jJFvpdpCakPgBZSteylJZCVn8QD8JIobux4KDH58wXcV8LeVhBUkkweROGzMs/L/l5U9oFoe6QKTz1qCGitzOltx/KN6beDtEObyLAOzaIOJkbH8grZ+Fiq+uChDaQTpd+H/GKWuqLwj5Jyo+FxenxKnrN5yR1QZu0SF10UGsLq6OOhH2ROa6pFKSbtY3VCrHR10JDrbDIu48ccRkdWj+oirrQGrbwHk+xjho1r+UbnNW1xkIbFqGSpIYkunepKYJlpLxl8BCzGxtqjyH2fqKdXzydfj39nVKdTagFqKajeSzP06VdUxeDWbXtSUdXNthgS2vYInXO0Xo+pRosFnqRAqleM93Ttd1CRdyEjlSVCBejZltmxxxJClHbIg09SHo8xYkvNbL1cOqj9AIJv09cuQp63jRcrFl7Re8nTcS1rmaa/k7pCImiJNqt7ulyZkWxkSRbUfnvTJ8JNz9JXYit4mJDKE11RN3TxEtHn2tdzaS+fygS4+nj51EX8TjrjJQtCRejhodiFzO++VmNsTwdydD3UehL82VrbLTIyLFij/EzgVsOFn2uWFlzY0pdzMT2nby1mXSV+EJEieYbGYVw3rhhk66UKKzlaZQo4XXLhxwYx7Uu65raLRaZ90WVUAWSZMlQsWCb1CHXsgRRMNbbpFy7cOH5pFWr3NlQqKtkXM+gi0t1oaEE67a2fMgefOuVcu+GqkE/y7kTXg808NrCrti1q3y8cOtWtROypI9LdSG1BqaLnrrgqaqiKd62cuc8x0S9qqn0xschgwr1Ro3PYSNaLtlaf2mOBLieposnmer8mCndJ61m4Ei6NZXjXeQfzJ6jOoJ47++ys6QGQzzREm8DcYQ6vGjFcjh5Q2HanFC7jCJ3oufxqKVLVX8FXdyw2N3d3fUO7Fyapa6/yEHC0YZkuDTZsJA0piQ0IlU2bNEicYFfesll+UqikyUIJyGKysbnWVgJtmgSQbu1c49cO1EXTTdzLG0lmUsO9zhCHxSkNr4/a47o3tT6Vdd0stan2mOkeFoqWx17mwRovRsq/7zUFSK+puVdX/HQfLGzMV87aoTARabPkfcfJXXwC4BMYWSYxO96E66Ru9T7vZiL/ej27flxpUW0ZunFEfRxufFZ6gEtNkII3tb1KXo25X0rbI/uIinSI93VCB/HBQ6LnXk3O00laLrW8ld4MLcVU0vxxnk3VjR3vj58kakBUlnkJs1cNFYBjZM49dhaMT1lREmhhfcc22oC4DkHrdFWCmWw9/c4h3UKUbhFMJ7SYltq8C6x2KAGFJ7+MlpXpkpXIqtJIkpi91wES7CsC53Sy8srGFaXN4kQ10t/7229YvU2Q9nDKCCEymhSbngkRChpQFsDq1WY52ZW22t5OoWkXpTUfElPiDT1uxaok7ZEKeNPq9lCN5Em7IHEIfAhhAK9Jw4ezJ/q2Q35DePV4NJOhBhErW5VViKGRYpskeyjTo9W5xpLuDNPqai364mnSwsXKIsRWjt5bbcIwhGaT7/A2df+1QMG5n8+7Lr8o7eMyr8w/tb8vGnT8/+559586MOL8imrV+cPbt2abzlwoNJf1OqfHP+PX7nSxVMc5hqKVva0rB+UN2btVsdaN+G2Q4daFtSOfPb6DfnwRxcXFauhJOArEycVNRvhXEN/pxc7m8d+a/qM/ERrPlanYS80Stl1rd271n3d01sO4TGvpvbgN5Qlb9XvjVq6rF/Q57zm8isaf/fPrhmWn9sS6k+NHpO/69rrixutP5xTaNiLcmOkHcoq07bkwgNjK+SG3q3fyj3Rki+tLUTDWQhzcgPj/+5/4CwHVQdGqDk63rp+7xh2bd3LO3uOaoSlKClrd/G02apBC6vu3lOqqAkq0taWUek5fhw7Dx8+K4QdHPuOHs2fbq3r24cOEzvIpBSLpHqoEA0kv4kqzVE1te+pWvRS+1nkP3yg9jPSDRCaPZ0VwM6N+7ZsKa7XjLXrau+FPk8IXnhZHFMcBMijpmJk1L8oxahDk7WOm+pmW91HzIhnygidMsI6P7j1idp74TWvB8oLITrhU888rhRLU6MT0LwMKS2urc+HY/9bIlXF2aGPf58+I5+wYmXN0PzcuPFJ0AG59zReFyTAyOGQeaJfHk+FFljwnJQHP1sRvzDOnTb9tBeil1xyWa23fH8Ynxk7ToWfKfaP5rnwaHn0m5nGCSZBAc0V4nGVdCKM6vF0BPJ5lGBsjSBEwe31qdFjC3bU4JMOrqfQ//PuTZvzVbt3F31LrRszBD++Pvk29+8Gf/PcDRthVE1ag8DpE4Is92zeko9cuiwf8OBD+Q/nzM2/PuW2oh7u3dddX/iNvX51PoLBt3HfPjdfnBSmtmwcFHJHhfL085kmtJ7GJB48642bW5/zunAQkyEKu3NDUzNeUzg24u995OZboLB8bvyEJAs/1TvQjn9fMuo9FHKIFJFHUj3QlHforIWoLbDtFTBLcLyJO9pAPavQb1oXTMP4nkRSLfuLC/f7huMdglcSo5siZYfyphh4ciKa0jYhelTvjutxzWW8Pbdl5HmjLt7wdWofds92g1iIEM2UxxPjcSVygbQE+eMjR8PfbZKv0kRLe6+z5SJLyaXxzsHSypml8TypmB4M5GWIRplzmmbiea4eC1jLNvPCJq0xlpTM4xFklBAktRNO1XAeL5DWExrtdhbnMqLLQekG3sy8SuNZj1HmjXsj4UhJg/RuiR6N7TmWFedHgSALCngF2bM9e6GEBkk8rUDa0aII43riCFJwDPWbLb0WVs9UDUNbreho/28rzRHhHw8VspXQLuF8iwjRytrir3MubTreP3yECyOnsK5JNxfS3kgBIFiIEoOapPBaa6ylqFo2VSa1sU7N9LfghHRsb5KJBw96co+b8NBKZJSe8D3/jNfY84TtPSmk3gQdrUFrihcJkd2jIJgVAdbgrSZDGeoJjNqL8ybWnvbj3kZ/qb5mlGnnDY170lUloZG+E/KBpewxz3h/S+hDYk7T5BuP0ZpKEm3l43jdex7byqMoJc2deQRSE06+jdG70lOlgDwATYMlKJypQQwLC6cs9idHj+lIJO3LEyeZuDM1t7uJx8LjLZLWxzK4pZ2Bfp/KZZRN1OY741+QDiBpYk3jIuvdk/6H3DkpPmyPJ8KThsirIhDz+9jlyzsaFp61foO7DjL1ppcqdFLSMS3DX4KoXI48MQOkUOn/TBJWejBNoK0fRJO0LOOmHZyQtkqBFV5szd8LKY6dFOSpa9e2VZHuca957RJP4pY30IW8V5owazWKNWMvCOszzzyTWxrao7U1IZcgiMct1g7U8GgvD7yR8Dw3rkL6Y28JstddiFyOHsFtUiCs5ZJLQknlBVFIWHCWa+VME0r+2CPI2usoLGxtfSn5rV4jxyoWSLmY9P3eFGSPwaRF4bwcHynBKY5nNUFFCk6TEaR5NfdiCS24AEoCiiJ/lvfCgglSY16kOVNpByx6Tk/lizU6Di3WrE2+obzaNiWqpgmTdLNYvBkpOFhztUmfEf3I2hclQffG3lExYapLSGpYwYXbe0N5WtSi5B06n94Q5E6kvHrX2jK4rJAxMsaozHCll3LzaPNS3W8WZkZ42JoEfe3GJUsK0kt+Ec8Zck2+cvfutjKvPPkfVkdcq4kKPU5va2QtMd0TivcwPWmCIglslAOP0GkuNS5HXk+IpNgy6Yc1QG4FRuj3dnd3F+Xjbx40pLgogR73b8eMze/atOnZSbTGnw69xryY5989r/j87q6ugnT0j68eXLweCEo/PWZcfv/jj7v8pla5jIYdjxw/nn9nxsz8hQ0T03t7hHl9f9bsouO9xISkGdoaHECGmIVbEUZOcRxYMQ1JMWVIOL2GHn1+snXQ91x/g8ms869Tb3dfrHdcc63ZGOHhbdtdlrPHQR8vdqgmPp3KpB7dscPlubA8AFZE1wMjkExpLjUU4bU48zLpBzmksCZE3/vJHXeekov41sFDVQueZ/F5w9I/a+0Gp5MgX3z/Ay7eNS+ppIWDpYAXMthQGoTHxYjOK7Oid1Sgg4BTIZcE/lQVgAaGnCbhVjQCl5q1G/SXEeoUT/ZwtGnGr7UzSdfd45GytLcVd7Dy4D3GaKa9geBEFGBp0qdSkL1RJIsTOIwF27blnx03vi0et74crxowMP/ihIn5Yzt3wa0eCUNK0Ms7UgTX47NWcy0syGBhYq6Z+7sgW37qmevWN6447i8j2Azzn3gCQgIrLuDBuSl5EalRuxQ/cy1EjbYBCxv3B0GW7uSnBQ5iTZg3HzjwvCFZCTfj/qNHoeb1XFtL22oRYZS95inrSoUymaT+LXyE8PN502ecZes5jUZwiwYvU/Ai3fTY0nzLwYMwpbJp0MNKXWii3dV85BQ/n/T8rCA//4T8G1On5St271YjtJpG98JUjza2SuxquRZe4K9h5LOC/Pwer7js8nzYI4/W3GaoqqgJZEmpEIEBkSbCfKq9FmdH344QsV2wbTvEytYunpo4hMrnOirIYZzVyGfWCB6SiatWm/kUkn+6Sa2nlrSVtWuh8gmeFeQzc7yyBTuWPvlksg/aw41i9Rip9BDxZKp5BP0stHj+j9kbN+abDx7MPzpyVO29r06eotbmIW9HKsk3z1DMmnb/1AT8rCCfGWPg/AXPykdr/MOtE6sUuVcNyg8ePw6F1Uv5gLjkaEptRonhPKw6lhvmrCCfOSNwMMdrf8F991feCznmgas6JSzt5QgUBTm1d57VoOabZ1sgnFHjwzfdXAjojHXrxWjrEdJo0yLqaae7bdYOrah08LOCfHbwlg1NGKOsMjc+stRScIvu6lQJctjKfnnPvfkF995XG79qbXvoeRzPt5vwe7NmF+dKx/nz7ikgweuvvLrvjMP1G9zY19NSWuodnknVBB4uA+3gp0oYwjZmMVZaxCtPKR0/T8cRGEClfF6OU9ft2+cqOWtnfGLUaLVqBV0r7XVRkBF1KaeE0noL0/f6iyBr/MjofzyXU1Xl0qnxv63dxetpiq9fuWBhrwZNultY2WJERYXAEqyoaWSNkjVVI/cnQfbQvVoccOH5jkOH8sXbd+SLd/SM1uNHt29/7rXtO6qPdwhju/x8yY6dz46dO597vrNn9LwXvxMfl69tr762/fDhRs1vqDB/bOToXrs+oRuWd1fXmEClqvY4Mkm6kUBrGjq+dioFOZXbLWWn0ehVteY1KZRTqGtUE+Z8i9ZKM6p689qFm81SNBYelgQ4Ps80abc6GmmfPe8U+ZFDfzzEOaHdoFbXJk0DhHGitV1aa6b9VjuNh1I8TZ5G9fF5b8YAFpMKb6Q4vIMKcYmRuUbmmtk7wmQ2tYyHlwiEK1LVcwp3g/WZYYseUefkaf6OtLRH03leT2XSTyUatHqDWO0kelMja4Ls9RNr2rl0vwWtwjWNBDUk6KEJTvg/9OFF+csvHVA7oc+Pn5DvOXIkn7N+gzsH9lhrjtc98ohYCPpPkybnB7q7VW1saWUpUuShkEXaJBU+WKSNKX1VLAZRdDP2NrRIDXJYMlfDyHy75MLMhVzDLV6tHR+fb/BGBE288IknzO1Fw7TeLcuL1zw0W8git242lAaQStqYckP0pSB7CNY9EJbLQCYJq2dowDt1dB0/XgQjXnfFVeVJf3D4iHzKqtUQ0zZ1GzYdnHfNQ3uLONe091LY4q3vI0NU2gn6AlpYCUBej0XN/SZpY6R9m2ho7Tse40ojD/QIsUfIkRGIPuPpx9dON9J2j+Nlie+rqKwXI1vEkdrIrC27L4cmyEjQkLAhl1mKt8ESWIR7PfkDKYLp7bnXpEFjXwky6hqFcHBUtnTU/MgpwmwZhp6tgD/3NkZM0cQIO1sUsSl9SywsbRmdXs1qcVBbvnBrbn2tkT1UvppA85FpQivBBg5B6F3RVJt7TwxZtBYEsJpKWjeM1cTd2volgvGmpN3Sdy3vineuvSnIIRqaasgh+eNaObMET/NoaDjYo9m1qBnv02cZA+iYVhzf6wbTXHUeLesNiliGKnIZalljSIvTY23Zvz9/LTG0e3sEl+z8rVvdQi3JGBfkMDIunFYURYMVXq1uubtQUMOTcOLx2zbxJVseFA/O5szzvGeKtnukRv5SeuVNXrW6z6Owlz3wYLIW1uSvFGTkhegNYy7VatW0nBeqpHg5PAENqyOUB9db59iuR8LTDbUprNGOqRnZTUPPkkAHgT1+/LiskTUp9xpvveW5sC68ZSR6BcfrquMCYN04GnThBIvt3OgpDS6tZKZUHukUQ7zJ9UfeiijMVLgzFMVDPmbrdaTpLX9tSpACZbVZ+D0lOod8yAiLa1rLk1cg9fXWcius9sVNG2t6evN5bobUqJ0lxPx5Fl+wXGWaqkefaQI7vHAEwSHkj6a/4xFwj4bzeCC8WWgohOvpCciNZG+wxdLwWrPPlESqlN2WK8kotHSI0II7mL0+vN6CEJpQW3c4Cil7giGWoFhbuSd5id9MyLvTNLiRUjrk8fTwcejo0Xzk0mWmYtLWRhJULof8fSrMXCtnmidC+jFkQaIbwIriobze1HxVDRak5gxbc9DcaSkeGMs/7XG7eTwrqXjb+r3w+dAS7Q1XDcqPt65vSn4EytGxoK1LI0s/xjV0KhZOyS7rVCg7tdKlnYJbSRAlgW8agPEw7HQqKcq68ejnpq1ZW7rSzr19WqNrJmlmjgo0jEwFOT7OPEaepOK1H/D6pWmFBcqB9uRdeAxHDx73CAcVWGtOCGd78jes+VnQRkt0knzX3pyUrQcOFMWkFfqsh+bDSKyl7Kx8Ck2Q6cg4/ki5KziWkTC2lfIpBUmaGo4pmt2rjVIMIi1dMiU7TmteaeVFawat1r87xe8dx6FjxyrptnSMInhZynD0XE9J1hCsiI+PteaVoTsACbb0gx6okaIJPYlHTXOiPf5lK8FJ02AplTSe3BNvxl3qeyl5L6FF8DlDcHnazHXrTJ+wJ2KXoo2DEBcaGQmv9bpXcFO2GU82XYpP0jIoOhGZRFUjqUlTcY5NiwI07ew19ESM3xrvuvZ6V/iZVvR4inelocUokDBnXFWj/9qPS9oZObYlq5N/1wqopAoMP5a3UtpboYICH55EIk/SEoUGNMKoYW6PNwJlEMbXPzTipqS2aGv27Em6HhqcQFo5auISI0sCpQFqyX+naWrttXZ80p5k+SaGXbth+KZenFSc7kkqsvKwU9fmS4z72JvhtvvIEbfHCcmPJodRkMP/ikbWhJK/5vEpt4MVPWVPGnWBJ9qofR59x7IDrCw/lHbadIfpVP6LNPfoTTq/jabyfzJoSH60JTsonQG506TnVHCPHj1aPi4EGalrCytrxqAmTF4DUQrMaAvRNMKYwuPh9eF6Kr09oV0UKfSUzkvQI9WXPH75irbTNT89ZizUrpomRoiACnMcBbQIks0/YP0gF2TujvMYcylRHh75QzeMlj2FbpYU9147aaep2tZTYWwVKXhfo8fa29WVv0zgJGkyInEOXXdNtlDwQxPiqJ0zS2C92K8pRvT6f7UKbK9LL8UrgnyhXvI9ax7S+VAN2sRHbmXSWT7d+Jn/mjW7kdC+94bhtddeNWBg3t0SNimoZnnFPBo5CHEpyJb7QxLqJiFrLyTwVqCkErc0EbgUX7PHQEsx9LTIYYptkYKNw//gL36pg/Ks1kDy6sHF9/9u7LjaeyMWL8mlwJsWjJMEVhPo7u7uukZGMELyPiBfsofuqKlnAEUeJYPLWxmu3URo1/HCjyY511rmnScQlJpbEY/z6LbtjbTxVyZOKo4T6vH4ez+cMxfaOZIHTNPA2uuZ9CWvJalBEMvRjfzRKGSeIlBSPod0LG/gBNUbWlrUC1GQX9qjeb2BJCTUK3btaiTI1/Zg4cDrx9/7xd3zYBV+Sk4FldMIK1SNjIRYyxvV3vNCAq8B6MHkyAPSDlZOpXRqkr/hqaZu+tteOrSP3HxLsiBv3rev+O6FrE1ZCJBs2rsPYmPN1WZh44pG1rSxN+ihCZMVCdRIE1GIUnPHpVwkjzGJQr6WoZsSxLHcaKlkNN68X4u+4WALd7550OAkQX7bkGuKNr789bnr19dwsVQFbcELCRfXIntUiK27xgMbPBRHnuQjy9L18tBZxatNaEwtTjtNWBFhTEqiVYqx6skTl+Y+be3agta3UTPJybcVhmPKtfe4eDmkKN1vWsJyaoIHMsKs8DQyvPh82il2tQRXepyac+Gllu0t6oUU12aKh2N2S7N+Y+rtIkF70MQBjlw5f0ERmkaQL1UDW9Ai/O/q6qr7kb1CqyXea6Fjr3Fm4TrrNS0w0m7qKLrIdI0k9vt2sutSjTXLxdiUItgToELhfC2y68HMUgCEj0zCxZbxJrnhPAafx4Oh4WWUb9Ek99Wz5XqEJVXLab+f8r0UF5/m20YJ7RbFsGU3WOVJHkGWBocUlche/LLmdPZqbC/EQEJq4VxLqK0Ai/bdlCw2b0TR8jJ4fdrt5oCkFhloxqcFEaXXNRlCCWoeLRyNPfr//wHNIQgzcji9GwAAAABJRU5ErkJggg==",
          fileName="C:/Users/bois/Pictures/robinet.png")}),
    Documentation(info="<html>
<p>This model allows to develop a schedule for DHW drawing up.</p>
<p>Schedule must use a constant time step in the file table which must match with the <b>time_step</b> parameter to compute the correct mass flow rate of drawing up.</p>
<p>This model does not need a fuild port for DHW because only energy from tank is extracted. This choice allows to reduce computing time and provide a stable interface for integration.</p>
<p><br>Add a coefficient schedule which can be used to size drawing up consumption with time.</p>
<p><br>Be aware that Tb and Te must be different to avoid a zero division error.</p>
<p><i>Current implementation use a constant Thermal capacity (Cp = cst).</i></p>
</html>"));
end Drawing_up_coef;
