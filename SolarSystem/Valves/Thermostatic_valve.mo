within SolarSystem.Valves;
model Thermostatic_valve

  Modelica.Fluid.Interfaces.FluidPort_a port_1(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{-10,90},{10,110}}),
        iconTransformation(extent={{-10,90},{10,110}})));
  Modelica.Fluid.Interfaces.FluidPort_a port_2(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{-10,-110},{10,-90}}),
        iconTransformation(extent={{-10,-110},{10,-90}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_3(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}}),
        iconTransformation(extent={{-110,-10},{-90,10}})));
  replaceable package Medium =
    Modelica.Media.Interfaces.PartialMedium "Medium in the component"
      annotation (choicesAllMatching = true);
  SolarSystem.Utilities.Other.Schedule control_Sj(table=
        control) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{-100,22},{-84,38}})));
  Buildings.Controls.Continuous.LimPID pid(
    controllerType=controllerType,
    k=k,
    Ti=Ti,
    Td=Td,
    wp=wp,
    wd=wd,
    Ni=Ni,
    Nd=Nd,
    initType=initType,
    limitsAtInit=limitsAtInit,
    xi_start=xi_start,
    xd_start=xd_start,
    y_start=y_start,
    yMax=1 - yMin,
    yMin=1 - yMax,
    reverseAction=reverseAction)
    annotation (Placement(transformation(extent={{-40,20},{-20,40}})));
  Modelica.Blocks.Interfaces.RealInput TdrawingUp "Temperature of drawing up"
    annotation (Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=0,
        origin={-100,70}), iconTransformation(
        extent={{-14,-14},{14,14}},
        rotation=180,
        origin={52,0})));

  parameter Real control[:,:]=[0.0,328.15]
    "Table matrix (time = first column; temperature to get = second column)"
                                                                            annotation (Dialog(group="Control"));

  parameter Modelica.Blocks.Types.SimpleController controllerType=Modelica.Blocks.Types.SimpleController.PI
    "Type of controller" annotation (Dialog(tab="Controller"));
  parameter Real k=0.1 "Gain of controller" annotation (Dialog(tab="Controller"));
  parameter Modelica.SIunits.Time Ti=150 "Time constant of Integrator block"
    annotation (Dialog(tab="Controller"));
  parameter Modelica.SIunits.Time Td=220 "Time constant of Derivative block"
    annotation (Dialog(tab="Controller"));
  parameter Real yMax=1 "Upper limit of output"
    annotation (Dialog(tab="Controller"));
  parameter Real yMin=0 "Lower limit of output"
    annotation (Dialog(tab="Controller"));
  parameter Real wp=0.8 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Controller"));
  parameter Real wd=0.2 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Controller"));
  parameter Real Ni=0.9 "Ni*Ti is time constant of anti-windup compensation"
    annotation (Dialog(tab="Controller"));
  parameter Real Nd=10 "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Controller"));
  parameter Boolean reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Controller"));
  parameter Modelica.Blocks.Types.InitPID initType=Modelica.Blocks.Types.InitPID.DoNotUse_InitialIntegratorState
    "Type of initialization (1: no init, 2: steady state, 3: initial state, 4: initial output)"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Boolean limitsAtInit=true
    "= false, if limits are ignored during initializiation"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Real xi_start=0
    "Initial or guess value value for integrator output (= integrator state)"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Real xd_start=0
    "Initial or guess value for state of derivative block"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Real y_start=0 "Initial value of output"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal
    "Nominal mass flow rate";
  parameter Real fraK=0.7 "Fraction Kv(port_3->port_2)/Kv(port_1->port_2)";
  parameter Real l[2]={0.001,0.001} "Valve leakage, l=Kv(y=0)/Kv(y=1)";
  parameter Modelica.SIunits.Pressure dpValve_nominal=1500
    "Nominal pressure drop of fully open valve, used if CvData=Buildings.Fluid.Types.CvTypes.OpPoint";
  parameter Modelica.SIunits.Pressure dpFixed_nominal[2]={0,0}
    "Nominal pressure drop of pipes and other equipment in flow legs at port_1 and port_3";
  parameter Real deltaM=0.02
    "Fraction of nominal flow rate where linearization starts, if y=1";
  Buildings.Fluid.Actuators.Valves.ThreeWayLinear val(
    redeclare package Medium =
        Medium,
    deltaM=deltaM,
    m_flow_nominal=m_flow_nominal,
    dpValve_nominal=dpValve_nominal,
    dpFixed_nominal=dpFixed_nominal,
    fraK=fraK,
    l=l) annotation (Placement(transformation(extent={{60,-10},{40,10}})));

equation
  connect(control_Sj.y[1], pid.u_s) annotation (Line(
      points={{-83.2,30},{-42,30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TdrawingUp, pid.u_m) annotation (Line(
      points={{-100,70},{-60,70},{-60,8},{-30,8},{-30,18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(val.port_2, port_3) annotation (Line(
      points={{40,0},{-100,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(port_1, val.port_1) annotation (Line(
      points={{0,100},{0,80},{80,80},{80,0},{60,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(port_2, val.port_3) annotation (Line(
      points={{0,-100},{0,-80},{40,-80},{40,-20},{50,-20},{50,-10}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(pid.y, val.y) annotation (Line(
      points={{-19,30},{50,30},{50,12}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{80,100}}),  graphics), Icon(coordinateSystem(extent={{-100,-100},
            {80,100}},             preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{100,40},{-100,-40}},
          lineColor={0,0,0},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={192,192,192},
          origin={-2,0},
          rotation=90),
        Rectangle(
          extent={{100,23},{-100,-23}},
          lineColor={0,0,0},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={0,127,255},
          origin={-2,1},
          rotation=-90),
        Polygon(
          points={{-8,-2},{-90,60},{-90,-58},{-8,-2}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{2,-8},{-60,-90},{60,-90},{2,-8}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{39,-2},{-39,60},{-39,-60},{39,-2}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          origin={1,52},
          rotation=-90)}),
    Documentation(info="<html>
<p>Can be used to control <b>port_3</b> temperature by mixing <b>port_1</b> and <b>port_2</b> fluid port temperature together.</p>
<p>This model is a three ways valve with a PID to control valve opening. </p>
<ul>
<li>Actuator position to 1 (<b>val.y</b>) ---&GT; full openning to <b>port_1</b></li>
<li>Actuator position to 0 (<b>val.y</b>) ---&GT; full openning to <b>port_2</b></li>
</ul>
</html>"));
end Thermostatic_valve;
