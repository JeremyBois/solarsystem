within SolarSystem;
package Valves "Valves to control flow direction"
  extends Modelica.Icons.Package;


annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
          -100},{100,100}}), graphics={
        Rectangle(
          extent={{100,23},{-100,-23}},
          lineColor={0,0,0},
          fillPattern=FillPattern.HorizontalCylinder,
          fillColor={0,127,255},
          origin={8,1},
          rotation=-90),
        Polygon(
          points={{12,10},{-50,-72},{70,-72},{12,10}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid),
        Polygon(
          points={{36,-3},{-39,60},{-39,-60},{36,-3}},
          lineColor={0,0,0},
          fillColor={0,0,0},
          fillPattern=FillPattern.Solid,
          origin={15,46},
          rotation=-90),
        Polygon(
          points={{12,10},{-70,72},{-70,-48},{12,10}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid)}));
end Valves;
