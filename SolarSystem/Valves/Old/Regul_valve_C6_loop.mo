within SolarSystem.Valves.Old;
model Regul_valve_C6_loop

  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM
                                                  thermo_valve(
    redeclare package Medium = Medium,
    deltaM=deltaM,
    m_flow_nominal=m_flow_nominal,
    dp_nominal=dp_nominal)           annotation (Placement(transformation(
        extent={{24,22},{-24,-22}},
        rotation=90,
        origin={-40,0})));
  Modelica.Fluid.Interfaces.FluidPort_a port_1(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{0,88},{20,108}}),
        iconTransformation(extent={{0,88},{20,108}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_2(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{0,-108},{20,-88}}),
        iconTransformation(extent={{0,-108},{20,-88}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_3(redeclare package Medium =
        Medium)
    annotation (Placement(transformation(extent={{-110,-10},{-90,10}})));
  replaceable package Medium =
    Modelica.Media.Interfaces.PartialMedium "Medium in the component"
      annotation (choicesAllMatching = true);
  Buildings.Rooms.Examples.BESTEST.BaseClasses.DaySchedule control_Sj(table=
        control) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{38,-74},{22,-58}})));
  Buildings.Controls.Continuous.LimPID pid(
    controllerType=controllerType,
    k=k,
    Ti=Ti,
    Td=Td,
    wp=wp,
    wd=wd,
    Ni=Ni,
    Nd=Nd,
    initType=initType,
    limitsAtInit=limitsAtInit,
    xi_start=xi_start,
    xd_start=xd_start,
    y_start=y_start,
    reverseAction=reverseAction,
    yMax=yMax,
    yMin=yMin)
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=90,
        origin={16,-38})));
  Modelica.Blocks.Interfaces.RealInput Tin "Temperature before heating slab"
                                      annotation (Placement(
        transformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={70,106}), iconTransformation(
        extent={{-14,-14},{14,14}},
        rotation=180,
        origin={126,62})));

  parameter Real control[:,:]=[0.0,311.15]
    "Table matrix (time = first column; temperature to get = second column)"
                                                                            annotation (Dialog(group="Control"));

  parameter Modelica.Blocks.Types.SimpleController controllerType=Modelica.Blocks.Types.SimpleController.PI
    "Type of controller" annotation (Dialog(tab="Controller"));
  parameter Real k=0.2 "Gain of controller" annotation (Dialog(tab="Controller"));
  parameter Modelica.SIunits.Time Ti=100 "Time constant of Integrator block"
    annotation (Dialog(tab="Controller"));
  parameter Modelica.SIunits.Time Td=200 "Time constant of Derivative block"
    annotation (Dialog(tab="Controller"));
  parameter Real yMax=1 "Upper limit of output"
    annotation (Dialog(tab="Controller"));
  parameter Real yMin=0 "Lower limit of output"
    annotation (Dialog(tab="Controller"));
  parameter Real wp=0.8 "Set-point weight for Proportional block (0..1)"
    annotation (Dialog(tab="Controller"));
  parameter Real wd=0.2 "Set-point weight for Derivative block (0..1)"
    annotation (Dialog(tab="Controller"));
  parameter Real Ni=0.9 "Ni*Ti is time constant of anti-windup compensation"
    annotation (Dialog(tab="Controller"));
  parameter Real Nd=10 "The higher Nd, the more ideal the derivative block"
    annotation (Dialog(tab="Controller"));
  parameter Boolean reverseAction=false
    "Set to true for throttling the water flow rate through a cooling coil controller"
    annotation (Dialog(tab="Controller"));
  parameter Modelica.Blocks.Types.InitPID initType=Modelica.Blocks.Types.InitPID.DoNotUse_InitialIntegratorState
    "Type of initialization (1: no init, 2: steady state, 3: initial state, 4: initial output)"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Boolean limitsAtInit=true
    "= false, if limits are ignored during initializiation"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Real xi_start=0
    "Initial or guess value value for integrator output (= integrator state)"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Real xd_start=0
    "Initial or guess value for state of derivative block"
    annotation (Dialog(tab="Controller", group="Initialization"));
  parameter Real y_start=0 "Initial value of output"
    annotation (Dialog(tab="Controller", group="Initialization"));
  Modelica.Blocks.Interfaces.RealOutput Sj_loop
    "Mass flow rate for recycled water" annotation (Placement(transformation(
        extent={{20,-20},{-20,20}},
        rotation=180,
        origin={128,50}), iconTransformation(
        extent={{-14,-14},{14,14}},
        rotation=180,
        origin={-106,-60})));
  Modelica.Blocks.Interfaces.RealOutput Sj_out "Mass flow rate for heat pump"
    annotation (Placement(transformation(
        extent={{20,-20},{-20,20}},
        rotation=180,
        origin={128,90}), iconTransformation(
        extent={{-14,-14},{14,14}},
        rotation=180,
        origin={-106,60})));
  Modelica.Blocks.Math.MultiSum gain_Sj(k={1,-1}, nu=2) annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={74,56})));
  Modelica.Blocks.Math.MultiProduct gain_Sj_loop(nu=2) annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=0,
        origin={40,32})));
  Modelica.Blocks.Interfaces.RealInput Sj_flow "Sj mass flow rate" annotation (
      Placement(transformation(
        extent={{-20,-20},{20,20}},
        rotation=-90,
        origin={-30,100}), iconTransformation(
        extent={{-14,-14},{14,14}},
        rotation=180,
        origin={126,-62})));
  parameter Modelica.SIunits.Pressure dp_nominal[3] = {1000,0,1000}
    "Pressure. Set negative at outflowing ports.";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal[3] = 0.153*1.7*{1,-1,-1}
    "Mass flow rate. Set negative at outflowing ports.";
  parameter Real deltaM=0.3
    "Fraction of nominal mass flow rate where transition to turbulent occurs";
equation
  connect(control_Sj.y[1], pid.u_s) annotation (Line(
      points={{21.2,-66},{16,-66},{16,-50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tin, pid.u_m) annotation (Line(
      points={{70,106},{70,78},{60,78},{60,-4},{40,-4},{40,-38},{28,-38}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(thermo_valve.port_3, port_3) annotation (Line(
      points={{-62,0},{-100,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(port_1, thermo_valve.port_1) annotation (Line(
      points={{10,98},{10,60},{-40,60},{-40,24}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(thermo_valve.port_2, port_2) annotation (Line(
      points={{-40,-24},{-40,-80},{10,-80},{10,-98}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Sj_flow, gain_Sj_loop.u[1]) annotation (Line(
      points={{-30,100},{-30,34.1},{34,34.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Sj_flow, gain_Sj.u[1]) annotation (Line(
      points={{-30,100},{-30,58.1},{68,58.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gain_Sj_loop.y, gain_Sj.u[2]) annotation (Line(
      points={{47.02,32},{52,32},{52,53.9},{68,53.9}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gain_Sj_loop.y, Sj_loop) annotation (Line(
      points={{47.02,32},{100,32},{100,50},{128,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gain_Sj.y, Sj_out) annotation (Line(
      points={{81.02,56},{100,56},{100,90},{128,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pid.y, gain_Sj_loop.u[2]) annotation (Line(
      points={{16,-27},{16,29.9},{34,29.9}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{120,100}}), graphics), Icon(coordinateSystem(extent={{-100,
            -100},{120,100}},      preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{120,-100}},
          lineColor={0,0,255},
          fillColor={170,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-98,54},{120,40}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="Temperature control"),
                                          Text(
          extent={{-100,24},{118,10}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="%control")}));
end Regul_valve_C6_loop;
