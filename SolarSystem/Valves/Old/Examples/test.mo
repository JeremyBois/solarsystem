within SolarSystem.Valves.Old.Examples;
model test
  Buildings.Fluid.Sources.Boundary_pT   boundary(redeclare package Medium =
               Modelica.Media.Water.ConstantPropertyLiquidWater, nPorts=
       1)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={40,-88})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                        boundary2(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    use_m_flow_in=true,
    nPorts=1,
    use_T_in=false,
    T=333.15)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={18,88})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senTem(redeclare package Medium =
               Modelica.Media.Water.ConstantPropertyLiquidWater,
      m_flow_nominal=1) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={18,48})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_panels(
    useMultipleHeatPorts=true,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    thicknessIns(displayUnit="mm") = 0.1,
    lambdaIns=0.04,
    m_flow_nominal=0.2,
    nSeg=10,
    length=5)
    annotation (Placement(transformation(extent={{-10,-6},{10,6}},
        rotation=90,
        origin={-64,38})));
  Buildings.Fluid.Movers.FlowMachine_m_flow fan(redeclare package Medium =
               Modelica.Media.Water.ConstantPropertyLiquidWater,
      m_flow_nominal=0.2) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-64,6})));
  Modelica.Blocks.Sources.RealExpression close(y=1)
    annotation (Placement(transformation(extent={{90,6},{70,26}})));
  Regul_valve_C6 thermostatic_valve(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{8,-12},{30,8}})));
equation

  connect(boundary2.ports[1], senTem.port_a) annotation (Line(
      points={{18,82},{18,58}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(inFlow_panels.port_b, senTem.port_a) annotation (Line(
      points={{-64,48},{-64,58},{18,58}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(fan.port_b, inFlow_panels.port_a) annotation (Line(
      points={{-64,16},{-64,28}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(senTem.port_b, thermostatic_valve.port_1) annotation (Line(
      points={{18,38},{18,7.8},{19,7.8}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(thermostatic_valve.port_3, fan.port_a) annotation (Line(
      points={{8,-2},{-28,-2},{-28,-14},{-64,-14},{-64,-4}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(thermostatic_valve.port_2, boundary.ports[1]) annotation (
      Line(
      points={{19,-11.8},{19,-46.9},{40,-46.9},{40,-82}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(close.y, thermostatic_valve.Sj_flow) annotation (Line(
      points={{69,16},{50,16},{50,-8.2},{30.6,-8.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(senTem.T, thermostatic_valve.Tin) annotation (Line(
      points={{29,48},{36,48},{36,50},{42,50},{42,4.2},{30.6,4.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(thermostatic_valve.Sj_out, boundary2.m_flow_in) annotation (
      Line(
      points={{7.4,4},{-6,4},{-6,8},{-14,8},{-14,98},{22.8,98},{22.8,94}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(thermostatic_valve.Sj_loop, fan.m_flow_in) annotation (Line(
      points={{7.4,-8},{-22,-8},{-22,-36},{-86,-36},{-86,5.8},{-76,5.8}},
      color={0,0,127},
      smooth=Smooth.None));

  annotation (Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-100,-100},{100,100}}), graphics));
end test;
