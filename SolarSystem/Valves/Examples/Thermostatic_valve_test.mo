within SolarSystem.Valves.Examples;
model Thermostatic_valve_test
  Buildings.Fluid.Sources.Boundary_pT   boundary(redeclare package Medium =
               Modelica.Media.Water.ConstantPropertyLiquidWater,
    nPorts=1,
    T=303.15)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={16,-70})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                        boundary2(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    use_T_in=false,
    use_m_flow_in=false,
    m_flow=-1,
    T=333.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=0,
        origin={-74,-4})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senTem(redeclare package Medium =
               Modelica.Media.Water.ConstantPropertyLiquidWater,
      m_flow_nominal=1) annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={-24,-4})));
  Thermostatic_valve thermostatic_valve(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    m_flow_nominal=1,
    wd=0.2,
    Td=150,
    control=[0.0,328.15],
    reverseAction=false,
    fraK=0.5,
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    Ti=220,
    k=0.1,
    Nd=10,
    wp=0.8)
    annotation (Placement(transformation(extent={{6,-14},{28,6}})));
  Buildings.Fluid.Sources.Boundary_pT   boundary1(
                                                 redeclare package Medium =
               Modelica.Media.Water.ConstantPropertyLiquidWater,
    T=333.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={16,70})));
equation

  connect(thermostatic_valve.port_3, senTem.port_a) annotation (Line(
      points={{6,-4},{-14,-4}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(senTem.port_b, boundary2.ports[1]) annotation (Line(
      points={{-34,-4},{-68,-4}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(senTem.T, thermostatic_valve.TdrawingUp) annotation (Line(
      points={{-24,7},{-24,28},{58,28},{58,2},{28,2},{28,-4},{24.5778,-4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(boundary1.ports[1], thermostatic_valve.port_1) annotation (
      Line(
      points={{16,64},{18.2222,64},{18.2222,6}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary.ports[1], thermostatic_valve.port_2) annotation (
      Line(
      points={{16,-64},{18.2222,-64},{18.2222,-14}},
      color={0,127,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-100,-100},{100,100}}), graphics));
end Thermostatic_valve_test;
