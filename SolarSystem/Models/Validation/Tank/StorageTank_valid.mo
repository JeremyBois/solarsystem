within SolarSystem.Models.Validation.Tank;
model StorageTank_valid "Simulation of solar tank recharge and discharge"
  extends Modelica.Icons.Example;

  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.153
    "Mass flow rate for pump S6 S5 and splitter";

  Buildings.Fluid.Movers.FlowControlled_m_flow
                                            Pump(
    allowFlowReversal=false,
    m_flow_start=0.1,
    m_flow_nominal=0.1,
    motorCooledByFluid=true,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    T_start=283.15) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-60,10})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{60,80},{80,100}})));
  Buildings.Fluid.Sources.Boundary_pT   boundary(
      nPorts=1, redeclare package Medium =
        SolarSystem.Models.Validation.Tank.MediumE)
    annotation (Placement(transformation(extent={{120,-4},{140,16}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    use_m_flow_in=true,
    nPorts=1,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    T=288.15)
    annotation (Placement(transformation(extent={{242,-46},{222,-26}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    V_start(displayUnit="l")=
            0.1,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    T_start=283.15)
    annotation (Placement(transformation(extent={{-98,50},{-78,70}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tinside
    annotation (Placement(transformation(extent={{132,32},{112,52}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(computeWetBulbTemperature=false, filNam="C:/Users/bois/Documents/GitHub/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-22,96},{-34,108}})));

  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex StorageTank(
    m_flow_nominal=0.1,
    VTan(displayUnit="l") = 0.5,
    dIns(displayUnit="mm") = 0.1,
    nSeg=20,
    tau=30,
    hexSegMult=3,
    hTan=1.6,
    hHex_a=1.415,
    hHex_b=0.255,
    Q_flow_nominal(displayUnit="kW") = 103000,
    mHex_flow_nominal=0.36,
    dExtHex(displayUnit="mm") = 0.0346,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    T_start=293.15,
    redeclare package MediumHex = MediumE,
    TTan_nominal=283.15,
    THex_nominal=318.15)
    annotation (Placement(transformation(extent={{166,-20},{204,24}})));

  Utilities.Other.Schedule                                 DrawingUp(table=[0.0,
        0.05]) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{218,-6},{240,16}})));
  Buildings.Fluid.SolarCollectors.Controls.SolarPumpController PumpControl(per=
        SolarSystem.Data.Parameter.SolarCollector.IDMK2_5_buildings())
    "Pump controller" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={-22,-72})));
  Modelica.Blocks.Math.Gain gain(k=m_flow_nominal)
    "Flow rate of the system in kg/s"
    annotation (Placement(transformation(
        extent={{8,-8},{-8,8}},
        rotation=270,
        origin={-70,-52})));
  Solar_Collector.EN12975          SolarCollector(
    azi=0,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    nSeg=40,
    nPanels=6,
    per=SolarSystem.Data.Parameter.SolarCollector.IDMK2_5_buildings(),
    lat=0.79587013890941,
    til=0.5235987755983,
    use_shaCoe_in=false,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    T_start=293.15)     annotation (Placement(transformation(extent={{-46,50},{-16,82}})));

  Utilities.Other.Integrator_energy integrator_energy
    annotation (Placement(transformation(extent={{-32,0},{-12,20}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Texch_out(
    m_flow_nominal=m_flow_nominal,
    tau=10,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE)
    annotation (Placement(transformation(extent={{76,-36},{56,-16}})));
equation

  connect(boundary1.ports[1], StorageTank.port_b) annotation (Line(
      points={{222,-36},{210,-36},{210,2},{204,2}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(StorageTank.port_a, boundary.ports[1]) annotation (Line(
      points={{166,2},{148,2},{148,6},{140,6}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(DrawingUp.y[1], boundary1.m_flow_in) annotation (Line(
      points={{241.1,5},{254,5},{254,-28},{242,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Pump.port_b, SurgeTank.port_a) annotation (Line(
      points={{-60,20},{-60,38},{-88,38},{-88,50}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(gain.y,Pump. m_flow_in) annotation (Line(
      points={{-70,-43.2},{-70,-18},{-80,-18},{-80,9.8},{-72,9.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(PumpControl.y, gain.u) annotation (Line(
      points={{-33.8,-72},{-70,-72},{-70,-61.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Meteo.weaBus, PumpControl.weaBus) annotation (Line(
      points={{-34,102},{-46,102},{-46,90},{-6,90},{-6,-66},{-11.8,-66}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Meteo.weaBus, SolarCollector.weaBus) annotation (Line(
      points={{-34,102},{-52,102},{-52,81.36},{-46,81.36}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Pump.port_b, SolarCollector.port_a) annotation (Line(
      points={{-60,20},{-60,66},{-46,66}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(SolarCollector.port_b, StorageTank.portHex_a) annotation (Line(
      points={{-16,66},{100,66},{100,-14},{160,-14},{160,-6.36},{166,-6.36}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(StorageTank.heaPorVol[4], Tinside.port) annotation (Line(
      points={{185,1.142},{176,1.142},{176,42},{132,42}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Pump.m_flow_actual, integrator_energy.m_flow) annotation (Line(
      points={{-65,21},{-65,32},{-40,32},{-40,2},{-32,2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(SolarCollector.Tinside[40], integrator_energy.Tout) annotation (Line(
      points={{-14.5,57.92},{-8,57.92},{-8,24},{-36,24},{-36,18},{-32,18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(SolarCollector.Tinside[1], integrator_energy.Tin) annotation (Line(
      points={{-14.5,51.68},{-10,51.68},{-10,26},{-38,26},{-38,12},{-32,12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(StorageTank.portHex_b, Texch_out.port_a) annotation (Line(
      points={{166,-15.6},{166,-20},{84,-20},{84,-26},{76,-26}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Texch_out.port_b, Pump.port_a) annotation (Line(
      points={{56,-26},{-60,-26},{-60,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Texch_out.T, PumpControl.TIn) annotation (Line(
      points={{66,-15},{66,-8},{0,-8},{0,-76},{-10,-76}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-120},{260,120}},
          preserveAspectRatio=false),graphics), Icon(coordinateSystem(
          extent={{-100,-120},{260,120}})),
    experiment(
      StartTime=1e+007,
      StopTime=2e+007,
      Interval=120,
      Tolerance=1e-007,
      __Dymola_Algorithm="Esdirk23a"),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end StorageTank_valid;
