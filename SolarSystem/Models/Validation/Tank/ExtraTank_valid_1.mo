within SolarSystem.Models.Validation.Tank;
model ExtraTank_valid_1 "Simulation of solar tank recharge and discharge"
  extends Modelica.Icons.Example;

  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.153
    "Mass flow rate for pump S6 S5 and splitter";

  Buildings.Fluid.Movers.FlowControlled_m_flow
                                            Pump(
    allowFlowReversal=false,
    m_flow_start=0.1,
    m_flow_nominal=0.1,
    motorCooledByFluid=true,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    T_start=283.15) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-60,10})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{60,80},{80,100}})));
  Buildings.Fluid.Sources.Boundary_pT   boundary(                        nPorts=
       1, redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE)
    annotation (Placement(transformation(extent={{124,0},{140,16}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    use_m_flow_in=true,
    nPorts=1,
    use_T_in=true,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    T=288.15)
    annotation (Placement(transformation(extent={{240,-46},{222,-28}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    V_start(displayUnit="l")=
            0.1,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    T_start=283.15)
    annotation (Placement(transformation(extent={{-98,50},{-78,70}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tinside
    annotation (Placement(transformation(extent={{132,32},{112,52}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(computeWetBulbTemperature=false, filNam="C:/Users/bois/Documents/GitHub/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-22,96},{-34,108}})));

  Utilities.Other.Schedule                                 DrawingUp(table=[0.0,
        0.1]) "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{140,78},{162,100}})));
  Buildings.Fluid.SolarCollectors.Controls.SolarPumpController PumpControl(per=
        SolarSystem.Data.Parameter.SolarCollector.IDMK2_5_buildings())
    "Pump controller" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={-22,-72})));
  Modelica.Blocks.Math.Gain gain(k=m_flow_nominal)
    "Flow rate of the system in kg/s"
    annotation (Placement(transformation(
        extent={{8,-8},{-8,8}},
        rotation=270,
        origin={-70,-52})));
  Solar_Collector.EN12975          SolarCollector(
    azi=0,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    nSeg=40,
    per=SolarSystem.Data.Parameter.SolarCollector.IDMK2_5_buildings(),
    lat=0.79587013890941,
    til=0.5235987755983,
    nPanels=1,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    T_start=293.15,
    use_shaCoe_in=false)
               annotation (Placement(transformation(extent={{-46,50},{-16,82}})));

  Utilities.Other.Integrator_energy integrator_energy
    annotation (Placement(transformation(extent={{-32,0},{-12,20}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Texch_out(
    m_flow_nominal=m_flow_nominal,
    tau=10,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE)
    annotation (Placement(transformation(extent={{76,-36},{56,-16}})));
  Exchanger_and_Tank.StratifiedEnhancedInternal_TwoHex ExtraTank(
    nSeg=20,
    VTan(displayUnit="l") = 0.4,
    hTan=1.67,
    dIns(displayUnit="mm") = 0.055,
    tau(displayUnit="min") = 120,
    hHex_a={0.86,1.375},
    hHex_b={0.175,0.955},
    Q_flow_nominal={25000,53000},
    mHex_flow_nominal={0.1747,0.36},
    dExtHex(displayUnit="mm") = {0.0279,0.0337},
    m_flow_nominal=m_flow_nominal,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    redeclare package MediumHex = MediumE,
    TTan_nominal={318.15,333.15},
    THex_nominal={283.15,353.15})
    annotation (Placement(transformation(extent={{152,-26},{202,26}})));

  Buildings.Fluid.Sources.Boundary_pT boundary5(
                       nPorts=1,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    T=353.15)                                               annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={232,48})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary6(
    nPorts=1,
    m_flow=m_flow_nominal,
    use_m_flow_in=true,
    use_T_in=true,
    redeclare package Medium = SolarSystem.Models.Validation.Tank.MediumE,
    T=353.15)
    annotation (Placement(transformation(extent={{240,84},{220,104}})));
  Modelica.Blocks.Sources.RealExpression delta(each y=273.15 + 60)
    "algo need at least 2 heating pumps"
    annotation (Placement(transformation(extent={{184,56},{204,74}})));
  Modelica.Blocks.Sources.RealExpression delta1(each y=273.15 + 20)
    "algo need at least 2 heating pumps"
    annotation (Placement(transformation(extent={{220,-74},{240,-56}})));
equation

  connect(DrawingUp.y[1], boundary1.m_flow_in) annotation (Line(
      points={{163.1,89},{180,89},{180,26},{240,26},{240,-29.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Pump.port_b, SurgeTank.port_a) annotation (Line(
      points={{-60,20},{-60,38},{-88,38},{-88,50}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(gain.y,Pump. m_flow_in) annotation (Line(
      points={{-70,-43.2},{-70,-18},{-80,-18},{-80,9.8},{-72,9.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(PumpControl.y, gain.u) annotation (Line(
      points={{-33.8,-72},{-70,-72},{-70,-61.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Meteo.weaBus, PumpControl.weaBus) annotation (Line(
      points={{-34,102},{-46,102},{-46,90},{-6,90},{-6,-66},{-11.8,-66}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Meteo.weaBus, SolarCollector.weaBus) annotation (Line(
      points={{-34,102},{-52,102},{-52,81.36},{-46,81.36}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Pump.port_b, SolarCollector.port_a) annotation (Line(
      points={{-60,20},{-60,66},{-46,66}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Pump.m_flow_actual, integrator_energy.m_flow) annotation (Line(
      points={{-65,21},{-65,32},{-40,32},{-40,2},{-32,2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(SolarCollector.Tinside[40], integrator_energy.Tout) annotation (Line(
      points={{-14.5,57.92},{-8,57.92},{-8,24},{-36,24},{-36,18},{-32,18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(SolarCollector.Tinside[1], integrator_energy.Tin) annotation (Line(
      points={{-14.5,51.68},{-10,51.68},{-10,26},{-38,26},{-38,12},{-32,12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texch_out.port_b, Pump.port_a) annotation (Line(
      points={{56,-26},{-60,-26},{-60,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Texch_out.T, PumpControl.TIn) annotation (Line(
      points={{66,-15},{66,-8},{0,-8},{0,-76},{-10,-76}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(boundary1.ports[1], ExtraTank.port_b) annotation (Line(
      points={{222,-37},{214,-37},{214,0},{202,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(ExtraTank.port_a, boundary.ports[1]) annotation (Line(
      points={{152,0},{146,0},{146,8},{140,8}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(ExtraTank.portHex_b1, Texch_out.port_a) annotation (Line(
      points={{152,-20.8},{120,-20.8},{120,-26},{76,-26}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(SolarCollector.port_b, ExtraTank.portHex_a1) annotation (Line(
      points={{-16,66},{100,66},{100,-9.88},{152,-9.88}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary6.ports[1], ExtraTank.portHex_a2) annotation (Line(
      points={{220,94},{210,94},{210,13},{202,13}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(ExtraTank.portHex_b2, boundary5.ports[1]) annotation (Line(
      points={{202,6.76},{218,6.76},{218,48},{222,48}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(DrawingUp.y[1], boundary6.m_flow_in) annotation (Line(
      points={{163.1,89},{180,89},{180,112},{256,112},{256,102},{240,102}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ExtraTank.heaPorVol[5], Tinside.port) annotation (Line(
      points={{177,-0.858},{160,-0.858},{160,42},{132,42}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(delta.y, boundary6.T_in) annotation (Line(
      points={{205,65},{250,65},{250,98},{242,98}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta1.y, boundary1.T_in) annotation (Line(
      points={{241,-65},{252,-65},{252,-33.4},{241.8,-33.4}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-120},{260,120}},
          preserveAspectRatio=false),graphics), Icon(coordinateSystem(
          extent={{-100,-120},{260,120}})),
    experiment(
      StartTime=1e+007,
      StopTime=2e+007,
      Interval=120,
      Tolerance=1e-007,
      __Dymola_Algorithm="Esdirk23a"),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end ExtraTank_valid_1;
