within SolarSystem.Models.Validation.Collector;
model collector_verif_new "Add to check the collector performance"
  import SolarSystem;
  extends Modelica.Icons.Example;
  package MediumE =
      Buildings.Media.Water "Medium water model";

  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(            HSou=
        Buildings.BoundaryConditions.Types.RadiationDataSource.File, filNam=
        "D:/Github/Lib/Modelica/solarsystem/Meteo/Toulouse/Toulouse_2005.mos")
    "Weather data input file"
    annotation (Placement(transformation(extent={{-100,60},{-60,100}})));
  inner Modelica.Fluid.System system(p_ambient=101325) annotation (Placement(
        transformation(extent={{302,78},{322,98}},
                                                 rotation=0)));
public
  Modelica.Blocks.Sources.CombiTimeTable table_extraction(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    tableOnFile=true,
    columns={3,2,14,15,6,38},
    tableName="Mars",
    fileName=
        "D:/Github/Projets/SolisArt/Etudes/Verification_modele_collecteur/Dymola/Input/Mars01_Avril01.txt")
    "Tin,   Tout,   Hdir (W/m2),   mflow (Kg/s),   Text,   Energy solaire(Wh)"
    annotation (Placement(transformation(extent={{-100,-12},{-76,12}})));

public
  Modelica.Blocks.Interfaces.RealOutput T_ext
    annotation (Placement(transformation(extent={{194,44},{214,64}})));
  SolarSystem.Models.Validation.Collector.EN12975_adapt_Hdir Col_exp(
    redeclare package Medium = MediumE,
    shaCoe=0,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    azi(displayUnit="deg") = 0,
    til(displayUnit="deg") = 1.0471975511966,
    rho=0.2,
    lat(displayUnit="deg") = 0.75607663196394,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    C=1,
    use_shaCoe_in=false,
    nPanels=5,
    per=SolarSystem.Data.Parameter.SolarCollector.TisunFA_correct(),
    nSeg=20,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Parallel)
    "Concentrating solar collector model"
    annotation (Placement(transformation(extent={{30,-28},{50,-8}})));

  Buildings.Fluid.Sources.Boundary_pT sin1(
    redeclare package Medium = MediumE,
    use_p_in=false,
    p(displayUnit="Pa") = 101325,
    nPorts=1) "Inlet for fluid flow"
    annotation (Placement(transformation(extent={{144,-28},{124,-8}},
                                                                  rotation=0)));
  Buildings.Fluid.Sensors.TemperatureTwoPort Texp_out(
    redeclare package Medium = MediumE,
    T_start(displayUnit="K"),
    tau=10,
    m_flow_nominal=Col_exp.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{82,-8},{102,-28}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Texp_in(
    redeclare package Medium = MediumE,
    tau=10,
    m_flow_nominal=Col_exp.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{-6,-30},{18,-52}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                      sou1(
    redeclare package Medium = MediumE,
    T=273.15 + 10,
    use_m_flow_in=true,
    use_T_in=true,
    nPorts=1)      "Inlet for water flow"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-50,-32})));

  SolarSystem.Utilities.Other.Integrator_energy Prod_exp
    "Irradiation from olivier file. Only direct inside panel" annotation (
      Placement(transformation(
        extent={{-41,-12},{41,12}},
        rotation=0,
        origin={205,-66})));
public
  Modelica.Blocks.Interfaces.RealOutput HDir "Hdir (W/m2) on collector"
    annotation (Placement(transformation(extent={{194,12},{214,32}})));
public
  Buildings.Fluid.Sources.Boundary_pT sin3(
    redeclare package Medium = MediumE,
    use_p_in=false,
    p(displayUnit="Pa") = 101325,
    nPorts=1) "Inlet for fluid flow"
    annotation (Placement(transformation(extent={{100,-260},{80,-240}},
                                                                  rotation=0)));
  Buildings.Fluid.Sensors.TemperatureTwoPort Tmodel_out(
    redeclare package Medium = MediumE,
    T_start(displayUnit="K"),
    tau=10,
    m_flow_nominal=Col_model.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{46,-260},{66,-240}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Tmodel_in(
    redeclare package Medium = MediumE,
    tau=10,
    m_flow_nominal=Col_model.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{-26,-232},{-6,-212}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                      sou3(
    redeclare package Medium = MediumE,
    T=273.15 + 10,
    nPorts=1,
    use_m_flow_in=true,
    use_T_in=true) "Inlet for water flow"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-46,-250})));
  SolarSystem.Models.Validation.Collector.EN12975_adapt_Text Col_model(
    redeclare package Medium = MediumE,
    shaCoe=0,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    rho=0.2,
    azi(displayUnit="deg") = 0,
    til(displayUnit="deg") = 1.0471975511966,
    lat(displayUnit="deg") = 0.75607663196394,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    use_shaCoe_in=false,
    nPanels=5,
    per=SolarSystem.Data.Parameter.SolarCollector.TisunFA_correct(),
    nSeg=20,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Parallel)
    "Concentrating solar collector model"
    annotation (Placement(transformation(extent={{14,-260},{34,-240}})));

  SolarSystem.Utilities.Other.Integrator_energy Prod_model
    "Irradiation from weather data file. Split between direct and diffuse"
    annotation (Placement(transformation(
        extent={{-43,12},{43,-12}},
        rotation=0,
        origin={207,-206})));

public
  Modelica.Blocks.Interfaces.RealOutput T_out
    annotation (Placement(transformation(extent={{194,78},{214,98}})));
public
  Modelica.Blocks.Interfaces.RealOutput T_in
    annotation (Placement(transformation(extent={{196,112},{216,132}})));
equation
  connect(Texp_out.port_b, sin1.ports[1]) annotation (Line(
      points={{102,-18},{124,-18}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(weaDat.weaBus, Col_exp.weaBus) annotation (Line(
      points={{-60,80},{30,80},{30,-8.4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(table_extraction.y[1], sou1.T_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-36},{-62,-36}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], sou1.m_flow_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-40},{-60,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[3], Col_exp.Hdir_data) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-11.2},{28,-11.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], Prod_exp.m_flow) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-76},{158.26,-76},{158.26,-74.88}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texp_in.T, Prod_exp.Tin) annotation (Line(
      points={{6,-53.1},{6,-66},{158,-66},{158,-65.76},{158.26,-65.76}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Texp_out.T, Prod_exp.Tout) annotation (Line(
      points={{92,-29},{92,-56},{158.26,-56},{158.26,-57.12}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(table_extraction.y[5], Col_exp.Text) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-20},{-20,-20},{-20,-20.4},{28,-20.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sou3.ports[1], Tmodel_in.port_a) annotation (Line(
      points={{-36,-250},{-30,-250},{-30,-222},{-26,-222}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tmodel_out.port_b, sin3.ports[1]) annotation (Line(
      points={{66,-250},{80,-250}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Col_model.port_b, Tmodel_out.port_a) annotation (Line(
      points={{34,-250},{46,-250}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tmodel_in.port_b, Col_model.port_a) annotation (Line(
      points={{-6,-222},{6,-222},{6,-250},{14,-250}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(table_extraction.y[1], sou3.T_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-254},{-58,-254}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], sou3.m_flow_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-258},{-56,-258}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tmodel_in.T, Prod_model.Tin) annotation (Line(
      points={{-16,-211},{-16,-206},{157.98,-206},{157.98,-206.24}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tmodel_out.T, Prod_model.Tout) annotation (Line(
      points={{56,-239},{56,-216},{157.98,-216},{157.98,-214.88}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], Prod_model.m_flow) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-198},{157.98,-198},{157.98,-197.12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[5], Col_model.Text) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-280},{6,-280},{6,-252.4},{12,-252.4}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(weaDat.weaBus, Col_model.weaBus) annotation (Line(
      points={{-60,80},{-120,80},{-120,-240.4},{14,-240.4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));

  connect(Texp_in.port_b, Col_exp.port_a) annotation (Line(points={{18,-41},{28,
          -41},{28,-18},{30,-18}}, color={0,127,255}));
  connect(sou1.ports[1], Texp_in.port_a) annotation (Line(points={{-40,-32},{
          -24,-32},{-24,-41},{-6,-41}}, color={0,127,255}));
  connect(Col_exp.port_b, Texp_out.port_a)
    annotation (Line(points={{50,-18},{72,-18},{82,-18}}, color={0,127,255}));
  connect(table_extraction.y[1], T_in) annotation (Line(points={{-74.8,0},{64,0},
          {64,122},{206,122}}, color={0,0,127}));
  connect(table_extraction.y[2], T_out) annotation (Line(points={{-74.8,0},{
          61.6,0},{61.6,88},{204,88}}, color={0,0,127}));
  connect(table_extraction.y[3], HDir) annotation (Line(points={{-74.8,0},{62,0},
          {62,22},{204,22}}, color={0,0,127}));
  connect(table_extraction.y[5], T_ext) annotation (Line(points={{-74.8,0},{60,
          0},{60,54},{204,54}}, color={0,0,127}));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,
            -300},{320,140}}), graphics={
        Text(
          extent={{166,-18},{260,-56}},
          lineColor={0,128,0},
          fontSize=11,
          textString="Olivier data"),
        Text(
          extent={{170,-220},{264,-258}},
          lineColor={0,128,0},
          fontSize=11,
          textString="Weather data")}),   Documentation(info="<html>
<p>Current tests</p>
<ul>
<li>Direct from data and no diffuse irradiation (bottom with 1)</li>
</ul>
<p>If test pass try with new collector to pass validation.</p>
</html>"),
    Icon(coordinateSystem(extent={{-120,-300},{320,140}})),
    experiment(
      StartTime=1.0368e+007,
      StopTime=1.34107e+007,
      Interval=360),
    __Dymola_experimentSetupOutput);
end collector_verif_new;
