within SolarSystem.Models.Validation.Collector;
model collector_verif "Add to check the collector performance"
  import SolarSystem;
  extends Modelica.Icons.Example;
  package MediumE =
      Buildings.Media.Water "Medium water model";

  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(filNam=
        "D:/Github/solarsystem/Meteo/Toulouse/FR-Toulouse-76300TM2.mos", HSou=
        Buildings.BoundaryConditions.Types.RadiationDataSource.File)
    "Weather data input file"
    annotation (Placement(transformation(extent={{-80,60},{-40,100}})));
  inner Modelica.Fluid.System system(p_ambient=101325) annotation (Placement(
        transformation(extent={{80,80},{100,100}},
                                                 rotation=0)));
protected
  Modelica.Blocks.Sources.CombiTimeTable table_extraction(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    tableOnFile=true,
    columns={3,2,14,15,6,38},
    tableName="Mars",
    fileName=
        "D:/Github/Projets/SolisArt/Etudes/Verification_modele_collecteur/Dymola/Input/Mars01_Avril01.txt")
    "Tin,   Tout,   Hdir (W/m2),   mflow (Kg/s),   Text,   Energy solaire(Wh)"
    annotation (Placement(transformation(extent={{-100,-12},{-76,12}})));

public
  Modelica.Blocks.Continuous.Integrator T_ext
    annotation (Placement(transformation(extent={{40,32},{60,52}})));
  SolarSystem.Models.Validation.Collector.EN12975_adapt_Hdir Col_H_on_File(
    redeclare package Medium = MediumE,
    shaCoe=0,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    nSeg=10,
    azi(displayUnit="deg") = 0,
    til(displayUnit="deg") = 1.0471975511966,
    rho=0.2,
    lat(displayUnit="deg") = 0.75607663196394,
    nPanels=10,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    per=SolarSystem.Data.Parameter.SolarCollector.TisunFA_mini(),
    C=1,
    use_shaCoe_in=false) "Concentrating solar collector model"
    annotation (Placement(transformation(extent={{14,-42},{34,-22}})));

  Buildings.Fluid.Sources.Boundary_pT sin1(
    redeclare package Medium = MediumE,
    use_p_in=false,
    p(displayUnit="Pa") = 101325,
    nPorts=1) "Inlet for fluid flow"
    annotation (Placement(transformation(extent={{102,-42},{82,-22}},
                                                                  rotation=0)));
  Buildings.Fluid.Sensors.TemperatureTwoPort TOut1(
    redeclare package Medium = MediumE,
    T_start(displayUnit="K"),
    tau=10,
    m_flow_nominal=Col_H_on_File.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{48,-42},{68,-22}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TIn1(
                                                 redeclare package Medium =
    MediumE,
    tau=10,
    m_flow_nominal=Col_H_on_File.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{-22,-42},{-2,-22}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                      sou1(
    redeclare package Medium = MediumE,
    T=273.15 + 10,
    nPorts=1,
    use_m_flow_in=true,
    use_T_in=true) "Inlet for water flow"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-44,-32})));
  Buildings.Fluid.Sources.Boundary_pT sin2(
    redeclare package Medium = MediumE,
    use_p_in=false,
    p(displayUnit="Pa") = 101325,
    nPorts=1) "Inlet for fluid flow"
    annotation (Placement(transformation(extent={{102,-158},{82,-138}},
                                                                  rotation=0)));
  Buildings.Fluid.Sensors.TemperatureTwoPort TOut2(
    redeclare package Medium = MediumE,
    T_start(displayUnit="K"),
    tau=10,
    m_flow_nominal=Col_H_on_File_BigPanel.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{48,-158},{68,-138}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TIn2(
                                                 redeclare package Medium =
    MediumE,
    tau=10,
    m_flow_nominal=Col_H_on_File_BigPanel.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{-22,-158},{-2,-138}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                      sou2(
    redeclare package Medium = MediumE,
    T=273.15 + 10,
    nPorts=1,
    use_m_flow_in=true,
    use_T_in=true) "Inlet for water flow"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-44,-148})));
  SolarSystem.Models.Validation.Collector.EN12975_adapt_Hdir Col_H_on_File_BigPanel(
    redeclare package Medium = MediumE,
    shaCoe=0,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    rho=0.2,
    totalArea=13.82,
    nSeg=10,
    azi(displayUnit="deg") = 0,
    til(displayUnit="deg") = 1.0471975511966,
    lat(displayUnit="deg") = 0.75607663196394,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    nPanels=1,
    per=SolarSystem.Data.Parameter.SolarCollector.TisunFA(),
    use_shaCoe_in=false) "Concentrating solar collector model"
    annotation (Placement(transformation(extent={{16,-158},{36,-138}})));

  SolarSystem.Utilities.Other.Integrator_energy production_H_on_File
    "Irradiation from olivier file. Only direct inside panel" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={64,-76})));
  SolarSystem.Utilities.Other.Integrator_energy production_H_on_File_BigPanel
    "Irradiation from olivier file. Only direct inside panel" annotation (Placement(
        transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={30,-102})));
public
  Modelica.Blocks.Continuous.Integrator HDir "Hdir (W/m2) on collector"
    annotation (Placement(transformation(extent={{40,0},{60,20}})));
public
  Buildings.Fluid.Sources.Boundary_pT sin3(
    redeclare package Medium = MediumE,
    use_p_in=false,
    p(displayUnit="Pa") = 101325,
    nPorts=1) "Inlet for fluid flow"
    annotation (Placement(transformation(extent={{100,-260},{80,-240}},
                                                                  rotation=0)));
  Buildings.Fluid.Sensors.TemperatureTwoPort TOut3(
    redeclare package Medium = MediumE,
    T_start(displayUnit="K"),
    tau=10,
    m_flow_nominal=Col_H_on_weather.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{46,-260},{66,-240}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TIn3(
                                                 redeclare package Medium =
    MediumE,
    tau=10,
    m_flow_nominal=Col_H_on_weather.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{-24,-260},{-4,-240}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                      sou3(
    redeclare package Medium = MediumE,
    T=273.15 + 10,
    nPorts=1,
    use_m_flow_in=true,
    use_T_in=true) "Inlet for water flow"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-46,-250})));
  SolarSystem.Models.Validation.Collector.EN12975_adapt_Text Col_H_on_weather(
    redeclare package Medium = MediumE,
    shaCoe=0,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    rho=0.2,
    nSeg=10,
    azi(displayUnit="deg") = 0,
    til(displayUnit="deg") = 1.0471975511966,
    lat(displayUnit="deg") = 0.75607663196394,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    nPanels=10,
    per=SolarSystem.Data.Parameter.SolarCollector.TisunFA_mini(),
    use_shaCoe_in=false) "Concentrating solar collector model"
    annotation (Placement(transformation(extent={{14,-260},{34,-240}})));

  SolarSystem.Utilities.Other.Integrator_energy production_H_on_weather
    "Irradiation from weather data file. Split between direct and diffuse" annotation (
      Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={20,-212})));
public
  Buildings.Fluid.Sources.Boundary_pT sin4(
    redeclare package Medium = MediumE,
    use_p_in=false,
    p(displayUnit="Pa") = 101325,
    nPorts=1) "Inlet for fluid flow"
    annotation (Placement(transformation(extent={{98,-348},{78,-328}},
                                                                  rotation=0)));
  Buildings.Fluid.Sensors.TemperatureTwoPort TOut4(
    redeclare package Medium = MediumE,
    T_start(displayUnit="K"),
    tau=10,
    m_flow_nominal=Col_H_on_weatherDirect.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{44,-348},{64,-328}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TIn4(
                                                 redeclare package Medium =
    MediumE,
    tau=10,
    m_flow_nominal=Col_H_on_weatherDirect.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{-26,-348},{-6,-328}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                      sou4(
    redeclare package Medium = MediumE,
    T=273.15 + 10,
    nPorts=1,
    use_m_flow_in=true,
    use_T_in=true) "Inlet for water flow"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-48,-338})));
  SolarSystem.Models.Validation.Collector.EN12975_adapt_HDirDif Col_H_on_weatherDirect(
    redeclare package Medium = MediumE,
    shaCoe=0,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    rho=0.2,
    nSeg=10,
    azi(displayUnit="deg") = 0,
    til(displayUnit="deg") = 1.0471975511966,
    lat(displayUnit="deg") = 0.75607663196394,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    nPanels=10,
    per=SolarSystem.Data.Parameter.SolarCollector.TisunFA_mini(),
    use_shaCoe_in=false) "Concentrating solar collector model"
    annotation (Placement(transformation(extent={{12,-348},{32,-328}})));

  SolarSystem.Utilities.Other.Integrator_energy production_H_on_weatherDirect
    "Irradiation from weather data file. Only direct inside panel" annotation (Placement(
        transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={20,-304})));
public
  Buildings.Fluid.Sources.Boundary_pT sin5(
    redeclare package Medium = MediumE,
    use_p_in=false,
    p(displayUnit="Pa") = 101325,
    nPorts=1) "Inlet for fluid flow"
    annotation (Placement(transformation(extent={{98,-422},{78,-402}},
                                                                  rotation=0)));
  Buildings.Fluid.Sensors.TemperatureTwoPort TOut5(
    redeclare package Medium = MediumE,
    T_start(displayUnit="K"),
    tau=10,
    m_flow_nominal=Col_H_on_weatherDirect.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{44,-422},{64,-402}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TIn5(
                                                 redeclare package Medium =
    MediumE,
    tau=10,
    m_flow_nominal=Col_H_on_weatherDirect.m_flow_nominal) "Temperature sensor"
    annotation (Placement(transformation(extent={{-26,-422},{-6,-402}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                      sou5(
    redeclare package Medium = MediumE,
    T=273.15 + 10,
    nPorts=1,
    use_m_flow_in=true,
    use_T_in=true) "Inlet for water flow"
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-48,-412})));
  SolarSystem.Models.Validation.Collector.EN12975_adapt_Hdir Col_H_on_olivier_angular(
    redeclare package Medium = MediumE,
    shaCoe=0,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    rho=0.2,
    nSeg=10,
    azi(displayUnit="deg") = 0,
    til(displayUnit="deg") = 1.0471975511966,
    lat(displayUnit="deg") = 0.75607663196394,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    nPanels=10,
    per=SolarSystem.Data.Parameter.SolarCollector.TisunFA_mini_test(),
    use_shaCoe_in=false) "Concentrating solar collector model"
    annotation (Placement(transformation(extent={{12,-422},{32,-402}})));
public
  Buildings.Fluid.Sources.Boundary_pT sin6(
    use_p_in=false,
    p(displayUnit="Pa") = 101325,
    nPorts=1,
    redeclare package Medium = SolarSystem.Media.WaterGlycol)
    "Inlet for fluid flow"
    annotation (Placement(transformation(extent={{100,-512},{80,-492}}, rotation=0)));
  Buildings.Fluid.Sensors.TemperatureTwoPort TOut6(
    T_start(displayUnit="K"),
    tau=10,
    m_flow_nominal=Col_H_on_weatherDirect.m_flow_nominal,
    redeclare package Medium = SolarSystem.Media.WaterGlycol)
    "Temperature sensor" annotation (Placement(transformation(extent={{48,-512},
            {68,-492}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TIn6(
    tau=10,
    m_flow_nominal=Col_H_on_weatherDirect.m_flow_nominal,
    redeclare package Medium = SolarSystem.Media.WaterGlycol)
    "Temperature sensor"
    annotation (Placement(transformation(extent={{-26,-512},{-6,-492}})));
  Buildings.Fluid.Sources.MassFlowSource_T sou6(
    T=273.15 + 10,
    nPorts=1,
    use_m_flow_in=true,
    use_T_in=true,
    redeclare package Medium = SolarSystem.Media.WaterGlycol)
    "Inlet for water flow" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-46,-502})));
  SolarSystem.Models.Validation.Collector.EN12975_adapt_Text Col_H_on_weather_media(
    shaCoe=0,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    rho=0.2,
    nSeg=10,
    azi(displayUnit="deg") = 0,
    til(displayUnit="deg") = 1.0471975511966,
    lat(displayUnit="deg") = 0.75607663196394,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    nPanels=10,
    per=SolarSystem.Data.Parameter.SolarCollector.TisunFA_mini(),
    use_shaCoe_in=false,
    redeclare package Medium = SolarSystem.Media.WaterGlycol)
    "Concentrating solar collector model"
    annotation (Placement(transformation(extent={{14,-512},{34,-492}})));
  SolarSystem.Utilities.Other.Integrator_energy production_H_on_olivier_angular
    "Irradiation from weather data file. Only direct inside panel" annotation (Placement(
        transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={24,-380})));
  SolarSystem.Utilities.Other.Integrator_energy production_H_on_weather_media(Cp_value=
       3608) "Irradiation from weather data file. Only direct inside panel"
                                                                   annotation (Placement(
        transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={20,-466})));
public
  Buildings.Fluid.Sources.Boundary_pT sin7(
    use_p_in=false,
    p(displayUnit="Pa") = 101325,
    nPorts=1,
    redeclare package Medium = SolarSystem.Media.WaterGlycol)
    "Inlet for fluid flow"
    annotation (Placement(transformation(extent={{100,-604},{80,-584}}, rotation=0)));
  Buildings.Fluid.Sensors.TemperatureTwoPort TOut7(
    T_start(displayUnit="K"),
    tau=10,
    m_flow_nominal=Col_H_on_weatherDirect.m_flow_nominal,
    redeclare package Medium = SolarSystem.Media.WaterGlycol)
    "Temperature sensor" annotation (Placement(transformation(extent={{46,-604},
            {66,-584}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TIn7(
    tau=10,
    m_flow_nominal=Col_H_on_weatherDirect.m_flow_nominal,
    redeclare package Medium = SolarSystem.Media.WaterGlycol)
    "Temperature sensor"
    annotation (Placement(transformation(extent={{-24,-604},{-4,-584}})));
  Buildings.Fluid.Sources.MassFlowSource_T sou7(
    T=273.15 + 10,
    nPorts=1,
    use_m_flow_in=true,
    use_T_in=true,
    redeclare package Medium = SolarSystem.Media.WaterGlycol)
    "Inlet for water flow" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={-46,-594})));
  SolarSystem.Models.Validation.Collector.EN12975_adapt_Hdir Col_H_on_file_media(
    shaCoe=0,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    rho=0.2,
    nSeg=10,
    azi(displayUnit="deg") = 0,
    til(displayUnit="deg") = 1.0471975511966,
    lat(displayUnit="deg") = 0.75607663196394,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    nPanels=10,
    per=SolarSystem.Data.Parameter.SolarCollector.TisunFA_mini(),
    use_shaCoe_in=false,
    redeclare package Medium = SolarSystem.Media.WaterGlycol)
    "Concentrating solar collector model"
    annotation (Placement(transformation(extent={{14,-604},{34,-584}})));

  SolarSystem.Utilities.Other.Integrator_energy production_H_on_file_media(Cp_value=
       3608) "Irradiation from weather data file. Only direct inside panel"
                                                                   annotation (
      Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=90,
        origin={24,-558})));
equation
  connect(table_extraction.y[2],T_ext. u) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,42},{38,42}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Col_H_on_File.port_b, TOut1.port_a) annotation (Line(
      points={{34,-32},{48,-32}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TOut1.port_b, sin1.ports[1]) annotation (Line(
      points={{68,-32},{82,-32}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TIn1.port_b, Col_H_on_File.port_a) annotation (Line(
      points={{-2,-32},{14,-32}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(sou1.ports[1], TIn1.port_a) annotation (Line(
      points={{-34,-32},{-22,-32}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(weaDat.weaBus, Col_H_on_File.weaBus) annotation (Line(
      points={{-40,80},{2,80},{2,-22.4},{14,-22.4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(table_extraction.y[1], sou1.T_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-36},{-56,-36}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], sou1.m_flow_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-40},{-54,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[3], Col_H_on_File.Hdir_data) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,20},{8,20},{8,-25.2},{12,-25.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sou2.ports[1], TIn2.port_a) annotation (Line(
      points={{-34,-148},{-22,-148}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TOut2.port_b, sin2.ports[1]) annotation (Line(
      points={{68,-148},{82,-148}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(table_extraction.y[1], sou2.T_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-152},{-56,-152}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], sou2.m_flow_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-156},{-54,-156}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Col_H_on_File_BigPanel.port_b, TOut2.port_a) annotation (Line(
      points={{36,-148},{48,-148}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TIn2.port_b, Col_H_on_File_BigPanel.port_a) annotation (Line(
      points={{-2,-148},{16,-148}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(table_extraction.y[3], Col_H_on_File_BigPanel.Hdir_data) annotation (
      Line(
      points={{-74.8,0},{-66,0},{-66,20},{8,20},{8,-141.2},{14,-141.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[5], Col_H_on_File_BigPanel.Text) annotation (Line(
      points={{-74.8,0},{-20,0},{-20,-126},{4,-126},{4,-150.4},{14,-150.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(weaDat.weaBus, Col_H_on_File_BigPanel.weaBus) annotation (Line(
      points={{-40,80},{2,80},{2,-138.4},{16,-138.4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(table_extraction.y[4], production_H_on_File.m_flow) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-52},{56.6,-52},{56.6,-64.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], production_H_on_File_BigPanel.m_flow)
    annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-122},{22.6,-122},{22.6,-113.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TIn1.T, production_H_on_File.Tin) annotation (Line(
      points={{-12,-21},{-12,-10},{-26,-10},{-26,-50},{64.2,-50},{64.2,-64.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TOut1.T, production_H_on_File.Tout) annotation (Line(
      points={{58,-21},{58,-8},{71.4,-8},{71.4,-64.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TOut2.T, production_H_on_File_BigPanel.Tout) annotation (Line(
      points={{58,-137},{58,-130},{37.4,-130},{37.4,-113.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TIn2.T, production_H_on_File_BigPanel.Tin) annotation (Line(
      points={{-12,-137},{-12,-132},{30.2,-132},{30.2,-113.4}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(table_extraction.y[5], Col_H_on_File.Text) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-20},{-20,-20},{-20,-34.4},{12,-34.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sou3.ports[1],TIn3. port_a) annotation (Line(
      points={{-36,-250},{-24,-250}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TOut3.port_b,sin3. ports[1]) annotation (Line(
      points={{66,-250},{80,-250}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Col_H_on_weather.port_b, TOut3.port_a) annotation (Line(
      points={{34,-250},{46,-250}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TIn3.port_b, Col_H_on_weather.port_a) annotation (Line(
      points={{-4,-250},{14,-250}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(table_extraction.y[1], sou3.T_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-254},{-58,-254}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], sou3.m_flow_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-258},{-56,-258}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TIn3.T, production_H_on_weather.Tin) annotation (Line(
      points={{-14,-239},{-14,-232},{20.2,-232},{20.2,-223.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TOut3.T, production_H_on_weather.Tout) annotation (Line(
      points={{56,-239},{56,-234},{27.4,-234},{27.4,-223.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], production_H_on_weather.m_flow) annotation (
      Line(
      points={{-74.8,0},{-66,0},{-66,-226},{12.6,-226},{12.6,-223.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[5], Col_H_on_weather.Text) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-280},{6,-280},{6,-252.4},{12,-252.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[3], HDir.u) annotation (Line(
      points={{-74.8,0},{-18,0},{-18,10},{38,10}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(weaDat.weaBus, Col_H_on_weather.weaBus) annotation (Line(
      points={{-40,80},{2,80},{2,-240.4},{14,-240.4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(sou4.ports[1],TIn4. port_a) annotation (Line(
      points={{-38,-338},{-26,-338}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TOut4.port_b,sin4. ports[1]) annotation (Line(
      points={{64,-338},{78,-338}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Col_H_on_weatherDirect.port_b, TOut4.port_a) annotation (Line(
      points={{32,-338},{44,-338}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TIn4.port_b, Col_H_on_weatherDirect.port_a) annotation (Line(
      points={{-6,-338},{12,-338}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(table_extraction.y[4], sou4.m_flow_in) annotation (Line(
      points={{-74.8,0},{-68,0},{-68,-346},{-58,-346}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[1], sou4.T_in) annotation (Line(
      points={{-74.8,0},{-68,0},{-68,-342},{-60,-342}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TIn4.T, production_H_on_weatherDirect.Tin) annotation (Line(
      points={{-16,-327},{-18,-327},{-18,-324},{20.2,-324},{20.2,-315.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], production_H_on_weatherDirect.m_flow)
    annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-320},{12.6,-320},{12.6,-315.4}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(weaDat.weaBus, Col_H_on_weatherDirect.weaBus) annotation (Line(
      points={{-40,80},{2,80},{2,-328.4},{12,-328.4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(TOut4.T, production_H_on_weatherDirect.Tout) annotation (Line(
      points={{54,-327},{56,-327},{56,-322},{27.4,-322},{27.4,-315.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[5], Col_H_on_weatherDirect.Text) annotation (Line(
      points={{-74.8,0},{-68,0},{-68,-364},{4,-364},{4,-340.4},{10,-340.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[1], sou5.T_in) annotation (Line(
      points={{-74.8,0},{-68,0},{-68,-416},{-60,-416}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], sou5.m_flow_in) annotation (Line(
      points={{-74.8,0},{-68,0},{-68,-420},{-58,-420}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[1], sou6.T_in) annotation (Line(
      points={{-74.8,0},{-68,0},{-68,-506},{-58,-506}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], sou6.m_flow_in) annotation (Line(
      points={{-74.8,0},{-68,0},{-68,-510},{-56,-510}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sou6.ports[1], TIn6.port_a) annotation (Line(
      points={{-36,-502},{-26,-502}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TIn6.port_b,Col_H_on_weather_media. port_a) annotation (Line(
      points={{-6,-502},{14,-502}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(sou5.ports[1], TIn5.port_a) annotation (Line(
      points={{-38,-412},{-26,-412}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TIn5.port_b, Col_H_on_olivier_angular.port_a) annotation (Line(
      points={{-6,-412},{12,-412}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Col_H_on_olivier_angular.port_b, TOut5.port_a) annotation (Line(
      points={{32,-412},{44,-412}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TOut5.port_b, sin5.ports[1]) annotation (Line(
      points={{64,-412},{78,-412}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Col_H_on_weather_media.port_b, TOut6.port_a) annotation (Line(
      points={{34,-502},{48,-502}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TOut6.port_b, sin6.ports[1]) annotation (Line(
      points={{68,-502},{80,-502}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(table_extraction.y[5], Col_H_on_olivier_angular.Text) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-434},{0,-434},{0,-414.4},{10,-414.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[5],Col_H_on_weather_media. Text) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-520},{0,-520},{0,-504.4},{12,-504.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(weaDat.weaBus, Col_H_on_olivier_angular.weaBus) annotation (Line(
      points={{-40,80},{2,80},{2,-402.4},{12,-402.4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(weaDat.weaBus,Col_H_on_weather_media. weaBus) annotation (Line(
      points={{-40,80},{2,80},{2,-492.4},{14,-492.4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(TIn5.T, production_H_on_olivier_angular.Tin) annotation (Line(
      points={{-16,-401},{-16,-398},{24.2,-398},{24.2,-391.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TOut5.T, production_H_on_olivier_angular.Tout) annotation (Line(
      points={{54,-401},{54,-398},{31.4,-398},{31.4,-391.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], production_H_on_olivier_angular.m_flow) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-396},{16.6,-396},{16.6,-391.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TIn6.T,production_H_on_weather_media. Tin) annotation (Line(
      points={{-16,-491},{-16,-488},{20.2,-488},{20.2,-477.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TOut6.T,production_H_on_weather_media. Tout) annotation (Line(
      points={{58,-491},{58,-486},{27.4,-486},{27.4,-477.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4],production_H_on_weather_media. m_flow) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-482},{12.6,-482},{12.6,-477.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[3], Col_H_on_olivier_angular.Hdir_data) annotation (Line(
      points={{-74.8,0},{-64,0},{-64,-388},{-2,-388},{-2,-405.2},{10,-405.2}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(sou7.ports[1],TIn7. port_a) annotation (Line(
      points={{-36,-594},{-24,-594}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TIn7.port_b, Col_H_on_file_media.port_a) annotation (Line(
      points={{-4,-594},{14,-594}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Col_H_on_file_media.port_b, TOut7.port_a) annotation (Line(
      points={{34,-594},{46,-594}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TOut7.port_b,sin7. ports[1]) annotation (Line(
      points={{66,-594},{80,-594}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(table_extraction.y[5], Col_H_on_file_media.Text) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-612},{0,-612},{0,-596.4},{12,-596.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(weaDat.weaBus, Col_H_on_file_media.weaBus) annotation (Line(
      points={{-40,80},{2,80},{2,-584.4},{14,-584.4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(TIn7.T, production_H_on_file_media.Tin) annotation (Line(
      points={{-14,-583},{-14,-580},{24.2,-580},{24.2,-569.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TOut7.T, production_H_on_file_media.Tout) annotation (Line(
      points={{56,-583},{56,-578},{31.4,-578},{31.4,-569.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], production_H_on_file_media.m_flow) annotation (
     Line(
      points={{-74.8,0},{-66,0},{-66,-574},{16.6,-574},{16.6,-569.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[1], sou7.T_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-598},{-58,-598}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[4], sou7.m_flow_in) annotation (Line(
      points={{-74.8,0},{-66,0},{-66,-602},{-56,-602}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(table_extraction.y[3], Col_H_on_file_media.Hdir_data) annotation (
      Line(
      points={{-74.8,0},{6,0},{6,-587.2},{12,-587.2}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -640},{320,100}}), graphics={
        Text(
          extent={{166,-18},{260,-56}},
          lineColor={0,128,0},
          fontSize=12,
          textString="Olivier data
mini collectors"),
        Text(
          extent={{170,-126},{264,-164}},
          lineColor={0,128,0},
          fontSize=11,
          textString="Olivier data
Only one collector"),
        Text(
          extent={{170,-220},{264,-258}},
          lineColor={0,128,0},
          fontSize=11,
          textString="Weather data
mini collectors"),
        Text(
          extent={{174,-316},{268,-354}},
          lineColor={0,128,0},
          fontSize=11,
          textString="Weather data
mini collectors
Direct irradiation = Direct + Diffuse"),
        Text(
          extent={{170,-482},{264,-520}},
          lineColor={0,128,0},
          fontSize=11,
          textString="Weather data
mini collectors
Water and glycol media"),
        Text(
          extent={{174,-392},{268,-430}},
          lineColor={0,128,0},
          fontSize=11,
          textString="Olivier data
mini collectors
No angular correction"),
        Text(
          extent={{170,-574},{264,-612}},
          lineColor={0,128,0},
          fontSize=10,
          textString="Olivier data
mini collectors
Water and glycol media")}),               Documentation(info="<html>
<p>Current tests</p>
<ul>
<li>Direct from data and no diffuse irradiation (bottom with 1)</li>
</ul>
<p>If test pass try with new collector to pass validation.</p>
</html>"),
    Icon(coordinateSystem(extent={{-100,-640},{320,100}})),
    experiment(
      StartTime=1.0368e+007,
      StopTime=1.34107e+007,
      Interval=360),
    __Dymola_experimentSetupOutput);
end collector_verif;
