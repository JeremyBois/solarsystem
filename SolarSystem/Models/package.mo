within SolarSystem;
package Models "Solars systems define to run simulation or validate models"
  extends Modelica.Icons.Package;


  annotation (
    conversion(from(version="", script="ConvertFromFramework_.mos")), Icon(
      coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
                  graphics),
    Diagram(coordinateSystem(extent={{-100,-100},{100,100}})));
end Models;
