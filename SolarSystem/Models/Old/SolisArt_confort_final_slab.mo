within SolarSystem.Models.Old;
model SolisArt_confort_final_slab
  "Simulation of solar tank recharge and discharge"
extends Modelica.Icons.Example;

  // Mediums
  package MediumA = Buildings.Media.GasesConstantDensity.SimpleAir
    "Medium air model";
  package MediumE = Buildings.Media.ConstantPropertyLiquidWater
    "Medium water model";

  // Rooms simplifications
  parameter Modelica.SIunits.Angle S_=
    Buildings.HeatTransfer.Types.Azimuth.S "Azimuth for south walls";
  parameter Modelica.SIunits.Angle E_=
    Buildings.HeatTransfer.Types.Azimuth.E "Azimuth for east walls";
  parameter Modelica.SIunits.Angle W_=
    Buildings.HeatTransfer.Types.Azimuth.W "Azimuth for west walls";
  parameter Modelica.SIunits.Angle N_=
    Buildings.HeatTransfer.Types.Azimuth.N "Azimuth for north walls";
  parameter Modelica.SIunits.Angle C_=
    Buildings.HeatTransfer.Types.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    Buildings.HeatTransfer.Types.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle Z_=
    Buildings.HeatTransfer.Types.Tilt.Wall "Tilt for wall";
  parameter Integer nConExtWin = 4 "Number of constructions with a window";
  parameter Integer nConBou = 1
    "Number of surface that are connected to constructions that are modeled inside the room";

  parameter Modelica.SIunits.Pressure dpPip_nominal = 1000
    "Pressure difference of pipe (without valve)";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.153
    "Mass flow rate for pump S6 S5 and splitter";

  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{722,-260},{762,-220}})));

  SolarSystemyy.Classes.Control.Horloge horloge(Heure_debut=0)
    annotation (Placement(transformation(extent={{780,-260},{820,-220}})));

   parameter Buildings.HeatTransfer.Data.Solids.Generic soil(
    k=1.9,
    c=790,
    d=1900,
    x=3) "Soil properties"
    annotation (Placement(transformation(extent={{708,-36},{728,-16}})));
  Buildings.Rooms.MixedAir roo(
    redeclare package Medium = MediumA,
    hRoo=2.7,
    nConExtWin=nConExtWin,
    nPorts=4,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    nConPar=0,
    nSurBou=0,
    linearizeRadiation=false,
    datConBou(
      layers={Floor},
      each A=67.5,
      each til=F_),
    AFlo=67.5,
    nConBou=nConBou,
    nConExt=1,
    datConExt(
      layers={Roof},
      A={67.5},
      til={C_},
      azi={S_}),
    datConExtWin(
      layers={Wall,Wall,Wall,Wall},
      each A=19.19,
      glaSys={GlaSys_Argon,GlaSys_Argon,GlaSys_Argon,GlaSys_Argon},
      wWin={3,3,3,1.7},
      hWin={1.7,1.7,1.7,1},
      each fFra=0.1,
      each til=Z_,
      azi={W_,S_,E_,N_}),
    T_start=293.15,
    lat=0.78539816339745,
    m_flow_nominal=0.08) "Room model for Case 600"
    annotation (Placement(transformation(extent={{646,-100},{680,-66}})));
  Modelica.Blocks.Sources.Constant qConGai_flow(k=0/48) "Convective heat gain"
    annotation (Placement(transformation(extent={{482,42},{500,60}})));
  Modelica.Blocks.Sources.Constant qRadGai_flow(k=0/48) "Radiative heat gain"
    annotation (Placement(transformation(extent={{520,60},{540,80}})));
  Modelica.Blocks.Routing.Multiplex3 multiplex3_1
    annotation (Placement(transformation(extent={{560,42},{578,60}})));
  Modelica.Blocks.Sources.Constant qLatGai_flow(k=0/67.5) "Latent heat gain"
    annotation (Placement(transformation(extent={{520,20},{540,40}})));
  Modelica.Blocks.Sources.Constant uSha(k=0)
    "Control signal for the shading device"
    annotation (Placement(transformation(extent={{494,92},{514,112}})));
  Modelica.Blocks.Routing.Replicator replicator(nout=max(1,nConExtWin))
    annotation (Placement(transformation(extent={{562,92},{580,112}})));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature TSoi[nConBou](each T=
        283.15) "Boundary condition for construction"
                                          annotation (Placement(transformation(
        extent={{0,0},{-24,24}},
        rotation=0,
        origin={736,-146})));
  Buildings.HeatTransfer.Conduction.SingleLayer soi(
    A=48,
    material=soil,
    steadyStateInitial=true,
    T_a_start=283.15,
    T_b_start=283.75) "2m deep soil (per definition on p.4 of ASHRAE 140-2007)"
    annotation (Placement(transformation(
        extent={{5,-5},{-3,3}},
        rotation=-90,
        origin={669,-125})));
  Buildings.Fluid.Sources.MassFlowSource_T sinInf(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{614,-184},{630,-168}})));
  Buildings.Fluid.Sources.Outside souInf(redeclare package Medium = MediumA,
      nPorts=1) "Source model for air infiltration"
           annotation (Placement(transformation(extent={{598,-118},{610,-106}})));
  Modelica.Blocks.Sources.Constant InfiltrationRate(k=-67.5*2.7*0.5/3600)
    "0.41 ACH adjusted for the altitude (0.5 at sea level)"
    annotation (Placement(transformation(extent={{-11,-11},{11,11}},
        rotation=90,
        origin={521,-205})));
  Modelica.Blocks.Math.Product product
    annotation (Placement(transformation(extent={{554,-170},{572,-152}})));
  Buildings.Fluid.Sensors.Density density(redeclare package Medium = MediumA)
    "Air density inside the building"
    annotation (Placement(transformation(extent={{608,-204},{590,-188}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TRooAir
    "Room air temperature"
    annotation (Placement(transformation(extent={{728,-58},{744,-42}})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM   heaCoo(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    m_flow_nominal=0.05 + 67.5*2.7*0.41/3600*1.2) "Heater and cooler"
    annotation (Placement(transformation(extent={{622,-118},{634,-106}})));
  Modelica.Blocks.Math.MultiSum multiSum(nu=1)
    annotation (Placement(transformation(extent={{520,-164},{536,-148}})));
  Modelica.Blocks.Math.Mean TRooHou(f=1/3600, y(start=293.15))
    "Hourly averaged room air temperature"
    annotation (Placement(transformation(extent={{780,-20},{800,0}})));
  Modelica.Blocks.Math.Mean TRooAnn(f=1/86400/365, y(start=293.15))
    "Annual averaged room air temperature"
    annotation (Placement(transformation(extent={{780,-100},{800,-80}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Wall(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    material={Models.Data.WoodPanel(x=0.04),Models.Data.GlassWool(x=0.08),Models.Data.Wood(x=
        0.04)},
    nLay=3) "Wall layers" annotation (Placement(transformation(extent={{660,54},{688,82}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Roof(material={Models.Data.Wood(x=
        0.1),Models.Data.GlassWool(x=0.1)}, nLay=2) "Roof layers"
    annotation (Placement(transformation(extent={{700,86},{728,114}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Floor(        material=
       {Buildings.HeatTransfer.Data.Solids.Concrete(x=0.15)}, nLay=1)
    "Floor layer"
    annotation (Placement(transformation(extent={{702,26},{730,54}})));
  Data.DoubleClearArgon16 GlaSys_Argon
    annotation (Placement(transformation(extent={{740,54},{768,82}})));
  Modelica.Blocks.Math.Mean TRooDay(y(start=293.15), f=1/3600/24)
    "Month averaged room air temperature"
    annotation (Placement(transformation(extent={{780,-60},{800,-40}})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C6(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    m_flow(start=0.04),
    m_flow_nominal=m_flow_nominal,
    T_start=T_start)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={56,-74})));
  Buildings.Fluid.Sources.Boundary_pT   boundary(         redeclare package
      Medium =         MediumE,
      nPorts=1)
    annotation (Placement(transformation(extent={{4,0},{16,12}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        MediumE,
    use_m_flow_in=false,
    use_T_in=true,
    m_flow=0.000001,
    nPorts=1)
    annotation (Placement(transformation(extent={{162,-44},{150,-32}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    V_start(displayUnit="l")=
            0.2,
    redeclare package Medium = MediumE,
    T_start=T_start)
    annotation (Placement(transformation(extent={{-64,-62},{-26,-24}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T5
    annotation (Placement(transformation(extent={{74,22},{62,34}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(
      computeWetBulbTemperature=false, filNam=
        "C:/Users/bois/Documents/Dymola/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-30,94},{-50,112}})));
  SolarSystemyy.Classes.SolarPanels.EN12975_Temp solarPanel_ISO(
    redeclare package Medium = MediumE,
    per=SolarSystemyy.Data.Parameter.SolarCollector.IDMK2_5_buildings(),
    azi=0,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    nSeg=40,
    nPanels=6,
    use_shaCoe_in=true,
    T_start=T_start,
    lat=0.78539816339745,
    til=0.5235987755983)
    annotation (Placement(transformation(extent={{-72,42},{-28,90}})));

  SolarSystemyy.Classes.Exchanger_and_Tank.StratifiedEnhancedInternalHex_simple
    Storage_tank(
    redeclare package Medium = MediumE,
    redeclare package MediumHex = MediumE,
    CHex=200,
    hexSegMult=3,
    nSeg=20,
    m_flow_nominal=0.000001,
    kIns=0.04,
    VTan(displayUnit="l") = 0.5,
    hTan=1.6,
    dIns(displayUnit="mm") = 0.1,
    hexTopHeight=1.415,
    hexBotHeight=0.255,
    dExtHex(displayUnit="mm") = 0.0346,
    Q_flow_nominal(displayUnit="W") = 60000,
    mHex_flow_nominal=m_flow_nominal,
    TTan_nominal=283.15,
    THex_nominal=318.15)
    annotation (Placement(transformation(extent={{70,-34},{108,10}})));
  Buildings.HeatTransfer.Sources.FixedTemperature Troom1(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{-8,-8},{8,8}},
        rotation=90,
        origin={120,-64})));
  SolarSystemyy.Classes.Exchanger_and_Tank.StratifiedEnhancedInternalHex_double
    Solar_tank(
    redeclare package Medium = MediumE,
    redeclare package MediumHex = MediumE,
    nSeg=20,
    hexSegMult={2,2},
    CHex={200,200},
    m_flow_nominal=m_flow_nominal,
    VTan(displayUnit="l") = 0.4,
    dExtHex(displayUnit="mm") = {0.0279,0.0337},
    Q_flow_nominal(displayUnit="W") = {25000,53000},
    TTan_nominal(displayUnit="degC") = {333.15,318.15},
    hTan=1.67,
    hexTopHeight={1.375,0.86},
    hexBotHeight={0.955,0.175},
    dIns(displayUnit="mm") = 0.1,
    mHex_flow_nominal={m_flow_nominal*1.7,m_flow_nominal},
    THex_nominal={353.15,283.15})
    annotation (Placement(transformation(extent={{192,-46},{244,20}})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C5(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={174,-68})));
  Buildings.HeatTransfer.Sources.FixedTemperature Troom2(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{-9,-9},{9,9}},
        rotation=90,
        origin={241,-71})));
  Buildings.Fluid.Sources.FixedBoundary Cool_water(redeclare package Medium =
        MediumE,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{11,-10},{-11,10}},
        rotation=-90,
        origin={256,-97})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T3
    annotation (Placement(transformation(extent={{200,14},{214,28}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T4
    annotation (Placement(transformation(extent={{200,38},{214,52}})));
  Buildings.Fluid.FixedResistances.Pipe flowBetween_tanks(
                                                         redeclare package
      Medium = MediumE,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    nSeg=8,
    length=4,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start)
    annotation (Placement(transformation(extent={{130,58},{142,70}})));
  Buildings.Fluid.FixedResistances.Pipe inFlowTop_Storage(
                                                         redeclare package
      Medium = MediumE,
    nSeg=6,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=3,
    useMultipleHeatPorts=true,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start)                                      annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={52,44})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{-1,1,-1},
    T_start=T_start) "Flow splitter"                   annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={52,64})));
  SolarSystemyy.Classes.Other.Water_Drawing_up_control Drawing_up1(
    T=5,
    k=0.8,
    redeclare package Medium = MediumE,
    table=[0,0,0; 6.99,0,0; 7,24,0.01; 8.99,24,0.01; 9,0,0; 11.99,0,0; 12,24,
        0.01; 13.99,24,0.01; 14,0,0; 18.99,0,0; 19,117,0.03; 20.99,117,0.03;
        21,0,0; 24,0,0])
    annotation (Placement(transformation(extent={{132,20},{172,50}})));
  Buildings.Fluid.FixedResistances.Pipe outFlow_panels(
                                                      redeclare package Medium
      = MediumE,
    nSeg=24,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=12,
    useMultipleHeatPorts=true,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start)
    annotation (Placement(transformation(extent={{12,56},{28,72}})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_panels(
                                                     redeclare package Medium
      = MediumE, thicknessIns(displayUnit="mm") = thicknessIns,
    nSeg=24,
    lambdaIns=lambdaIns,
    length=12,
    useMultipleHeatPorts=true,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start)
    annotation (Placement(transformation(extent={{-11,-7},{11,7}},
        rotation=90,
        origin={-81,-17})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal1(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{1,-1,1},
    T_start=T_start) "Flow splitter"                   annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={68,-120})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C2(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    m_flow_nominal=m_flow_nominal*1.7,
    T_start=T_start)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={400,-74})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal2(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{1,-1,1},
    T_start=T_start) "Flow splitter"                   annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={174,-120})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T7(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{294,-130},{314,-110}})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal3(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*1.7*{1,-1,1},
    T_start=T_start) "Flow splitter"                   annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={360,-120})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C4(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={360,-60})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T8(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal*1.7)
    annotation (Placement(transformation(extent={{324,54},{344,74}})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal4(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*1.7*{-1,1,-1},
    T_start=T_start) "Flow splitter"                   annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={362,64})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_topExtra(
                                                      redeclare package Medium
      = MediumE,
    nSeg=6,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=3,
    m_flow_nominal=m_flow_nominal*1.7,
    T_start=T_start)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={362,34})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_heating(
                                                      redeclare package Medium
      = MediumE,
    nSeg=26,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=13,
    m_flow_nominal=m_flow_nominal*1.7,
    T_start=T_start)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={402,32})));
  Buildings.Fluid.FixedResistances.Pipe outFlow_heating(
                                                       redeclare package Medium
      = MediumE,
    nSeg=6,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=3,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal*1.7)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=0,
        origin={336,-120})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM spl(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*1.7*{-1,1,-1},
    T_start=T_start)
    annotation (Placement(transformation(extent={{266,58},{278,70}})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_backup(
                                                     redeclare package Medium
      = MediumE,
    nSeg=12,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=6,
    m_flow_nominal=m_flow_nominal*1.7,
    T_start=T_start)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={272,-36})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal5(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=m_flow_nominal*{-1,1,-1},
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    T_start=T_start) "Flow splitter"                   annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={182,64})));
  Buildings.Fluid.FixedResistances.Pipe flow_between_tankAndBackup(
                                                                  redeclare
      package Medium = MediumE,
    nSeg=8,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=4,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start)
    annotation (Placement(transformation(extent={{232,58},{244,70}})));
  Buildings.Fluid.FixedResistances.Pipe inFlowLow_storage(
                                                         redeclare package
      Medium = MediumE,
    nSeg=6,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=3,
    useMultipleHeatPorts=true,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={16,-76})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T2(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{52,-130},{32,-110}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T1(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{-14,54},{6,74}})));
  Buildings.Fluid.Boilers.BoilerPolynomial boi(
    redeclare package Medium =
        MediumE,
    fue=Buildings.Fluid.Data.Fuels.NaturalGasHigherHeatingValue(),
    dp_nominal=1000,
    m_flow_nominal=m_flow_nominal*1.7,
    T_start=T_start,
    Q_flow_nominal=5000)                                           annotation (
      Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={272,38})));
  Buildings.HeatTransfer.Sources.FixedTemperature Troom3(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{232,28},{248,44}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T10(redeclare package Medium =
        MediumE, m_flow_nominal=0.04)
    annotation (Placement(transformation(extent={{-8,-8},{8,8}},
        rotation=90,
        origin={160,0})));
  Buildings.Fluid.Actuators.Valves.ThreeWayLinear solar_valve(
    fraK=0.5,
    l={0.001,0.001},
    redeclare package Medium = MediumE,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    dpValve_nominal=1500)
    annotation (Placement(transformation(extent={{-20,-110},{0,-130}})));
  Buildings.Fluid.Actuators.Valves.ThreeWayLinear extra_valve(
    fraK=0.5,
    l={0.001,0.001},
    redeclare package Medium = MediumE,
    T_start=T_start,
    dpValve_nominal=3000,
    m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{262,-110},{282,-130}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{-68,-282},{-40,-254}})));
  Modelica.Blocks.Math.RealToBoolean real_solar
                                           annotation (Placement(transformation(
        extent={{-9,9},{9,-9}},
        rotation=0,
        origin={259,-159})));
  SolarSystemyy.Classes.Control.SolisArt_mod.pump_algo pump_algo(
    S6_yMin=0.25,
    S5_yMin=0.25,
    S4_yMin=0.25,
    Sj_yMin=0.25,
    S6_Td=120,
    S6_wp=0.7,
    S6_wd=0.3,
    S5_Td=120,
    S5_wp=0.7,
    S5_wd=0.3,
    S4_Td=120,
    S4_wp=0.7,
    S4_wd=0.3,
    Sj_Td=120,
    Sj_wp=0.7,
    Sj_wd=0.3,
    m_flow_mini=0)
    annotation (Placement(transformation(extent={{290,-274},{462,-172}})));
  SolarSystemyy.Classes.Control.SolisArt_mod.Algo algo(WeatherFile=
        "C:/Users/bois/Documents/Dymola/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos",
      Tambiant_out_start={293.15,293.15})
    annotation (Placement(transformation(extent={{30,-286},{228,-152}})));
  Modelica.Blocks.Sources.RealExpression fake_room(each y=30 + 273.15)
    "algo need at least 2 heating pumps"
    annotation (Placement(transformation(extent={{-84,-298},{-44,-278}})));
  SolarSystemyy.Classes.Control.Bloc.Shadow shadow(n=5)
    annotation (Placement(transformation(extent={{24,88},{-16,112}})));
  Buildings.Fluid.FixedResistances.Pipe flowBetween_tanks1(
                                                         redeclare package
      Medium = MediumE,
    nSeg=8,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=4,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{130,-126},{118,-114}})));
  Buildings.Fluid.FixedResistances.Pipe flowBetween_tanks2(
                                                         redeclare package
      Medium = MediumE,
    nSeg=8,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=4,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{230,-126},{218,-114}})));
  Buildings.Fluid.FixedResistances.Pipe inFlowTop_Storage1(
                                                         redeclare package
      Medium = MediumE,
    nSeg=12,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=6,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start)                                      annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={182,32})));
  Buildings.Fluid.FixedResistances.Pipe outFlow_heating1(
                                                       redeclare package Medium
      = MediumE,
    nSeg=26,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=13,
    m_flow_nominal=m_flow_nominal*1.7,
    T_start=T_start)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=0,
        origin={386,-120})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_backup1(
                                                     redeclare package Medium
      = MediumE,
    nSeg=6,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=3,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal*1.7)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={360,-24})));
  parameter Modelica.SIunits.Length thicknessIns=0.100
    "Thickness of insulation";
  parameter Modelica.SIunits.ThermalConductivity lambdaIns=0.04
    "Heat conductivity of insulation";
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature
    prescribedTemperature[24]
    annotation (Placement(transformation(extent={{-20,-12},{-40,8}})));
  parameter Modelica.Media.Interfaces.PartialMedium.Temperature T_start=303.15
    "Start value of temperature";
  Modelica.Blocks.Routing.Replicator replicator_Text(nout=24) annotation (
      Placement(transformation(
        extent={{-9,-8},{9,8}},
        rotation=90,
        origin={0,-37})));
  SolarSystemyy.Classes.Control.Bloc.Limit limit(n=1, threshold={333.15,
        353.15}) annotation (Placement(transformation(
        extent={{8,-6},{-8,6}},
        rotation=90,
        origin={294,20})));
  Modelica.Blocks.Math.MultiProduct gain(nu=2) annotation (Placement(
        transformation(
        extent={{-2,-2},{2,2}},
        rotation=90,
        origin={260,12})));
  Modelica.Blocks.Sources.Constant extraction_meca(k=-150/3600)
    "0.41 ACH adjusted for the altitude (0.5 at sea level)" annotation (
      Placement(transformation(
        extent={{11,-11},{-11,11}},
        rotation=90,
        origin={515,-105})));
  Modelica.Blocks.Math.Product product1
    annotation (Placement(transformation(extent={{548,-136},{566,-118}})));
  Buildings.Fluid.Sources.MassFlowSource_T sinInf1(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{604,-142},{620,-126}})));
  Buildings.Fluid.HeatExchangers.RadiantSlabs.SingleCircuitSlab sla(
    sysTyp=Buildings.Fluid.HeatExchangers.RadiantSlabs.BaseClasses.Types.SystemType.Floor,
    pipe=Buildings.Fluid.Data.Pipes.PEX_DN_20(),
    iLayPip=1,
    nSeg=30,
    A=67.5,
    redeclare package Medium = MediumE,
    layers=Heating_slab,
    disPip=0.22,
    T_start=T_start,
    m_flow_nominal=1.75*m_flow_nominal)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={436,-24})));

  parameter Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Heating_slab(nLay=2,
      material={Buildings.HeatTransfer.Data.Solids.Concrete(x=0.3),Models.Data.GlassWool(x=
        0.05)})
    "Material layers from surface a to b (15cm concrete, 5 cm insulation)"
    annotation (Placement(transformation(extent={{708,-6},{728,14}})));
  parameter Buildings.Fluid.Data.Pipes.PEX_RADTEST
                                   pipe "Pipe material"
    annotation (Placement(transformation(extent={{672,-28},{692,-8}})));
equation
  connect(qRadGai_flow.y,multiplex3_1. u1[1])  annotation (Line(
      points={{541,70},{550,70},{550,57.3},{558.2,57.3}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qLatGai_flow.y,multiplex3_1. u3[1])  annotation (Line(
      points={{541,30},{550,30},{550,44.7},{558.2,44.7}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiplex3_1.y,roo. qGai_flow) annotation (Line(
      points={{578.9,51},{632,51},{632,-74.5},{639.2,-74.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.uSha,replicator. y) annotation (Line(
      points={{644.3,-69.4},{640,-69.4},{640,102},{580.9,102}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qConGai_flow.y,multiplex3_1. u2[1]) annotation (Line(
      points={{500.9,51},{558.2,51}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(uSha.y,replicator. u) annotation (Line(
      points={{515,102},{560.2,102}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(product.y,sinInf. m_flow_in)       annotation (Line(
      points={{572.9,-161},{604,-161},{604,-169.6},{614,-169.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(density.port,roo. ports[1])  annotation (Line(
      points={{599,-204},{644,-204},{644,-91.5},{647.7,-91.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(density.d,product. u2) annotation (Line(
      points={{589.1,-196},{540,-196},{540,-166.4},{552.2,-166.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.heaPorAir,TRooAir. port)  annotation (Line(
      points={{662.15,-83},{712,-83},{712,-50},{728,-50}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(souInf.ports[1],heaCoo. port_a)        annotation (Line(
      points={{610,-112},{622,-112}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(heaCoo.port_b,roo. ports[2])  annotation (Line(
      points={{634,-112},{644,-112},{644,-91.5},{649.4,-91.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(sinInf.ports[1],roo. ports[3])        annotation (Line(
      points={{630,-176},{644,-176},{644,-91.5},{651.1,-91.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(multiSum.y,product. u1) annotation (Line(
      points={{537.36,-156},{552.2,-156},{552.2,-155.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(InfiltrationRate.y,multiSum. u[1]) annotation (Line(
      points={{521,-192.9},{520,-192.9},{520,-156}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T,TRooHou. u) annotation (Line(
      points={{744,-50},{760,-50},{760,-10},{778,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T,TRooAnn. u) annotation (Line(
      points={{744,-50},{760,-50},{760,-90},{778,-90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooDay.u) annotation (Line(
      points={{744,-50},{778,-50}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(Meteo.weaBus,solarPanel_ISO. weaBus) annotation (Line(
      points={{-50,103},{-72,103},{-72,89.04}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(boundary1.ports[1], Storage_tank.port_b) annotation (Line(
      points={{150,-38},{144,-38},{144,-12},{108,-12}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Storage_tank.port_a, boundary.ports[1]) annotation (Line(
      points={{70,-12},{36,-12},{36,6},{16,6}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Storage_tank.heaPorSid, Troom1.port) annotation (Line(
      points={{99.64,-12},{104,-12},{104,-6},{120,-6},{120,-56}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Storage_tank.heaPorTop, Troom1.port) annotation (Line(
      points={{92.8,4.28},{92.8,10},{104,10},{104,-6},{120,-6},{120,-56}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Storage_tank.port_b1, C6.port_a) annotation (Line(
      points={{70,-29.6},{70,-30},{56,-30},{56,-64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.port_b1,C5. port_a) annotation (Line(
      points={{192,-34.78},{192,-34},{174,-34},{174,-58}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.heaPorSid,Troom2. port) annotation (Line(
      points={{232.56,-13},{236,-13},{236,-14},{240,-14},{240,-20},{241,-20},{241,
          -62}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Solar_tank.heaPorTop,Troom2. port) annotation (Line(
      points={{223.2,11.42},{223.2,14},{248,14},{248,-10},{241,-10},{241,
          -62}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(splitVal.port_1,flowBetween_tanks. port_a)
                                              annotation (Line(
      points={{58,64},{130,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal.port_3,inFlowTop_Storage. port_a)
                                                annotation (Line(
      points={{52,58},{52,50}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlowTop_Storage.port_b, Storage_tank.port_a1) annotation (Line(
      points={{52,38},{52,2},{64,2},{64,-20.36},{70,-20.36}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(outFlow_panels.port_b,splitVal. port_2)      annotation (Line(
      points={{28,64},{46,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_panels.port_b,solarPanel_ISO. port_a)        annotation (
      Line(
      points={{-81,-6},{-81,66},{-72,66}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.heaPorVol[2],T4. port) annotation (Line(
      points={{218,-14.683},{194,-14.683},{194,45},{200,45}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Cool_water.ports[1],Solar_tank. port_b) annotation (Line(
      points={{256,-86},{256,-13},{244,-13}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.heaPorVol[18],T3. port) annotation (Line(
      points={{218,-11.515},{218,-14},{194,-14},{194,21},{200,21}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Storage_tank.heaPorVol[4], T5.port) annotation (Line(
      points={{89,-12.858},{80,-12.858},{80,28},{74,28}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(C6.port_b,splitVal1. port_3) annotation (Line(
      points={{56,-84},{56,-102},{68,-102},{68,-114}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C5.port_b,splitVal2. port_3) annotation (Line(
      points={{174,-78},{174,-114}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C4.port_b,splitVal3. port_3) annotation (Line(
      points={{360,-70},{360,-114}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T8.port_b,splitVal4. port_2) annotation (Line(
      points={{344,64},{356,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal4.port_3,inFlow_topExtra. port_a) annotation (Line(
      points={{362,58},{362,40}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal4.port_1,inFlow_heating. port_a)  annotation (Line(
      points={{368,64},{402,64},{402,38}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_topExtra.port_b,Solar_tank. port_a2) annotation (Line(
      points={{362,28},{362,3.5},{244,3.5}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal3.port_2,outFlow_heating. port_a)  annotation (Line(
      points={{354,-120},{342,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(outFlow_heating.port_b,T7. port_b)  annotation (Line(
      points={{330,-120},{314,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(spl.port_2,T8. port_a) annotation (Line(
      points={{278,64},{324,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flowBetween_tanks.port_b,splitVal5. port_2)
                                                   annotation (Line(
      points={{142,64},{176,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flow_between_tankAndBackup.port_b,spl. port_1)
                                              annotation (Line(
      points={{244,64},{266,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal5.port_1,flow_between_tankAndBackup. port_a)
                                                    annotation (Line(
      points={{188,64},{232,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlowLow_storage.port_b, Storage_tank.port_b1) annotation (Line(
      points={{16,-70},{16,-29.6},{70,-29.6}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal1.port_2,T2. port_a) annotation (Line(
      points={{62,-120},{52,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solarPanel_ISO.port_b,T1. port_a) annotation (Line(
      points={{-28,66},{-22,66},{-22,64},{-14,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T1.port_b,outFlow_panels. port_a)      annotation (Line(
      points={{6,64},{12,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_backup.port_b,boi. port_a)   annotation (Line(
      points={{272,-30},{272,24}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(boi.port_b,spl. port_3) annotation (Line(
      points={{272,52},{272,58}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Troom3.port,boi. heatPort) annotation (Line(
      points={{248,36},{254,36},{254,38},{261.92,38}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Solar_tank.port_a,T10. port_a) annotation (Line(
      points={{192,-13},{160,-13},{160,-8}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T10.port_b,Drawing_up1. port_a) annotation (Line(
      points={{160,8},{160,12},{152,12},{152,20.1875}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Meteo.weaBus,weaBus)  annotation (Line(
      points={{-50,103},{-94,103},{-94,-268},{-54,-268}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(T2.port_b,solar_valve. port_2) annotation (Line(
      points={{32,-120},{0,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_3,inFlowLow_storage. port_a)    annotation (Line(
      points={{-10,-110},{-10,-102},{16,-102},{16,-82}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_1,inFlow_panels. port_a)        annotation (Line(
      points={{-20,-120},{-81,-120},{-81,-28}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_1,SurgeTank. port_a) annotation (Line(
      points={{-20,-120},{-45,-120},{-45,-62}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T7.port_a, extra_valve.port_2) annotation (Line(
      points={{294,-120},{282,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(extra_valve.port_3, inFlow_backup.port_a) annotation (Line(
      points={{272,-110},{272,-42}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T5.T,boundary1. T_in) annotation (Line(
      points={{62,28},{58,28},{58,14},{116,14},{116,6},{124,6},{124,-6},{154,-6},
          {154,-18},{168,-18},{168,-35.6},{163.2,-35.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Meteo.weaBus, roo.weaBus) annotation (Line(
      points={{-50,103},{-94,103},{-94,-300},{680,-300},{680,-60},{678.215,-60},
          {678.215,-67.785}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Meteo.weaBus, souInf.weaBus) annotation (Line(
      points={{-50,103},{-94,103},{-94,-300},{680,-300},{680,-144},{570,-144},{570,
          -111.88},{598,-111.88}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));

  connect(real_solar.y, pump_algo.V3V_extra) annotation (Line(
      points={{268.9,-159},{278,-159},{278,-181.69},{291.518,-181.69}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo.S6_outter, pump_algo.pumpControl_S6) annotation (Line(
      points={{229.98,-199.688},{260,-199.688},{260,-202.09},{291.518,
          -202.09}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo.S5_outter, pump_algo.pumpControl_S5) annotation (Line(
      points={{229.98,-216.241},{259.99,-216.241},{259.99,-222.49},{291.518,
          -222.49}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo.S4_outter, pump_algo.pumpControl_S4) annotation (Line(
      points={{229.98,-232.006},{259.99,-232.006},{259.99,-242.89},{291.518,
          -242.89}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo.Sj_outter, pump_algo.pumpControl_Sj) annotation (Line(
      points={{229.98,-247.771},{258.99,-247.771},{258.99,-263.29},{291.518,
          -263.29}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(pump_algo.pumps_miniFlows[3], algo.mini_flow_S4) annotation (Line(
      points={{391.176,-276.04},{391.176,-296},{-22,-296},{-22,-138},{
          77.124,-138},{77.124,-151.606}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(pump_algo.S4_outter, algo.flow_S4) annotation (Line(
      points={{464.024,-182.2},{482,-182.2},{482,-138},{57.324,-138},{
          57.324,-151.606}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(pump_algo.S4_outter, C4.m_flow_in) annotation (Line(
      points={{464.024,-182.2},{482,-182.2},{482,-56},{372,-56},{372,-59.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(pump_algo.S5_outter, C5.m_flow_in) annotation (Line(
      points={{464.024,-208.72},{482,-208.72},{482,-138},{200,-138},{200,
          -67.8},{186,-67.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(pump_algo.S6_outter, C6.m_flow_in) annotation (Line(
      points={{463.012,-239.32},{482,-239.32},{482,-138},{100,-138},{100,
          -73.8},{68,-73.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(pump_algo.Sj_outter[1], C2.m_flow_in) annotation (Line(
      points={{464.024,-269.92},{482,-269.92},{482,-73.8},{412,-73.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo.V3V_extra_outter, real_solar.u) annotation (Line(
      points={{229.98,-159.488},{239.99,-159.488},{239.99,-159},{248.2,-159}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(algo.V3V_extra_outter, extra_valve.y) annotation (Line(
      points={{229.98,-159.488},{240,-159.488},{240,-138},{272,-138},{272,
          -132}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(algo.V3V_solar_outter, solar_valve.y) annotation (Line(
      points={{229.98,-175.253},{240,-175.253},{240,-138},{-10,-138},{-10,
          -132}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T3.T, algo.T3) annotation (Line(
      points={{214,21},{222,21},{222,120},{-100,120},{-100,-176.829},{
          30.396,-176.829}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T4.T, algo.T4) annotation (Line(
      points={{214,45},{222,45},{222,120},{-100,120},{-100,-188},{30,-188},
          {30,-188.653},{30.396,-188.653}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T5.T, algo.T5) annotation (Line(
      points={{62,28},{38,28},{38,120},{-100,120},{-100,-199.688},{30.396,
          -199.688}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T7.T, algo.T7) annotation (Line(
      points={{304,-109},{304,120},{-100,120},{-100,-210},{30.396,-210},{
          30.396,-211.512}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(weaBus.TDryBul, algo.Text) annotation (Line(
      points={{-54,-268},{-12.5,-268},{-12.5,-267.871},{31.584,-267.871}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(T8.T, algo.T8) annotation (Line(
      points={{334,75},{334,120},{-100,120},{-100,-223.335},{30.396,
          -223.335}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T10.T, algo.T10) annotation (Line(
      points={{151.2,5.38845e-016},{126,5.38845e-016},{126,20},{100,20},{
          100,120},{-100,120},{-100,-236.341},{30.792,-236.341}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(TRooAir.T, algo.Tambiant[1]) annotation (Line(
      points={{744,-50},{752,-50},{752,42},{782,42},{782,120},{-100,120},{
          -100,-253.879},{29.604,-253.879}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(fake_room.y, algo.Tambiant[2]) annotation (Line(
      points={{-42,-288},{0,-288},{0,-249.544},{29.604,-249.544}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T3.T, pump_algo.T3) annotation (Line(
      points={{214,21},{220,21},{220,20},{222,20},{222,120},{-100,120},{
          -100,-138},{330.471,-138},{330.471,-174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T4.T, pump_algo.T4) annotation (Line(
      points={{214,45},{222,45},{222,120},{-100,120},{-100,-138},{350,-138},
          {350,-174.04},{350.706,-174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T5.T, pump_algo.T5) annotation (Line(
      points={{62,28},{38,28},{38,120},{-100,120},{-100,-138},{370.941,-138},
          {370.941,-174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T7.T, pump_algo.T7) annotation (Line(
      points={{304,-109},{304,120},{-100,120},{-100,-138},{391.176,-138},{
          391.176,-174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T8.T, pump_algo.T8) annotation (Line(
      points={{334,75},{334,120},{-100,120},{-100,-138},{411.412,-138},{
          411.412,-174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(shadow.out_value, solarPanel_ISO.shaCoe_in) annotation (Line(
      points={{-18.4,100},{-24,100},{-24,90},{-86,90},{-86,72.24},{-76.4,
          72.24}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarPanel_ISO.Tinside[1], shadow.in_value[1]) annotation (Line(
      points={{-25.8,44.52},{-18,44.52},{-18,82},{25.6,82},{25.6,98.08}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[10], shadow.in_value[2]) annotation (Line(
      points={{-25.8,46.68},{-18,46.68},{-18,82},{25.6,82},{25.6,99.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[20], shadow.in_value[3]) annotation (Line(
      points={{-25.8,49.08},{-18,49.08},{-18,82},{25.6,82},{25.6,100}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[30], shadow.in_value[4]) annotation (Line(
      points={{-25.8,51.48},{-18,51.48},{-18,82},{25.6,82},{25.6,100.96}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[40], shadow.in_value[5]) annotation (Line(
      points={{-25.8,53.88},{-18,53.88},{-18,82},{25.6,82},{25.6,101.92}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[40], algo.T1) annotation (Line(
      points={{-25.8,53.88},{-18,53.88},{-18,82},{38,82},{38,120},{-100,120},
          {-100,-165.006},{30.396,-165.006}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[40], pump_algo.T1) annotation (Line(
      points={{-25.8,53.88},{-18,53.88},{-18,82},{38,82},{38,120},{-100,120},
          {-100,-138},{310.235,-138},{310.235,-174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(splitVal2.port_2, flowBetween_tanks1.port_a) annotation (Line(
      points={{168,-120},{130,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flowBetween_tanks1.port_b, splitVal1.port_1) annotation (Line(
      points={{118,-120},{74,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(extra_valve.port_1, flowBetween_tanks2.port_a) annotation (Line(
      points={{262,-120},{230,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flowBetween_tanks2.port_b, splitVal2.port_1) annotation (Line(
      points={{218,-120},{180,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal5.port_3, inFlowTop_Storage1.port_a) annotation (Line(
      points={{182,58},{182,38}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.port_a1, inFlowTop_Storage1.port_b) annotation (Line(
      points={{192,-25.54},{180,-25.54},{180,-26},{176,-26},{176,0},{182,0},
          {182,26}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(outFlow_heating1.port_b, splitVal3.port_1) annotation (Line(
      points={{380,-120},{366,-120}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(C2.port_b, outFlow_heating1.port_a) annotation (Line(
      points={{400,-84},{400,-120},{392,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_backup1.port_b, C4.port_a) annotation (Line(
      points={{360,-30},{360,-50}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.port_b2, inFlow_backup1.port_a) annotation (Line(
      points={{244,-5.74},{280,-5.74},{280,-8},{360,-8},{360,-18}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(prescribedTemperature.port, inFlow_panels.heatPorts) annotation (Line(
      points={{-40,-2},{-60,-2},{-60,-17},{-77.5,-17}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(prescribedTemperature.port, outFlow_panels.heatPorts) annotation (
      Line(
      points={{-40,-2},{-60,-2},{-60,20},{20,20},{20,60}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(weaBus.TDryBul, replicator_Text.u) annotation (Line(
      points={{-54,-268},{-94,-268},{-94,-80},{-6.66134e-016,-80},{
          -6.66134e-016,-47.8}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(replicator_Text.y, prescribedTemperature.T) annotation (Line(
      points={{4.44089e-016,-27.1},{4.44089e-016,-2},{-18,-2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gain.y, boi.y) annotation (Line(
      points={{260,14.34},{260,16.255},{260.8,16.255},{260.8,21.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(limit.out_value, gain.u[1]) annotation (Line(
      points={{294,11.04},{294,6},{259.3,6},{259.3,10}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(boi.T, limit.in_value[1]) annotation (Line(
      points={{260.8,53.4},{260.8,56},{294,56},{294,28.64}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(algo.BackupHeater_outter, gain.u[2]) annotation (Line(
      points={{229.98,-274.571},{238,-274.571},{238,-296},{480,-296},{480,6},
          {260,6},{260,10},{260.7,10}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(product1.y, sinInf1.m_flow_in) annotation (Line(
      points={{566.9,-127},{575.45,-127},{575.45,-127.6},{604,-127.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(density.d, product1.u2) annotation (Line(
      points={{589.1,-196},{540,-196},{540,-132},{540,-132},{546,-132},{546.2,-132},
          {546.2,-132.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(extraction_meca.y, product1.u1) annotation (Line(
      points={{515,-117.1},{515,-121.6},{546.2,-121.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sinInf1.ports[1], roo.ports[4]) annotation (Line(
      points={{620,-134},{644,-134},{644,-91.5},{652.8,-91.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(inFlow_heating.port_b, sla.port_a) annotation (Line(
      points={{402,26},{402,14},{436,14},{436,-14}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(sla.port_b, C2.port_a) annotation (Line(
      points={{436,-34},{436,-48},{400,-48},{400,-64}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(sla.surf_a, roo.surf_conBou[1]) annotation (Line(
      points={{446,-28},{618,-28},{618,-102},{668.1,-102},{668.1,-96.6}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(TSoi[1].port, soi.port_a) annotation (Line(
      points={{712,-134},{668,-134},{668,-130}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(soi.port_b, sla.surf_b) annotation (Line(
      points={{668,-122},{668,-104},{600,-104},{600,-44},{420,-44},{420,-28},
          {426,-28}},
      color={191,0,0},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-300},{820,120}},
          preserveAspectRatio=false),graphics), Icon(coordinateSystem(
          extent={{-100,-300},{820,120}})),
    experiment(
      StopTime=3.17388e+007,
      Interval=360,
      Tolerance=1e-007,
      __Dymola_Algorithm="esdirk23a"),
    __Dymola_experimentSetupOutput);
end SolisArt_confort_final_slab;
