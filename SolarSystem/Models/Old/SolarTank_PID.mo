within SolarSystem.Models.Old;
model SolarTank_PID "Simulation of solar tank recharge and discharge"
extends Modelica.Icons.Example;
  Buildings.Fluid.Movers.FlowMachine_m_flow pump(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    m_flow_start=0.1,
    m_flow_nominal=0.1,
    motorCooledByFluid=true,
    T_start=313.15) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-60,10})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{60,80},{80,100}})));
  Buildings.Fluid.Sensors.Temperature Tpanel_in(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{-70,74},{-50,94}})));
  Buildings.Fluid.Sensors.Temperature Tpanel_out(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{-2,74},{18,94}})));
  Buildings.Fluid.Sensors.Temperature Texch_out(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{50,-40},{70,-60}})));
  Modelica.Blocks.Continuous.Integrator integrator
    annotation (Placement(transformation(extent={{-36,-6},{-16,14}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    use_m_flow_in=true,
    use_T_in=false,
    T=288.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{248,-36},{228,-16}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    V_start(displayUnit="l")=
            0.1,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    T_start=283.15)
    annotation (Placement(transformation(extent={{-96,50},{-76,70}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tinside
    annotation (Placement(transformation(extent={{132,20},{112,40}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(filNam="C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-22,72},{-34,84}})));

  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex tan(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    hTan=1,
    dIns(displayUnit="mm") = 0.2,
    redeclare package MediumHex =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    nSeg=5,
    hexSegMult=2,
    VTan=1.5,
    m_flow_nominal=0.1,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    CHex=200,
    Q_flow_nominal(displayUnit="W") = 8000,
    mHex_flow_nominal=8000/20/4180,
    hexTopHeight=0.9,
    hexBotHeight=0.6,
    dExtHex=0.02,
    T_start=293.15,
    TTan_nominal=293.15,
    THex_nominal=313.15)
    annotation (Placement(transformation(extent={{164,-20},{202,24}})));

  SolarSystemyy.Classes.Control.SolisArt_mod.solarTank_mod solarTank_mod
    annotation (Placement(transformation(extent={{-2,-102},{-28,-76}})));
  Buildings.Rooms.Examples.BESTEST.BaseClasses.DaySchedule instructions(table=[0,
        0.001; 7*3600,0.001; 7*3600,0.003; 14*3600,0.003; 14*3600,0.005; 19
        *3600,0.005; 19*3600,0.001; 24*3600,0.001])
    "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{208,40},{230,62}})));
  SolarSystemyy.Classes.Control.SolisArt_mod.PID_control pID_control(
    yMax=1,
    day_schedule=[0,10],
    k=0.05,
    Ti=120,
    Td=150,
    yMin=0,
    wp=0.5,
    wd=0.5)
    annotation (Placement(transformation(extent={{-48,-96},{-74,-68}})));
  Modelica.Blocks.Math.Gain max_flow(k=0.04) "Gain for cooling"
    annotation (Placement(transformation(extent={{-8,-8},{8,8}},
        rotation=90,
        origin={-80,-40})));
  Buildings.HeatTransfer.Sources.FixedTemperature rooT(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{170,-66},{190,-46}})));
  Buildings.Fluid.SolarCollectors.ASHRAE93 solCol(
    shaCoe=0,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    nSeg=9,
    azi=0,
    nPanels=2,
    per=SolarSystemyy.Data.Parameter.SolarCollector.IDMK2_5_buildings(),
    T_start=313.15,
    lat=0.78539816339745,
    til=0.5235987755983) "Flat plate solar collector model"
    annotation (Placement(transformation(extent={{-36,46},{-16,66}})));

  Buildings.Fluid.Sources.Boundary_pT   boundary(         redeclare package
      Medium =         Modelica.Media.Water.ConstantPropertyLiquidWater, nPorts=1)
    annotation (Placement(transformation(extent={{122,-8},{142,12}})));
  Modelica.Blocks.Sources.BooleanExpression
                                         Max3(y=true)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{28,-112},{18,-96}})));
  Buildings.Rooms.Examples.BESTEST.BaseClasses.DaySchedule instructions1(table=[0,
        0; 7*3600,0; 7*3600,0.04; 10*3600,0.04; 10*3600,0; 12*3600,0.02; 14
        *3600,0.02; 14*3600,0.001; 19*3600,0.001; 19*3600,0.001; 21*3600,
        0.02; 21*3600,0; 24*3600,0])
    "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{216,8},{238,30}})));
  Modelica.Blocks.Math.MultiSum delta_T1(     k={1,-1}, nu=2) "T1 - T7"
    annotation (Placement(transformation(extent={{0,-58},{-14,-44}})));
equation
  connect(pump.m_flow_actual, integrator.u) annotation (Line(
      points={{-65,21},{-65,28},{-42,28},{-42,4},{-38,4}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(Tpanel_out.T, solarTank_mod.T1) annotation (Line(
      points={{15,84},{24,84},{24,-81.4167},{-2,-81.4167}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinside.T, solarTank_mod.T3) annotation (Line(
      points={{112,30},{30,30},{30,-85.75},{-2,-85.75}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pID_control.pumpControl, solarTank_mod.pumpControl_S5) annotation (
      Line(
      points={{-48,-91},{-31,-91},{-31,-91.1667},{-28,-91.1667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(pID_control.value_outer, max_flow.u) annotation (Line(
      points={{-74,-82},{-80,-82},{-80,-49.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(max_flow.y, pump.m_flow_in) annotation (Line(
      points={{-80,-31.2},{-80,9.8},{-72,9.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(tan.heaPorSid, rooT.port) annotation (Line(
      points={{193.64,2},{212,2},{212,-56},{190,-56}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(tan.heaPorTop, rooT.port) annotation (Line(
      points={{186.8,18.28},{186.8,26},{212,26},{212,-56},{190,-56}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(tan.heaPorVol[3], Tinside.port) annotation (Line(
      points={{183,2},{172,2},{172,30},{132,30}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(pump.port_b, solCol.port_a) annotation (Line(
      points={{-60,20},{-60,56},{-36,56}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(solCol.weaBus, Meteo.weaBus) annotation (Line(
      points={{-36,65.6},{-38,65.6},{-38,78},{-34,78}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Tpanel_in.port, solCol.port_a) annotation (Line(
      points={{-60,74},{-48,74},{-48,56},{-36,56}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary.ports[1], tan.port_a) annotation (Line(
      points={{142,2},{164,2}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary1.ports[1], tan.port_b) annotation (Line(
      points={{228,-26},{218,-26},{218,-4},{202,-4},{202,2}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(solCol.port_b, tan.port_a1) annotation (Line(
      points={{-16,56},{154,56},{154,-6.36},{164,-6.36}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_b1, pump.port_a) annotation (Line(
      points={{164,-15.6},{164,-28},{-60,-28},{-60,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(solCol.port_b, Tpanel_out.port) annotation (Line(
      points={{-16,56},{-2,56},{-2,74},{8,74}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_b1, Texch_out.port) annotation (Line(
      points={{164,-15.6},{164,-28},{60,-28},{60,-40}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(pump.port_b, SurgeTank.port_a) annotation (Line(
      points={{-60,20},{-60,34},{-86,34},{-86,50}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Max3.y, solarTank_mod.V3V_solaire) annotation (Line(
      points={{17.5,-104},{8,-104},{8,-96.5833},{-2,-96.5833}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(instructions1.y[1], boundary1.m_flow_in) annotation (Line(
      points={{239.1,19},{254,19},{254,-18},{248,-18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tpanel_out.T, delta_T1.u[1]) annotation (Line(
      points={{15,84},{24,84},{24,-48.55},{-4.44089e-016,-48.55}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinside.T, delta_T1.u[2]) annotation (Line(
      points={{112,30},{30,30},{30,-53.45},{-4.44089e-016,-53.45}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(delta_T1.y, pID_control.signal) annotation (Line(
      points={{-15.19,-51},{-30.595,-51},{-30.595,-73},{-48,-73}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-120},{260,120}},
          preserveAspectRatio=true), graphics), Icon(coordinateSystem(
          extent={{-100,-120},{260,120}})),
    experiment(StopTime=3.1536e+007, Interval=240));
end SolarTank_PID;
