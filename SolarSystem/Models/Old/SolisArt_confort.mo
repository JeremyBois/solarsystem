within SolarSystem.Models.Old;
model SolisArt_confort "Simulation of solar tank recharge and discharge"
extends Modelica.Icons.Example;

  // Mediums
  package MediumA = Buildings.Media.GasesConstantDensity.SimpleAir
    "Medium air model";
  package MediumE = Buildings.Media.ConstantPropertyLiquidWater
    "Medium water model";

  // Rooms simplifications
  parameter Modelica.SIunits.Angle S_=
    Buildings.HeatTransfer.Types.Azimuth.S "Azimuth for south walls";
  parameter Modelica.SIunits.Angle E_=
    Buildings.HeatTransfer.Types.Azimuth.E "Azimuth for east walls";
  parameter Modelica.SIunits.Angle W_=
    Buildings.HeatTransfer.Types.Azimuth.W "Azimuth for west walls";
  parameter Modelica.SIunits.Angle N_=
    Buildings.HeatTransfer.Types.Azimuth.N "Azimuth for north walls";
  parameter Modelica.SIunits.Angle C_=
    Buildings.HeatTransfer.Types.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    Buildings.HeatTransfer.Types.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle Z_=
    Buildings.HeatTransfer.Types.Tilt.Wall "Tilt for wall";
  parameter Integer nConExtWin = 2 "Number of constructions with a window";
  parameter Integer nConBou = 1
    "Number of surface that are connected to constructions that are modeled inside the room";

  parameter Modelica.SIunits.Pressure dpPip_nominal = 1000
    "Pressure difference of pipe (without valve)";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.04
    "Mass flow rate for pump S6 S5 and splitter";
  parameter Modelica.Blocks.Interfaces.RealOutput flow_mini=0.0005
    "Value of Real output";
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{722,-260},{762,-220}})));

  SolarSystemyy.Classes.Control.Horloge horloge(Heure_debut=0)
    annotation (Placement(transformation(extent={{780,-260},{820,-220}})));

   parameter Buildings.HeatTransfer.Data.Solids.Generic soil(
    k=1.9,
    c=790,
    d=1900,
    x=3) "Soil properties"
    annotation (Placement(transformation(extent={{706,-8},{726,12}})));
  Buildings.Rooms.MixedAir roo(
    redeclare package Medium = MediumA,
    hRoo=2.7,
    nConExtWin=nConExtWin,
    nPorts=3,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    nConPar=0,
    nSurBou=0,
    linearizeRadiation=false,
    nConExt=3,
    datConBou(
      layers={Floor},
      each A=67.5,
      each til=F_),
    AFlo=67.5,
    datConExt(
      layers={Roof,Wall,Wall},
      A={67.5,19.2,19.2},
      til={C_,Z_,Z_},
      azi={S_,E_,S_}),
    datConExtWin(
      layers={Wall,Wall},
      each A=19.2,
      glaSys={GlaSys_Argon,GlaSys_Argon},
      each wWin=4,
      each hWin=2,
      each fFra=0.1,
      each til=Z_,
      azi={W_,N_}),
    nConBou=nConBou,
    lat=0.78539816339745) "Room model for Case 600"
    annotation (Placement(transformation(extent={{646,-100},{676,-70}})));
  Modelica.Blocks.Sources.Constant qConGai_flow(k=80/48) "Convective heat gain"
    annotation (Placement(transformation(extent={{482,42},{500,60}})));
  Modelica.Blocks.Sources.Constant qRadGai_flow(k=120/48) "Radiative heat gain"
    annotation (Placement(transformation(extent={{520,60},{540,80}})));
  Modelica.Blocks.Routing.Multiplex3 multiplex3_1
    annotation (Placement(transformation(extent={{560,42},{578,60}})));
  Modelica.Blocks.Sources.Constant qLatGai_flow(k=0) "Latent heat gain"
    annotation (Placement(transformation(extent={{520,20},{540,40}})));
  Modelica.Blocks.Sources.Constant uSha(k=0)
    "Control signal for the shading device"
    annotation (Placement(transformation(extent={{500,100},{520,120}})));
  Modelica.Blocks.Routing.Replicator replicator(nout=max(1,nConExtWin))
    annotation (Placement(transformation(extent={{562,100},{580,120}})));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature TSoi[nConBou](each T=
        283.15) "Boundary condition for construction"
                                          annotation (Placement(transformation(
        extent={{0,0},{-24,24}},
        rotation=0,
        origin={736,-146})));
  Buildings.HeatTransfer.Conduction.SingleLayer soi(
    A=48,
    material=soil,
    steadyStateInitial=true,
    T_a_start=283.15,
    T_b_start=283.75) "2m deep soil (per definition on p.4 of ASHRAE 140-2007)"
    annotation (Placement(transformation(
        extent={{5,-5},{-3,3}},
        rotation=-90,
        origin={669,-105})));
  Buildings.Fluid.Sources.MassFlowSource_T sinInf(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{614,-184},{630,-168}})));
  Buildings.Fluid.Sources.Outside souInf(redeclare package Medium = MediumA,
      nPorts=1) "Source model for air infiltration"
           annotation (Placement(transformation(extent={{598,-106},{610,-94}})));
  Modelica.Blocks.Sources.Constant InfiltrationRate(k=-48*2.7*0.5/3600)
    "0.41 ACH adjusted for the altitude (0.5 at sea level)"
    annotation (Placement(transformation(extent={{480,-220},{502,-198}})));
  Modelica.Blocks.Math.Product product
    annotation (Placement(transformation(extent={{554,-170},{572,-152}})));
  Buildings.Fluid.Sensors.Density density(redeclare package Medium = MediumA)
    "Air density inside the building"
    annotation (Placement(transformation(extent={{608,-204},{590,-188}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TRooAir
    "Room air temperature"
    annotation (Placement(transformation(extent={{728,-58},{744,-42}})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM   heaCoo(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    m_flow_nominal=48*2.7*0.41/3600*1.2,
    dp_nominal=1,
    linearized=true,
    from_dp=true) "Heater and cooler"
    annotation (Placement(transformation(extent={{622,-106},{634,-94}})));
  Modelica.Blocks.Math.MultiSum multiSum(nu=1)
    annotation (Placement(transformation(extent={{520,-164},{536,-148}})));
  Modelica.Blocks.Math.Mean TRooHou(f=1/3600, y(start=293.15))
    "Hourly averaged room air temperature"
    annotation (Placement(transformation(extent={{780,-20},{800,0}})));
  Modelica.Blocks.Math.Mean TRooAnn(f=1/86400/365, y(start=293.15))
    "Annual averaged room air temperature"
    annotation (Placement(transformation(extent={{780,-100},{800,-80}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Wall(
    nLay=4,
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    material={Models.Data.WoodPanel(x=0.04),Models.Data.GlassWool(x=0.08),Models.Data.Air(),
        Models.Data.Wood(x=0.04)}) "Wall layers"
    annotation (Placement(transformation(extent={{660,58},{688,86}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Roof(material={Models.Data.Wood(x=
        0.1),Models.Data.GlassWool(x=0.1)}, nLay=2) "Roof layers"
    annotation (Placement(transformation(extent={{700,98},{728,126}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Floor(nLay=2, material={
        Models.Data.GlassWool(x=0.05),Buildings.HeatTransfer.Data.Solids.Concrete(x=0.3)})
    "Floor layer" annotation (Placement(transformation(extent={{702,20},{730,48}})));
  Data.DoubleClearArgon16 GlaSys_Argon
    annotation (Placement(transformation(extent={{740,58},{768,86}})));
  Modelica.Blocks.Math.Mean TRooDay(y(start=293.15), f=1/3600/24)
    "Month averaged room air temperature"
    annotation (Placement(transformation(extent={{780,-60},{800,-40}})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C6(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    m_flow(start=0.04),
    T_start=283.15,
    m_flow_nominal=m_flow_nominal)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={56,-74})));
  Buildings.Fluid.Sources.Boundary_pT   boundary(         redeclare package
      Medium =         MediumE,
      nPorts=1)
    annotation (Placement(transformation(extent={{4,0},{16,12}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        MediumE,
    use_m_flow_in=false,
    use_T_in=true,
    m_flow=0.000001,
    nPorts=1)
    annotation (Placement(transformation(extent={{162,-44},{150,-32}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    V_start(displayUnit="l")=
            0.2,
    redeclare package Medium = MediumE,
    T_start=323.15)
    annotation (Placement(transformation(extent={{-64,-62},{-26,-24}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T5
    annotation (Placement(transformation(extent={{74,22},{60,36}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(filNam="C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos",
      computeWetBulbTemperature=false)
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-24,94},{-44,112}})));
  Buildings.Fluid.SolarCollectors.EN12975 solarPanel_ISO(
    redeclare package Medium = MediumE,
    per=SolarSystemyy.Data.Parameter.SolarCollector.IDMK2_5_buildings(),
    azi=0,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    use_shaCoe_in=false,
    m_flow(start=0.04),
    dp(start=100),
    nSeg=40,
    nPanels=5,
    lat=0.78539816339745,
    til=0.5235987755983)
    annotation (Placement(transformation(extent={{-72,40},{-28,88}})));

  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex Strorage_tank(
    redeclare package Medium =
        MediumE,
    hTan=1,
    redeclare package MediumHex =
        MediumE,
    VTan=1.5,
    dExtHex=0.02,
    hexTopHeight=0.9,
    CHex=200,
    hexBotHeight=0.1,
    Q_flow_nominal(displayUnit="W") = 8000,
    mHex_flow_nominal=8000/20/4180,
    hexSegMult=3,
    nSeg=20,
    m_flow_nominal=0.000001,
    dIns(displayUnit="mm") = 0.2,
    kIns=0.04,
    TTan_nominal=293.15,
    THex_nominal=323.15)
    annotation (Placement(transformation(extent={{70,-34},{108,10}})));
  Buildings.HeatTransfer.Sources.FixedTemperature Troom1(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{-9,-9},{9,9}},
        rotation=90,
        origin={127,-51})));
  SolarSystemyy.Classes.Exchanger_and_Tank.StratifiedEnhancedInternalHex_double
    Solar_tank(
    redeclare package Medium = MediumE,
    hTan=1,
    redeclare package MediumHex = MediumE,
    VTan=1.5,
    nSeg=20,
    dIns(displayUnit="mm") = 0.0002,
    hexTopHeight={0.9,0.5},
    hexBotHeight={0.6,0.1},
    hexSegMult={2,2},
    CHex={200,200},
    Q_flow_nominal(displayUnit="W") = {8000,8000},
    mHex_flow_nominal={8000/20/4180,8000/20/4180},
    dExtHex={0.02,0.02},
    m_flow_nominal=m_flow_nominal,
    TTan_nominal={566.3,566.3},
    THex_nominal={596.3,596.3})
    annotation (Placement(transformation(extent={{184,-44},{236,14}})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C5(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    T_start=283.15,
    m_flow_nominal=m_flow_nominal)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={174,-68})));
  Buildings.HeatTransfer.Sources.FixedTemperature Troom2(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{-9,-9},{9,9}},
        rotation=90,
        origin={241,-71})));
  Buildings.Fluid.Sources.FixedBoundary Cool_water(redeclare package Medium =
        MediumE,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{11,-10},{-11,10}},
        rotation=-90,
        origin={256,-97})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T3
    annotation (Placement(transformation(extent={{200,14},{214,28}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T4
    annotation (Placement(transformation(extent={{200,38},{214,52}})));
  Buildings.Fluid.Sensors.MassFlowRate flowBetween_tanks(redeclare package
      Medium = MediumE)
    annotation (Placement(transformation(extent={{130,58},{142,70}})));
  Buildings.Fluid.Sensors.MassFlowRate inFlowTop_Storage(redeclare package
      Medium = MediumE)                                   annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={52,44})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{-1,1,-1}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={52,64})));
  SolarSystemyy.Classes.Other.Water_Drawing_up_control Drawing_up1(
    T=5,
    k=0.8,
    table=[0,0,0; 5.99,0,0; 6,200,0.04; 8.99,200,0.04; 9,100,0.05; 9.99,100,
        0.05; 10,0,0; 10.99,0,0; 11,0,0; 11.99,0,0; 12,200,0.07; 13.49,200,
        0.07; 13.5,0,0; 14.49,0,0; 14.5,0,0; 15.49,0,0; 15.5,0,0; 16.49,0,0;
        16.5,0,0; 17.49,0,0; 17.5,0,0; 17.99,0,0; 18,0,0; 18.99,0,0; 19,150,
        0.05; 21,150,0.05; 21.01,0,0; 24,0,0],
    redeclare package Medium = MediumE)
    annotation (Placement(transformation(extent={{136,22},{176,52}})));
  Buildings.Fluid.Sensors.MassFlowRate outFlow_panels(redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{12,56},{28,72}})));
  Buildings.Fluid.Sensors.MassFlowRate inFlow_panels(redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-84,-12})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal1(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{1,-1,1}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={68,-120})));
  Buildings.Fluid.HeatExchangers.Radiators.RadiatorEN442_2 rad(
    redeclare package Medium =
        MediumE,
    nEle=20,
    Q_flow_nominal=5000,
    T_a_nominal=313.15,
    T_b_nominal=303.15) annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=-90,
        origin={422,-22})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C2(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    T_start=283.15,
    m_flow_nominal=m_flow_nominal)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={400,-66})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal2(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{1,-1,1}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={182,-120})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T7(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{294,-130},{314,-110}})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal3(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{1,-1,1}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={362,-120})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C4(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    T_start=283.15,
    m_flow_nominal=m_flow_nominal)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={322,-58})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T8(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{324,54},{344,74}})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal4(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=m_flow_nominal*{-1,1,-1},
    dp_nominal={dpPip_nominal,0,dpPip_nominal}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={362,64})));
  Buildings.Fluid.Sensors.MassFlowRate inFlow_topExtra(
                                                      redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={362,34})));
  Buildings.Fluid.Sensors.MassFlowRate inFlow_heating(redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={402,32})));
  Buildings.Fluid.Sensors.MassFlowRate outFlow_heating(redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=0,
        origin={344,-120})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM spl(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{-1,1,-1})
    annotation (Placement(transformation(extent={{266,58},{278,70}})));
  Buildings.Fluid.Sensors.MassFlowRate inFlow_backup(redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={272,-36})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal5(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=m_flow_nominal*{-1,1,-1},
    dp_nominal={dpPip_nominal,0,dpPip_nominal}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={182,64})));
  Buildings.Fluid.Sensors.MassFlowRate flow_between_tankAndBackup(redeclare
      package Medium = MediumE)
    annotation (Placement(transformation(extent={{232,58},{244,70}})));
  SolarSystemyy.Classes.Control.SolisArt_mod.Algo_PID_Deprecated algo_all(
    n=1,
    Sj_onTime={240},
    Tambiant_out_start={18})
    annotation (Placement(transformation(extent={{106,-258},{204,-144}})));
  Buildings.Fluid.Sensors.MassFlowRate inFlowLow_storage(redeclare package
      Medium = MediumE)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={16,-76})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T2(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{52,-130},{32,-110}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T1(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{-20,54},{0,74}})));
  Buildings.Fluid.Boilers.BoilerPolynomial boi(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=0.04,
    Q_flow_nominal=20000,
    fue=Buildings.Fluid.Data.Fuels.NaturalGasHigherHeatingValue(),
    dp_nominal=1000)                                               annotation (
      Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={270,36})));
  Buildings.HeatTransfer.Sources.FixedTemperature Troom3(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{232,28},{248,44}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T10(redeclare package Medium =
        MediumE, m_flow_nominal=0.04)
    annotation (Placement(transformation(extent={{-8,-8},{8,8}},
        rotation=90,
        origin={160,0})));
  Buildings.Fluid.Actuators.Valves.ThreeWayLinear solar_valve(
    fraK=0.5,
    l={0.001,0.001},
    redeclare package Medium = MediumE,
    dpValve_nominal=6000,
    m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{-20,-110},{0,-130}})));
  Buildings.Fluid.Actuators.Valves.ThreeWayLinear solar_valve1(
    fraK=0.5,
    l={0.001,0.001},
    redeclare package Medium = MediumE,
    dpValve_nominal=6000,
    m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{262,-110},{282,-130}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{28,-268},{54,-242}})));
equation

  connect(qRadGai_flow.y,multiplex3_1. u1[1])  annotation (Line(
      points={{541,70},{550,70},{550,57.3},{558.2,57.3}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qLatGai_flow.y,multiplex3_1. u3[1])  annotation (Line(
      points={{541,30},{550,30},{550,44.7},{558.2,44.7}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiplex3_1.y,roo. qGai_flow) annotation (Line(
      points={{578.9,51},{632,51},{632,-77.5},{640,-77.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.uSha,replicator. y) annotation (Line(
      points={{644.5,-73},{636,-73},{636,110},{580.9,110}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qConGai_flow.y,multiplex3_1. u2[1]) annotation (Line(
      points={{500.9,51},{558.2,51}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(uSha.y,replicator. u) annotation (Line(
      points={{521,110},{560.2,110}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(product.y,sinInf. m_flow_in)       annotation (Line(
      points={{572.9,-161},{604,-161},{604,-169.6},{614,-169.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(density.port,roo. ports[1])  annotation (Line(
      points={{599,-204},{644,-204},{644,-92.5},{647.75,-92.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(density.d,product. u2) annotation (Line(
      points={{589.1,-196},{540,-196},{540,-166.4},{552.2,-166.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TSoi[1].port,soi. port_a) annotation (Line(
      points={{712,-134},{668,-134},{668,-110}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(soi.port_b,roo. surf_conBou[1]) annotation (Line(
      points={{668,-102},{668,-97},{665.5,-97}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(roo.heaPorAir,TRooAir. port)  annotation (Line(
      points={{660.25,-85},{712,-85},{712,-50},{728,-50}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(souInf.ports[1],heaCoo. port_a)        annotation (Line(
      points={{610,-100},{622,-100}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(heaCoo.port_b,roo. ports[2])  annotation (Line(
      points={{634,-100},{644,-100},{644,-92.5},{649.75,-92.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(sinInf.ports[1],roo. ports[3])        annotation (Line(
      points={{630,-176},{644,-176},{644,-92.5},{651.75,-92.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(multiSum.y,product. u1) annotation (Line(
      points={{537.36,-156},{552.2,-156},{552.2,-155.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(InfiltrationRate.y,multiSum. u[1]) annotation (Line(
      points={{503.1,-209},{520,-209},{520,-156}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T,TRooHou. u) annotation (Line(
      points={{744,-50},{752,-50},{752,-10},{778,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T,TRooAnn. u) annotation (Line(
      points={{744,-50},{752,-50},{752,-90},{778,-90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooDay.u) annotation (Line(
      points={{744,-50},{778,-50}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(Meteo.weaBus,solarPanel_ISO. weaBus) annotation (Line(
      points={{-44,103},{-72,103},{-72,87.04}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(boundary1.ports[1],Strorage_tank. port_b) annotation (Line(
      points={{150,-38},{144,-38},{144,-12},{108,-12}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Strorage_tank.port_a,boundary. ports[1]) annotation (Line(
      points={{70,-12},{36,-12},{36,6},{16,6}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Strorage_tank.heaPorSid,Troom1. port) annotation (Line(
      points={{99.64,-12},{104,-12},{104,-6},{118,-6},{118,-16},{127,-16},{127,-42}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Strorage_tank.heaPorTop,Troom1. port) annotation (Line(
      points={{92.8,4.28},{92.8,10},{108,10},{108,-6},{118,-6},{118,-16},{
          127,-16},{127,-42}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Strorage_tank.port_b1,C6. port_a) annotation (Line(
      points={{70,-29.6},{70,-30},{56,-30},{56,-64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.port_b1,C5. port_a) annotation (Line(
      points={{184,-34.14},{184,-34},{174,-34},{174,-58}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.heaPorSid,Troom2. port) annotation (Line(
      points={{224.56,-15},{224,-15},{224,-20},{241,-20},{241,-62}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Solar_tank.heaPorTop,Troom2. port) annotation (Line(
      points={{215.2,6.46},{215.2,14},{248,14},{248,-10},{241,-10},{241,-62}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(splitVal.port_1,flowBetween_tanks. port_a)
                                              annotation (Line(
      points={{58,64},{130,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal.port_3,inFlowTop_Storage. port_a)
                                                annotation (Line(
      points={{52,58},{52,50}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlowTop_Storage.port_b,Strorage_tank. port_a1)
                                                      annotation (Line(
      points={{52,38},{52,2},{64,2},{64,-20.36},{70,-20.36}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(outFlow_panels.port_b,splitVal. port_2)      annotation (Line(
      points={{28,64},{46,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_panels.port_b,solarPanel_ISO. port_a)        annotation (
      Line(
      points={{-84,-6},{-84,64},{-72,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.heaPorVol[2],T4. port) annotation (Line(
      points={{210,-16.479},{194,-16.479},{194,45},{200,45}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Cool_water.ports[1],Solar_tank. port_b) annotation (Line(
      points={{256,-86},{256,-14},{236,-14},{236,-15}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.heaPorVol[18],T3. port) annotation (Line(
      points={{210,-13.695},{210,-14},{194,-14},{194,21},{200,21}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Strorage_tank.heaPorVol[4],T5. port) annotation (Line(
      points={{89,-12.858},{80,-12.858},{80,29},{74,29}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(C6.port_b,splitVal1. port_3) annotation (Line(
      points={{56,-84},{56,-102},{68,-102},{68,-114}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C5.port_b,splitVal2. port_3) annotation (Line(
      points={{174,-78},{174,-94},{182,-94},{182,-114}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal2.port_2,splitVal1. port_1) annotation (Line(
      points={{176,-120},{74,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C2.port_b,splitVal3. port_1) annotation (Line(
      points={{400,-76},{400,-120},{368,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(rad.port_b,C2. port_a) annotation (Line(
      points={{422,-36},{422,-42},{400,-42},{400,-56}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C4.port_b,splitVal3. port_3) annotation (Line(
      points={{322,-68},{322,-94},{362,-94},{362,-114}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T8.port_b,splitVal4. port_2) annotation (Line(
      points={{344,64},{356,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.port_b2,C4. port_a) annotation (Line(
      points={{236,-8.62},{284,-8.62},{284,-18},{322,-18},{322,-48}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal4.port_3,inFlow_topExtra. port_a) annotation (Line(
      points={{362,58},{362,40}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_heating.port_b,rad. port_a)  annotation (Line(
      points={{402,26},{402,-2},{422,-2},{422,-8}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal4.port_1,inFlow_heating. port_a)  annotation (Line(
      points={{368,64},{402,64},{402,38}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_topExtra.port_b,Solar_tank. port_a2) annotation (Line(
      points={{362,28},{362,-0.5},{236,-0.5}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal3.port_2,outFlow_heating. port_a)  annotation (Line(
      points={{356,-120},{350,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(outFlow_heating.port_b,T7. port_b)  annotation (Line(
      points={{338,-120},{314,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(spl.port_2,T8. port_a) annotation (Line(
      points={{278,64},{324,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flowBetween_tanks.port_b,splitVal5. port_2)
                                                   annotation (Line(
      points={{142,64},{176,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flow_between_tankAndBackup.port_b,spl. port_1)
                                              annotation (Line(
      points={{244,64},{266,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal5.port_1,flow_between_tankAndBackup. port_a)
                                                    annotation (Line(
      points={{188,64},{232,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlowLow_storage.port_b,Strorage_tank. port_b1)    annotation (Line(
      points={{16,-70},{16,-29.6},{70,-29.6}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal1.port_2,T2. port_a) annotation (Line(
      points={{62,-120},{52,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solarPanel_ISO.port_b,T1. port_a) annotation (Line(
      points={{-28,64},{-20,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T1.port_b,outFlow_panels. port_a)      annotation (Line(
      points={{0,64},{12,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_backup.port_b,boi. port_a)   annotation (Line(
      points={{272,-30},{272,22},{270,22}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(boi.port_b,spl. port_3) annotation (Line(
      points={{270,50},{270,54},{272,54},{272,58}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Troom3.port,boi. heatPort) annotation (Line(
      points={{248,36},{259.92,36}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(T1.T,algo_all. T1) annotation (Line(
      points={{-10,75},{-10,118},{-98,118},{-98,-150.243},{106.721,-150.243}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T3.T,algo_all. T3) annotation (Line(
      points={{214,21},{222,21},{222,118},{-98,118},{-98,-161.1},{106.721,
          -161.1}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T4.T,algo_all. T4) annotation (Line(
      points={{214,45},{222,45},{222,118},{-98,118},{-98,-172.5},{106.721,
          -172.5}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T5.T,algo_all. T5) annotation (Line(
      points={{60,29},{38,29},{38,118},{-98,118},{-98,-184.171},{106.865,
          -184.171}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T7.T,algo_all. T7) annotation (Line(
      points={{304,-109},{304,118},{-98,118},{-98,-196.114},{107.153,
          -196.114}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T8.T,algo_all. T8) annotation (Line(
      points={{334,75},{334,118},{-98,118},{-98,-207.514},{107.153,-207.514}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Solar_tank.port_a,T10. port_a) annotation (Line(
      points={{184,-15},{160,-15},{160,-8}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T10.port_b,Drawing_up1. port_a) annotation (Line(
      points={{160,8},{156,8},{156,22.1875}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T10.T,algo_all. T10) annotation (Line(
      points={{151.2,5.38845e-016},{132,5.38845e-016},{132,10},{124,10},{
          124,118},{-98,118},{-98,-208},{-76,-208},{-76,-219.457},{107.153,
          -219.457}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Meteo.weaBus,weaBus)  annotation (Line(
      points={{-44,103},{-92,103},{-92,-255},{41,-255}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(weaBus.TDryBul,algo_all. Text) annotation (Line(
      points={{41,-255},{72.5,-255},{72.5,-250.943},{107.441,-250.943}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(C4.P,algo_all. flow_S4) annotation (Line(
      points={{330,-69},{330,-86},{304,-86},{304,118},{-98,118},{-98,-142},
          {124,-142},{124,-144.814},{123.438,-144.814}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T2.port_b,solar_valve. port_2) annotation (Line(
      points={{32,-120},{0,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_3,inFlowLow_storage. port_a)    annotation (Line(
      points={{-10,-110},{-10,-102},{16,-102},{16,-82}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_1,inFlow_panels. port_a)        annotation (Line(
      points={{-20,-120},{-84,-120},{-84,-18}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_1,SurgeTank. port_a) annotation (Line(
      points={{-20,-120},{-45,-120},{-45,-62}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T7.port_a,solar_valve1. port_2) annotation (Line(
      points={{294,-120},{282,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve1.port_3,inFlow_backup. port_a)   annotation (Line(
      points={{272,-110},{272,-42}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve1.port_1,splitVal2. port_1) annotation (Line(
      points={{262,-120},{188,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(algo_all.BackupHeater_outter,boi. y) annotation (Line(
      points={{204.288,-252.571},{380,-252.571},{380,10},{258.8,10},{258.8,
          19.2}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo_all.V3V_solar_outter,solar_valve. y) annotation (Line(
      points={{204.288,-220},{216,-220},{216,-138},{-10,-138},{-10,-132}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo_all.V3V_extra_outter,solar_valve1. y) annotation (Line(
      points={{204.576,-236.286},{272,-236.286},{272,-132}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T5.T,boundary1. T_in) annotation (Line(
      points={{60,29},{58,29},{58,14},{116,14},{116,6},{124,6},{124,-6},{154,-6},
          {154,-18},{168,-18},{168,-35.6},{163.2,-35.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(splitVal5.port_3,Solar_tank. port_a1) annotation (Line(
      points={{182,58},{182,0},{176,0},{176,-26.02},{184,-26.02}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Meteo.weaBus, roo.weaBus) annotation (Line(
      points={{-44,103},{-92,103},{-92,-300},{680,-300},{680,-60},{674.425,-60},
          {674.425,-71.575}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Meteo.weaBus, souInf.weaBus) annotation (Line(
      points={{-44,103},{-92,103},{-92,-300},{680,-300},{680,-144},{570,-144},{570,
          -99.88},{598,-99.88}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(rad.heatPortCon, roo.heaPorAir) annotation (Line(
      points={{432.08,-19.2},{660,-19.2},{660,-86},{660.25,-85}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=1));
  connect(rad.heatPortRad, roo.heaPorRad) annotation (Line(
      points={{432.08,-24.8},{480,-24.8},{480,-88},{660.25,-88},{660.25,
          -87.85}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=1));

  connect(TRooAir.T, algo_all.Tambiant[1]) annotation (Line(
      points={{744,-50},{754,-50},{754,-200},{698,-200},{698,-280},{-80,
          -280},{-80,-240},{78,-240},{78,-236.014},{106.432,-236.014}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo_all.S4_outter, C4.m_flow_in) annotation (Line(
      points={{204.288,-149.429},{380,-149.429},{380,-57.8},{334,-57.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(algo_all.S5_outter, C5.m_flow_in) annotation (Line(
      points={{204.288,-165.714},{216,-165.714},{216,-67.8},{186,-67.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(algo_all.S6_outter, C6.m_flow_in) annotation (Line(
      points={{204.288,-182},{216,-182},{216,-84},{90,-84},{90,-73.8},{68,
          -73.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(algo_all.Sj_outter[1], C2.m_flow_in) annotation (Line(
      points={{204.288,-198.286},{422,-198.286},{422,-65.8},{412,-65.8}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-300},{820,120}},
          preserveAspectRatio=true), graphics), Icon(coordinateSystem(
          extent={{-100,-300},{820,120}})),
    experiment(
      StopTime=3.1536e+007,
      Interval=120,
      Tolerance=1e-007,
      Algorithm="Esdirk23a"),
    __Dymola_experimentSetupOutput);
end SolisArt_confort;
