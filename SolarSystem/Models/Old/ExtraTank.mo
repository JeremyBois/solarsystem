within SolarSystem.Models.Old;
model ExtraTank
  "Simulation of extra tank recharge and discharge (2 exchangers)"
extends Modelica.Icons.Example;
  Buildings.Fluid.FixedResistances.LosslessPipe pip(
    m_flow_nominal=0.1,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    allowFlowReversal=false,
    m_flow(start=scheduled_solarPanel_control3_1.flowElse))
    annotation (Placement(transformation(extent={{38,66},{58,46}})));
  Buildings.Fluid.FixedResistances.LosslessPipe pip1(
    m_flow_nominal=0.1,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    allowFlowReversal=false,
    m_flow(start=scheduled_solarPanel_control3_1.flowElse))
    annotation (Placement(transformation(extent={{14,-44},{-6,-24}})));
  Buildings.Fluid.Movers.FlowMachine_m_flow pump(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    allowFlowReversal=false,
    motorCooledByFluid=false,
    m_flow(start=scheduled_solarPanel_control3_1.flowElse),
    T_start=283.15,
    m_flow_nominal=0.03)
                    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-60,10})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{60,80},{80,100}})));
  SolarSystemyy.Classes.Control.Scheduled_solarPanel_control_noSample
    scheduled_solarPanel_control3_1(
    minGlobalRadiation=40,
    flowElse=0,
    flowRate(start=0.1),
    table=[0,0.1; 5.99,0.1; 6,0.1; 8.99,0.1; 9,0.1; 9.99,0.1; 10,0.1; 10.99,
        0.1; 11,0.1; 11.99,0.1; 12,0.1; 13.49,0.1; 13.5,0.1; 14.49,0.1; 14.5,
        0.1; 15.49,0.1; 15.5,0.1; 16.49,0.1; 16.5,0.1; 17.49,0.1; 17.5,0.1;
        17.99,0.1; 18,0.1; 18.99,0.1; 19,0.1; 19.99,0.1; 20,0.1; 21,0.1;
        21.01,0.1; 24,0.1],
    hysteresisHight=4)
    annotation (Placement(transformation(extent={{-2,-102},{-60,-58}})));
  Buildings.Fluid.Sensors.Temperature Tpanel_in(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{-86,48},{-66,68}})));
  Buildings.Fluid.Sensors.Temperature Tpanel_out(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{-2,74},{18,94}})));
  Buildings.Fluid.Sensors.Temperature Texch_out(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{50,-40},{70,-60}})));
  Modelica.Blocks.Continuous.Integrator integrator
    annotation (Placement(transformation(extent={{-36,-6},{-16,14}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    V_start(displayUnit="l")=
            0.1,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    T_start=283.15)
    annotation (Placement(transformation(extent={{100,82},{120,102}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tinside
    annotation (Placement(transformation(extent={{132,20},{112,40}})));
  SolarSystemyy.BaseClasses.DirectTiltedSurface RadiationDir(
    azi(displayUnit="deg") = 3.1415926535898,
    til=0.5235987755983,
    lat(displayUnit="deg") = 0.78539816339745) "Direct radiation on the panel"
    annotation (Placement(transformation(extent={{118,-80},{94,-62}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(filNam="C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{172,-86},{160,-74}})));
public
  SolarSystemyy.BaseClasses.DiffuseIsotropic RadiationInd(
    outSkyCon=false,
    outGroCon=false,
    til=0.5235987755983) "Indirect radiation on the panel"
    annotation (Placement(transformation(extent={{118,-110},{94,-90}})));
  Buildings.Fluid.SolarCollectors.EN12975 solarPanel_ISO(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    azi=0,
    rho=0.2,
    per=SolarSystemyy.Data.Parameter.SolarCollector.IDMK2_5_buildings(),
    nSeg=40,
    lat=0.78539816339745,
    til=0.5235987755983,
    nPanels=5)
    annotation (Placement(transformation(extent={{-46,46},{-8,80}})));

  Buildings.Fluid.Sources.Boundary_pT boundary(      nPorts=1, redeclare
      package Medium = Modelica.Media.Water.ConstantPropertyLiquidWater)
                                                               annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={348,12})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senTem( m_flow_nominal=0.1,
      redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{298,2},{318,22}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=senTem.T)
    annotation (Placement(transformation(extent={{154,56},{174,76}})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary1(
    use_T_in=true,
    m_flow=0,
    nPorts=1,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{184,42},{204,62}})));
  Buildings.Fluid.Sources.Boundary_pT boundary5(
                       nPorts=1, redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)   annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={332,34})));
  SolarSystemyy.Classes.Exchanger_and_Tank.StratifiedEnhancedInternalHex_double
    tan(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    hTan=1,
    dIns(displayUnit="mm") = 0.2,
    nSeg=20,
    redeclare package MediumHex =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    hexTopHeight={0.9,0.4},
    hexBotHeight={0.5,0.1},
    hexSegMult={1,1},
    CHex={40,40},
    Q_flow_nominal={0.278*4200*20,0.278*4200*20},
    mHex_flow_nominal={0.278,0.278},
    m_flow_nominal=0.001,
    VTan=0.7,
    TTan_nominal={293.15,293.15},
    THex_nominal={323.15,323.15})
    annotation (Placement(transformation(extent={{212,-16},{288,60}})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary6(
    nPorts=1,
    use_T_in=false,
    m_flow=0.278,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    T=293.15)
    annotation (Placement(transformation(extent={{342,54},{322,74}})));
  SolarSystemyy.Classes.Control.Horloge horloge(Heure_debut=0)
    annotation (Placement(transformation(extent={{292,-102},{332,-62}})));
equation
  connect(scheduled_solarPanel_control3_1.flowRate, pump.m_flow_in)
    annotation (Line(
      points={{-58.6,-80.8},{-92,-80.8},{-92,9.8},{-72,9.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pip1.port_a, Texch_out.port) annotation (Line(
      points={{14,-34},{60,-34},{60,-40}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(pip1.port_b, pump.port_a) annotation (Line(
      points={{-6,-34},{-60,-34},{-60,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(pump.m_flow_actual, integrator.u) annotation (Line(
      points={{-65,21},{-65,28},{-42,28},{-42,4},{-38,4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(SurgeTank.port_a, pip.port_b) annotation (Line(
      points={{110,82},{110,56},{58,56}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tinside.T, scheduled_solarPanel_control3_1.T_out) annotation (
      Line(
      points={{112,30},{26,30},{26,-95.2},{-5.2,-95.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Meteo.weaBus,RadiationDir. weaBus) annotation (Line(
      points={{160,-80},{126,-80},{126,-71},{118,-71}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Meteo.weaBus,RadiationInd. weaBus) annotation (Line(
      points={{160,-80},{126,-80},{126,-100},{118,-100}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(RadiationDir.H, scheduled_solarPanel_control3_1.dirRad) annotation (
      Line(
      points={{92.8,-71},{40,-71},{40,-63.1},{-5.3,-63.1}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(RadiationInd.H, scheduled_solarPanel_control3_1.difRad) annotation (
      Line(
      points={{92.8,-100},{36,-100},{36,-71.5},{-5.3,-71.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarPanel_ISO.port_b, pip.port_a) annotation (Line(
      points={{-8,63},{16,63},{16,56},{38,56}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(pump.port_b, solarPanel_ISO.port_a) annotation (Line(
      points={{-60,20},{-60,63},{-46,63}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Meteo.weaBus, solarPanel_ISO.weaBus) annotation (Line(
      points={{160,-80},{126,-80},{126,-114},{-94,-114},{-94,79.32},{-46,
          79.32}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));

  connect(Tpanel_in.port, solarPanel_ISO.port_a) annotation (Line(
      points={{-76,48},{-62,48},{-62,72},{-46,72},{-46,63}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tpanel_out.port, solarPanel_ISO.port_b) annotation (Line(
      points={{8,74},{0,74},{0,63},{-8,63}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(senTem.port_b,boundary. ports[1]) annotation (Line(
      points={{318,12},{338,12}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(realExpression.y,boundary1. T_in) annotation (Line(
      points={{175,66},{176,66},{176,56},{182,56}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(tan.port_b2,boundary5. ports[1]) annotation (Line(
      points={{288,30.36},{303,30.36},{303,34},{322,34}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_b,senTem. port_a) annotation (Line(
      points={{288,22},{292,22},{292,12},{298,12}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_a,boundary1. ports[1]) annotation (Line(
      points={{212,22},{208,22},{208,52},{204,52}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_a2,boundary6. ports[1]) annotation (Line(
      points={{288,41},{304,41},{304,64},{322,64}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.heaPorVol[10], Tinside.port) annotation (Line(
      points={{250,21.886},{230,22},{230,16},{190,16},{190,30},{132,30}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(pip.port_b, tan.port_a1) annotation (Line(
      points={{58,56},{146,56},{146,7.56},{212,7.56}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_b1, pip1.port_a) annotation (Line(
      points={{212,-3.08},{174,-3.08},{174,-4},{128,-4},{128,-34},{14,-34}},
      color={0,127,255},
      smooth=Smooth.None));

  connect(Texch_out.T, scheduled_solarPanel_control3_1.T_in) annotation (
      Line(
      points={{67,-50},{72,-50},{72,-85.3},{-5.3,-85.3}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-120},{360,120}},
          preserveAspectRatio=true), graphics), Icon(coordinateSystem(
          extent={{-100,-120},{360,120}})),
    experiment(
      StopTime=3.1536e+007,
      Interval=120,
      Tolerance=1e-007,
      Algorithm="Esdirk23a"),
    __Dymola_experimentSetupOutput);
end ExtraTank;
