within SolarSystem.Models.Old;
model SolarTank_solis_suncontrol
  "Simulation of solar tank recharge and discharge"
extends Modelica.Icons.Example;
  Buildings.Fluid.Movers.FlowMachine_m_flow pump(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    allowFlowReversal=false,
    m_flow_start=0.1,
    T_start=283.15,
    m_flow_nominal=0.1,
    motorCooledByFluid=true)
                    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-60,10})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{60,80},{80,100}})));
  Buildings.Fluid.Sensors.Temperature Tpanel_in(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{-70,74},{-50,94}})));
  Buildings.Fluid.Sensors.Temperature Tpanel_out(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{-2,74},{18,94}})));
  Buildings.Fluid.Sensors.Temperature Texch_out(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{44,-40},{64,-60}})));
  Modelica.Blocks.Continuous.Integrator integrator
    annotation (Placement(transformation(extent={{-36,-6},{-16,14}})));
  Buildings.Fluid.Sources.Boundary_pT   boundary(         redeclare package
      Medium =         Modelica.Media.Water.ConstantPropertyLiquidWater,
      nPorts=1)
    annotation (Placement(transformation(extent={{118,-12},{138,8}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    use_m_flow_in=true,
    T=288.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{242,-46},{222,-26}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    V_start(displayUnit="l")=
            0.1,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    T_start=283.15)
    annotation (Placement(transformation(extent={{-98,50},{-78,70}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tinside
    annotation (Placement(transformation(extent={{132,20},{112,40}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(filNam="C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-22,96},{-34,108}})));
  Buildings.Fluid.SolarCollectors.EN12975 solarPanel_ISO(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    per=SolarSystemyy.Data.Parameter.SolarCollector.IDMK2_5_buildings(),
    azi=0,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    nPanels=6,
    nSeg=9,
    lat=0.78539816339745,
    til=0.5235987755983)
    annotation (Placement(transformation(extent={{-46,46},{-12,80}})));

  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex tan(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    hTan=1,
    dIns(displayUnit="mm") = 0.2,
    redeclare package MediumHex =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    m_flow_nominal=0.1,
    VTan=1.5,
    hexSegMult=2,
    dExtHex=0.02,
    hexTopHeight=0.9,
    hexBotHeight=0.6,
    CHex=200,
    nSeg=5,
    Q_flow_nominal(displayUnit="W") = 8000,
    TTan_nominal=293.15,
    THex_nominal=323.15,
    mHex_flow_nominal=8000/20/4180)
    annotation (Placement(transformation(extent={{166,-20},{204,24}})));

  Buildings.Rooms.Examples.BESTEST.BaseClasses.DaySchedule instructions(table=[0,
        0; 7*3600,0; 7*3600,0.04; 10*3600,0.04; 10*3600,0; 12*3600,0.02; 14
        *3600,0.02; 14*3600,0.001; 19*3600,0.001; 19*3600,0.001; 21*3600,
        0.02; 21*3600,0; 24*3600,0])
    "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{218,-6},{240,16}})));
  Buildings.Fluid.SolarCollectors.Controls.SolarPumpController pumCon(per=
        SolarSystemyy.Data.Parameter.SolarCollector.IDMK2_5_buildings())
    "Pump controller" annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={-22,-72})));
  Modelica.Blocks.Math.Gain gain(k=0.04) "Flow rate of the system in kg/s"
    annotation (Placement(transformation(
        extent={{8,-8},{-8,8}},
        rotation=270,
        origin={-70,-52})));
equation
  connect(pump.m_flow_actual, integrator.u) annotation (Line(
      points={{-65,21},{-65,28},{-42,28},{-42,4},{-38,4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pump.port_b, solarPanel_ISO.port_a) annotation (Line(
      points={{-60,20},{-60,63},{-46,63}},
      color={0,127,255},
      smooth=Smooth.None));

  connect(Tpanel_in.port, solarPanel_ISO.port_a) annotation (Line(
      points={{-60,74},{-54,74},{-54,63},{-46,63}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tpanel_out.port, solarPanel_ISO.port_b) annotation (Line(
      points={{8,74},{0,74},{0,63},{-12,63}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Meteo.weaBus, solarPanel_ISO.weaBus) annotation (Line(
      points={{-34,102},{-46,102},{-46,79.32}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(boundary1.ports[1], tan.port_b) annotation (Line(
      points={{222,-36},{210,-36},{210,2},{204,2}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_a, boundary.ports[1]) annotation (Line(
      points={{166,2},{148,2},{148,-2},{138,-2}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(instructions.y[1], boundary1.m_flow_in) annotation (Line(
      points={{241.1,5},{254,5},{254,-28},{242,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarPanel_ISO.port_b, tan.port_a1) annotation (Line(
      points={{-12,63},{154,63},{154,-6.36},{166,-6.36}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_b1, pump.port_a) annotation (Line(
      points={{166,-15.6},{54,-15.6},{54,-22},{-60,-22},{-60,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_b1, Texch_out.port) annotation (Line(
      points={{166,-15.6},{54,-15.6},{54,-40}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(pump.port_b, SurgeTank.port_a) annotation (Line(
      points={{-60,20},{-60,38},{-88,38},{-88,50}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.heaPorVol[3], Tinside.port) annotation (Line(
      points={{185,2},{172,2},{172,30},{132,30}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(gain.y, pump.m_flow_in) annotation (Line(
      points={{-70,-43.2},{-70,-18},{-80,-18},{-80,9.8},{-72,9.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pumCon.y, gain.u) annotation (Line(
      points={{-33.8,-72},{-70,-72},{-70,-61.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinside.T, pumCon.TIn) annotation (Line(
      points={{112,30},{24,30},{24,-76},{-10,-76}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Meteo.weaBus, pumCon.weaBus) annotation (Line(
      points={{-34,102},{-46,102},{-46,90},{-6,90},{-6,-66},{-11.8,-66}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-120},{260,120}},
          preserveAspectRatio=true), graphics), Icon(coordinateSystem(
          extent={{-100,-120},{260,120}})),
    experiment(StopTime=3.1536e+007, Interval=240));
end SolarTank_solis_suncontrol;
