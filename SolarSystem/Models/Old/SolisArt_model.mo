within SolarSystem.Models.Old;
model SolisArt_model "Simulation of solar tank recharge and discharge"
extends Modelica.Icons.Example;
  package MediumE = Buildings.Media.ConstantPropertyLiquidWater
    "Medium water model";

  parameter Modelica.SIunits.Pressure dpPip_nominal = 1000
    "Pressure difference of pipe (without valve)";

  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.04
    "Mass flow rate for pump S6 S5 and splitter";

  parameter Modelica.Blocks.Interfaces.RealOutput flow_mini=0.0005
    "Value of Real output";

  Buildings.Fluid.Movers.FlowMachine_m_flow C6(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    m_flow(start=0.04),
    T_start=283.15,
    m_flow_nominal=m_flow_nominal)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={54,-52})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{500,100},{540,140}})));
  Buildings.Fluid.Sources.Boundary_pT   boundary(         redeclare package
      Medium =         MediumE,
      nPorts=1)
    annotation (Placement(transformation(extent={{2,22},{14,34}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        MediumE,
    use_m_flow_in=false,
    use_T_in=true,
    m_flow=0.000001,
    nPorts=1)
    annotation (Placement(transformation(extent={{160,-22},{148,-10}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    V_start(displayUnit="l")=
            0.2,
    redeclare package Medium = MediumE,
    T_start=323.15)
    annotation (Placement(transformation(extent={{-66,-40},{-28,-2}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T5
    annotation (Placement(transformation(extent={{72,44},{58,58}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(filNam="C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos",
      computeWetBulbTemperature=false)
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-26,116},{-46,134}})));
  Buildings.Fluid.SolarCollectors.EN12975 solarPanel_ISO(
    redeclare package Medium = MediumE,
    per=SolarSystemyy.Data.Parameter.SolarCollector.IDMK2_5_buildings(),
    azi=0,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    use_shaCoe_in=false,
    T_start=323.15,
    m_flow(start=0.04),
    dp(start=100),
    nSeg=40,
    lat=0.78539816339745,
    til=0.5235987755983,
    nPanels=5)
    annotation (Placement(transformation(extent={{-74,62},{-30,110}})));

  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex Strorage_tank(
    redeclare package Medium =
        MediumE,
    hTan=1,
    redeclare package MediumHex =
        MediumE,
    VTan=1.5,
    dExtHex=0.02,
    hexTopHeight=0.9,
    CHex=200,
    hexBotHeight=0.1,
    Q_flow_nominal(displayUnit="W") = 8000,
    mHex_flow_nominal=8000/20/4180,
    hexSegMult=3,
    kIns=0.1,
    nSeg=20,
    dIns(displayUnit="mm") = 0.1,
    m_flow_nominal=0.000001,
    T_start=293.15,
    TTan_nominal=293.15,
    THex_nominal=323.15)
    annotation (Placement(transformation(extent={{68,-12},{106,32}})));

  Buildings.HeatTransfer.Sources.FixedTemperature Troom1(T=273.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{-9,-9},{9,9}},
        rotation=90,
        origin={125,-29})));
  SolarSystemyy.Classes.Exchanger_and_Tank.StratifiedEnhancedInternalHex_double
    Solar_tank(
    redeclare package Medium = MediumE,
    hTan=1,
    redeclare package MediumHex = MediumE,
    VTan=1.5,
    nSeg=20,
    dIns(displayUnit="mm") = 0.0002,
    hexTopHeight={0.9,0.5},
    hexBotHeight={0.6,0.1},
    hexSegMult={2,2},
    CHex={200,200},
    Q_flow_nominal(displayUnit="W") = {8000,8000},
    mHex_flow_nominal={8000/20/4180,8000/20/4180},
    dExtHex={0.02,0.02},
    m_flow_nominal=m_flow_nominal,
    TTan_nominal={566.3,566.3},
    THex_nominal={596.3,596.3})
    annotation (Placement(transformation(extent={{182,-22},{234,36}})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C5(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    T_start=283.15,
    m_flow_nominal=m_flow_nominal)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={172,-46})));
  Buildings.HeatTransfer.Sources.FixedTemperature Troom2(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{-9,-9},{9,9}},
        rotation=90,
        origin={239,-49})));
  Buildings.Fluid.Sources.FixedBoundary Cool_water(redeclare package Medium =
        MediumE,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{11,-10},{-11,10}},
        rotation=-90,
        origin={254,-75})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T3
    annotation (Placement(transformation(extent={{198,36},{212,50}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T4
    annotation (Placement(transformation(extent={{198,60},{212,74}})));
  Modelica.Blocks.Logical.Switch modifier_switch
    annotation (Placement(transformation(extent={{5,-5},{-5,5}},
        rotation=-90,
        origin={207,-55})));
  Modelica.Blocks.Sources.RealExpression solar_off(y=flow_mini)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{5,8},{-5,-8}},
        rotation=-90,
        origin={197,-82})));
  Modelica.Blocks.Sources.RealExpression solar_on(y=m_flow_nominal)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{5,-8},{-5,8}},
        rotation=-90,
        origin={217,-82})));
  Modelica.Blocks.Logical.Switch modifier_switch1
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=0,
        origin={86,-60})));
  Modelica.Blocks.Sources.RealExpression storage_on(y=m_flow_nominal)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{5,-8},{-5,8}},
        rotation=0,
        origin={117,-52})));
  Modelica.Blocks.Sources.RealExpression storage_off(y=flow_mini)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{-5,8},{5,-8}},
        rotation=180,
        origin={117,-68})));
  Buildings.Fluid.Sensors.MassFlowRate flowBetween_tanks(redeclare package
      Medium = MediumE)
    annotation (Placement(transformation(extent={{128,80},{140,92}})));
  Buildings.Fluid.Sensors.MassFlowRate inFlowTop_Storage(redeclare package
      Medium = MediumE)                                   annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={50,66})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{-1,1,-1}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={50,86})));

  SolarSystemyy.Classes.Other.Water_Drawing_up_control Drawing_up1(
    T=5,
    k=0.8,
    table=[0,0,0; 5.99,0,0; 6,200,0.04; 8.99,200,0.04; 9,100,0.05; 9.99,100,
        0.05; 10,0,0; 10.99,0,0; 11,0,0; 11.99,0,0; 12,200,0.07; 13.49,200,
        0.07; 13.5,0,0; 14.49,0,0; 14.5,0,0; 15.49,0,0; 15.5,0,0; 16.49,0,0;
        16.5,0,0; 17.49,0,0; 17.5,0,0; 17.99,0,0; 18,0,0; 18.99,0,0; 19,150,
        0.05; 21,150,0.05; 21.01,0,0; 24,0,0],
    redeclare package Medium = MediumE)
    annotation (Placement(transformation(extent={{134,44},{174,74}})));
  Buildings.Fluid.Sensors.MassFlowRate outFlow_panels(redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{10,78},{26,94}})));
  SolarSystemyy.Classes.Control.Horloge horloge(Heure_debut=0)
    annotation (Placement(transformation(extent={{500,-242},{540,-202}})));

  Buildings.Fluid.Sensors.MassFlowRate inFlow_panels(redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-86,10})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal1(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{1,-1,1}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={66,-98})));
  Buildings.Fluid.HeatExchangers.Radiators.RadiatorEN442_2 rad(
    redeclare package Medium =
        MediumE,
    nEle=20,
    Q_flow_nominal=5000,
    T_a_nominal=313.15,
    T_b_nominal=303.15) annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=-90,
        origin={420,0})));

  Buildings.Fluid.Movers.FlowMachine_m_flow C2(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    T_start=283.15,
    m_flow_nominal=m_flow_nominal)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={400,-44})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal2(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{1,-1,1}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={180,-98})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T7(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{292,-108},{312,-88}})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal3(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{1,-1,1}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={360,-98})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C4(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    T_start=283.15,
    m_flow_nominal=m_flow_nominal)
                    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={320,-36})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T8(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{322,76},{342,96}})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal4(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=m_flow_nominal*{-1,1,-1},
    dp_nominal={dpPip_nominal,0,dpPip_nominal}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={360,86})));
  Buildings.Fluid.Sensors.MassFlowRate inFlow_topExtra(
                                                      redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={360,56})));
  Buildings.Fluid.Sensors.MassFlowRate inFlow_heating(redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={400,54})));
  Buildings.Fluid.Sensors.MassFlowRate outFlow_heating(redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=0,
        origin={342,-98})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM spl(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    m_flow_nominal=m_flow_nominal*{-1,1,-1})
    annotation (Placement(transformation(extent={{264,80},{276,92}})));

  Buildings.Fluid.Sensors.MassFlowRate inFlow_backup(redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={270,-14})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal5(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=m_flow_nominal*{-1,1,-1},
    dp_nominal={dpPip_nominal,0,dpPip_nominal}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={180,86})));
  Buildings.Fluid.Sensors.MassFlowRate flow_between_tankAndBackup(redeclare
      package Medium = MediumE)
    annotation (Placement(transformation(extent={{230,80},{242,92}})));
  SolarSystemyy.Classes.Control.SolisArt_mod.Algo_Deprecated algo_all(
    n=1,
    Sj_onTime={240},
    Tambiant_out_start={18},
    pumpControl_Sj_start={true})
    annotation (Placement(transformation(extent={{102,-246},{200,-132}})));
  Modelica.Blocks.Sources.RealExpression storage_on1(y=m_flow_nominal)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{5,-8},{-5,8}},
        rotation=0,
        origin={365,-46})));
  Modelica.Blocks.Sources.RealExpression storage_off1(
                                                     y=flow_mini)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{-5,8},{5,-8}},
        rotation=180,
        origin={365,-64})));
  Modelica.Blocks.Logical.Switch modifier_switch2
    annotation (Placement(transformation(extent={{5,-5},{-5,5}},
        rotation=0,
        origin={345,-55})));
  Modelica.Blocks.Sources.RealExpression storage_on2(y=m_flow_nominal)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{5,-8},{-5,8}},
        rotation=0,
        origin={449,-46})));
  Modelica.Blocks.Sources.RealExpression storage_off2(
                                                     y=flow_mini)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{-5,8},{5,-8}},
        rotation=180,
        origin={449,-64})));
  Modelica.Blocks.Logical.Switch modifier_switch3
    annotation (Placement(transformation(extent={{5,-5},{-5,5}},
        rotation=0,
        origin={425,-55})));
  Buildings.Fluid.Sensors.MassFlowRate inFlowLow_storage(redeclare package
      Medium = MediumE)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={14,-54})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T2(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{50,-108},{30,-88}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T1(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{-22,76},{-2,96}})));
  Buildings.Fluid.Boilers.BoilerPolynomial boi(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=0.04,
    Q_flow_nominal=20000,
    fue=Buildings.Fluid.Data.Fuels.NaturalGasHigherHeatingValue(),
    dp_nominal=1000)                                               annotation (
      Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={270,58})));

  Buildings.HeatTransfer.Sources.FixedTemperature Troom3(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{230,50},{246,66}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T10(redeclare package Medium =
        MediumE, m_flow_nominal=0.04)
    annotation (Placement(transformation(extent={{-8,-8},{8,8}},
        rotation=90,
        origin={158,22})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{26,-246},{52,-220}})));
  Buildings.Rooms.Examples.BESTEST.BaseClasses.DaySchedule T_int_test(table=[0,20;
        7*3600,20; 7*3600,19; 10*3600,19; 10*3600,19; 12*3600,18; 14*3600,18; 14
        *3600,17; 19*3600,17; 19*3600,19; 21*3600,19; 21*3600,20; 24*3600,20])
    "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{0,-220},{22,-198}})));
  Buildings.Fluid.Actuators.Valves.ThreeWayLinear solar_valve(
    fraK=0.5,
    l={0.001,0.001},
    redeclare package Medium = MediumE,
    dpValve_nominal=6000,
    m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{-22,-88},{-2,-108}})));
  Buildings.Fluid.Actuators.Valves.ThreeWayLinear solar_valve1(
    fraK=0.5,
    l={0.001,0.001},
    redeclare package Medium = MediumE,
    dpValve_nominal=6000,
    m_flow_nominal=m_flow_nominal)
    annotation (Placement(transformation(extent={{260,-88},{280,-108}})));
  Buildings.HeatTransfer.Sources.FixedTemperature Text_air(T=283.15)
    "Conv outside temperature"
    annotation (Placement(transformation(extent={{-9,-9},{9,9}},
        rotation=180,
        origin={469,31})));
  Buildings.HeatTransfer.Sources.FixedTemperature T_ext_dry(T=283.15)
    "Rad outside temperature"
    annotation (Placement(transformation(extent={{-9,-9},{9,9}},
        rotation=180,
        origin={469,-29})));
equation
  connect(Meteo.weaBus, solarPanel_ISO.weaBus) annotation (Line(
      points={{-46,125},{-74,125},{-74,109.04}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(boundary1.ports[1], Strorage_tank.port_b) annotation (Line(
      points={{148,-16},{142,-16},{142,10},{106,10}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Strorage_tank.port_a, boundary.ports[1]) annotation (Line(
      points={{68,10},{34,10},{34,28},{14,28}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Strorage_tank.heaPorSid, Troom1.port) annotation (Line(
      points={{97.64,10},{102,10},{102,16},{116,16},{116,6},{125,6},{125,-20}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Strorage_tank.heaPorTop, Troom1.port) annotation (Line(
      points={{90.8,26.28},{90.8,32},{106,32},{106,16},{116,16},{116,6},{125,6},
          {125,-20}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Strorage_tank.port_b1, C6.port_a) annotation (Line(
      points={{68,-7.6},{68,-8},{54,-8},{54,-42}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.port_b1, C5.port_a) annotation (Line(
      points={{182,-12.14},{182,-12},{172,-12},{172,-36}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.heaPorSid, Troom2.port) annotation (Line(
      points={{222.56,7},{222,7},{222,2},{239,2},{239,-40}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Solar_tank.heaPorTop, Troom2.port) annotation (Line(
      points={{213.2,28.46},{213.2,36},{246,36},{246,12},{239,12},{239,-40}},
      color={191,0,0},
      smooth=Smooth.None));

  connect(modifier_switch.y, C5.m_flow_in) annotation (Line(
      points={{207,-49.5},{207,-45.8},{184,-45.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(storage_on.y, modifier_switch1.u1) annotation (Line(
      points={{111.5,-52},{108,-52},{108,-55.2},{93.2,-55.2}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(splitVal.port_1, flowBetween_tanks.port_a)
                                              annotation (Line(
      points={{56,86},{128,86}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal.port_3, inFlowTop_Storage.port_a)
                                                annotation (Line(
      points={{50,80},{50,72}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlowTop_Storage.port_b, Strorage_tank.port_a1)
                                                      annotation (Line(
      points={{50,60},{50,24},{62,24},{62,1.64},{68,1.64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(outFlow_panels.port_b, splitVal.port_2)      annotation (Line(
      points={{26,86},{44,86}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_panels.port_b, solarPanel_ISO.port_a)        annotation (
      Line(
      points={{-86,16},{-86,86},{-74,86}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.heaPorVol[2], T4.port) annotation (Line(
      points={{208,5.521},{192,5.521},{192,67},{198,67}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Cool_water.ports[1], Solar_tank.port_b) annotation (Line(
      points={{254,-64},{254,8},{234,8},{234,7}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.heaPorVol[18], T3.port) annotation (Line(
      points={{208,8.305},{208,8},{192,8},{192,43},{198,43}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Strorage_tank.heaPorVol[4], T5.port) annotation (Line(
      points={{87,9.142},{78,9.142},{78,51},{72,51}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(C6.port_b, splitVal1.port_3) annotation (Line(
      points={{54,-62},{54,-80},{66,-80},{66,-92}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C5.port_b, splitVal2.port_3) annotation (Line(
      points={{172,-56},{172,-72},{180,-72},{180,-92}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal2.port_2, splitVal1.port_1) annotation (Line(
      points={{174,-98},{72,-98}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C2.port_b, splitVal3.port_1) annotation (Line(
      points={{400,-54},{400,-98},{366,-98}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(rad.port_b, C2.port_a) annotation (Line(
      points={{420,-14},{420,-20},{400,-20},{400,-34}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C4.port_b, splitVal3.port_3) annotation (Line(
      points={{320,-46},{320,-72},{360,-72},{360,-92}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T8.port_b, splitVal4.port_2) annotation (Line(
      points={{342,86},{354,86}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.port_b2, C4.port_a) annotation (Line(
      points={{234,13.38},{282,13.38},{282,14},{320,14},{320,-26}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal4.port_3,inFlow_topExtra. port_a) annotation (Line(
      points={{360,80},{360,62}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_heating.port_b, rad.port_a)  annotation (Line(
      points={{400,48},{400,20},{420,20},{420,14}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal4.port_1, inFlow_heating.port_a)  annotation (Line(
      points={{366,86},{400,86},{400,60}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_topExtra.port_b, Solar_tank.port_a2) annotation (Line(
      points={{360,50},{360,21.5},{234,21.5}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal3.port_2, outFlow_heating.port_a)  annotation (Line(
      points={{354,-98},{348,-98}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(outFlow_heating.port_b, T7.port_b)  annotation (Line(
      points={{336,-98},{312,-98}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(spl.port_2, T8.port_a) annotation (Line(
      points={{276,86},{322,86}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flowBetween_tanks.port_b, splitVal5.port_2)
                                                   annotation (Line(
      points={{140,86},{174,86}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flow_between_tankAndBackup.port_b, spl.port_1)
                                              annotation (Line(
      points={{242,86},{264,86}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal5.port_1, flow_between_tankAndBackup.port_a)
                                                    annotation (Line(
      points={{186,86},{230,86}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(storage_on1.y, modifier_switch2.u1)
                                             annotation (Line(
      points={{359.5,-46},{356,-46},{356,-51},{351,-51}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(modifier_switch2.y, C4.m_flow_in) annotation (Line(
      points={{339.5,-55},{336.75,-55},{336.75,-35.8},{332,-35.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(storage_off2.y, modifier_switch3.u3)
                                              annotation (Line(
      points={{443.5,-64},{438,-64},{438,-59},{431,-59}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(storage_on2.y, modifier_switch3.u1)
                                             annotation (Line(
      points={{443.5,-46},{438,-46},{438,-51},{431,-51}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(modifier_switch3.y, C2.m_flow_in) annotation (Line(
      points={{419.5,-55},{417.75,-55},{417.75,-43.8},{412,-43.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(inFlowLow_storage.port_b, Strorage_tank.port_b1)    annotation (Line(
      points={{14,-48},{14,-7.6},{68,-7.6}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal1.port_2, T2.port_a) annotation (Line(
      points={{60,-98},{50,-98}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solarPanel_ISO.port_b, T1.port_a) annotation (Line(
      points={{-30,86},{-22,86}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T1.port_b, outFlow_panels.port_a)      annotation (Line(
      points={{-2,86},{10,86}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_backup.port_b, boi.port_a)   annotation (Line(
      points={{270,-8},{270,44}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(boi.port_b, spl.port_3) annotation (Line(
      points={{270,72},{270,80}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Troom3.port, boi.heatPort) annotation (Line(
      points={{246,58},{259.92,58}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(algo_all.pumpControl_S4, modifier_switch2.u2) annotation (Line(
      points={{201.508,-138.706},{374,-138.706},{374,-55},{351,-55}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(storage_off1.y, modifier_switch2.u3) annotation (Line(
      points={{359.5,-64},{356,-64},{356,-59},{351,-59}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(algo_all.pumpControl_S5, modifier_switch.u2) annotation (Line(
      points={{201.508,-152.118},{207,-152.118},{207,-61}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo_all.pumpControl_S6, modifier_switch1.u2) annotation (Line(
      points={{201.508,-165.529},{220,-165.529},{220,-108},{140,-108},{140,
          -60},{93.2,-60}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo_all.pumpControl_Sj[1], modifier_switch3.u2) annotation (Line(
      points={{201.508,-178.941},{460,-178.941},{460,-55},{431,-55}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T1.T, algo_all.T1) annotation (Line(
      points={{-12,97},{-12,140},{-100,140},{-100,-139.041},{102.565,
          -139.041}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(T3.T, algo_all.T3) annotation (Line(
      points={{212,43},{220,43},{220,140},{-100,140},{-100,-148.429},{
          102.565,-148.429}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(T4.T, algo_all.T4) annotation (Line(
      points={{212,67},{220,67},{220,140},{-100,140},{-100,-157.818},{
          102.565,-157.818}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(T5.T, algo_all.T5) annotation (Line(
      points={{58,51},{36,51},{36,140},{-100,140},{-100,-167.206},{102.565,
          -167.206}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(T7.T, algo_all.T7) annotation (Line(
      points={{302,-87},{302,140},{-100,140},{-100,-177.935},{102.565,
          -177.935}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(T8.T, algo_all.T8) annotation (Line(
      points={{332,97},{332,140},{-100,140},{-100,-187.994},{102.565,
          -187.994}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(Solar_tank.port_a, T10.port_a) annotation (Line(
      points={{182,7},{158,7},{158,14}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T10.port_b, Drawing_up1.port_a) annotation (Line(
      points={{158,30},{154,30},{154,44.1875}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T10.T, algo_all.T10) annotation (Line(
      points={{149.2,22},{130,22},{130,32},{122,32},{122,140},{-100,140},{
          -100,-186},{-78,-186},{-78,-198.053},{102.565,-198.053}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Meteo.weaBus, weaBus) annotation (Line(
      points={{-46,125},{-94,125},{-94,-233},{39,-233}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(T_int_test.y[1], algo_all.Tambiant[1]) annotation (Line(
      points={{23.1,-209},{61.55,-209},{61.55,-229.571},{102.188,-229.571}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(weaBus.TDryBul, algo_all.Text) annotation (Line(
      points={{39,-233},{70.5,-233},{70.5,-240.3},{102.188,-240.3}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(C4.P, algo_all.flow_S4) annotation (Line(
      points={{328,-47},{328,-64},{302,-64},{302,140},{-100,140},{-100,-122},
          {128,-122},{128,-135.353},{128.385,-135.353}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T2.port_b, solar_valve.port_2) annotation (Line(
      points={{30,-98},{-2,-98}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_3, inFlowLow_storage.port_a)    annotation (Line(
      points={{-12,-88},{-12,-80},{14,-80},{14,-60}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_1, inFlow_panels.port_a)        annotation (Line(
      points={{-22,-98},{-86,-98},{-86,4}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_1, SurgeTank.port_a) annotation (Line(
      points={{-22,-98},{-47,-98},{-47,-40}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T7.port_a, solar_valve1.port_2) annotation (Line(
      points={{292,-98},{280,-98}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve1.port_3, inFlow_backup.port_a)   annotation (Line(
      points={{270,-88},{270,-20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve1.port_1, splitVal2.port_1) annotation (Line(
      points={{260,-98},{186,-98}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(algo_all.BackupHeater_outter, boi.y) annotation (Line(
      points={{201.508,-239.294},{386,-239.294},{386,32},{258.8,32},{258.8,
          41.2}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(algo_all.V3V_solar_outter, solar_valve.y) annotation (Line(
      points={{201.508,-205.765},{214,-205.765},{214,-116},{-12,-116},{-12,
          -110}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(algo_all.V3V_extra_outter, solar_valve1.y) annotation (Line(
      points={{201.508,-219.176},{270,-219.176},{270,-110}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T5.T, boundary1.T_in) annotation (Line(
      points={{58,51},{56,51},{56,36},{114,36},{114,28},{122,28},{122,16},{152,16},
          {152,4},{166,4},{166,-13.6},{161.2,-13.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(modifier_switch1.y, C6.m_flow_in) annotation (Line(
      points={{79.4,-60},{74.7,-60},{74.7,-51.8},{66,-51.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(storage_off.y, modifier_switch1.u3) annotation (Line(
      points={{111.5,-68},{108,-68},{108,-64.8},{93.2,-64.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solar_off.y, modifier_switch.u3) annotation (Line(
      points={{197,-76.5},{197,-69.25},{203,-69.25},{203,-61}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solar_on.y, modifier_switch.u1) annotation (Line(
      points={{217,-76.5},{217,-69.25},{211,-69.25},{211,-61}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(rad.heatPortCon, Text_air.port) annotation (Line(
      points={{430.08,2.8},{445.04,2.8},{445.04,31},{460,31}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(rad.heatPortRad, T_ext_dry.port) annotation (Line(
      points={{430.08,-2.8},{445.04,-2.8},{445.04,-29},{460,-29}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(splitVal5.port_3, Solar_tank.port_a1) annotation (Line(
      points={{180,80},{180,22},{174,22},{174,-4.02},{182,-4.02}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  annotation (Diagram(coordinateSystem(extent={{-100,-260},{600,140}},
          preserveAspectRatio=true), graphics), Icon(coordinateSystem(
          extent={{-100,-260},{600,140}})),
    experiment(
      StopTime=3.1536e+007,
      Interval=120,
      Tolerance=1e-007,
      Algorithm="Dassl"),
    __Dymola_experimentSetupOutput);
end SolisArt_model;
