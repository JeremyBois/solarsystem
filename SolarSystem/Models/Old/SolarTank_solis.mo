within SolarSystem.Models.Old;
model SolarTank_solis "Simulation of solar tank recharge and discharge"
extends Modelica.Icons.Example;
  Buildings.Fluid.Movers.FlowMachine_m_flow pump(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    allowFlowReversal=false,
    m_flow_start=0.1,
    T_start=283.15,
    m_flow_nominal=0.1,
    motorCooledByFluid=true)
                    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-60,10})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{60,80},{80,100}})));
  Buildings.Fluid.Sensors.Temperature Tpanel_in(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{-70,74},{-50,94}})));
  Buildings.Fluid.Sensors.Temperature Tpanel_out(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{-2,74},{18,94}})));
  Buildings.Fluid.Sensors.Temperature Texch_out(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{44,-40},{64,-60}})));
  Modelica.Blocks.Continuous.Integrator integrator
    annotation (Placement(transformation(extent={{-36,-6},{-16,14}})));
  Buildings.Fluid.Sources.Boundary_pT   boundary(         redeclare package
      Medium =         Modelica.Media.Water.ConstantPropertyLiquidWater,
      nPorts=1)
    annotation (Placement(transformation(extent={{118,-12},{138,8}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    use_m_flow_in=true,
    T=288.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{242,-46},{222,-26}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    V_start(displayUnit="l")=
            0.1,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    T_start=283.15)
    annotation (Placement(transformation(extent={{-98,50},{-78,70}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tinside
    annotation (Placement(transformation(extent={{132,20},{112,40}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(filNam="C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-22,96},{-34,108}})));
  Buildings.Fluid.SolarCollectors.EN12975 solarPanel_ISO(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    per=SolarSystemyy.Data.Parameter.SolarCollector.IDMK2_5_buildings(),
    azi=0,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    nPanels=6,
    nSeg=9,
    lat=0.78539816339745,
    til=0.5235987755983)
    annotation (Placement(transformation(extent={{-46,46},{-12,80}})));

  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex tan(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    hTan=1,
    dIns(displayUnit="mm") = 0.2,
    redeclare package MediumHex =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    m_flow_nominal=0.1,
    VTan=1.5,
    hexSegMult=2,
    dExtHex=0.02,
    hexTopHeight=0.9,
    hexBotHeight=0.6,
    CHex=200,
    nSeg=5,
    Q_flow_nominal(displayUnit="W") = 8000,
    mHex_flow_nominal=8000/20/4180,
    TTan_nominal=293.15,
    THex_nominal=313.15)
    annotation (Placement(transformation(extent={{166,-20},{204,24}})));

  SolarSystemyy.Classes.Control.SolisArt_mod.solarTank_mod solarTank_mod
    annotation (Placement(transformation(extent={{2,-84},{-24,-58}})));
  Modelica.Blocks.Logical.Switch modifier_switch
    annotation (Placement(transformation(extent={{-48,-82},{-66,-64}})));
  Modelica.Blocks.Sources.RealExpression Max1(y=0)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{-26,-106},{-36,-90}})));
  Modelica.Blocks.Sources.RealExpression Max2(y=0.04)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{-24,-60},{-34,-44}})));
  Buildings.Rooms.Examples.BESTEST.BaseClasses.DaySchedule instructions(table=[0,
        0; 7*3600,0; 7*3600,0.04; 10*3600,0.04; 10*3600,0; 12*3600,0.02; 14
        *3600,0.02; 14*3600,0.001; 19*3600,0.001; 19*3600,0.001; 21*3600,
        0.02; 21*3600,0; 24*3600,0])
    "Scheduled consigne setup (repeat every days)"
    annotation (Placement(transformation(extent={{218,-6},{240,16}})));
  Modelica.Blocks.Sources.BooleanExpression
                                         Max3(y=true)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{32,-94},{22,-78}})));
equation
  connect(pump.m_flow_actual, integrator.u) annotation (Line(
      points={{-65,21},{-65,28},{-42,28},{-42,4},{-38,4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pump.port_b, solarPanel_ISO.port_a) annotation (Line(
      points={{-60,20},{-60,63},{-46,63}},
      color={0,127,255},
      smooth=Smooth.None));

  connect(Tpanel_in.port, solarPanel_ISO.port_a) annotation (Line(
      points={{-60,74},{-54,74},{-54,63},{-46,63}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tpanel_out.port, solarPanel_ISO.port_b) annotation (Line(
      points={{8,74},{0,74},{0,63},{-12,63}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Meteo.weaBus, solarPanel_ISO.weaBus) annotation (Line(
      points={{-34,102},{-46,102},{-46,79.32}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarTank_mod.pumpControl_S5, modifier_switch.u2) annotation (
      Line(
      points={{-24,-73.1667},{-35.1,-73.1667},{-35.1,-73},{-46.2,-73}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Max1.y, modifier_switch.u3) annotation (Line(
      points={{-36.5,-98},{-40,-98},{-40,-80.2},{-46.2,-80.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Max2.y, modifier_switch.u1) annotation (Line(
      points={{-34.5,-52},{-38,-52},{-38,-65.8},{-46.2,-65.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tpanel_out.T, solarTank_mod.T1) annotation (Line(
      points={{15,84},{24,84},{24,-63.4167},{2,-63.4167}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tinside.T, solarTank_mod.T3) annotation (Line(
      points={{112,30},{30,30},{30,-67.75},{2,-67.75}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(boundary1.ports[1], tan.port_b) annotation (Line(
      points={{222,-36},{210,-36},{210,2},{204,2}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_a, boundary.ports[1]) annotation (Line(
      points={{166,2},{148,2},{148,-2},{138,-2}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(instructions.y[1], boundary1.m_flow_in) annotation (Line(
      points={{241.1,5},{254,5},{254,-28},{242,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarPanel_ISO.port_b, tan.port_a1) annotation (Line(
      points={{-12,63},{154,63},{154,-6.36},{166,-6.36}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_b1, pump.port_a) annotation (Line(
      points={{166,-15.6},{54,-15.6},{54,-22},{-60,-22},{-60,0}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_b1, Texch_out.port) annotation (Line(
      points={{166,-15.6},{54,-15.6},{54,-40}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(pump.port_b, SurgeTank.port_a) annotation (Line(
      points={{-60,20},{-60,38},{-88,38},{-88,50}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.heaPorVol[3], Tinside.port) annotation (Line(
      points={{185,2},{172,2},{172,30},{132,30}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Max3.y, solarTank_mod.V3V_solaire) annotation (Line(
      points={{21.5,-86},{12,-86},{12,-78.5833},{2,-78.5833}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(modifier_switch.y, pump.m_flow_in) annotation (Line(
      points={{-66.9,-73},{-80,-73},{-80,9.8},{-72,9.8}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-120},{260,120}},
          preserveAspectRatio=true), graphics), Icon(coordinateSystem(
          extent={{-100,-120},{260,120}})),
    experiment(StopTime=3.1536e+007, Interval=240));
end SolarTank_solis;
