within SolarSystem.Models.Old;
model Solar_solis_splitter "Simulation of solar tank recharge and discharge"
extends Modelica.Icons.Example;

  parameter Modelica.SIunits.Pressure dpPip_nominal = 1000
    "Pressure difference of pipe (without valve)";
  parameter Modelica.SIunits.MassFlowRate flow_nominal = 0.04
    "Mass flow rate for pump S6 S5 and splitter";
  package MediumE = Buildings.Media.ConstantPropertyLiquidWater
    "Medium water model";
  parameter Modelica.Blocks.Interfaces.RealOutput flow_mini=0.00005
    "Value of Real output";
  Buildings.Fluid.Movers.FlowMachine_m_flow C6(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    m_flow(start=0.04),
    m_flow_nominal=0.04,
    dp(start=10000),
    T_start=313.15) annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={66,-32})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{280,100},{300,120}})));
  Buildings.Fluid.Sensors.Temperature T1(redeclare package Medium =
        MediumE)
    annotation (Placement(transformation(extent={{-16,76},{4,96}})));
  Buildings.Fluid.Sources.Boundary_pT   boundary(         redeclare package
      Medium =         MediumE,
      nPorts=1)
    annotation (Placement(transformation(extent={{46,4},{58,16}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        MediumE,
    use_m_flow_in=false,
    use_T_in=true,
    m_flow=0.000001,
    nPorts=1)
    annotation (Placement(transformation(extent={{128,4},{116,16}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    V_start(displayUnit="l")=
            0.2,
    redeclare package Medium = MediumE,
    T_start=313.15)
    annotation (Placement(transformation(extent={{-52,-40},{-32,-20}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T5
    annotation (Placement(transformation(extent={{72,44},{58,58}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(filNam="C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos",
      computeWetBulbTemperature=false)
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-22,96},{-34,108}})));
  Buildings.Fluid.SolarCollectors.EN12975 solarPanel_ISO(
    redeclare package Medium = MediumE,
    per=SolarSystemyy.Data.Parameter.SolarCollector.IDMK2_5_buildings(),
    azi=0,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    use_shaCoe_in=false,
    m_flow(start=0.04),
    dp(start=500),
    nPanels=8,
    nSeg=50,
    T_start=313.15,
    lat=0.78539816339745,
    til=0.78539816339745)
    annotation (Placement(transformation(extent={{-46,46},{-12,80}})));

  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex Strorage_tank(
    redeclare package Medium =
        MediumE,
    hTan=1,
    redeclare package MediumHex =
        MediumE,
    VTan=1.5,
    dExtHex=0.02,
    hexTopHeight=0.9,
    CHex=200,
    hexBotHeight=0.1,
    m_flow_nominal=0.04,
    hexSegMult=3,
    kIns=0.1,
    nSeg=20,
    dIns(displayUnit="mm") = 0.1,
    mHex_flow_nominal=0.79,
    Q_flow_nominal(displayUnit="kW") = 240000,
    T_start=313.15,
    TTan_nominal=283.15,
    THex_nominal=318.15)
    annotation (Placement(transformation(extent={{66,-12},{104,32}})));

  Buildings.HeatTransfer.Sources.FixedTemperature Troom1(T=273.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{110,-10},{120,0}})));
  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex Solar_tank(
    redeclare package Medium =
        MediumE,
    hTan=1,
    redeclare package MediumHex =
        MediumE,
    VTan=1.5,
    dExtHex=0.02,
    hexTopHeight=0.9,
    hexBotHeight=0.6,
    CHex=200,
    m_flow_nominal=0.3,
    hexSegMult=3,
    nSeg=20,
    Q_flow_nominal(displayUnit="kW") = 120000,
    dIns(displayUnit="mm") = 0.1,
    mHex_flow_nominal=0.78,
    T_start=313.15,
    TTan_nominal=283.15,
    THex_nominal=318.15)
    annotation (Placement(transformation(extent={{180,-16},{218,28}})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C5(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    m_flow_nominal=0.04,
    T_start=313.15) annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={180,-38})));
  Buildings.HeatTransfer.Sources.FixedTemperature Troom2(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{220,-8},{230,2}})));
  Buildings.Fluid.Sources.FixedBoundary Cool_water(redeclare package Medium =
        MediumE,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=-90,
        origin={248,-60})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T3
    annotation (Placement(transformation(extent={{198,36},{212,50}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T4
    annotation (Placement(transformation(extent={{198,60},{212,74}})));
  Modelica.Blocks.Logical.Switch modifier_switch
    annotation (Placement(transformation(extent={{5,-5},{-5,5}},
        rotation=-90,
        origin={207,-45})));
  Modelica.Blocks.Sources.RealExpression solar_off(y=flow_mini)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{5,8},{-5,-8}},
        rotation=-90,
        origin={199,-68})));
  Modelica.Blocks.Sources.RealExpression solar_on(y=0.04)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{5,-8},{-5,8}},
        rotation=-90,
        origin={215,-68})));
  Modelica.Blocks.Logical.Switch modifier_switch1
    annotation (Placement(transformation(extent={{5,-5},{-5,5}},
        rotation=0,
        origin={97,-35})));
  Modelica.Blocks.Sources.RealExpression storage_on(y=0.04)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{5,-8},{-5,8}},
        rotation=0,
        origin={123,-28})));
  Modelica.Blocks.Sources.RealExpression storage_off(y=flow_mini)
    "Transform time in secondes to hours"
    annotation (Placement(transformation(extent={{-5,8},{5,-8}},
        rotation=180,
        origin={123,-42})));
  SolarSystemyy.Classes.Control.Bloc.storage_solar_control
    storage_solar_control(
    pumpControl_S6(start=true),
    pumpControl_S5(start=false),
    V3Vsolar_pause(displayUnit="s") = 200,
    pumpS5_pause(displayUnit="s") = 200,
    pumpS6_pause(displayUnit="s") = 200)
    annotation (Placement(transformation(extent={{48,-126},{122,-90}})));
  Buildings.Fluid.Sensors.MassFlowRate flow_solarTank(redeclare package Medium
      = MediumE)
    annotation (Placement(transformation(extent={{142,80},{154,92}})));
  Buildings.Fluid.Sensors.MassFlowRate flow_storageTank(redeclare package
      Medium = MediumE)                                   annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={50,66})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal(
    redeclare package Medium =
        MediumE,
    dp_nominal={dpPip_nominal,0,0},
    m_flow_nominal=flow_nominal*{-1,1,-1},
    T_start=313.15) "Flow splitter"                    annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={50,86})));

  SolarSystemyy.Classes.Other.Water_Drawing_up_control Drawing_up1(
    k=0.8,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    table=[0,0,0; 5.99,0,0; 6,150,0.04; 8.99,150,0.04; 9,100,0.05; 9.99,100,
        0.05; 10,0,0; 10.99,0,0; 11,0,0; 11.99,0,0; 12,200,0.07; 13.49,200,
        0.07; 13.5,0,0; 14.49,0,0; 14.5,0,0; 15.49,0,0; 15.5,0,0; 16.49,0,0;
        16.5,0,0; 17.49,0,0; 17.5,0,0; 17.99,0,0; 18,0,0; 18.99,0,0; 19,150,
        0.05; 21,150,0.05; 21.01,0,0; 24,0,0],
    T(displayUnit="s") = 5)
    annotation (Placement(transformation(extent={{142,42},{168,68}})));
  Buildings.Fluid.Sensors.MassFlowRate flow_solarPanel_out(redeclare package
      Medium = MediumE)
    annotation (Placement(transformation(extent={{10,56},{24,70}})));
  SolarSystemyy.Classes.Control.Horloge horloge(Heure_debut=0)
    annotation (Placement(transformation(extent={{240,100},{260,120}})));

  Buildings.Fluid.Sensors.MassFlowRate flow_solarPanel_out1(redeclare package
      Medium =         MediumE)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-70,12})));
  Buildings.Fluid.Actuators.Valves.ThreeWayLinear solar_valve(
    fraK=0.5,
    l={0.001,0.001},
    redeclare package Medium = MediumE,
    m_flow_nominal=0.04,
    dpValve_nominal=6000,
    T_start=313.15)
    annotation (Placement(transformation(extent={{-20,-46},{0,-66}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T2(redeclare package Medium =
        MediumE, m_flow_nominal=0.04)
    annotation (Placement(transformation(extent={{40,-66},{20,-46}})));
  Modelica.Blocks.Math.BooleanToReal ratio annotation (Placement(transformation(
        extent={{7,7},{-7,-7}},
        rotation=0,
        origin={85,-75})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal2(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=flow_nominal*{1,-1,1},
    dp_nominal={dpPip_nominal,0,dpPip_nominal},
    T_start=313.15) "Flow splitter"                    annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={66,-56})));
equation
  connect(Meteo.weaBus, solarPanel_ISO.weaBus) annotation (Line(
      points={{-34,102},{-46,102},{-46,79.32}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(solarPanel_ISO.port_b, T1.port) annotation (Line(
      points={{-12,63},{-7,63},{-7,76},{-6,76}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary1.ports[1], Strorage_tank.port_b) annotation (Line(
      points={{116,10},{104,10}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Strorage_tank.port_a, boundary.ports[1]) annotation (Line(
      points={{66,10},{58,10}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(T5.T, boundary1.T_in) annotation (Line(
      points={{58,51},{40,51},{40,34},{134,34},{134,12.4},{129.2,12.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Strorage_tank.heaPorSid, Troom1.port) annotation (Line(
      points={{95.64,10},{102,10},{102,16},{112,16},{112,4},{122,4},{122,-5},{120,
          -5}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Strorage_tank.heaPorTop, Troom1.port) annotation (Line(
      points={{88.8,26.28},{88.8,32},{106,32},{106,16},{112,16},{112,4},{122,4},
          {122,-5},{120,-5}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Strorage_tank.port_b1, C6.port_a) annotation (Line(
      points={{66,-7.6},{66,-22}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Solar_tank.port_b1, C5.port_a) annotation (Line(
      points={{180,-11.6},{180,-28}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Solar_tank.heaPorSid, Troom2.port) annotation (Line(
      points={{209.64,6},{214,6},{214,16},{232,16},{232,-3},{230,-3}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Solar_tank.heaPorTop, Troom2.port) annotation (Line(
      points={{202.8,22.28},{202.8,34},{214,34},{214,16},{232,16},{232,-3},
          {230,-3}},
      color={191,0,0},
      smooth=Smooth.None));

  connect(solar_off.y, modifier_switch.u3) annotation (Line(
      points={{199,-62.5},{199,-56},{203,-56},{203,-51}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solar_on.y, modifier_switch.u1) annotation (Line(
      points={{215,-62.5},{215,-58},{211,-58},{211,-51}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(modifier_switch.y, C5.m_flow_in) annotation (Line(
      points={{207,-39.5},{207,-37.8},{192,-37.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(modifier_switch1.y, C6.m_flow_in) annotation (Line(
      points={{91.5,-35},{85.75,-35},{85.75,-31.8},{78,-31.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(storage_off.y, modifier_switch1.u3) annotation (Line(
      points={{117.5,-42},{110,-42},{110,-39},{103,-39}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(storage_on.y, modifier_switch1.u1) annotation (Line(
      points={{117.5,-28},{108,-28},{108,-31},{103,-31}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T1.T, storage_solar_control.T1) annotation (Line(
      points={{1,86},{18,86},{18,114},{-96,114},{-96,-91.8},{48,-91.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(T3.T, storage_solar_control.T3) annotation (Line(
      points={{212,43},{268,43},{268,-130},{10,-130},{10,-97.2},{48,-97.2}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T4.T, storage_solar_control.T4) annotation (Line(
      points={{212,67},{272,67},{272,-140},{16,-140},{16,-102.6},{48,-102.6}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(T5.T, storage_solar_control.T5) annotation (Line(
      points={{58,51},{4,51},{4,-108},{48,-108}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(storage_solar_control.pumpControl_S6, modifier_switch1.u2)
    annotation (Line(
      points={{129.4,-108},{152,-108},{152,-35},{103,-35}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(storage_solar_control.pumpControl_S5, modifier_switch.u2)
    annotation (Line(
      points={{129.4,-120.6},{207,-120.6},{207,-51}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(flow_solarTank.port_b, Solar_tank.port_a1)
                                                 annotation (Line(
      points={{154,86},{174,86},{174,-2.36},{180,-2.36}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(splitVal.port_1, flow_solarTank.port_a)
                                              annotation (Line(
      points={{56,86},{142,86}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(splitVal.port_3, flow_storageTank.port_a)
                                                annotation (Line(
      points={{50,80},{50,72}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(flow_storageTank.port_b, Strorage_tank.port_a1)
                                                      annotation (Line(
      points={{50,60},{50,24},{62,24},{62,1.64},{66,1.64}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(solarPanel_ISO.port_b, flow_solarPanel_out.port_a) annotation (Line(
      points={{-12,63},{10,63}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(flow_solarPanel_out.port_b, splitVal.port_2) annotation (Line(
      points={{24,63},{34,63},{34,86},{44,86}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(flow_solarPanel_out1.port_b, solarPanel_ISO.port_a) annotation (
      Line(
      points={{-70,18},{-70,63},{-46,63}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Solar_tank.heaPorVol[2], T4.port) annotation (Line(
      points={{199,4.878},{186,4.878},{186,67},{198,67}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Solar_tank.port_a, Drawing_up1.port_a) annotation (Line(
      points={{180,6},{155,6},{155,42.1625}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Cool_water.ports[1], Solar_tank.port_b) annotation (Line(
      points={{248,-54},{248,8},{218,8},{218,6}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Solar_tank.heaPorVol[18], T3.port) annotation (Line(
      points={{199,6.99},{192,6.99},{192,4},{186,4},{186,43},{198,43}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Strorage_tank.heaPorVol[4], T5.port) annotation (Line(
      points={{85,9.142},{78,9.142},{78,51},{72,51}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(T2.port_b, solar_valve.port_2)
                                 annotation (Line(
      points={{20,-56},{0,-56}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(solar_valve.port_1, flow_solarPanel_out1.port_a)
                                                   annotation (Line(
      points={{-20,-56},{-70,-56},{-70,6}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(solar_valve.port_1, SurgeTank.port_a)
                                        annotation (Line(
      points={{-20,-56},{-42,-56},{-42,-40}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(solar_valve.port_3, Strorage_tank.port_b1)
                                             annotation (Line(
      points={{-10,-46},{-10,-7.6},{66,-7.6}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(ratio.y, solar_valve.y)
                          annotation (Line(
      points={{77.3,-75},{-10,-75},{-10,-68}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solar_valve.y_actual, storage_solar_control.solar_valve_pos)
    annotation (Line(
      points={{-5,-63},{2,-63},{2,-120.6},{48,-120.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(C6.port_b, splitVal2.port_3) annotation (Line(
      points={{66,-42},{66,-50}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(C5.port_b, splitVal2.port_1) annotation (Line(
      points={{180,-48},{180,-56},{72,-56}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(splitVal2.port_2, T2.port_a) annotation (Line(
      points={{60,-56},{40,-56}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(storage_solar_control.V3V_solar, ratio.u) annotation (Line(
      points={{129.4,-95.4},{144,-95.4},{144,-75},{93.4,-75}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-100,-140},{300,120}},
          preserveAspectRatio=true), graphics), Icon(coordinateSystem(
          extent={{-100,-140},{300,120}})),
    experiment(
      StopTime=3.1536e+007,
      Interval=120,
      Tolerance=1e-007,
      Algorithm="Esdirk23a"),
    __Dymola_experimentSetupOutput);
end Solar_solis_splitter;
