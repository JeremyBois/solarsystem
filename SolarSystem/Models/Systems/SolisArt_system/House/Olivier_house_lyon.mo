within SolarSystem.Models.Systems.SolisArt_system.House;
model Olivier_house_lyon
  "Basic test with light-weight construction and free floating temperature"
  import SolarSystem;
  extends Modelica.Icons.Example;

  package MediumA =
      Buildings.Media.Air "Medium model";
  parameter Modelica.SIunits.Angle S_=
    Buildings.Types.Azimuth.S "Azimuth for south walls";
  parameter Modelica.SIunits.Angle E_=
    Buildings.Types.Azimuth.E "Azimuth for east walls";
  parameter Modelica.SIunits.Angle W_=
    Buildings.Types.Azimuth.W "Azimuth for west walls";
  parameter Modelica.SIunits.Angle N_=
    Buildings.Types.Azimuth.N "Azimuth for north walls";
  parameter Modelica.SIunits.Angle C_=
    Buildings.Types.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    Buildings.Types.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle Z_=
    Buildings.Types.Tilt.Wall "Tilt for wall";
  parameter Integer nConExtWin = 4 "Number of constructions with a window";
  parameter Integer nConBou = 1
    "Number of surface that are connected to constructions that are modeled inside the room";
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{-100,80},{-80,100}})));

  SolarSystem.House.MixedAir
                           roo(
    redeclare package Medium = MediumA,
    nConExtWin=nConExtWin,
    nConBou=1,
    nPorts=3,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    nSurBou=0,
    linearizeRadiation=false,
    lat=weaDat.lat,
    nConExt=1,
    nConPar=2,
    hRoo=5,
    datConExt(
      layers={Roof},
      A={70},
      til={C_},
      azi={S_}),
    datConBou(
      layers={Floor},
      each A=65,
      each til=F_),
    AFlo=65,
    datConPar(
      layers={Partition,Partition},
      A={19.19,65},
      til={Z_,C_}),
    C_inHouse=partition_int.c*partition_int.d*3,
    datConExtWin(
      layers={Wall,Wall,Wall,Wall},
      A={24.29,24.29,24.29,20.89},
      glaSys={GlaSys_Argon,GlaSys_Argon,GlaSys_Argon,GlaSys_Argon},
      wWin={3,3,3,1.7},
      hWin={1.7,1.7,1.7,1},
      each fFra=0.1,
      each til=Z_,
      azi={W_,S_,E_,N_})) "Room model for Case 600"
    annotation (Placement(transformation(extent={{42,-104},{72,-74}})));
  Modelica.Blocks.Sources.Constant qConGai_flow(k=50/67.5)
    "Convective heat gain"
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  Modelica.Blocks.Sources.Constant qRadGai_flow(k=50/67.5)
    "Radiative heat gain"
    annotation (Placement(transformation(extent={{-60,80},{-40,100}})));
  Modelica.Blocks.Routing.Multiplex3 multiplex3_1
    annotation (Placement(transformation(extent={{-8,42},{8,58}})));
  Modelica.Blocks.Sources.Constant qLatGai_flow(k=0) "Latent heat gain"
    annotation (Placement(transformation(extent={{-60,0},{-40,20}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
      computeWetBulbTemperature=false, filNam="D:/Github/solarsystem/Meteo/Lyon/FRA_Lyon.074810_IWEC.mos")
    annotation (Placement(transformation(extent={{104,-168},{92,-156}})));
  Modelica.Blocks.Sources.Constant uSha(k=0)
    "Control signal for the shading device"
    annotation (Placement(transformation(extent={{-8,84},{4,96}})));
  Modelica.Blocks.Routing.Replicator replicator(nout=max(1,nConExtWin))
    annotation (Placement(transformation(extent={{12,86},{20,94}})));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature TSoi[nConBou](each T=
        283.15) "Boundary condition for construction"
                                          annotation (Placement(transformation(
        extent={{0,0},{-8,8}},
        rotation=0,
        origin={78,-126})));
  Buildings.HeatTransfer.Conduction.SingleLayer soi(
    steadyStateInitial=true,
    A=67.5,
    redeclare SolarSystem.Data.Parameter.Soil material,
    T_a_start=283.15,
    T_b_start=283.75) "2m deep soil (per definition on p.4 of ASHRAE 140-2007)"
    annotation (Placement(transformation(
        extent={{5,-5},{-3,3}},
        rotation=-90,
        origin={63,-109})));
  Buildings.Fluid.Sources.MassFlowSource_T sinInf(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{10,-140},{22,-128}})));
  Buildings.Fluid.Sources.Outside souInf(redeclare package Medium = MediumA,
      nPorts=1) "Source model for air infiltration"
           annotation (Placement(transformation(extent={{-18,-108},{-6,-96}})));
  Modelica.Blocks.Sources.Constant InfiltrationRate(k=-150/3600)
    "0.41 ACH adjusted for the altitude (0.5 at sea level)"
    annotation (Placement(transformation(extent={{-100,-160},{-92,-152}})));
  Modelica.Blocks.Math.Product product
    annotation (Placement(transformation(extent={{-44,-134},{-34,-124}})));
  Buildings.Fluid.Sensors.Density density(redeclare package Medium = MediumA)
    "Air density inside the building"
    annotation (Placement(transformation(extent={{-34,-150},{-44,-140}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{2,-170},{18,-154}})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM   heaCoo(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    m_flow_nominal=67.5*1.5*1.2/3600) "Heater and cooler"
    annotation (Placement(transformation(extent={{10,-108},{22,-96}})));
  Modelica.Blocks.Math.MultiSum multiSum(nu=1)
    annotation (Placement(transformation(extent={{-72,-154},{-60,-142}})));

  Modelica.Blocks.Math.Mean TRooHou1(
                                    f=1/3600, y(start=293.15))
    "Hourly averaged room air temperature"
    annotation (Placement(transformation(extent={{160,20},{180,40}})));
  Modelica.Blocks.Math.Mean TRooAnn1(              y(start=293.15), f=1/
        3.1536e+07) "Annual averaged room air temperature"
    annotation (Placement(transformation(extent={{160,-60},{180,-40}})));
  Modelica.Blocks.Math.Mean TRooDay(y(start=293.15), f=1/3600/24)
    "Month averaged room air temperature"
    annotation (Placement(transformation(extent={{160,-20},{180,0}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TRooAir
    "Room air temperature"
    annotation (Placement(transformation(extent={{102,-92},{118,-76}})));
  Buildings.Controls.Continuous.LimPID conHea(
    Td=60,
    initType=Modelica.Blocks.Types.InitPID.InitialState,
    Ti=300,
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=0.1) "Controller for heating"
    annotation (Placement(transformation(extent={{-68,-32},{-60,-24}})));
  Modelica.Blocks.Math.Gain gaiHea(k=1E6) "Gain for heating"
    annotation (Placement(transformation(extent={{-56,-32},{-48,-24}})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow preHea
    "Prescribed heat flow for heating and cooling"
    annotation (Placement(transformation(extent={{-2,-44},{10,-32}})));
  Modelica.Blocks.Continuous.Integrator EHea(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{-16,-30},{0,-14}})));
  SolarSystem.Utilities.Other.Schedule TSetHea(table=[
        0.0,273.15 + 20]) "Heating setpoint"
    annotation (Placement(transformation(extent={{-88,-32},{-80,-24}})));
  Modelica.Blocks.Math.Mean PHea(f=1/3600) "Hourly averaged heating power"
    annotation (Placement(transformation(extent={{-16,-10},{0,6}})));
  Utilities.Other.MiniAverage miniAverage(filNam=weaDat.filNam)
    annotation (Placement(transformation(extent={{120,-140},{180,-100}})));
protected
  SolarSystem.Data.Parameter.Construction.Layers.DoubleClearArgon16
                                 GlaSys_Argon
    annotation (Placement(transformation(extent={{106,64},{126,84}})));
  SolarSystem.Data.Parameter.Construction.Layers.Wood_part
                                    partition_int(x=2)
    annotation (Placement(transformation(extent={{150,64},{170,84}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Wall(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=3,
    material={SolarSystem.Data.Parameter.Construction.Layers.WoodPanel(x=0.04),
        SolarSystem.Data.Parameter.Construction.Layers.GlassWool(x=0.08),
        SolarSystem.Data.Parameter.Construction.Layers.WoodPanel(x=0.04)})
    "Wall layers"
    annotation (Placement(transformation(extent={{40,44},{60,64}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Roof(nLay=2, material=
       {SolarSystem.Data.Parameter.Construction.Layers.WoodPanel(x=0.1),
        SolarSystem.Data.Parameter.Construction.Layers.GlassWool(x=0.1)})
    "Roof layers"
    annotation (Placement(transformation(extent={{60,64},{80,84}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Floor(nLay=2,
      material={SolarSystem.Data.Parameter.Construction.Layers.GlassWool(x=0.05),
        Buildings.HeatTransfer.Data.Solids.Concrete(x=0.3)}) "Floor layer"
    annotation (Placement(transformation(extent={{60,24},{80,44}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Partition(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=2,
    material={SolarSystem.Data.Parameter.Construction.Layers.WoodPanel(x=0.03),
        Buildings.HeatTransfer.Data.Solids.Concrete(x=0.2)}) "Wall layers"
    annotation (Placement(transformation(extent={{80,44},{100,64}})));
equation
  connect(qRadGai_flow.y,multiplex3_1. u1[1])  annotation (Line(
      points={{-39,90},{-20,90},{-20,55.6},{-9.6,55.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qLatGai_flow.y,multiplex3_1. u3[1])  annotation (Line(
      points={{-39,10},{-20,10},{-20,44.4},{-9.6,44.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiplex3_1.y, roo.qGai_flow) annotation (Line(
      points={{8.8,50},{20,50},{20,-83},{40.5,-83}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.uSha, replicator.y) annotation (Line(
      points={{40.5,-77},{24,-77},{24,90},{20.4,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qConGai_flow.y, multiplex3_1.u2[1]) annotation (Line(
      points={{-39,50},{-9.6,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(weaDat.weaBus, roo.weaBus)  annotation (Line(
      points={{92,-162},{86.07,-162},{86.07,-75.575},{70.425,-75.575}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(uSha.y, replicator.u) annotation (Line(
      points={{4.6,90},{11.2,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(product.y, sinInf.m_flow_in)       annotation (Line(
      points={{-33.5,-129},{-30,-129},{-30,-129.2},{10,-129.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(density.port, roo.ports[1])  annotation (Line(
      points={{-39,-150},{38,-150},{38,-98.5},{45.75,-98.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(density.d, product.u2) annotation (Line(
      points={{-44.5,-145},{-50,-145},{-50,-132},{-45,-132}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(weaDat.weaBus, weaBus) annotation (Line(
      points={{92,-162},{10,-162}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(TSoi[1].port, soi.port_a) annotation (Line(
      points={{70,-122},{62,-122},{62,-114}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(soi.port_b, roo.surf_conBou[1]) annotation (Line(
      points={{62,-106},{62,-101},{61.5,-101}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(weaBus, souInf.weaBus)        annotation (Line(
      points={{10,-162},{-24,-162},{-24,-101.88},{-18,-101.88}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(souInf.ports[1], heaCoo.port_a)        annotation (Line(
      points={{-6,-102},{10,-102}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(heaCoo.port_b, roo.ports[2])  annotation (Line(
      points={{22,-102},{32,-102},{32,-96.5},{45.75,-96.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(sinInf.ports[1], roo.ports[3])        annotation (Line(
      points={{22,-134},{36,-134},{36,-94.5},{45.75,-94.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(multiSum.y, product.u1) annotation (Line(
      points={{-58.98,-148},{-48,-148},{-48,-126},{-45,-126}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(TRooAir.T, TRooDay.u) annotation (Line(
      points={{118,-84},{136,-84},{136,-10},{158,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooHou1.u) annotation (Line(
      points={{118,-84},{136,-84},{136,30},{158,30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooAnn1.u) annotation (Line(
      points={{118,-84},{136,-84},{136,-50},{158,-50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.heaPorAir, TRooAir.port) annotation (Line(
      points={{56.25,-89},{74.125,-89},{74.125,-84},{102,-84}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(conHea.y,gaiHea. u) annotation (Line(
      points={{-59.6,-28},{-56.8,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(EHea.u,gaiHea. y) annotation (Line(
      points={{-17.6,-22},{-28,-22},{-28,-28},{-47.6,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TSetHea.y[1],conHea. u_s) annotation (Line(
      points={{-79.6,-28},{-68.8,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(PHea.u,gaiHea. y) annotation (Line(
      points={{-17.6,-2},{-38,-2},{-38,-28},{-47.6,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, conHea.u_m) annotation (Line(
      points={{118,-84},{124,-84},{124,-70},{-74,-70},{-74,-38},{-64,-38},{
          -64,-32.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(preHea.port, roo.heaPorAir) annotation (Line(
      points={{10,-38},{48,-38},{48,-76},{56.25,-76},{56.25,-89}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(gaiHea.y, preHea.Q_flow) annotation (Line(
      points={{-47.6,-28},{-38,-28},{-38,-38},{-2,-38}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(InfiltrationRate.y, multiSum.u[1]) annotation (Line(
      points={{-91.6,-156},{-80,-156},{-80,-148},{-72,-148}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (
Documentation(info="<html>
<p>
This model is used for the test case 600FF of the BESTEST validation suite.
Case 600FF is a light-weight building.
The room temperature is free floating.
</p>
</html>", revisions="<html>
<ul>
<li>
October 9, 2013, by Michael Wetter:<br/>
Implemented soil properties using a record so that <code>TSol</code> and
<code>TLiq</code> are assigned.
This avoids an error when the model is checked in the pedantic mode.
</li>
<li>
July 15, 2012, by Michael Wetter:<br/>
Added reference results.
Changed implementation to make this model the base class
for all BESTEST cases.
Added computation of hourly and annual averaged room air temperature.
<li>
October 6, 2011, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-100,-180},{200,100}},
          preserveAspectRatio=false),
            graphics),
    Icon(coordinateSystem(extent={{-100,-180},{200,100}})),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end Olivier_house_lyon;
