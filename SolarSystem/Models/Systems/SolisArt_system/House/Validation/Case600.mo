within SolarSystem.Models.Systems.SolisArt_system.House.Validation;
model Case600
  "Basic test with light-weight construction and free floating temperature"
  extends Modelica.Icons.Example;

  package MediumA = Buildings.Media.GasesConstantDensity.SimpleAir
    "Medium model";
  parameter Modelica.SIunits.Angle S_=
    Buildings.HeatTransfer.Types.Azimuth.S "Azimuth for south walls";
  parameter Modelica.SIunits.Angle E_=
    Buildings.HeatTransfer.Types.Azimuth.E "Azimuth for east walls";
  parameter Modelica.SIunits.Angle W_=
    Buildings.HeatTransfer.Types.Azimuth.W "Azimuth for west walls";
  parameter Modelica.SIunits.Angle N_=
    Buildings.HeatTransfer.Types.Azimuth.N "Azimuth for north walls";
  parameter Modelica.SIunits.Angle C_=
    Buildings.HeatTransfer.Types.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    Buildings.HeatTransfer.Types.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle Z_=
    Buildings.HeatTransfer.Types.Tilt.Wall "Tilt for wall";
  parameter Integer nConExtWin = 4 "Number of constructions with a window";
  parameter Integer nConBou = 1
    "Number of surface that are connected to constructions that are modeled inside the room";
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{-98,82},{-82,98}})));
   parameter Buildings.HeatTransfer.Data.Solids.Generic soil(
    k=1.9,
    c=790,
    d=1900,
    x=3) "Soil properties"
    annotation (Placement(transformation(extent={{40,22},{60,42}})));

  Buildings.Rooms.MixedAir roo(
    redeclare package Medium = MediumA,
    hRoo=2.7,
    nConExtWin=nConExtWin,
    nConBou=1,
    nPorts=3,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    nConPar=0,
    nSurBou=0,
    linearizeRadiation=false,
    lat=weaDat.lat,
    datConBou(
      layers={Floor},
      each A=67.5,
      each til=F_),
    AFlo=67.5,
    datConExt(
      layers={Roof},
      A={67.5},
      til={C_},
      azi={S_}),
    nConExt=1,
    datConExtWin(
      layers={Wall,Wall,Wall,Wall},
      each A=19.19,
      glaSys={GlaSys_Argon,GlaSys_Argon,GlaSys_Argon,GlaSys_Argon},
      wWin={3,3,3,1.7},
      hWin={1.7,1.7,1.7,1},
      each fFra=0.1,
      each til=Z_,
      azi={W_,S_,E_,N_})) "Room model for Case 600"
    annotation (Placement(transformation(extent={{42,-104},{72,-74}})));
  Modelica.Blocks.Sources.Constant qConGai_flow(k=80/67.5)
    "Convective heat gain"
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  Modelica.Blocks.Sources.Constant qRadGai_flow(k=120/67.5)
    "Radiative heat gain"
    annotation (Placement(transformation(extent={{-60,80},{-40,100}})));
  Modelica.Blocks.Routing.Multiplex3 multiplex3_1
    annotation (Placement(transformation(extent={{-8,42},{8,58}})));
  Modelica.Blocks.Sources.Constant qLatGai_flow(k=0) "Latent heat gain"
    annotation (Placement(transformation(extent={{-60,0},{-40,20}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(filNam=
        "modelica://Buildings/Resources/weatherdata/DRYCOLD.mos")
    annotation (Placement(transformation(extent={{104,-168},{92,-156}})));
  Modelica.Blocks.Sources.Constant uSha(k=0)
    "Control signal for the shading device"
    annotation (Placement(transformation(extent={{-4,86},{4,94}})));
  Modelica.Blocks.Routing.Replicator replicator(nout=max(1,nConExtWin))
    annotation (Placement(transformation(extent={{12,86},{20,94}})));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature TSoi[nConBou](each T=
        283.15) "Boundary condition for construction"
                                          annotation (Placement(transformation(
        extent={{0,0},{-8,8}},
        rotation=0,
        origin={78,-126})));
  Buildings.HeatTransfer.Conduction.SingleLayer soi(
    material=soil,
    steadyStateInitial=true,
    A=67.5,
    T_a_start=283.15,
    T_b_start=283.75) "2m deep soil (per definition on p.4 of ASHRAE 140-2007)"
    annotation (Placement(transformation(
        extent={{5,-5},{-3,3}},
        rotation=-90,
        origin={63,-109})));
  Buildings.Fluid.Sources.MassFlowSource_T sinInf(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{10,-140},{22,-128}})));
  Buildings.Fluid.Sources.Outside souInf(redeclare package Medium = MediumA,
      nPorts=1) "Source model for air infiltration"
           annotation (Placement(transformation(extent={{-18,-108},{-6,-96}})));
  Modelica.Blocks.Sources.Constant InfiltrationRate(k=-67.5*1.5/3600)
    "0.41 ACH adjusted for the altitude (0.5 at sea level)"
    annotation (Placement(transformation(extent={{-90,-152},{-82,-144}})));
  Modelica.Blocks.Math.Product product
    annotation (Placement(transformation(extent={{-44,-134},{-34,-124}})));
  Buildings.Fluid.Sensors.Density density(redeclare package Medium = MediumA)
    "Air density inside the building"
    annotation (Placement(transformation(extent={{-34,-150},{-44,-140}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{2,-170},{18,-154}})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM   heaCoo(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    m_flow_nominal=67.5*1.5*1.2/3600) "Heater and cooler"
    annotation (Placement(transformation(extent={{10,-108},{22,-96}})));
  replaceable parameter
    Buildings.Rooms.Examples.BESTEST.Data.StandardResultsFreeFloating
      staRes(
        minT( Min=-18.8+273.15, Max=-15.6+273.15, Mean=-17.6+273.15),
        maxT( Min=64.9+273.15,  Max=69.5+273.15,  Mean=66.2+273.15),
        meanT(Min=24.2+273.15,  Max=25.9+273.15,  Mean=25.1+273.15))
          constrainedby Modelica.Icons.Record
    "Reference results from ASHRAE/ANSI Standard 140"
    annotation (Placement(transformation(extent={{84,22},{98,36}})));
  Modelica.Blocks.Math.MultiSum multiSum(nu=1)
    annotation (Placement(transformation(extent={{-72,-154},{-60,-142}})));

  Modelica.Blocks.Math.Mean TRooHou1(
                                    f=1/3600, y(start=293.15))
    "Hourly averaged room air temperature"
    annotation (Placement(transformation(extent={{160,20},{180,40}})));
  Modelica.Blocks.Math.Mean TRooAnn1(              y(start=293.15), f=1/
        3.1536e+07) "Annual averaged room air temperature"
    annotation (Placement(transformation(extent={{160,-60},{180,-40}})));
  Modelica.Blocks.Math.Mean TRooDay(y(start=293.15), f=1/3600/24)
    "Month averaged room air temperature"
    annotation (Placement(transformation(extent={{160,-20},{180,0}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TRooAir
    "Room air temperature"
    annotation (Placement(transformation(extent={{102,-92},{118,-76}})));
  Buildings.Controls.Continuous.LimPID conHea(
    Td=60,
    initType=Modelica.Blocks.Types.InitPID.InitialState,
    Ti=300,
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=0.1) "Controller for heating"
    annotation (Placement(transformation(extent={{-68,-32},{-60,-24}})));
  Buildings.Controls.Continuous.LimPID conCoo(
    Td=60,
    reverseAction=true,
    initType=Modelica.Blocks.Types.InitPID.InitialState,
    Ti=300,
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=0.1) "Controller for cooling"
    annotation (Placement(transformation(extent={{-68,-54},{-60,-46}})));
  Modelica.Blocks.Math.Gain gaiHea(k=1E6) "Gain for heating"
    annotation (Placement(transformation(extent={{-54,-32},{-46,-24}})));
  Modelica.Blocks.Math.Gain gaiCoo(k=-1E6) "Gain for cooling"
    annotation (Placement(transformation(extent={{-54,-54},{-46,-46}})));
  Modelica.Blocks.Math.Sum sum1(nin=2)
    annotation (Placement(transformation(extent={{-16,-42},{-8,-34}})));
  Modelica.Blocks.Routing.Multiplex2 multiplex2
    annotation (Placement(transformation(extent={{-32,-42},{-24,-34}})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow preHea
    "Prescribed heat flow for heating and cooling"
    annotation (Placement(transformation(extent={{-2,-44},{10,-32}})));
  Modelica.Blocks.Continuous.Integrator EHea(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{-16,-26},{-8,-18}})));
  Modelica.Blocks.Continuous.Integrator ECoo(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Cooling energy in Joules"
    annotation (Placement(transformation(extent={{-16,-54},{-8,-46}})));
  Buildings.Rooms.Examples.BESTEST.BaseClasses.DaySchedule TSetHea(table=[
        0.0,273.15 + 20]) "Heating setpoint"
    annotation (Placement(transformation(extent={{-88,-32},{-80,-24}})));
  Buildings.Rooms.Examples.BESTEST.BaseClasses.DaySchedule TSetCoo(table=[
        0.0,273.15 + 27]) "Cooling setpoint"
    annotation (Placement(transformation(extent={{-88,-54},{-80,-46}})));
  Modelica.Blocks.Math.Mean PHea(f=1/3600) "Hourly averaged heating power"
    annotation (Placement(transformation(extent={{-16,-14},{-8,-6}})));
  Modelica.Blocks.Math.Mean PCoo(f=1/3600) "Hourly averaged cooling power"
    annotation (Placement(transformation(extent={{-16,-68},{-8,-60}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Wall(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    material={SolarSystem.Data.Layers.WoodPanel(x=0.04),
        SolarSystem.Data.Layers.GlassWool(x=0.08),
        SolarSystem.Data.Layers.Wood(x=0.04)},
    nLay=3) "Wall layers"
    annotation (Placement(transformation(extent={{40,68},{52,80}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Roof(        nLay=
       2, material={SolarSystem.Data.Layers.Wood(x=0.1),
        SolarSystem.Data.Layers.GlassWool(x=0.1)}) "Roof layers"
    annotation (Placement(transformation(extent={{62,86},{74,98}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Floor(nLay=2, material=
       {SolarSystem.Data.Layers.GlassWool(x=0.05),
        Buildings.HeatTransfer.Data.Solids.Concrete(x=0.3)}) "Floor layer"
    annotation (Placement(transformation(extent={{62,50},{76,64}})));
  Data.Layers.DoubleClearArgon16 GlaSys_Argon
    annotation (Placement(transformation(extent={{82,68},{94,80}})));
equation
  connect(qRadGai_flow.y,multiplex3_1. u1[1])  annotation (Line(
      points={{-39,90},{-20,90},{-20,55.6},{-9.6,55.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qLatGai_flow.y,multiplex3_1. u3[1])  annotation (Line(
      points={{-39,10},{-20,10},{-20,44.4},{-9.6,44.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiplex3_1.y, roo.qGai_flow) annotation (Line(
      points={{8.8,50},{20,50},{20,-81.5},{36,-81.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.uSha, replicator.y) annotation (Line(
      points={{40.5,-77},{24,-77},{24,90},{20.4,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qConGai_flow.y, multiplex3_1.u2[1]) annotation (Line(
      points={{-39,50},{-9.6,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(weaDat.weaBus, roo.weaBus)  annotation (Line(
      points={{92,-162},{86.07,-162},{86.07,-75.575},{70.425,-75.575}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(uSha.y, replicator.u) annotation (Line(
      points={{4.4,90},{11.2,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(product.y, sinInf.m_flow_in)       annotation (Line(
      points={{-33.5,-129},{-30,-129},{-30,-129.2},{10,-129.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(density.port, roo.ports[1])  annotation (Line(
      points={{-39,-150},{38,-150},{38,-96.5},{43.75,-96.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(density.d, product.u2) annotation (Line(
      points={{-44.5,-145},{-50,-145},{-50,-132},{-45,-132}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(weaDat.weaBus, weaBus) annotation (Line(
      points={{92,-162},{10,-162}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(TSoi[1].port, soi.port_a) annotation (Line(
      points={{70,-122},{62,-122},{62,-114}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(soi.port_b, roo.surf_conBou[1]) annotation (Line(
      points={{62,-106},{62,-101},{61.5,-101}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(weaBus, souInf.weaBus)        annotation (Line(
      points={{10,-162},{-24,-162},{-24,-101.88},{-18,-101.88}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(souInf.ports[1], heaCoo.port_a)        annotation (Line(
      points={{-6,-102},{10,-102}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(heaCoo.port_b, roo.ports[2])  annotation (Line(
      points={{22,-102},{32,-102},{32,-96.5},{45.75,-96.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(sinInf.ports[1], roo.ports[3])        annotation (Line(
      points={{22,-134},{36,-134},{36,-96.5},{47.75,-96.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(multiSum.y, product.u1) annotation (Line(
      points={{-58.98,-148},{-48,-148},{-48,-126},{-45,-126}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(InfiltrationRate.y, multiSum.u[1]) annotation (Line(
      points={{-81.6,-148},{-72,-148}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(TRooAir.T, TRooDay.u) annotation (Line(
      points={{118,-84},{136,-84},{136,-10},{158,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooHou1.u) annotation (Line(
      points={{118,-84},{136,-84},{136,30},{158,30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooAnn1.u) annotation (Line(
      points={{118,-84},{136,-84},{136,-50},{158,-50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.heaPorAir, TRooAir.port) annotation (Line(
      points={{56.25,-89},{74.125,-89},{74.125,-84},{102,-84}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(conHea.y,gaiHea. u) annotation (Line(
      points={{-59.6,-28},{-54.8,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(conCoo.y,gaiCoo. u)  annotation (Line(
      points={{-59.6,-50},{-54.8,-50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiHea.y,multiplex2. u1[1]) annotation (Line(
      points={{-45.6,-28},{-38,-28},{-38,-35.6},{-32.8,-35.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiCoo.y,multiplex2. u2[1]) annotation (Line(
      points={{-45.6,-50},{-38,-50},{-38,-40.4},{-32.8,-40.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiplex2.y,sum1. u) annotation (Line(
      points={{-23.6,-38},{-16.8,-38}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sum1.y,preHea. Q_flow) annotation (Line(
      points={{-7.6,-38},{-2,-38}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(EHea.u,gaiHea. y) annotation (Line(
      points={{-16.8,-22},{-28,-22},{-28,-28},{-45.6,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ECoo.u,gaiCoo. y) annotation (Line(
      points={{-16.8,-50},{-45.6,-50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TSetHea.y[1],conHea. u_s) annotation (Line(
      points={{-79.6,-28},{-68.8,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TSetCoo.y[1],conCoo. u_s) annotation (Line(
      points={{-79.6,-50},{-68.8,-50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(PCoo.u,gaiCoo. y) annotation (Line(
      points={{-16.8,-64},{-38,-64},{-38,-50},{-45.6,-50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(PHea.u,gaiHea. y) annotation (Line(
      points={{-16.8,-10},{-38,-10},{-38,-28},{-45.6,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, conCoo.u_m) annotation (Line(
      points={{118,-84},{124,-84},{124,-70},{-64,-70},{-64,-54.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, conHea.u_m) annotation (Line(
      points={{118,-84},{124,-84},{124,-70},{-74,-70},{-74,-38},{-64,-38},{
          -64,-32.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(preHea.port, roo.heaPorAir) annotation (Line(
      points={{10,-38},{48,-38},{48,-76},{56.25,-76},{56.25,-89}},
      color={191,0,0},
      smooth=Smooth.None));
  annotation (
experiment(StopTime=3.1536e+07),
__Dymola_Commands(file="modelica://Buildings/Resources/Scripts/Dymola/Rooms/Examples/BESTEST/Case600FF.mos"
        "Simulate and plot"), Documentation(info="<html>
<p>
This model is used for the test case 600FF of the BESTEST validation suite.
Case 600FF is a light-weight building.
The room temperature is free floating.
</p>
</html>", revisions="<html>
<ul>
<li>
October 9, 2013, by Michael Wetter:<br/>
Implemented soil properties using a record so that <code>TSol</code> and
<code>TLiq</code> are assigned.
This avoids an error when the model is checked in the pedantic mode.
</li>
<li>
July 15, 2012, by Michael Wetter:<br/>
Added reference results.
Changed implementation to make this model the base class
for all BESTEST cases.
Added computation of hourly and annual averaged room air temperature.
<li>
October 6, 2011, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-100,-180},{200,100}},
          preserveAspectRatio=true),
            graphics),
    Icon(coordinateSystem(extent={{-100,-180},{200,100}})));
end Case600;
