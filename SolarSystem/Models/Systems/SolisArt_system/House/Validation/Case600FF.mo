within SolarSystem.Models.Systems.SolisArt_system.House.Validation;
model Case600FF
  "Basic test with light-weight construction and free floating temperature"
  extends Modelica.Icons.Example;

  package MediumA = Buildings.Media.GasesConstantDensity.SimpleAir
    "Medium model";
  parameter Modelica.SIunits.Angle S_=
    Buildings.HeatTransfer.Types.Azimuth.S "Azimuth for south walls";
  parameter Modelica.SIunits.Angle E_=
    Buildings.HeatTransfer.Types.Azimuth.E "Azimuth for east walls";
  parameter Modelica.SIunits.Angle W_=
    Buildings.HeatTransfer.Types.Azimuth.W "Azimuth for west walls";
  parameter Modelica.SIunits.Angle N_=
    Buildings.HeatTransfer.Types.Azimuth.N "Azimuth for north walls";
  parameter Modelica.SIunits.Angle C_=
    Buildings.HeatTransfer.Types.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    Buildings.HeatTransfer.Types.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle Z_=
    Buildings.HeatTransfer.Types.Tilt.Wall "Tilt for wall";
  parameter Integer nConExtWin = 4 "Number of constructions with a window";
  parameter Integer nConBou = 1
    "Number of surface that are connected to constructions that are modeled inside the room";
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{-98,82},{-82,98}})));
   parameter Buildings.HeatTransfer.Data.Solids.Generic soil(
    k=1.9,
    c=790,
    d=1900,
    x=3) "Soil properties"
    annotation (Placement(transformation(extent={{40,22},{60,42}})));

  Buildings.Rooms.MixedAir roo(
    redeclare package Medium = MediumA,
    hRoo=2.7,
    nConExtWin=nConExtWin,
    nConBou=1,
    nPorts=3,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    nConPar=0,
    nSurBou=0,
    linearizeRadiation=false,
    lat=weaDat.lat,
    datConBou(
      layers={Floor},
      each A=67.5,
      each til=F_),
    AFlo=67.5,
    nConExt=1,
    datConExt(
      layers={Roof},
      A={67.5},
      til={C_},
      azi={S_}),
    datConExtWin(
      layers={Wall,Wall,Wall,Wall},
      each A=19.19,
      glaSys={GlaSys_Argon,GlaSys_Argon,GlaSys_Argon,GlaSys_Argon},
      wWin={3,3,3,1.7},
      hWin={1.7,1.7,1.7,1},
      each fFra=0.1,
      each til=Z_,
      azi={W_,S_,E_,N_})) "Room model for Case 600"
    annotation (Placement(transformation(extent={{36,-30},{66,0}})));
  Modelica.Blocks.Sources.Constant qConGai_flow(k=80/67.5)
    "Convective heat gain"
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  Modelica.Blocks.Sources.Constant qRadGai_flow(k=120/67.5)
    "Radiative heat gain"
    annotation (Placement(transformation(extent={{-60,80},{-40,100}})));
  Modelica.Blocks.Routing.Multiplex3 multiplex3_1
    annotation (Placement(transformation(extent={{-8,42},{8,58}})));
  Modelica.Blocks.Sources.Constant qLatGai_flow(k=0) "Latent heat gain"
    annotation (Placement(transformation(extent={{-60,0},{-40,20}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(filNam=
        "modelica://Buildings/Resources/weatherdata/DRYCOLD.mos")
    annotation (Placement(transformation(extent={{98,-94},{86,-82}})));
  Modelica.Blocks.Sources.Constant uSha(k=0)
    "Control signal for the shading device"
    annotation (Placement(transformation(extent={{-4,86},{4,94}})));
  Modelica.Blocks.Routing.Replicator replicator(nout=max(1,nConExtWin))
    annotation (Placement(transformation(extent={{12,86},{20,94}})));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature TSoi[nConBou](each T=
        283.15) "Boundary condition for construction"
                                          annotation (Placement(transformation(
        extent={{0,0},{-8,8}},
        rotation=0,
        origin={72,-52})));
  Buildings.HeatTransfer.Conduction.SingleLayer soi(
    material=soil,
    steadyStateInitial=true,
    A=67.5,
    T_a_start=283.15,
    T_b_start=283.75) "2m deep soil (per definition on p.4 of ASHRAE 140-2007)"
    annotation (Placement(transformation(
        extent={{5,-5},{-3,3}},
        rotation=-90,
        origin={57,-35})));
  Buildings.Fluid.Sources.MassFlowSource_T sinInf(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{4,-66},{16,-54}})));
  Buildings.Fluid.Sources.Outside souInf(redeclare package Medium = MediumA,
      nPorts=1) "Source model for air infiltration"
           annotation (Placement(transformation(extent={{-24,-34},{-12,-22}})));
  Modelica.Blocks.Sources.Constant InfiltrationRate(k=-67.5*1.5/3600)
    "0.41 ACH adjusted for the altitude (0.5 at sea level)"
    annotation (Placement(transformation(extent={{-96,-78},{-88,-70}})));
  Modelica.Blocks.Math.Product product
    annotation (Placement(transformation(extent={{-50,-60},{-40,-50}})));
  Buildings.Fluid.Sensors.Density density(redeclare package Medium = MediumA)
    "Air density inside the building"
    annotation (Placement(transformation(extent={{-40,-76},{-50,-66}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{-4,-96},{12,-80}})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM   heaCoo(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    m_flow_nominal=67.5*1.5*1.2/3600) "Heater and cooler"
    annotation (Placement(transformation(extent={{4,-34},{16,-22}})));
  replaceable parameter
    Buildings.Rooms.Examples.BESTEST.Data.StandardResultsFreeFloating
      staRes(
        minT( Min=-18.8+273.15, Max=-15.6+273.15, Mean=-17.6+273.15),
        maxT( Min=64.9+273.15,  Max=69.5+273.15,  Mean=66.2+273.15),
        meanT(Min=24.2+273.15,  Max=25.9+273.15,  Mean=25.1+273.15))
          constrainedby Modelica.Icons.Record
    "Reference results from ASHRAE/ANSI Standard 140"
    annotation (Placement(transformation(extent={{84,22},{98,36}})));
  Modelica.Blocks.Math.MultiSum multiSum(nu=1)
    annotation (Placement(transformation(extent={{-78,-80},{-66,-68}})));

  Modelica.Blocks.Math.Mean TRooHou1(
                                    f=1/3600, y(start=293.15))
    "Hourly averaged room air temperature"
    annotation (Placement(transformation(extent={{160,20},{180,40}})));
  Modelica.Blocks.Math.Mean TRooAnn1(              y(start=293.15), f=1/
        3.1536e+07) "Annual averaged room air temperature"
    annotation (Placement(transformation(extent={{160,-60},{180,-40}})));
  Modelica.Blocks.Math.Mean TRooDay(y(start=293.15), f=1/3600/24)
    "Month averaged room air temperature"
    annotation (Placement(transformation(extent={{160,-20},{180,0}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TRooAir
    "Room air temperature"
    annotation (Placement(transformation(extent={{98,-18},{114,-2}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Wall(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    material={SolarSystem.Data.Layers.WoodPanel(x=0.04),
        SolarSystem.Data.Layers.GlassWool(x=0.08),
        SolarSystem.Data.Layers.Wood(x=0.04)},
    nLay=3) "Wall layers"
    annotation (Placement(transformation(extent={{54,66},{66,78}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Roof(        nLay=
       2, material={SolarSystem.Data.Layers.Wood(x=0.1),
        SolarSystem.Data.Layers.GlassWool(x=0.1)}) "Roof layers"
    annotation (Placement(transformation(extent={{76,84},{88,96}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Floor(nLay=2, material=
       {SolarSystem.Data.Layers.GlassWool(x=0.05),
        Buildings.HeatTransfer.Data.Solids.Concrete(x=0.3)}) "Floor layer"
    annotation (Placement(transformation(extent={{76,48},{90,62}})));
  Data.Layers.DoubleClearArgon16 GlaSys_Argon
    annotation (Placement(transformation(extent={{96,66},{108,78}})));
equation
  connect(qRadGai_flow.y,multiplex3_1. u1[1])  annotation (Line(
      points={{-39,90},{-20,90},{-20,55.6},{-9.6,55.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qLatGai_flow.y,multiplex3_1. u3[1])  annotation (Line(
      points={{-39,10},{-20,10},{-20,44.4},{-9.6,44.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiplex3_1.y, roo.qGai_flow) annotation (Line(
      points={{8.8,50},{20,50},{20,-7.5},{30,-7.5}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.uSha, replicator.y) annotation (Line(
      points={{34.5,-3},{24,-3},{24,90},{20.4,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qConGai_flow.y, multiplex3_1.u2[1]) annotation (Line(
      points={{-39,50},{-9.6,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(weaDat.weaBus, roo.weaBus)  annotation (Line(
      points={{86,-88},{80.07,-88},{80.07,-1.575},{64.425,-1.575}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(uSha.y, replicator.u) annotation (Line(
      points={{4.4,90},{11.2,90}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(product.y, sinInf.m_flow_in)       annotation (Line(
      points={{-39.5,-55},{-36,-55},{-36,-55.2},{4,-55.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(density.port, roo.ports[1])  annotation (Line(
      points={{-45,-76},{32,-76},{32,-22.5},{37.75,-22.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(density.d, product.u2) annotation (Line(
      points={{-50.5,-71},{-56,-71},{-56,-58},{-51,-58}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(weaDat.weaBus, weaBus) annotation (Line(
      points={{86,-88},{4,-88}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(TSoi[1].port, soi.port_a) annotation (Line(
      points={{64,-48},{56,-48},{56,-40}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(soi.port_b, roo.surf_conBou[1]) annotation (Line(
      points={{56,-32},{56,-27},{55.5,-27}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(weaBus, souInf.weaBus)        annotation (Line(
      points={{4,-88},{-30,-88},{-30,-27.88},{-24,-27.88}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(souInf.ports[1], heaCoo.port_a)        annotation (Line(
      points={{-12,-28},{4,-28}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(heaCoo.port_b, roo.ports[2])  annotation (Line(
      points={{16,-28},{26,-28},{26,-22.5},{39.75,-22.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(sinInf.ports[1], roo.ports[3])        annotation (Line(
      points={{16,-60},{30,-60},{30,-22.5},{41.75,-22.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(multiSum.y, product.u1) annotation (Line(
      points={{-64.98,-74},{-54,-74},{-54,-52},{-51,-52}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(InfiltrationRate.y, multiSum.u[1]) annotation (Line(
      points={{-87.6,-74},{-78,-74}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(TRooAir.T, TRooDay.u) annotation (Line(
      points={{114,-10},{158,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooHou1.u) annotation (Line(
      points={{114,-10},{136,-10},{136,30},{158,30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooAnn1.u) annotation (Line(
      points={{114,-10},{136,-10},{136,-50},{158,-50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.heaPorAir, TRooAir.port) annotation (Line(
      points={{50.25,-15},{74.125,-15},{74.125,-10},{98,-10}},
      color={191,0,0},
      smooth=Smooth.None));
  annotation (
experiment(StopTime=3.1536e+007, Tolerance=1e-007),
__Dymola_Commands(file="modelica://Buildings/Resources/Scripts/Dymola/Rooms/Examples/BESTEST/Case600FF.mos"
        "Simulate and plot"), Documentation(info="<html>
<p>
This model is used for the test case 600FF of the BESTEST validation suite.
Case 600FF is a light-weight building.
The room temperature is free floating.
</p>
</html>", revisions="<html>
<ul>
<li>
October 9, 2013, by Michael Wetter:<br/>
Implemented soil properties using a record so that <code>TSol</code> and
<code>TLiq</code> are assigned.
This avoids an error when the model is checked in the pedantic mode.
</li>
<li>
July 15, 2012, by Michael Wetter:<br/>
Added reference results.
Changed implementation to make this model the base class
for all BESTEST cases.
Added computation of hourly and annual averaged room air temperature.
<li>
October 6, 2011, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-100,-100},{200,100}},
          preserveAspectRatio=true),
            graphics),
    Icon(coordinateSystem(extent={{-100,-100},{200,100}})),
    __Dymola_experimentSetupOutput);
end Case600FF;
