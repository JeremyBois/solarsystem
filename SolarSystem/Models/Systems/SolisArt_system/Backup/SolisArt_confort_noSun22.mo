within SolarSystem.Models.Systems.SolisArt_system.Backup;
model SolisArt_confort_noSun22 "Simulation of SolisConfort system"
  import SolarSystem;

// Systems parameters (PUBLIC)
public
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal[2] = {40*Collector.nPanels*Collector.per.A/3600,
                                                               70*Collector.nPanels*Collector.per.A/3600}
    "Mass flow rate for left pumps ([1] and right pumps [2]";
  parameter Modelica.Media.Interfaces.PartialMedium.Temperature T_start=279.55
    "Start value of temperature";
  parameter Modelica.SIunits.Pressure dpPip_nominal = 1000
    "Pressure difference of pipe (without valve)";
  parameter Modelica.SIunits.Pressure dpValve_nominal=1000
    "Nominal pressure drop of fully open valve, used if CvData=Buildings.Fluid.Types.CvTypes.OpPoint";
  parameter Modelica.SIunits.Pressure dp_nominal[2]={7960, 17730}
    "Nominal pressure for left and right pumps";
  parameter Modelica.Blocks.Interfaces.RealOutput Cp_value=3608
    "Value of Real output";
  parameter Integer collector_seg(min=1) = 20
    "Collector discretization used inside control algorithms"
          annotation(Dialog(connectorSizing=true), HideResult=true);

// Algorithms (public)
  SolarSystem.Control.SolisArt_mod.Algo_one Control(
    V3V_extra_outter(start=0),
    BackupHeater_outter(start=1),
    WeatherFile=City_Infos.path,
    S4_outter(start=true),
    S5_outter(start=false),
    Sj_outter(start={true,false}),
    V3V_solar_outter(start=0),
    S6_outter(start=true),
    Tambiant_out_start={293.15,303.15}) "Control equipement state"
    annotation (Placement(transformation(extent={{64,-258},{188,-174}})));
  SolarSystem.Control.SolisArt_mod.Pump_speed.pump_pids Pump_Control(
    S4_outter(start=1),
    S6_outter(start=0),
    Sj_reverseAction=true,
    Sj_k=0.1,
    S6_yMin=0.21,
    S5_yMin=0.21,
    S4_yMin=0.21,
    Sj_controller=Modelica.Blocks.Types.SimpleController.PI,
    Sj_yMin=0.21,
    S6_controller=Modelica.Blocks.Types.SimpleController.PI,
    S5_controller=Modelica.Blocks.Types.SimpleController.PI,
    S4_controller=Modelica.Blocks.Types.SimpleController.PI,
    right_pumps=3000,
    left_pumps=3000,
    S5_outter(start=0),
    Sj_outter(start={1,0}),
    Sj_Ti=120,
    S6_k=0.1,
    S5_k=0.1,
    S6_Ti=40,
    S5_Ti=40,
    S4_Ti=50) "Control pump rotationnal speed"
    annotation (Placement(transformation(extent={{294,-248},{426,-170}})));

// Flow/Temp sensors (public)
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T3
    annotation (Placement(transformation(extent={{200,18},{214,32}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T4
    annotation (Placement(transformation(extent={{200,38},{214,52}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T5
    annotation (Placement(transformation(extent={{80,34},{68,46}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T7(redeclare package Medium =
        MediumE,
    T_start=T_start,
    tau=tau,
    m_flow_nominal=m_flow_nominal[2])
    annotation (Placement(transformation(extent={{314,-130},{294,-110}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T8(redeclare package Medium =
        MediumE,
    T_start=T_start,
    tau=tau,
    m_flow_nominal=m_flow_nominal[2])
    annotation (Placement(transformation(extent={{324,54},{344,74}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor House_TempSensor
    "Room air temperature"
    annotation (Placement(transformation(extent={{654,-30},{672,-12}})));

// Systems (public)
  SolarSystem.Solar_Collector.EN12975_TextInput
                                      Collector(
    redeclare package Medium = MediumE,
    til(displayUnit="deg") = 0.5235987755983,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Parallel,
    T_start=T_start,
    azi(displayUnit="rad") = 0,
    use_shaCoe_in=false,
    nPanels=6,
    homotopyInitialization=false,
    computeFlowResistance=true,
    from_dp=true,
    linearizeFlowResistance=false,
    deltaM=0.1,
    lat(displayUnit="rad") = City_Infos.lattitude,
    nSeg=collector_seg,
    shaCoe=1,
    per=SolarSystem.Data.Parameter.SolarCollector.IDMK2_5_buildings_NoSun(),
    m_flow(start=m_flow_nominal[1]),
    dp(start=m_flow_nominal[1]*m_flow_nominal[1]*1534.996356))
    annotation (Placement(transformation(extent={{-74,36},{-22,96}})));
  SolarSystem.Heat_transfer.BoilerPolynomial_Integrator
                                           Boiler(
    redeclare package Medium = MediumE,
    fue=Buildings.Fluid.Data.Fuels.NaturalGasHigherHeatingValue(),
    T_start=T_start,
    effCur=Buildings.Fluid.Types.EfficiencyCurves.Constant,
    linearizeFlowResistance=linearizeFlowResistance,
    dp_nominal=1000,
    m_flow_nominal=m_flow_nominal[2],
    a={0.779},
    Q_flow_nominal=15000)
              annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={272,24})));
  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex Buffer_Tank(
    redeclare package Medium = MediumE,
    redeclare package MediumHex = MediumE,
    hTan=1.6,
    dIns(displayUnit="mm") = 0.1,
    nSeg=20,
    m_flow_nominal=0.000001,
    tau(displayUnit="s") = 30,
    hHex_a=1.415,
    hHex_b=0.255,
    hexSegMult=3,
    Q_flow_nominal(displayUnit="kW") = 103000,
    mHex_flow_nominal=0.36,
    dExtHex(displayUnit="mm") = 0.0346,
    T_start=T_start,
    VTan(displayUnit="l") = 0.5,
    kIns=lambdaIns,
    TTan_nominal=283.15,
    THex_nominal=318.15)
    annotation (Placement(transformation(extent={{74,-26},{116,26}})));
  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex            Health_Tank(
    nSeg=20,
    tau(displayUnit="s") = 120,
    redeclare package Medium = MediumE,
    redeclare package MediumHex = MediumE,
    kIns=lambdaIns,
    m_flow_nominal=m_flow_nominal[1],
    Q_flow_nominal=53000,
    mHex_flow_nominal=0.36,
    VTan(displayUnit="l") = 0.3,
    hTan=1.3,
    dIns(displayUnit="mm") = 0.055,
    hHex_a=1.1,
    hHex_b=0.255,
    hexSegMult=3,
    dExtHex(displayUnit="mm") = 0.0337,
    TTan_nominal(displayUnit="degC") = 333.15,
    THex_nominal=353.15)
    annotation (Placement(transformation(extent={{236,-42},{188,14}})));

// Drawing up (public)
  SolarSystem.Valves.DrawingUp.Water_Drawing_up_control DrawingUp(
  redeclare package Medium = MediumE,
  f_cut=1/180,
  start_value(displayUnit="d") = 0,
  table=[0,0,0; 5.99,0,0; 6,45,0.01; 8.99,45,0.01; 9,0,0; 11.99,0,0; 12,45,
         0.01; 14.99,45,0.01; 15,0,0; 17.99,0,0; 18,185,0.035; 20.99,185,0.035;
         21,0,0; 24,0,0],
  gain=0.50)
  annotation (Placement(transformation(extent={{184,84},{236,112}})));
  Buildings.Fluid.Sources.Boundary_pT PipeNetwork_Source(
    redeclare package Medium = MediumE,
    nPorts=1,
    use_T_in=true,
    T=283.15) annotation (Placement(transformation(
        extent={{6,-6},{-6,6}},
        rotation=-90,
        origin={248,-90})));

// Pumps (public)
  Buildings.Fluid.Movers.SpeedControlled_Nrpm
                                            C6(
    redeclare package Medium =
        MediumE,
    motorCooledByFluid=true,
    T_start=T_start,
    tau=tau,
    dp(start=dp_nominal[1]),
    m_flow(start=m_flow_nominal[1]),
    riseTime=riseTime_pump,
    filteredSpeed=filteredSpeed,
    per=C6_5)       annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={60,-76})));

  Buildings.Fluid.Movers.SpeedControlled_Nrpm
                                            C5(
    redeclare package Medium =
        MediumE,
    motorCooledByFluid=true,
    T_start=T_start,
    tau=tau,
    dp(start=dp_nominal[1]),
    m_flow(start=m_flow_nominal[1]),
    riseTime=riseTime_pump,
    filteredSpeed=filteredSpeed,
    per=C6_5)       annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={130,-74})));

  Buildings.Fluid.Movers.SpeedControlled_Nrpm
                                            C4(
    redeclare package Medium =
        MediumE,
    motorCooledByFluid=true,
    T_start=T_start,
    tau=tau,
    dp(start=dp_nominal[2]),
    m_flow(start=m_flow_nominal[2]),
    riseTime=riseTime_pump,
    filteredSpeed=filteredSpeed,
    per=C4_2)       annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={370,-60})));
  Buildings.Fluid.Movers.SpeedControlled_Nrpm
                                            C2(
    redeclare package Medium =
        MediumE,
    motorCooledByFluid=true,
    T_start=T_start,
    tau=tau,
    dp(start=dp_nominal[2]),
    m_flow(start=m_flow_nominal[2]),
    riseTime=riseTime_pump,
    filteredSpeed=filteredSpeed,
    per=C4_2)       annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={438,-60})));

// Energy integrators (public)
  SolarSystem.Utilities.Other.Integrator_energy integrator_collector(Cp_value=MediumE.cp_const)
    annotation (Placement(transformation(extent={{-4,92},{34,110}})));
  SolarSystem.Utilities.Other.Integrator_energy_boiler
                                                integrator_boiler(Cp_value=MediumE.cp_const)
    annotation (Placement(transformation(extent={{322,20},{356,40}})));
  SolarSystem.Utilities.Other.Integrator_energy integrator_radiator(Cp_value=MediumE.cp_const)
    annotation (Placement(transformation(extent={{406,-28},{374,-10}})));
  SolarSystem.Utilities.Other.Integrator_energy integrator_drawingUp(Cp_value=MediumE.cp_const)
    annotation (Placement(transformation(extent={{168,66},{202,78}})));

// Flow resistances (public)
  Buildings.Fluid.FixedResistances.Pipe inFlow_Collector(
    redeclare package Medium = MediumE,
    thicknessIns(displayUnit="mm") = thicknessIns,
    nSeg=24,
    lambdaIns=lambdaIns,
    length=12,
    useMultipleHeatPorts=true,
    T_start=T_start,
    linearizeFlowResistance=linearizeFlowResistance,
    v_nominal=v_nominal,
    m_flow_nominal=m_flow_nominal[1],
    from_dp=false,
    dp_nominal=dpValve_nominal*2 - (k_collector*m_flow_nominal[1]*
        m_flow_nominal[1]),
    m_flow(start=m_flow_nominal[1]),
    dp(start=dpValve_nominal*2 - (k_collector*m_flow_nominal[1]*m_flow_nominal[
          1])))                                      annotation (Placement(
        transformation(
        extent={{-8,-6},{8,6}},
        rotation=90,
        origin={-80,-20})));

// Sources (public)
  SolarSystem.Data.Parameter.City_Datas.City_Data  City_Infos
    annotation (Placement(transformation(extent={{514,-232},{544,-202}})));

// Mediums (PROTECTED)
protected
  package MediumA = Buildings.Media.Air "Medium air model";
  package MediumE = SolarSystem.Media.WaterGlycol "Medium water model";

// Rooms simplifications
  parameter Modelica.SIunits.Angle S_=
    Buildings.Types.Azimuth.S "Azimuth for south walls";
  parameter Modelica.SIunits.Angle E_=
    Buildings.Types.Azimuth.E "Azimuth for east walls";
  parameter Modelica.SIunits.Angle W_=
    Buildings.Types.Azimuth.W "Azimuth for west walls";
  parameter Modelica.SIunits.Angle N_=
    Buildings.Types.Azimuth.N "Azimuth for north walls";
  parameter Modelica.SIunits.Angle C_=
    Buildings.Types.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    Buildings.Types.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle Z_=
    Buildings.Types.Tilt.Wall "Tilt for wall";
  parameter Integer nConExtWin = 4 "Number of constructions with a window";
  parameter Integer nConBou = 1
    "Number of surface that are connected to constructions that are modeled inside the room";
  parameter Modelica.SIunits.Length thicknessIns=0.100
    "Thickness of insulation";
  parameter Modelica.SIunits.ThermalConductivity lambdaIns=0.04
    "Heat conductivity of insulation";

// Constraints
  parameter Real deltaM=0.15
    "Fraction of nominal flow rate where linearization starts, if y=1";
  parameter Modelica.SIunits.Time tau=10
    "Time constant of fluid volume for nominal flow, used if dynamicBalance=true";
  parameter Real k_collector = Collector.per.dp_nominal / (m_flow_nominal[1] * m_flow_nominal[1])
    "Subtract from collector pressure losses (used to always have correct flow rate in each branch (unit=Pa.s-2 / Kg-2)";
  parameter Modelica.SIunits.Velocity v_nominal=0.3
    "Velocity at m_flow_nominal (used to compute default diameter)";
  parameter Boolean linearizeFlowResistance=true
    "= true, use linear relation between m_flow and dp for any flow rate";
  parameter Boolean computeFlowResistance=false
    "=true, compute flow resistance. Set to false to assume no friction";
  parameter Modelica.Fluid.Types.Dynamics energyDynamics_Splitter=Modelica.Fluid.Types.Dynamics.DynamicFreeInitial
    "Formulation of energy balance";
  parameter Boolean dynamicBalance_Splitter=false
    "Set to true to use a dynamic balance, which often leads to smaller systems of equations";
  parameter Boolean filteredOpening[3]={true, true, true}
    "= true, if opening is filtered with a 2nd order CriticalDamping filter [1] pour v3v, [2] pour v2v, [3] pour pompes";
  parameter Modelica.SIunits.Time riseTime_pump=120
    "Rise time of the filter (time to reach 99.6 % of an opening step)[1] pour v3v, [2] pour v2v, [3] pour pompes";
  parameter Modelica.SIunits.Time riseTime_V2V=120
    "Rise time of the filter (time to reach 99.6 % of an opening step)[1] pour v3v, [2] pour v2v, [3] pour pompes";
  parameter Modelica.SIunits.Time riseTime_V3V=120
    "Rise time of the filter (time to reach 99.6 % of an opening step)[1] pour v3v, [2] pour v2v, [3] pour pompes";
  parameter Boolean filteredSpeed=true
    "= true, if speed is filtered with a 2nd order CriticalDamping filter";

// House (protected)
  SolarSystem.House.MixedAir House(
    redeclare package Medium = MediumA,
    nConExtWin=nConExtWin,
    nPorts=3,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    nSurBou=0,
    nConBou=nConBou,
    nConExt=1,
    m_flow_nominal=0.08,
    nConPar=2,
    hRoo=5,
    linearizeRadiation=true,
    C_inHouse=partition_int.c*partition_int.d*3,
    datConExt(
      layers={Roof},
      A={70},
      til={C_},
      azi={S_}),
    datConPar(
      layers={Partition,Partition},
      A={19.19,65},
      til={Z_,C_}),
    datConBou(
      layers={Floor},
      each A=65,
      each til=F_),
    AFlo=65,
    lat=City_Infos.lattitude,
    T_start=293.15,
    datConExtWin(
      layers={Wall,Wall,Wall,Wall},
      A={24.29,24.29,24.29,20.89},
      glaSys={GlaSys_Argon,GlaSys_Argon,GlaSys_Argon,GlaSys_Argon},
      wWin={3,3,3,1.7},
      hWin={1.7,1.7,1.7,1},
      each fFra=0.1,
      each til=Z_,
      azi={W_,S_,E_,N_})) "House used with solar system"
    annotation (Placement(transformation(extent={{600,-106},{640,-66}})));

// House inputs (protected)
  Modelica.Blocks.Sources.Constant ConGai_Flow(k=50/House.AFlo)
    "Convective heat gain"
    annotation (Placement(transformation(extent={{500,10},{520,30}})));
  Modelica.Blocks.Sources.Constant RadGai_Flow(k=50/House.AFlo)
    "Radiative heat gain"
    annotation (Placement(transformation(extent={{500,50},{520,70}})));
  Modelica.Blocks.Sources.Constant LatGai_Flow(k=0) "Latent heat gain"
    annotation (Placement(transformation(extent={{500,-40},{520,-20}})));
  Modelica.Blocks.Routing.Multiplex3 To_List
    annotation (Placement(transformation(extent={{552,10},{572,30}})));
  Modelica.Blocks.Sources.Constant Shade(k=0)
    "Control signal for the shading device"
    annotation (Placement(transformation(extent={{500,90},{520,110}})));
  Modelica.Blocks.Routing.Replicator Replicator(nout=max(1,nConExtWin))
    annotation (Placement(transformation(extent={{550,90},{568,110}})));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature TSoil[nConBou](each T=283.15)
    "Boundary condition for construction" annotation (Placement(transformation(
        extent={{0,0},{-24,24}},
        rotation=-90,
        origin={614,-164})));
  Buildings.HeatTransfer.Conduction.SingleLayer Soil_Conductance(
    steadyStateInitial=true,
    A=House.AFlo,
    redeclare SolarSystem.Data.Parameter.Soil material,
    T_a_start=283.15,
    T_b_start=283.75) "2m deep soil (per definition on p.4 of ASHRAE 140-2007)"
    annotation (Placement(transformation(
        extent={{5,-5},{-3,3}},
        rotation=-90,
        origin={627,-119})));
  Buildings.Fluid.Sources.Outside AirInfiltration_source(redeclare package
      Medium = MediumA, nPorts=1) "Source model for air infiltration"
    annotation (Placement(transformation(extent={{564,-108},{576,-96}})));
  Buildings.Fluid.Sensors.Density Density(redeclare package Medium = MediumA)
    "Air density inside the building"
    annotation (Placement(transformation(extent={{586,-166},{568,-150}})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM OutsideAir_resistance(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    m_flow_nominal=0.05 + 67.5*2.7*0.41/3600*1.2) "Outside air resistance"
    annotation (Placement(transformation(extent={{584,-108},{596,-96}})));
  Modelica.Blocks.Sources.Constant Air_Supply(k=-150/3600)
    "0.41 ACH adjusted for the altitude (0.5 at sea level)" annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={510,-68})));
  Modelica.Blocks.Math.Product To_VolumeFlow
    annotation (Placement(transformation(extent={{552,-142},{570,-124}})));
  Buildings.Fluid.Sources.MassFlowSource_T sinInf1(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{582,-142},{598,-126}})));

// House composition (protected)
  SolarSystem.Data.Parameter.Construction.Layers.DoubleClearArgon16
                                 GlaSys_Argon
    annotation (Placement(transformation(extent={{598,82},{618,102}})));
  SolarSystem.Data.Parameter.Construction.Layers.Wood_part
                                    partition_int(x=2)
    annotation (Placement(transformation(extent={{642,82},{662,102}})));

// Systems (protected)
  SolarSystem.Heat_transfer.RadiatorEN442_2_extratT Radiator(
    redeclare package Medium = MediumE,
    nEle=20,
    T_start=T_start,
    Q_flow_nominal=10000,
    n=1.287,
    dp(start=0),
    m_flow(start=m_flow_nominal[2]),
    m_flow_nominal=Radiator.Q_flow_nominal/(MediumE.cp_const*(Radiator.T_a_nominal
         - Radiator.T_b_nominal)),
    T_a_nominal=313.15,
    T_b_nominal=303.15) annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=-90,
        origin={438,-10})));

// Flow resistances (protected)
  Buildings.Fluid.FixedResistances.Pipe outFlow_Collector(
    redeclare package Medium = MediumE,
    nSeg=24,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=12,
    useMultipleHeatPorts=true,
    T_start=T_start,
    linearizeFlowResistance=linearizeFlowResistance,
    v_nominal=v_nominal,
    dp_nominal=dpValve_nominal*2,
    m_flow_nominal=m_flow_nominal[1],
    from_dp=false,
    m_flow(start=m_flow_nominal[1]),
    dp(start=dpValve_nominal*2))
    annotation (Placement(transformation(extent={{-8,58},{8,70}})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM res(
    m_flow(start=0),
    redeclare package Medium = MediumE,
    dp_nominal=1414,
    m_flow_nominal=m_flow_nominal[2])
    annotation (Placement(transformation(extent={{402,54},{422,74}})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM res1(
    redeclare package Medium = MediumE,
    dp_nominal=10838,
    m_flow_nominal=m_flow_nominal[2],
    m_flow(start=m_flow_nominal[2]),
    dp(start=10838))
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={20,-50})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM res2(
    redeclare package Medium = MediumE,
    dp_nominal=1498,
    m_flow_nominal=m_flow_nominal[1],
    m_flow(start=m_flow_nominal[1]),
    dp(start=1498))
    annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={60,-50})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM res4(
    dp(start=0),
    redeclare package Medium = MediumE,
    dp_nominal=13316,
    m_flow_nominal=m_flow_nominal[2])
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={272,-48})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM BufferTank_BottomSplitter1(
    redeclare package Medium = MediumE,
    T_start=T_start,
    dp_nominal=dp_nominal[1]*{0,0,0},
    dynamicBalance=dynamicBalance_Splitter,
    energyDynamics=energyDynamics_Splitter,
    m_flow_nominal=m_flow_nominal[1]*{1,1,1},
    from_dp=false) "Flow splitter"                               annotation (
      Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=180,
        origin={60,-22})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM BufferTank_TopSplitter(
    redeclare package Medium = MediumE,
    T_start=T_start,
    dp_nominal=dp_nominal[1]*{0,0,0},
    dynamicBalance=dynamicBalance_Splitter,
    energyDynamics=energyDynamics_Splitter,
    m_flow_nominal=m_flow_nominal[1]*{2,-1,-1},
    from_dp=false) "Flow splitter"                                annotation (
      Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=180,
        origin={60,64})));

  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM BufferTank_BottomSplitter(
    redeclare package Medium = MediumE,
    T_start=T_start,
    dp_nominal=dp_nominal[1]*{0,0,0},
    dynamicBalance=dynamicBalance_Splitter,
    energyDynamics=energyDynamics_Splitter,
    m_flow_nominal=m_flow_nominal[1]*{1,-2,1},
    from_dp=false) "Flow splitter"                               annotation (
      Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={60,-120})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM HealthTank_ExtraBottomSplitter(
    redeclare package Medium = MediumE,
    T_start=T_start,
    dp_nominal=dp_nominal[2]*{0,0,0},
    dynamicBalance=dynamicBalance_Splitter,
    energyDynamics=energyDynamics_Splitter,
    m_flow_nominal=m_flow_nominal[2]*{0.00001,-1,1},
    from_dp=false) "Flow splitter"                               annotation (
      Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={370,-120})));

  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM HealthTank_ExtraTopSplitter(
    redeclare package Medium = MediumE,
    T_start=T_start,
    dp_nominal=dp_nominal[2]*{0,0,0},
    dynamicBalance=dynamicBalance_Splitter,
    energyDynamics=energyDynamics_Splitter,
    m_flow_nominal=m_flow_nominal[2]*{1,-1,-0.00001},
    from_dp=false) "Flow splitter"                                annotation (
      Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=180,
        origin={370,64})));

// Valves (protected)
  Buildings.Fluid.Actuators.Valves.ThreeWayLinear Valve_Solar(
    redeclare package Medium = MediumE,
    T_start=T_start,
    deltaM=deltaM,
    filteredOpening=filteredOpening[1],
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    dynamicBalance=false,
    riseTime=riseTime_V3V,
    m_flow_nominal=-m_flow_nominal[1],
    dpValve_nominal=dpValve_nominal,
    l={0.005,0.005},
    fraK=1,
    init=Modelica.Blocks.Types.Init.SteadyState,
    from_dp=false,
    homotopyInitialization=true)
    annotation (Placement(transformation(extent={{-20,-110},{0,-130}})));

  Buildings.Fluid.Actuators.Valves.TwoWayLinear Valve_C6(
    redeclare package Medium = MediumE,
    dpValve_nominal=dpValve_nominal,
    m_flow_nominal=m_flow_nominal[1],
    filteredOpening=filteredOpening[2],
    riseTime=riseTime_V2V,
    from_dp=false,
    deltaM=deltaM,
    m_flow(start=m_flow_nominal[1]),
    dp(start=dpValve_nominal)) "Pressure drop"
                    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={60,-104})));
  Buildings.Fluid.Actuators.Valves.TwoWayLinear Valve_C5(
    redeclare package Medium = MediumE,
    dpValve_nominal=dpValve_nominal,
    m_flow_nominal=m_flow_nominal[1],
    filteredOpening=filteredOpening[2],
    riseTime=riseTime_V2V,
    from_dp=true,
    deltaM=deltaM) "Pressure drop"
                    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={130,-94})));
  Buildings.Fluid.Actuators.Valves.TwoWayLinear Valve_C4(
    redeclare package Medium = MediumE,
    dpValve_nominal=dpValve_nominal,
    m_flow_nominal=m_flow_nominal[2],
    filteredOpening=filteredOpening[2],
    riseTime=riseTime_V2V,
    from_dp=true,
    deltaM=deltaM) "Pressure drop"
                    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={370,-94})));
  Buildings.Fluid.Actuators.Valves.TwoWayLinear Valve_C2(
    redeclare package Medium = MediumE,
    dpValve_nominal=dpValve_nominal,
    m_flow_nominal=m_flow_nominal[2],
    filteredOpening=filteredOpening[2],
    riseTime=riseTime_V2V,
    from_dp=true,
    deltaM=deltaM,
    m_flow(start=m_flow_nominal[2]),
    dp(start=dpValve_nominal)) "Pressure drop"
                    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={438,-94})));

// Flow/Temp sensors (protected)
  Buildings.Fluid.Sensors.MassFlowRate Collector_FlowSensor(redeclare package
      Medium = MediumE) annotation (Placement(transformation(
        extent={{-4,4},{4,-4}},
        rotation=90,
        origin={-80,20})));
  Buildings.Fluid.Sensors.MassFlowRate Boiler_FlowSensor(redeclare package
      Medium = MediumE) annotation (Placement(transformation(
        extent={{-4,4},{4,-4}},
        rotation=90,
        origin={272,-26})));
  Buildings.Fluid.Sensors.MassFlowRate Radiator_FlowSensor(redeclare package
      Medium = MediumE) annotation (Placement(transformation(
        extent={{4,-4},{-4,4}},
        rotation=90,
        origin={438,18})));
  Buildings.Fluid.Sensors.MassFlowRate DrawingUp_FlowSensor(redeclare package
      Medium = MediumE) annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=90,
        origin={174,32})));
  Buildings.Fluid.Sensors.TemperatureTwoPort PipeNetwork_TempSensor(
    redeclare package Medium = MediumE,
    m_flow_nominal=0.04,
    tau=10) annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={248,-58})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Boiler_Out_TempSensor(
    redeclare package Medium = MediumE,
    tau=15,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal[2])
                     annotation (Placement(transformation(
        extent={{-4,4},{4,-4}},
        rotation=90,
        origin={272,50})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Boiler_In_TempSensor(
    redeclare package Medium = MediumE,
    tau=15,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal[2])
                     annotation (Placement(transformation(
        extent={{-4,4},{4,-4}},
        rotation=90,
        origin={272,-12})));

// Sources (protected)
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{-74,-272},{-46,-248}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Weather(
      computeWetBulbTemperature=false, filNam=City_Infos.path) "Weather datas"
    annotation (Placement(transformation(extent={{-38,100},{-60,116}})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{560,-240},{600,-200}})));
  Buildings.Fluid.Sources.Boundary_pT NoFlow_StaticSource(
    redeclare package Medium = MediumE,
    use_T_in=true,
    nPorts=1) annotation (Placement(transformation(extent={{52,12},{40,24}})));
  Buildings.Fluid.Sources.MassFlowSource_T NoFlow_DynamicSource(
    redeclare package Medium = MediumE,
    use_m_flow_in=false,
    use_T_in=true,
    m_flow=0.000001,
    nPorts=1) annotation (Placement(transformation(extent={{92,38},{100,46}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    redeclare package Medium = MediumE,
    T_start=T_start,
    V_start(displayUnit="l") = 0.2)
    annotation (Placement(transformation(extent={{-60,-70},{-34,-44}})));
  Modelica.Blocks.Sources.CombiTimeTable PipeNetwork_Temp(extrapolation=
        Modelica.Blocks.Types.Extrapolation.Periodic, table=City_Infos.water)
    "Cold water temperature according to month of the year"
    annotation (Placement(transformation(extent={{-4,-4},{4,4}},
        rotation=-90,
        origin={220,-90})));
  Valves.Thermostatic_valve Thermo_Valve(
    m_flow_nominal=0.04,
    redeclare package Medium = MediumE,
    control=[0.0,313.15],
    deltaM=deltaM,
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    wp=1,
    wd=0,
    fraK=1,
    reverseAction=false,
    k=0.03,
    Ti=220)                                                   annotation (
      Placement(transformation(
        extent={{9,10},{-9,-10}},
        rotation=90,
        origin={175,-4})));
  Buildings.Fluid.Sensors.TemperatureTwoPort DrawingUp_TempSensor(
    redeclare package Medium = MediumE,
    m_flow_nominal=0.04,
    tau=tau) annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=90,
        origin={174,48})));
  Buildings.HeatTransfer.Sources.FixedTemperature Temp_House(T=293.15)
    "Room temperature" annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=90,
        origin={100,-88})));

// Other (protected)
  Modelica.Blocks.Sources.RealExpression fake_room(each y=30 + 273.15)
    "algo need at least 2 heating pumps"
    annotation (Placement(transformation(extent={{-44,-238},{-12,-220}})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature Text[24]
    annotation (Placement(transformation(extent={{-20,-30},{-40,-10}})));
  SolarSystem.Utilities.Limit.Overheat Boiler_Control(n=1, threshold={323.15,343.15})
    annotation (Placement(transformation(
        extent={{8,-6},{-8,6}},
        rotation=90,
        origin={294,24})));
  Modelica.Blocks.Math.MultiProduct Boiler_Limiter(nu=2)  annotation (Placement(
        transformation(
        extent={{-2,-2},{2,2}},
        rotation=180,
        origin={286,8})));
  Modelica.Blocks.Sources.RealExpression C4_flow(y=C4.m_flow)
    annotation (Placement(transformation(extent={{20,-168},{38,-152}})));
  Modelica.Blocks.Math.Gain C4_Limiter(k=0.2) annotation (Placement(
        transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={70,-150})));

  Buildings.Fluid.Storage.ExpansionVessel SurgeTank_extra(
    redeclare package Medium = MediumE,
    T_start=T_start,
    V_start(displayUnit="l") = 0.1)
    annotation (Placement(transformation(extent={{314,-72},{340,-46}})));
  Modelica.Blocks.Sources.Constant Tboundaries[24](each k=273.15)
                                                             annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-4,-50})));

  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Wall(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=3,
    material={SolarSystem.Data.Parameter.Construction.Layers.WoodPanel(x=0.04),
        SolarSystem.Data.Parameter.Construction.Layers.GlassWool(x=0.08),
        SolarSystem.Data.Parameter.Construction.Layers.WoodPanel(x=0.04)})
    "Wall layers"
    annotation (Placement(transformation(extent={{600,12},{620,32}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Roof(        nLay=
       2, material={SolarSystem.Data.Parameter.Construction.Layers.WoodPanel(x=
        0.1),SolarSystem.Data.Parameter.Construction.Layers.GlassWool(x=0.1)})
    "Roof layers"
    annotation (Placement(transformation(extent={{620,32},{640,52}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Floor(nLay=2, material=
       {SolarSystem.Data.Parameter.Construction.Layers.GlassWool(x=0.05),
        Buildings.HeatTransfer.Data.Solids.Concrete(x=0.3)}) "Floor layer"
    annotation (Placement(transformation(extent={{620,-8},{640,12}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Partition(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=2,
    material={SolarSystem.Data.Parameter.Construction.Layers.WoodPanel(x=0.03),
        Buildings.HeatTransfer.Data.Solids.Concrete(x=0.2)}) "Wall layers"
    annotation (Placement(transformation(extent={{640,12},{660,32}})));
public
  parameter SolarSystem.Data.Parameter.Pump.SolisArt.C6_5_SolisConfort C6_5(
    motorCooledByFluid=true,
    hydraulicEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{5,
          10,15,20,25,30,35,40}, eta={1,1,1,1,1,1,1,1}),
    motorEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{5,10,15,
          20,25,30,35,40}, eta={1,1,1,1,1,1,1,1}),
    pressure(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{5,10,15,20,25,
          30,35,40}, dp=dp_nominal[1]*{4.9,4.6,4.3,4,3.5,3.2,2.5,1}))
    annotation (Placement(transformation(extent={{114,-166},{134,-146}})));
  parameter SolarSystem.Data.Parameter.Pump.SolisArt.C2_4_SolisConfort C4_2(
    motorEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{8.75,17.5,
          26.25,35,43.75,52.5,61.25,70}, eta={1,1,1,1,1,1,1,1}),
    motorCooledByFluid=true,
    pressure(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{8.75,17.5,26.25,
          35,43.75,52.5,61.25,70}, dp=dp_nominal[2]*{4.9,4.6,4.3,4,3.5,3.2,2.5,1}),
    N_nominal=3000,
    hydraulicEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{8.75,
          17.5,26.25,35,43.75,52.5,61.25,70}, eta={1,1,1,1,1,1,1,1}))
    annotation (Placement(transformation(extent={{152,-166},{172,-146}})));
equation
  connect(RadGai_Flow.y, To_List.u1[1]) annotation (Line(
      points={{521,60},{540,60},{540,27},{550,27}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(LatGai_Flow.y, To_List.u3[1]) annotation (Line(
      points={{521,-30},{540,-30},{540,13},{550,13}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(To_List.y, House.qGai_flow) annotation (Line(
      points={{573,20},{580,20},{580,-78},{598,-78}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(House.uSha,Replicator. y) annotation (Line(
      points={{598,-70},{580,-70},{580,100},{568.9,100}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ConGai_Flow.y, To_List.u2[1]) annotation (Line(
      points={{521,20},{550,20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Shade.y, Replicator.u) annotation (Line(
      points={{521,100},{548.2,100}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Density.port, House.ports[1]) annotation (Line(
      points={{577,-166},{602,-166},{602,-98.6667},{605,-98.6667}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(TSoil[1].port, Soil_Conductance.port_a) annotation (Line(
      points={{626,-140},{626,-124}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=1));
  connect(Soil_Conductance.port_b, House.surf_conBou[1]) annotation (Line(
      points={{626,-116},{626,-102}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=1));
  connect(House.heaPorAir, House_TempSensor.port) annotation (Line(
      points={{619,-86},{640,-86},{640,-40},{654,-40},{654,-21}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(AirInfiltration_source.ports[1], OutsideAir_resistance.port_a)
    annotation (Line(
      points={{576,-102},{584,-102}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(OutsideAir_resistance.port_b, House.ports[2]) annotation (Line(
      points={{596,-102},{602,-102},{602,-96},{605,-96}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));

  connect(Weather.weaBus, weaBus) annotation (Line(
      points={{-60,108},{-96,108},{-96,-260},{-60,-260}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(Valve_Solar.port_1, inFlow_Collector.port_a) annotation (Line(
      points={{-20,-120},{-80,-120},{-80,-28}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Valve_Solar.port_1,SurgeTank. port_a) annotation (Line(
      points={{-20,-120},{-47,-120},{-47,-70}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T5.T, NoFlow_DynamicSource.T_in) annotation (Line(
      points={{68,40},{64,40},{64,52},{80,52},{80,43.6},{91.2,43.6}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Weather.weaBus, House.weaBus) annotation (Line(
      points={{-60,108},{-96,108},{-96,-272},{658,-272},{658,-60},{637.9,-60},{637.9,
          -68.1}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Weather.weaBus, AirInfiltration_source.weaBus) annotation (Line(
      points={{-60,108},{-96,108},{-96,-272},{658,-272},{658,-60},{560,-60},{
          560,-101.88},{564,-101.88}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Radiator.heatPortCon, House.heaPorAir) annotation (Line(
      points={{448.08,-7.2},{614,-7.2},{614,-86},{619,-86}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=1));
  connect(Radiator.heatPortRad, House.heaPorRad) annotation (Line(
      points={{448.08,-12.8},{614,-12.8},{614,-90},{619,-90},{619,-89.8}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=1));

  connect(Control.V3V_solar_outter,Valve_Solar. y) annotation (Line(
      points={{189.24,-188.576},{248,-188.576},{248,-188},{248,-188},{248,-140},
          {-10,-140},{-10,-132}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(T3.T, Control.T3) annotation (Line(
      points={{214,25},{240,25},{240,120},{-100,120},{-100,-189.565},{64.248,
          -189.565}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(T4.T, Control.T4) annotation (Line(
      points={{214,45},{240,45},{240,120},{-100,120},{-100,-196.976},{64.248,
          -196.976}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(T5.T, Control.T5) annotation (Line(
      points={{68,40},{44,40},{44,120},{-100,120},{-100,-203.894},{64.248,
          -203.894}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(T7.T, Control.T7) annotation (Line(
      points={{304,-109},{304,120},{-100,120},{-100,-212},{64.248,-212},{64.248,
          -211.306}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(weaBus.TDryBul, Control.Text) annotation (Line(
      points={{-60,-260},{-12.5,-260},{-12.5,-246.635},{64.992,-246.635}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(T8.T, Control.T8) annotation (Line(
      points={{334,75},{334,120},{-100,120},{-100,-218.718},{64.248,-218.718}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(House_TempSensor.T, Control.Tambiant[1]) annotation (Line(
      points={{672,-21},{680,-21},{680,120},{-100,120},{-100,-237.865},{63.752,
          -237.865}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(fake_room.y, Control.Tambiant[2]) annotation (Line(
      points={{-10.4,-229},{0,-229},{0,-235.147},{63.752,-235.147}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Text.port, inFlow_Collector.heatPorts) annotation (Line(
      points={{-40,-20},{-77,-20}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Text.port, outFlow_Collector.heatPorts) annotation (Line(
      points={{-40,-20},{-60,-20},{-60,12},{0,12},{0,61}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Boiler_Limiter.y, Boiler.y) annotation (Line(
      points={{283.66,8},{268,8},{268,12},{252,12},{252,4},{260.8,4},{260.8,7.2}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(Boiler.T, Boiler_Control.in_value[1]) annotation (Line(
      points={{260.8,39.4},{260.8,42},{294,42},{294,32.64}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(To_VolumeFlow.y, sinInf1.m_flow_in) annotation (Line(
      points={{570.9,-133},{576,-133},{576,-132},{576,-132},{576,-127.6},{582,-127.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Density.d,To_VolumeFlow. u2) annotation (Line(
      points={{567.1,-158},{540,-158},{540,-138},{550.2,-138},{550.2,-138.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Air_Supply.y, To_VolumeFlow.u1) annotation (Line(
      points={{521,-68},{540,-68},{540,-127.6},{550.2,-127.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sinInf1.ports[1], House.ports[3]) annotation (Line(
      points={{598,-134},{602,-134},{602,-93.3333},{605,-93.3333}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Boiler_In_TempSensor.port_b, Boiler.port_a) annotation (Line(
      points={{272,-8},{272,10}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Boiler.port_b, Boiler_Out_TempSensor.port_a) annotation (Line(
      points={{272,38},{272,46}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_Collector.port_b, Collector_FlowSensor.port_a) annotation (
      Line(
      points={{-80,-12},{-80,16}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Collector_FlowSensor.m_flow, integrator_collector.m_flow) annotation (
     Line(
      points={{-75.6,20},{-14,20},{-14,93.8},{-4,93.8}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Boiler_FlowSensor.port_b, Boiler_In_TempSensor.port_a) annotation (
      Line(
      points={{272,-22},{272,-16}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Boiler_FlowSensor.m_flow, integrator_boiler.m_flow) annotation (Line(
      points={{276.4,-26},{310,-26},{310,26},{322,26}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Radiator.Tinside[20], integrator_radiator.Tout) annotation (Line(
      points={{424.14,-10.56},{422,-10.56},{422,0},{416,0},{416,-11.8},{406,
          -11.8}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Radiator.Tinside[1], integrator_radiator.Tin) annotation (Line(
      points={{429.46,-10.56},{422,-10.56},{422,0},{416,0},{416,-17.2},{406,
          -17.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Radiator_FlowSensor.m_flow, integrator_radiator.m_flow) annotation (
      Line(
      points={{433.6,18},{416,18},{416,-26.2},{406,-26.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Boiler_Out_TempSensor.T, integrator_boiler.Tout) annotation (Line(
      points={{276.4,50},{310,50},{310,38},{322,38}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Boiler_In_TempSensor.T, integrator_boiler.Tin) annotation (Line(
      points={{276.4,-12},{310,-12},{310,32},{322,32}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(DrawingUp_TempSensor.T, Thermo_Valve.TdrawingUp) annotation (Line(
      points={{169.6,48},{156,48},{156,-20},{174,-20},{174,-9.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(DrawingUp_TempSensor.port_b, DrawingUp.port_a) annotation (Line(
      points={{174,52},{174,60},{212,60},{212,84.175},{210,84.175}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(DrawingUp_FlowSensor.port_b, DrawingUp_TempSensor.port_a) annotation (
     Line(
      points={{174,36},{174,44}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Thermo_Valve.port_3, DrawingUp_FlowSensor.port_a) annotation (Line(
      points={{174,6},{174,28}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(DrawingUp_FlowSensor.m_flow, integrator_drawingUp.m_flow) annotation (
     Line(
      points={{169.6,32},{156,32},{156,67.2},{168,67.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(PipeNetwork_Source.ports[1], PipeNetwork_TempSensor.port_a)
    annotation (Line(
      points={{248,-84},{248,-64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(PipeNetwork_TempSensor.port_b, Thermo_Valve.port_2) annotation (Line(
      points={{248,-52},{248,-40},{160,-40},{160,-4},{164,-4}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(DrawingUp_TempSensor.T, integrator_drawingUp.Tout) annotation (Line(
      points={{169.6,48},{156,48},{156,76.8},{168,76.8}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(PipeNetwork_TempSensor.T, integrator_drawingUp.Tin) annotation (Line(
      points={{241.4,-58},{156,-58},{156,73.2},{168,73.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Radiator.port_b, C2.port_a) annotation (Line(
      points={{438,-24},{438,-50}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));

  connect(T5.T, NoFlow_StaticSource.T_in) annotation (Line(
      points={{68,40},{64,40},{64,20.4},{53.2,20.4}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(NoFlow_StaticSource.ports[1], Buffer_Tank.port_a) annotation (Line(
      points={{40,18},{20,18},{20,0},{74,0}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(NoFlow_DynamicSource.ports[1], Buffer_Tank.port_b) annotation (Line(
      points={{100,42},{124,42},{124,0},{116,0}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Buffer_Tank.heaPorVol[4], T5.port) annotation (Line(
      points={{95,-1.014},{82,-1.014},{82,40},{80,40}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Temp_House.port, Buffer_Tank.heaPorSid) annotation (Line(
      points={{100,-80},{100,-42},{120,-42},{120,0},{106.76,0}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Temp_House.port, Buffer_Tank.heaPorTop) annotation (Line(
      points={{100,-80},{100,-42},{120,-42},{120,0},{108,0},{108,18},{99.2,18},{
          99.2,19.24}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Health_Tank.port_a, Thermo_Valve.port_1) annotation (Line(
      points={{236,-14},{236,14},{194,14},{194,-4},{184,-4}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(PipeNetwork_TempSensor.port_b, Health_Tank.port_b) annotation (Line(
      points={{248,-52},{248,-40},{188,-40},{188,-14}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Health_Tank.heaPorVol[12], T3.port) annotation (Line(
      points={{212,-13.748},{194,-13.748},{194,25},{200,25}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Health_Tank.heaPorVol[4], T4.port) annotation (Line(
      points={{212,-15.092},{212,-12},{194,-12},{194,45},{200,45}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Temp_House.port, Health_Tank.heaPorTop) annotation (Line(
      points={{100,-80},{100,-42},{240,-42},{240,24},{224,24},{224,14},{207.2,14},
          {207.2,6.72}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Temp_House.port, Health_Tank.heaPorSid) annotation (Line(
      points={{100,-80},{100,-42},{240,-42},{240,24},{224,24},{224,-14},{198.56,
          -14}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Control.S6_outter, Pump_Control.pumpControl_S6) annotation (Line(
      points={{189.24,-203.894},{260,-203.894},{260,-193.01},{295.165,-193.01}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Control.S5_outter, Pump_Control.pumpControl_S5) annotation (Line(
      points={{189.24,-214.271},{280,-214.271},{280,-208.61},{295.165,-208.61}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Control.S4_outter, Pump_Control.pumpControl_S4) annotation (Line(
      points={{189.24,-224.153},{260,-224.153},{260,-224.21},{295.165,-224.21}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Control.Sj_outter, Pump_Control.pumpControl_Sj) annotation (Line(
      points={{189.24,-234.035},{240,-234.035},{240,-239.81},{295.165,-239.81}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T3.T, Pump_Control.T3) annotation (Line(
      points={{214,25},{240,25},{240,120},{480,120},{480,-140},{325.059,-140},{
          325.059,-171.56}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T4.T, Pump_Control.T4) annotation (Line(
      points={{214,45},{240,45},{240,120},{480,120},{480,-140},{340.588,-140},{
          340.588,-171.56}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T5.T, Pump_Control.T5) annotation (Line(
      points={{68,40},{44,40},{44,120},{480,120},{480,-140},{356.118,-140},{
          356.118,-171.56}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T7.T, Pump_Control.T7) annotation (Line(
      points={{304,-109},{304,120},{480,120},{480,-140},{370,-140},{370,-171.56},
          {371.647,-171.56}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T8.T, Pump_Control.T8) annotation (Line(
      points={{334,75},{334,120},{480,120},{480,-154},{387.176,-154},{387.176,
          -171.56}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Pump_Control.S4_outter, C4.Nrpm) annotation (Line(
      points={{427.553,-177.8},{480,-177.8},{480,-80},{400,-80},{400,-60},{382,
          -60}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Pump_Control.S5_outter, C5.Nrpm) annotation (Line(
      points={{427.553,-198.08},{480,-198.08},{480,-80},{160,-80},{160,-74},{
          142,-74}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Pump_Control.S6_outter, C6.Nrpm) annotation (Line(
      points={{426.776,-221.48},{480,-221.48},{480,-80},{160,-80},{160,-62},{80,
          -62},{80,-76},{72,-76}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Pump_Control.Sj_outter[1], C2.Nrpm) annotation (Line(
      points={{427.553,-244.88},{480,-244.88},{480,-80},{460,-80},{460,-60},{
          450,-60}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Temp_House.port, Boiler.heatPort) annotation (Line(
      points={{100,-80},{100,-42},{240,-42},{240,24},{261.92,24}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Collector_FlowSensor.port_b, Collector.port_a) annotation (Line(
      points={{-80,24},{-80,66},{-74,66}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Weather.weaBus, Collector.weaBus) annotation (Line(
      points={{-60,108},{-80,108},{-80,94.8},{-74,94.8}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(C4_flow.y, Control.flow_S4) annotation (Line(
      points={{38.9,-160},{81.112,-160},{81.112,-173.753}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(C4_flow.y, C4_Limiter.u) annotation (Line(
      points={{38.9,-160},{52,-160},{52,-150},{65.2,-150}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(C4_Limiter.y, Control.mini_flow_S4) annotation (Line(
      points={{74.4,-150},{93.512,-150},{93.512,-173.753}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Collector.Tinside[collector_seg], integrator_collector.Tout) annotation (Line(
      points={{-19.4,45},{-16,45},{-16,108.2},{-4,108.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Collector.Tinside[1], integrator_collector.Tin) annotation (Line(
      points={{-19.4,45},{-16,45},{-16,102.8},{-4,102.8}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Collector.Tinside[collector_seg], Pump_Control.T1) annotation (Line(
      points={{-19.4,45},{-20,45},{-20,120},{480,120},{480,-140},{309.529,-140},
          {309.529,-171.56}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Boiler_Control.out_value, Boiler_Limiter.u[1]) annotation (Line(
      points={{294,15.04},{294,8},{288,8},{288,7.3}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Control.BackupHeater_outter, Boiler_Limiter.u[2]) annotation (Line(
      points={{189.24,-250.835},{220,-250.835},{220,-258},{480,-258},{480,8},{
          288,8},{288,8.7}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Collector.Tinside[collector_seg], Control.T1) annotation (Line(
      points={{-19.4,45},{-20,45},{-20,120},{-100,120},{-100,-182},{-18,-182},{
          -18,-182.153},{64.248,-182.153}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T8.port_b, HealthTank_ExtraTopSplitter.port_1) annotation (Line(
      points={{344,64},{364,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(outFlow_Collector.port_b, BufferTank_TopSplitter.port_1) annotation (
      Line(
      points={{8,64},{54,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(PipeNetwork_Temp.y[1], PipeNetwork_Source.T_in) annotation (Line(
      points={{220,-94.4},{220,-102},{250.4,-102},{250.4,-97.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(HealthTank_ExtraBottomSplitter.port_2, T7.port_a) annotation (Line(
      points={{364,-120},{314,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(HealthTank_ExtraTopSplitter.port_2, res.port_a) annotation (Line(
      points={{376,64},{402,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(res.port_b, Radiator_FlowSensor.port_a) annotation (Line(
      points={{422,64},{438,64},{438,22}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Valve_Solar.port_3, res1.port_a) annotation (Line(
      points={{-10,-110},{-10,-88},{20,-88},{20,-60}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(res4.port_b, Boiler_FlowSensor.port_a) annotation (Line(
      points={{272,-38},{272,-30}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(res1.port_b, BufferTank_BottomSplitter1.port_1) annotation (Line(
      points={{20,-40},{20,-22},{54,-22}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(BufferTank_BottomSplitter1.port_2, Buffer_Tank.portHex_b) annotation (
     Line(
      points={{66,-22},{74,-22},{74,-20.8}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(BufferTank_BottomSplitter1.port_3, res2.port_a) annotation (Line(
      points={{60,-28},{60,-40}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(res2.port_b, C6.port_a) annotation (Line(
      points={{60,-60},{60,-66}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C6.port_b, Valve_C6.port_a) annotation (Line(
      points={{60,-86},{60,-98}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Valve_C6.port_b, BufferTank_BottomSplitter.port_3) annotation (Line(
      points={{60,-110},{60,-114}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C5.port_b,Valve_C5. port_a) annotation (Line(
      points={{130,-84},{130,-88}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C4.port_b,Valve_C4. port_a) annotation (Line(
      points={{370,-70},{370,-88}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Valve_C4.port_b, HealthTank_ExtraBottomSplitter.port_3) annotation (
      Line(
      points={{370,-100},{370,-114}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C2.port_b,Valve_C2. port_a) annotation (Line(
      points={{438,-70},{438,-88}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Valve_C2.port_b, HealthTank_ExtraBottomSplitter.port_1) annotation (
      Line(
      points={{438,-100},{438,-120},{376,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Radiator_FlowSensor.port_b, Radiator.port_a) annotation (Line(
      points={{438,14},{438,4}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(BufferTank_TopSplitter.port_3, Buffer_Tank.portHex_a) annotation (
      Line(
      points={{60,58},{60,-9.88},{74,-9.88}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Collector.port_b, outFlow_Collector.port_a) annotation (Line(
      points={{-22,66},{-16,66},{-16,64},{-8,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(BufferTank_BottomSplitter.port_2, Valve_Solar.port_2) annotation (
      Line(
      points={{54,-120},{0,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Pump_Control.pumps_state[4], Valve_C2.y) annotation (Line(
      points={{332.824,-252.056},{332.824,-258},{480,-258},{480,-104},{460,-104},
          {460,-94},{445.2,-94}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Pump_Control.pumps_state[3], Valve_C4.y) annotation (Line(
      points={{332.824,-249.56},{332.824,-258},{480,-258},{480,-104},{400,-104},
          {400,-94},{377.2,-94}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Pump_Control.pumps_state[2], Valve_C5.y) annotation (Line(
      points={{332.824,-247.064},{332.824,-258},{440,-258},{440,-104},{160,-104},
          {160,-94},{137.2,-94}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Pump_Control.pumps_state[1], Valve_C6.y) annotation (Line(
      points={{332.824,-244.568},{332.824,-258},{480,-258},{480,-104},{67.2,
          -104}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(HealthTank_ExtraTopSplitter.port_3, Health_Tank.portHex_a)
    annotation (Line(
      points={{370,58},{370,0},{260,0},{260,-24.64},{236,-24.64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Health_Tank.portHex_b, C4.port_a) annotation (Line(
      points={{236,-36.4},{370,-36.4},{370,-50}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T7.port_b, res4.port_a) annotation (Line(
      points={{294,-120},{272,-120},{272,-58}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Boiler_Out_TempSensor.port_b, T8.port_a) annotation (Line(
      points={{272,54},{272,64},{324,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(HealthTank_ExtraBottomSplitter.port_2, SurgeTank_extra.port_a)
    annotation (Line(
      points={{364,-120},{327,-120},{327,-72}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Control.V3V_extra_outter, integrator_boiler.extra_valve) annotation (
      Line(
      points={{189.24,-178.694},{280,-178.694},{280,-178},{280,-178},{280,-140},
          {290,-140},{290,-32},{314,-32},{314,22},{322,22}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Tboundaries.y, Text.T) annotation (Line(
      points={{-4,-39},{-4,-20},{-18,-20}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Valve_C5.port_b, BufferTank_BottomSplitter.port_1) annotation (Line(
      points={{130,-100},{130,-120},{66,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(BufferTank_TopSplitter.port_2, C5.port_a) annotation (Line(
      points={{66,64},{130,64},{130,-64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Tboundaries[1].y, Collector.Text) annotation (Line(
      points={{-4,-39},{-4,-2},{-100,-2},{-100,54},{-79.2,54}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  annotation (Diagram(coordinateSystem(extent={{-100,-280},{680,120}},
          preserveAspectRatio=false),graphics), Icon(coordinateSystem(
          extent={{-100,-280},{680,120}})),
    experiment(
      StopTime=3.16224e+007,
      Interval=240,
      __Dymola_Algorithm="esdirk23a"),
    __Dymola_experimentSetupOutput(doublePrecision=true),
    Documentation(info="<html>
<p>SolisArt SolisConfort system with a radiator</p>
<p>Collectors system configuration must be parallel to have balance between branches.</p>
<p>To change weather configuration file you must use a city_infos compatible table.</p>
<p><br>WORKING FOR A FULL YEAR (around 13 cpu hours to complete with chambery parameters)</p>
<p><br>Which need some improvements :</p>
<ol>
<li>To much iterations due to valve_C2 and valve_C4. Possible solution can be other start values or longer opening lifting.</li>
<li>Chambery with two failed newton system resolution slower the process (see first point).</li>
<li>Boiler efficiency with a polynomial curve</li>
</ol>
<p><br>Difference with 17:</p>
<ol>
<li>Integrator inside Boiler consumption.</li>
<li>Rendement = Rendement g&eacute;n&eacute;ration * Rendement r&eacute;gulation = 0.82 * 0.95 = 77.9&percnt;</li>
<li>Update ECS consumption to 275 l/jour (40&deg;C) = 165 l/jour (60&deg;C)</li>
</ol>
<p>Difference with 18:</p>
<ol>
<li>Cleaning of variables scope (protected vs public)</li>
</ol>
<p><br>Difference with 19:</p>
<ol>
<li>Collector dynamic partition size </li>
</ol>
<p><br>Difference with 20:</p>
<ol>
<li>Ajusted collector pressure losses</li>
<li>Adjusted Radiator nominal power</li>
</ol>
<p><br>Difference with 21:</p>
<ol>
<li>Ajusted wall area for roof, partition and floor</li>
<li>Adjust wall U factor<br></li>
</ol>
<p><br><br>This model reduce to zero solar gains and add somes changes to other components:</p>
<ul>
<li>Heathl tank volume = 300.</li>
<li>Tank height = 1.3 meters.</li>
<li>Exchanger size = 0.255 to 1.1 meters</li>
<li>Exchanger discretization = 3</li>
<li>Add another surge tank due to two separate water circuit</li>
<li>Text = 0&deg;C</li>
<li>Collector shadow = 100&percnt;</li>
<li>Collector diff irradiation part = 0&percnt;</li>
</ul>
</html>"));
end SolisArt_confort_noSun22;
