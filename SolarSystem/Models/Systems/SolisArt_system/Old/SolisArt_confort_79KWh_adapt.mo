within SolarSystem.Models.Systems.SolisArt_system.Old;
model SolisArt_confort_79KWh_adapt
  "Simulation of solar tank recharge and discharge"
  import SolarSystem;
extends Modelica.Icons.Example;

  // Mediums
protected
  package MediumA =
      Buildings.Media.GasesConstantDensity.SimpleAir "Medium air model";
  package MediumE =
      Buildings.Media.ConstantPropertyLiquidWater "Medium water model";

  // Rooms simplifications
  parameter Modelica.SIunits.Angle S_=
    Buildings.HeatTransfer.Types.Azimuth.S "Azimuth for south walls";
  parameter Modelica.SIunits.Angle E_=
    Buildings.HeatTransfer.Types.Azimuth.E "Azimuth for east walls";
  parameter Modelica.SIunits.Angle W_=
    Buildings.HeatTransfer.Types.Azimuth.W "Azimuth for west walls";
  parameter Modelica.SIunits.Angle N_=
    Buildings.HeatTransfer.Types.Azimuth.N "Azimuth for north walls";
  parameter Modelica.SIunits.Angle C_=
    Buildings.HeatTransfer.Types.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    Buildings.HeatTransfer.Types.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle Z_=
    Buildings.HeatTransfer.Types.Tilt.Wall "Tilt for wall";
  parameter Integer nConExtWin = 4 "Number of constructions with a window";
  parameter Integer nConBou = 1
    "Number of surface that are connected to constructions that are modeled inside the room";

  parameter Modelica.SIunits.Pressure dpPip_nominal = 1000
    "Pressure difference of pipe (without valve)";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.153
    "Mass flow rate for pump S6 S5 and splitter";
  parameter Modelica.SIunits.Length thicknessIns=0.100
    "Thickness of insulation";
  parameter Modelica.SIunits.ThermalConductivity lambdaIns=0.04
    "Heat conductivity of insulation";
  parameter Modelica.Media.Interfaces.PartialMedium.Temperature T_start=279.55
    "Start value of temperature";
  parameter Real deltaM=0.1
    "Fraction of nominal flow rate where linearization starts, if y=1";
  parameter Modelica.SIunits.Time tau=10
    "Time constant of fluid volume for nominal flow, used if dynamicBalance=true";
protected
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{722,-260},{762,-220}})));

  SolarSystem.House.MixedAir
                           roo(
    redeclare package Medium = MediumA,
    nConExtWin=nConExtWin,
    nPorts=3,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    nSurBou=0,
    datConBou(
      layers={Floor},
      each A=67.5,
      each til=F_),
    AFlo=67.5,
    nConBou=nConBou,
    nConExt=1,
    datConExtWin(
      layers={Wall,Wall,Wall,Wall},
      each A=19.19,
      glaSys={GlaSys_Argon,GlaSys_Argon,GlaSys_Argon,GlaSys_Argon},
      wWin={3,3,3,1.7},
      hWin={1.7,1.7,1.7,1},
      each fFra=0.1,
      each til=Z_,
      azi={W_,S_,E_,N_}),
    m_flow_nominal=0.08,
    nConPar=2,
    hRoo=5,
    datConPar(
      layers={Partition,Partition},
      A={19.19,67.5},
      til={Z_,C_}),
    datConExt(
      layers={Roof},
      A={135},
      til={C_},
      azi={S_}),
    linearizeRadiation=true,
    T_start=293.15,
    lat=0.79587013890941,
    C_inHouse=partition_int.c*partition_int.d*3) "Room model for Case 600"
    annotation (Placement(transformation(extent={{646,-100},{676,-70}})));
  Modelica.Blocks.Sources.Constant qConGai_flow(k=50/67.5)
    "Convective heat gain"
    annotation (Placement(transformation(extent={{490,42},{508,60}})));
  Modelica.Blocks.Sources.Constant qRadGai_flow(k=50/67.5)
    "Radiative heat gain"
    annotation (Placement(transformation(extent={{520,60},{540,80}})));
  Modelica.Blocks.Routing.Multiplex3 multiplex3_1
    annotation (Placement(transformation(extent={{562,42},{580,60}})));
  Modelica.Blocks.Sources.Constant qLatGai_flow(k=0) "Latent heat gain"
    annotation (Placement(transformation(extent={{520,20},{540,40}})));
  Modelica.Blocks.Sources.Constant uSha(k=0)
    "Control signal for the shading device"
    annotation (Placement(transformation(extent={{500,90},{520,110}})));
  Modelica.Blocks.Routing.Replicator replicator(nout=max(1,nConExtWin))
    annotation (Placement(transformation(extent={{562,90},{580,110}})));
  Modelica.Thermal.HeatTransfer.Sources.FixedTemperature TSoi[nConBou](each T=
        283.15) "Boundary condition for construction"
                                          annotation (Placement(transformation(
        extent={{0,0},{-24,24}},
        rotation=0,
        origin={736,-146})));
  Buildings.HeatTransfer.Conduction.SingleLayer soi(
    A=48,
    material=soil,
    steadyStateInitial=true,
    T_a_start=283.15,
    T_b_start=283.75) "2m deep soil (per definition on p.4 of ASHRAE 140-2007)"
    annotation (Placement(transformation(
        extent={{5,-5},{-3,3}},
        rotation=-90,
        origin={669,-105})));
  Buildings.Fluid.Sources.Outside souInf(redeclare package Medium = MediumA,
      nPorts=1) "Source model for air infiltration"
           annotation (Placement(transformation(extent={{598,-110},{610,-98}})));
  Buildings.Fluid.Sensors.Density density(redeclare package Medium = MediumA)
    "Air density inside the building"
    annotation (Placement(transformation(extent={{620,-166},{602,-150}})));
  Buildings.Fluid.FixedResistances.FixedResistanceDpM   heaCoo(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    m_flow_nominal=0.05 + 67.5*2.7*0.41/3600*1.2) "Heater and cooler"
    annotation (Placement(transformation(extent={{622,-110},{634,-98}})));

  Buildings.Fluid.Movers.FlowMachine_m_flow C6(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    m_flow(start=0.04),
    m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    tau=tau)        annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={68,-86})));
  Buildings.Fluid.Sources.Boundary_pT   boundary(         redeclare package
      Medium =         MediumE,
    use_T_in=true,
    nPorts=1)
    annotation (Placement(transformation(extent={{72,4},{60,16}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        MediumE,
    use_m_flow_in=false,
    use_T_in=true,
    m_flow=0.000001,
    nPorts=1)
    annotation (Placement(transformation(extent={{86,14},{98,26}})));
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    redeclare package Medium = MediumE,
    T_start=T_start,
    V_start(displayUnit="l") = 0.3)
    annotation (Placement(transformation(extent={{-60,-70},{-34,-44}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(
      computeWetBulbTemperature=false, filNam=
        "D:/Github/solarsystem/Meteo/Chambery/FR_Chambery_74910TM2.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-30,94},{-50,112}})));
  Buildings.HeatTransfer.Sources.FixedTemperature Troom1(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{-8,-8},{8,8}},
        rotation=90,
        origin={112,-62})));
  Buildings.HeatTransfer.Sources.FixedTemperature Troom2(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{-8,-8},{8,8}},
        rotation=90,
        origin={230,-82})));
  Buildings.Fluid.FixedResistances.Pipe flowBetween_tanks(
                                                         redeclare package
      Medium = MediumE,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    nSeg=8,
    length=4,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{132,58},{144,70}})));
  Buildings.Fluid.FixedResistances.Pipe inFlowTop_Storage(
                                                         redeclare package
      Medium = MediumE,
    nSeg=6,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=3,
    useMultipleHeatPorts=true,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    linearizeFlowResistance=linearizeFlowResistance)      annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={52,44})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=m_flow_nominal*{-1,1,-1},
    T_start=T_start,
    dp_nominal={-dpPip_nominal,0,-dpPip_nominal}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={52,64})));
  Buildings.Fluid.FixedResistances.Pipe outFlow_panels(
                                                      redeclare package Medium
      = MediumE,
    nSeg=24,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=12,
    useMultipleHeatPorts=true,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{12,56},{28,72}})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_panels(
                                                     redeclare package Medium
      = MediumE, thicknessIns(displayUnit="mm") = thicknessIns,
    nSeg=24,
    lambdaIns=lambdaIns,
    length=12,
    useMultipleHeatPorts=true,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{-11,-7},{11,7}},
        rotation=90,
        origin={-81,-17})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal1(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=m_flow_nominal*{1,-1,1},
    T_start=T_start,
    dp_nominal={0,-dpPip_nominal,dpPip_nominal}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={68,-120})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal2(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=m_flow_nominal*{1,-1,1},
    T_start=T_start,
    dp_nominal={0,-dpPip_nominal,dpPip_nominal}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={182,-120})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal3(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=m_flow_nominal*1.75*{1,-1,1},
    T_start=T_start,
    dp_nominal={-dpPip_nominal,0,-dpPip_nominal}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={360,-120})));

  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal4(
    redeclare package Medium =
        MediumE,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal*1.75*{-1,1,-1},
    dp_nominal={-dpPip_nominal,0,-dpPip_nominal}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={362,64})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_topExtra(
                                                      redeclare package Medium
      = MediumE,
    nSeg=6,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=3,
    m_flow_nominal=m_flow_nominal*1.75,
    T_start=T_start,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={362,34})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_heating(
                                                      redeclare package Medium
      = MediumE,
    nSeg=26,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=13,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal*1.75,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=180,
        origin={386,64})));
  Buildings.Fluid.FixedResistances.Pipe outFlow_heating(
                                                       redeclare package Medium
      = MediumE,
    nSeg=6,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=3,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal*1.75,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=0,
        origin={336,-120})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM spl(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=m_flow_nominal*1.75*{-1,1,-1},
    T_start=T_start,
    dp_nominal={-dpPip_nominal,0,-dpPip_nominal})
    annotation (Placement(transformation(extent={{266,58},{278,70}})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_backup(
                                                     redeclare package Medium
      = MediumE,
    nSeg=12,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=6,
    m_flow_nominal=m_flow_nominal*1.75,
    T_start=T_start,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={272,-58})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM splitVal5(
    redeclare package Medium =
        MediumE,
    m_flow_nominal=m_flow_nominal*{-1,1,-1},
    T_start=T_start,
    dp_nominal={-dpPip_nominal,0,-dpPip_nominal}) "Flow splitter"
                                                       annotation (Placement(
        transformation(
        extent={{-6,6},{6,-6}},
        rotation=180,
        origin={182,64})));
  Buildings.Fluid.FixedResistances.Pipe flow_between_tankAndBackup(
                                                                  redeclare
      package Medium = MediumE,
    nSeg=8,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=4,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{232,58},{244,70}})));
  Buildings.Fluid.FixedResistances.Pipe inFlowLow_storage(
                                                         redeclare package
      Medium = MediumE,
    nSeg=6,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=3,
    useMultipleHeatPorts=true,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{-6,-6},{6,6}},
        rotation=90,
        origin={16,-76})));
  Modelica.Blocks.Math.RealToBoolean real_solar
                                           annotation (Placement(transformation(
        extent={{-9,9},{9,-9}},
        rotation=0,
        origin={259,-159})));

  Modelica.Blocks.Sources.RealExpression fake_room(each y=30 + 273.15)
    "algo need at least 2 heating pumps"
    annotation (Placement(transformation(extent={{-84,-298},{-44,-278}})));
  SolarSystem.Utilities.Limit.Shadow shadow(n=5)
    annotation (Placement(transformation(extent={{24,88},{-16,112}})));
  Buildings.Fluid.FixedResistances.Pipe flowBetween_tanks1(
                                                         redeclare package
      Medium = MediumE,
    nSeg=8,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=4,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{146,-126},{134,-114}})));
  Buildings.Fluid.FixedResistances.Pipe flowBetween_tanks2(
                                                         redeclare package
      Medium = MediumE,
    nSeg=8,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=4,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{230,-126},{218,-114}})));
  Buildings.Fluid.FixedResistances.Pipe inFlowTop_Storage1(
                                                         redeclare package
      Medium = MediumE,
    nSeg=12,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=6,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    linearizeFlowResistance=linearizeFlowResistance)      annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={182,32})));
  Buildings.Fluid.FixedResistances.Pipe outFlow_heating1(
                                                       redeclare package Medium
      = MediumE,
    nSeg=26,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=13,
    m_flow_nominal=m_flow_nominal*1.75,
    T_start=T_start,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=0,
        origin={386,-120})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_backup1(
                                                     redeclare package Medium
      = MediumE,
    nSeg=6,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    length=3,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal*1.75)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={318,-26})));

  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature
    prescribedTemperature[24]
    annotation (Placement(transformation(extent={{-20,-12},{-40,8}})));

  Modelica.Blocks.Routing.Replicator replicator_Text(nout=24) annotation (
      Placement(transformation(
        extent={{-9,-8},{9,8}},
        rotation=90,
        origin={0,-21})));
  SolarSystem.Utilities.Limit.Overheat limit(n=1, threshold={323.15,343.15}) annotation (
      Placement(transformation(
        extent={{8,-6},{-8,6}},
        rotation=90,
        origin={294,24})));
  Modelica.Blocks.Math.MultiProduct gain(nu=2) annotation (Placement(
        transformation(
        extent={{-2,-2},{2,2}},
        rotation=90,
        origin={284,8})));
  Modelica.Blocks.Sources.Constant extraction_meca(k=-150/3600)
    "0.41 ACH adjusted for the altitude (0.5 at sea level)" annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=180,
        origin={516,-120})));
  Modelica.Blocks.Math.Product product1
    annotation (Placement(transformation(extent={{548,-134},{566,-116}})));
  Buildings.Fluid.Sources.MassFlowSource_T sinInf1(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{602,-134},{618,-118}})));
  Buildings.Fluid.FixedResistances.Pipe inFlow_heating1(
                                                      redeclare package Medium
      = MediumE,
    thicknessIns=thicknessIns,
    lambdaIns=lambdaIns,
    T_start=T_start,
    nSeg=12,
    length=6,
    m_flow_nominal=m_flow_nominal*1.75,
    linearizeFlowResistance=linearizeFlowResistance)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={438,34})));
   parameter Buildings.HeatTransfer.Data.Solids.Generic soil(
    k=1.9,
    c=790,
    d=1900,
    x=3) "Soil properties"
    annotation (Placement(transformation(extent={{692,14},{712,34}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Wall(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=3,
    material={SolarSystem.Data.Layers.WoodPanel(x=0.04),
        SolarSystem.Data.Layers.GlassWool(x=0.05),
        SolarSystem.Data.Layers.Wood(x=0.04)}) "Wall layers"
    annotation (Placement(transformation(extent={{664,70},{684,90}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Roof(        nLay=
       2, material={SolarSystem.Data.Layers.Wood(x=0.1),
        SolarSystem.Data.Layers.GlassWool(x=0.095)}) "Roof layers"
    annotation (Placement(transformation(extent={{692,92},{714,114}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Floor(nLay=2, material=
       {SolarSystem.Data.Layers.GlassWool(x=0.05),
        Buildings.HeatTransfer.Data.Solids.Concrete(x=0.3)}) "Floor layer"
    annotation (Placement(transformation(extent={{694,44},{714,64}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Partition(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=2,
    material={SolarSystem.Data.Layers.WoodPanel(x=0.03),
        Buildings.HeatTransfer.Data.Solids.Concrete(x=0.2)}) "Wall layers"
    annotation (Placement(transformation(extent={{694,68},{714,88}})));
  Data.Layers.DoubleClearArgon16 GlaSys_Argon
    annotation (Placement(transformation(extent={{724,68},{744,88}})));
  SolarSystem.Data.Layers.Wood_part partition_int(x=4)
    annotation (Placement(transformation(extent={{690,-30},{714,-6}})));
  Buildings.Fluid.Sensors.MassFlowRate mFlow_drawingUp(redeclare package Medium
      = MediumE) annotation (Placement(transformation(
        extent={{-9,-8},{9,8}},
        rotation=90,
        origin={148,-1})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T_cold_water(
    redeclare package Medium = MediumE,
    m_flow_nominal=0.04,
    tau=10) annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=90,
        origin={256,-70})));
  SolarSystem.Utilities.Limit.NoFreeze tlimit annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={0,-48})));
  Modelica.Blocks.Math.Min min1 annotation (Placement(transformation(
        extent={{-3,-3},{3,3}},
        rotation=90,
        origin={5,-63})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tmini_before
    annotation (Placement(transformation(extent={{-46,-30},{-34,-18}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tmini_before1
    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={28,22})));
  Modelica.Blocks.Sources.CombiTimeTable Water_temp(extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic, table=[0,
        273.15 + 6.4; 2628000,273.15 + 7.4; 5256000,273.15 + 8.9; 7884000,
        273.15 + 10; 10512000,273.15 + 12; 13140000,273.15 + 14; 15768000,
        273.15 + 15; 18396000,273.15 + 15; 21024000,273.15 + 13; 23652000,
        273.15 + 11; 26280000,273.15 + 8; 28908000,273.15 + 6.8])
    "Cold water temperature according to month of the year"
    annotation (Placement(transformation(extent={{232,-112},{244,-100}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T_inRad(redeclare package Medium
      = MediumE, m_flow_nominal=m_flow_nominal*1.75,
    T_start=T_start,
    tau=tau)                                        annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={438,-12})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Tafter_boiler(
    redeclare package Medium = MediumE,
    m_flow_nominal=m_flow_nominal*1.75,
    tau=15,
    T_start=T_start)
            annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=90,
        origin={272,48})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T_before_boiler(
    redeclare package Medium = MediumE,
    m_flow_nominal=m_flow_nominal*1.75,
    tau=15,
    T_start=T_start)
            annotation (Placement(transformation(
        extent={{-6,6},{6,-6}},
        rotation=90,
        origin={272,-14})));
  Valves.Thermostatic_valve thermostatic_valve1(
    m_flow_nominal=0.04,
    redeclare package Medium = MediumE,
    control=[0.0,313.15],
    k=0.05,
    Nd=20,
    fraK=0.5,
    deltaM=deltaM,
    controllerType=Modelica.Blocks.Types.SimpleController.PI)
              annotation (Placement(transformation(
        extent={{9,10},{-9,-10}},
        rotation=90,
        origin={149,-28})));
  Buildings.HeatTransfer.Sources.FixedTemperature Troom3(T=293.15)
    "Room temperature"
    annotation (Placement(transformation(extent={{232,28},{248,44}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Tdrawing_up(
    redeclare package Medium = MediumE,
    m_flow_nominal=0.04,
    tau=tau)
            annotation (Placement(transformation(
        extent={{-8,-8},{8,8}},
        rotation=90,
        origin={148,22})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C2(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal*1.75,
    tau=tau)        annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={438,-86})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C5(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    tau=tau)        annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={182,-86})));
  Buildings.Fluid.Movers.FlowMachine_m_flow C4(
    redeclare package Medium =
        MediumE,
    m_flow_start=0.1,
    motorCooledByFluid=true,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    tau=tau)        annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={318,-52})));
  // Sensors
public
  Buildings.Fluid.Sensors.TemperatureTwoPort T1(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    tau=tau)
    annotation (Placement(transformation(extent={{-14,54},{6,74}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T2(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    tau=tau)
    annotation (Placement(transformation(extent={{52,-130},{32,-110}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T3
    annotation (Placement(transformation(extent={{200,16},{214,30}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T4
    annotation (Placement(transformation(extent={{200,38},{214,52}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T5
    annotation (Placement(transformation(extent={{74,22},{62,34}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T7(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    tau=tau)
    annotation (Placement(transformation(extent={{294,-130},{314,-110}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort T8(redeclare package Medium =
        MediumE, m_flow_nominal=m_flow_nominal*1.75,
    T_start=T_start,
    tau=tau)
    annotation (Placement(transformation(extent={{324,54},{344,74}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TRooAir
    "Room air temperature"
    annotation (Placement(transformation(extent={{728,-58},{744,-42}})));
   Modelica.Blocks.Math.Mean TRooDay(y(start=293.15), f=1/3600/24)
    "Month averaged room air temperature"
    annotation (Placement(transformation(extent={{780,-60},{800,-40}})));
  // Valves
  Buildings.Fluid.Actuators.Valves.ThreeWayLinear solar_valve(
    fraK=0.5,
    l={0.001,0.001},
    redeclare package Medium = MediumE,
    m_flow_nominal=m_flow_nominal,
    T_start=T_start,
    dpValve_nominal=1500,
    deltaM=deltaM)
    annotation (Placement(transformation(extent={{-20,-110},{0,-130}})));
  Buildings.Fluid.Actuators.Valves.ThreeWayLinear extra_valve(
    fraK=0.5,
    l={0.001,0.001},
    redeclare package Medium = MediumE,
    T_start=T_start,
    dpValve_nominal=3000,
    m_flow_nominal=m_flow_nominal,
    deltaM=deltaM)
    annotation (Placement(transformation(extent={{262,-110},{282,-130}})));
  // Algorithms
  SolarSystem.Control.SolisArt_mod.pump_algo pump_algo(
    m_flow_mini=0,
    Sj_wd=0.2,
    Sj_controller=Modelica.Blocks.Types.SimpleController.PID,
    Sj_Nd=20,
    Sj_Ti=150,
    Sj_Td=220,
    Sj_wp=0.8,
    S4_Ti=200,
    S4_Td=220,
    S4_wp=0.7,
    S4_wd=0.4,
    S6_wp=0.8,
    S6_wd=0.2,
    S6_Ti=200,
    S6_Td=220,
    S5_Td=250,
    S5_Ti=200,
    S5_wp=0.8,
    S5_wd=0.2,
    S4_k=0.1,
    pid_S4(reverseAction=true),
    Sj_reverseAction=true,
    Sj_k=0.05,
    nb_panel=solarPanel_ISO.nPanels,
    area_panel=solarPanel_ISO.per.A)
    annotation (Placement(transformation(extent={{290,-274},{462,-172}})));
  SolarSystem.Control.SolisArt_mod.Algo_one
                            algo(WeatherFile=Meteo.filNam, Tambiant_out_start={
        293.15,303.15})
    annotation (Placement(transformation(extent={{32,-286},{230,-152}})));
  // Energy integrators
  SolarSystem.Utilities.Other.Integrator_energy integrator_collector
    annotation (Placement(transformation(extent={{-6,30},{16,44}})));
  SolarSystem.Utilities.Other.Integrator_energy integrator_boiler
    annotation (Placement(transformation(extent={{322,20},{350,40}})));
  SolarSystem.Utilities.Other.Integrator_energy integrator_radiator
    annotation (Placement(transformation(extent={{406,-28},{384,-10}})));
  SolarSystem.Utilities.Other.Integrator_energy integrator_drawingUp
    annotation (Placement(transformation(extent={{138,-64},{166,-46}})));
  // Pumps and mass flows
  Buildings.Fluid.Sensors.MassFlowRate mFlow_collector(redeclare package Medium
      = MediumE) annotation (
      Placement(transformation(
        extent={{-9,9},{9,-9}},
        rotation=90,
        origin={-81,17})));
  Buildings.Fluid.Sensors.MassFlowRate mFlow_boiler(redeclare package Medium =
        MediumE)                                     annotation (Placement(
        transformation(
        extent={{-9,8},{9,-8}},
        rotation=90,
        origin={272,-35})));
  Buildings.Fluid.Sensors.MassFlowRate mFlow_radiator(redeclare package Medium
      = MediumE) annotation (Placement(transformation(
        extent={{9,-8},{-9,8}},
        rotation=90,
        origin={438,7})));
  // Other
  SolarSystem.Valves.DrawingUp.Water_Drawing_up_control
                                     Drawing_up1(
    redeclare package Medium = MediumE,
    table=[0,0,0; 6.99,0,0; 7,24,0.01; 8.99,24,0.01; 9,0,0; 11.99,0,0; 12,24,
        0.01; 13.99,24,0.01; 14,0,0; 18.99,0,0; 19,117,0.03; 20.99,117,0.03;
        21,0,0; 24,0,0],
    f_cut=1/180,
    gain=0.6,
    start_value(displayUnit="d") = 0)
    annotation (Placement(transformation(extent={{130,34},{166,52}})));
  SolarSystem.Heat_transfer.RadiatorEN442_2_extratT rad(
    redeclare package Medium = MediumE,
    nEle=20,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal*1.75,
    Q_flow_nominal=10000,
    T_a_nominal=313.15,
    T_b_nominal=303.15,
    n=1.287) annotation (Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=-90,
        origin={438,-38})));
  SolarSystem.Solar_Collector.EN12975          solarPanel_ISO(
    redeclare package Medium = MediumE,
    azi=0,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Series,
    nSeg=40,
    use_shaCoe_in=true,
    T_start=T_start,
    til=0.5235987755983,
    per=SolarSystem.Data.Parameter.SolarCollector.IDMK2_5_buildings(),
    lat(displayUnit="deg") = 0.84735735184325,
    computeFlowResistance=computeFlowResistance,
    linearizeFlowResistance=linearizeFlowResistance,
    nPanels=3) annotation (Placement(transformation(extent={{-72,42},{-28,90}})));
  Buildings.Fluid.Boilers.BoilerPolynomial boi(
    redeclare package Medium =
        MediumE,
    fue=Buildings.Fluid.Data.Fuels.NaturalGasHigherHeatingValue(),
    dp_nominal=1000,
    m_flow_nominal=m_flow_nominal*1.75,
    T_start=T_start,
    Q_flow_nominal=15000,
    effCur=Buildings.Fluid.Types.EfficiencyCurves.Constant,
    linearizeFlowResistance=linearizeFlowResistance)               annotation (
      Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={272,24})));
  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex Storage_tank(
    redeclare package Medium = MediumE,
    redeclare package MediumHex = MediumE,
    hTan=1.6,
    dIns(displayUnit="mm") = 0.1,
    nSeg=20,
    m_flow_nominal=0.000001,
    tau(displayUnit="s") = 30,
    hHex_a=1.415,
    hHex_b=0.255,
    hexSegMult=3,
    Q_flow_nominal(displayUnit="kW") = 103000,
    mHex_flow_nominal=0.36,
    dExtHex(displayUnit="mm") = 0.0346,
    T_start=T_start,
    VTan(displayUnit="l") = 0.5,
    TTan_nominal=283.15,
    THex_nominal=318.15)
    annotation (Placement(transformation(extent={{68,-42},{110,10}})));
  SolarSystem.Exchanger_and_Tank.StratifiedEnhancedInternal_TwoHex Solar_tank(
    nSeg=20,
    dIns(displayUnit="mm") = 0.055,
    tau(displayUnit="s") = 120,
    redeclare package Medium = MediumE,
    redeclare package MediumHex = MediumE,
    hHex_a={0.86,1.375},
    hHex_b={0.175,0.955},
    Q_flow_nominal={25000,53000},
    mHex_flow_nominal={0.1747,0.36},
    dExtHex(displayUnit="mm") = {0.0279,0.0337},
    m_flow_nominal=m_flow_nominal,
    hTan=1.67,
    VTan(displayUnit="l") = 0.4,
    TTan_nominal={318.15,333.15},
    THex_nominal={283.15,353.15})
    annotation (Placement(transformation(extent={{184,-40},{232,16}})));
  // Boundaries
  Buildings.Fluid.Sources.Boundary_pT   Cold_water(redeclare package Medium =
        MediumE,
    nPorts=1,
    use_T_in=true,
    T=283.15)
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=-90,
        origin={256,-98})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{-72,-282},{-44,-254}})));

  parameter Boolean linearizeFlowResistance=true
    "= true, use linear relation between m_flow and dp for any flow rate";
  parameter Boolean computeFlowResistance=false
    "=true, compute flow resistance. Set to false to assume no friction";
equation
  connect(qRadGai_flow.y,multiplex3_1. u1[1])  annotation (Line(
      points={{541,70},{550,70},{550,57.3},{560.2,57.3}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qLatGai_flow.y,multiplex3_1. u3[1])  annotation (Line(
      points={{541,30},{550,30},{550,44.7},{560.2,44.7}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiplex3_1.y,roo. qGai_flow) annotation (Line(
      points={{580.9,51},{632,51},{632,-79},{644.5,-79}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.uSha,replicator. y) annotation (Line(
      points={{644.5,-73},{636,-73},{636,100},{580.9,100}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qConGai_flow.y,multiplex3_1. u2[1]) annotation (Line(
      points={{508.9,51},{560.2,51}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(uSha.y,replicator. u) annotation (Line(
      points={{521,100},{560.2,100}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(density.port,roo. ports[1])  annotation (Line(
      points={{611,-166},{644,-166},{644,-94.5},{649.75,-94.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TSoi[1].port,soi. port_a) annotation (Line(
      points={{712,-134},{668,-134},{668,-110}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(soi.port_b,roo. surf_conBou[1]) annotation (Line(
      points={{668,-102},{668,-97},{665.5,-97}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(roo.heaPorAir,TRooAir. port)  annotation (Line(
      points={{660.25,-85},{712,-85},{712,-50},{728,-50}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(souInf.ports[1],heaCoo. port_a)        annotation (Line(
      points={{610,-104},{622,-104}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(heaCoo.port_b,roo. ports[2])  annotation (Line(
      points={{634,-104},{644,-104},{644,-92.5},{649.75,-92.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooDay.u) annotation (Line(
      points={{744,-50},{778,-50}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(Meteo.weaBus,solarPanel_ISO. weaBus) annotation (Line(
      points={{-50,103},{-72,103},{-72,89.04}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(splitVal.port_1,flowBetween_tanks. port_a)
                                              annotation (Line(
      points={{58,64},{132,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal.port_3,inFlowTop_Storage. port_a)
                                                annotation (Line(
      points={{52,58},{52,50}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(outFlow_panels.port_b,splitVal. port_2)      annotation (Line(
      points={{28,64},{46,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C6.port_b,splitVal1. port_3) annotation (Line(
      points={{68,-96},{68,-114}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C5.port_b,splitVal2. port_3) annotation (Line(
      points={{182,-96},{182,-114}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C4.port_b,splitVal3. port_3) annotation (Line(
      points={{318,-62},{318,-92},{360,-92},{360,-114}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T8.port_b,splitVal4. port_2) annotation (Line(
      points={{344,64},{356,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal4.port_3,inFlow_topExtra. port_a) annotation (Line(
      points={{362,58},{362,40}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal4.port_1,inFlow_heating. port_a)  annotation (Line(
      points={{368,64},{380,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal3.port_2,outFlow_heating. port_a)  annotation (Line(
      points={{354,-120},{342,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(outFlow_heating.port_b,T7. port_b)  annotation (Line(
      points={{330,-120},{314,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(spl.port_2,T8. port_a) annotation (Line(
      points={{278,64},{324,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flowBetween_tanks.port_b,splitVal5. port_2)
                                                   annotation (Line(
      points={{144,64},{176,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flow_between_tankAndBackup.port_b,spl. port_1)
                                              annotation (Line(
      points={{244,64},{266,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal5.port_1,flow_between_tankAndBackup. port_a)
                                                    annotation (Line(
      points={{188,64},{232,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal1.port_2,T2. port_a) annotation (Line(
      points={{62,-120},{52,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solarPanel_ISO.port_b,T1. port_a) annotation (Line(
      points={{-28,66},{-22,66},{-22,64},{-14,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T1.port_b,outFlow_panels. port_a)      annotation (Line(
      points={{6,64},{12,64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Troom3.port,boi. heatPort) annotation (Line(
      points={{248,36},{254,36},{254,24},{261.92,24}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Meteo.weaBus,weaBus)  annotation (Line(
      points={{-50,103},{-96,103},{-96,-268},{-58,-268}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(T2.port_b,solar_valve. port_2) annotation (Line(
      points={{32,-120},{0,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_3,inFlowLow_storage. port_a)    annotation (Line(
      points={{-10,-110},{-10,-102},{16,-102},{16,-82}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_1,inFlow_panels. port_a)        annotation (Line(
      points={{-20,-120},{-81,-120},{-81,-28}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(solar_valve.port_1,SurgeTank. port_a) annotation (Line(
      points={{-20,-120},{-47,-120},{-47,-70}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T7.port_a, extra_valve.port_2) annotation (Line(
      points={{294,-120},{282,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(extra_valve.port_3, inFlow_backup.port_a) annotation (Line(
      points={{272,-110},{272,-64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T5.T,boundary1. T_in) annotation (Line(
      points={{62,28},{62,40},{82,40},{82,22},{84,22},{84,22.4},{84.8,22.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Meteo.weaBus, roo.weaBus) annotation (Line(
      points={{-50,103},{-94,103},{-94,-300},{680,-300},{680,-60},{674.425,-60},
          {674.425,-71.575}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Meteo.weaBus, souInf.weaBus) annotation (Line(
      points={{-50,103},{-94,103},{-94,-300},{680,-300},{680,-144},{570,
          -144},{570,-103.88},{598,-103.88}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(rad.heatPortCon, roo.heaPorAir) annotation (Line(
      points={{448.08,-35.2},{660.25,-35.2},{660.25,-85}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=1));
  connect(rad.heatPortRad, roo.heaPorRad) annotation (Line(
      points={{448.08,-40.8},{498,-40.8},{498,-88},{660.25,-88},{660.25,-87.85}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=1));

  connect(real_solar.y, pump_algo.V3V_extra) annotation (Line(
      points={{268.9,-159},{278,-159},{278,-181.69},{291.518,-181.69}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo.S6_outter, pump_algo.pumpControl_S6) annotation (Line(
      points={{231.98,-199.688},{260,-199.688},{260,-202.09},{291.518,-202.09}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo.S5_outter, pump_algo.pumpControl_S5) annotation (Line(
      points={{231.98,-216.241},{259.99,-216.241},{259.99,-222.49},{291.518,
          -222.49}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo.S4_outter, pump_algo.pumpControl_S4) annotation (Line(
      points={{231.98,-232.006},{259.99,-232.006},{259.99,-242.89},{291.518,
          -242.89}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo.Sj_outter, pump_algo.pumpControl_Sj) annotation (Line(
      points={{231.98,-247.771},{258.99,-247.771},{258.99,-263.29},{291.518,
          -263.29}},
      color={255,0,255},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(pump_algo.pumps_miniFlows[3], algo.mini_flow_S4) annotation (Line(
      points={{391.176,-276.04},{391.176,-296},{-22,-296},{-22,-138},{79.124,
          -138},{79.124,-151.606}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(pump_algo.S4_outter, algo.flow_S4) annotation (Line(
      points={{464.024,-182.2},{480,-182.2},{480,-138},{59.324,-138},{59.324,
          -151.606}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(pump_algo.S4_outter, C4.m_flow_in) annotation (Line(
      points={{464.024,-182.2},{480,-182.2},{480,-60},{420,-60},{420,-52},{330,
          -52},{330,-51.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(pump_algo.S5_outter, C5.m_flow_in) annotation (Line(
      points={{464.024,-208.72},{480,-208.72},{480,-138},{200,-138},{200,-85.8},
          {194,-85.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(pump_algo.S6_outter, C6.m_flow_in) annotation (Line(
      points={{463.012,-239.32},{480,-239.32},{480,-138},{100,-138},{100,-85.8},
          {80,-85.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo.V3V_extra_outter, real_solar.u) annotation (Line(
      points={{231.98,-159.488},{239.99,-159.488},{239.99,-159},{248.2,-159}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(algo.V3V_extra_outter, extra_valve.y) annotation (Line(
      points={{231.98,-159.488},{240,-159.488},{240,-138},{272,-138},{272,-132}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(algo.V3V_solar_outter, solar_valve.y) annotation (Line(
      points={{231.98,-175.253},{240,-175.253},{240,-138},{-10,-138},{-10,-132}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T3.T, algo.T3) annotation (Line(
      points={{214,23},{220,23},{220,120},{-100,120},{-100,-176.829},{32.396,
          -176.829}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T4.T, algo.T4) annotation (Line(
      points={{214,45},{220,45},{220,120},{-100,120},{-100,-188},{30,-188},{30,
          -188.653},{32.396,-188.653}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T5.T, algo.T5) annotation (Line(
      points={{62,28},{40,28},{40,120},{-100,120},{-100,-199.688},{32.396,
          -199.688}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T7.T, algo.T7) annotation (Line(
      points={{304,-109},{304,120},{-100,120},{-100,-210},{32.396,-210},{32.396,
          -211.512}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(weaBus.TDryBul, algo.Text) annotation (Line(
      points={{-58,-268},{-12.5,-268},{-12.5,-267.871},{33.584,-267.871}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(T8.T, algo.T8) annotation (Line(
      points={{334,75},{334,120},{-100,120},{-100,-223.335},{32.396,-223.335}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(TRooAir.T, algo.Tambiant[1]) annotation (Line(
      points={{744,-50},{752,-50},{752,42},{782,42},{782,120},{-100,120},{-100,
          -253.879},{31.604,-253.879}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(fake_room.y, algo.Tambiant[2]) annotation (Line(
      points={{-42,-288},{0,-288},{0,-249.544},{31.604,-249.544}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T3.T, pump_algo.T3) annotation (Line(
      points={{214,23},{220,23},{220,120},{-100,120},{-100,-138},{330.471,-138},
          {330.471,-174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T4.T, pump_algo.T4) annotation (Line(
      points={{214,45},{220,45},{220,120},{-100,120},{-100,-138},{350,-138},{
          350,-174.04},{350.706,-174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T5.T, pump_algo.T5) annotation (Line(
      points={{62,28},{40,28},{40,120},{-100,120},{-100,-138},{370.941,-138},{
          370.941,-174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T7.T, pump_algo.T7) annotation (Line(
      points={{304,-109},{304,120},{-100,120},{-100,-138},{391.176,-138},{
          391.176,-174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T8.T, pump_algo.T8) annotation (Line(
      points={{334,75},{334,120},{-100,120},{-100,-138},{411.412,-138},{411.412,
          -174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(shadow.out_value, solarPanel_ISO.shaCoe_in) annotation (Line(
      points={{-18.4,100},{-24,100},{-24,92},{-86,92},{-86,72.24},{-76.4,72.24}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[1], shadow.in_value[1]) annotation (Line(
      points={{-25.8,44.52},{-18,44.52},{-18,82},{32,82},{32,100},{25.6,100},{25.6,
          98.08}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[10], shadow.in_value[2]) annotation (Line(
      points={{-25.8,46.68},{-18,46.68},{-18,82},{32,82},{32,100},{25.6,100},{25.6,
          99.04}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[20], shadow.in_value[3]) annotation (Line(
      points={{-25.8,49.08},{-18,49.08},{-18,82},{32,82},{32,100},{25.6,100}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[30], shadow.in_value[4]) annotation (Line(
      points={{-25.8,51.48},{-18,51.48},{-18,82},{32,82},{32,100},{25.6,100},{25.6,
          100.96}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[40], shadow.in_value[5]) annotation (Line(
      points={{-25.8,53.88},{-18,53.88},{-18,82},{32,82},{32,100},{25.6,100},{25.6,
          101.92}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[40], algo.T1) annotation (Line(
      points={{-25.8,53.88},{40,53.88},{40,120},{-100,120},{-100,-165.006},{
          32.396,-165.006}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[40], pump_algo.T1) annotation (Line(
      points={{-25.8,53.88},{40,53.88},{40,120},{-100,120},{-100,-138},{310.235,
          -138},{310.235,-174.04}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(splitVal2.port_2, flowBetween_tanks1.port_a) annotation (Line(
      points={{176,-120},{146,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flowBetween_tanks1.port_b, splitVal1.port_1) annotation (Line(
      points={{134,-120},{74,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(extra_valve.port_1, flowBetween_tanks2.port_a) annotation (Line(
      points={{262,-120},{230,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(flowBetween_tanks2.port_b, splitVal2.port_1) annotation (Line(
      points={{218,-120},{188,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(splitVal5.port_3, inFlowTop_Storage1.port_a) annotation (Line(
      points={{182,58},{182,38}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(outFlow_heating1.port_b, splitVal3.port_1) annotation (Line(
      points={{380,-120},{366,-120}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(inFlow_backup1.port_b, C4.port_a) annotation (Line(
      points={{318,-32},{318,-42}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(prescribedTemperature.port, inFlow_panels.heatPorts) annotation (Line(
      points={{-40,-2},{-60,-2},{-60,-17},{-77.5,-17}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(prescribedTemperature.port, outFlow_panels.heatPorts) annotation (
      Line(
      points={{-40,-2},{-60,-2},{-60,20},{20,20},{20,60}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(replicator_Text.y, prescribedTemperature.T) annotation (Line(
      points={{4.44089e-016,-11.1},{4.44089e-016,-2},{-18,-2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gain.y, boi.y) annotation (Line(
      points={{284,10.34},{284,12},{280,12},{280,2},{260.8,2},{260.8,7.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(limit.out_value, gain.u[1]) annotation (Line(
      points={{294,15.04},{294,6},{283.3,6}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));

  connect(boi.T, limit.in_value[1]) annotation (Line(
      points={{260.8,39.4},{260.8,42},{294,42},{294,32.64}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(algo.BackupHeater_outter, gain.u[2]) annotation (Line(
      points={{231.98,-274.571},{238,-274.571},{238,-296},{480,-296},{480,20},{
          400,20},{400,4},{284,4},{284,6},{284.7,6}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(product1.y, sinInf1.m_flow_in) annotation (Line(
      points={{566.9,-125},{575.45,-125},{575.45,-119.6},{602,-119.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(density.d, product1.u2) annotation (Line(
      points={{601.1,-158},{540,-158},{540,-130},{546.2,-130},{546.2,-130.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(extraction_meca.y, product1.u1) annotation (Line(
      points={{527,-120},{527,-119.6},{546.2,-119.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sinInf1.ports[1], roo.ports[3]) annotation (Line(
      points={{618,-126},{644,-126},{644,-90.5},{649.75,-90.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(T_inRad.port_b, rad.port_a) annotation (Line(
      points={{438,-18},{438,-24}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(C2.port_b, outFlow_heating1.port_a) annotation (Line(
      points={{438,-96},{438,-120},{392,-120}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T_before_boiler.port_b, boi.port_a) annotation (Line(
      points={{272,-8},{272,10}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(boi.port_b, Tafter_boiler.port_a) annotation (Line(
      points={{272,38},{272,42}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Tafter_boiler.port_b, spl.port_3) annotation (Line(
      points={{272,54},{272,58}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_panels.port_b, mFlow_collector.port_a) annotation (Line(
      points={{-81,-6},{-81,8}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(mFlow_collector.port_b, solarPanel_ISO.port_a) annotation (Line(
      points={{-81,26},{-82,26},{-82,66},{-72,66}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(mFlow_collector.m_flow, integrator_collector.m_flow) annotation (Line(
      points={{-71.1,17},{-66,17},{-66,26},{-14,26},{-14,31.4},{-6,31.4}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(inFlow_backup.port_b, mFlow_boiler.port_a) annotation (Line(
      points={{272,-52},{272,-44}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(mFlow_boiler.port_b, T_before_boiler.port_a) annotation (Line(
      points={{272,-26},{272,-20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(mFlow_boiler.m_flow, integrator_boiler.m_flow) annotation (Line(
      points={{280.8,-35},{310,-35},{310,22},{322,22}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(inFlow_heating1.port_b, mFlow_radiator.port_a) annotation (Line(
      points={{438,28},{438,16}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(mFlow_radiator.port_b, T_inRad.port_a) annotation (Line(
      points={{438,-2},{438,-6}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(rad.Tinside[20], integrator_radiator.Tout) annotation (Line(
      points={{424.14,-38.56},{416,-38.56},{416,-11.8},{406,-11.8}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(rad.Tinside[1], integrator_radiator.Tin) annotation (Line(
      points={{429.46,-38.56},{416,-38.56},{416,-17.2},{406,-17.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(mFlow_radiator.m_flow, integrator_radiator.m_flow) annotation (Line(
      points={{429.2,7},{416,7},{416,-26.2},{406,-26.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[40], integrator_collector.Tout) annotation (
      Line(
      points={{-25.8,53.88},{-12,53.88},{-12,42.6},{-6,42.6}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(solarPanel_ISO.Tinside[1], integrator_collector.Tin) annotation (Line(
      points={{-25.8,44.52},{-14,44.52},{-14,38.4},{-6,38.4}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Tafter_boiler.T, integrator_boiler.Tout) annotation (Line(
      points={{278.6,48},{310,48},{310,38},{322,38}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T_before_boiler.T, integrator_boiler.Tin) annotation (Line(
      points={{278.6,-14},{310,-14},{310,32},{322,32}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Tdrawing_up.T, thermostatic_valve1.TdrawingUp) annotation (Line(
      points={{139.2,22},{126,22},{126,-38},{148,-38},{148,-33.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Tdrawing_up.port_b, Drawing_up1.port_a) annotation (Line(
      points={{148,30},{148,34.1125}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(mFlow_drawingUp.port_b, Tdrawing_up.port_a) annotation (Line(
      points={{148,8},{148,14}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(thermostatic_valve1.port_3, mFlow_drawingUp.port_a) annotation (Line(
      points={{148,-18},{148,-10}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(mFlow_drawingUp.m_flow, integrator_drawingUp.m_flow) annotation (Line(
      points={{139.2,-1},{126,-1},{126,-62.2},{138,-62.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Cold_water.ports[1], T_cold_water.port_a) annotation (Line(
      points={{256,-92},{256,-78}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T_cold_water.port_b, thermostatic_valve1.port_2) annotation (Line(
      points={{256,-62},{256,-40},{132,-40},{132,-28},{138,-28}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Tdrawing_up.T, integrator_drawingUp.Tout) annotation (Line(
      points={{139.2,22},{126,22},{126,-47.8},{138,-47.8}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(T_cold_water.T, integrator_drawingUp.Tin) annotation (Line(
      points={{247.2,-70},{126,-70},{126,-53.2},{138,-53.2}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(tlimit.Text_out, replicator_Text.u) annotation (Line(
      points={{0,-42},{0,-31.8}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(min1.y, tlimit.Tcollector) annotation (Line(
      points={{5,-59.7},{5,-56.75},{3.6,-56.75},{3.6,-54}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(inFlow_panels.heatPorts[2], Tmini_before.port) annotation (Line(
      points={{-77.5,-19.8875},{-52,-19.8875},{-52,-24},{-46,-24}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Tmini_before.T, min1.u2) annotation (Line(
      points={{-34,-24},{-20,-24},{-20,-40},{10,-40},{10,-68},{6.8,-68},{6.8,-66.6}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(outFlow_panels.heatPorts[2], Tmini_before1.port) annotation (Line(
      points={{17.9,60},{17.9,32},{28,32},{28,28}},
      color={127,0,0},
      smooth=Smooth.None));
  connect(Tmini_before1.T, min1.u1) annotation (Line(
      points={{28,16},{28,-20},{12,-20},{12,-70},{3.2,-70},{3.2,-66.6}},
      color={0,200,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(weaBus.TDryBul, tlimit.Text) annotation (Line(
      points={{-58,-268},{-94,-268},{-94,-100},{-20,-100},{-20,-80},{-3.6,-80},{
          -3.6,-54}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(rad.port_b, C2.port_a) annotation (Line(
      points={{438,-52},{438,-76}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(pump_algo.Sj_outter[1], C2.m_flow_in) annotation (Line(
      points={{464.024,-269.92},{486,-269.92},{486,-85.8},{450,-85.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Water_temp.y[1],Cold_water. T_in) annotation (Line(
      points={{244.6,-106},{250,-106},{250,-112},{258.4,-112},{258.4,-105.2}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(inFlow_heating.port_b, inFlow_heating1.port_a) annotation (Line(
      points={{392,64},{438,64},{438,40}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T5.T, boundary.T_in) annotation (Line(
      points={{62,28},{62,40},{82,40},{82,12.4},{73.2,12.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(inFlowTop_Storage.port_b, Storage_tank.portHex_a) annotation (Line(
      points={{52,38},{52,-25.88},{68,-25.88}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(boundary.ports[1], Storage_tank.port_a) annotation (Line(
      points={{60,10},{56,10},{56,-16},{68,-16}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(boundary1.ports[1], Storage_tank.port_b) annotation (Line(
      points={{98,20},{120,20},{120,-16},{110,-16}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Storage_tank.portHex_b, inFlowLow_storage.port_b) annotation (Line(
      points={{68,-36.8},{16,-36.8},{16,-70}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Storage_tank.portHex_b, C6.port_a) annotation (Line(
      points={{68,-36.8},{68,-76}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Storage_tank.heaPorVol[4], T5.port) annotation (Line(
      points={{89,-17.014},{76,-17.014},{76,28},{74,28}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Troom1.port, Storage_tank.heaPorSid) annotation (Line(
      points={{112,-54},{112,-34},{116,-34},{116,-12},{104,-12},{104,-16},{
          100.76,-16}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Troom1.port, Storage_tank.heaPorTop) annotation (Line(
      points={{112,-54},{112,-34},{116,-34},{116,-12},{104,-12},{104,6},{93.2,6},
          {93.2,3.24}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Solar_tank.port_a, thermostatic_valve1.port_1) annotation (Line(
      points={{184,-12},{166,-12},{166,-28},{158,-28}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(T_cold_water.port_b, Solar_tank.port_b) annotation (Line(
      points={{256,-62},{256,-12},{232,-12}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlowTop_Storage1.port_b, Solar_tank.portHex_a1) annotation (Line(
      points={{182,26},{182,6},{176,6},{176,-22.64},{184,-22.64}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.portHex_b1, C5.port_a) annotation (Line(
      points={{184,-34.4},{182,-34.4},{182,-76}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.portHex_b2, inFlow_backup1.port_a) annotation (Line(
      points={{232,-4.72},{318,-4.72},{318,-20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(inFlow_topExtra.port_b, Solar_tank.portHex_a2) annotation (Line(
      points={{362,28},{362,0},{240,0},{240,2},{232,2}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=1));
  connect(Solar_tank.heaPorVol[12], T3.port) annotation (Line(
      points={{208,-11.748},{194,-11.748},{194,23},{200,23}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Solar_tank.heaPorVol[4], T4.port) annotation (Line(
      points={{208,-13.092},{208,-12},{194,-12},{194,45},{200,45}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Troom2.port, Solar_tank.heaPorSid) annotation (Line(
      points={{230,-74},{230,-60},{240,-60},{240,-8},{224,-8},{224,-12},{221.44,
          -12}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  connect(Troom2.port, Solar_tank.heaPorTop) annotation (Line(
      points={{230,-74},{230,-60},{240,-60},{240,-8},{224,-8},{224,22},{212.8,22},
          {212.8,8.72}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.4));
  annotation (Diagram(coordinateSystem(extent={{-100,-320},{820,120}},
          preserveAspectRatio=false),graphics), Icon(coordinateSystem(
          extent={{-100,-320},{820,120}})),
    experiment(
      StopTime=3.17388e+007,
      Interval=120,
      Tolerance=1e-007,
      __Dymola_Algorithm="esdirk23a"),
    __Dymola_experimentSetupOutput(doublePrecision=true),
    Documentation(info="<html>
<p>SolisArt SolisConfort system with a radiator</p>
<p>New weather file :</p>
<ul>
<li>change weather data</li>
<li>change cold water data</li>
</ul>
</html>"));
end SolisArt_confort_79KWh_adapt;
