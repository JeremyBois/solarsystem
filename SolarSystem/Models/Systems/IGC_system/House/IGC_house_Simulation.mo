within SolarSystem.Models.Systems.IGC_system.House;
model IGC_house_Simulation
  extends Modelica.Icons.Example;
  extends IGC_house(House(nPorts=3), souInf_House(nPorts=3));

protected
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{180,180},{200,200}})));

public
  Modelica.Blocks.Math.Mean TRooHou1(
                                    f=1/3600, y(start=293.15))
    "Hourly averaged room air temperature"
    annotation (Placement(transformation(extent={{170,18},{190,38}})));
  Modelica.Blocks.Math.Mean TRooDay(y(start=293.15), f=1/3600/24)
    "Month averaged room air temperature"
    annotation (Placement(transformation(extent={{170,-24},{190,-4}})));
protected
  Buildings.Controls.Continuous.LimPID conHea(
    Td=60,
    initType=Modelica.Blocks.Types.InitPID.InitialState,
    Ti=300,
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=0.1) "Controller for heating"
    annotation (Placement(transformation(extent={{-214,-124},{-206,-116}})));
protected
  Modelica.Blocks.Math.Gain gaiHea(k=1E6) "Gain for heating"
    annotation (Placement(transformation(extent={{-200,-124},{-192,-116}})));
protected
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow preHea
    "Prescribed heat flow for heating and cooling"
    annotation (Placement(transformation(extent={{-158,-146},{-146,-134}})));
public
  Modelica.Blocks.Continuous.Integrator EHea(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{-156,-120},{-140,-104}})));
protected
  SolarSystem.Utilities.Other.Schedule TSetHea(table=[0*3600,
        19 + 273.15; 10*3600,19 + 273.15; 10*3600,16 + 273.15; 18*3600,16 + 273.15;
        18*3600,19 + 273.15; 34*3600,19 + 273.15; 34*3600,16 + 273.15; 42*3600,16
         + 273.15; 42*3600,19 + 273.15; 58*3600,19 + 273.15; 58*3600,16 + 273.15;
        66*3600,16 + 273.15; 66*3600,19 + 273.15; 82*3600,19 + 273.15; 82*3600,16
         + 273.15; 90*3600,16 + 273.15; 90*3600,19 + 273.15; 106*3600,19 + 273.15;
        106*3600,16 + 273.15; 114*3600,16 + 273.15; 114*3600,19 + 273.15; 168*3600,
        19 + 273.15])
    "Heating setpoint 19-16-19�C weekdays and 19�C weekends"
    annotation (Placement(transformation(extent={{-234,-124},{-226,-116}})));
public
  Modelica.Blocks.Math.Mean PHea(f=1/3600) "Hourly averaged heating power"
    annotation (Placement(transformation(extent={{-156,-98},{-140,-82}})));
protected
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Weather(
      computeWetBulbTemperature=false, filNam=weather_file.path)
    annotation (Placement(transformation(extent={{94,-64},{68,-44}})));
protected
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{-6,-62},{10,-46}})));
public
  Modelica.Blocks.Sources.Constant InfiltrationRate_House(k=-(74 + 0.16*
        floor_area)/3600) "Air supply and infiltrations"
    annotation (Placement(transformation(extent={{-66,-18},{-54,-6}})));
protected
  Modelica.Blocks.Math.Product ToMass_House
    annotation (Placement(transformation(extent={{-40,-20},{-30,-10}})));
protected
  Buildings.Fluid.Sensors.Density density_House(redeclare package Medium =
        MediumA) "Air density inside the building"
    annotation (Placement(transformation(extent={{-4,-32},{-14,-22}})));
public
  Buildings.Fluid.Sources.MassFlowSource_T sinInf_House(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{-16,-16},{-4,-4}})));
protected
  Buildings.Fluid.FixedResistances.FixedResistanceDpM Resistance_House(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    m_flow_nominal=floor_area*1.5*1.2/3600)
    annotation (Placement(transformation(extent={{-10,6},{2,18}})));
public
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature Tinstruction
    "Room air temperature instruction"
    annotation (Placement(transformation(extent={{-198,-38},{-182,-22}})));
equation

  connect(conHea.y,gaiHea. u) annotation (Line(
      points={{-205.6,-120},{-200.8,-120}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(EHea.u,gaiHea. y) annotation (Line(
      points={{-157.6,-112},{-184,-112},{-184,-120},{-191.6,-120}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TSetHea.y[1],conHea. u_s) annotation (Line(
      points={{-225.6,-120},{-214.8,-120}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(PHea.u,gaiHea. y) annotation (Line(
      points={{-157.6,-90},{-184,-90},{-184,-120},{-191.6,-120}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiHea.y, preHea.Q_flow) annotation (Line(
      points={{-191.6,-120},{-184,-120},{-184,-140},{-158,-140}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(TRooAir.T, TRooDay.u) annotation (Line(
      points={{148,-14},{168,-14}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooHou1.u) annotation (Line(
      points={{148,-14},{160,-14},{160,28},{168,28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, conHea.u_m) annotation (Line(
      points={{148,-14},{160,-14},{160,-148},{-210,-148},{-210,-124.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(preHea.port, House.heaPorAir) annotation (Line(
      points={{-146,-140},{-80,-140},{-80,19},{30.25,19}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Weather.weaBus, House.weaBus) annotation (Line(
      points={{68,-54},{50,-54},{50,32.425},{44.425,32.425}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Weather.weaBus, VideSanitaire.weaBus) annotation (Line(
      points={{68,-54},{50,-54},{50,-78.1},{33.9,-78.1}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Weather.weaBus, Combles.weaBus) annotation (Line(
      points={{68,-54},{50,-54},{50,144},{34.005,144},{34.005,144.005}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Weather.weaBus, weaBus) annotation (Line(
      points={{68,-54},{2,-54}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(weaBus, souInf_House.weaBus) annotation (Line(
      points={{2,-54},{-74,-54},{-74,6},{-72,6},{-72,6.12},{-70,6.12}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.TDryBul, Toutside.T) annotation (Line(
      points={{2,-54},{20,-54},{20,-12},{112,-12},{112,4},{101.6,4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(InfiltrationRate_House.y, ToMass_House.u1) annotation (Line(
      points={{-53.4,-12},{-41,-12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(density_House.d, ToMass_House.u2) annotation (Line(
      points={{-14.5,-27},{-46,-27},{-46,-18},{-41,-18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ToMass_House.y, sinInf_House.m_flow_in) annotation (Line(
      points={{-29.5,-15},{-22.75,-15},{-22.75,-5.2},{-16,-5.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Resistance_House.port_b, House.ports[1]) annotation (Line(
      points={{2,12},{16,12},{16,11.5},{19.75,11.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(sinInf_House.ports[1], House.ports[2]) annotation (Line(
      points={{-4,-10},{16,-10},{16,11.5},{19.75,11.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(density_House.port, House.ports[3]) annotation (Line(
      points={{-9,-32},{16,-32},{16,11.5},{19.75,11.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(souInf_House.ports[3], Resistance_House.port_a) annotation (Line(
      points={{-58,6},{-34,6},{-34,12},{-10,12}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TSetHea.y[1], Tinstruction.T) annotation (Line(
      points={{-225.6,-120},{-222,-120},{-222,-120},{-220,-120},{-220,-30},{
          -199.6,-30}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (
experiment(
      StopTime=3.1536e+007,
      Interval=3600,
      __Dymola_Algorithm="Lsodar"), Documentation(info="<html>
<p>
This model is used for the test case 600FF of the BESTEST validation suite.
Case 600FF is a light-weight building.
The room temperature is free floating.
</p>
</html>", revisions="<html>
<ul>
<li>
October 9, 2013, by Michael Wetter:<br/>
Implemented soil properties using a record so that <code>TSol</code> and
<code>TLiq</code> are assigned.
This avoids an error when the model is checked in the pedantic mode.
</li>
<li>
July 15, 2012, by Michael Wetter:<br/>
Added reference results.
Changed implementation to make this model the base class
for all BESTEST cases.
Added computation of hourly and annual averaged room air temperature.
<li>
October 6, 2011, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-240,-160},{200,200}},
          preserveAspectRatio=false),
            graphics),
    Icon(coordinateSystem(extent={{-240,-160},{200,200}})),
    __Dymola_experimentSetupOutput(doublePrecision=true, events=false));
end IGC_house_Simulation;
