within SolarSystem.Models.Systems.IGC_system.House;
model IGC_characteristics_passive
  "Contains all material to characterize the building and all constants"
  extends Modelica.Icons.MaterialProperty;

  // Air Medium
protected
package MediumA =
      Buildings.Media.Air "Medium model";

  // Walls azimut
  parameter Modelica.SIunits.Angle E_=
    SolarSystem.Data.Parameter.Construction.Azimuth.E "Azimuth for east walls";
  parameter Modelica.SIunits.Angle O_=
    SolarSystem.Data.Parameter.Construction.Azimuth.W "Azimuth for west walls";
  parameter Modelica.SIunits.Angle N_=
    SolarSystem.Data.Parameter.Construction.Azimuth.N "Azimuth for north walls";
  parameter Modelica.SIunits.Angle S_=
    SolarSystem.Data.Parameter.Construction.Azimuth.S "Azimuth for south walls";

  // Walls inclinaison
  parameter Modelica.SIunits.Angle C_=
    SolarSystem.Data.Parameter.Construction.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    SolarSystem.Data.Parameter.Construction.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle W_=
    SolarSystem.Data.Parameter.Construction.Tilt.Wall "Tilt for wall";
  parameter Modelica.SIunits.Angle V_=
    SolarSystem.Data.Parameter.Construction.Tilt.Ceiling
    "Tilt for roof with velux use an horizontal inclinaison because other are not implemented";

  // House main characteristics
public
  parameter Modelica.SIunits.Area floor_area=98.4 "House floor area";
  parameter Modelica.SIunits.Length house_height=2.5 "House average height";
  parameter Modelica.SIunits.Length underfloor_height=1.15
    "Underfloor space average height";

  // Convective and radiative coefficients
  parameter Modelica.SIunits.CoefficientOfHeatTransfer h_int[3]={7.7, 7.7, 7.7}
    "Interior h coefficient: room, ceiling, underfloor";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer h_ext[3]={25, 25, 25}
    "Exterior h coefficient: room, ceiling, underfloor";

protected
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Wall(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=4,
    absSol_a=0.6,
    absSol_b=0.6,
    material={SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Enduit(x=
        0.01),
        SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Optibric(x=
        0.2),laine_verre,
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Platre(x=0.01)})
                 "Wall layers"
    annotation (Placement(transformation(extent={{112,72},{132,92}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Roof(        nLay=
       2,
    absSol_a=0.6,
    material={laineRoche_souffle,
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Platre(0.01)})
                 "Roof layers"
    annotation (Placement(transformation(extent={{130,92},{150,112}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Floor(nLay=4,
    absSol_a=0.6,
    absSol_b=0.6,
    material={
        SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Chape_beton(
        x=0.05),polyurethane,
        SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Hourdis_polystyrene(
        x=0.12),
        SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Chape_beton(
        x=0.05)}) "Floor layer"
    annotation (Placement(transformation(extent={{130,52},{150,72}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Partition(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=3,
    material={SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Gypsum(x=0.025),
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Rval(x=1),
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Gypsum(x=0.025)})
    "Wall layers"
    annotation (Placement(transformation(extent={{156,60},{176,80}})));
protected
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Roof_combles(nLay=1, material={
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Tuile_argile(x=0.04)},
    absSol_a=0.7,
    absSol_b=0.7) "Roof layers"
    annotation (Placement(transformation(extent={{130,120},{150,140}})));
protected
  Modelica.Blocks.Math.Add3 Convective(
    k1=0.7/floor_area,
    k2=0.42,
    k3=0.8)                            annotation (Placement(transformation(extent={{-180,70},
            {-160,90}})));
  Modelica.Blocks.Math.Add3 Radiative(
    k1=0.3/floor_area,
    k2=0.58,
    k3=0.2)                            annotation (Placement(transformation(extent={{-180,
            110},{-160,130}})));
protected
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Wall_VS(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=1,
    material={SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Beton(x=0.2)},
    absSol_a=0.6,
    absSol_b=0.6) "Wall layers"
    annotation (Placement(transformation(extent={{130,26},{150,46}})));
public
  replaceable parameter
              Data.Parameter.City_Datas.Biarritz weather_file constrainedby
    Data.Parameter.City_Datas.City_Data
    annotation (Placement(transformation(extent={{118,152},{138,172}})));
protected
  Data.Parameter.Soil soil_bordeaux(
    k=0.52,
    c=180,
    d=2050,
    x=4)    annotation (Placement(transformation(extent={{130,0},{150,20}})));
protected
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Linear_losses(
                                                                       nLay=1, material=
       {SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Linear_bridge(x=1)})
    "Linear thermal losses"
    annotation (Placement(transformation(extent={{156,84},{176,104}})));
public
  Modelica.Blocks.Routing.Multiplex3 Internal_gains
    annotation (Placement(transformation(extent={{-134,92},{-118,108}})));
public
  Modelica.Blocks.Continuous.Integrator EInternal(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{-88,138},{-72,154}})));
public
  Modelica.Blocks.Math.Sum sum_gains(nin=3, k={floor_area,floor_area,floor_area})
    annotation (__Dymola_tag={"Power", "Internal"}, Placement(transformation(extent={{-110,
            114},{-98,126}})));
protected
  Modelica.Blocks.Sources.RealExpression Latent(y=0)
    annotation (Placement(transformation(extent={{-180,134},{-162,150}})));
public
  Modelica.Blocks.Sources.CombiTimeTable schedules(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    table=[0,390,0,1.14,1,1,1; 3600,390,0,1.14,1,1,1; 7200,390,0,1.14,1,1,1;
        10800,390,0,1.14,1,1,1; 14400,390,0,1.14,1,1,1; 18000,390,0,1.14,1,1,1;
        21600,390,0,5.7,1,1,1; 25200,390,1.4,5.7,0,0,0; 28800,390,1.4,5.7,0,1,
        0.7; 32400,390,0,5.7,0,1,0.7; 36000,0,0,1.14,0,1,0.7; 39600,0,0,1.14,0,
        1,0.7; 43200,0,0,1.14,0,1,0.7; 46800,0,0,1.14,0,0.7,1; 50400,0,0,1.14,0,
        0.7,1; 54000,0,0,1.14,0,0.7,1; 57600,0,0,1.14,0,0.7,1; 61200,0,0,1.14,0,
        0.7,1; 64800,390,0,5.7,0,0,0; 68400,390,1.4,5.7,0,0,0; 72000,390,1.4,
        5.7,0,0,0; 75600,390,1.4,5.7,0,0,0; 79200,390,0,1.14,1,1,1; 82800,390,0,
        1.14,1,1,1; 86400,390,0,1.14,1,1,1; 90000,390,0,1.14,1,1,1; 93600,390,0,
        1.14,1,1,1; 97200,390,0,1.14,1,1,1; 100800,390,0,1.14,1,1,1; 104400,390,
        0,1.14,1,1,1; 108000,390,0,5.7,1,1,1; 111600,390,1.4,5.7,0,0,0; 115200,
        390,1.4,5.7,0,1,0.7; 118800,390,0,5.7,0,1,0.7; 122400,0,0,1.14,0,1,0.7;
        126000,0,0,1.14,0,1,0.7; 129600,0,0,1.14,0,1,0.7; 133200,0,0,1.14,0,0.7,
        1; 136800,0,0,1.14,0,0.7,1; 140400,0,0,1.14,0,0.7,1; 144000,0,0,1.14,0,
        0.7,1; 147600,0,0,1.14,0,0.7,1; 151200,390,0,5.7,0,0,0; 154800,390,1.4,
        5.7,0,0,0; 158400,390,1.4,5.7,0,0,0; 162000,390,1.4,5.7,0,0,0; 165600,
        390,0,1.14,1,1,1; 169200,390,0,1.14,1,1,1; 172800,390,0,1.14,1,1,1;
        176400,390,0,1.14,1,1,1; 180000,390,0,1.14,1,1,1; 183600,390,0,1.14,1,1,
        1; 187200,390,0,1.14,1,1,1; 190800,390,0,1.14,1,1,1; 194400,390,0,5.7,1,
        1,1; 198000,390,1.4,5.7,0,0,0; 201600,390,1.4,5.7,0,1,0.7; 205200,390,0,
        5.7,0,1,0.7; 208800,0,0,1.14,0,1,0.7; 212400,0,0,1.14,0,1,0.7; 216000,0,
        0,1.14,0,1,0.7; 219600,0,0,1.14,0,0.7,1; 223200,390,0,5.7,0,0.7,1;
        226800,390,0,5.7,0,0.7,1; 230400,390,0,5.7,0,0.7,1; 234000,390,0,5.7,0,
        0.7,1; 237600,390,0,5.7,0,0,0; 241200,390,1.4,5.7,0,0,0; 244800,390,1.4,
        5.7,0,0,0; 248400,390,1.4,5.7,0,0,0; 252000,390,0,1.14,1,1,1; 255600,
        390,0,1.14,1,1,1; 259200,390,0,1.14,1,1,1; 262800,390,0,1.14,1,1,1;
        266400,390,0,1.14,1,1,1; 270000,390,0,1.14,1,1,1; 273600,390,0,1.14,1,1,
        1; 277200,390,0,1.14,1,1,1; 280800,390,0,5.7,1,1,1; 284400,390,1.4,5.7,
        0,0,0; 288000,390,1.4,5.7,0,1,0.7; 291600,390,0,5.7,0,1,0.7; 295200,0,0,
        1.14,0,1,0.7; 298800,0,0,1.14,0,1,0.7; 302400,0,0,1.14,0,1,0.7; 306000,
        0,0,1.14,0,0.7,1; 309600,0,0,1.14,0,0.7,1; 313200,0,0,1.14,0,0.7,1;
        316800,0,0,1.14,0,0.7,1; 320400,0,0,1.14,0,0.7,1; 324000,390,0,5.7,0,0,
        0; 327600,390,1.4,5.7,0,0,0; 331200,390,1.4,5.7,0,0,0; 334800,390,1.4,
        5.7,0,0,0; 338400,390,0,1.14,1,1,1; 342000,390,0,1.14,1,1,1; 345600,390,
        0,1.14,1,1,1; 349200,390,0,1.14,1,1,1; 352800,390,0,1.14,1,1,1; 356400,
        390,0,1.14,1,1,1; 360000,390,0,1.14,1,1,1; 363600,390,0,1.14,1,1,1;
        367200,390,0,5.7,1,1,1; 370800,390,1.4,5.7,0,0,0; 374400,390,1.4,5.7,0,
        1,0.7; 378000,390,0,5.7,0,1,0.7; 381600,0,0,1.14,0,1,0.7; 385200,0,0,
        1.14,0,1,0.7; 388800,0,0,1.14,0,1,0.7; 392400,0,0,1.14,0,0.7,1; 396000,
        0,0,1.14,0,0.7,1; 399600,0,0,1.14,0,0.7,1; 403200,0,0,1.14,0,0.7,1;
        406800,0,0,1.14,0,0.7,1; 410400,390,0,5.7,0,0,0; 414000,390,1.4,5.7,0,0,
        0; 417600,390,1.4,5.7,0,0,0; 421200,390,1.4,5.7,0,0,0; 424800,390,0,
        1.14,1,1,1; 428400,390,0,1.14,1,1,1; 432000,390,0,1.14,1,1,1; 435600,
        390,0,1.14,1,1,1; 439200,390,0,1.14,1,1,1; 442800,390,0,1.14,1,1,1;
        446400,390,0,1.14,1,1,1; 450000,390,0,1.14,1,1,1; 453600,390,1.4,5.7,1,
        1,1; 457200,390,1.4,5.7,0,0,0; 460800,390,0.7,5.7,0,1,0.7; 464400,390,
        0.7,5.7,0,1,0.7; 468000,390,0.7,5.7,0,1,0.7; 471600,390,0.7,5.7,0,1,0.7;
        475200,390,0.7,5.7,0,1,0.7; 478800,390,0.7,5.7,0,0.7,1; 482400,390,0.7,
        5.7,0,0.7,1; 486000,390,0.7,5.7,0,0.7,1; 489600,390,0.7,5.7,0,0.7,1;
        493200,390,0.7,5.7,0,0.7,1; 496800,390,0.7,5.7,0,0,0; 500400,390,1.4,
        5.7,0,0,0; 504000,390,1.4,5.7,0,0,0; 507600,390,1.4,5.7,0,0,0; 511200,
        390,0,1.14,1,1,1; 514800,390,0,1.14,1,1,1; 518400,390,0,1.14,1,1,1;
        522000,390,0,1.14,1,1,1; 525600,390,0,1.14,1,1,1; 529200,390,0,1.14,1,1,
        1; 532800,390,0,1.14,1,1,1; 536400,390,0,1.14,1,1,1; 540000,390,1.4,5.7,
        1,1,1; 543600,390,1.4,5.7,0,0,0; 547200,390,0.7,5.7,0,1,0.7; 550800,390,
        0.7,5.7,0,1,0.7; 554400,390,0.7,5.7,0,1,0.7; 558000,390,0.7,5.7,0,1,0.7;
        561600,390,0.7,5.7,0,1,0.7; 565200,390,0.7,5.7,0,0.7,1; 568800,390,0.7,
        5.7,0,0.7,1; 572400,390,0.7,5.7,0,0.7,1; 576000,390,0.7,5.7,0,0.7,1;
        579600,390,0.7,5.7,0,0.7,1; 583200,390,0.7,5.7,0,0,0; 586800,390,1.4,
        5.7,0,0,0; 590400,390,1.4,5.7,0,0,0; 594000,390,1.4,5.7,0,0,0; 597600,
        390,0,1.14,1,1,1; 601200,390,0,1.14,1,1,1; 604800,390,0,1.14,1,1,1])
    "1 = Occupation, 2 = lumi�res, 3 = equipements, 4=occultation hiver, 5=occultation ete est, 6=occultation ete ouest"
    annotation (Placement(transformation(extent={{-240,80},{-220,100}})));
protected
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Velux_wall(
    absSol_a=0.6,
    material={
        SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Laine_verre(
        x=0.16),SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Platre(
        0.01)},
    nLay=2) "Velux layers"
    annotation (Placement(transformation(extent={{100,120},{120,140}})));

public
  Buildings.HeatTransfer.Data.GlazingSystems.Generic GlaSys_Argon_PVC(
    gas={Argon},
    glass={PlaniLux,PlanithermXN},
    UFra=2.09) "Windows with PVC frame"
    annotation (Placement(transformation(extent={{90,84},{110,104}})));
  Buildings.HeatTransfer.Data.GlazingSystems.Generic          GlaSys_Velux(
    UFra=2.17,
    glass={VerreExt,VerreInt},
    gas={Air})
    annotation (Placement(transformation(extent={{90,8},{110,28}})));

  Modelica.Blocks.Interfaces.RealOutput integrator_Internal(unit="J")
    annotation (__Dymola_tag={"Energy", "Internal"}, Placement(transformation(extent={{-68,136},
            {-48,156}})));
  parameter Buildings.HeatTransfer.Data.Gases.Argon Argon(x=0.016)
    annotation (Placement(transformation(extent={{196,30},{216,50}})));
  parameter Data.Parameter.Construction.Layers.IGC_passive.Verre_vitrage
    PlaniLux(
    k=1,
    tauIR=0,
    absIR_b=0.837,
    tauSol={0.849},
    rhoSol_a={0.076},
    rhoSol_b={0.076},
    absIR_a=0.837,
    x=0.004)       "PLANILUX 4mm.SGG (exterior glass surface)"
    annotation (Placement(transformation(extent={{196,52},{216,72}})));
  parameter Data.Parameter.Construction.Layers.IGC_passive.Verre_vitrage PlanithermXN(
    k=1,
    tauIR=0,
    tauSol={0.5614},
    rhoSol_a={0.293},
    rhoSol_b={0.261},
    absIR_a=0.047,
    absIR_b=0.840,
    x(displayUnit="m") = 0.004)
                   "PLANITHERM XN 4mm.SGG (interior glass surface)"
    annotation (Placement(transformation(extent={{196,6},{216,26}})));
  parameter Buildings.HeatTransfer.Data.Gases.Air Air(x=0.016)
    annotation (Placement(transformation(extent={{224,30},{244,50}})));
  parameter Data.Parameter.Construction.Layers.IGC_passive.Verre_vitrage VerreExt(
    tauIR=0,
    k=0.577,
    tauSol={0.207},
    rhoSol_a={0.164},
    rhoSol_b={0.444},
    absIR_a=0.845,
    absIR_b=0.845,
    x=0.007)       "Exterior glass surface for velux"
    annotation (Placement(transformation(extent={{224,52},{244,72}})));
  parameter Data.Parameter.Construction.Layers.IGC_passive.Verre_vitrage VerreInt(
    k=1,
    tauIR=0,
    tauSol={0.250},
    rhoSol_a={0.593},
    rhoSol_b={0.474},
    absIR_a=0.013,
    absIR_b=0.88,
    x=0.004)      "Interior glass surface for Velux (Cool-Lite Xtrem 68-28)"
    annotation (Placement(transformation(extent={{224,6},{244,26}})));
public
  Buildings.HeatTransfer.Data.GlazingSystems.Generic GlaSys_Eq_south(
    gas={Argon},
    glass={PlaniLux,PlanithermXN},
    UFra=3.396482)
    "Windows with PVC/ALU frame combinaison of both window on south wall"
    annotation (Placement(transformation(extent={{90,60},{110,80}})));
  Data.Parameter.Construction.Layers.IGC_passive.Laine_verre laine_verre(x=0.14)
    annotation (Placement(transformation(extent={{216,156},{236,176}})));
  Data.Parameter.Construction.Layers.IGC_passive.LaineRoche_souffle
    laineRoche_souffle(x=0.365)
    annotation (Placement(transformation(extent={{216,180},{236,200}})));
  Data.Parameter.Construction.Layers.IGC_passive.Polyurethane polyurethane(x=0.1)
    annotation (Placement(transformation(extent={{216,132},{236,152}})));
public
  Buildings.HeatTransfer.Data.GlazingSystems.Generic GlaSys_Eq_east(
    gas={Argon},
    glass={PlaniLux,PlanithermXN},
    UFra=3.2707317)
    "Windows with PVC/ALU frame combinaison of both window on east wall"
    annotation (Placement(transformation(extent={{90,34},{110,54}})));
equation

  connect(Internal_gains.y,sum_gains. u) annotation (Line(
      points={{-117.2,100},{-114,100},{-114,120},{-111.2,120}},
      color={0,0,127}));
  connect(sum_gains.y,EInternal. u) annotation (Line(
      points={{-97.4,120},{-94,120},{-94,146},{-89.6,146}},
      color={0,0,127}));
  connect(Latent.y, Internal_gains.u3[1]) annotation (Line(
      points={{-161.1,142},{-140,142},{-140,94.4},{-135.6,94.4}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(Radiative.y, Internal_gains.u1[1]) annotation (Line(
      points={{-159,120},{-152,120},{-152,105.6},{-135.6,105.6}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(Convective.y, Internal_gains.u2[1]) annotation (Line(
      points={{-159,80},{-146,80},{-146,100},{-135.6,100}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(schedules.y[1], Radiative.u1) annotation (Line(
      points={{-219,90},{-200,90},{-200,128},{-182,128}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(schedules.y[1], Convective.u1) annotation (Line(
      points={{-219,90},{-200,90},{-200,88},{-182,88}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(schedules.y[2], Convective.u2) annotation (Line(
      points={{-219,90},{-206,90},{-206,80},{-182,80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(schedules.y[2], Radiative.u2) annotation (Line(
      points={{-219,90},{-206,90},{-206,120},{-182,120}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(schedules.y[3], Convective.u3) annotation (Line(
      points={{-219,90},{-214,90},{-214,72},{-182,72}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(schedules.y[3], Radiative.u3) annotation (Line(
      points={{-219,90},{-214,90},{-214,112},{-182,112}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(EInternal.y, integrator_Internal) annotation (Line(
      points={{-71.2,146},{-58,146}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(extent={{-240,-40},{260,200}},preserveAspectRatio=false)),
                                                                     Icon(
        coordinateSystem(extent={{-240,-40},{260,200}})),
    experiment(StopTime=604800, Interval=1800),
    __Dymola_experimentSetupOutput,
    Documentation(info="<html>
<h4><span style=\"color: #008000\">Modification 2015-10-06:</span></h4>
<ul>
<li>Switch radiative and convectives connectors to internals gains (before they are reversed)</li>
</ul>
<h4><span style=\"color: #008000\">Modification 2015-11-26:</span></h4>
<p>Changement de source pour les apports internes:</p>
<ul>
<li>Une seule table sans interpolation (maintien de la valeur durant chaque tranches horaires)</li>
<p>Plus rapide</p>
<p>Moins de m&eacute;moire</p>
<p>Sc&eacute;nario g&eacute;n&eacute;r&eacute;s automatiquement</p>
<li>Sc&eacute;narios RT 2012 (semaine, Mercredi, Weekend)</li>
<li>R&eacute;partition en radiatif et convectif des apports</li>
</ul>
<p><br>Modifications des mat&eacute;riaux:</p>
<ul>
<li>Efisol dans le plancher: 0.05 &agrave; 0.1 m</li>
<li>Roof (vers combles) : 0.405 &agrave; 0.365 m</li>
</ul>
<p><br>Ajout de la composition des parois du puit de lumi&egrave;re.</p>
<p><br>Ajout d&rsquo;une inclinaison pour le velux de 20&deg; non possible (pas impl&eacute;ment&eacute;) donc d&eacute;finit comme horizontale.</p>
<h4><span style=\"color: #008000\">Modification 2015-11-27:</span></h4>
<p>Ajout des sc&eacute;narios pour l&rsquo;occultation mais pas trouver comment ajouter un volet ...</p>
<p><br><h4>Modification 2016-10-10:</h4></p>
<p>Modification de l&rsquo;&eacute;paisseur d&rsquo;isolant pour le puits de lumi&egrave;re (0.16m contre 0.2m avant) (Voir harmonisation).</p>
<p>Modification de la composition des fen&ecirc;tres (Voir harmonisation).</p>
<h4>Modifications 2016-10-18:</h4>
<p>Voir modification de <a href=\"SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive\">SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive</a></p>
<p><br><h4>Modification 2017-02-01:</h4></p>
<p>Correction &eacute;paisseur du verre des vitrages (r&eacute;duction d&rsquo;un facteur 10 pour correspondre &agrave; des mm pas des cm ...)</p>
</html>"));
end IGC_characteristics_passive;
