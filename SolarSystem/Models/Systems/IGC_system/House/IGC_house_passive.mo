within SolarSystem.Models.Systems.IGC_system.House;
model IGC_house_passive
  "Basic test with light-weight construction and free floating temperature"
  extends Modelica.Icons.Example;
  extends
    SolarSystem.Models.Systems.IGC_system.House.IGC_characteristics_passive(
      schedules(table=[0,390,0,1.14; 3600,390,0,1.14; 7200,390,0,1.14; 10800,
          390,0,1.14; 14400,390,0,1.14; 18000,390,0,1.14; 21600,390,0,5.7;
          25200,390,1.4,5.7; 28800,390,1.4,5.7; 32400,390,0,5.7; 36000,0,0,1.14;
          39600,0,0,1.14; 43200,0,0,1.14; 46800,0,0,1.14; 50400,0,0,1.14; 54000,
          0,0,1.14; 57600,0,0,1.14; 61200,0,0,1.14; 64800,390,0,5.7; 68400,390,
          1.4,5.7; 72000,390,1.4,5.7; 75600,390,1.4,5.7; 79200,390,0,1.14;
          82800,390,0,1.14; 86400,390,0,1.14; 90000,390,0,1.14; 93600,390,0,
          1.14; 97200,390,0,1.14; 100800,390,0,1.14; 104400,390,0,1.14; 108000,
          390,0,5.7; 111600,390,1.4,5.7; 115200,390,1.4,5.7; 118800,390,0,5.7;
          122400,0,0,1.14; 126000,0,0,1.14; 129600,0,0,1.14; 133200,0,0,1.14;
          136800,0,0,1.14; 140400,0,0,1.14; 144000,0,0,1.14; 147600,0,0,1.14;
          151200,390,0,5.7; 154800,390,1.4,5.7; 158400,390,1.4,5.7; 162000,390,
          1.4,5.7; 165600,390,0,1.14; 169200,390,0,1.14; 172800,390,0,1.14;
          176400,390,0,1.14; 180000,390,0,1.14; 183600,390,0,1.14; 187200,390,0,
          1.14; 190800,390,0,1.14; 194400,390,0,5.7; 198000,390,1.4,5.7; 201600,
          390,1.4,5.7; 205200,390,0,5.7; 208800,0,0,1.14; 212400,0,0,1.14;
          216000,0,0,1.14; 219600,0,0,1.14; 223200,390,0,5.7; 226800,390,0,5.7;
          230400,390,0,5.7; 234000,390,0,5.7; 237600,390,0,5.7; 241200,390,1.4,
          5.7; 244800,390,1.4,5.7; 248400,390,1.4,5.7; 252000,390,0,1.14;
          255600,390,0,1.14; 259200,390,0,1.14; 262800,390,0,1.14; 266400,390,0,
          1.14; 270000,390,0,1.14; 273600,390,0,1.14; 277200,390,0,1.14; 280800,
          390,0,5.7; 284400,390,1.4,5.7; 288000,390,1.4,5.7; 291600,390,0,5.7;
          295200,0,0,1.14; 298800,0,0,1.14; 302400,0,0,1.14; 306000,0,0,1.14;
          309600,0,0,1.14; 313200,0,0,1.14; 316800,0,0,1.14; 320400,0,0,1.14;
          324000,390,0,5.7; 327600,390,1.4,5.7; 331200,390,1.4,5.7; 334800,390,
          1.4,5.7; 338400,390,0,1.14; 342000,390,0,1.14; 345600,390,0,1.14;
          349200,390,0,1.14; 352800,390,0,1.14; 356400,390,0,1.14; 360000,390,0,
          1.14; 363600,390,0,1.14; 367200,390,0,5.7; 370800,390,1.4,5.7; 374400,
          390,1.4,5.7; 378000,390,0,5.7; 381600,0,0,1.14; 385200,0,0,1.14;
          388800,0,0,1.14; 392400,0,0,1.14; 396000,0,0,1.14; 399600,0,0,1.14;
          403200,0,0,1.14; 406800,0,0,1.14; 410400,390,0,5.7; 414000,390,1.4,
          5.7; 417600,390,1.4,5.7; 421200,390,1.4,5.7; 424800,390,0,1.14;
          428400,390,0,1.14; 432000,390,0,1.14; 435600,390,0,1.14; 439200,390,0,
          1.14; 442800,390,0,1.14; 446400,390,0,1.14; 450000,390,0,1.14; 453600,
          390,1.4,5.7; 457200,390,1.4,5.7; 460800,390,0.7,5.7; 464400,390,0.7,
          5.7; 468000,390,0.7,5.7; 471600,390,0.7,5.7; 475200,390,0.7,5.7;
          478800,390,0.7,5.7; 482400,390,0.7,5.7; 486000,390,0.7,5.7; 489600,
          390,0.7,5.7; 493200,390,0.7,5.7; 496800,390,0.7,5.7; 500400,390,1.4,
          5.7; 504000,390,1.4,5.7; 507600,390,1.4,5.7; 511200,390,0,1.14;
          514800,390,0,1.14; 518400,390,0,1.14; 522000,390,0,1.14; 525600,390,0,
          1.14; 529200,390,0,1.14; 532800,390,0,1.14; 536400,390,0,1.14; 540000,
          390,1.4,5.7; 543600,390,1.4,5.7; 547200,390,0.7,5.7; 550800,390,0.7,
          5.7; 554400,390,0.7,5.7; 558000,390,0.7,5.7; 561600,390,0.7,5.7;
          565200,390,0.7,5.7; 568800,390,0.7,5.7; 572400,390,0.7,5.7; 576000,
          390,0.7,5.7; 579600,390,0.7,5.7; 583200,390,0.7,5.7; 586800,390,1.4,
          5.7; 590400,390,1.4,5.7; 594000,390,1.4,5.7; 597600,390,0,1.14;
          601200,390,0,1.14; 604800,390,0,1.14]));

public
  Buildings.Rooms.MixedAir House(
    redeclare package Medium = MediumA,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    nSurBou=0,
    nConExt=0,
    hRoo=house_height,
    nConPar=1,
    hIntFixed=h_int[1],
    datConPar(
      layers={Partition},
      A={58.135},
      til={W_}),
    AFlo=floor_area,
    hExtFixed=h_ext[1],
    lat=weather_file.lattitude,
    nConBou=4,
    nConExtWin=5,
    datConExtWin(
      layers={Wall,Wall,Wall,Wall,Velux_wall},
      azi={N_,O_,E_,S_,E_},
      fFra={0.2,0.1,0.164,0.176106,0.3},
      til={W_,W_,W_,W_,V_},
      glaSys={GlaSys_Argon_PVC, GlaSys_Argon_PVC, GlaSys_Eq_east, GlaSys_Eq_south,GlaSys_Velux},
      A={26.4,26.4,26.4,26.4,1.2*1.2},
      hWin={0.95,1.35*2,sqrt(5.375),sqrt(6.779999),1.18},
      wWin={0.6,1.2,sqrt(5.375),sqrt(6.779999),1.14}),
    datConBou(
      layers={Roof,Floor,Linear_losses,Velux_wall},
      A={floor_area - 1.2*1.2,floor_area,14,8.6},
      til={C_,F_,W_,W_}),
    T_start=293.15)
    "Room model. Roof and Velux connected to <Combles> and floor connected to <VideSanitaire>. Substract velux surface from ceilling."
    annotation (Placement(transformation(extent={{18,4},{48,34}})));

protected
  Buildings.Fluid.Sources.Outside souInf_House(redeclare package Medium =
        MediumA, nPorts=2) "Source model for air infiltration"
    annotation (Placement(transformation(extent={{-70,0},{-58,12}})));

public
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TRooAir
    "Room air temperature"
    annotation (Placement(transformation(extent={{132,-22},{148,-6}})));
public
  SolarSystem.House.MixedAir_simple Combles(
    hIntFixed=h_int[2],
    hRoo=1,
    redeclare package Medium = MediumA,
    nConExt=5,
    nConPar=0,
    AFlo=floor_area,
    hExtFixed=h_ext[2],
    lat=weather_file.lattitude,
    nPorts=3,
    nConBou=1,
    nSurBou=2,
    surBou(A={floor_area,8.6}, til={F_,W_}),
    datConExt(
      each layers=Roof_combles,
      A={49.2082,23.21,23.21,23.21,23.21},
      til={C_,W_,W_,W_,W_},
      azi={E_,O_,N_,S_,E_}),
    datConBou(
      layers={Linear_losses},
      A={12.6475},
      til={W_}),
    T_start=276.85)
    "Divide roof surface into five surfaces (4 walls and one ceilling). Assume room height = 1m"
    annotation (Placement(transformation(extent={{-2,108},{36,146}})));
  SolarSystem.House.MixedAir_simple VideSanitaire(
    hIntFixed=h_int[3],
    nConPar=0,
    hRoo=underfloor_height,
    nConExt=0,
    intConMod=Buildings.HeatTransfer.Types.InteriorConvection.Fixed,
    redeclare package Medium = MediumA,
    AFlo=floor_area,
    hExtFixed=h_ext[3],
    lat=weather_file.lattitude,
    nPorts=3,
    nSurBou=2,
    surBou(A={floor_area,floor_area}, til={C_,F_}),
    nConBou=2,
    datConBou(
      layers={Wall_VS,Linear_losses},
      A={48.576,7.6342},
      til={W_,W_}),
    T_start=291.15)
    "One wall for all walls, ceiling and floor conduction compute outside this room, linear losses added and connected to Text"
    annotation (Placement(transformation(extent={{-4,-116},{36,-76}})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature TSoil
    "Boundary condition for construction" annotation (Placement(transformation(
        extent={{0,0},{-8,8}},
        rotation=0,
        origin={48,-140})));
protected
  Buildings.HeatTransfer.Conduction.SingleLayer soi1(
    steadyStateInitial=true,
    A=67.5,
    material=soil_bordeaux,
    T_a_start=283.15,
    T_b_start=283.75) "2m deep soil (per definition on p.4 of ASHRAE 140-2007)"
    annotation (Placement(transformation(
        extent={{5,-5},{-3,3}},
        rotation=-90,
        origin={13,-129})));
protected
  Modelica.Blocks.Sources.CombiTimeTable Ground_temp(extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
      table=weather_file.ground)
    "Cold water temperature according to month of the year" annotation (
      Placement(transformation(
        extent={{4,-4},{-4,4}},
        rotation=0,
        origin={56,-136})));
protected
  Buildings.Fluid.Sources.MassFlowSource_T sinInf_Combles(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{-20,122},{-8,134}})));
public
  Modelica.Blocks.Sources.Constant InfiltrationRate_Combles(k=-5*Combles.hRoo*
        floor_area/3600)
    annotation (Placement(transformation(extent={{22,154},{10,166}})));
protected
  Buildings.Fluid.Sensors.Density density_Combles(redeclare package Medium =
        MediumA) "Air density inside the building"
    annotation (Placement(transformation(extent={{-8,108},{-18,118}})));
protected
  Buildings.Fluid.FixedResistances.FixedResistanceDpM Resistance_Combles(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    m_flow_nominal=3*Combles.hRoo*floor_area/3600,
    use_dh=false)
    annotation (Placement(transformation(extent={{-20,148},{-8,160}})));
  Buildings.Fluid.Sources.MassFlowSource_T sinInf_VS(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{-20,-108},{-8,-96}})));
public
  Modelica.Blocks.Sources.Constant InfiltrationRate_VS(k=-0.25*VideSanitaire.hRoo
        *floor_area/3600)
    annotation (Placement(transformation(extent={{22,-68},{10,-56}})));
protected
  Buildings.Fluid.Sensors.Density density_VS(redeclare package Medium = MediumA)
    "Air density inside the building"
    annotation (Placement(transformation(extent={{-10,-126},{-20,-116}})));
protected
  Buildings.Fluid.FixedResistances.FixedResistanceDpM Resistance_VS(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    use_dh=false,
    m_flow_nominal=0.5*Combles.hRoo*floor_area/3600)
    annotation (Placement(transformation(extent={{-22,-86},{-10,-74}})));
protected
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature Toutside
    "Boundary condition for construction" annotation (Placement(transformation(
        extent={{0,0},{-16,16}},
        rotation=0,
        origin={100,-4})));
public
  Modelica.Blocks.Math.Product to_mass_flowRate_Combles
    "Volumic to mass flow rate"                                                     annotation (Placement(
        transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={-34,134})));
  Modelica.Blocks.Math.Product to_mass_flowRate_VS "Volumic to mass flow rate" annotation (Placement(
        transformation(
        extent={{-4,-4},{4,4}},
        rotation=0,
        origin={-32,-98})));
equation

  connect(House.heaPorAir, TRooAir.port) annotation (Line(
      points={{32.25,19},{120,19},{120,-14},{132,-14}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));

  connect(Combles.surf_surBou[1],House. surf_conBou[1]) annotation (Line(
      points={{13.39,113.225},{13.39,62},{54,62},{54,0},{37.5,0},{37.5,6.4375}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));

  connect(density_VS.port, VideSanitaire.ports[1]) annotation (Line(
      points={{-15,-126},{-4,-126},{-4,-108.667},{1,-108.667}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Resistance_VS.port_b, VideSanitaire.ports[2]) annotation (Line(
      points={{-10,-80},{-4,-80},{-4,-106},{1,-106}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(sinInf_VS.ports[1], VideSanitaire.ports[3]) annotation (Line(
      points={{-8,-102},{-6,-102},{-6,-103.333},{1,-103.333}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(density_Combles.port, Combles.ports[1]) annotation (Line(
      points={{-13,108},{-2,108},{-2,114.967},{2.75,114.967}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Resistance_Combles.port_b, Combles.ports[2]) annotation (Line(
      points={{-8,154},{-2,154},{-2,117.5},{2.75,117.5}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(sinInf_Combles.ports[1], Combles.ports[3]) annotation (Line(
      points={{-8,128},{-4,128},{-4,120.033},{2.75,120.033}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));

  connect(souInf_House.ports[1], Resistance_VS.port_a) annotation (Line(
      points={{-58,7.2},{-58,8},{-26,8},{-26,-80},{-22,-80}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(souInf_House.ports[2], Resistance_Combles.port_a) annotation (Line(
      points={{-58,4.8},{-58,8},{-26,8},{-26,154},{-20,154}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(VideSanitaire.surf_conBou[1], soi1.port_b) annotation (Line(
      points={{22,-112.5},{22,-126},{12,-126}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(VideSanitaire.surf_surBou[2], soi1.port_b) annotation (Line(
      points={{12.2,-109.5},{12,-109.5},{12,-126}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(VideSanitaire.surf_surBou[1],House. surf_conBou[2]) annotation (Line(
      points={{12.2,-110.5},{12.2,-110},{12,-110},{12,-120},{52,-120},{52,-4},{
          37.5,-4},{37.5,6.8125}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));

  connect(Combles.surf_conBou[1], Toutside.port) annotation (Line(
      points={{22.7,111.8},{22.7,100},{64,100},{64,4},{84,4}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(VideSanitaire.surf_conBou[2], Toutside.port) annotation (Line(
      points={{22,-111.5},{22,-114},{64,-114},{64,4},{84,4}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(House.surf_conBou[3], Toutside.port) annotation (Line(
      points={{37.5,7.1875},{37.5,4},{84,4}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Ground_temp.y[1], TSoil.T) annotation (Line(
      points={{51.6,-136},{48.8,-136}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(TSoil.port, soi1.port_a) annotation (Line(
      points={{40,-136},{12,-136},{12,-134}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Internal_gains.y, House.qGai_flow) annotation (Line(
      points={{-117.2,100},{0,100},{0,25},{16.8,25}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(House.surf_conBou[4], Combles.surf_surBou[2]) annotation (Line(
      points={{37.5,7.5625},{37.5,0},{54,0},{54,62},{13.39,62},{13.39,114.175}},
      color={191,0,0},
      smooth=Smooth.None));

  connect(to_mass_flowRate_Combles.y, sinInf_Combles.m_flow_in)
    annotation (Line(points={{-29.6,134},{-24,134},{-24,132.8},{-20,132.8}}, color={0,0,127}));
  connect(InfiltrationRate_Combles.y, to_mass_flowRate_Combles.u1)
    annotation (Line(points={{9.4,160},{-16,160},{-38.8,160},{-38.8,136.4}}, color={0,0,127}));
  connect(density_Combles.d, to_mass_flowRate_Combles.u2)
    annotation (Line(points={{-18.5,113},{-38.8,113},{-38.8,131.6}}, color={0,0,127}));
  connect(InfiltrationRate_VS.y, to_mass_flowRate_VS.u1) annotation (Line(points={{9.4,-62},{-16,-62},{-40,
          -62},{-40,-95.6},{-36.8,-95.6}}, color={0,0,127}));
  connect(density_VS.d, to_mass_flowRate_VS.u2) annotation (Line(points={{-20.5,-121},{-40,-121},{-40,-100.4},
          {-36.8,-100.4}}, color={0,0,127}));
  connect(to_mass_flowRate_VS.y, sinInf_VS.m_flow_in)
    annotation (Line(points={{-27.6,-98},{-24,-98},{-24,-97.2},{-20,-97.2}}, color={0,0,127}));
  annotation (Documentation(info="<html>
<h4><span style=\"color: #008000\">Modifications 2015-11-26:</span></h4>
<p>Ajout des parois du puit de lumi&egrave;re (8.6m2).</p>
<p>Ajout fen&ecirc;tre de toit sur une surface horizontale car impossible sur une inclin&eacute;e (surface du mur un peu plus grande que fen&ecirc;tre pour &eacute;viter une division par 0).</p>
<p><br><b><span style=\"font-family: MS Shell Dlg 2; color: #008000;\">Modifications 2016-06-20:</span></b></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Mise &agrave; jour des ponts thermiques (valeur maison IGC sous Energy Plus de Aur&eacute;lie):</span></p>
<ul>
<li><span style=\"font-family: MS Shell Dlg 2;\">25.75 pi&egrave;ces chauff&eacute;es</span></li>
<li><span style=\"font-family: MS Shell Dlg 2;\">12.6475 combles</span></li>
<li><span style=\"font-family: MS Shell Dlg 2;\">7.6342 vide sanitaire</span></li>
</ul>
<h4>Modifications 2016-10-10:</h4>
<p><br><span style=\"font-family: MS Shell Dlg 2;\">Modification des infiltrations + ajout ventilation (Voir harmonisation).</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Mise &agrave; jour des surface vitr&eacute;es en tenant compte des nouvelles fen&ecirc;tres ---&GT; On a 8 fen&ecirc;tres (Ouest fusionn&eacute;es car similaires).</span></p>
<h4>Modifications 2016-10-18:</h4>
<p><span style=\"font-family: MS Shell Dlg 2;\">Regroupement des vitrages sur le m&ecirc;me mur pour conserver seulement une surface d&rsquo;&eacute;change de surface vitr&eacute;e et de performance &eacute;nerg&eacute;tique &eacute;quivalente.</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">On modifie donc les parois SUD et EST</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">N, O, S, S, E, E, E</span></p>
<pre>hWin={0.95,  1.35*2,  2.15,  2.15,  2.15,  1.2 ,  1.18},
wWin={0.6 ,  1.2   ,  0.9 ,  1.6 ,  2.4 ,  1.35,  1.14}),
fFra={0.2 ,  0.1   ,  0.1 ,  0.2 ,  0.2 ,  0.1 ,  0.3 },
UFra={2.09,  2.09  ,  2.09,  3.6 ,  3.6 ,  2.09,  2.17},</pre>
<p><br>devient:</p>
<p><br>N, O, E, S, E</p>
<pre>hWin={0.95,  1.35*2,  sqrt(5.375),  sqrt(6.779999),  1.18},
wWin={0.6 ,  1.2   ,  sqrt(5.375),  sqrt(6.779999),  1.14}),
fFra={0.2 ,  0.1   ,  0.164000   ,  0.176106      ,  0.3 },
UFra={2.09,  2.09  ,  3.2707317  ,  3.396482      ,  2.17},</pre>
<p><br>Le nombre de surface avec vitrage passe donc &agrave; <b>5</b> et on doit avoir <b>4</b> fen&ecirc;tre diff&eacute;rentes (PVC, EST, SUD, VELUX).</p>
</html>", revisions="<html>
<ul>
<li>
October 9, 2013, by Michael Wetter:<br/>
Implemented soil properties using a record so that <code>TSol</code> and
<code>TLiq</code> are assigned.
This avoids an error when the model is checked in the pedantic mode.
</li>
<li>
July 15, 2012, by Michael Wetter:<br/>
Added reference results.
Changed implementation to make this model the base class
for all BESTEST cases.
Added computation of hourly and annual averaged room air temperature.
<li>
October 6, 2011, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-240,-140},{260,200}},
          preserveAspectRatio=false)),
    Icon(coordinateSystem(extent={{-240,-140},{260,200}})),
    __Dymola_experimentSetupOutput(doublePrecision=true, events=false));
end IGC_house_passive;
