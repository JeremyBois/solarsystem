within SolarSystem.Models.Systems.IGC_system.House;
model IGC_house
  "Basic test with light-weight construction and free floating temperature"
  extends Modelica.Icons.Example;
  extends SolarSystem.Models.Systems.IGC_system.House.IGC_characteristics_BBC;

public
  Buildings.Rooms.MixedAir   House(
    redeclare package Medium = MediumA,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    nSurBou=0,
    nConExt=0,
    hRoo=house_height,
    nConPar=1,
    hIntFixed=h_int[1],
    datConPar(
      layers={Partition},
      A={58.135},
      til={W_}),
    datConExtWin(
      each layers=Wall,
      each A=26.4,
      each glaSys=GlaSys_Argon,
      wWin={1.36 + 1.35,0.6,0.9 + 1.6,2.4 + 1.2},
      hWin={1.2 + 1.2,0.95,2.15 + 2.15,2.15 + 1.35},
      each fFra=0.1,
      each til=W_,
      azi={NW_,NE_,SW_,SE_}),
    AFlo=floor_area,
    hExtFixed=h_ext[1],
    lat=weather_file.lattitude,
    nConExtWin=wall_with_window,
    nConBou=3,
    datConBou(
      layers={Roof,Floor,Linear_losses},
      A={floor_area,floor_area,10.64},
      til={C_,F_,W_}),
    T_start=293.15)
    "Room model. Roof connect to <Combles> and floor connect to <VideSanitaire>"
    annotation (Placement(transformation(extent={{16,4},{46,34}})));
protected
  Modelica.Blocks.Sources.Constant uSha(k=0) "No shading"
    annotation (Placement(transformation(extent={{-12,28},{-6,34}})));
  Modelica.Blocks.Routing.Replicator replicator(nout=max(1, wall_with_window))
    annotation (Placement(transformation(extent={{-2,28},{4,34}})));
protected
  Buildings.Fluid.Sources.Outside souInf_House(redeclare package Medium =
        MediumA, nPorts=2) "Source model for air infiltration"
    annotation (Placement(transformation(extent={{-70,0},{-58,12}})));

public
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TRooAir
    "Room air temperature"
    annotation (Placement(transformation(extent={{132,-22},{148,-6}})));
public
  SolarSystem.House.MixedAir_simple Combles(
    hIntFixed=h_int[2],
    hRoo=1,
    redeclare package Medium = MediumA,
    nConExt=5,
    nConPar=0,
    nSurBou=1,
    surBou(A={floor_area}, til={F_}),
    AFlo=floor_area,
    hExtFixed=h_ext[2],
    lat=weather_file.lattitude,
    nPorts=3,
    datConExt(
      each layers=Roof_combles,
      A={49.2082,23.21,23.21,23.21,23.21},
      til={C_,W_,W_,W_,W_},
      azi={SE_,NW_,NE_,SW_,SE_}),
    nConBou=1,
    datConBou(
      layers={Linear_losses},
      A={12.6475},
      til={W_}),
    T_start=276.85)
    "Divide roof surface into five surfaces (4 walls and one ceilling). Assume room height = 1m"
    annotation (Placement(transformation(extent={{-2,108},{36,146}})));
  SolarSystem.House.MixedAir_simple VideSanitaire(
    hIntFixed=h_int[3],
    nConPar=0,
    hRoo=underfloor_height,
    nConExt=0,
    intConMod=Buildings.HeatTransfer.Types.InteriorConvection.Fixed,
    redeclare package Medium = MediumA,
    AFlo=floor_area,
    hExtFixed=h_ext[3],
    lat=weather_file.lattitude,
    nPorts=3,
    nSurBou=2,
    surBou(A={floor_area,floor_area}, til={C_,F_}),
    nConBou=2,
    datConBou(
      layers={Wall_VS,Linear_losses},
      A={48.576,7.6342},
      til={W_,W_}),
    T_start=291.15)
    "One wall for all walls, ceiling and floor conduction compute outside this room, linear losses added and connected to Text"
    annotation (Placement(transformation(extent={{-4,-116},{36,-76}})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature TSoil
    "Boundary condition for construction" annotation (Placement(transformation(
        extent={{0,0},{-8,8}},
        rotation=0,
        origin={48,-140})));
protected
  Buildings.HeatTransfer.Conduction.SingleLayer soi1(
    steadyStateInitial=true,
    A=67.5,
    material=soil_bordeaux,
    T_a_start=283.15,
    T_b_start=283.75) "2m deep soil (per definition on p.4 of ASHRAE 140-2007)"
    annotation (Placement(transformation(
        extent={{5,-5},{-3,3}},
        rotation=-90,
        origin={13,-129})));
protected
  Modelica.Blocks.Sources.CombiTimeTable Ground_temp(extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
      table=weather_file.ground)
    "Cold water temperature according to month of the year" annotation (
      Placement(transformation(
        extent={{4,-4},{-4,4}},
        rotation=0,
        origin={56,-136})));
protected
  Buildings.Fluid.Sources.MassFlowSource_T sinInf_Combles(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{-20,120},{-8,132}})));
public
  Modelica.Blocks.Sources.Constant InfiltrationRate_Combles(k=-2*Combles.hRoo*
        floor_area/3600)
    annotation (Placement(transformation(extent={{-68,122},{-56,134}})));
protected
  Modelica.Blocks.Math.Product ToMass_Combles
    annotation (Placement(transformation(extent={{-46,120},{-36,130}})));
protected
  Buildings.Fluid.Sensors.Density density_Combles(redeclare package Medium =
        MediumA) "Air density inside the building"
    annotation (Placement(transformation(extent={{-10,104},{-20,114}})));
protected
  Buildings.Fluid.FixedResistances.FixedResistanceDpM Resistance_Combles(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    m_flow_nominal=3*Combles.hRoo*floor_area/3600)
    annotation (Placement(transformation(extent={{-20,142},{-8,154}})));
  Buildings.Fluid.Sources.MassFlowSource_T sinInf_VS(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{-20,-104},{-8,-92}})));
public
  Modelica.Blocks.Sources.Constant InfiltrationRate_VS(k=-0.25*VideSanitaire.hRoo
        *floor_area/3600)
    annotation (Placement(transformation(extent={{-66,-104},{-54,-92}})));
protected
  Modelica.Blocks.Math.Product ToMass_VS
    annotation (Placement(transformation(extent={{-38,-106},{-28,-96}})));
protected
  Buildings.Fluid.Sensors.Density density_VS(redeclare package Medium = MediumA)
    "Air density inside the building"
    annotation (Placement(transformation(extent={{-10,-122},{-20,-112}})));
protected
  Buildings.Fluid.FixedResistances.FixedResistanceDpM Resistance_VS(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    m_flow_nominal=0.5*Combles.hRoo*floor_area/3600)
    annotation (Placement(transformation(extent={{-22,-80},{-10,-68}})));
protected
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature Toutside
    "Boundary condition for construction" annotation (Placement(transformation(
        extent={{0,0},{-16,16}},
        rotation=0,
        origin={100,-4})));
equation
  connect(House.uSha, replicator.y) annotation (Line(
      points={{14.8,32.5},{10,32.5},{10,31},{4.3,31}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(uSha.y, replicator.u) annotation (Line(
      points={{-5.7,31},{-2.6,31}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));

  connect(House.heaPorAir, TRooAir.port) annotation (Line(
      points={{30.25,19},{120,19},{120,-14},{132,-14}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));

  connect(Combles.surf_surBou[1],House. surf_conBou[1]) annotation (Line(
      points={{13.39,113.7},{13.39,62},{52,62},{52,0},{35.5,0},{35.5,6.5}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));

  connect(ToMass_Combles.y, sinInf_Combles.m_flow_in) annotation (Line(
      points={{-35.5,125},{-30,125},{-30,130.8},{-20,130.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(density_Combles.d, ToMass_Combles.u2) annotation (Line(
      points={{-20.5,109},{-50,109},{-50,122},{-47,122}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(InfiltrationRate_Combles.y, ToMass_Combles.u1) annotation (Line(
      points={{-55.4,128},{-47,128}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(ToMass_VS.y, sinInf_VS.m_flow_in) annotation (Line(
      points={{-27.5,-101},{-24,-101},{-24,-93.2},{-20,-93.2}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(density_VS.d, ToMass_VS.u2) annotation (Line(
      points={{-20.5,-117},{-42,-117},{-42,-104},{-39,-104}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(InfiltrationRate_VS.y, ToMass_VS.u1) annotation (Line(
      points={{-53.4,-98},{-39,-98}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(density_VS.port, VideSanitaire.ports[1]) annotation (Line(
      points={{-15,-122},{-4,-122},{-4,-108.667},{1,-108.667}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Resistance_VS.port_b, VideSanitaire.ports[2]) annotation (Line(
      points={{-10,-74},{-4,-74},{-4,-106},{1,-106}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(sinInf_VS.ports[1], VideSanitaire.ports[3]) annotation (Line(
      points={{-8,-98},{-6,-98},{-6,-103.333},{1,-103.333}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(density_Combles.port, Combles.ports[1]) annotation (Line(
      points={{-15,104},{-2,104},{-2,114.967},{2.75,114.967}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Resistance_Combles.port_b, Combles.ports[2]) annotation (Line(
      points={{-8,148},{-2,148},{-2,117.5},{2.75,117.5}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(sinInf_Combles.ports[1], Combles.ports[3]) annotation (Line(
      points={{-8,126},{-4,126},{-4,120.033},{2.75,120.033}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));

  connect(souInf_House.ports[1], Resistance_VS.port_a) annotation (Line(
      points={{-58,7.2},{-58,8},{-58,8},{-56,8},{-56,8},{-26,8},{-26,-74},{-22,
          -74}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(souInf_House.ports[2], Resistance_Combles.port_a) annotation (Line(
      points={{-58,4.8},{-58,8},{-26,8},{-26,148},{-20,148}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(VideSanitaire.surf_conBou[1], soi1.port_b) annotation (Line(
      points={{22,-112.5},{22,-126},{12,-126}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(VideSanitaire.surf_surBou[2], soi1.port_b) annotation (Line(
      points={{12.2,-109.5},{12,-109.5},{12,-126}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(VideSanitaire.surf_surBou[1],House. surf_conBou[2]) annotation (Line(
      points={{12.2,-110.5},{12.2,-110},{12,-110},{12,-120},{52,-120},{52,-4},{35.5,
          -4},{35.5,7}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));

  connect(Combles.surf_conBou[1], Toutside.port) annotation (Line(
      points={{22.7,111.8},{22.7,100},{64,100},{64,4},{84,4}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(VideSanitaire.surf_conBou[2], Toutside.port) annotation (Line(
      points={{22,-111.5},{22,-114},{64,-114},{64,4},{84,4}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(House.surf_conBou[3], Toutside.port) annotation (Line(
      points={{35.5,7.5},{35.5,4},{84,4}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Ground_temp.y[1], TSoil.T) annotation (Line(
      points={{51.6,-136},{48.8,-136}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(TSoil.port, soi1.port_a) annotation (Line(
      points={{40,-136},{12,-136},{12,-134}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Internal_gains.y, House.qGai_flow) annotation (Line(
      points={{-93.2,134},{-90,134},{-90,25},{14.8,25}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  annotation (Documentation(info="<html>
<p>
This model is used for the test case 600FF of the BESTEST validation suite.
Case 600FF is a light-weight building.
The room temperature is free floating.
</p>
</html>", revisions="<html>
<ul>
<li>
October 9, 2013, by Michael Wetter:<br/>
Implemented soil properties using a record so that <code>TSol</code> and
<code>TLiq</code> are assigned.
This avoids an error when the model is checked in the pedantic mode.
</li>
<li>
July 15, 2012, by Michael Wetter:<br/>
Added reference results.
Changed implementation to make this model the base class
for all BESTEST cases.
Added computation of hourly and annual averaged room air temperature.
<li>
October 6, 2011, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-240,-140},{180,200}},
          preserveAspectRatio=false),
            graphics),
    Icon(coordinateSystem(extent={{-240,-140},{180,200}})),
    __Dymola_experimentSetupOutput(doublePrecision=true, events=false));
end IGC_house;
