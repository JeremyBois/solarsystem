within SolarSystem.Models.Systems.IGC_system.House;
model IGC_characteristics_BBC
  "Contains all material to characterize the building and all constants"
  extends Modelica.Icons.MaterialProperty;

  // Air Medium
protected
package MediumA =
      Buildings.Media.Air "Medium model";

  // Walls azimut
  parameter Modelica.SIunits.Angle SE_=
    SolarSystem.Data.Parameter.Construction.Azimuth.SE
    "Azimuth for southeast walls";
  parameter Modelica.SIunits.Angle SW_=
    SolarSystem.Data.Parameter.Construction.Azimuth.SW "Azimuth for east walls";
  parameter Modelica.SIunits.Angle NE_=
    SolarSystem.Data.Parameter.Construction.Azimuth.NE "Azimuth for west walls";
  parameter Modelica.SIunits.Angle NW_=
    SolarSystem.Data.Parameter.Construction.Azimuth.NW
    "Azimuth for north walls";

  // Walls inclinaison
  parameter Modelica.SIunits.Angle C_=
    Buildings.Types.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    Buildings.Types.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle W_=
    Buildings.Types.Tilt.Wall "Tilt for wall";

  // House main characteristics
public
  parameter Modelica.SIunits.Area floor_area=98.4 "House floor area";
  parameter Modelica.SIunits.Length house_height=2.5 "House average height";
  parameter Modelica.SIunits.Length underfloor_height=1.15
    "Underfloor space average height";
  parameter Integer wall_with_window=4
    "Number of walls inside the room with a window";

  // Convective and radiative coefficients
  parameter Modelica.SIunits.CoefficientOfHeatTransfer h_int[3]={7.6, 8, 7.6}
    "Interior h coefficient: room, ceiling, underfloor";
  parameter Modelica.SIunits.CoefficientOfHeatTransfer h_ext[3]={25, 25, 25}
    "Exterior h coefficient: room, ceiling, underfloor";

  // Internal Gains
  parameter Modelica.SIunits.Power people_gains= 97.5 "Heat gains per people";
  parameter Integer nb_people = 4 "Number of people inside the house";
  parameter Modelica.SIunits.Power lights_gains[2]={80, 59}
    "Lights gains: convective, radiative (1.4 W/m2 used)";
  parameter Modelica.SIunits.Power equipments_gains[2]={449, 112}
    "Equipments gains: convective, radiative (5.7 W/m2 used)";

protected
  Data.Parameter.Construction.Layers.IGC_BBC.Sgg_Planilux
                                 GlaSys_Argon
    annotation (Placement(transformation(extent={{80,84},{100,104}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Wall(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=4,
    absSol_a=0.6,
    absSol_b=0.6,
    material={SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Enduit(x=
        0.01),SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Optibric(
        0.2),SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Laine_verre(
        x=0.1),SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Platre(x=
        0.01)}) "Wall layers"
    annotation (Placement(transformation(extent={{110,72},{130,92}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Roof(        nLay=
       2,
    absSol_a=0.6,
    material={
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Laine_souffle(x=
        0.32),SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Platre(
        0.01)}) "Roof layers"
    annotation (Placement(transformation(extent={{130,92},{150,112}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Floor(
    nLay=4,
    absSol_a=0.6,
    absSol_b=0.6,
    material={
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Chape_beton(x=
        0.05),
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Polyurethane(x=
        0.05),SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Hourdis(x=
        0.12),
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Chape_beton(x=
        0.05)}) "Floor layer"
    annotation (Placement(transformation(extent={{130,52},{150,72}})));
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Partition(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=3,
    material={SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Gypsum(x=0.025),
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Rval(x=1),
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Gypsum(x=0.025)})
    "Wall layers"
    annotation (Placement(transformation(extent={{160,60},{180,80}})));
protected
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Roof_combles(nLay=1, material={
        SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Tuile_argile(x=0.04)},
    absSol_a=0.7,
    absSol_b=0.7) "Roof layers"
    annotation (Placement(transformation(extent={{130,120},{150,140}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule Lights_schedule(table=[0,
        0; 7*3600,0; 7*3600,1; 9*3600,1; 9*3600,0; 19*3600,0; 19*3600,1; 22*3600,
        1; 22*3600,0; 24*3600,0]) "Heating setpoint"
                                     annotation (Placement(transformation(extent={{-220,
            158},{-200,178}})));
protected
  Modelica.Blocks.Math.Add3 Convective(
    k1=0.7/floor_area,
    k2=lights_gains[1]/floor_area,
    k3=equipments_gains[1]/floor_area) annotation (Placement(transformation(extent={{-144,
            138},{-124,158}})));
  Modelica.Blocks.Math.Add3 Radiative(
    k1=0.3/floor_area,
    k2=lights_gains[2]/floor_area,
    k3=equipments_gains[2]/floor_area) annotation (Placement(transformation(extent={{-144,
            108},{-124,128}})));
protected
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Wall_VS(
    roughness_a=Buildings.HeatTransfer.Types.SurfaceRoughness.Medium,
    nLay=1,
    material={SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Beton(x=0.2)},
    absSol_a=0.6,
    absSol_b=0.6) "Wall layers"
    annotation (Placement(transformation(extent={{130,26},{150,46}})));
protected
  Data.Parameter.City_Datas.Bordeaux weather_file
    annotation (Placement(transformation(extent={{70,-38},{90,-18}})));
protected
  Modelica.Blocks.Math.Add3 People(
    k1=people_gains*nb_people,
    k2=people_gains*nb_people,
    k3=people_gains*nb_people)
    annotation (Placement(transformation(extent={{-204,98},{-184,118}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule Equipment_schedule(table=[0*
        3600,0.2; 6*3600,0.2; 6*3600,1; 10*3600,1; 10*3600,0.2; 18*3600,0.2; 18*
        3600,1; 22*3600,1; 22*3600,0.2; 30*3600,0.2; 30*3600,1; 34*3600,1; 34*3600,
        0.2; 42*3600,0.2; 42*3600,1; 46*3600,1; 46*3600,0.2; 54*3600,0.2; 54*3600,
        1; 58*3600,1; 58*3600,0.2; 66*3600,0.2; 66*3600,1; 70*3600,1; 70*3600,0.2;
        78*3600,0.2; 78*3600,1; 82*3600,1; 82*3600,0.2; 90*3600,0.2; 90*3600,1;
        94*3600,1; 94*3600,0.2; 102*3600,0.2; 102*3600,1; 106*3600,1; 106*3600,0.2;
        114*3600,0.2; 114*3600,1; 118*3600,1; 118*3600,0.2; 126*3600,0.2; 126*3600,
        1; 142*3600,1; 142*3600,0.2; 150*3600,0.2; 150*3600,1; 166*3600,1; 166*3600,
        0.2; 168*3600,0.2])
    annotation (Placement(transformation(extent={{-240,40},{-220,60}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule People_schedule_Bed(table=[0*
        3600,0.7; 6*3600,0.7; 6*3600,1; 7*3600,1; 7*3600,0; 18*3600,0; 18*3600,
        1; 20*3600,1; 20*3600,0; 23*3600,0; 23*3600,0.7; 30*3600,0.7; 30*3600,1;
        31*3600,1; 31*3600,0; 42*3600,0; 42*3600,1; 44*3600,1; 44*3600,0; 47*
        3600,0; 47*3600,0.7; 54*3600,0.7; 54*3600,1; 55*3600,1; 55*3600,0; 66*
        3600,0; 66*3600,1; 68*3600,1; 68*3600,0; 71*3600,0; 71*3600,0.7; 78*
        3600,0.7; 78*3600,1; 79*3600,1; 79*3600,0; 90*3600,0; 90*3600,1; 92*
        3600,1; 92*3600,0; 95*3600,0; 95*3600,0.7; 102*3600,0.7; 102*3600,1;
        103*3600,1; 103*3600,0; 114*3600,0; 114*3600,1; 116*3600,1; 116*3600,0;
        119*3600,0; 119*3600,0.7; 126*3600,0.7; 126*3600,1; 128*3600,1; 128*
        3600,0; 138*3600,0; 138*3600,1; 140*3600,1; 140*3600,0; 142*3600,0; 142
        *3600,1; 143*3600,1; 143*3600,0.7; 150*3600,0.7; 150*3600,1; 152*3600,1;
        152*3600,0; 162*3600,0; 162*3600,1; 164*3600,1; 164*3600,0; 166*3600,0;
        166*3600,1; 167*3600,1; 167*3600,0.7; 168*3600,0.7])
    annotation (Placement(transformation(extent={{-240,70},{-220,90}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule People_schedule_Lounge(table=[0*
        3600,0; 8*3600,0; 8*3600,1; 10*3600,1; 10*3600,0; 21*3600,0; 21*3600,1;
        22*3600,1; 22*3600,0; 32*3600,0; 32*3600,1; 34*3600,1; 34*3600,0; 45*3600,
        0; 45*3600,1; 46*3600,1; 46*3600,0; 56*3600,0; 56*3600,1; 58*3600,1; 58*
        3600,0; 69*3600,0; 69*3600,1; 70*3600,1; 70*3600,0; 80*3600,0; 80*3600,1;
        82*3600,1; 82*3600,0; 93*3600,0; 93*3600,1; 94*3600,1; 94*3600,0; 104*3600,
        0; 104*3600,1; 106*3600,1; 106*3600,0; 117*3600,0; 117*3600,1; 118*3600,
        1; 118*3600,0; 129*3600,0; 129*3600,1; 132*3600,1; 132*3600,0; 133*3600,
        0; 133*3600,1; 138*3600,1; 138*3600,0; 141*3600,0; 141*3600,1; 142*3600,
        1; 142*3600,0; 153*3600,0; 153*3600,1; 156*3600,1; 156*3600,0; 157*3600,
        0; 157*3600,1; 162*3600,1; 162*3600,0; 165*3600,0; 165*3600,1; 166*3600,
        1; 166*3600,0; 168*3600,0])
    annotation (Placement(transformation(extent={{-240,130},{-220,150}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule People_schedule_Kitchen(table=[0*
        3600,0; 7*3600,0; 7*3600,1; 8*3600,1; 8*3600,0; 20*3600,0; 20*3600,1; 21
        *3600,1; 21*3600,0; 31*3600,0; 31*3600,1; 32*3600,1; 32*3600,0; 44*3600,
        0; 44*3600,1; 45*3600,1; 45*3600,0; 55*3600,0; 55*3600,1; 56*3600,1; 56*
        3600,0; 68*3600,0; 68*3600,1; 69*3600,1; 69*3600,0; 79*3600,0; 79*3600,1;
        80*3600,1; 80*3600,0; 92*3600,0; 92*3600,1; 93*3600,1; 93*3600,0; 103*3600,
        0; 103*3600,1; 104*3600,1; 104*3600,0; 116*3600,0; 116*3600,1; 117*3600,
        1; 117*3600,0; 128*3600,0; 128*3600,1; 129*3600,1; 129*3600,0; 132*3600,
        0; 132*3600,1; 133*3600,1; 133*3600,0; 140*3600,0; 140*3600,1; 141*3600,
        1; 141*3600,0; 152*3600,0; 152*3600,1; 153*3600,1; 153*3600,0; 156*3600,
        0; 156*3600,1; 157*3600,1; 157*3600,0; 164*3600,0; 164*3600,1; 165*3600,
        1; 165*3600,0; 168*3600,0])
    annotation (Placement(transformation(extent={{-240,100},{-220,120}})));
protected
  Data.Parameter.Soil soil_bordeaux(
    x=0.5,
    k=0.52,
    c=180,
    d=2050) annotation (Placement(transformation(extent={{130,0},{150,20}})));
protected
  Buildings.HeatTransfer.Data.OpaqueConstructions.Generic Linear_losses(
                                                                       nLay=1, material=
       {SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Linear_bridge(x=1)})
    "Linear thermal losses"
    annotation (Placement(transformation(extent={{160,84},{180,104}})));
  Buildings.Rooms.Validation.BESTEST.BaseClasses.DaySchedule Clothing_schedule(table=[0*
        24*3600,1; 90*24*3600,1; 90*24*3600,0.5; 272*24*3600,0.5; 272*24*3600,1;
        364*24*3600,1])
    annotation (Placement(transformation(extent={{-200,130},{-180,150}})));
public
  Modelica.Blocks.Routing.Multiplex3 Internal_gains
    annotation (Placement(transformation(extent={{-110,126},{-94,142}})));
public
  Modelica.Blocks.Continuous.Integrator EInternal(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{-58,150},{-42,166}})));
protected
  Modelica.Blocks.Math.Sum sum_gains(nin=3, k={floor_area,floor_area,floor_area})
    annotation (Placement(transformation(extent={{-86,164},{-74,176}})));
protected
  Modelica.Blocks.Sources.RealExpression Latent(y=0)
    annotation (Placement(transformation(extent={{-144,88},{-126,104}})));
public
  Modelica.Blocks.Math.Mean PInternal(f=1/3600) "Hourly averaged heating power"
    annotation (Placement(transformation(extent={{-58,174},{-42,190}})));
equation
  connect(Lights_schedule.y[1], Convective.u2)
    annotation (Line(
      points={{-199,168},{-150,168},{-150,148},{-146,148}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Lights_schedule.y[1], Radiative.u2) annotation (Line(
      points={{-199,168},{-150,168},{-150,118},{-146,118}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(Equipment_schedule.y[1], Radiative.u3) annotation (Line(
      points={{-219,50},{-152,50},{-152,110},{-146,110}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(Equipment_schedule.y[1], Convective.u3) annotation (Line(
      points={{-219,50},{-152,50},{-152,140},{-146,140}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(People_schedule_Bed.y[1], People.u3) annotation (Line(
      points={{-219,80},{-210,80},{-210,100},{-206,100}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(People_schedule_Lounge.y[1], People.u1) annotation (Line(
      points={{-219,140},{-212,140},{-212,116},{-206,116}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(People_schedule_Kitchen.y[1], People.u2) annotation (Line(
      points={{-219,110},{-212,110},{-212,108},{-206,108}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(People.y, Radiative.u1) annotation (Line(
      points={{-183,108},{-160,108},{-160,126},{-146,126}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(People.y, Convective.u1) annotation (Line(
      points={{-183,108},{-160,108},{-160,156},{-146,156}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(Internal_gains.y,sum_gains. u) annotation (Line(
      points={{-93.2,134},{-90,134},{-90,170},{-87.2,170}},
      color={0,0,127}));
  connect(sum_gains.y,EInternal. u) annotation (Line(
      points={{-73.4,170},{-66,170},{-66,158},{-59.6,158}},
      color={0,0,127}));
  connect(sum_gains.y,PInternal. u) annotation (Line(
      points={{-73.4,170},{-66,170},{-66,182},{-59.6,182}},
      color={0,0,127}));
  connect(Latent.y, Internal_gains.u3[1]) annotation (Line(
      points={{-125.1,96},{-118,96},{-118,128.4},{-111.6,128.4}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(Radiative.y, Internal_gains.u1[1]) annotation (Line(
      points={{-123,118},{-122,118},{-122,139.6},{-111.6,139.6}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(Convective.y, Internal_gains.u2[1]) annotation (Line(
      points={{-123,148},{-120,148},{-120,134},{-111.6,134}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  annotation (Diagram(coordinateSystem(extent={{-240,-40},{180,200}},preserveAspectRatio=false), graphics),
                                                                     Icon(
        coordinateSystem(extent={{-240,-40},{180,200}})),
    experiment(StopTime=604800, Interval=1800),
    __Dymola_experimentSetupOutput,
    Documentation(info="<html>
<h4><span style=\"color:#008000\">Modification 2015-10-06:</span></h4>
<ul>
<li>Switch radiative and convectives connectors to internals gains (before they are reversed)</li>
</ul>
</html>"));
end IGC_characteristics_BBC;
