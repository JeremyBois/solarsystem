within SolarSystem.Models.Systems.IGC_system.House;
model IGC_house_Simulation_passive
  extends Modelica.Icons.Example;
  extends IGC_house_passive(
    GlaSys_Argon_PVC(
                 gas={Argon},         glass={PlaniLux,PlanithermUltra}),
                            House(nPorts=3), souInf_House(nPorts=3),
    redeclare Data.Parameter.ParametricStudy201611.Weathers.Strasbourg
                                                             weather_file,
    Roof(material={laineRoche_souffle,
          SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Platre(0.01)}),
    Floor(material={
          SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Chape_beton(
          x=0.05),polyurethane,
          SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Hourdis_polystyrene(
          x=0.12),
          SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Chape_beton(
          x=0.05)}),
    Wall(material={
          SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Enduit(x=0.01),
          SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Optibric(x=
           0.2),laine_verre,
          SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Platre(x=0.01)}));

protected
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{300,-160},{320,-140}})));

public
  Modelica.Blocks.Math.Mean TRooHou1(
                                    f=1/3600, y(start=293.15))
    "Hourly averaged room air temperature"
    annotation (Placement(transformation(extent={{170,-70},{190,-50}})));
  Modelica.Blocks.Math.Mean TRooDay(y(start=293.15), f=1/3600/24)
    "Month averaged room air temperature"
    annotation (Placement(transformation(extent={{170,-24},{190,-4}})));
protected
  Buildings.Controls.Continuous.LimPID conHea(
    Td=60,
    initType=Modelica.Blocks.Types.InitPID.InitialState,
    Ti=300,
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=0.1) "Controller for heating"
    annotation (Placement(transformation(extent={{-214,-124},{-206,-116}})));
protected
  Modelica.Blocks.Math.Gain gaiHea(k=1E6) "Gain for heating"
    annotation (Placement(transformation(extent={{-200,-124},{-192,-116}})));
protected
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow preHea
    "Prescribed heat flow for heating and cooling"
    annotation (Placement(transformation(extent={{-158,-146},{-146,-134}})));
public
  Modelica.Blocks.Continuous.Integrator EHea(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{-156,-120},{-140,-104}})));
protected
  Modelica.Blocks.Sources.CombiTimeTable
                                       TSetHea(
    table=[0,292.15; 3600,292.15; 7200,292.15; 10800,292.15; 14400,292.15;
        18000,292.15; 21600,292.15; 25200,292.15; 28800,292.15; 32400,292.15;
        36000,289.15; 39600,289.15; 43200,289.15; 46800,289.15; 50400,289.15;
        54000,289.15; 57600,289.15; 61200,289.15; 64800,292.15; 68400,292.15;
        72000,292.15; 75600,292.15; 79200,292.15; 82800,292.15; 86400,292.15;
        90000,292.15; 93600,292.15; 97200,292.15; 100800,292.15; 104400,292.15;
        108000,292.15; 111600,292.15; 115200,292.15; 118800,292.15; 122400,
        289.15; 126000,289.15; 129600,289.15; 133200,289.15; 136800,289.15;
        140400,289.15; 144000,289.15; 147600,289.15; 151200,292.15; 154800,
        292.15; 158400,292.15; 162000,292.15; 165600,292.15; 169200,292.15;
        172800,292.15; 176400,292.15; 180000,292.15; 183600,292.15; 187200,
        292.15; 190800,292.15; 194400,292.15; 198000,292.15; 201600,292.15;
        205200,292.15; 208800,289.15; 212400,289.15; 216000,289.15; 219600,
        289.15; 223200,292.15; 226800,292.15; 230400,292.15; 234000,292.15;
        237600,292.15; 241200,292.15; 244800,292.15; 248400,292.15; 252000,
        292.15; 255600,292.15; 259200,292.15; 262800,292.15; 266400,292.15;
        270000,292.15; 273600,292.15; 277200,292.15; 280800,292.15; 284400,
        292.15; 288000,292.15; 291600,292.15; 295200,289.15; 298800,289.15;
        302400,289.15; 306000,289.15; 309600,289.15; 313200,289.15; 316800,
        289.15; 320400,289.15; 324000,292.15; 327600,292.15; 331200,292.15;
        334800,292.15; 338400,292.15; 342000,292.15; 345600,292.15; 349200,
        292.15; 352800,292.15; 356400,292.15; 360000,292.15; 363600,292.15;
        367200,292.15; 370800,292.15; 374400,292.15; 378000,292.15; 381600,
        289.15; 385200,289.15; 388800,289.15; 392400,289.15; 396000,289.15;
        399600,289.15; 403200,289.15; 406800,289.15; 410400,292.15; 414000,
        292.15; 417600,292.15; 421200,292.15; 424800,292.15; 428400,292.15;
        432000,292.15; 435600,292.15; 439200,292.15; 442800,292.15; 446400,
        292.15; 450000,292.15; 453600,292.15; 457200,292.15; 460800,292.15;
        464400,292.15; 468000,292.15; 471600,292.15; 475200,292.15; 478800,
        292.15; 482400,292.15; 486000,292.15; 489600,292.15; 493200,292.15;
        496800,292.15; 500400,292.15; 504000,292.15; 507600,292.15; 511200,
        292.15; 514800,292.15; 518400,292.15; 522000,292.15; 525600,292.15;
        529200,292.15; 532800,292.15; 536400,292.15; 540000,292.15; 543600,
        292.15; 547200,292.15; 550800,292.15; 554400,292.15; 558000,292.15;
        561600,292.15; 565200,292.15; 568800,292.15; 572400,292.15; 576000,
        292.15; 579600,292.15; 583200,292.15; 586800,292.15; 590400,292.15;
        594000,292.15; 597600,292.15; 601200,292.15; 604800,292.15],
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic)
    "Heating setpoint 19-18-16�C"
    annotation (Placement(transformation(extent={{-234,-124},{-226,-116}})));
public
  Modelica.Blocks.Math.Mean PHea(f=1/3600) "Hourly averaged heating power"
    annotation (Placement(transformation(extent={{-156,-98},{-140,-82}})));
protected
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Weather(
      computeWetBulbTemperature=false, filNam=weather_file.path)
    annotation (Placement(transformation(extent={{94,-70},{68,-50}})));
protected
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{-8,-68},{8,-52}})));
public
  Modelica.Blocks.Sources.Constant InfiltrationRate_House(k=-0.4*(26.4*4 + 8.6
         + floor_area)/3600 - 90/3600)
    "walls + velux walls + plafond + Ventilation"
    annotation (Placement(transformation(extent={{-66,-18},{-54,-6}})));
protected
  Modelica.Blocks.Math.Product ToMass_House
    annotation (Placement(transformation(extent={{-40,-20},{-30,-10}})));
protected
  Buildings.Fluid.Sensors.Density density_House(redeclare package Medium =
        MediumA) "Air density inside the building"
    annotation (Placement(transformation(extent={{-4,-32},{-14,-22}})));
public
  Buildings.Fluid.Sources.MassFlowSource_T sinInf_House(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{-16,-16},{-4,-4}})));
protected
  Buildings.Fluid.FixedResistances.FixedResistanceDpM Resistance_House(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    dp_nominal=1,
    linearized=true,
    from_dp=true,
    m_flow_nominal=floor_area*1.5*1.2/3600)
    annotation (Placement(transformation(extent={{-10,6},{2,18}})));
public
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature Tinstruction
    "Room air temperature instruction"
    annotation (Placement(transformation(extent={{-198,-38},{-182,-22}})));
  parameter Data.Parameter.Construction.Layers.IGC_passive.Verre_vitrage
    PlanithermOne(
    x=0.04,
    k=1,
    rhoSol_a={0.469},
    rhoSol_b={0.412},
    tauIR=0,
    absIR_a=0.013,
    absIR_b=0.837,
    tauSol={0.465}) "PLANITHERM ONE 4mm.SGG (interior glass surface)"
    annotation (Placement(transformation(extent={{280,144},{300,164}})));
  parameter Data.Parameter.Construction.Layers.IGC_passive.Verre_vitrage PlanithermUltra(
    x=0.04,
    k=1,
    tauIR=0,
    absIR_b=0.837,
    tauSol={0.591},
    rhoSol_a={0.312},
    rhoSol_b={0.264},
    absIR_a=0.037) "PLANITHERM ONE 4mm.SGG (interior glass surface)"
    annotation (Placement(transformation(extent={{278,166},{298,186}})));
public
  Modelica.Blocks.Continuous.Integrator ConsoElectromenager(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J"))
    annotation (Placement(transformation(extent={{-140,-36},{-124,-20}})));
public
  Modelica.Blocks.Continuous.Integrator ConsoEclairage(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J"))
    annotation (Placement(transformation(extent={{-140,-8},{-124,8}})));
public
  Modelica.Blocks.Continuous.Integrator ConsoElectrique(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J"))
    annotation (Placement(transformation(extent={{-120,22},{-104,38}})));
protected
  Modelica.Blocks.Math.Gain Coef[3](k={1,floor_area,floor_area})
    "Gain for electrical internal loads"
    annotation (Placement(transformation(extent={{-188,26},{-180,34}})));
public
  Modelica.Blocks.Math.Sum sum(nin=2) annotation (__Dymola_tag={"Power",
        "Internal"}, Placement(transformation(extent={{-146,24},{-134,36}})));
public
  Modelica.Blocks.Continuous.Integrator ProdOccupants(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J"))
    annotation (Placement(transformation(extent={{-140,52},{-124,68}})));
equation

  connect(conHea.y,gaiHea. u) annotation (Line(
      points={{-205.6,-120},{-200.8,-120}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(EHea.u,gaiHea. y) annotation (Line(
      points={{-157.6,-112},{-184,-112},{-184,-120},{-191.6,-120}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TSetHea.y[1],conHea. u_s) annotation (Line(
      points={{-225.6,-120},{-214.8,-120}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(PHea.u,gaiHea. y) annotation (Line(
      points={{-157.6,-90},{-184,-90},{-184,-120},{-191.6,-120}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(gaiHea.y, preHea.Q_flow) annotation (Line(
      points={{-191.6,-120},{-184,-120},{-184,-140},{-158,-140}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(TRooAir.T, TRooDay.u) annotation (Line(
      points={{148,-14},{168,-14}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooHou1.u) annotation (Line(
      points={{148,-14},{160,-14},{160,-60},{168,-60}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, conHea.u_m) annotation (Line(
      points={{148,-14},{160,-14},{160,-148},{-210,-148},{-210,-124.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(preHea.port, House.heaPorAir) annotation (Line(
      points={{-146,-140},{-80,-140},{-80,19},{32.25,19}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Weather.weaBus, House.weaBus) annotation (Line(
      points={{68,-60},{50,-60},{50,32.425},{46.425,32.425}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Weather.weaBus, VideSanitaire.weaBus) annotation (Line(
      points={{68,-60},{50,-60},{50,-78.1},{33.9,-78.1}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Weather.weaBus, Combles.weaBus) annotation (Line(
      points={{68,-60},{50,-60},{50,144},{34.005,144},{34.005,144.005}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Weather.weaBus, weaBus) annotation (Line(
      points={{68,-60},{0,-60}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(weaBus, souInf_House.weaBus) annotation (Line(
      points={{0,-60},{-74,-60},{-74,6},{-72,6},{-72,6.12},{-70,6.12}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.TDryBul, Toutside.T) annotation (Line(
      points={{0,-60},{20,-60},{20,-12},{112,-12},{112,4},{101.6,4}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(InfiltrationRate_House.y, ToMass_House.u1) annotation (Line(
      points={{-53.4,-12},{-41,-12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(density_House.d, ToMass_House.u2) annotation (Line(
      points={{-14.5,-27},{-46,-27},{-46,-18},{-41,-18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ToMass_House.y, sinInf_House.m_flow_in) annotation (Line(
      points={{-29.5,-15},{-22.75,-15},{-22.75,-5.2},{-16,-5.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Resistance_House.port_b, House.ports[1]) annotation (Line(
      points={{2,12},{16,12},{16,11.5},{21.75,11.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(sinInf_House.ports[1], House.ports[2]) annotation (Line(
      points={{-4,-10},{16,-10},{16,11.5},{21.75,11.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(density_House.port, House.ports[3]) annotation (Line(
      points={{-9,-32},{16,-32},{16,11.5},{21.75,11.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(souInf_House.ports[3], Resistance_House.port_a) annotation (Line(
      points={{-58,6},{-34,6},{-34,12},{-10,12}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(TSetHea.y[1], Tinstruction.T) annotation (Line(
      points={{-225.6,-120},{-220,-120},{-220,-30},{-199.6,-30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sum.y, ConsoElectrique.u)
    annotation (Line(points={{-133.4,30},{-121.6,30}}, color={0,0,127}));
  connect(Coef[1].y, ProdOccupants.u) annotation (Line(points={{-179.6,30},{
          -164,30},{-164,60},{-141.6,60}}, color={0,0,127}));
  connect(Coef[2].y, ConsoEclairage.u) annotation (Line(points={{-179.6,30},{
          -166,30},{-166,0},{-141.6,0}}, color={0,0,127}));
  connect(Coef[3].y, ConsoElectromenager.u) annotation (Line(points={{-179.6,30},
          {-168,30},{-168,-28},{-141.6,-28}}, color={0,0,127}));
  connect(schedules.y[1], Coef[1].u) annotation (Line(points={{-219,90},{-220,
          90},{-220,30},{-188.8,30}}, color={0,0,127}));
  connect(schedules.y[2], Coef[2].u) annotation (Line(points={{-219,90},{-216,
          90},{-216,30},{-188.8,30}}, color={0,0,127}));
  connect(schedules.y[3], Coef[3].u) annotation (Line(points={{-219,90},{-218,
          90},{-218,30},{-188.8,30}}, color={0,0,127}));
  connect(Coef[2].y, sum.u[1]) annotation (Line(points={{-179.6,30},{-162,30},{
          -162,28},{-148,28},{-147.2,28},{-147.2,29.4}}, color={0,0,127}));
  connect(Coef[3].y, sum.u[2]) annotation (Line(points={{-179.6,30},{-162,30},{
          -162,32},{-148,32},{-148,30.6},{-147.2,30.6}}, color={0,0,127}));
  annotation (
experiment(
      StopTime=3.1536e+007,
      Interval=1800,
      Tolerance=1e-006,
      __Dymola_Algorithm="Cvode"),  Documentation(info="<html>
<h4><span style=\"color: #008000\">Modifications 2015-11-27:</span></h4>
<p>Modification du sc&eacute;nario de chauffage pour celui de la RT2012.</p>
<p><br><b>Modifications 2016-10-10:</b></p>
<p>Ajout du d&eacute;tail des apports internes et des consommations &eacute;lectriques pour estimer la consommation annuelle.</p>
<h4>Modifications 2016-10-18:</h4>
<p>Voir modification de <a href=\"SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive\">SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive</a></p>
</html>", revisions="<html>
<ul>
<li>
October 9, 2013, by Michael Wetter:<br/>
Implemented soil properties using a record so that <code>TSol</code> and
<code>TLiq</code> are assigned.
This avoids an error when the model is checked in the pedantic mode.
</li>
<li>
July 15, 2012, by Michael Wetter:<br/>
Added reference results.
Changed implementation to make this model the base class
for all BESTEST cases.
Added computation of hourly and annual averaged room air temperature.
<li>
October 6, 2011, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-240,-160},{320,200}},
          preserveAspectRatio=false)),
    Icon(coordinateSystem(extent={{-240,-160},{320,200}})),
    __Dymola_experimentSetupOutput(doublePrecision=true),
    __Dymola_experimentFlags(
      Advanced(GenerateVariableDependencies=false, OutputModelicaCode=false),
      Evaluate=true,
      OutputCPUtime=true,
      OutputFlatModelica=false));
end IGC_house_Simulation_passive;
