within SolarSystem.Models.Systems.IGC_system.System;
model IGC_airVectorRecyclePassive_solar26_bis
  "Simulation of SolisArt system with air vector."

  extends SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive(
                   House(nPorts=4, datConExtWin(
layers={Wall,Wall,Wall,Wall,Velux_wall},
azi={N_,O_,E_,S_,E_},
fFra={0.2,0.1,0.164,0.176106,0.3},
til={W_,W_,W_,W_,V_},
glaSys={GlaSys_Argon_PVC, GlaSys_Argon_PVC, GlaSys_Eq_east, GlaSys_Eq_south,GlaSys_Velux},
A={26.4,26.4,26.4,26.4,1.2*1.2},
hWin = {sqrt(WindowNorthArea), sqrt(WindowWestArea), sqrt(WindowEastArea), sqrt(WindowSouthArea), 1.18},
wWin = {sqrt(WindowNorthArea), sqrt(WindowWestArea), sqrt(WindowEastArea), sqrt(WindowSouthArea), 1.14})),     souInf_House(nPorts=4),
    weather_file=weather_data,
    schedules(table=loads_data.schedule),
    laineRoche_souffle(x=CeilingInsulationLength),
    laine_verre(x=WallInsulationLength),
    polyurethane(x=FloorInsulationLength),
    PlanithermXN(tauSol={tauSol}, absIR_a=absIR_a));

  extends
    SolarSystem.Models.Systems.IGC_system.System.Variables_selection_monozone;

// Systems parameters (public)

public
  parameter Real pv_area=0 "PV_area Used";
  parameter Real pv_nb=0 "Number of PV used";

  // Mediums
  package MediumE = SolarSystem.Media.WaterGlycol
    "Medium water model with glycol";
  package MediumDHW = Buildings.Media.Water "Medium water model";

  // Pump initialization
  parameter SolarSystem.Data.Unit.MassFlowRatePerArea pump_flows[2] = {40/3600, 40/3600}
  "Mass flow rate for system pump ([1] == S5, S6, [2] == S2) per collector square meter (kg/s/m2 collector)";

protected
    parameter Modelica.SIunits.ThermalConductivity lambdaTank=0.04
    "Heat conductivity of insulation";
    parameter Modelica.SIunits.ThermalConductivity lambdaPipe=0.04
    "Heat conductivity of insulation";

 // MORRIS INPUTS
    // Computation of tank and pipes
public
    parameter Modelica.SIunits.Length pipeInsulationThickness = 0.013 "Pipe insulation thickness";
    parameter Modelica.SIunits.Length bufferInsulationThickness = 0.055 "Insulation thickness of the Buffer tank (default with 100mm)";
    parameter Modelica.SIunits.Length DHWInsulationThickness = 0.055 "Insulation thickness of the DHW tank (default with 55mm)";
    // Computation of walls / ceiling / floor using existing component insulance
public
    parameter Modelica.SIunits.ThermalInsulance WallInsulance = 4 "Thermal insulance of walls";
    parameter Modelica.SIunits.ThermalInsulance FloorInsulance = 6 "Thermal insulance of floor";
    parameter Modelica.SIunits.ThermalInsulance CeilingInsulance = 6 "Thermal insulance of ceiling (excepted velux)";
    // Remove insulance part from other components (except insulation) to compute the insulation thickness
protected
    parameter Modelica.SIunits.Length WallInsulationLength = (WallInsulance - 1.3687)*laine_verre.k "Wall insulation thikness from insulance.";
    parameter Modelica.SIunits.Length FloorInsulationLength = (FloorInsulance - 4.448)*polyurethane.k "Floor insulation thikness from insulance.";
    parameter Modelica.SIunits.Length CeilingInsulationLength = (CeilingInsulance - 0.04)*laineRoche_souffle.k "Ceiling insulation thikness from insulance.";

    // Compute window area parameters
public
    parameter Modelica.SIunits.Length WindowNorthArea = 0.57;
    parameter Modelica.SIunits.Length WindowSouthArea = 6.78;
    parameter Modelica.SIunits.Length WindowWestArea = 3.24;
    parameter Modelica.SIunits.Length WindowEastArea = 5.38;
    // Compute windows performance parameters
    parameter Modelica.SIunits.TransmissionCoefficient tauSol = 0.643 "Default to XN value";
    parameter Modelica.SIunits.Emissivity absIR_a = 0.047 "Default to XN value";
    // Compute solar instruction

  // Schedules and weathers / collector data definition
public
    inner parameter Modelica.SIunits.Temperature SolarTempInstruction = 283.15 "Solar temperature instruction (overheating allows below)";

  parameter Real posTweak = 1 "Coefficient used to shift up (>1) or down (<1) the heat exchanger.";

  replaceable Data.Parameter.ParametricStudy201611.DrawingUp.ADEME_33   drawingUp_data
    constrainedby
    Data.Parameter.ParametricStudy201611.DrawingUp.AbstractDrawingUp
    annotation (Placement(transformation(extent={{-356,244},{-304,296}})));
  replaceable
    Data.Parameter.ParametricStudy201611.Instructions.Consigne19_hygroB instructions_data
    constrainedby
    Data.Parameter.ParametricStudy201611.Instructions.AbstractInstructions
    annotation (Placement(transformation(extent={{-456,244},{-404,296}})));

    replaceable parameter
    Data.Parameter.ParametricStudy201611.Exchangers.BlackSteel exchanger_data
    constrainedby
    Data.Parameter.ParametricStudy201611.Exchangers.AbstractExchanger
    annotation (Placement(transformation(extent={{-556,244},{-504,296}})));

  replaceable parameter Data.Parameter.ParametricStudy201611.Weathers.Bordeaux weather_data
    constrainedby Data.Parameter.City_Datas.City_Data
    annotation (Placement(transformation(extent={{-256,244},{-204,296}})));

replaceable parameter Data.Parameter.Optimization201706.Collectors.IDMK
    collectorPer constrainedby
    Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector
                 "A generic solar collector based on IDMK"
    annotation (Placement(transformation(extent={{-654,244},{-602,296}})));

   // Helper to compute convection for tank to room
protected
  parameter Modelica.SIunits.Distance DHW_r = sqrt(DHW_Tank.VTan /
                                                   (Modelica.Constants.pi * DHW_Tank.hTan)) "Radius of sanitary tank.";
  parameter Modelica.SIunits.Distance Buffer_r = sqrt(Buffer_Tank.VTan /
                                                   (Modelica.Constants.pi * Buffer_Tank.hTan)) "Radius of sanitary tank.";
  parameter Integer posT3Sensor =  DHW_Tank.nSeg -
                                   integer(ceil((DHW_Tank.hHex_a - (DHW_Tank.hHex_a - DHW_Tank.hHex_b) / 2) *
                                                 DHW_Tank.nSeg / DHW_Tank.hTan))
  "Pos of height of exchanger minus half the distance between height and bottom of exchanger. Then find corresponding segment in tank for T3";

   // Internal calculations

  parameter Modelica.SIunits.MassFlowRate m_flow_nominal[2] = pump_flows * Collector.nPanels*Collector.per.A
    "Mass flow rate for left pumps [1] and right pumps [2] [l/h] to [kg/s]";

  // This relation is only True if we have 4 * dp_solar_other + dp_V3V_solar
  // on each possible flow path for S5 and S6.
  parameter Real kv_solar = (m_flow_nominal[1] * 3.6) / (((4 * dp_solar_other + dp_V3V_solar) / 100000)^0.5)
  "Kv factor for solar pumps (S5 and S6)";
  // Solar mass flow rate [kg/s] en [m3/s] ---> m_flow_solar / 1000
  parameter Modelica.SIunits.MassFlowRate m_flow_solar[5] = linspace(0.1, 1, 5) * m_flow_nominal[1]
  "Mass flow rate for solar pumps (S5 and S6)";
  parameter Modelica.SIunits.Pressure dp_solar[5] = (m_flow_solar .* m_flow_solar) * (3.6*3.6*100000) / (kv_solar^2)
  "Element wise multiplication (Q[i]^2 / factor[i] for i in range(1, 6)";
  // Heat mass flow rate [kg/s] to [m3/s] ---> m_flow_solar / (3600*1000)
  parameter Modelica.SIunits.MassFlowRate m_flow_heat[5] = linspace(0.1, 1, 5) * m_flow_nominal[2]
  "Mass flow rate for heating pumps (S2)";
  // This relation is only True if we have 2 * dp_solar_other + dp_V3V_solar (without losses inside branche with S2)
  // on each possible flow path for S2 (pressure losses in the heat branch == dp_other * 2)
  parameter Modelica.SIunits.Pressure dp_heat_other_nominal = (dp_heat[1] - (4 * dp_solar[1] / 6))
  "Value of the aditional pressure to add to the S2 branch to get the correct pressure losses
  (pressure_needed - pressure_dispo_solar_side)";
  parameter Modelica.SIunits.Pressure dp_heat[5] = (m_flow_heat .* m_flow_heat) * (3.6*3.6*100000) / (kv_solar^2)
  "Element wise multiplication (Q[i]^2 / factor[i] for i in range(1, 11)";
  parameter Modelica.SIunits.Pressure dp_V3V_solar = 10000
    "Pressure difference V3V solar (when opened)";
  parameter Modelica.SIunits.Pressure dp_solar_other = 5000
    "Pressure difference for other component";
  parameter Integer collector_seg(min=1) = 20
    "Collector discretization used inside control algorithms";
  parameter Modelica.Media.Interfaces.PartialMedium.Temperature T_start=328.15
    "Start value of temperature";
  parameter Modelica.SIunits.SpecificHeatCapacity Cp_value=3608
    "Value of Real output";
  parameter Modelica.SIunits.Time tau=15
    "Time constant of fluid volume for nominal flow, used if dynamicBalance=true";
  parameter Modelica.SIunits.Velocity v_nominal=0.3
    "Velocity at m_flow_nominal (used to compute default diameter)";
  // 1=V3V, 2=V2VC6, 3=V2VC5, 4=V2VC2
  parameter Boolean homotopyInitialization[4]={false, true, true, true}
    "= true, use homotopy method";
  // 1=V3V, 2=V2VC6, 3=V2VC5, 4=V2VC2
  parameter Boolean filteredOpening[4]={false, false, false, true}
    "= true, if opening is filtered with a 2nd order CriticalDamping filter";
  // 1=C6, 2=C5, 3=C2
  parameter Boolean filteredSpeed[3]= {false, false, true}
    "= true, if speed is filtered with a 2nd order CriticalDamping filter";
  // 1=V3V, 2=V2VC6, 3=V2VC5, 4=V2VC2
  parameter Real deltaM[4]={0.05, 0.05, 0.05, 0.05}
    "Fraction of nominal flow rate where linearization starts, if y=1";
  parameter Modelica.SIunits.Time riseTime_pump=30
    "Rise time of the filter (time to reach 99.6 % of an opening step)[1] pour v3v, [2] pour v2v, [3] pour pompes";
  parameter Modelica.SIunits.Time riseTime_V2V=30
    "Rise time of the filter (time to reach 99.6 % of an opening step)[1] pour v3v, [2] pour v2v, [3] pour pompes";
  parameter Modelica.SIunits.Time riseTime_V3V=30
    "Rise time of the filter (time to reach 99.6 % of an opening step)[1] pour v3v, [2] pour v2v, [3] pour pompes";

// Pumps parameters (protected)
  parameter Buildings.Fluid.Movers.Data.Generic C6_5(
    motorCooledByFluid=true,
    pressure(V_flow=m_flow_solar/1000, dp=Modelica.Math.Vectors.reverse(
          dp_solar)),
    use_powerCharacteristic=true,
    power(V_flow={0.002,0.22,0.486,0.789,1.054,1.289,1.598,1.858}/3600, P={6.233,
          8.107,10.516,13.731,17.038,20.525,26.162,31.89}))
    annotation (Placement(transformation(extent={{-614,-18},{-594,2}})));

  parameter Buildings.Fluid.Movers.Data.Generic C4_2(
    motorCooledByFluid=true,
    pressure(V_flow=m_flow_heat/1000, dp=Modelica.Math.Vectors.reverse(dp_heat)),
    use_powerCharacteristic=true,
    power(V_flow={0.002,0.22,0.486,0.789,1.054,1.289,1.598,1.858}/3600, P={6.233,
          8.107,10.516,13.731,17.038,20.525,26.162,31.89}))
    annotation (Placement(transformation(extent={{-400,-38},{-380,-18}})));

// Algorithms (public)
public
  Control.Electrical_control.Equipements_control_monozone algorithm_control(
      table=instructions_data.schedule, temp_min_storage=313.15) annotation (
      __Dymola_tag={"algo","Consignesouff"}, Placement(transformation(extent={{
            -764,-162},{-472,-72}})));
// Flow/Temp sensors (public)
public
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T3
    annotation (Placement(transformation(extent={{-526,122},{-512,136}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T4
    annotation (Placement(transformation(extent={{-528,142},{-514,156}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T5
    annotation (Placement(transformation(extent={{-640,126},{-652,138}})));

  Buildings.Fluid.Sensors.TemperatureTwoPort Tafter_hex(
    tau=tau,
    redeclare package Medium = MediumA,
    m_flow_nominal=900/3600*1.2)
    "Stable temperature after exchanger but before electrical supply"
                                 annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=270,
        origin={-320,120})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tsoufflage
    "Accurate temperature of air after exchanger and electrical supply."
    annotation (Placement(transformation(extent={{-324,142},{-340,158}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T7
    "Room air temperature"
    annotation (Placement(transformation(extent={{-382,-8},{-398,8}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tbefore_hex
    "Accurate temperature after mixing room but before exchanger."
    annotation (Placement(transformation(extent={{-162,-58},{-178,-42}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T22
    "Temperature of water next to C6 pump."
    annotation (Placement(transformation(extent={{8,8},{-8,-8}},
        rotation=180,
        origin={-650,0})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T21
    "Temperature of water next to C5 pump."
    annotation (Placement(transformation(extent={{-562,-8},{-578,8}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Tbefore_hex_stable(
    tau=tau,
    redeclare package Medium = MediumA,
    m_flow_nominal=900/3600*1.2)
    "Stable temperature after mixing room but before exchanger." annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={-210,-20})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Tsoufflage_stable(
    tau=tau,
    redeclare package Medium = MediumA,
    m_flow_nominal=900/3600*1.2)
    "Stable temperature of air after exchanger and electrical supply."
    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-210,20})));

// Systems (public)

  Solar_Collector.EN12975_Integrator_complete    Collector(
    redeclare package Medium = MediumE,
    rho=0.2,
    sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Parallel,
    T_start=T_start,
    deltaM=0.1,
    lat(displayUnit="rad") = weather_file.lattitude,
    nSeg=collector_seg,
    from_dp=false,
    linearizeFlowResistance=false,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    azi(displayUnit="rad") = 0,
    use_shaCoe_in=false,
    computeFlowResistance=false,
    homotopyInitialization=false,
    per=collectorPer,
    nPanels=4,
    til(displayUnit="deg") = 0.33004176155213)
                                  annotation (__Dymola_tag={"Power","Solar", "Energy"},
      Placement(transformation(extent={{-792,130},{-740,190}})));

  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex Buffer_Tank(
    redeclare package Medium = MediumDHW,
    redeclare package MediumHex = MediumE,
    nSeg=20,
    tau(displayUnit="s") = 30,
    hexSegMult=3,
    Q_flow_nominal(displayUnit="kW") = 103000,
    mHex_flow_nominal=0.36,
    dExtHex(displayUnit="mm") = 0.0346,
    T_start=T_start,
    VTan(displayUnit="l") = 0.3,
    dpHex_nominal=0,
    computeFlowResistance=false,
    m_flow_nominal=0,
    kIns=lambdaTank,
    cHex=exchanger_data.heatCapacity,
    dHex=exchanger_data.density,
    hTan=1.75*Buffer_Tank.VTan/0.5,
    hHex_a=1.415*Buffer_Tank.hTan/1.75,
    hHex_b=0.255*Buffer_Tank.hTan/1.75,
    dIns(displayUnit="mm") = bufferInsulationThickness,
    TTan_nominal=283.15,
    THex_nominal=318.15)
    "Height of exchanger relatives to tank height. Height tank relative to tank volume."
    annotation (__Dymola_tag={"Temperature","Solar"}, Placement(transformation(
          extent={{-652,54},{-612,108}})));
  Buildings.Fluid.Storage.StratifiedEnhancedInternalHex DHW_Tank(
    nSeg=20,
    tau(displayUnit="s") = 120,
    redeclare package Medium = MediumDHW,
    redeclare package MediumHex = MediumE,
    m_flow_nominal=m_flow_nominal[1],
    computeFlowResistance=false,
    dpHex_nominal=0,
    T_start=T_start,
    kIns=lambdaTank,
    Q_flow_nominal=53000,
    mHex_flow_nominal=0.366,
    dExtHex(displayUnit="mm") = 0.0337,
    cHex=exchanger_data.heatCapacity,
    dHex=exchanger_data.density,
    hHex_b=DHW_Tank.hHex_a - 0.685*DHW_Tank.hTan/1.67,
    hHex_a=posTweak*0.86*DHW_Tank.hTan/1.67,
    hTan=1.67*DHW_Tank.VTan/0.4,
    VTan(displayUnit="l") = 0.3,
    dIns(displayUnit="mm") = DHWInsulationThickness,
    TTan_nominal=318.15,
    THex_nominal=283.15)
    annotation (Placement(transformation(extent={{-538,54},{-490,110}})));

  Buildings.Fluid.HeatExchangers.ConstantEffectiveness hex(redeclare package
      Medium1 = MediumE, redeclare package Medium2 = MediumA,
    m1_flow_nominal=m_flow_nominal[2],
    linearizeFlowResistance1=false,
    dp2_nominal=0,
    m2_flow_nominal=90/3600*1.2,
    dp1(start=0),
    dp1_nominal=0,
    show_T=false,
    m1_flow(start=m_flow_nominal[2]),
    from_dp1=false)                                        annotation (__Dymola_tag={"Temperature", "Computed"},
      Placement(transformation(
        extent={{-23,32},{23,-32}},
        rotation=-90,
        origin={-340,75})));

// Drawing up (public)
  Buildings.Fluid.Sources.Boundary_pT PipeNetwork_Source(
    nPorts=1,
    use_T_in=true,
    redeclare package Medium = MediumDHW,
    T=283.15) annotation (__Dymola_tag={"Temperature"}, Placement(transformation(
        extent={{6,-6},{-6,6}},
        rotation=-90,
        origin={-480,14})));
  Valves.DrawingUp.Drawing_up_coef Drawing_up(
    redeclare package Medium = MediumDHW,
    table=drawingUp_data.schedule,
    coef_schedules=drawingUp_coefs.schedule) annotation (__Dymola_tag={"Energy",
        "Power","DHW","Pump","flow"}, Placement(transformation(extent={{-570,66},
            {-600,98}})));
// Pumps/fans (public)
public
  Buildings.Fluid.Movers.SpeedControlled_Nrpm
                                            C6(
    redeclare package Medium =
        MediumE,
    T_start=T_start,
    tau=tau,
    riseTime=riseTime_pump,
    per=C6_5,
    filteredSpeed=filteredSpeed[1])
                    annotation (__Dymola_tag={"Pump", "flow"}, Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-680,-6})));

  Buildings.Fluid.Movers.SpeedControlled_Nrpm
                                            C5(
    redeclare package Medium =
        MediumE,
    T_start=T_start,
    tau=tau,
    riseTime=riseTime_pump,
    per=C6_5,
    filteredSpeed=filteredSpeed[2])
                    annotation (__Dymola_tag={"Pump", "flow"}, Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={-540,-6})));

  Buildings.Fluid.Movers.SpeedControlled_Nrpm
                                            C2(
    redeclare package Medium =
        MediumE,
    T_start=T_start,
    tau=tau,
    riseTime=riseTime_pump,
    per=C4_2,
    filteredSpeed=filteredSpeed[3])
                    annotation (__Dymola_tag={"Pump", "flow"}, Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=90,
        origin={-362,-8})));
        Buildings.Fluid.Movers.FlowControlled_m_flow fan(redeclare package
      Medium =
        MediumA,
    allowFlowReversal=false,
    m_flow_nominal=900/3600*1.2,
    tau=tau,
    riseTime=100,
    filteredSpeed=false,
    redeclare Buildings.Fluid.Movers.Data.Generic per,
    nominalValuesDefineDefaultPressureCurve=true)
    annotation (__Dymola_tag={"Fan", "flow"}, Placement(transformation(extent={{-160,-30},{-180,-10}})));

// Energy integrators (public)
  SolarSystem.Utilities.Other.Integrator_simple integrator_losses annotation (
      __Dymola_tag={"Energy","Losses"}, Placement(transformation(extent={{-424,-154},
            {-384,-126}})));

  SolarSystem.Utilities.Other.Integrator_energy integrator_collector(Cp_value=MediumE.cp_const)
    annotation (__Dymola_tag={"Energy", "Solar"}, Placement(transformation(extent={{-424,-114},{-384,-86}})));
  SolarSystem.Utilities.Other.Integrator_energy integrator_DHW_Solar_charge(Cp_value=MediumE.cp_const)
    "Integration of the solar energy given to the DHW_Tank."
    annotation (__Dymola_tag={"Energy", "Solar"}, Placement(transformation(extent={{-300,
            -114},{-260,-86}})));
  SolarSystem.Utilities.Other.Integrator_energy_air integrator_exchanger
    annotation (__Dymola_tag={"Energy", "Solar"}, Placement(transformation(extent={{-300,
            -152},{-260,-124}})));
  SolarSystem.Utilities.Other.Integrator_simple integrator_pumps(k=1)
    "Integration of pumps consumption" annotation (__Dymola_tag={"Energy", "Electric"},
      Placement(transformation(extent={{-420,38},{-394,54}})));
  Utilities.Other.TankConvection tankConvection(A={Modelica.Constants.pi*2*
        DHW_r*DHW_Tank.hTan,Modelica.Constants.pi*2*Buffer_r*Buffer_Tank.hTan,
        Modelica.Constants.pi*DHW_r*DHW_r,Modelica.Constants.pi*Buffer_r*
        Buffer_r})
    annotation (__Dymola_tag={"Energy", "Losses"}, Placement(transformation(extent={{-480,
            176},{-406,208}})));

    // Flow resistances (public)
public
  Buildings.Fluid.FixedResistances.Pipe inFlow_Collector(
    redeclare package Medium = MediumE,
    T_start=T_start,
    v_nominal=v_nominal,
    from_dp=false,
    dp(start=0),
    linearizeFlowResistance=false,
    nSeg=20,
    length=10,
    dp_nominal=dp_solar[1]/6,
    m_flow_nominal=m_flow_nominal[1],
    lambdaIns=lambdaPipe,
    thicknessIns(displayUnit="mm") = pipeInsulationThickness)
                                                     annotation (Placement(
        transformation(
        extent={{-8,-6},{8,6}},
        rotation=90,
        origin={-800,80})));

// Sources (public)
public
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow Elec_to_DHW
    "Prescribed heat flow for heating and cooling"
    annotation (Placement(transformation(extent={{-438,76},{-460,98}})));

  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow Elec_to_air
    "Prescribed heat flow to blowing air" annotation (Placement(transformation(
        extent={{8,-8},{-8,8}},
        rotation=180,
        origin={-330,176})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (__Dymola_tag={"Power", "Weather"}, Placement(transformation(extent={{-834,-142},{-806,-116}})));
// Flow resistances (protected)
protected
  Buildings.Fluid.FixedResistances.Pipe outFlow_Collector(
    redeclare package Medium = MediumE,
    useMultipleHeatPorts=true,
    T_start=T_start,
    v_nominal=v_nominal,
    dp(start=0),
    nSeg=20,
    length=10,
    from_dp=false,
    linearizeFlowResistance=false,
    dp_nominal=dp_solar[1]/6,
    m_flow_nominal=m_flow_nominal[1],
    lambdaIns=lambdaPipe,
    thicknessIns=pipeInsulationThickness)
    annotation (Placement(transformation(extent={{-728,154},{-712,166}})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM BufferTank_BottomSplitter1(
    redeclare package Medium = MediumE,
    T_start=T_start,
    from_dp=false,
    dp_nominal={0,0,0},
    m_flow_nominal=m_flow_nominal[1]*{1,1,-1})
                   "Flow splitter"                         annotation (
      Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=180,
        origin={-680,60})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM BufferTank_TopSplitter(
    redeclare package Medium = MediumE,
    T_start=T_start,
    from_dp=false,
    m_flow_nominal=m_flow_nominal[1]*{1,-1,-1},
    dp_nominal={0,0,0})
                   "Flow splitter"                          annotation (
      Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=180,
        origin={-680,160})));
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM HealthTank_SolarBottomSplitter(
    redeclare package Medium = MediumE,
    T_start=T_start,
    from_dp=false,
    m_flow_nominal=m_flow_nominal[1]*{1,-1,1},
    dp_nominal={0,0,0})
                   "Flow splitter"                         annotation (
      Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={-540,-40})));
protected
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM HealthTank_SolarTopSplitter(
    redeclare package Medium = MediumE,
    T_start=T_start,
    m_flow_nominal=m_flow_nominal[1]*{1,-1,-1},
    from_dp=false,
    tau=tau,
    dp_nominal={0,0,0})
             "Flow splitter"                                      annotation (
      Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=180,
        origin={-550,160})));

public
  Buildings.Fluid.FixedResistances.FixedResistanceDpM Resistance_House(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    linearized=true,
    from_dp=true,
    dp_nominal=100,
    m_flow_nominal=0.03 + 0.4*(26.4*4 + 8.6 + floor_area)/3600)
    annotation (Placement(transformation(extent={{-16,2},{-8,10}})));

// Valves (protected)
public
  Buildings.Fluid.Actuators.Valves.ThreeWayEqualPercentageLinear
                                                  Valve_Solar(
    redeclare package Medium = MediumE,
    T_start=T_start,
    filteredOpening=filteredOpening[1],
    riseTime=riseTime_V3V,
    l={0.005,0.005},
    fraK=1,
    from_dp=false,
    deltaM=deltaM[1],
    tau=tau,
    portFlowDirection_1=Modelica.Fluid.Types.PortFlowDirection.Leaving,
    portFlowDirection_2=Modelica.Fluid.Types.PortFlowDirection.Entering,
    portFlowDirection_3=Modelica.Fluid.Types.PortFlowDirection.Leaving,
    homotopyInitialization=homotopyInitialization[1],
    dpValve_nominal=dp_solar[1]/3,
    dpFixed_nominal={0,dp_solar[1]/3},
    m_flow_nominal=m_flow_nominal[1])
    annotation (Placement(transformation(extent={{-728,-30},{-708,-50}})));

  Buildings.Fluid.Actuators.Valves.TwoWayEqualPercentage
                                                Valve_C6(
    redeclare package Medium = MediumE,
    riseTime=riseTime_V2V,
    m_flow(start=0),
    dp(start=0),
    from_dp=false,
    filteredOpening=filteredOpening[2],
    deltaM=deltaM[2],
    homotopyInitialization=homotopyInitialization[2],
    dpValve_nominal=dp_solar[1]/6,
    dpFixed_nominal=dp_solar[1]/6,
    m_flow_nominal=m_flow_nominal[1])                 "Pressure drop"
                    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={-680,34})));
public
  Buildings.Fluid.Actuators.Valves.TwoWayEqualPercentage
                                                Valve_C5(
    redeclare package Medium = MediumE,
    riseTime=riseTime_V2V,
    from_dp=false,
    filteredOpening=filteredOpening[3],
    deltaM=deltaM[3],
    homotopyInitialization=homotopyInitialization[3],
    dpValve_nominal=dp_solar[1]/6,
    dpFixed_nominal=dp_solar[1]/6,
    m_flow_nominal=m_flow_nominal[1])                 "Pressure drop"
                    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={-540,24})));
protected
  Buildings.Fluid.Actuators.Valves.TwoWayEqualPercentage
                                                Valve_C2(
    redeclare package Medium = MediumE,
    riseTime=riseTime_V2V,
    from_dp=false,
    m_flow(start=0),
    dp(start=0),
    filteredOpening=filteredOpening[4],
    deltaM=deltaM[4],
    homotopyInitialization=homotopyInitialization[4],
    m_flow_nominal=m_flow_nominal[2],
    dpValve_nominal=dp_heat_other_nominal/2,
    dpFixed_nominal=dp_heat_other_nominal/2)          "Pressure drop"
                    annotation (Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={-360,24})));

// Flow/Temp sensors (protected)
protected
  Buildings.Fluid.Sensors.MassFlowRate Collector_FlowSensor(redeclare package
      Medium = MediumE) annotation (Placement(transformation(
        extent={{-4,4},{4,-4}},
        rotation=90,
        origin={-800,114})));
public
  Buildings.Fluid.Sensors.TemperatureTwoPort Tcold_water(
    redeclare package Medium = MediumDHW,
    m_flow_nominal=0.04,
    tau=10) annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-480,36})));

// Sources (protected)
protected
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Weather(
      computeWetBulbTemperature=false,
    filNam=weather_file.path) "Weather datas"
    annotation (Placement(transformation(extent={{-668,186},{-700,214}})));

  Buildings.Fluid.Sources.Boundary_pT NoFlow_StaticSource(
    redeclare package Medium = MediumDHW,
    use_T_in=true,
    nPorts=1) annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=90,
        origin={-660,116})));
  Buildings.Fluid.Sources.MassFlowSource_T NoFlow_DynamicSource(
    redeclare package Medium = MediumDHW,
    use_T_in=true,
    nPorts=1) annotation (Placement(transformation(extent={{-634,136},{-626,144}})));
public
  Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
    redeclare package Medium = MediumE,
    T_start=T_start,
    V_start(displayUnit="l") = 0.12)
    annotation (Placement(transformation(extent={{-652,186},{-628,212}})));
protected
  Modelica.Blocks.Sources.CombiTimeTable PipeNetwork_Temp(extrapolation=
        Modelica.Blocks.Types.Extrapolation.Periodic, table=weather_file.water,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments)
    "Cold water temperature according to month of the year"
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=-90,
        origin={-478,-12})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature Text annotation (
      Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-740,14})));

  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{170,178},{190,198}})));

// Other (protected)
Buildings.Fluid.MixingVolumes.MixingVolume vol(nPorts=2, redeclare package
      Medium = MediumA,
    m_flow_nominal=900/3600*1.2,
    prescribedHeatFlowRate=true,
    V=0.5)              annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-290,150})));
     Buildings.Fluid.Sensors.Density density_mix(redeclare package Medium =
        MediumA) "Air density inside the building"
    annotation (Placement(transformation(extent={{-84,-12},{-96,0}})));
public
  Buildings.Fluid.Sensors.MassFlowRate recycled_air_flow(redeclare package
      Medium = MediumA) "Recycled air mass flow rate from the house"
    annotation (__Dymola_tag={"Fan", "flow"}, Placement(transformation(extent={{-52,-40},
            {-64,-28}})));
  Buildings.Fluid.Sensors.MassFlowRate Outside_air_flow(redeclare package
      Medium = MediumA) "Mass flow rate of outside air used" annotation (__Dymola_tag={"Fan", "flow"},
      Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=-90,
        origin={-120,-60})));
protected
  Modelica.Thermal.HeatTransfer.Sensors.HeatFlowSensor heatFlowSensor
    annotation (Placement(transformation(
        extent={{8,-8},{-8,8}},
        rotation=-90,
        origin={-740,40})));
  Modelica.Thermal.HeatTransfer.Components.ThermalCollector Losses_pipes_collector(m=48)
    "Collect all losses from tanks to the house."
    annotation (Placement(transformation(extent={{-750,60},{-730,80}})));

public
  Buildings.Fluid.Sources.MassFlowSource_T Outside(
    redeclare package Medium = MediumA,
    use_T_in=true,
    use_m_flow_in=true,
    use_X_in=true,
    nPorts=1) "Source model for air infiltration"
    annotation (Placement(transformation(extent={{-98,-122},{-112,-108}})));
protected
Buildings.Fluid.MixingVolumes.MixingVolume mixing_box(
    redeclare package Medium = MediumA,
    prescribedHeatFlowRate=false,
    V=0.5,
    nPorts=4,
    m_flow_nominal=90/3600*1.2)
              annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={-118,-10})));
     Buildings.Fluid.Sensors.Density density_outside(redeclare package Medium =
        MediumA) "Air density inside the building"
    annotation (Placement(transformation(extent={{-52,-10},{-64,-22}})));
  Buildings.Utilities.Psychrometrics.X_pTphi x_pTphi
    annotation (Placement(transformation(extent={{-136,-154},{-116,-134}})));
  Buildings.Utilities.Psychrometrics.ToTotalAir
             toTotalAir
    annotation (Placement(transformation(extent={{-110,-154},{-90,-134}})));
  Buildings.Fluid.Sources.MassFlowSource_T Infiltrations(
    redeclare package Medium = MediumA,
    use_T_in=true,
    use_X_in=true,
    nPorts=1,
    use_m_flow_in=true)
    "Source model for air infiltration (107 = walls(26.4*4) + velux walls(1.2*1.2))"
    annotation (Placement(transformation(extent={{-6,-54},{8,-40}})));
public
  Buildings.Fluid.Sensors.TemperatureTwoPort Tbefore_DHW_tank(
    redeclare package Medium = MediumE,
    tau=tau,
    m_flow_nominal=m_flow_nominal[1]) annotation (Placement(transformation(
        extent={{7,8},{-7,-8}},
        rotation=90,
        origin={-550,123})));

  Modelica.Blocks.Math.Product to_mass_flowRate_mix "Volumic to mass flow rate"
    annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=-90,
        origin={-170,2})));
  Modelica.Blocks.Math.Product to_mass_flowRate_outside
    "Volumic to mass flow rate" annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=-90,
        origin={-78,-100})));

protected
  Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM HealthTank_SolarBottomSplitter1(
    redeclare package Medium = MediumE,
    T_start=T_start,
    from_dp=false,
    dp_nominal={0,0,0},
    m_flow_nominal=m_flow_nominal[1]*{1,-1,1})
                   "Flow splitter"                         annotation (
      Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=180,
        origin={-680,-40})));

public
  Modelica.Blocks.Sources.Constant InfiltrationRate_House(k=-0.4*(26.4*4 + 8.6
         + floor_area)/3600) "walls + velux walls +plafond"
    annotation (Placement(transformation(extent={{-122,34},{-102,54}})));
protected
  Modelica.Blocks.Math.Product ToMass_House
    annotation (Placement(transformation(extent={{-60,28},{-46,42}})));
public
  Modelica.Blocks.Math.Sum sum_pumps_power(nin=3, k={1,1,1}) annotation (
      __Dymola_tag={"Power","Internal"}, Placement(transformation(extent={{-442,
            40},{-430,52}})));

// Boundaries conditions

  replaceable Data.Parameter.ParametricStudy201611.InternalLoads.RT2012
    loads_data constrainedby
    Data.Parameter.ParametricStudy201611.InternalLoads.AbstractLoads
    "Description of occupancy, lights consumption, and equipments consumption."
    annotation (Placement(transformation(extent={{-156,244},{-104,296}})));
  replaceable
    Data.Parameter.ParametricStudy201611.DrawingUp.ADEME_coefs
    drawingUp_coefs constrainedby
    Data.Parameter.ParametricStudy201611.DrawingUp.AbstractDrawingUpCoefs
    annotation (Placement(transformation(extent={{-354,326},{-306,374}})));
equation
//
// Energy integrators
//
  // Collector integrator (Tout is collector Tin because we gains are taken from collector not the opposite like with a tank)
  integrator_collector.Tout = inFlow_Collector.vol[inFlow_Collector.nSeg].T;
  integrator_collector.Tin = Collector.Tinside[collector_seg]
    "Last discretization as output temperature";
  integrator_collector.m_flow = Collector_FlowSensor.m_flow;
  // DHW integrator
  integrator_DHW_Solar_charge.Tin = Tbefore_DHW_tank.T;
  integrator_DHW_Solar_charge.Tout = T21.T;
  integrator_DHW_Solar_charge.m_flow = C5.m_flow;
  // Exchanger integrator
  integrator_exchanger.Tin = Tbefore_hex_stable.T;
  integrator_exchanger.Tout = Tafter_hex.T;
  integrator_exchanger.Cp_air =MediumA.specificHeatCapacityCp(state=MediumA.setState_phX(p=fan.port_b.p, h=fan.port_b.h_outflow, X=fan.port_b.Xi_outflow))
    "Internal mass capacity";
  integrator_exchanger.m_flow = fan.m_flow;
  // Losses (pipes) integrator
  integrator_losses.Heat_flow = heatFlowSensor.Q_flow;

//
// Algorithm connections
//
  // Temperatures
  algorithm_control.T1 = Collector.Tinside[collector_seg]
    "Connect collector to T1";
  algorithm_control.T3 = T3.T "Connect bottom tank temperature to T3";
  algorithm_control.T4 = T4.T "Connect top tank temperature to T4";
  algorithm_control.T5 = T5.T "Connect middle storage tank temperature to T5";
  algorithm_control.T7 = T7.T "Connect exchanger outlet temperature to T7";
  // Vector of temperatures(only one room here)
  algorithm_control.Tamb = TRooAir.T "Connect room temperature to Tamb";
  algorithm_control.Tsouff = Tsoufflage.T
    "Connect room blowing temperature to Tsouff";
  algorithm_control.Texch_out = Tafter_hex.T
    "Connect air exchanger outlet temperature to Texch_out";
  // Valve connections
  Valve_Solar.y = if algorithm_control.V3V_solar then 1.0 else 0.0
    "Transform boolean to real and connect result";
  Valve_C6.y = if algorithm_control.S6_state then 1.0 else 0.0
    "Transform boolean to real and connect result";
  Valve_C5.y = if algorithm_control.S5_state then 1.0 else 0.0
    "Transform boolean to real and connect result";
  Valve_C2.y = if algorithm_control.S2_state then 1.0 else 0.0
    "Transform boolean to real and connect result";
  // Electrical power
  Elec_to_DHW.Q_flow = algorithm_control.power_DHW_elec
    "Division is handled by the heat port itself. So we provide full power value";
  Elec_to_air.Q_flow = algorithm_control.power_air_elec
    "Power to air";
  connect(Valve_Solar.port_1, inFlow_Collector.port_a) annotation (Line(
      points={{-728,-40},{-800,-40},{-800,72}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(T5.T, NoFlow_DynamicSource.T_in) annotation (Line(
      points={{-652,132},{-662,132},{-662,141.6},{-634.8,141.6}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));

  connect(inFlow_Collector.port_b, Collector_FlowSensor.port_a) annotation (
      Line(
      points={{-800,88},{-800,110}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(PipeNetwork_Source.ports[1], Tcold_water.port_a) annotation (Line(
      points={{-480,20},{-480,30}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));

  connect(T5.T, NoFlow_StaticSource.T_in) annotation (Line(
      points={{-652,132},{-662,132},{-662,124},{-662.4,124},{-662.4,123.2}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(NoFlow_StaticSource.ports[1], Buffer_Tank.port_a) annotation (Line(
      points={{-660,110},{-660,81},{-652,81}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(NoFlow_DynamicSource.ports[1], Buffer_Tank.port_b) annotation (Line(
      points={{-626,140},{-618,140},{-618,81},{-612,81}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Buffer_Tank.heaPorVol[4], T5.port) annotation (Line(
      points={{-632,79.947},{-632,132},{-640,132}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(DHW_Tank.heaPorVol[posT3Sensor], T3.port) annotation (Line(
      points={{-514,82},{-526,82},{-526,129}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(DHW_Tank.heaPorVol[4], T4.port) annotation (Line(
      points={{-514,80.908},{-514,82},{-528,82},{-528,149}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(Collector_FlowSensor.port_b, Collector.port_a) annotation (Line(
      points={{-800,118},{-800,160},{-792,160}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(PipeNetwork_Temp.y[1], PipeNetwork_Source.T_in) annotation (Line(
      points={{-478,-5.4},{-477.6,-5.4},{-477.6,6.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Collector.port_b, outFlow_Collector.port_a) annotation (Line(
      points={{-740,160},{-728,160}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(weaBus, House.weaBus) annotation (Line(
      points={{-820,-129},{-820,-180},{66,-180},{66,32.425},{46.425,32.425}},
      color={255,204,51},
      thickness=0.25,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, Combles.weaBus) annotation (Line(
      points={{-820,-129},{-820,-180},{66,-180},{66,144.005},{34.005,144.005}},
      color={255,204,51},
      thickness=0.25,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, VideSanitaire.weaBus) annotation (Line(
      points={{-820,-129},{-820,-180},{66,-180},{66,-80},{34,-80},{34,-78.1},{33.9,
          -78.1}},
      color={255,204,51},
      thickness=0.25,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.TDryBul, Toutside.T) annotation (Line(
      points={{-820,-129},{-820,-180},{66,-180},{66,-12},{110,-12},{110,4},{101.6,
          4}},
      color={255,204,51},
      thickness=0.25,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(Weather.weaBus, Collector.weaBus) annotation (Line(
      points={{-700,200},{-792,200},{-792,188.8}},
      color={255,204,51},
      thickness=0.25,
      smooth=Smooth.None));
  connect(Weather.weaBus, weaBus) annotation (Line(
      points={{-700,200},{-756,200},{-756,200},{-820,200},{-820,-64},{-820,-64},
          {-820,-129}},
      color={255,204,51},
      thickness=0.25,
      smooth=Smooth.None), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));

  connect(Elec_to_air.port, vol.heatPort) annotation (Line(
      points={{-322,176},{-320,176},{-320,150},{-300,150}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Elec_to_DHW.port, DHW_Tank.heaPorVol[5]) annotation (Line(
      points={{-460,87},{-512,87},{-512,81.076},{-514,81.076}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));

  connect(Elec_to_DHW.port, DHW_Tank.heaPorVol[4]) annotation (Line(
      points={{-460,87},{-470,87},{-470,86},{-512,86},{-512,80.908},{-514,80.908}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));

  connect(Elec_to_DHW.port, DHW_Tank.heaPorVol[3]) annotation (Line(
      points={{-460,87},{-470,87},{-470,88},{-512,88},{-512,80.74},{-514,80.74}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));

  connect(hex.port_b2, Tafter_hex.port_a) annotation (Line(
      points={{-320.8,98},{-320,98},{-320,110}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Tafter_hex.port_b, vol.ports[1]) annotation (Line(
      points={{-320,130},{-320,140},{-292,140}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));

  connect(vol.heatPort, Tsoufflage.port) annotation (Line(
      points={{-300,150},{-324,150}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(C2.heatPort, T7.port) annotation (Line(
      points={{-368.8,-8},{-370,-8},{-370,0},{-382,0}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));

  connect(weaBus, souInf_House.weaBus) annotation (Line(
      points={{-820,-129},{-820,-180},{-72,-180},{-72,6.12},{-70,6.12}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(souInf_House.ports[3], density_outside.port) annotation (Line(
      points={{-58,6},{-58,-10}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(weaBus.relHum, x_pTphi.phi) annotation (Line(
      points={{-820,-129},{-820,-180},{-166,-180},{-166,-150},{-138,-150}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.TDryBul, x_pTphi.T) annotation (Line(
      points={{-820,-129},{-820,-180},{-166,-180},{-166,-144},{-138,-144}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.pAtm, x_pTphi.p_in) annotation (Line(
      points={{-820,-129},{-820,-180},{-166,-180},{-166,-138},{-138,-138}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));

  connect(x_pTphi.X[1], toTotalAir.XiDry) annotation (Line(
      points={{-115,-144},{-111,-144}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(toTotalAir.XiTotalAir, Outside.X_in[1]) annotation (Line(
      points={{-89,-144},{-84,-144},{-84,-117.8},{-96.6,-117.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(toTotalAir.XNonVapor, Outside.X_in[2]) annotation (Line(
      points={{-89,-148},{-84,-148},{-84,-117.8},{-96.6,-117.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(recycled_air_flow.port_b, mixing_box.ports[1]) annotation (Line(
      points={{-64,-34},{-114,-34},{-114,-20},{-115,-20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Outside.ports[1], Outside_air_flow.port_a) annotation (Line(
      points={{-112,-115},{-120,-115},{-120,-66}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Outside_air_flow.port_b, mixing_box.ports[2]) annotation (Line(
      points={{-120,-54},{-120,-20},{-117,-20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(mixing_box.ports[3], fan.port_a) annotation (Line(
      points={{-119,-20},{-160,-20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(mixing_box.ports[4], density_mix.port) annotation (Line(
      points={{-121,-20},{-90,-20},{-90,-12}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(weaBus.TDryBul, Outside.T_in) annotation (Line(
      points={{-820,-129},{-820,-180},{-72,-180},{-72,-112.2},{-96.6,-112.2}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(vol.ports[2], Tsoufflage_stable.port_a) annotation (Line(
      points={{-288,140},{-260,140},{-260,20},{-220,20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(fan.port_b, Tbefore_hex_stable.port_a) annotation (Line(
      points={{-180,-20},{-200,-20}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Tbefore_hex_stable.port_b, hex.port_a2) annotation (Line(
      points={{-220,-20},{-320.8,-20},{-320.8,52}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(Tsoufflage_stable.port_b, House.ports[1]) annotation (Line(
      points={{-200,20},{0,20},{0,11.5},{21.75,11.5}},
      color={0,127,255},
      smooth=Smooth.None,
      thickness=0.5));
  connect(C6.heatPort, T22.port) annotation (Line(
      points={{-673.2,-6},{-668,-6},{-668,8.88178e-016},{-658,8.88178e-016}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(C5.heatPort, T21.port) annotation (Line(
      points={{-546.8,-6},{-552,-6},{-552,0},{-562,0}},
      color={191,0,0},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));

  connect(toTotalAir.XiTotalAir, Infiltrations.X_in[1]) annotation (Line(
      points={{-89,-144},{-46,-144},{-46,-49.8},{-7.4,-49.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(toTotalAir.XNonVapor, Infiltrations.X_in[2]) annotation (Line(
      points={{-89,-148},{-46,-148},{-46,-49.8},{-7.4,-49.8}},
      color={0,0,127},
      smooth=Smooth.None,
      pattern=LinePattern.Dash,
      thickness=0.25));
  connect(weaBus.TDryBul, Infiltrations.T_in) annotation (Line(
      points={{-820,-129},{-820,-180},{-72,-180},{-72,-44.2},{-7.4,-44.2}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));

  connect(Infiltrations.ports[1], House.ports[2]) annotation (Line(
      points={{8,-47},{8,-48},{14,-48},{14,11.5},{21.75,11.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Text.port, heatFlowSensor.port_a) annotation (Line(
      points={{-740,20},{-740,32}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(heatFlowSensor.port_b, Losses_pipes_collector.port_b) annotation (
      Line(
      points={{-740,48},{-740,60}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(weaBus.TDryBul, Text.T) annotation (Line(
      points={{-820,-129},{-820,-62},{-820,-62},{-820,0},{-740,0},{-740,6.8}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(inFlow_Collector.heatPorts[:], Losses_pipes_collector.port_a[1:
    inFlow_Collector.nSeg]) annotation (Line(
      points={{-797,80},{-740,80}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));
  connect(outFlow_Collector.heatPorts[:], Losses_pipes_collector.port_a[
    inFlow_Collector.nSeg + 1:inFlow_Collector.nSeg*2]) annotation (Line(
      points={{-720,157},{-720,80},{-740,80}},
      color={191,0,0},
      smooth=Smooth.None,
      thickness=0.5));

  connect(fan.heatPort, Tbefore_hex.port) annotation (Line(
      points={{-170,-26.8},{-160,-26.8},{-160,-50},{-162,-50}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(Tcold_water.port_b, DHW_Tank.port_b) annotation (Line(
      points={{-480,42},{-480,82},{-490,82}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tbefore_DHW_tank.port_b, DHW_Tank.portHex_a) annotation (Line(
      points={{-550,116},{-550,71.36},{-538,71.36}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Buffer_Tank.portHex_b, BufferTank_BottomSplitter1.port_2) annotation (
     Line(
      points={{-652,59.4},{-652,60},{-674,60}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Valve_Solar.port_3, BufferTank_BottomSplitter1.port_1) annotation (
      Line(
      points={{-718,-30},{-718,60},{-686,60}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(outFlow_Collector.port_b, BufferTank_TopSplitter.port_1) annotation (
      Line(
      points={{-712,160},{-686,160}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(BufferTank_TopSplitter.port_3, Buffer_Tank.portHex_a) annotation (
      Line(
      points={{-680,154},{-680,70.74},{-652,70.74}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tcold_water.T, Drawing_up.Te) annotation (Line(
      points={{-486.6,36},{-560,36},{-560,70},{-571.5,70}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(BufferTank_TopSplitter.port_2, SurgeTank.port_a) annotation (Line(
      points={{-674,160},{-640,160},{-640,186}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(HealthTank_SolarTopSplitter.port_3, Tbefore_DHW_tank.port_a)
    annotation (Line(
      points={{-550,154},{-550,130}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(BufferTank_TopSplitter.port_2, HealthTank_SolarTopSplitter.port_1)
    annotation (Line(
      points={{-674,160},{-556,160}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(HealthTank_SolarTopSplitter.port_2, hex.port_a1) annotation (Line(
      points={{-544,160},{-359.2,160},{-359.2,98}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(to_mass_flowRate_mix.y, fan.m_flow_in) annotation (Line(points={{-170,
          -2.4},{-170,-8},{-169.8,-8}}, color={0,0,127}));
  connect(to_mass_flowRate_outside.y, Outside.m_flow_in) annotation (Line(
        points={{-78,-104.4},{-78,-109.4},{-98,-109.4}}, color={0,0,127}));
  connect(density_outside.d, to_mass_flowRate_outside.u1) annotation (Line(
        points={{-64.6,-16},{-72,-16},{-75.6,-16},{-75.6,-95.2}}, color={0,0,
          127}));
  connect(density_mix.d, to_mass_flowRate_mix.u1) annotation (Line(points={{
          -96.6,-6},{-100,-6},{-100,14},{-167.6,14},{-167.6,6.8}}, color={0,0,
          127}));
  connect(DHW_Tank.port_a, Drawing_up.tank_port) annotation (Line(points={{-538,
          82},{-552,82},{-552,82.8},{-571.5,82.8}}, color={0,127,255}));
  connect(HealthTank_SolarBottomSplitter.port_2,
    HealthTank_SolarBottomSplitter1.port_1) annotation (Line(points={{-546,-40},
          {-602,-40},{-674,-40}}, color={0,127,255}));
  connect(HealthTank_SolarBottomSplitter1.port_2, Valve_Solar.port_2)
    annotation (Line(points={{-686,-40},{-696,-40},{-708,-40}},
        color={0,127,255}));
  connect(BufferTank_BottomSplitter1.port_3, Valve_C6.port_a) annotation (Line(
        points={{-680,54},{-680,54},{-680,40}}, color={0,127,255}));
  connect(Valve_C6.port_b, C6.port_a) annotation (Line(points={{-680,28},{-680,4}},
                      color={0,127,255}));
  connect(C6.port_b, HealthTank_SolarBottomSplitter1.port_3) annotation (Line(
        points={{-680,-16},{-680,-16},{-680,-34}},
                                                 color={0,127,255}));
  connect(C5.port_b, HealthTank_SolarBottomSplitter.port_3) annotation (Line(
        points={{-540,-16},{-540,-25},{-540,-34}}, color={0,127,255}));
  connect(DHW_Tank.portHex_b, Valve_C5.port_a) annotation (Line(points={{-538,59.6},
          {-540,59.6},{-540,30}}, color={0,127,255}));
  connect(Valve_C5.port_b, C5.port_a)
    annotation (Line(points={{-540,18},{-540,4}}, color={0,127,255}));
  connect(C2.port_a, Valve_C2.port_b)
    annotation (Line(points={{-362,2},{-360,2},{-360,18}}, color={0,127,255}));
  connect(hex.port_b1, Valve_C2.port_a) annotation (Line(points={{-359.2,52},{-360,
          52},{-360,30}}, color={0,127,255}));
  connect(C2.port_b, HealthTank_SolarBottomSplitter.port_1) annotation (Line(
        points={{-362,-18},{-362,-40},{-534,-40}}, color={0,127,255}));
  connect(weaBus.TDryBul, algorithm_control.Text) annotation (Line(
      points={{-820,-129},{-791,-129},{-791,-130.235},{-762.919,-130.235}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.Bezier), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(algorithm_control.S2_speed, C2.Nrpm) annotation (Line(
      points={{-464.97,-102.441},{-444,-102.441},{-444,-56},{-334,-56},{-334,-8},
          {-350,-8}},
      color={0,0,127},
      smooth=Smooth.Bezier));
  connect(algorithm_control.S5_speed, C5.Nrpm) annotation (Line(
      points={{-464.97,-116.206},{-444,-116.206},{-444,-56},{-520,-56},{-520,-6},
          {-528,-6}},
      color={0,0,127},
      smooth=Smooth.Bezier));
  connect(algorithm_control.S6_speed, C6.Nrpm) annotation (Line(
      points={{-464.97,-109.324},{-444,-109.324},{-444,-56},{-700,-56},{-700,-6},
          {-692,-6}},
      color={0,0,127},
      smooth=Smooth.Bezier));
  connect(algorithm_control.fan_flowRate, to_mass_flowRate_mix.u2) annotation (
      Line(
      points={{-465.511,-75.1765},{-190,-75.1765},{-190,14},{-172.4,14},{-172.4,
          6.8}},
      color={0,0,127},
      smooth=Smooth.Bezier));
  connect(algorithm_control.minimal_flowRate, to_mass_flowRate_outside.u2)
    annotation (Line(
      points={{-465.511,-82.5882},{-438,-82.5882},{-438,-80},{-80.4,-80},{-80.4,
          -95.2}},
      color={0,0,127},
      smooth=Smooth.Bezier));
  connect(InfiltrationRate_House.y, ToMass_House.u1) annotation (Line(points={{-101,44},
          {-80,44},{-80,39.2},{-61.4,39.2}},           color={0,0,127}));
  connect(density_outside.d, ToMass_House.u2) annotation (Line(points={{-64.6,
          -16},{-80,-16},{-80,30.8},{-61.4,30.8}}, color={0,0,127}));
  connect(C2.P, sum_pumps_power.u[1]) annotation (Line(points={{-354,-19},{-354,
          -19},{-354,-26},{-374,-26},{-374,-18},{-450,-18},{-450,45.2},{-443.2,45.2}},
        color={0,0,127}));
  connect(C5.P, sum_pumps_power.u[2]) annotation (Line(points={{-532,-17},{-532,
          -26},{-452,-26},{-452,46},{-443.2,46}}, color={0,0,127}));
  connect(C6.P, sum_pumps_power.u[3]) annotation (Line(points={{-688,-17},{-688,
          -24},{-454,-24},{-454,46.8},{-443.2,46.8}}, color={0,0,127}));
  connect(sum_pumps_power.y, integrator_pumps.Heat_flow) annotation (Line(
        points={{-429.4,46},{-424,46},{-418.05,46}}, color={0,0,127}));
  connect(DHW_Tank.heaPorSid, tankConvection.port_a[1]) annotation (Line(points=
         {{-500.56,82},{-500.56,82},{-500,82},{-500,166},{-500,190.8},{-478.52,190.8}},
        color={191,0,0}));
  connect(Buffer_Tank.heaPorSid, tankConvection.port_a[2]) annotation (Line(
        points={{-620.8,81},{-600,81},{-600,191.6},{-478.52,191.6}}, color={191,
          0,0}));
  connect(DHW_Tank.heaPorTop, tankConvection.port_a[3]) annotation (Line(points=
         {{-509.2,102.72},{-509.2,192.4},{-478.52,192.4}}, color={191,0,0}));
  connect(Buffer_Tank.heaPorTop, tankConvection.port_a[4]) annotation (Line(
        points={{-628,100.98},{-600,100.98},{-600,100},{-600,193.2},{-478.52,193.2}},
        color={191,0,0}));
  connect(tankConvection.port_b, House.heaPorAir) annotation (Line(points={{-407.48,
          192},{74,192},{74,19},{32.25,19}}, color={191,0,0}));
  connect(ToMass_House.y, Infiltrations.m_flow_in) annotation (Line(points={{-45.3,
          35},{-32,35},{-32,-36},{-12,-36},{-12,-41.4},{-6,-41.4}}, color={0,0,127}));
  connect(House.ports[3], recycled_air_flow.port_a) annotation (Line(points={{
          21.75,11.5},{6,11.5},{6,-34},{-52,-34}}, color={0,127,255}));
  connect(souInf_House.ports[4], Resistance_House.port_a)
    annotation (Line(points={{-58,6},{-37,6},{-16,6}}, color={0,127,255}));
  connect(Resistance_House.port_b, House.ports[4]) annotation (Line(points={{-8,
          6},{0,6},{0,11.5},{21.75,11.5}}, color={0,127,255}));
  annotation (Diagram(coordinateSystem(extent={{-840,-180},{240,220}},
          preserveAspectRatio=false)),          Icon(coordinateSystem(
          extent={{-840,-180},{240,220}})),
    experiment(
      StartTime=2.26368e+007,
      StopTime=4.1904e+007,
      Interval=180,
      Tolerance=1e-006,
      __Dymola_Algorithm="Cvode"),
    __Dymola_experimentSetupOutput(doublePrecision=true),
    Documentation(info="<html>
<p>Collectors system configuration must be parallel to have balance between branches.</p>
<p>To change weather configuration file you must use a city_infos compatible table.</p>
<p><br>Change pid values inside Pump_Control to smooth temperature, power and mass flow.</p>
<p>Change to 7kW Elec heating maximum power.</p>
<p>Change to 5 min the time we wait before using the electrical extra heating battery.</p>
<p>Add mixing box to reduce power demands by recycling house air.</p>
<h4>Substance in Air Media model. </h4>
<p>Transform concentration to Kg_Water/Kg_DryAir to Kg_Water/Kg_Air in accordance to media model</p>
<p>Water = 1 Remaining = Air = 2</p>
<p><br>Change Pressure and flow inside each branch. </p>
<ul>
<li>1000 Pa at each valve (Q = 0.15467 Kg/s)</li>
<li>1000 Pa at each equipment (exchanger, tanks) (Q = 0.15467 Kg/s)</li>
</ul>
<p><br>Pressure inside equipment adapted to nominal flow[1] using k value.</p>
<p><br>Change PID to smooth result and reduce computing time.</p>
<p>Correct Pump min and max inside PID. Correct initial value for stay on models allowing to start simulation at any step time.</p>
<p>Change name inside fan control --&GT; refactoring to do inside backup ....</p>
<p>Improve PID for Tinstruction and C2. </p>
<p><br>Limit negative flow inside hex.</p>
<p><br>Add integrator inside tanks exchangers and take T1_out at the end of the pipe after the collector to avoid take account of losses to environment.</p>
<p>Change integrator entry inside collector integrator to take account of gains inside first collector segment. We move Tin from first segment of collector to last segment in the previous pipe.</p>
<h4><span style=\"color: #008000\">Version 5: Use of SolarSystem.Control.IGC_control</span></h4>
<p>Add integrator for storage and health tank.</p>
<p>Add exterior pipes losses integration (complete = gains and losses, or only losses).</p>
<p>Connect tanks losses to ambiant temperature node.</p>
<h4><span style=\"color: #008000\">Version 6: Use of SolarSystem.Control.IGC_control_instruction</span></h4>
<h4>Last modification: 20151128</h4>
<p>Replace ventilation by an hygro B system ---&GT; Allow a reduced flow during unoccupancies (20m3/h) and a minimal during occupancy (90m3/h)</p>
<p>Change of temperature consigne: 16&deg;C during the night and 19&deg;C afternoon.</p>
<p>Allow overheating by solar energy during afternoon (22&deg;C).</p>
<p>Add a specific integrator for tank losses. He compute a classical integration and a integration under conditions (when we need heat or overheat). Used to get the usefull solar tank energy part.</p>
<h4><span style=\"color: #008000\">Version 7: Use of SolarSystem.Control.IGC_control_instruction</span></h4>
<h4>Last modifications: 20151207</h4>
<p>Change infiltrations rate to match 0.4 m3/h/m2 (parois froides)</p>
<p>Change walls compositions and add roof window (Velux). (more on <a href=\"SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive\">SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive</a>)</p>
<p>Change scedules to match RT2012 and Aur&eacute;lie simulations. (more on <a href=\"SolarSystem.Models.Systems.IGC_system.House.IGC_characteristics_passive\">SolarSystem.Models.Systems.IGC_system.House.IGC_characteristics_passive</a>)</p>
<p>Change schedule for temperature instruction to use new generate scheudule (same as before but use less memory and computing time)</p>
<p>Change to pumps parameters to correct error after updating to latest version of Buildings library.</p>
<p>Same with glasses components (must write some components as vector)</p>
<h4><span style=\"color: #008000\">Version 8: </span></h4>
<h4><span style=\"color: #008000\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></h4>
<h4>Last modifications: 20151210</h4>
<p>Change all control algorithm to reduce the number of parameter and clean it up.</p>
<p>Clean all parameters from the whole system</p>
<p>Redefine integrators to limit computation time</p>
<h4><span style=\"color: #008000\">Version 9: </span></h4>
<h4><span style=\"color: #008000\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></h4>
<h4>Last modifications: 20151218</h4>
<p>Remove splitters (keep 3 splitters to cut equation systems)</p>
<p>Use a higher value (20---&GT;100) for the surge tank.</p>
<p>Change heat exchanger initial conditions to reduce non-linearity problems.</p>
<p>Reduce valve Dp to get correct mass flow rate (1000 ---&GT; 200)</p>
<p>Reduce discretization inside pipe for collector losses (24 --&GT; 20) and reduce length of pipes (12 --&GT; 10).</p>
<p>Add a variable selection</p>
<h4><span style=\"color: #008000\">Version 10: </span></h4>
<h4><span style=\"color: #008000\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></h4>
<h4>Last modifications: 20160106</h4>
<p>Change drawing up model.</p>
<p>Change drawing up integrator to match new system</p>
<p><br>Change variable selection to match new system</p>
<h4><span style=\"color: #008000\">Version 11: </span></h4>
<h4><span style=\"color: #008000\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></h4>
<h4>Last modifications: 20160106</h4>
<p>Remove filteredspeed for pumps</p>
<p>Remove filteringOpenning for V3V</p>
<p>Set riseTime for fan to 100 (120 previously)</p>
<p>Change DHW media to pure water (previously water + glycol) ---&GT; 3 Medias (Eau, Eau+Glycol, Air)</p>
<p>Set Dynamic to false for S5 and S6</p>
<p>Set surge tank volume to 150 from 120</p>
<p>Set delta_m to 0.02 from 0.15 (linearization start later)</p>
<p>Set to 0 the pump flow inside storage tank (must clean storage tank model)</p>
<h4><span style=\"color: #008000\">Version 12: </span></h4>
<h4><span style=\"color: #008000\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></h4>
<h4>Last modifications: 20160127</h4>
<p>Filtering for V2V of S2 and S2</p>
<p>No filtering for V2V of S5/S6 and S5/S6</p>
<p>Dynamic for S2 only</p>
<p>Remove T8 sensor</p>
<p>Add splitter on top of DHW_tank</p>
<p>Change collector model to follow buildings models evolution automagically</p>
<p>Check modelica pedantic mode (connect pumps/fans and change how GlycolWater media)</p>
<p><br><b><span style=\"color: #008000;\">Version 13: </span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2; color: #008000;\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20160205</span></b></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">General speed up due to:</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Homotopy for all V2V and not for the V3V</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Change all valves to <a href=\"Buildings.Fluid.Actuators.Valves.TwoWayEqualPercentage\">Buildings.Fluid.Actuators.Valves.TwoWayEqualPercentage</a> based version </span></p>
<p><br><b><span style=\"color: #008000;\">Version 14: </span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2; color: #008000;\">Control used: SolarSystem.Control.Electrical_control.Equipements_control_smooth</span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20160216</span></b></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Smooth min and max used in place of max and min (enable linearization)</span></p>
<ul>
<li><span style=\"font-family: MS Shell Dlg 2;\">Smooth drawing up</span></li>
<li><span style=\"font-family: MS Shell Dlg 2;\">Smooth control</span></li>
</ul>
<p><br><span style=\"font-family: MS Shell Dlg 2;\">Remove unused elements from the house model (create a version with occultations and another without)</span></p>
<p><br><b><span style=\"font-family: MS Shell Dlg 2; color: #008000;\">Version 15: </span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2; color: #008000;\">Control used: SolarSystem.Control.Electrical_control.Equipements_control_smooth_enhanced</span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20160623</span></b></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Use of smooth to avoid event inside the control algorithm.</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Keep classic `max` function inside drawing up.</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Set Tolerance to 1e-6 as the optimal tolerance value (increase speed compare to 1e-4 due to less step back used by the solver).</span></p>
<p>Fan does not use filter for its speed now to avoid strange air temperature.</p>
<p><br><b><span style=\"font-size: 16pt;\">MAJ of Dymola + Modelica (3.2.1 --&GT; 3.2.2) + Buildings (20160623):</span></b></p>
<ul>
<li>Remove old house model for new from buildings library (Remove internal capacity)</li>
<li>Remove Dynamic balance from every model</li>
<li>Update pump per parameters (N_nominal --&GT; speed_rpm_nominal)</li>
</ul>
<h4><span style=\"color: #008000\">Version 16: </span></h4>
<h4><span style=\"color: #008000\">Control used: SolarSystem.Control.Electrical_control.Equipements_control_smooth_enhanced</span></h4>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20160630</span></b></p>
<p>DHW temperature setpoint = 55&deg;C</p>
<p>Temperature setpoint: 19&deg;C (occupancy), 18&deg;C (Night), 16&deg;C (inoccupancy)</p>
<h4>Version 17: </h4>
<h4>Control used: SolarSystem.Control.Electrical_control.Equipements_control_smooth_enhanced</h4>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20160722</span></b></p>
<p>Redefine the pumps curves according to system pressure losses assuming water density of 1 kg/dm<sup>3</sup></p>
<p>Selection of a new mass flow rate automagically adapt the pump curve to fit the system pressure losses.</p>
<p>More robust hydraulic pressure losses repartition to help solver to converge easily</p>
<p>Create doc to explain how to compute pump curve according to user flow rate input</p>
<p>Initial temperature is now 55&deg;C to avoid unnecessary energy to init the system</p>
<p>Update buffer tank water media to simple water (from water + glycol)</p>
<h4>Version 18: </h4>
<h4>Control used: SolarSystem.Control.FSM.FSM_Monozone</h4>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20160824</span></b></p>
<p>Rewrite all control algorithm</p>
<p>Transform pump state into real to avoid if statement for valve position</p>
<p><b><span style=\"font-size: 20pt;\">STOP DUE TO INEFICIENCY ...</span></b></p>
<h4>Version 19: </h4>
<h4>Control used: SolarSystem.Control.Electrical_control.Equipements_control_monozone</h4>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20160906</span></b></p>
<p>Allow V3V valve opening only for heat also (before only if a tank can be loaded)</p>
<p>Rewrite of control algorithm (clean, only monozone)</p>
<p>Update Variable selection for monozone control algorithm</p>
<p>Add a collector type selector to be able to easily chance a per parameter or collector per type completely.</p>
<p>Power gains and losses (W/m2) for collector are now computed by Dymola and output directly assign with scripts. No more need to convert in post processing using regex matching. (see <a href=\"SolarSystem.Solar_Collector.EN12975_Integrator_complete\">SolarSystem.Solar_Collector.EN12975_Integrator_complete</a> )</p>
<h4>Version 20: </h4>
<h4>Control used: SolarSystem.Control.Electrical_control.Equipements_control_monozone</h4>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20160908</span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Model used for Morris sensibility analysis.</span></b></p>
<p>Collector per characteristics can be updated using <b>CollectorPer </b>records (useful to change values from outside the model).</p>
<p>Tanks heights and exchanger heights are now proportionnal. We can now change tank volume and dymola adapts it size.</p>
<p>Tank insulation thickness calculate using Fourrier law (using insulance from parameter) and take care of tank diameter.</p>
<p>Pipe insulation thickness is defined by Morris analysis because pipe diameter is not known &agrave; priori.</p>
<p>Assign dynamically wall insulation thickness using insulance parameter.</p>
<p>Assign window area dynamically using a unit height (1 meter) and setting window width to window area.</p>
<p>Assign window solar factor by updating interior glasses surface in a way that the total window solar transmitance is the one asked (T_sol = T_sol(glass one) * T_sol(glass two)).</p>
<p>Assign solar temperature instruction using parameter (set to 10&deg;C at night and to <code>SolarTempInstruction </code>otherwise<code>).</code></p>
<h4>Version 21: </h4>
<h4>Control used: SolarSystem.Control.Electrical_control.Equipements_control_monozone</h4>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20161018</span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Model which can be used for Morris sensibility analysis.</span></b></p>
<p>Add infiltrations = -0.4*(26.4*4 + 8.6 + 98.4)/3600 (velux surface already account in ceilling)</p>
<p>Replace windows component with SGG PLANITHERM ULTRA (PLANITHERM ONE avant)</p>
<p>Assign default values for composants to be able to run a simulation without a parameter sample.</p>
<p>Change of drawing up: 4 * 33 l/pers/day (60&deg;C) used ---&GT; 220 l/day (40&deg;C) with the most on the evenning</p>
<p>Windows area now used correct value. Area defines is split among existing glazing using its area squared root.</p>
<p>Elements inherite from IGC_house_passive (walls, ceilling, floor) composition are the one used inside the final composition.</p>
<p>Add pump power curves to compute pumps consumption. Based on the Wilo-Yonos ECO 30/1-5 BMS for 3mCE of pressure lost.</p>
<p>Update simulation range to cover all electrical needs plus initialization.</p>
<p>Add a parameter to fake a change in PV area (pv_area)</p>
<p>Add pv_area and Collector.nPanels as outputs (SELECTION.Other)</p>
<p>Voir modification de <a href=\"SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive\">SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive</a></p>
<h4>Version 23:</h4>
<h4>Control used: SolarSystem.Control.Electrical_control.Equipements_control_monozone</h4>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20161125</span></b></p>
<h4>Model which can be used for Morris sensibility analysis or parametrical study.</h4>
<p>Make easy to lauch parametric simulations (Data from records dictionnaries) with</p>
<ul>
<li>Different schedules (drawing up, ventilation, temperatures (house and solar) instructions, drawing Up size)</li>
<li>Different weather data </li>
<li>Different solar collectors</li>
<li>Different internal loads (occupancy, lights, equipments)</li>
</ul>
<p><br>Top level model must add a variable with the following signature: `inner parameter Modelica.SIunits.Temperature SolarTempInstruction` because instruction schedule</p>
<p>use a **outer** variable of the same name. This can be used to select a specific schedule for temperature and ventilation and only update the solar temperature instruction.</p>
<p>Remove integrator for solar buffer tank (not used in post-processing) and update integrator for tank losses (remove conditionnal integrators which only integrate if room temperature lower than max(instruction or solar instruction).</p>
<p>Model now accept to move the solar exchanger inside the DHW tank. Exchanger size increases with tank height (proportionnal to volume) and T3 sensor position is also updated to match new exchanger position automatically.</p>
<p>Update pump mass flow rate (40 to 20) which reduce computation time (1400 sec when 3 simulations at the same time) and increase a little the solar heating performance but increase also pump consumption.</p>
<p>Drawing up now accept a schedule to size the energy consumption.</p>
<h4><span style=\"color: #000000\">CORRECTIONS:</span></h4>
<p>Remove duplicated airflow sink which was increasing building losses.</p>
<p>Remove solar factor which was not handle anyway.</p>
<p>Add convective model for energy computed from tank boundaries and update integrator accordingly</p>
<p>Update solar exchanger inside the tank (change power / mass flow rate and diameter)</p>
<p>Update electrical power to DHW water tank (previously divide per 3).</p>
<p>Change volume reference for:</p>
<ul>
<li>DHW tank (before=300l, now=400l (as in the docs)).</li>
<li>Storage tank (before=300l, now=500l (as in the docs)). Also update height to 1.75 to match docs.</li>
</ul>
<p><br>Update T4 sensor position to be between top and bottom of electrical extra energy feed as T3 is for solar exchanger. (Electrical energy is provide to segment 3, 4, 5).</p>
<h4>Version 24:</h4>
<h4>Control used: SolarSystem.Control.Electrical_control.Equipements_control_monozone</h4>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20161128</span></b></p>
<h4>Model which can be used for Morris sensibility analysis or parametrical study.</h4>
<p><br>Correction of air flow model. Avoid inconsiency in flow direction which leads to very strange air temperature ...</p>
<h4>Version 25:</h4>
<h4>Control used: SolarSystem.Control.Electrical_control.Equipements_control_monozone</h4>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20161128</span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Model which can be used for Morris sensibility analysis or parametrical study.</span></b></p>
<p><br>Add default schedule for sensitivity analysis.</p>
<p>Add interface to change XN tauSol and absIR_a values. Default of XN values used.</p>
<h4>Version 26:</h4>
<h4>Control used: SolarSystem.Control.Electrical_control.Equipements_control_monozone</h4>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20170119</span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Model which can be used for Morris sensibility analysis or parametrical study.</span></b></p>
<p><br>Update collector tilt default to 18.91&deg; (33 before). </p>
<p>Used min value for morris parameter that are not part of the most influent:</p>
<ul>
<li>pipeInsulationThickness = 0.013</li>
<li>FloorInsulance = 6</li>
<li>SolarTempInstruction = 283.15</li>
<li>bufferInsulance = 7</li>
<li>DHWInsulance = 7</li>
</ul>
<p><br>Used following schedules :</p>
<ul>
<li>Drawing up = ADEME + monthly coefficient</li>
<li>Internal loads = RT 2012</li>
<li>Weather = Bordeaux</li>
<li>Consigne = 19-18-16 + Hygro B</li>
<li>Exchanger = BlackSteel</li>
</ul>
<h4>Version 26_bis:</h4>
<h4>Control used: SolarSystem.Control.Electrical_control.Equipements_control_monozone</h4>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20170607</span></b></p>
<p>Same as 26 but used thickness of insulatioin for tanks instead of Insulance.</p>
</html>"),
    __Dymola_experimentFlags(
      Advanced(GenerateVariableDependencies=false, OutputModelicaCode=false),
      Evaluate=true,
      OutputCPUtime=true,
      OutputFlatModelica=false));
end IGC_airVectorRecyclePassive_solar26_bis;
