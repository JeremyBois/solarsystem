within SolarSystem.Models.Systems.IGC_system.System;
model ReferenceSystem_28 "Reference system for solar system 28"

  extends SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive(
                   House(nPorts=4, datConExtWin(
layers={Wall,Wall,Wall,Wall,Velux_wall},
azi={N_,O_,E_,S_,E_},
fFra={0.2,0.1,0.164,0.176106,0.3},
til={W_,W_,W_,W_,V_},
glaSys={GlaSys_Argon_PVC, GlaSys_Argon_PVC, GlaSys_Eq_east, GlaSys_Eq_south,GlaSys_Velux},
A={26.4,26.4,26.4,26.4,1.2*1.2},
hWin = {sqrt(WindowNorthArea), sqrt(WindowWestArea), sqrt(WindowEastArea), sqrt(WindowSouthArea), 1.18},
wWin = {sqrt(WindowNorthArea), sqrt(WindowWestArea), sqrt(WindowEastArea), sqrt(WindowSouthArea), 1.14})),     souInf_House(nPorts=4),
    weather_file=weather_data,
    schedules(table=loads_data.schedule),
    laineRoche_souffle(x=CeilingInsulationLength),
    laine_verre(x=WallInsulationLength),
    polyurethane(x=FloorInsulationLength),
    PlanithermXN(tauSol={tauSol}, absIR_a=absIR_a),
    GlaSys_Argon_PVC(gas={glazing.Gas}, glass={glazing.Exterior,glazing.Interior}),
    GlaSys_Eq_south(glass={glazing.Exterior,glazing.Interior}, gas={glazing.Gas}),
    GlaSys_Eq_east(glass={glazing.Exterior,glazing.Interior}, gas={glazing.Gas}));


  extends
    SolarSystem.Models.Systems.IGC_system.System.Variables_selection_monozone;

// Systems parameters (public)

public
  parameter Real pv_area=0 "PV_area Used";
  parameter Real pv_nb=0 "Number of PV used";

  // Mediums
  package MediumE = SolarSystem.Media.WaterGlycol
    "Medium water model with glycol";
  package MediumDHW = Buildings.Media.Water "Medium water model";

protected
    parameter Modelica.SIunits.ThermalConductivity lambdaTank=0.04
    "Heat conductivity of insulation";
    parameter Modelica.SIunits.ThermalConductivity lambdaPipe=0.04
    "Heat conductivity of insulation";

 // MORRIS INPUTS
    // Computation of tank and pipes
public
    parameter Modelica.SIunits.Length pipeInsulationThickness = 0.013 "Pipe insulation thickness";
    parameter Modelica.SIunits.Length bufferInsulationThickness = 0.055 "Insulation thickness of the Buffer tank (default with 100mm)";
    parameter Modelica.SIunits.Length DHWInsulationThickness = 0.055 "Insulation thickness of the DHW tank (default with 55mm)";
    // Computation of walls / ceiling / floor using existing component insulance
public
    parameter Modelica.SIunits.ThermalInsulance WallInsulance = 4 "Thermal insulance of walls";
    parameter Modelica.SIunits.ThermalInsulance FloorInsulance = 6 "Thermal insulance of floor";
    parameter Modelica.SIunits.ThermalInsulance CeilingInsulance = 6 "Thermal insulance of ceiling (excepted velux)";
    // Remove insulance part from other components (except insulation) to compute the insulation thickness
protected
    parameter Modelica.SIunits.Length WallInsulationLength = (WallInsulance - 1.3687)*laine_verre.k "Wall insulation thikness from insulance.";
    parameter Modelica.SIunits.Length FloorInsulationLength = (FloorInsulance - 4.448)*polyurethane.k "Floor insulation thikness from insulance.";
    parameter Modelica.SIunits.Length CeilingInsulationLength = (CeilingInsulance - 0.04)*laineRoche_souffle.k "Ceiling insulation thikness from insulance.";

    // Compute window area parameters
public
    parameter Modelica.SIunits.Length WindowNorthArea = 0.57;
    parameter Modelica.SIunits.Length WindowSouthArea = 6.78;
    parameter Modelica.SIunits.Length WindowWestArea = 3.24;
    parameter Modelica.SIunits.Length WindowEastArea = 5.38;
    // Compute windows performance parameters
    parameter Modelica.SIunits.TransmissionCoefficient tauSol = 0.643 "Default to XN value";
    parameter Modelica.SIunits.Emissivity absIR_a = 0.047 "Default to XN value";
    // Compute solar instruction

  // Schedules and weathers / collector data definition
public
    inner parameter Modelica.SIunits.Temperature SolarTempInstruction = 283.15 "Solar temperature instruction (overheating allows below)";

  parameter Real posTweak = 1 "Coefficient used to shift up (>1) or down (<1) the heat exchanger.";

// Helper to compute convection for tank to room
protected
  parameter Modelica.SIunits.Distance DHW_r = sqrt(DHW_Tank.VTan /
                                                   (Modelica.Constants.pi * DHW_Tank.hTan)) "Radius of sanitary tank.";

// Other
  parameter Modelica.Media.Interfaces.PartialMedium.Temperature T_start=328.15
    "Start value of temperature";
  parameter Modelica.SIunits.SpecificHeatCapacity Cp_value=3608
    "Value of Real output";
  parameter Modelica.SIunits.Time tau=15
    "Time constant of fluid volume for nominal flow, used if dynamicBalance=true";

protected
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{164,172},{184,192}})));
  Buildings.Utilities.Psychrometrics.X_pTphi x_pTphi
    annotation (Placement(transformation(extent={{-168,-156},{-148,-136}})));
  Buildings.Utilities.Psychrometrics.ToTotalAir
             toTotalAir
    annotation (Placement(transformation(extent={{-134,-156},{-114,-136}})));
public
  Buildings.Fluid.Sources.MassFlowSource_T Outside(
    redeclare package Medium = MediumA,
    use_T_in=true,
    use_m_flow_in=true,
    use_X_in=true,
    nPorts=1) "Source model for air infiltration"
    annotation (Placement(transformation(extent={{-130,-124},{-144,-110}})));
  Buildings.Fluid.Sensors.MassFlowRate Outside_air_flow(redeclare package
      Medium = MediumA) "Mass flow rate of outside air used" annotation (__Dymola_tag={"Fan", "flow"},
      Placement(transformation(
        extent={{6,6},{-6,-6}},
        rotation=-90,
        origin={-152,-62})));
protected
Buildings.Fluid.MixingVolumes.MixingVolume mixing_box(
    redeclare package Medium = MediumA,
    prescribedHeatFlowRate=false,
    nPorts=4,
    m_flow_nominal=algorithm_control.fan_flowRate_max*1.2,
    V=0.05)   annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={-150,-12})));
public
        Buildings.Fluid.Movers.FlowControlled_m_flow fan(redeclare package
      Medium =
        MediumA,
    allowFlowReversal=false,
    m_flow_nominal=algorithm_control.fan_flowRate_max*1.2,
    tau=tau,
    riseTime=100,
    filteredSpeed=false,
    redeclare Buildings.Fluid.Movers.Data.Generic per,
    nominalValuesDefineDefaultPressureCurve=true)
    annotation (__Dymola_tag={"Fan", "flow"}, Placement(transformation(extent={{-192,
            -32},{-212,-12}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tbefore_hex
    "Accurate temperature after mixing room but before exchanger."
    annotation (Placement(transformation(extent={{-194,-60},{-210,-44}})));
  Modelica.Blocks.Math.Product to_mass_flowRate_mix "Volumic to mass flow rate"
    annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=-90,
        origin={-202,0})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Tbefore_hex_stable(
    tau=tau,
    redeclare package Medium = MediumA,
    m_flow_nominal=algorithm_control.fan_flowRate_max*1.2)
    "Stable temperature after mixing room but before exchanger." annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={-242,-22})));
  Buildings.Fluid.Sensors.TemperatureTwoPort Tsoufflage_stable(
    tau=tau,
    redeclare package Medium = MediumA,
    m_flow_nominal=algorithm_control.fan_flowRate_max*1.2)
    "Stable temperature of air after exchanger and electrical supply."
    annotation (Placement(transformation(
        extent={{10,10},{-10,-10}},
        rotation=180,
        origin={-242,18})));
public
  Modelica.Blocks.Sources.Constant InfiltrationRate_House(k=-0.4*(26.4*4 + 8.6
         + floor_area)/3600) "walls + velux walls +plafond"
    annotation (Placement(transformation(extent={{-154,32},{-134,52}})));
protected
  Modelica.Blocks.Math.Product ToMass_House
    annotation (Placement(transformation(extent={{-92,26},{-78,40}})));
     Buildings.Fluid.Sensors.Density density_mix(redeclare package Medium =
        MediumA) "Air density inside the building"
    annotation (Placement(transformation(extent={{-116,-14},{-128,-2}})));
     Buildings.Fluid.Sensors.Density density_outside(redeclare package Medium
      = MediumA) "Air density inside the building"
    annotation (Placement(transformation(extent={{-84,-12},{-96,-24}})));
public
  Buildings.Fluid.Sensors.MassFlowRate recycled_air_flow(redeclare package
      Medium = MediumA) "Recycled air mass flow rate from the house"
    annotation (__Dymola_tag={"Fan", "flow"}, Placement(transformation(extent={{-84,-42},
            {-96,-30}})));
protected
  Buildings.Fluid.Sources.MassFlowSource_T Infiltrations(
    redeclare package Medium = MediumA,
    use_T_in=true,
    use_X_in=true,
    nPorts=1,
    use_m_flow_in=true)
    "Source model for air infiltration (107 = walls(26.4*4) + velux walls(1.2*1.2))"
    annotation (Placement(transformation(extent={{-40,-56},{-26,-42}})));
public
Buildings.Fluid.MixingVolumes.MixingVolume vol(nPorts=2, redeclare package
      Medium = MediumA,
    m_flow_nominal=algorithm_control.fan_flowRate_max*1.2,
    prescribedHeatFlowRate=true,
    V=0.05)             annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-250,160})));
public
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tsoufflage
    "Accurate temperature of air after exchanger and electrical supply."
    annotation (Placement(transformation(extent={{-284,152},{-300,168}})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow Elec_to_air
    "Prescribed heat flow to blowing air" annotation (Placement(transformation(
        extent={{8,-8},{-8,8}},
        rotation=180,
        origin={-290,186})));
  replaceable
    Data.Parameter.ParametricStudy201611.DrawingUp.ADEME_coefs
    drawingUp_coefs constrainedby
    Data.Parameter.ParametricStudy201611.DrawingUp.ADEME_coefs
    annotation (Placement(transformation(extent={{-316,346},{-268,394}})));
  replaceable parameter Data.Parameter.ParametricStudy201611.Weathers.Strasbourg
                                                                               weather_data
    constrainedby Data.Parameter.City_Datas.City_Data
    annotation (Placement(transformation(extent={{-218,264},{-166,316}})));
  replaceable Data.Parameter.ParametricStudy201611.InternalLoads.RT2012
    loads_data constrainedby
    Data.Parameter.ParametricStudy201611.InternalLoads.RT2012
    "Description of occupancy, lights consumption, and equipments consumption."
    annotation (Placement(transformation(extent={{-118,264},{-66,316}})));
  replaceable Data.Parameter.Optimization201706.Glazings.PlanithermXN glazing
    constrainedby Data.Parameter.Optimization201706.Glazings.PlanithermXN
    annotation (Placement(transformation(extent={{-18,266},{36,320}})));
  replaceable Data.Parameter.ParametricStudy201611.DrawingUp.ADEME_33   drawingUp_data
    constrainedby Data.Parameter.ParametricStudy201611.DrawingUp.ADEME_33
    annotation (Placement(transformation(extent={{-318,264},{-266,316}})));
  replaceable
    Data.Parameter.ParametricStudy201611.Instructions.Consigne19_hygroB instructions_data
    constrainedby
    Data.Parameter.ParametricStudy201611.Instructions.Consigne19_hygroB
    annotation (Placement(transformation(extent={{-418,264},{-366,316}})));
  Buildings.Fluid.Storage.StratifiedEnhanced DHW_Tank(
    hTan=1.67*DHW_Tank.VTan/0.4,
    dIns=DHWInsulationThickness,
    kIns=lambdaTank,
    nSeg=20,
    T_start=T_start,
    tau=120,
    redeclare package Medium = MediumDHW,
    m_flow_nominal=0,
    VTan(displayUnit="l") = 0.3)
    annotation (Placement(transformation(extent={{-444,-10},{-364,70}})));
  Valves.DrawingUp.Drawing_up_coef Drawing_up(
    redeclare package Medium = MediumDHW,
    table=drawingUp_data.schedule,
    coef_schedules=drawingUp_coefs.schedule) annotation (__Dymola_tag={"Energy",
        "Power","DHW","Pump","flow"}, Placement(transformation(extent={{-454,124},
            {-484,92}})));
public
  Buildings.Fluid.Sensors.TemperatureTwoPort Tcold_water(
    redeclare package Medium = MediumDHW,
    m_flow_nominal=0.04,
    tau=10) annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(
        extent={{-6,-6},{6,6}},
        rotation=90,
        origin={-346,-26})));
  Buildings.Fluid.Sources.Boundary_pT PipeNetwork_Source(
    nPorts=1,
    use_T_in=true,
    redeclare package Medium = MediumDHW,
    T=283.15) annotation (__Dymola_tag={"Temperature"}, Placement(transformation(
        extent={{6,-6},{-6,6}},
        rotation=-90,
        origin={-364,-52})));
protected
  Modelica.Blocks.Sources.CombiTimeTable PipeNetwork_Temp(extrapolation=
        Modelica.Blocks.Types.Extrapolation.Periodic, table=weather_file.water,
    smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments)
    "Cold water temperature according to month of the year"
    annotation (Placement(transformation(extent={{6,-6},{-6,6}},
        rotation=-90,
        origin={-362,-80})));
protected
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Weather(
      computeWetBulbTemperature=false,
    filNam=weather_file.path) "Weather datas"
    annotation (Placement(transformation(extent={{-492,316},{-562,372}})));
public
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (__Dymola_tag={"Power", "Weather"}, Placement(transformation(extent={{-570,
            -68},{-542,-42}})));
  Modelica.Blocks.Math.Product to_mass_flowRate_outside
    "Volumic to mass flow rate" annotation (Placement(transformation(
        extent={{-4,-4},{4,4}},
        rotation=-90,
        origin={-114,-92})));
public
  Buildings.Fluid.FixedResistances.FixedResistanceDpM Resistance_House(
    redeclare package Medium = MediumA,
    allowFlowReversal=false,
    linearized=true,
    from_dp=true,
    dp_nominal=100,
    m_flow_nominal=0.03 + 0.4*(26.4*4 + 8.6 + floor_area)/3600)
    annotation (Placement(transformation(extent={{-20,0},{-6,12}})));
  Utilities.Other.DHWTankConvection
                                 tankConvection(A={Modelica.Constants.pi*2*
        DHW_r*DHW_Tank.hTan,Modelica.Constants.pi*DHW_r*DHW_r})
    annotation (__Dymola_tag={"Energy", "Losses"}, Placement(transformation(extent={{-450,
            182},{-376,214}})));
  Control.Electrical_control.ReferenceControl_28 algorithm_control(tableOnFile=false,
      table=instructions_data.schedule)
    annotation (__Dymola_tag={"algo","Consignesouff"}, Placement(transformation(extent={{-836,16},{-640,158}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T4
    annotation (Placement(transformation(extent={{-348,74},{-334,88}})));
public
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T3
    annotation (Placement(transformation(extent={{-338,38},{-324,52}})));
public
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow Elec_to_DHW
    "Prescribed heat flow for heating and cooling"
    annotation (Placement(transformation(extent={{-516,6},{-494,28}})));
equation
  connect(DHW_Tank.port_a, Drawing_up.tank_port) annotation (Line(points={{-444,
          30},{-450,30},{-450,107.2},{-455.5,107.2}}, color={0,127,255}));
  connect(Tcold_water.port_b, DHW_Tank.port_b) annotation (Line(points={{-346,
          -20},{-364,-20},{-364,30}},
                                 color={0,127,255}));
  connect(PipeNetwork_Source.ports[1], Tcold_water.port_a)
    annotation (Line(points={{-364,-46},{-364,-40},{-364,-32},{-346,-32}},
                                                     color={0,127,255}));
  connect(PipeNetwork_Temp.y[1], PipeNetwork_Source.T_in) annotation (Line(
        points={{-362,-73.4},{-362,-59.2},{-361.6,-59.2}}, color={0,0,127}));
  connect(Weather.weaBus, weaBus) annotation (Line(
      points={{-562,344},{-562,344},{-600,344},{-600,-54},{-556,-54},{-556,-55}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(weaBus.pAtm, x_pTphi.p_in) annotation (Line(
      points={{-556,-55},{-556,-140},{-170,-140}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.TDryBul, x_pTphi.T) annotation (Line(
      points={{-556,-55},{-556,-55},{-556,-176},{-200,-176},{-200,-160},{-200,-146},
          {-170,-146}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.relHum, x_pTphi.phi) annotation (Line(
      points={{-556,-55},{-556,-55},{-556,-152},{-170,-152}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(x_pTphi.X[1], toTotalAir.XiDry) annotation (Line(points={{-147,-146},{
          -135,-146},{-135,-146}}, color={0,0,127}));
  connect(toTotalAir.XNonVapor, Outside.X_in[2]) annotation (Line(points={{-113,
          -150},{-108,-150},{-108,-119.8},{-128.6,-119.8}}, color={0,0,127}));
  connect(toTotalAir.XNonVapor, Infiltrations.X_in[2]) annotation (Line(points={
          {-113,-150},{-82,-150},{-52,-150},{-52,-51.8},{-41.4,-51.8}}, color={0,
          0,127}));
  connect(toTotalAir.XiTotalAir, Outside.X_in[1]) annotation (Line(points={{-113,
          -146},{-100,-146},{-100,-119.8},{-128.6,-119.8}}, color={0,0,127}));
  connect(toTotalAir.XiTotalAir, Infiltrations.X_in[1]) annotation (Line(points=
         {{-113,-146},{-84,-146},{-60,-146},{-60,-51.8},{-41.4,-51.8}}, color={0,
          0,127}));
  connect(Outside.ports[1], Outside_air_flow.port_a) annotation (Line(points={{-144,
          -117},{-148,-117},{-148,-116},{-148,-68},{-152,-68}}, color={0,127,255}));
  connect(Outside_air_flow.port_b, mixing_box.ports[1]) annotation (Line(points={{-152,
          -56},{-152,-22},{-147,-22}},           color={0,127,255}));
  connect(density_mix.port, mixing_box.ports[2]) annotation (Line(points={{-122,-14},
          {-122,-14},{-122,-26},{-149,-26},{-149,-22}},      color={0,127,255}));
  connect(souInf_House.ports[3], density_outside.port) annotation (Line(points={
          {-58,6},{-52,6},{-52,-12},{-90,-12}}, color={0,127,255}));
  connect(density_outside.d, ToMass_House.u2) annotation (Line(points={{-96.6,-18},
          {-102,-18},{-102,-18},{-102,28.8},{-93.4,28.8}}, color={0,0,127}));
  connect(InfiltrationRate_House.y, ToMass_House.u1) annotation (Line(points={{-133,
          42},{-112,42},{-112,37.2},{-93.4,37.2}}, color={0,0,127}));
  connect(density_mix.d, to_mass_flowRate_mix.u1) annotation (Line(points={{-128.6,
          -8},{-136,-8},{-136,16},{-199.6,16},{-199.6,4.8}}, color={0,0,127}));
  connect(mixing_box.ports[3], fan.port_a) annotation (Line(points={{-151,-22},{-151,
          -22},{-192,-22}},           color={0,127,255}));
  connect(fan.port_b, Tbefore_hex_stable.port_a) annotation (Line(points={{-212,
          -22},{-232,-22},{-232,-22}}, color={0,127,255}));
  connect(weaBus.TDryBul, Outside.T_in) annotation (Line(
      points={{-556,-55},{-556,-55},{-556,-176},{-76,-176},{-76,-114.2},{-128.6,
          -114.2}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.TDryBul, Infiltrations.T_in) annotation (Line(
      points={{-556,-55},{-556,-176},{-76,-176},{-76,-46.2},{-41.4,-46.2}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, souInf_House.weaBus) annotation (Line(
      points={{-556,-55},{-556,-55},{-556,-180},{-80,-180},{-80,6.12},{-70,6.12}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));

  connect(weaBus.TDryBul, Toutside.T) annotation (Line(
      points={{-556,-55},{-556,-55},{-556,-176},{116,-176},{116,4},{101.6,4}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, House.weaBus) annotation (Line(
      points={{-556,-55},{-556,-180},{100,-180},{100,-28},{74,-28},{74,32.425},{
          46.425,32.425}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, VideSanitaire.weaBus) annotation (Line(
      points={{-556,-55},{-556,-55},{-556,-180},{100,-180},{100,-78.1},{33.9,-78.1}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));

  connect(weaBus, Combles.weaBus) annotation (Line(
      points={{-556,-55},{-556,-180},{100,-180},{100,-28},{74,-28},{74,144.005},
          {34.005,144.005}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(ToMass_House.y, Infiltrations.m_flow_in) annotation (Line(points={{-77.3,
          33},{-48,33},{-48,-43.4},{-40,-43.4}}, color={0,0,127}));
  connect(to_mass_flowRate_outside.y, Outside.m_flow_in) annotation (Line(
        points={{-114,-96.4},{-114,-111.4},{-130,-111.4}}, color={0,0,127}));
  connect(density_outside.d, to_mass_flowRate_outside.u1) annotation (Line(
        points={{-96.6,-18},{-102,-18},{-102,-18},{-102,-82},{-111.6,-82},{-111.6,
          -87.2}}, color={0,0,127}));
  connect(to_mass_flowRate_mix.y, fan.m_flow_in) annotation (Line(points={{-202,
          -4.4},{-202,-10},{-201.8,-10}}, color={0,0,127}));
  connect(vol.ports[1], Tsoufflage_stable.port_a) annotation (Line(points={{-252,
          150},{-252,150},{-252,18}},           color={0,127,255}));
  connect(Tsoufflage_stable.port_b, House.ports[1]) annotation (Line(points={{-232,
          18},{8,18},{8,11.5},{21.75,11.5}}, color={0,127,255}));
  connect(Infiltrations.ports[1], House.ports[2]) annotation (Line(points={{-26,
          -49},{10,-49},{10,11.5},{21.75,11.5}}, color={0,127,255}));
  connect(House.ports[3], recycled_air_flow.port_a) annotation (Line(points={{21.75,
          11.5},{12.875,11.5},{12.875,-36},{-84,-36}}, color={0,127,255}));
  connect(Resistance_House.port_b, House.ports[4]) annotation (Line(points={{-6,
          6},{10,6},{10,11.5},{21.75,11.5}}, color={0,127,255}));
  connect(souInf_House.ports[4], Resistance_House.port_a)
    annotation (Line(points={{-58,6},{-22,6},{-20,6}}, color={0,127,255}));
  connect(DHW_Tank.heaPorTop, tankConvection.port_a[1]) annotation (Line(points=
         {{-396,59.6},{-396,152},{-474,152},{-474,197.2},{-448.52,197.2}},
        color={191,0,0}));
  connect(DHW_Tank.heaPorSid, tankConvection.port_a[2]) annotation (Line(points=
         {{-381.6,30},{-378,30},{-378,152},{-474,152},{-474,198.8},{-448.52,198.8}},
        color={191,0,0}));
  connect(Elec_to_air.port, vol.heatPort) annotation (Line(points={{-282,186},{-272,
          186},{-272,160},{-260,160}}, color={191,0,0}));
  connect(vol.heatPort, Tsoufflage.port) annotation (Line(points={{-260,160},{-272,
          160},{-284,160}}, color={191,0,0}));
  connect(Elec_to_DHW.port, DHW_Tank.heaPorVol[3]) annotation (Line(points={{-494,
          17},{-430,17},{-430,28.2},{-404,28.2}}, color={191,0,0}));
  connect(Elec_to_DHW.port, DHW_Tank.heaPorVol[4]) annotation (Line(points={{-494,
          17},{-428,17},{-428,28.44},{-404,28.44}}, color={191,0,0}));
  connect(Elec_to_DHW.port, DHW_Tank.heaPorVol[5]) annotation (Line(points={{-494,
          17},{-426,17},{-426,28.68},{-404,28.68}}, color={191,0,0}));
  connect(DHW_Tank.heaPorVol[4], T4.port) annotation (Line(points={{-404,28.44},
          {-404,28.44},{-404,14},{-348,14},{-348,81}},           color={191,0,0}));
  connect(DHW_Tank.heaPorVol[12], T3.port) annotation (Line(points={{-404,30.36},
          {-396,30.36},{-396,20},{-348,20},{-348,45},{-338,45}},
                                       color={191,0,0}));
  connect(Tbefore_hex_stable.port_b, vol.ports[2]) annotation (Line(points={{-252,
          -22},{-268,-22},{-276,-22},{-276,150},{-248,150}}, color={0,127,255}));
  connect(algorithm_control.fan_flowRate, to_mass_flowRate_mix.u2) annotation (
      Line(points={{-637.937,135.409},{-552,135.409},{-552,202},{-552,228},{
          -204.4,228},{-204.4,4.8}},
                              color={0,0,127}));
  connect(algorithm_control.minimal_flowRate, to_mass_flowRate_outside.u2)
    annotation (Line(points={{-637.937,126.373},{-552,126.373},{-552,228},{
          -116.4,228},{-116.4,-87.2}},
                                color={0,0,127}));
  connect(algorithm_control.power_DHW_elec, Elec_to_DHW.Q_flow) annotation (Line(
        points={{-640,90.8727},{-614,90.8727},{-614,92},{-532,92},{-532,17},{
          -516,17}},
                color={0,0,127}));
  connect(algorithm_control.power_air_elec, Elec_to_air.Q_flow) annotation (Line(
        points={{-637.937,41.1727},{-584,41.1727},{-584,240},{-316,240},{-316,
          186},{-298,186}},
                       color={0,0,127}));
  connect(T3.T, algorithm_control.T3) annotation (Line(points={{-324,45},{-296,
          45},{-296,-116},{-870,-116},{-870,134.764},{-831.874,134.764}},
                                                                      color={0,0,
          127}));
  connect(T4.T, algorithm_control.T4) annotation (Line(points={{-334,81},{-328,
          81},{-328,82},{-320,82},{-320,-110},{-862,-110},{-862,117.336},{
          -831.874,117.336}},
        color={0,0,127}));
  connect(weaBus.TDryBul, algorithm_control.Text) annotation (Line(
      points={{-556,-55},{-854,-55},{-854,82.4818},{-832.905,82.4818}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(TRooAir.T, algorithm_control.Tamb) annotation (Line(points={{148,-14},
          {210,-14},{210,-214},{-850,-214},{-850,65.3773},{-832.389,65.3773}},
        color={0,0,127}));
  connect(Tsoufflage.T, algorithm_control.Tsouff) annotation (Line(points={{-300,
          160},{-308,160},{-308,-138},{-831.874,-138},{-831.874,28.2636}}, color=
         {0,0,127}));
  connect(tankConvection.port_b, House.heaPorAir) annotation (Line(points={{
          -377.48,198},{-170,198},{32.25,198},{32.25,19}}, color={191,0,0}));
  connect(fan.heatPort, Tbefore_hex.port) annotation (Line(points={{-202,-28.8},
          {-186,-28.8},{-186,-52},{-194,-52}}, color={191,0,0}));
  connect(Tcold_water.T, Drawing_up.Te) annotation (Line(points={{-352.6,-26},{
          -368,-26},{-368,-34},{-390,-34},{-390,-52},{-472,-52},{-472,132},{
          -440,132},{-440,120},{-455.5,120}}, color={0,0,127}));
  connect(recycled_air_flow.port_b, mixing_box.ports[4]) annotation (Line(points=
         {{-96,-36},{-122,-36},{-153,-36},{-153,-22}}, color={0,127,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-880,-180},
            {260,420}})),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-880,-180},{260,420}})),
    experiment(
      StartTime=2.26368e+007,
      StopTime=4.1904e+007,
      Interval=180,
      Tolerance=1e-006,
      __Dymola_Algorithm="Cvode"),
    __Dymola_experimentSetupOutput(doublePrecision=true),
    __Dymola_experimentFlags(
      Advanced(GenerateVariableDependencies=false, OutputModelicaCode=false),
      Evaluate=true,
      OutputCPUtime=true,
      OutputFlatModelica=false));
end ReferenceSystem_28;
