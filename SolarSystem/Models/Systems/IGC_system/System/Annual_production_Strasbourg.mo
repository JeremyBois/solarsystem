within SolarSystem.Models.Systems.IGC_system.System;
model Annual_production_Strasbourg
  "Compute PV production based on thermal collector with pv number as input"

  import SI = Modelica.SIunits;

protected
  parameter SI.Area floor_area = 98.4 "Floor area of the house";
  parameter SI.Area PV_unit_area = 0.95 * 1.62 "Sillia Serie 60M 260, internal dimensions." annotation (Dialog(group="PV"));

  // Thermal collector inputs
public
  parameter Integer collectorNb = 0 "Number of collector used" annotation (Dialog(group="Thermal collector"));
  parameter SI.Distance collectorHeight = 0 "Collector height" annotation (Dialog(group="Thermal collector"));
  parameter SI.Distance collectorWidth = 0 "Collector height" annotation (Dialog(group="Thermal collector"));
  parameter Integer collectorMax = 0 "Collector max number. Used to define max area per orientation" annotation (Dialog(group="Thermal collector"));
  parameter SI.Area collectorArea = collectorHeight*collectorWidth "Collector brut area. Used to define max area per orientation" annotation (Dialog(group="Thermal collector"));

  // PV inputs
public
  parameter Integer PV_nb = 0 "Number of PV." annotation (Dialog(group="PV"));
  parameter Integer PV_max = 0 "Number of PV maximum for East OR West roof." annotation (Dialog(group="PV"));
  parameter SI.Area PV_area = PV_nb * PV_unit_area "PV total area." annotation (Dialog(group="PV"));
  // PV inputs fixed
  parameter Real fAct=0.9 "Fraction of surface area with active solar cells" annotation (Dialog(group="PV"));
  parameter Real eta=0.1582 "Module conversion efficiency" annotation (Dialog(group="PV"));
  parameter Real pf=1 "Power factor required in EU countries" annotation (Dialog(group="PV"));
  parameter Real eta_DCAC=0.954 "Efficiency of DC/AC conversion" annotation (Dialog(group="PV"));

protected
  parameter SI.Angle til = 0.94247779607694 "Surface tilt (54�)" annotation (Dialog(group="PV"));
  parameter SI.Angle lat = weather_file.lattitude "Latitude" annotation (Dialog(group="PV"));

  // Thermal computation
  // Allows thermal collector in south and west.
public
  parameter SI.Area maxArea = collectorMax * collectorArea "Maximum number of thermal collector for west and south";
  parameter SI.Area collectorMaxArea = min(maxArea, collectorNb * collectorArea) "Thermal collector area in the south roof";

protected
  parameter SI.Area maxAreaEastWest = PV_max * PV_unit_area "Maximum number of thermal collector for east (velux)";

  // max used to avoid negative value
  // min used to make sure we cannot add more PV than allowed
  parameter SI.Area PV_south = max(0, min(PV_area,
                                          PV_unit_area * integer((maxArea - collectorMaxArea) / PV_unit_area))) "Make sure to maximize south area";
  parameter SI.Area PV_west = max(0, min(maxAreaEastWest, PV_area - PV_south)) "All roof part are the same";
  parameter SI.Area PV_east = max(0, min(maxAreaEastWest, PV_area - PV_south - PV_west));

protected
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Weather(
      computeWetBulbTemperature=false,
    filNam=weather_file.path) "Weather datas"
    annotation (Placement(transformation(extent={{100,72},{68,100}})));
public
  Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
                                                            Consumer(
    P_nominal=1000,
    V_nominal=230,
    mode=Buildings.Electrical.Types.Load.FixedZ_steady_state)
    "Consumes the power generted by the PVs or grid if PVs not enough"
    annotation (Placement(transformation(extent={{68,-44},{88,-24}})));
protected
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid
                                                         grid(f=50, V=230)
    "Electrical grid model"
    annotation (Placement(transformation(extent={{26,-38},{50,-62}})));
public
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.PVSimpleOriented
                                                                     pv_WEST(
    lat=lat,
    pf=pf,
    eta_DCAC=eta_DCAC,
    fAct=fAct,
    eta=eta,
    til=til,
    A=PV_west,
    azi=1.5707963267949,
    V_nominal=230) "PV array oriented"
    annotation (Placement(transformation(extent={{8,0},{28,20}})));
protected
  Modelica.Blocks.Continuous.Integrator pv_production(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{72,4},{84,16}})));
public
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.PVSimpleOriented
                                                                     pv_EAST(
    lat=lat,
    pf=pf,
    eta_DCAC=eta_DCAC,
    fAct=fAct,
    eta=eta,
    til=til,
    A=PV_east,
    V_nominal=230,
    azi=-1.5707963267949) "PV array oriented East"
    annotation (Placement(transformation(extent={{8,-30},{28,-10}})));
  Buildings.Electrical.AC.ThreePhasesBalanced.Sources.PVSimpleOriented
                                                                     pv_SOUTH(
    azi=0,
    lat=lat,
    pf=pf,
    eta_DCAC=eta_DCAC,
    fAct=fAct,
    eta=eta,
    A=PV_south,
    V_nominal=230,
    til(displayUnit="rad") = til)
                   "PV array oriented South"
    annotation (Placement(transformation(extent={{8,30},{28,50}})));
protected
  Modelica.Blocks.Math.Sum sum_gains1(nin=3, k={1,1,1})
    annotation (__Dymola_tag={"Power", "Internal"}, Placement(transformation(extent={{46,4},{
            58,16}})));
public
  Buildings.BoundaryConditions.WeatherData.Bus
                                     weaBus "Weather data" annotation (
     Placement(transformation(extent={{32,60},{60,86}}),   iconTransformation(
          extent={{-10,88},{10,108}})));
  Modelica.Blocks.Interfaces.RealOutput PV_Energy "PVs total solar production" annotation (__Dymola_tag={"Solar", "Energy"},
      Placement(transformation(extent={{96,-4},{124,24}}), iconTransformation(extent={{80,40},{120,
            80}})));
protected
  replaceable Data.Parameter.ParametricStudy201611.Weathers.Strasbourg
                                                 weather_file constrainedby
    Data.Parameter.City_Datas.City_Data
    annotation (Placement(transformation(extent={{-138,80},{-120,98}})));

public
  Modelica.Blocks.Sources.CombiTimeTable schedules(
    extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
    smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
    table=schedule_table)
    "1 = Occupation, 2 = lumi�res, 3 = equipements, 4=occultation hiver, 5=occultation ete est, 6=occultation ete ouest"
    annotation (Placement(transformation(extent={{-140,-10},{-120,10}})));
protected
  Modelica.Blocks.Math.Gain Coef[3](k={1,floor_area,floor_area})
    "Gain for electrical internal loads"
    annotation (Placement(transformation(extent={{-110,-4},{-102,4}})));
public
  Modelica.Blocks.Math.Sum sum(nin=2) annotation (__Dymola_tag={"Power",
        "Internal"}, Placement(transformation(extent={{-72,-6},{-60,6}})));
public
  Modelica.Blocks.Continuous.Integrator ConsoElectrique(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J"))
    annotation (Placement(transformation(extent={{-46,-8},{-30,8}})));
public
  Modelica.Blocks.Continuous.Integrator ConsoEclairage(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J"))
    annotation (Placement(transformation(extent={{-74,-36},{-58,-20}})));
public
  Modelica.Blocks.Continuous.Integrator ConsoElectromenager(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J"))
    annotation (Placement(transformation(extent={{-74,-62},{-58,-46}})));
public
  Modelica.Blocks.Continuous.Integrator ProdOccupants(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J"))
    annotation (Placement(transformation(extent={{-76,20},{-60,36}})));
  parameter Real schedule_table[:,:]=[0,390,0,1.14,1,1,1; 3600,390,0,1.14,1,1,1; 7200,390,
      0,1.14,1,1,1; 10800,390,0,1.14,1,1,1; 14400,390,0,1.14,1,1,1; 18000,390,0,
      1.14,1,1,1; 21600,390,0,5.7,1,1,1; 25200,390,1.4,5.7,0,0,0; 28800,390,1.4,
      5.7,0,1,0.7; 32400,390,0,5.7,0,1,0.7; 36000,0,0,1.14,0,1,0.7; 39600,0,0,1.14,
      0,1,0.7; 43200,0,0,1.14,0,1,0.7; 46800,0,0,1.14,0,0.7,1; 50400,0,0,1.14,0,
      0.7,1; 54000,0,0,1.14,0,0.7,1; 57600,0,0,1.14,0,0.7,1; 61200,0,0,1.14,0,0.7,
      1; 64800,390,0,5.7,0,0,0; 68400,390,1.4,5.7,0,0,0; 72000,390,1.4,5.7,0,0,0;
      75600,390,1.4,5.7,0,0,0; 79200,390,0,1.14,1,1,1; 82800,390,0,1.14,1,1,1; 86400,
      390,0,1.14,1,1,1; 90000,390,0,1.14,1,1,1; 93600,390,0,1.14,1,1,1; 97200,390,
      0,1.14,1,1,1; 100800,390,0,1.14,1,1,1; 104400,390,0,1.14,1,1,1; 108000,390,
      0,5.7,1,1,1; 111600,390,1.4,5.7,0,0,0; 115200,390,1.4,5.7,0,1,0.7; 118800,
      390,0,5.7,0,1,0.7; 122400,0,0,1.14,0,1,0.7; 126000,0,0,1.14,0,1,0.7; 129600,
      0,0,1.14,0,1,0.7; 133200,0,0,1.14,0,0.7,1; 136800,0,0,1.14,0,0.7,1; 140400,
      0,0,1.14,0,0.7,1; 144000,0,0,1.14,0,0.7,1; 147600,0,0,1.14,0,0.7,1; 151200,
      390,0,5.7,0,0,0; 154800,390,1.4,5.7,0,0,0; 158400,390,1.4,5.7,0,0,0; 162000,
      390,1.4,5.7,0,0,0; 165600,390,0,1.14,1,1,1; 169200,390,0,1.14,1,1,1; 172800,
      390,0,1.14,1,1,1; 176400,390,0,1.14,1,1,1; 180000,390,0,1.14,1,1,1; 183600,
      390,0,1.14,1,1,1; 187200,390,0,1.14,1,1,1; 190800,390,0,1.14,1,1,1; 194400,
      390,0,5.7,1,1,1; 198000,390,1.4,5.7,0,0,0; 201600,390,1.4,5.7,0,1,0.7; 205200,
      390,0,5.7,0,1,0.7; 208800,0,0,1.14,0,1,0.7; 212400,0,0,1.14,0,1,0.7; 216000,
      0,0,1.14,0,1,0.7; 219600,0,0,1.14,0,0.7,1; 223200,390,0,5.7,0,0.7,1; 226800,
      390,0,5.7,0,0.7,1; 230400,390,0,5.7,0,0.7,1; 234000,390,0,5.7,0,0.7,1; 237600,
      390,0,5.7,0,0,0; 241200,390,1.4,5.7,0,0,0; 244800,390,1.4,5.7,0,0,0; 248400,
      390,1.4,5.7,0,0,0; 252000,390,0,1.14,1,1,1; 255600,390,0,1.14,1,1,1; 259200,
      390,0,1.14,1,1,1; 262800,390,0,1.14,1,1,1; 266400,390,0,1.14,1,1,1; 270000,
      390,0,1.14,1,1,1; 273600,390,0,1.14,1,1,1; 277200,390,0,1.14,1,1,1; 280800,
      390,0,5.7,1,1,1; 284400,390,1.4,5.7,0,0,0; 288000,390,1.4,5.7,0,1,0.7; 291600,
      390,0,5.7,0,1,0.7; 295200,0,0,1.14,0,1,0.7; 298800,0,0,1.14,0,1,0.7; 302400,
      0,0,1.14,0,1,0.7; 306000,0,0,1.14,0,0.7,1; 309600,0,0,1.14,0,0.7,1; 313200,
      0,0,1.14,0,0.7,1; 316800,0,0,1.14,0,0.7,1; 320400,0,0,1.14,0,0.7,1; 324000,
      390,0,5.7,0,0,0; 327600,390,1.4,5.7,0,0,0; 331200,390,1.4,5.7,0,0,0; 334800,
      390,1.4,5.7,0,0,0; 338400,390,0,1.14,1,1,1; 342000,390,0,1.14,1,1,1; 345600,
      390,0,1.14,1,1,1; 349200,390,0,1.14,1,1,1; 352800,390,0,1.14,1,1,1; 356400,
      390,0,1.14,1,1,1; 360000,390,0,1.14,1,1,1; 363600,390,0,1.14,1,1,1; 367200,
      390,0,5.7,1,1,1; 370800,390,1.4,5.7,0,0,0; 374400,390,1.4,5.7,0,1,0.7; 378000,
      390,0,5.7,0,1,0.7; 381600,0,0,1.14,0,1,0.7; 385200,0,0,1.14,0,1,0.7; 388800,
      0,0,1.14,0,1,0.7; 392400,0,0,1.14,0,0.7,1; 396000,0,0,1.14,0,0.7,1; 399600,
      0,0,1.14,0,0.7,1; 403200,0,0,1.14,0,0.7,1; 406800,0,0,1.14,0,0.7,1; 410400,
      390,0,5.7,0,0,0; 414000,390,1.4,5.7,0,0,0; 417600,390,1.4,5.7,0,0,0; 421200,
      390,1.4,5.7,0,0,0; 424800,390,0,1.14,1,1,1; 428400,390,0,1.14,1,1,1; 432000,
      390,0,1.14,1,1,1; 435600,390,0,1.14,1,1,1; 439200,390,0,1.14,1,1,1; 442800,
      390,0,1.14,1,1,1; 446400,390,0,1.14,1,1,1; 450000,390,0,1.14,1,1,1; 453600,
      390,1.4,5.7,1,1,1; 457200,390,1.4,5.7,0,0,0; 460800,390,0.7,5.7,0,1,0.7; 464400,
      390,0.7,5.7,0,1,0.7; 468000,390,0.7,5.7,0,1,0.7; 471600,390,0.7,5.7,0,1,0.7;
      475200,390,0.7,5.7,0,1,0.7; 478800,390,0.7,5.7,0,0.7,1; 482400,390,0.7,5.7,
      0,0.7,1; 486000,390,0.7,5.7,0,0.7,1; 489600,390,0.7,5.7,0,0.7,1; 493200,390,
      0.7,5.7,0,0.7,1; 496800,390,0.7,5.7,0,0,0; 500400,390,1.4,5.7,0,0,0; 504000,
      390,1.4,5.7,0,0,0; 507600,390,1.4,5.7,0,0,0; 511200,390,0,1.14,1,1,1; 514800,
      390,0,1.14,1,1,1; 518400,390,0,1.14,1,1,1; 522000,390,0,1.14,1,1,1; 525600,
      390,0,1.14,1,1,1; 529200,390,0,1.14,1,1,1; 532800,390,0,1.14,1,1,1; 536400,
      390,0,1.14,1,1,1; 540000,390,1.4,5.7,1,1,1; 543600,390,1.4,5.7,0,0,0; 547200,
      390,0.7,5.7,0,1,0.7; 550800,390,0.7,5.7,0,1,0.7; 554400,390,0.7,5.7,0,1,0.7;
      558000,390,0.7,5.7,0,1,0.7; 561600,390,0.7,5.7,0,1,0.7; 565200,390,0.7,5.7,
      0,0.7,1; 568800,390,0.7,5.7,0,0.7,1; 572400,390,0.7,5.7,0,0.7,1; 576000,390,
      0.7,5.7,0,0.7,1; 579600,390,0.7,5.7,0,0.7,1; 583200,390,0.7,5.7,0,0,0; 586800,
      390,1.4,5.7,0,0,0; 590400,390,1.4,5.7,0,0,0; 594000,390,1.4,5.7,0,0,0; 597600,
      390,0,1.14,1,1,1; 601200,390,0,1.14,1,1,1; 604800,390,0,1.14,1,1,1]
    "Table matrix (time = first column; e.g., table=[0,2])";
  Modelica.Blocks.Interfaces.RealOutput Occupants_Energy
    "Inhabitants total loads" annotation (__Dymola_tag={"Solar","Energy"},
      Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={-94,94}), iconTransformation(extent={{80,40},{120,80}})));
  Modelica.Blocks.Interfaces.RealOutput ApportElectrique_Energy
    "Electric total consumption" annotation (__Dymola_tag={"Solar","Energy"},
      Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={-70,94}), iconTransformation(extent={{80,40},{120,80}})));
  Modelica.Blocks.Interfaces.RealOutput Eclairage_Energy
    "Lights total consumption" annotation (__Dymola_tag={"Solar","Energy"},
      Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={-48,94}), iconTransformation(extent={{80,40},{120,80}})));
  Modelica.Blocks.Interfaces.RealOutput Electromenager_Energy
    "Equipments total consumption" annotation (__Dymola_tag={"Solar","Energy"},
      Placement(transformation(
        extent={{-14,-14},{14,14}},
        rotation=90,
        origin={-24,94}), iconTransformation(extent={{80,40},{120,80}})));
equation
  connect(grid.terminal,Consumer. terminal) annotation (Line(
      points={{38,-38},{38,-34},{68,-34}},
      color={0,120,120},
      smooth=Smooth.None));
  connect(pv_SOUTH.P,sum_gains1. u[1]) annotation (Line(points={{29,47},{40,47},
          {40,9.2},{44.8,9.2}},   color={0,0,127}));
  connect(pv_WEST.P,sum_gains1. u[2]) annotation (Line(points={{29,17},{40,17},{
          40,10},{44.8,10}},  color={0,0,127}));
  connect(pv_EAST.P,sum_gains1. u[3]) annotation (Line(points={{29,-13},{40,-13},
          {40,10.8},{44.8,10.8}}, color={0,0,127}));
  connect(sum_gains1.y,pv_production. u)
    annotation (Line(points={{58.6,10},{58.6,10},{70.8,10}},
                                                   color={0,0,127}));
  connect(pv_SOUTH.terminal,Consumer. terminal)
    annotation (Line(points={{8,40},{-8,40},{-8,-34},{68,-34}},     color={0,120,120}));
  connect(pv_WEST.terminal,Consumer. terminal)
    annotation (Line(points={{8,10},{-4,10},{-4,-34},{68,-34}},   color={0,120,120}));
  connect(pv_EAST.terminal,Consumer. terminal) annotation (Line(points={{8,-20},
          {0,-20},{0,-34},{68,-34}},
                     color={0,120,120}));
  connect(pv_production.y,PV_Energy)
    annotation (Line(points={{84.6,10},{88,10},{110,10}},       color={0,0,127}));
  connect(weaBus,pv_EAST. weaBus) annotation (Line(
      points={{46,73},{48,73},{48,64},{0,64},{0,-6},{18,-6},{18,-11}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus,pv_WEST. weaBus) annotation (Line(
      points={{46,73},{48,73},{48,64},{0,64},{0,24},{18,24},{18,19}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus,pv_SOUTH. weaBus) annotation (Line(
      points={{46,73},{48,73},{48,64},{0,64},{0,56},{18,56},{18,49}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(Weather.weaBus, weaBus) annotation (Line(
      points={{68,86},{46,86},{46,73},{46,73}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(sum.y, ConsoElectrique.u)
    annotation (Line(points={{-59.4,0},{-47.6,0}},           color={0,0,127}));
  connect(Coef[1].y, ProdOccupants.u) annotation (Line(points={{-101.6,0},{-86,
          0},{-86,28},{-77.6,28}},
                                color={0,0,127}));
  connect(Coef[2].y, ConsoEclairage.u) annotation (Line(points={{-101.6,0},{-86,
          0},{-86,-28},{-75.6,-28}},
                                  color={0,0,127}));
  connect(Coef[3].y, ConsoElectromenager.u) annotation (Line(points={{-101.6,0},
          {-86,0},{-86,-54},{-75.6,-54}},color={0,0,127}));
  connect(schedules.y[1], Coef[1].u) annotation (Line(points={{-119,0},{-114,0},
          {-110.8,0}}, color={0,0,127}));
  connect(schedules.y[2], Coef[2].u) annotation (Line(points={{-119,0},{-114,0},
          {-110.8,0}}, color={0,0,127}));
  connect(schedules.y[3], Coef[3].u) annotation (Line(points={{-119,0},{-114,0},
          {-110.8,0}}, color={0,0,127}));
  connect(Coef[2].y, sum.u[1]) annotation (Line(points={{-101.6,0},{-73.2,0},{
          -73.2,-0.6}},
                  color={0,0,127}));
  connect(Coef[3].y, sum.u[2]) annotation (Line(points={{-101.6,0},{-73.2,0},{
          -73.2,0.6}},
                 color={0,0,127}));
  connect(ProdOccupants.y, Occupants_Energy) annotation (Line(points={{-59.2,28},
          {-56,28},{-56,54},{-94,54},{-94,94}}, color={0,0,127}));
  connect(ConsoElectrique.y, ApportElectrique_Energy) annotation (Line(points={{
          -29.2,0},{-28,0},{-28,58},{-70,58},{-70,94}}, color={0,0,127}));
  connect(ConsoEclairage.y, Eclairage_Energy) annotation (Line(points={{-57.2,
          -28},{-40,-28},{-22,-28},{-22,64},{-48,64},{-48,94}}, color={0,0,127}));
  connect(ConsoElectromenager.y, Electromenager_Energy) annotation (Line(points=
         {{-57.2,-54},{-18,-54},{-18,72},{-24,72},{-24,94}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-140,-100},
            {100,100}})),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false, extent={{-140,-100},{100,100}}),
        graphics={Text(
          extent={{-130,-68},{-32,-84}},
          lineColor={28,108,200},
          textString="House loads",
          textStyle={TextStyle.Bold}), Text(
          extent={{-4,-68},{94,-84}},
          lineColor={28,108,200},
          textString="PV production",
          textStyle={TextStyle.Bold})}),
    Documentation(info="<html>
<p>Mode that compute the collector production according to PV area and Thermal collector area.</p>
<p><br>Collector area can only be added to South roof. </p>
<p><br>PV can only be added to South, West, and East.</p>
<p><br>PV surface on South roof depends on Collector area. PV surface on West and East depends on PV area.</p>
<p><br>A PV is added to a roof only if enough place to put it all. Avoid half of a PV in South and the rest in West ...</p>
<p><br>54&deg; is assumed for roof til</p>
</html>"));
end Annual_production_Strasbourg;
