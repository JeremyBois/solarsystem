within SolarSystem.Models.Systems.IGC_system.System;
model Variables_selection_monozone
  "Describe all the variables to keep for the output"

  annotation(__Dymola_selections={
             Selection(name="SELECTION", match={
             MatchVariable(name="*(T(\d|amb*|ext|exch_out*|souff*)|.T)", tag={"Temperature", "Computed"}, newName="Temperature.Computed.%path%"),
             MatchVariable(name="*T_in{1-2}", tag={"Temperature", "Computed"}, newName="Temperature.Computed.%path%"),
             MatchVariable(name="*((.y[{12}])|.T)", tag={"Temperature", "Consigne"}, newName="Temperature.Consigne.%path%"),
             MatchVariable(name="*temperature_instruction_souff", tag={"Consignesouff"}, newName="Temperature.Consigne.%path%"),
             MatchVariable(name="*", tag={"Pump", "state"}, newName="Pump.State.%path%"),
             MatchVariable(name="*", tag={"Pump", "speed"}, newName="Pump.Speed.%path%"),
             MatchVariable(name="((*m_flow_p|*m_flow_b)|(C\d.m_flow))", tag={"Pump", "flow"}, newName="Pump.MassFlow.%path%"),
             MatchVariable(name="*", tag={"Valve"}, newName="Valve.%path%"),
             MatchVariable(name="*((Collector.TotalPowerLost|TotalPowerGain)|.H|.weaBus.HDi*)", tag={"Power", "Solar"}, newName="Power.Solar.%path%"),
             MatchVariable(name="*", tag={"Power", "Electric"}, newName="Power.Electric.%path%"),
             MatchVariable(name="*integrator.u", tag={"Power", "DHW"}, newName="Power.DrawingUp.%path%"),
             MatchVariable(name="*", tag={"Power", "Internal"}, newName="Power.Internal.%path%"),
             MatchVariable(name="*(Cp_value|Cp_air|Tin|Tout|m_flow|Energy|integrator.y)", tag={"Energy", "Solar"}, newName="Energy.Solar.%path%"),
             MatchVariable(name="*", tag={"Energy", "Electric"}, newName="Energy.Electric.%path%"),
             MatchVariable(name="*Energy", tag={"Energy", "Losses"}, newName="Energy.Losses.%path%"),
             MatchVariable(name="*", tag={"Energy", "Internal"}, newName="Energy.Internal.%path%"),
             MatchVariable(name="*Energy", tag={"Energy", "DHW"}, newName="Energy.DrawingUp.%path%"),
             MatchVariable(name="*((.in|.)direct_solar_state)|*(need*)", tag={"algo"}, newName="Algorithm.State.%path%"),
             MatchVariable(name="(pv_nb)|(pv_area)|(Collector.nPanels)", newName="Other.%path%"),
             MatchVariable(name="(fan|Outside_air_flow|recycled_air_flow)*.m_flow", tag={"Fan", "flow"}, newName="Fan.MassFlow.%path%")})},
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{20,-200},{220,-20}}), graphics={Ellipse(
          extent={{34,-198},{210,-22}},
          lineColor={0,0,255},
          fillColor={0,127,127},
          fillPattern=FillPattern.Solid), Text(
          extent={{44,-58},{204,-162}},
          lineColor={0,0,0},
          textString="Variable selection
Monozone")}),
    Diagram(coordinateSystem(extent={{20,-200},{220,-20}}), graphics={Ellipse(
          extent={{80,-140},{160,-60}},
          lineColor={0,0,255},
          fillColor={0,127,127},
          fillPattern=FillPattern.Solid), Text(
          extent={{86,-66},{156,-136}},
          lineColor={0,0,0},
          textString="Variable selection
Monozone")}),
    Documentation(info="<html>
<h4>Mise &agrave; jour: 20160622</h4>
<p><span style=\"font-family: MS Shell Dlg 2;\">Ajout des variables d&rsquo;&eacute;tat pour:</span></p>
<ul>
<li><span style=\"font-family: MS Shell Dlg 2;\">&Eacute;tat du chauffage solaire (direct / indirect)</span></li>
<li><span style=\"font-family: MS Shell Dlg 2;\">Consigne de temp&eacute;rature de soufflage</span></li>
<li><span style=\"font-family: MS Shell Dlg 2;\">Tous les &eacute;tats commen&ccedil;ant par `need`</span></li>
</ul>
<p><br><b>Description</b></p>
<p>Selection of all element needed for post-treatment.</p>
<p>Only use regex match with tag to group output according to variable function.</p>
</html>"));

             // Temperatures
             // Pumps
             // Valves
             // Power
             // Energy
             // Fans

end Variables_selection_monozone;
