within SolarSystem.Models.Systems.IGC_system;
package Backup
  extends Modelica.Icons.InternalPackage;













  model IGC_airVectorRecyclePassive_solar15
    "Simulation of SolisArt system with air vector."
    import SolarSystem;
    extends SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive_old(
                     House(nPorts=3), souInf_House(nPorts=4),
      redeclare SolarSystem.Data.Parameter.City_Datas.Bordeaux weather_file);
    extends SolarSystem.Models.Systems.IGC_system.System.Variables_selection;

  // Systems parameters (public)
  public
    parameter Modelica.SIunits.MassFlowRate m_flow_nominal[2] = {40*Collector.nPanels*Collector.per.A/3600,
                                                                 70*Collector.nPanels*Collector.per.A/3600}
      "Mass flow rate for left pumps ([1] and right pumps [2]";
    parameter Integer collector_seg(min=1) = 20
      "Collector discretization used inside control algorithms";
    // Mediums
    package MediumE = SolarSystem.Media.WaterGlycol
      "Medium water model with glycol";
    package MediumDHW = Buildings.Media.Water "Medium water model";

  // Constraints (protected)
  protected
    parameter Modelica.Media.Interfaces.PartialMedium.Temperature T_start=289.55
      "Start value of temperature";
    parameter Modelica.SIunits.Pressure dpPip_nominal = 200
      "Pressure difference of pipe (without valve)";
    parameter Modelica.SIunits.Pressure dpValve_nominal=200
      "Nominal pressure drop of fully open valve, used if CvData=Buildings.Fluid.Types.CvTypes.OpPoint";
    parameter Modelica.SIunits.Pressure dp_nominal[2]={4000, 12252.4893}
      "Nominal pressure for left and right pumps";
    parameter Modelica.SIunits.SpecificHeatCapacity Cp_value=3608
      "Value of Real output";
    parameter Modelica.SIunits.Time tau=15
      "Time constant of fluid volume for nominal flow, used if dynamicBalance=true";
    parameter Real k_collector = Collector.per.dp_nominal / (m_flow_nominal[1] * m_flow_nominal[1])
      "Subtract from collector pressure losses (used to always have correct flow rate in each branch (unit=Pa.s-2 / Kg-2)";
    parameter Modelica.SIunits.Velocity v_nominal=0.3
      "Velocity at m_flow_nominal (used to compute default diameter)";
    // 1=V3V, 2=V2VC6, 3=V2VC5, 4=V2VC2
    parameter Boolean homotopyInitialization[4]={false, true, true, true}
      "= true, use homotopy method";
    // 1=V3V, 2=V2VC6, 3=V2VC5, 4=V2VC2
    parameter Boolean filteredOpening[4]={false, false, false, true}
      "= true, if opening is filtered with a 2nd order CriticalDamping filter";
    // 1=C6, 2=C5, 3=C2
    parameter Boolean filteredSpeed[3]= {false, false, true}
      "= true, if speed is filtered with a 2nd order CriticalDamping filter";
    // 1=V3V, 2=V2VC6, 3=V2VC5, 4=V2VC2
    parameter Real deltaM[4]={0.05, 0.05, 0.05, 0.05}
      "Fraction of nominal flow rate where linearization starts, if y=1";
    parameter Modelica.SIunits.Time riseTime_pump=30
      "Rise time of the filter (time to reach 99.6 % of an opening step)[1] pour v3v, [2] pour v2v, [3] pour pompes";
    parameter Modelica.SIunits.Time riseTime_V2V=30
      "Rise time of the filter (time to reach 99.6 % of an opening step)[1] pour v3v, [2] pour v2v, [3] pour pompes";
    parameter Modelica.SIunits.Time riseTime_V3V=30
      "Rise time of the filter (time to reach 99.6 % of an opening step)[1] pour v3v, [2] pour v2v, [3] pour pompes";
    parameter Modelica.SIunits.Length thicknessIns=0.100
      "Thickness of insulation";
    parameter Modelica.SIunits.ThermalConductivity lambdaIns=0.04
      "Heat conductivity of insulation";

  // Pumps parameters (protected)
    parameter SolarSystem.Data.Parameter.Pump.SolisArt.C6_5_SolisConfort C6_5(
      motorCooledByFluid=true,
      hydraulicEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{0.2,
            1,2.5,5,10,15,20,25,30,35,40}, eta={1,1,1,1,1,1,1,1,1,1,1}),
      motorEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{0.2,1,
            2.5,5,10,15,20,25,30,35,40}, eta={1,1,1,1,1,1,1,1,1,1,1}),
      pressure(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{0.2,1,2.5,5,10,
            15,20,25,30,35,40}, dp=dp_nominal[1]*{4.9,4.6,4.3,4,3.5,3.2,2.6,2,1,0.4,
            0.2}))
      annotation (Placement(transformation(extent={{-620,-10},{-600,10}})));

    parameter SolarSystem.Data.Parameter.Pump.SolisArt.C2_4_SolisConfort C4_2(
      motorCooledByFluid=true,
      hydraulicEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{0.2,
            1,4,8.75,17.5,26.25,35,43.75,52.5,61.25,70}, eta={1,1,1,1,1,1,1,1,1,1,
            1}),
      motorEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{0.2,1,
            4,8.75,17.5,26.25,35,43.75,52.5,61.25,70}, eta={1,1,1,1,1,1,1,1,1,1,1}),
      pressure(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{0.2,1,4,8.75,
            17.5,26.25,35,43.75,52.5,61.25,70}, dp=dp_nominal[2]*{4.9,4.6,4.3,4,3.5,
            3.2,2.6,2,1,0.4,0.2}))
      annotation (Placement(transformation(extent={{-390,28},{-370,48}})));

  // Algorithms (public)
  public
    SolarSystem.Control.Electrical_control.Equipements_control_smooth_enhanced
      algorithm_control(
      table={[0,289.15,283.15,0.025; 3600,289.15,283.15,0.025; 7200,289.15,283.15,
          0.025; 10800,289.15,283.15,0.025; 14400,289.15,283.15,0.025; 18000,
          289.15,283.15,0.025; 21600,289.15,283.15,0.025; 25200,292.15,295.15,
          0.025; 28800,292.15,295.15,0.025; 32400,289.15,295.15,0.025; 36000,
          289.15,295.15,0.00555555555555556; 39600,289.15,295.15,
          0.00555555555555556; 43200,289.15,295.15,0.00555555555555556; 46800,
          289.15,295.15,0.00555555555555556; 50400,289.15,295.15,
          0.00555555555555556; 54000,289.15,295.15,0.00555555555555556; 57600,
          289.15,295.15,0.00555555555555556; 61200,289.15,295.15,
          0.00555555555555556; 64800,292.15,283.15,0.025; 68400,292.15,283.15,
          0.025; 72000,292.15,283.15,0.025; 75600,292.15,283.15,0.025; 79200,
          292.15,283.15,0.025; 82800,292.15,283.15,0.025; 86400,289.15,283.15,
          0.025; 90000,289.15,283.15,0.025; 93600,289.15,283.15,0.025; 97200,
          289.15,283.15,0.025; 100800,289.15,283.15,0.025; 104400,289.15,283.15,
          0.025; 108000,289.15,283.15,0.025; 111600,292.15,295.15,0.025; 115200,
          292.15,295.15,0.025; 118800,289.15,295.15,0.025; 122400,289.15,295.15,
          0.00555555555555556; 126000,289.15,295.15,0.00555555555555556; 129600,
          289.15,295.15,0.00555555555555556; 133200,289.15,295.15,
          0.00555555555555556; 136800,289.15,295.15,0.00555555555555556; 140400,
          289.15,295.15,0.00555555555555556; 144000,289.15,295.15,
          0.00555555555555556; 147600,289.15,295.15,0.00555555555555556; 151200,
          292.15,283.15,0.025; 154800,292.15,283.15,0.025; 158400,292.15,283.15,
          0.025; 162000,292.15,283.15,0.025; 165600,292.15,283.15,0.025; 169200,
          292.15,283.15,0.025; 172800,289.15,283.15,0.025; 176400,289.15,283.15,
          0.025; 180000,289.15,283.15,0.025; 183600,289.15,283.15,0.025; 187200,
          289.15,283.15,0.025; 190800,289.15,283.15,0.025; 194400,289.15,283.15,
          0.025; 198000,292.15,295.15,0.025; 201600,292.15,295.15,0.025; 205200,
          289.15,295.15,0.025; 208800,289.15,295.15,0.00555555555555556; 212400,
          289.15,295.15,0.00555555555555556; 216000,289.15,295.15,
          0.00555555555555556; 219600,289.15,295.15,0.00555555555555556; 223200,
          289.15,295.15,0.025; 226800,289.15,295.15,0.025; 230400,289.15,295.15,
          0.025; 234000,289.15,295.15,0.025; 237600,292.15,283.15,0.025; 241200,
          292.15,283.15,0.025; 244800,292.15,283.15,0.025; 248400,292.15,283.15,
          0.025; 252000,292.15,283.15,0.025; 255600,292.15,283.15,0.025; 259200,
          289.15,283.15,0.025; 262800,289.15,283.15,0.025; 266400,289.15,283.15,
          0.025; 270000,289.15,283.15,0.025; 273600,289.15,283.15,0.025; 277200,
          289.15,283.15,0.025; 280800,289.15,283.15,0.025; 284400,292.15,295.15,
          0.025; 288000,292.15,295.15,0.025; 291600,289.15,295.15,0.025; 295200,
          289.15,295.15,0.00555555555555556; 298800,289.15,295.15,
          0.00555555555555556; 302400,289.15,295.15,0.00555555555555556; 306000,
          289.15,295.15,0.00555555555555556; 309600,289.15,295.15,
          0.00555555555555556; 313200,289.15,295.15,0.00555555555555556; 316800,
          289.15,295.15,0.00555555555555556; 320400,289.15,295.15,
          0.00555555555555556; 324000,292.15,283.15,0.025; 327600,292.15,283.15,
          0.025; 331200,292.15,283.15,0.025; 334800,292.15,283.15,0.025; 338400,
          292.15,283.15,0.025; 342000,292.15,283.15,0.025; 345600,289.15,283.15,
          0.025; 349200,289.15,283.15,0.025; 352800,289.15,283.15,0.025; 356400,
          289.15,283.15,0.025; 360000,289.15,283.15,0.025; 363600,289.15,283.15,
          0.025; 367200,289.15,283.15,0.025; 370800,292.15,295.15,0.025; 374400,
          292.15,295.15,0.025; 378000,289.15,295.15,0.025; 381600,289.15,295.15,
          0.00555555555555556; 385200,289.15,295.15,0.00555555555555556; 388800,
          289.15,295.15,0.00555555555555556; 392400,289.15,295.15,
          0.00555555555555556; 396000,289.15,295.15,0.00555555555555556; 399600,
          289.15,295.15,0.00555555555555556; 403200,289.15,295.15,
          0.00555555555555556; 406800,289.15,295.15,0.00555555555555556; 410400,
          292.15,283.15,0.025; 414000,292.15,283.15,0.025; 417600,292.15,283.15,
          0.025; 421200,292.15,283.15,0.025; 424800,292.15,283.15,0.025; 428400,
          292.15,283.15,0.025; 432000,289.15,283.15,0.025; 435600,289.15,283.15,
          0.025; 439200,289.15,283.15,0.025; 442800,289.15,283.15,0.025; 446400,
          289.15,283.15,0.025; 450000,289.15,283.15,0.025; 453600,289.15,283.15,
          0.025; 457200,292.15,295.15,0.025; 460800,292.15,295.15,0.025; 464400,
          292.15,295.15,0.025; 468000,292.15,295.15,0.025; 471600,292.15,295.15,
          0.025; 475200,292.15,295.15,0.025; 478800,292.15,295.15,0.025; 482400,
          292.15,295.15,0.025; 486000,292.15,295.15,0.025; 489600,292.15,295.15,
          0.025; 493200,292.15,295.15,0.025; 496800,292.15,283.15,0.025; 500400,
          292.15,283.15,0.025; 504000,292.15,283.15,0.025; 507600,292.15,283.15,
          0.025; 511200,292.15,283.15,0.025; 514800,292.15,283.15,0.025; 518400,
          289.15,283.15,0.025; 522000,289.15,283.15,0.025; 525600,289.15,283.15,
          0.025; 529200,289.15,283.15,0.025; 532800,289.15,283.15,0.025; 536400,
          289.15,283.15,0.025; 540000,289.15,283.15,0.025; 543600,292.15,295.15,
          0.025; 547200,292.15,295.15,0.025; 550800,292.15,295.15,0.025; 554400,
          292.15,295.15,0.025; 558000,292.15,295.15,0.025; 561600,292.15,295.15,
          0.025; 565200,292.15,295.15,0.025; 568800,292.15,295.15,0.025; 572400,
          292.15,295.15,0.025; 576000,292.15,295.15,0.025; 579600,292.15,295.15,
          0.025; 583200,292.15,283.15,0.025; 586800,292.15,283.15,0.025; 590400,
          292.15,283.15,0.025; 594000,292.15,283.15,0.025; 597600,292.15,283.15,
          0.025; 601200,292.15,283.15,0.025; 604800,292.15,283.15,0.025]},
      DHW_elec_power=3000,
      heater_elec_power={3000}) "Control algorithm for the whole system"
      annotation (__Dymola_tag={"algo","Consignesouff"}, Placement(transformation(
            extent={{-758,-168},{-474,-62}})));

  // Flow/Temp sensors (public)
  public
    Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T3
      annotation (Placement(transformation(extent={{-528,122},{-514,136}})));
    Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T4
      annotation (Placement(transformation(extent={{-528,142},{-514,156}})));
    Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T5
      annotation (Placement(transformation(extent={{-640,126},{-652,138}})));

    Buildings.Fluid.Sensors.TemperatureTwoPort Tafter_hex(
      tau=tau,
      redeclare package Medium = MediumA,
      m_flow_nominal=900/3600*1.2)
      "Stable temperature after exchanger but before electrical supply"
                                   annotation (Placement(transformation(
          extent={{10,-10},{-10,10}},
          rotation=270,
          origin={-320,120})));
    Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tsoufflage
      "Accurate temperature of air after exchanger and electrical supply."
      annotation (Placement(transformation(extent={{-324,142},{-340,158}})));
    Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T7
      "Room air temperature"
      annotation (Placement(transformation(extent={{-380,0},{-396,16}})));
    Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor Tbefore_hex
      "Accurate temperature after mixing room but before exchanger."
      annotation (Placement(transformation(extent={{-162,-58},{-178,-42}})));
    Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T22
      "Temperature of water next to C6 pump."
      annotation (Placement(transformation(extent={{8,8},{-8,-8}},
          rotation=180,
          origin={-652,8})));
    Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor T21
      "Temperature of water next to C5 pump."
      annotation (Placement(transformation(extent={{-560,0},{-576,16}})));
    Buildings.Fluid.Sensors.TemperatureTwoPort Tbefore_hex_stable(
      tau=tau,
      redeclare package Medium = MediumA,
      m_flow_nominal=900/3600*1.2)
      "Stable temperature after mixing room but before exchanger." annotation (
        Placement(transformation(
          extent={{10,-10},{-10,10}},
          rotation=0,
          origin={-210,-20})));
    Buildings.Fluid.Sensors.TemperatureTwoPort Tsoufflage_stable(
      tau=tau,
      redeclare package Medium = MediumA,
      m_flow_nominal=900/3600*1.2)
      "Stable temperature of air after exchanger and electrical supply."
      annotation (Placement(transformation(
          extent={{10,10},{-10,-10}},
          rotation=180,
          origin={-210,20})));

  // Systems (public)
  public
    SolarSystem.Solar_Collector.EN12975_Integrator Collector(
      redeclare package Medium = MediumE,
      rho=0.2,
      sysConfig=Buildings.Fluid.SolarCollectors.Types.SystemConfiguration.Parallel,
      T_start=T_start,
      deltaM=0.1,
      lat(displayUnit="rad") = weather_file.lattitude,
      nSeg=collector_seg,
      from_dp=false,
      linearizeFlowResistance=false,
      nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
      azi(displayUnit="rad") = 0,
      use_shaCoe_in=false,
      nPanels=4,
      computeFlowResistance=false,
      homotopyInitialization=false,
      til(displayUnit="deg") = 0.57595865315813,
      per=SolarSystem.Data.Parameter.SolarCollector.IDMK2_5_buildings())
                                    annotation (__Dymola_tag={"Power","Solar", "Energy"},
        Placement(transformation(extent={{-792,130},{-740,190}})));

    Buildings.Fluid.Storage.StratifiedEnhancedInternalHex Buffer_Tank(
      redeclare package Medium = MediumE,
      redeclare package MediumHex = MediumE,
      hTan=1.6,
      dIns(displayUnit="mm") = 0.1,
      nSeg=20,
      m_flow_nominal=0.000001,
      tau(displayUnit="s") = 30,
      hHex_a=1.415,
      hHex_b=0.255,
      hexSegMult=3,
      Q_flow_nominal(displayUnit="kW") = 103000,
      mHex_flow_nominal=0.36,
      dExtHex(displayUnit="mm") = 0.0346,
      T_start=T_start,
      kIns=lambdaIns,
      computeFlowResistance=true,
      dpHex_nominal=Buffer_Tank.mHex_flow_nominal*dpValve_nominal/(m_flow_nominal[
          1]*m_flow_nominal[1]),
      VTan(displayUnit="l") = 0.3,
      TTan_nominal=283.15,
      THex_nominal=318.15)
      annotation (__Dymola_tag={"Temperature", "Solar"}, Placement(transformation(extent={{-654,54},{-614,108}})));
    Buildings.Fluid.Storage.StratifiedEnhancedInternalHex DHW_Tank(
      nSeg=20,
      dIns(displayUnit="mm") = 0.055,
      tau(displayUnit="s") = 120,
      redeclare package Medium = MediumDHW,
      redeclare package MediumHex = MediumE,
      hTan=1.67,
      kIns=lambdaIns,
      m_flow_nominal=m_flow_nominal[1],
      hHex_a=0.86,
      hHex_b=0.175,
      dExtHex(displayUnit="mm") = 0.0279,
      Q_flow_nominal=25000,
      mHex_flow_nominal=0.1747,
      computeFlowResistance=true,
      VTan(displayUnit="l") = 0.3,
      dpHex_nominal=DHW_Tank.mHex_flow_nominal*dpValve_nominal/(m_flow_nominal[1]
          *m_flow_nominal[1]),
      TTan_nominal=318.15,
      THex_nominal=283.15) annotation (Placement(transformation(extent={{-538,54},
              {-490,110}})));

    Buildings.Fluid.HeatExchangers.ConstantEffectiveness hex(redeclare package
        Medium1 = MediumE, redeclare package Medium2 = MediumA,
      m1_flow_nominal=m_flow_nominal[2],
      linearizeFlowResistance1=false,
      dp2_nominal=0,
      m2_flow_nominal=90/3600*1.2,
      dp1(start=0),
      dp1_nominal=0,
      show_T=false,
      m1_flow(start=m_flow_nominal[2]),
      from_dp1=false)                                        annotation (__Dymola_tag={"Temperature", "Computed"},
        Placement(transformation(
          extent={{-23,32},{23,-32}},
          rotation=-90,
          origin={-340,75})));

  // Drawing up (public)
    Buildings.Fluid.Sources.Boundary_pT PipeNetwork_Source(
      nPorts=1,
      use_T_in=true,
      redeclare package Medium = MediumDHW,
      T=283.15) annotation (__Dymola_tag={"Temperature"}, Placement(transformation(
          extent={{6,-6},{-6,6}},
          rotation=-90,
          origin={-480,14})));
    SolarSystem.Valves.DrawingUp.Drawing_up_mflow Drawing_up(redeclare package
        Medium = MediumDHW, table=[0,0; 3600,0; 7200,0; 10800,0; 14400,0; 18000,0;
          21600,16.5; 25200,16.5; 28800,16.5; 32400,0; 36000,0; 39600,0; 43200,
          16.5; 46800,16.5; 50400,16.5; 54000,0; 57600,0; 61200,0; 64800,58.6575;
          68400,58.6575; 72000,58.6575; 75600,0; 79200,0; 82800,0; 86400,0; 90000,
          0; 93600,0; 97200,0; 100800,0; 104400,0; 108000,16.5; 111600,16.5;
          115200,16.5; 118800,0; 122400,0; 126000,0; 129600,16.5; 133200,16.5;
          136800,16.5; 140400,0; 144000,0; 147600,0; 151200,58.6575; 154800,
          58.6575; 158400,58.6575; 162000,0; 165600,0; 169200,0; 172800,0; 176400,
          0; 180000,0; 183600,0; 187200,0; 190800,0; 194400,16.5; 198000,16.5;
          201600,16.5; 205200,0; 208800,0; 212400,0; 216000,16.5; 219600,16.5;
          223200,16.5; 226800,0; 230400,0; 234000,0; 237600,58.6575; 241200,
          58.6575; 244800,58.6575; 248400,0; 252000,0; 255600,0; 259200,0; 262800,
          0; 266400,0; 270000,0; 273600,0; 277200,0; 280800,16.5; 284400,16.5;
          288000,16.5; 291600,0; 295200,0; 298800,0; 302400,16.5; 306000,16.5;
          309600,16.5; 313200,0; 316800,0; 320400,0; 324000,58.6575; 327600,
          58.6575; 331200,58.6575; 334800,0; 338400,0; 342000,0; 345600,0; 349200,
          0; 352800,0; 356400,0; 360000,0; 363600,0; 367200,16.5; 370800,16.5;
          374400,16.5; 378000,0; 381600,0; 385200,0; 388800,16.5; 392400,16.5;
          396000,16.5; 399600,0; 403200,0; 406800,0; 410400,58.6575; 414000,
          58.6575; 417600,58.6575; 421200,0; 424800,0; 428400,0; 432000,0; 435600,
          0; 439200,0; 442800,0; 446400,0; 450000,0; 453600,16.5; 457200,16.5;
          460800,16.5; 464400,0; 468000,0; 471600,0; 475200,16.5; 478800,16.5;
          482400,16.5; 486000,0; 489600,0; 493200,0; 496800,58.6575; 500400,
          58.6575; 504000,58.6575; 507600,0; 511200,0; 514800,0; 518400,0; 522000,
          0; 525600,0; 529200,0; 532800,0; 536400,0; 540000,16.5; 543600,16.5;
          547200,16.5; 550800,0; 554400,0; 558000,0; 561600,16.5; 565200,16.5;
          568800,16.5; 572400,0; 576000,0; 579600,0; 583200,58.6575; 586800,
          58.6575; 590400,58.6575; 594000,0; 597600,0; 601200,0; 604800,0])
                                                            annotation (
        __Dymola_tag={"Energy", "Power", "DHW", "Pump", "flow"}, Placement(transformation(extent={{-570,66},
              {-600,98}})));
  // Pumps/fans (public)
  public
    Buildings.Fluid.Movers.SpeedControlled_Nrpm
                                              C6(
      redeclare package Medium =
          MediumE,
      T_start=T_start,
      tau=tau,
      dp(start=dp_nominal[1]),
      m_flow(start=m_flow_nominal[1]),
      riseTime=riseTime_pump,
      per=C6_5,
      filteredSpeed=filteredSpeed[1])
                      annotation (__Dymola_tag={"Pump", "flow"}, Placement(transformation(
          extent={{10,-10},{-10,10}},
          rotation=90,
          origin={-680,20})));

    Buildings.Fluid.Movers.SpeedControlled_Nrpm
                                              C5(
      redeclare package Medium =
          MediumE,
      T_start=T_start,
      tau=tau,
      dp(start=dp_nominal[1]),
      m_flow(start=m_flow_nominal[1]),
      riseTime=riseTime_pump,
      per=C6_5,
      filteredSpeed=filteredSpeed[2])
                      annotation (__Dymola_tag={"Pump", "flow"}, Placement(transformation(
          extent={{10,10},{-10,-10}},
          rotation=90,
          origin={-540,20})));

    Buildings.Fluid.Movers.SpeedControlled_Nrpm
                                              C2(
      redeclare package Medium =
          MediumE,
      T_start=T_start,
      tau=tau,
      dp(start=dp_nominal[2]),
      m_flow(start=m_flow_nominal[2]),
      riseTime=riseTime_pump,
      per=C4_2,
      filteredSpeed=filteredSpeed[3])
                      annotation (__Dymola_tag={"Pump", "flow"}, Placement(transformation(
          extent={{10,10},{-10,-10}},
          rotation=90,
          origin={-360,20})));
          Buildings.Fluid.Movers.FlowControlled_m_flow fan(redeclare package
        Medium =
          MediumA,
      allowFlowReversal=false,
      m_flow_nominal=900/3600*1.2,
      tau=tau,
      riseTime=100,
      filteredSpeed=false,
      redeclare Buildings.Fluid.Movers.Data.Generic per)
      annotation (__Dymola_tag={"Fan", "flow"}, Placement(transformation(extent={{-160,-30},{-180,-10}})));

  // Energy integrators (public)
    SolarSystem.Utilities.Other.Integrator_simple integrator_losses annotation (
       __Dymola_tag={"Energy","Losses"}, Placement(transformation(extent={{-424,
              -154},{-384,-126}})));
    SolarSystem.Utilities.Other.Integrator_energy integrator_StorageTank_Solar_charge(Cp_value=MediumE.cp_const)
      "Integration of the solar energy given to the storage tank"
      annotation (__Dymola_tag={"Energy", "Solar"}, Placement(transformation(extent={{-364,-114},{-324,-86}})));

    SolarSystem.Utilities.Other.Integrator_energy integrator_collector(Cp_value=MediumE.cp_const)
      annotation (__Dymola_tag={"Energy", "Solar"}, Placement(transformation(extent={{-424,-114},{-384,-86}})));
    SolarSystem.Utilities.Other.Integrator_energy integrator_DHW_Solar_charge(Cp_value=MediumE.cp_const)
      "Integration of the solar energy given to the DHW_Tank."
      annotation (__Dymola_tag={"Energy", "Solar"}, Placement(transformation(extent={{-302,
              -114},{-262,-86}})));
    SolarSystem.Utilities.Other.Integrator_with_conditions
      integrator_StorageTank_losses "Compute Buffer tank losses"
      annotation (__Dymola_tag={"Energy", "Losses"}, Placement(transformation(extent={{-364,-152},{-324,-126}})));
    SolarSystem.Utilities.Other.Integrator_with_conditions integrator_DHW_losses
      "Compute DHW tank losses"
      annotation (__Dymola_tag={"Energy", "Losses"}, Placement(transformation(extent={{-304,-154},{-264,-126}})));
    SolarSystem.Utilities.Other.Integrator_energy_air integrator_exchanger
      annotation (__Dymola_tag={"Energy", "Solar"}, Placement(transformation(extent={{-240,-112},{-200,-84}})));
  // Flow resistances (public)
  public
    Buildings.Fluid.FixedResistances.Pipe inFlow_Collector(
      redeclare package Medium = MediumE,
      thicknessIns(displayUnit="mm") = thicknessIns,
      lambdaIns=lambdaIns,
      useMultipleHeatPorts=true,
      T_start=T_start,
      v_nominal=v_nominal,
      from_dp=false,
      dp(start=0),
      m_flow_nominal=m_flow_nominal[1],
      dp_nominal=dpValve_nominal/2,
      linearizeFlowResistance=false,
      nSeg=20,
      length=10)                                       annotation (Placement(
          transformation(
          extent={{-8,-6},{8,6}},
          rotation=90,
          origin={-800,80})));

  // Sources (public)
  public
    Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow Elec_to_DHW
      "Prescribed heat flow for heating and cooling"
      annotation (Placement(transformation(extent={{-438,76},{-460,98}})));

    Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow Elec_to_air
      "Prescribed heat flow to blowing air" annotation (Placement(transformation(
          extent={{8,-8},{-8,8}},
          rotation=180,
          origin={-330,176})));
    Buildings.BoundaryConditions.WeatherData.Bus weaBus
      annotation (__Dymola_tag={"Power", "Weather"}, Placement(transformation(extent={{-834,-142},{-806,-116}})));
  // Flow resistances (protected)
  protected
    Buildings.Fluid.FixedResistances.Pipe outFlow_Collector(
      redeclare package Medium = MediumE,
      thicknessIns=thicknessIns,
      lambdaIns=lambdaIns,
      useMultipleHeatPorts=true,
      T_start=T_start,
      v_nominal=v_nominal,
      dp(start=0),
      m_flow_nominal=m_flow_nominal[1],
      dp_nominal=dpValve_nominal/2,
      nSeg=20,
      length=10,
      from_dp=false,
      linearizeFlowResistance=false)
      annotation (Placement(transformation(extent={{-728,154},{-712,166}})));
    Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM BufferTank_BottomSplitter1(
      redeclare package Medium = MediumE,
      T_start=T_start,
      dp_nominal=dp_nominal[1]*{0,0,0},
      m_flow_nominal=m_flow_nominal[1]*{1,1,1},
      from_dp=false) "Flow splitter"                         annotation (
        Placement(transformation(
          extent={{6,6},{-6,-6}},
          rotation=180,
          origin={-680,60})));
    Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM BufferTank_TopSplitter(
      redeclare package Medium = MediumE,
      T_start=T_start,
      dp_nominal=dp_nominal[1]*{0,0,0},
      m_flow_nominal=m_flow_nominal[1]*{2,-1,-1},
      from_dp=false) "Flow splitter"                          annotation (
        Placement(transformation(
          extent={{6,6},{-6,-6}},
          rotation=180,
          origin={-680,160})));
    Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM HealthTank_SolarBottomSplitter(
      redeclare package Medium = MediumE,
      T_start=T_start,
      dp_nominal=dp_nominal[1]*{0,0,0},
      m_flow_nominal=m_flow_nominal[1]*{1,-2,1},
      from_dp=false) "Flow splitter"                         annotation (
        Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=180,
          origin={-540,-40})));
  protected
    Buildings.Fluid.FixedResistances.SplitterFixedResistanceDpM HealthTank_SolarTopSplitter(
      redeclare package Medium = MediumE,
      T_start=T_start,
      dp_nominal=dp_nominal[1]*{0,0,0},
      m_flow_nominal=m_flow_nominal[1]*{1,-1,-1},
      from_dp=false,
      tau=tau) "Flow splitter"                                      annotation (
        Placement(transformation(
          extent={{6,6},{-6,-6}},
          rotation=180,
          origin={-550,160})));

  public
    Buildings.Fluid.FixedResistances.FixedResistanceDpM Resistance_House(
      redeclare package Medium = MediumA,
      allowFlowReversal=false,
      dp_nominal=1,
      linearized=true,
      from_dp=true,
      m_flow_nominal=floor_area*1.5*1.2/3600)
      annotation (Placement(transformation(extent={{-6,-26},{-18,-14}})));

  // Valves (protected)
  public
    Buildings.Fluid.Actuators.Valves.ThreeWayEqualPercentageLinear
                                                    Valve_Solar(
      redeclare package Medium = MediumE,
      T_start=T_start,
      filteredOpening=filteredOpening[1],
      riseTime=riseTime_V3V,
      dpValve_nominal=dpValve_nominal,
      l={0.005,0.005},
      fraK=1,
      from_dp=false,
      m_flow_nominal=m_flow_nominal[1],
      dpFixed_nominal={0,0},
      deltaM=deltaM[1],
      tau=tau,
      portFlowDirection_1=Modelica.Fluid.Types.PortFlowDirection.Leaving,
      portFlowDirection_2=Modelica.Fluid.Types.PortFlowDirection.Entering,
      portFlowDirection_3=Modelica.Fluid.Types.PortFlowDirection.Leaving,
      homotopyInitialization=homotopyInitialization[1])
      annotation (Placement(transformation(extent={{-730,-30},{-710,-50}})));

    Buildings.Fluid.Actuators.Valves.TwoWayEqualPercentage
                                                  Valve_C6(
      redeclare package Medium = MediumE,
      dpValve_nominal=dpValve_nominal,
      m_flow_nominal=m_flow_nominal[1],
      riseTime=riseTime_V2V,
      m_flow(start=0),
      dp(start=0),
      from_dp=false,
      dpFixed_nominal=0,
      filteredOpening=filteredOpening[2],
      deltaM=deltaM[2],
      homotopyInitialization=homotopyInitialization[2]) "Pressure drop"
                      annotation (Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=-90,
          origin={-680,-6})));
  public
    Buildings.Fluid.Actuators.Valves.TwoWayEqualPercentage
                                                  Valve_C5(
      redeclare package Medium = MediumE,
      dpValve_nominal=dpValve_nominal,
      m_flow_nominal=m_flow_nominal[1],
      riseTime=riseTime_V2V,
      from_dp=false,
      filteredOpening=filteredOpening[3],
      deltaM=deltaM[3],
      homotopyInitialization=homotopyInitialization[3]) "Pressure drop"
                      annotation (Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=-90,
          origin={-540,-6})));
  protected
    Buildings.Fluid.Actuators.Valves.TwoWayEqualPercentage
                                                  Valve_C2(
      redeclare package Medium = MediumE,
      riseTime=riseTime_V2V,
      from_dp=false,
      dpValve_nominal=3063.12,
      m_flow_nominal=m_flow_nominal[2],
      m_flow(start=0),
      dp(start=0),
      filteredOpening=filteredOpening[4],
      deltaM=deltaM[4],
      homotopyInitialization=homotopyInitialization[4]) "Pressure drop"
                      annotation (Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=-90,
          origin={-360,-6})));

  // Flow/Temp sensors (protected)
  protected
    Buildings.Fluid.Sensors.MassFlowRate Collector_FlowSensor(redeclare package
        Medium = MediumE) annotation (Placement(transformation(
          extent={{-4,4},{4,-4}},
          rotation=90,
          origin={-800,114})));
  public
    Buildings.Fluid.Sensors.TemperatureTwoPort Tcold_water(
      redeclare package Medium = MediumDHW,
      m_flow_nominal=0.04,
      tau=10) annotation (__Dymola_tag={"Temperature", "Consigne"}, Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=90,
          origin={-480,36})));

  // Sources (protected)
  protected
    Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Weather(
        computeWetBulbTemperature=false,
      filNam=weather_file.path) "Weather datas"
      annotation (Placement(transformation(extent={{-668,186},{-700,214}})));

    Buildings.Fluid.Sources.Boundary_pT NoFlow_StaticSource(
      redeclare package Medium = MediumE,
      use_T_in=true,
      nPorts=1) annotation (Placement(transformation(extent={{6,-6},{-6,6}},
          rotation=90,
          origin={-660,116})));
    Buildings.Fluid.Sources.MassFlowSource_T NoFlow_DynamicSource(
      redeclare package Medium = MediumE,
      use_T_in=true,
      nPorts=1) annotation (Placement(transformation(extent={{-634,136},{-626,144}})));
  public
    Buildings.Fluid.Storage.ExpansionVessel SurgeTank(
      redeclare package Medium = MediumE,
      T_start=T_start,
      V_start(displayUnit="l") = 0.12)
      annotation (Placement(transformation(extent={{-652,186},{-628,212}})));
  protected
    Modelica.Blocks.Sources.CombiTimeTable PipeNetwork_Temp(extrapolation=
          Modelica.Blocks.Types.Extrapolation.Periodic, table=weather_file.water,
      smoothness=Modelica.Blocks.Types.Smoothness.LinearSegments)
      "Cold water temperature according to month of the year"
      annotation (Placement(transformation(extent={{6,-6},{-6,6}},
          rotation=-90,
          origin={-478,-12})));
    Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature Text annotation (
        Placement(transformation(
          extent={{-6,-6},{6,6}},
          rotation=90,
          origin={-740,14})));

    inner Modelica.Fluid.System system
      annotation (Placement(transformation(extent={{140,190},{160,210}})));

  // Other (protected)
  Buildings.Fluid.MixingVolumes.MixingVolume vol(nPorts=2, redeclare package
        Medium = MediumA,
      m_flow_nominal=900/3600*1.2,
      prescribedHeatFlowRate=true,
      V=0.5)              annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=0,
          origin={-290,150})));
       Buildings.Fluid.Sensors.Density density_mix(redeclare package Medium =
          MediumA) "Air density inside the building"
      annotation (Placement(transformation(extent={{-84,-12},{-96,0}})));
  public
    Buildings.Fluid.Sensors.MassFlowRate recycled_air_flow(redeclare package
        Medium = MediumA) "Recycled air mass flow rate from the house"
      annotation (__Dymola_tag={"Fan", "flow"}, Placement(transformation(extent={{-52,-46},{-64,-34}})));
    Buildings.Fluid.Sensors.MassFlowRate Outside_air_flow(redeclare package
        Medium = MediumA) "Mass flow rate of outside air used" annotation (__Dymola_tag={"Fan", "flow"},
        Placement(transformation(
          extent={{6,6},{-6,-6}},
          rotation=-90,
          origin={-120,-60})));
  protected
    Modelica.Thermal.HeatTransfer.Components.ThermalCollector Losses_collector(m=4)
      "Collect all losses from tanks to the house."
      annotation (Placement(transformation(extent={{-10,10},{10,-10}},
          rotation=-90,
          origin={-470,200})));
    Modelica.Thermal.HeatTransfer.Sensors.HeatFlowSensor heatFlowSensor
      annotation (Placement(transformation(
          extent={{8,-8},{-8,8}},
          rotation=-90,
          origin={-740,40})));
    Modelica.Thermal.HeatTransfer.Components.ThermalCollector Losses_pipes_collector(m=48)
      "Collect all losses from tanks to the house."
      annotation (Placement(transformation(extent={{-750,60},{-730,80}})));

  public
    Buildings.Fluid.Sources.MassFlowSource_T Outside(
      redeclare package Medium = MediumA,
      use_T_in=true,
      use_m_flow_in=true,
      use_X_in=true,
      nPorts=1) "Source model for air infiltration"
      annotation (Placement(transformation(extent={{-98,-122},{-112,-108}})));
  protected
  Buildings.Fluid.MixingVolumes.MixingVolume mixing_box(
      redeclare package Medium = MediumA,
      prescribedHeatFlowRate=false,
      V=0.5,
      nPorts=4,
      m_flow_nominal=90/3600*1.2)
                annotation (Placement(transformation(
          extent={{10,-10},{-10,10}},
          rotation=0,
          origin={-118,-10})));
       Buildings.Fluid.Sensors.Density density_outside(redeclare package Medium =
          MediumA) "Air density inside the building"
      annotation (Placement(transformation(extent={{-52,-10},{-64,-22}})));
    Buildings.Utilities.Psychrometrics.X_pTphi x_pTphi
      annotation (Placement(transformation(extent={{-136,-154},{-116,-134}})));
    Buildings.Utilities.Psychrometrics.ToTotalAir
               toTotalAir
      annotation (Placement(transformation(extent={{-110,-154},{-90,-134}})));
    Buildings.Fluid.Sources.MassFlowSource_T Infiltrations(
      redeclare package Medium = MediumA,
      use_T_in=true,
      use_X_in=true,
      nPorts=1,
      use_m_flow_in=false,
      m_flow=-0.4*(107 + floor_area*1)/3600)
      "Source model for air infiltration (107 = walls(26.4*4) + velux walls(1.2*1.2))"
      annotation (Placement(transformation(extent={{-58,-122},{-44,-108}})));
  public
    Buildings.Fluid.Sensors.TemperatureTwoPort Tbefore_DHW_tank(
      redeclare package Medium = MediumE,
      tau=tau,
      m_flow_nominal=m_flow_nominal[1]) annotation (Placement(transformation(
          extent={{7,8},{-7,-8}},
          rotation=90,
          origin={-550,123})));

    Modelica.Blocks.Math.Product to_mass_flowRate_mix
      "Volumic to mass flow rate"
      annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=-90,
          origin={-170,2})));
    Modelica.Blocks.Math.Product to_mass_flowRate_outside
      "Volumic to mass flow rate" annotation (Placement(transformation(
          extent={{-4,-4},{4,4}},
          rotation=-90,
          origin={-82,-100})));

  equation
  //
  // Energy integrators
  //
    //Storage tank integrator
    integrator_StorageTank_Solar_charge.Tin = outFlow_Collector.vol[outFlow_Collector.nSeg].T
      "Tin for storage tank";
    integrator_StorageTank_Solar_charge.Tout = T22.T "Tout for Storage tank";
    integrator_StorageTank_Solar_charge.m_flow = C6.m_flow;
    // Collector integrator (Tout is collector Tin because we gains are taken from collector not the opposite like with a tank)
    integrator_collector.Tout = inFlow_Collector.vol[inFlow_Collector.nSeg].T;
    integrator_collector.Tin = Collector.Tinside[collector_seg]
      "Last discretization as output temperature";
    integrator_collector.m_flow = Collector_FlowSensor.m_flow;
    // DHW integrator
    integrator_DHW_Solar_charge.Tin = Tbefore_DHW_tank.T;
    integrator_DHW_Solar_charge.Tout = T21.T;
    integrator_DHW_Solar_charge.m_flow = C5.m_flow;
    // Exchanger integrator
    integrator_exchanger.Tin = Tbefore_hex_stable.T;
    integrator_exchanger.Tout = Tafter_hex.T;
    integrator_exchanger.Cp_air =MediumA.specificHeatCapacityCp(state=MediumA.setState_phX(p=fan.port_b.p, h=fan.port_b.h_outflow, X=fan.port_b.Xi_outflow))
      "Internal mass capacity";
    // integrator_exchanger.Cp_air = if algorithm_control.Sj_state[1] then MediumA.specificHeatCapacityCp(state=MediumA.setState_phX(p=fan.port_b.p, h=fan.port_b.h_outflow, X=fan.port_b.Xi_outflow)) else 1004
    //   "Internal mass capacity";
    integrator_exchanger.m_flow = fan.m_flow;
    // Losses integrator (Storage tank)
    integrator_StorageTank_losses.Power = Buffer_Tank.Ql_flow
      "Losses from storage tank";
    integrator_StorageTank_losses.Conditions[1] = Modelica.Math.BooleanVectors.anyTrue(algorithm_control.need_heat.y)
      "need_heat is a vector";
    integrator_StorageTank_losses.Conditions[2] = Modelica.Math.BooleanVectors.anyTrue(algorithm_control.need_overheat.y)
      "need_overheat is a vector";
    // Losses integrator (DHW tank)
    integrator_DHW_losses.Power = DHW_Tank.Ql_flow "Losses from storage tank";
    integrator_DHW_losses.Conditions[1] = Modelica.Math.BooleanVectors.anyTrue(algorithm_control.need_heat.y);
    integrator_DHW_losses.Conditions[2] = Modelica.Math.BooleanVectors.anyTrue(algorithm_control.need_overheat.y);
    // Losses (pipes) integrator
    integrator_losses.Heat_flow = heatFlowSensor.Q_flow;

  //
  // Algorithm connections
  //
    // Temperatures
    algorithm_control.T1 = Collector.Tinside[collector_seg]
      "Connect collector to T1";
    algorithm_control.T3 = T3.T "Connect bottom tank temperature to T3";
    algorithm_control.T4 = T4.T "Connect top tank temperature to T4";
    algorithm_control.T5 = T5.T "Connect middle storage tank temperature to T5";
    algorithm_control.T7 = T7.T "Connect exchanger outlet temperature to T7";
    // Vector of temperatures(only one room here)
    algorithm_control.Tamb[1] = TRooAir.T "Connect room temperature to Tamb";
    algorithm_control.Tsouff[1] = Tsoufflage.T
      "Connect room blowing temperature to Tsouff";
    algorithm_control.Texch_out[1] = Tafter_hex.T
      "Connect air exchanger outlet temperature to Texch_out";
    // Valve connections
    Valve_Solar.y = if algorithm_control.V3V_solar then 1.0 else 0.0
      "Transform boolean to real and connect result";
    Valve_C6.y = if algorithm_control.S6_state then 1.0 else 0.0
      "Transform boolean to real and connect result";
    Valve_C5.y = if algorithm_control.S5_state then 1.0 else 0.0
      "Transform boolean to real and connect result";
    Valve_C2.y = if algorithm_control.Sj_state[1] then 1.0 else 0.0
      "Transform boolean to real and connect result";
    // Electrical power
    Elec_to_DHW.Q_flow = algorithm_control.power_DHW_elec / 3
      "Divide per 3 because power dispatch to 3 nodes in the tank";
    Elec_to_air.Q_flow = algorithm_control.power_air_elec[1]
      "Divide per 3 because power dispatch to 3 nodes in the tank";
    connect(Valve_Solar.port_1, inFlow_Collector.port_a) annotation (Line(
        points={{-730,-40},{-800,-40},{-800,72}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(T5.T, NoFlow_DynamicSource.T_in) annotation (Line(
        points={{-652,132},{-662,132},{-662,141.6},{-634.8,141.6}},
        color={0,0,127},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));

    connect(inFlow_Collector.port_b, Collector_FlowSensor.port_a) annotation (
        Line(
        points={{-800,88},{-800,110}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(PipeNetwork_Source.ports[1], Tcold_water.port_a) annotation (Line(
        points={{-480,20},{-480,30}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));

    connect(T5.T, NoFlow_StaticSource.T_in) annotation (Line(
        points={{-652,132},{-662,132},{-662,124},{-662.4,124},{-662.4,123.2}},
        color={0,0,127},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));
    connect(NoFlow_StaticSource.ports[1], Buffer_Tank.port_a) annotation (Line(
        points={{-660,110},{-660,81},{-654,81}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(NoFlow_DynamicSource.ports[1], Buffer_Tank.port_b) annotation (Line(
        points={{-626,140},{-618,140},{-618,81},{-614,81}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Buffer_Tank.heaPorVol[4], T5.port) annotation (Line(
        points={{-634,79.947},{-634,132},{-640,132}},
        color={191,0,0},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));
    connect(DHW_Tank.heaPorVol[12], T3.port) annotation (Line(
        points={{-514,82.252},{-528,82.252},{-528,129}},
        color={191,0,0},
        smooth=Smooth.None));
    connect(DHW_Tank.heaPorVol[4], T4.port) annotation (Line(
        points={{-514,80.908},{-514,82},{-528,82},{-528,149}},
        color={191,0,0},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));
    connect(Collector_FlowSensor.port_b, Collector.port_a) annotation (Line(
        points={{-800,118},{-800,160},{-792,160}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(PipeNetwork_Temp.y[1], PipeNetwork_Source.T_in) annotation (Line(
        points={{-478,-5.4},{-477.6,-5.4},{-477.6,6.8}},
        color={0,0,127},
        smooth=Smooth.None));
    connect(C6.port_b, Valve_C6.port_a) annotation (Line(
        points={{-680,10},{-680,0}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(C5.port_b,Valve_C5. port_a) annotation (Line(
        points={{-540,10},{-540,0}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(C2.port_b,Valve_C2. port_a) annotation (Line(
        points={{-360,10},{-360,0}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Collector.port_b, outFlow_Collector.port_a) annotation (Line(
        points={{-740,160},{-728,160}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(weaBus, House.weaBus) annotation (Line(
        points={{-820,-129},{-820,-180},{66,-180},{66,32.425},{44.425,32.425}},
        color={255,204,51},
        thickness=0.25,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(weaBus, Combles.weaBus) annotation (Line(
        points={{-820,-129},{-820,-180},{66,-180},{66,144.005},{34.005,144.005}},
        color={255,204,51},
        thickness=0.25,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(weaBus, VideSanitaire.weaBus) annotation (Line(
        points={{-820,-129},{-820,-180},{66,-180},{66,-80},{34,-80},{34,-78.1},{33.9,
            -78.1}},
        color={255,204,51},
        thickness=0.25,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(weaBus.TDryBul, Toutside.T) annotation (Line(
        points={{-820,-129},{-820,-180},{66,-180},{66,-12},{110,-12},{110,4},{101.6,
            4}},
        color={255,204,51},
        thickness=0.25,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(Weather.weaBus, Collector.weaBus) annotation (Line(
        points={{-700,200},{-792,200},{-792,188.8}},
        color={255,204,51},
        thickness=0.25,
        smooth=Smooth.None));
    connect(Weather.weaBus, weaBus) annotation (Line(
        points={{-700,200},{-820,200},{-820,-129}},
        color={255,204,51},
        thickness=0.25,
        smooth=Smooth.None), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}}));
    connect(hex.port_b1, C2.port_a) annotation (Line(
        points={{-359.2,52},{-359.2,46},{-360,46},{-360,30}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));

    connect(Elec_to_air.port, vol.heatPort) annotation (Line(
        points={{-322,176},{-320,176},{-320,150},{-300,150}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Elec_to_DHW.port, DHW_Tank.heaPorVol[6]) annotation (Line(
        points={{-460,87},{-512,87},{-512,81.244},{-514,81.244}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));

    connect(Elec_to_DHW.port, DHW_Tank.heaPorVol[5]) annotation (Line(
        points={{-460,87},{-470,87},{-470,86},{-512,86},{-512,81.076},{-514,
            81.076}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));

    connect(Elec_to_DHW.port, DHW_Tank.heaPorVol[4]) annotation (Line(
        points={{-460,87},{-470,87},{-470,88},{-512,88},{-512,80.908},{-514,
            80.908}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));

    connect(House.ports[1], Resistance_House.port_a) annotation (Line(
        points={{19.75,11.5},{0,11.5},{0,-20},{-6,-20}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));

    connect(hex.port_b2, Tafter_hex.port_a) annotation (Line(
        points={{-320.8,98},{-320,98},{-320,110}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Tafter_hex.port_b, vol.ports[1]) annotation (Line(
        points={{-320,130},{-320,140},{-292,140}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));

    connect(vol.heatPort, Tsoufflage.port) annotation (Line(
        points={{-300,150},{-324,150}},
        color={0,0,127},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));
    connect(C2.heatPort, T7.port) annotation (Line(
        points={{-366.8,20},{-370,20},{-370,8},{-380,8}},
        color={0,0,127},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));

    connect(weaBus, souInf_House.weaBus) annotation (Line(
        points={{-820,-129},{-820,-180},{-72,-180},{-72,6.12},{-70,6.12}},
        color={255,204,51},
        thickness=0.5,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(souInf_House.ports[3], density_outside.port) annotation (Line(
        points={{-58,6},{-58,-10}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(weaBus.relHum, x_pTphi.phi) annotation (Line(
        points={{-820,-129},{-820,-180},{-166,-180},{-166,-150},{-138,-150}},
        color={255,204,51},
        thickness=0.5,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(weaBus.TDryBul, x_pTphi.T) annotation (Line(
        points={{-820,-129},{-820,-180},{-166,-180},{-166,-144},{-138,-144}},
        color={255,204,51},
        thickness=0.5,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(weaBus.pAtm, x_pTphi.p_in) annotation (Line(
        points={{-820,-129},{-820,-180},{-166,-180},{-166,-138},{-138,-138}},
        color={255,204,51},
        thickness=0.5,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));

    connect(x_pTphi.X[1], toTotalAir.XiDry) annotation (Line(
        points={{-115,-144},{-111,-144}},
        color={0,0,127},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));
    connect(toTotalAir.XiTotalAir, Outside.X_in[1]) annotation (Line(
        points={{-89,-144},{-86,-144},{-86,-117.8},{-96.6,-117.8}},
        color={0,0,127},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));
    connect(toTotalAir.XNonVapor, Outside.X_in[2]) annotation (Line(
        points={{-89,-148},{-84,-148},{-84,-117.8},{-96.6,-117.8}},
        color={0,0,127},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));
    connect(Resistance_House.port_b, recycled_air_flow.port_a) annotation (Line(
        points={{-18,-20},{-40,-20},{-40,-40},{-52,-40}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(recycled_air_flow.port_b, mixing_box.ports[1]) annotation (Line(
        points={{-64,-40},{-114,-40},{-114,-20},{-115,-20}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Resistance_House.port_b, souInf_House.ports[4]) annotation (Line(
        points={{-18,-20},{-40,-20},{-40,6},{-58,6}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Outside.ports[1], Outside_air_flow.port_a) annotation (Line(
        points={{-112,-115},{-120,-115},{-120,-66}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Outside_air_flow.port_b, mixing_box.ports[2]) annotation (Line(
        points={{-120,-54},{-120,-20},{-117,-20}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(mixing_box.ports[3], fan.port_a) annotation (Line(
        points={{-119,-20},{-160,-20}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(mixing_box.ports[4], density_mix.port) annotation (Line(
        points={{-121,-20},{-90,-20},{-90,-12}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(weaBus.TDryBul, Outside.T_in) annotation (Line(
        points={{-820,-129},{-820,-180},{-72,-180},{-72,-112.2},{-96.6,-112.2}},
        color={255,204,51},
        thickness=0.5,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(vol.ports[2], Tsoufflage_stable.port_a) annotation (Line(
        points={{-288,140},{-260,140},{-260,20},{-220,20}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(fan.port_b, Tbefore_hex_stable.port_a) annotation (Line(
        points={{-180,-20},{-200,-20}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Tbefore_hex_stable.port_b, hex.port_a2) annotation (Line(
        points={{-220,-20},{-320.8,-20},{-320.8,52}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Tsoufflage_stable.port_b, House.ports[2]) annotation (Line(
        points={{-200,20},{0,20},{0,11.5},{19.75,11.5}},
        color={0,127,255},
        smooth=Smooth.None,
        thickness=0.5));
    connect(C6.heatPort, T22.port) annotation (Line(
        points={{-673.2,20},{-668,20},{-668,8},{-660,8}},
        color={191,0,0},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));
    connect(C5.heatPort, T21.port) annotation (Line(
        points={{-546.8,20},{-552,20},{-552,8},{-560,8}},
        color={191,0,0},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));

    connect(toTotalAir.XiTotalAir, Infiltrations.X_in[1]) annotation (Line(
        points={{-89,-144},{-84,-144},{-84,-117.8},{-59.4,-117.8}},
        color={0,0,127},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));
    connect(toTotalAir.XNonVapor, Infiltrations.X_in[2]) annotation (Line(
        points={{-89,-148},{-84,-148},{-84,-117.8},{-59.4,-117.8}},
        color={0,0,127},
        smooth=Smooth.None,
        pattern=LinePattern.Dash,
        thickness=0.25));
    connect(weaBus.TDryBul, Infiltrations.T_in) annotation (Line(
        points={{-820,-129},{-820,-180},{-72,-180},{-72,-112.2},{-59.4,-112.2}},
        color={255,204,51},
        thickness=0.5,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));

    connect(Infiltrations.ports[1], House.ports[3]) annotation (Line(
        points={{-44,-115},{-44,-114},{-40,-114},{-40,-60},{0,-60},{0,11.5},{19.75,
            11.5}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(DHW_Tank.heaPorTop, Losses_collector.port_a[3]) annotation (Line(
        points={{-509.2,102.72},{-509.2,120},{-500,120},{-500,200},{-479.75,200}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));
    connect(DHW_Tank.heaPorSid, Losses_collector.port_a[4]) annotation (Line(
        points={{-500.56,82},{-500,82},{-500,200},{-479.25,200}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Buffer_Tank.heaPorTop, Losses_collector.port_a[1]) annotation (Line(
        points={{-630,100.98},{-630,120},{-620,120},{-620,200},{-480.75,200}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Buffer_Tank.heaPorSid, Losses_collector.port_a[2]) annotation (Line(
        points={{-622.8,81},{-620,81},{-620,200},{-480.25,200}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Losses_collector.port_b, House.heaPorAir) annotation (Line(
        points={{-460,200},{80,200},{80,19},{30.25,19}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));
    connect(Text.port, heatFlowSensor.port_a) annotation (Line(
        points={{-740,20},{-740,32}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));
    connect(heatFlowSensor.port_b, Losses_pipes_collector.port_b) annotation (
        Line(
        points={{-740,48},{-740,60}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));
    connect(weaBus.TDryBul, Text.T) annotation (Line(
        points={{-820,-129},{-820,0},{-740,0},{-740,6.8}},
        color={255,204,51},
        thickness=0.5,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(inFlow_Collector.heatPorts[:], Losses_pipes_collector.port_a[1:
      inFlow_Collector.nSeg]) annotation (Line(
        points={{-797,80},{-740,80}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));
    connect(outFlow_Collector.heatPorts[:], Losses_pipes_collector.port_a[
      inFlow_Collector.nSeg + 1:inFlow_Collector.nSeg*2]) annotation (Line(
        points={{-720,157},{-720,80},{-740,80}},
        color={191,0,0},
        smooth=Smooth.None,
        thickness=0.5));

    connect(weaBus.TDryBul, algorithm_control.Text) annotation (Line(
        points={{-820,-129},{-820,-130.588},{-756.948,-130.588}},
        color={255,204,51},
        thickness=0.5,
        smooth=Smooth.None), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(fan.heatPort, Tbefore_hex.port) annotation (Line(
        points={{-170,-26.8},{-160,-26.8},{-160,-50},{-162,-50}},
        color={191,0,0},
        smooth=Smooth.None));
    connect(C5.port_a, DHW_Tank.portHex_b) annotation (Line(
        points={{-540,30},{-540,59.6},{-538,59.6}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Tcold_water.port_b, DHW_Tank.port_b) annotation (Line(
        points={{-480,42},{-480,82},{-490,82}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Tbefore_DHW_tank.port_b, DHW_Tank.portHex_a) annotation (Line(
        points={{-550,116},{-550,71.36},{-538,71.36}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Buffer_Tank.portHex_b, BufferTank_BottomSplitter1.port_2) annotation (
       Line(
        points={{-654,59.4},{-654,60},{-674,60}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(BufferTank_BottomSplitter1.port_3, C6.port_a) annotation (Line(
        points={{-680,54},{-680,30}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Valve_Solar.port_3, BufferTank_BottomSplitter1.port_1) annotation (
        Line(
        points={{-720,-30},{-720,60},{-686,60}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(outFlow_Collector.port_b, BufferTank_TopSplitter.port_1) annotation (
        Line(
        points={{-712,160},{-686,160}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(BufferTank_TopSplitter.port_3, Buffer_Tank.portHex_a) annotation (
        Line(
        points={{-680,154},{-680,70.74},{-654,70.74}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Valve_C2.port_b, HealthTank_SolarBottomSplitter.port_1) annotation (
        Line(
        points={{-360,-12},{-360,-40},{-534,-40}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(HealthTank_SolarBottomSplitter.port_3, Valve_C5.port_b) annotation (
        Line(
        points={{-540,-34},{-540,-12}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(Tcold_water.T, Drawing_up.Te) annotation (Line(
        points={{-486.6,36},{-560,36},{-560,70},{-571.5,70}},
        color={0,0,127},
        smooth=Smooth.None));

    connect(BufferTank_TopSplitter.port_2, SurgeTank.port_a) annotation (Line(
        points={{-674,160},{-640,160},{-640,186}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(HealthTank_SolarBottomSplitter.port_2, Valve_Solar.port_2)
      annotation (Line(
        points={{-546,-40},{-710,-40}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(HealthTank_SolarBottomSplitter.port_2, Valve_C6.port_b) annotation (
        Line(
        points={{-546,-40},{-680,-40},{-680,-12}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(HealthTank_SolarTopSplitter.port_3, Tbefore_DHW_tank.port_a)
      annotation (Line(
        points={{-550,154},{-550,130}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(BufferTank_TopSplitter.port_2, HealthTank_SolarTopSplitter.port_1)
      annotation (Line(
        points={{-674,160},{-556,160}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(HealthTank_SolarTopSplitter.port_2, hex.port_a1) annotation (Line(
        points={{-544,160},{-359.2,160},{-359.2,98}},
        color={0,127,255},
        smooth=Smooth.None));
    connect(algorithm_control.Sj_speed[1], C2.Nrpm) annotation (Line(points={{
            -467.163,-97.8529},{-460,-97.8529},{-460,-98},{-452,-98},{-452,-60},
            {-340,-60},{-340,20},{-348,20}},            color={0,0,127}));
    connect(algorithm_control.S6_speed, C6.Nrpm) annotation (Line(points={{
            -467.163,-105.959},{-458,-105.959},{-458,-106},{-448,-106},{-448,
            -20},{-700,-20},{-700,20},{-692,20}},       color={0,0,127}));
    connect(algorithm_control.S5_speed, C5.Nrpm) annotation (Line(points={{
            -467.163,-114.065},{-456,-114.065},{-456,-114},{-444,-114},{-444,
            -20},{-520,-20},{-520,20},{-528,20}},       color={0,0,127}));
    connect(to_mass_flowRate_mix.y, fan.m_flow_in) annotation (Line(points={{-170,
            -2.4},{-170,-8},{-169.8,-8}}, color={0,0,127}));
    connect(to_mass_flowRate_outside.y, Outside.m_flow_in) annotation (Line(
          points={{-82,-104.4},{-82,-109.4},{-98,-109.4}}, color={0,0,127}));
    connect(density_outside.d, to_mass_flowRate_outside.u1) annotation (Line(
          points={{-64.6,-16},{-72,-16},{-79.6,-16},{-79.6,-95.2}}, color={0,0,
            127}));
    connect(density_mix.d, to_mass_flowRate_mix.u1) annotation (Line(points={{
            -96.6,-6},{-100,-6},{-100,14},{-167.6,14},{-167.6,6.8}}, color={0,0,
            127}));
    connect(algorithm_control.minimal_flowRate[1], to_mass_flowRate_outside.u2)
      annotation (Line(points={{-467.689,-74.4706},{-434,-74.4706},{-434,-60},{
            -140,-60},{-140,-80},{-84.4,-80},{-84.4,-95.2}}, color={0,0,127}));
    connect(algorithm_control.fan_flowRate[1], to_mass_flowRate_mix.u2)
      annotation (Line(points={{-467.689,-65.7412},{-440,-65.7412},{-440,-60},{
            -140,-60},{-140,18},{-172.4,18},{-172.4,6.8}}, color={0,0,127}));
    connect(DHW_Tank.port_a, Drawing_up.tank_port) annotation (Line(points={{-538,
            82},{-552,82},{-552,82.8},{-571.5,82.8}}, color={0,127,255}));
    annotation (Diagram(coordinateSystem(extent={{-840,-180},{180,220}},
            preserveAspectRatio=false)),          Icon(coordinateSystem(
            extent={{-840,-180},{180,220}})),
      experiment(
        StartTime=2.29824e+007,
        StopTime=3.92256e+007,
        Interval=180,
        Tolerance=1e-006,
        __Dymola_Algorithm="Cvode"),
      __Dymola_experimentSetupOutput(doublePrecision=true),
      Documentation(info="<html>
<p>Collectors system configuration must be parallel to have balance between branches.</p>
<p>To change weather configuration file you must use a city_infos compatible table.</p>
<p><br>Change pid values inside Pump_Control to smooth temperature, power and mass flow.</p>
<p>Change to 7kW Elec heating maximum power.</p>
<p>Change to 5 min the time we wait before using the electrical extra heating battery.</p>
<p>Add mixing box to reduce power demands by recycling house air.</p>
<h4>Substance in Air Media model. </h4>
<p>Transform concentration to Kg_Water/Kg_DryAir to Kg_Water/Kg_Air in accordance to media model</p>
<p>Water = 1 Remaining = Air = 2</p>
<p><br>Change Pressure and flow inside each branch. </p>
<ul>
<li>1000 Pa at each valve (Q = 0.15467 Kg/s)</li>
<li>1000 Pa at each equipment (exchanger, tanks) (Q = 0.15467 Kg/s)</li>
</ul>
<p><br>Pressure inside equipment adapted to nominal flow[1] using k value.</p>
<p><br>Change PID to smooth result and reduce computing time.</p>
<p>Correct Pump min and max inside PID. Correct initial value for stay on models allowing to start simulation at any step time.</p>
<p>Change name inside fan control --&GT; refactoring to do inside backup ....</p>
<p>Improve PID for Tinstruction and C2. </p>
<p><br>Limit negative flow inside hex.</p>
<p><br>Add integrator inside tanks exchangers and take T1_out at the end of the pipe after the collector to avoid take account of losses to environment.</p>
<p>Change integrator entry inside collector integrator to take account of gains inside first collector segment. We move Tin from first segment of collector to last segment in the previous pipe.</p>
<h4><span style=\"color: #008000\">Version 5: Use of SolarSystem.Control.IGC_control</span></h4>
<p>Add integrator for storage and health tank.</p>
<p>Add exterior pipes losses integration (complete = gains and losses, or only losses).</p>
<p>Connect tanks losses to ambiant temperature node.</p>
<h4><span style=\"color: #008000\">Version 6: Use of SolarSystem.Control.IGC_control_instruction</span></h4>
<h4>Last modification: 20151128</h4>
<p>Replace ventilation by an hygro B system ---&GT; Allow a reduced flow during unoccupancies (20m3/h) and a minimal during occupancy (90m3/h)</p>
<p>Change of temperature consigne: 16&deg;C during the night and 19&deg;C afternoon.</p>
<p>Allow overheating by solar energy during afternoon (22&deg;C).</p>
<p>Add a specific integrator for tank losses. He compute a classical integration and a integration under conditions (when we need heat or overheat). Used to get the usefull solar tank energy part.</p>
<h4><span style=\"color: #008000\">Version 7: Use of SolarSystem.Control.IGC_control_instruction</span></h4>
<h4>Last modifications: 20151207</h4>
<p>Change infiltrations rate to match 0.4 m3/h/m2 (parois froides)</p>
<p>Change walls compositions and add roof window (Velux). (more on <a href=\"SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive\">SolarSystem.Models.Systems.IGC_system.House.IGC_house_passive</a>)</p>
<p>Change scedules to match RT2012 and Aur&eacute;lie simulations. (more on <a href=\"SolarSystem.Models.Systems.IGC_system.House.IGC_characteristics_passive\">SolarSystem.Models.Systems.IGC_system.House.IGC_characteristics_passive</a>)</p>
<p>Change schedule for temperature instruction to use new generate scheudule (same as before but use less memory and computing time)</p>
<p>Change to pumps parameters to correct error after updating to latest version of Buildings library.</p>
<p>Same with glasses components (must write some components as vector)</p>
<h4><span style=\"color: #008000\">Version 8: </span></h4>
<h4><span style=\"color: #008000\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></h4>
<h4>Last modifications: 20151210</h4>
<p>Change all control algorithm to reduce the number of parameter and clean it up.</p>
<p>Clean all parameters from the whole system</p>
<p>Redefine integrators to limit computation time</p>
<h4><span style=\"color: #008000\">Version 9: </span></h4>
<h4><span style=\"color: #008000\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></h4>
<h4>Last modifications: 20151218</h4>
<p>Remove splitters (keep 3 splitters to cut equation systems)</p>
<p>Use a higher value (20---&GT;100) for the surge tank.</p>
<p>Change heat exchanger initial conditions to reduce non-linearity problems.</p>
<p>Reduce valve Dp to get correct mass flow rate (1000 ---&GT; 200)</p>
<p>Reduce discretization inside pipe for collector losses (24 --&GT; 20) and reduce length of pipes (12 --&GT; 10).</p>
<p>Add a variable selection</p>
<h4><span style=\"color: #008000\">Version 10: </span></h4>
<h4><span style=\"color: #008000\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></h4>
<h4>Last modifications: 20160106</h4>
<p>Change drawing up model.</p>
<p>Change drawing up integrator to match new system</p>
<p><br>Change variable selection to match new system</p>
<h4><span style=\"color: #008000\">Version 11: </span></h4>
<h4><span style=\"color: #008000\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></h4>
<h4>Last modifications: 20160106</h4>
<p>Remove filteredspeed for pumps</p>
<p>Remove filteringOpenning for V3V</p>
<p>Set riseTime for fan to 100 (120 previously)</p>
<p>Change DHW media to pure water (previously water + glycol) ---&GT; 3 Medias (Eau, Eau+Glycol, Air)</p>
<p>Set Dynamic to false for S5 and S6</p>
<p>Set surge tank volume to 150 from 120</p>
<p>Set delta_m to 0.02 from 0.15 (linearization start later)</p>
<p>Set to 0 the pump flow inside storage tank (must clean storage tank model)</p>
<h4><span style=\"color: #008000\">Version 12: </span></h4>
<h4><span style=\"color: #008000\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></h4>
<h4>Last modifications: 20160127</h4>
<p>Filtering for V2V of S2 and S2</p>
<p>No filtering for V2V of S5/S6 and S5/S6</p>
<p>Dynamic for S2 only</p>
<p>Remove T8 sensor</p>
<p>Add splitter on top of DHW_tank</p>
<p>Change collector model to follow buildings models evolution automagically</p>
<p>Check modelica pedantic mode (connect pumps/fans and change how GlycolWater media)</p>
<p><br><b><span style=\"color: #008000;\">Version 13: </span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2; color: #008000;\">Control used: SolarSystem.Control.Electrical_control.Equipements_control</span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20160205</span></b></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">General speed up due to:</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Homotopy for all V2V and not for the V3V</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Change all valves to <a href=\"Buildings.Fluid.Actuators.Valves.TwoWayEqualPercentage\">Buildings.Fluid.Actuators.Valves.TwoWayEqualPercentage</a> based version </span></p>
<p><br><b><span style=\"color: #008000;\">Version 14: </span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2; color: #008000;\">Control used: SolarSystem.Control.Electrical_control.Equipements_control_smooth</span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20160216</span></b></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Smooth min and max used in place of max and min (enable linearization)</span></p>
<ul>
<li><span style=\"font-family: MS Shell Dlg 2;\">Smooth drawing up</span></li>
<li><span style=\"font-family: MS Shell Dlg 2;\">Smooth control</span></li>
</ul>
<p><br><span style=\"font-family: MS Shell Dlg 2;\">Remove unused elements from the house model (create a version with occultations and another without)</span></p>
<p><br><b><span style=\"font-family: MS Shell Dlg 2; color: #008000;\">Version 15: </span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2; color: #008000;\">Control used: SolarSystem.Control.Electrical_control.Equipements_control_smooth_enhanced</span></b></p>
<p><b><span style=\"font-family: MS Shell Dlg 2;\">Last modifications: 20160623</span></b></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Use of smooth to avoid event inside the control algorithm.</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Keep classic `max` function inside drawing up.</span></p>
<p><span style=\"font-family: MS Shell Dlg 2;\">Set Tolerance to 1e-6 as the optimal tolerance value (increase speed compare to 1e-4 due to less step back used by the solver).</span></p>
<p>Fan does not use filter for its speed now to avoid strange air temperature.</p>
<p><br><b><span style=\"font-size: 16pt;\">MAJ of Dymola + Modelica (3.2.1 --&GT; 3.2.2) + Buildings (20160623):</span></b></p>
<ul>
<li>Remove old house model for new from buildings library (Remove internal capacity)</li>
<li>Remove Dynamic balance from every model</li>
<li>Update pump per parameters (N_nominal --&GT; speed_rpm_nominal)</li>
</ul>
</html>"),
      __Dymola_Commands(file="Scripts/Display_script_IGC_AIRvector.mos"
          "Test arguments"));
  end IGC_airVectorRecyclePassive_solar15;

  model Annual_production_pv_area
    "Compute PV production based on thermal collector and pv area as input"

    import SI = Modelica.SIunits;

  protected
    parameter SI.Area floor_area = 98.4 "Floor area of the house";

    // Thermal collector inputs
  public
    parameter Integer collectorNb = 0 "Number of collector used" annotation (Dialog(group="Thermal collector"));
    parameter SI.Distance collectorHeight = 0 "Collector height" annotation (Dialog(group="Thermal collector"));
    parameter SI.Distance collectorWidth = 0 "Collector height" annotation (Dialog(group="Thermal collector"));
    parameter Integer collectorMax = 7 "Collector max number. Used to define max area per orientation" annotation (Dialog(group="Thermal collector"));
    parameter SI.Area collectorArea = collectorHeight*collectorWidth "Collector brut area. Used to define max area per orientation" annotation (Dialog(group="Thermal collector"));

    // PV inputs
  public
    parameter SI.Area PV_area = 0 "PV total area." annotation (Dialog(group="PV"));
    // PV inputs fixed
    parameter Real fAct=0.9 "Fraction of surface area with active solar cells" annotation (Dialog(group="PV"));
    parameter Real eta=0.1582 "Module conversion efficiency" annotation (Dialog(group="PV"));
    parameter Real pf=1 "Power factor required in EU countries" annotation (Dialog(group="PV"));
    parameter Real eta_DCAC=0.954 "Efficiency of DC/AC conversion" annotation (Dialog(group="PV"));

  protected
    parameter SI.Angle til = 0.33004176155213 "Surface tilt" annotation (Dialog(group="PV"));
    parameter SI.Angle lat = weather_file.lattitude "Latitude" annotation (Dialog(group="PV"));

    // Thermal computation
    // Allows thermal collector in south and west.
  public
    parameter SI.Area maxArea = collectorMax * collectorArea "Maximum number of thermal collector for west and south";
    parameter SI.Area collectorMaxArea = min(maxArea, collectorNb * collectorArea) "Thermal collector area in the south roof";
    parameter SI.Area collectorWest = max(0, collectorNb * collectorArea - maxArea) "Collector to west roof.";
  protected
    parameter SI.Area maxAreaEast = maxArea "Maximum number of thermal collector for east (velux)";
    // PV repartition --> This only works if PV_area lower than 3 * maxArea

    // max used to avoid negative value
    // min used to make sure we cannot add more PV than allowed
    parameter SI.Area PV_south = max(0, min(PV_area, maxArea - collectorMaxArea)) "Make sure to maximize south area";
    parameter SI.Area PV_west = max(0, min(maxArea - collectorWest, PV_area - PV_south)) "All roof part are the same";
    parameter SI.Area PV_east = max(0, min(maxAreaEast, PV_area - PV_south - PV_west));

  protected
    Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Weather(
        computeWetBulbTemperature=false,
      filNam=weather_file.path) "Weather datas"
      annotation (Placement(transformation(extent={{100,72},{68,100}})));
  public
    Buildings.Electrical.AC.ThreePhasesBalanced.Loads.Inductive
                                                              Consumer(
      P_nominal=1000,
      V_nominal=230,
      mode=Buildings.Electrical.Types.Load.FixedZ_steady_state)
      "Consumes the power generted by the PVs or grid if PVs not enough"
      annotation (Placement(transformation(extent={{68,-44},{88,-24}})));
  protected
    Buildings.Electrical.AC.ThreePhasesBalanced.Sources.Grid
                                                           grid(f=50, V=230)
      "Electrical grid model"
      annotation (Placement(transformation(extent={{26,-38},{50,-62}})));
  public
    Buildings.Electrical.AC.ThreePhasesBalanced.Sources.PVSimpleOriented
                                                                       pv_WEST(
      lat=lat,
      pf=pf,
      eta_DCAC=eta_DCAC,
      fAct=fAct,
      eta=eta,
      til=til,
      A=PV_west,
      azi=1.5707963267949,
      V_nominal=230) "PV array oriented"
      annotation (Placement(transformation(extent={{8,0},{28,20}})));
  protected
    Modelica.Blocks.Continuous.Integrator pv_production(
      k=1,
      initType=Modelica.Blocks.Types.Init.InitialState,
      y_start=0,
      u(unit="W"),
      y(unit="J")) "Heating energy in Joules"
      annotation (Placement(transformation(extent={{72,4},{84,16}})));
  public
    Buildings.Electrical.AC.ThreePhasesBalanced.Sources.PVSimpleOriented
                                                                       pv_EAST(
      lat=lat,
      pf=pf,
      eta_DCAC=eta_DCAC,
      fAct=fAct,
      eta=eta,
      til=til,
      A=PV_east,
      V_nominal=230,
      azi=-1.5707963267949) "PV array oriented East"
      annotation (Placement(transformation(extent={{8,-30},{28,-10}})));
    Buildings.Electrical.AC.ThreePhasesBalanced.Sources.PVSimpleOriented
                                                                       pv_SOUTH(
      azi=0,
      lat=lat,
      pf=pf,
      eta_DCAC=eta_DCAC,
      fAct=fAct,
      eta=eta,
      til=til,
      A=PV_south,
      V_nominal=230) "PV array oriented South"
      annotation (Placement(transformation(extent={{8,30},{28,50}})));
  protected
    Modelica.Blocks.Math.Sum sum_gains1(nin=3, k={1,1,1})
      annotation (__Dymola_tag={"Power", "Internal"}, Placement(transformation(extent={{46,4},{
              58,16}})));
  public
    Buildings.BoundaryConditions.WeatherData.Bus
                                       weaBus "Weather data" annotation (
       Placement(transformation(extent={{32,60},{60,86}}),   iconTransformation(
            extent={{-10,88},{10,108}})));
    Modelica.Blocks.Interfaces.RealOutput PV_Energy "PVs total solar production" annotation (__Dymola_tag={"Solar", "Energy"},
        Placement(transformation(extent={{96,-4},{124,24}}), iconTransformation(extent={{80,40},{120,
              80}})));
  protected
    replaceable Data.Parameter.ParametricStudy201611.Weathers.Strasbourg
                                                   weather_file constrainedby
      Data.Parameter.City_Datas.City_Data
      annotation (Placement(transformation(extent={{-138,80},{-120,98}})));

  public
    Modelica.Blocks.Sources.CombiTimeTable schedules(
      extrapolation=Modelica.Blocks.Types.Extrapolation.Periodic,
      smoothness=Modelica.Blocks.Types.Smoothness.ConstantSegments,
      table=schedule_table)
      "1 = Occupation, 2 = lumi�res, 3 = equipements, 4=occultation hiver, 5=occultation ete est, 6=occultation ete ouest"
      annotation (Placement(transformation(extent={{-140,-10},{-120,10}})));
  protected
    Modelica.Blocks.Math.Gain Coef[3](k={1,floor_area,floor_area})
      "Gain for electrical internal loads"
      annotation (Placement(transformation(extent={{-110,-4},{-102,4}})));
  public
    Modelica.Blocks.Math.Sum sum(nin=2) annotation (__Dymola_tag={"Power",
          "Internal"}, Placement(transformation(extent={{-72,-6},{-60,6}})));
  public
    Modelica.Blocks.Continuous.Integrator ConsoElectrique(
      k=1,
      initType=Modelica.Blocks.Types.Init.InitialState,
      y_start=0,
      u(unit="W"),
      y(unit="J"))
      annotation (Placement(transformation(extent={{-46,-8},{-30,8}})));
  public
    Modelica.Blocks.Continuous.Integrator ConsoEclairage(
      k=1,
      initType=Modelica.Blocks.Types.Init.InitialState,
      y_start=0,
      u(unit="W"),
      y(unit="J"))
      annotation (Placement(transformation(extent={{-74,-36},{-58,-20}})));
  public
    Modelica.Blocks.Continuous.Integrator ConsoElectromenager(
      k=1,
      initType=Modelica.Blocks.Types.Init.InitialState,
      y_start=0,
      u(unit="W"),
      y(unit="J"))
      annotation (Placement(transformation(extent={{-74,-62},{-58,-46}})));
  public
    Modelica.Blocks.Continuous.Integrator ProdOccupants(
      k=1,
      initType=Modelica.Blocks.Types.Init.InitialState,
      y_start=0,
      u(unit="W"),
      y(unit="J"))
      annotation (Placement(transformation(extent={{-76,20},{-60,36}})));
    parameter Real schedule_table[:,:]=[0,390,0,1.14,1,1,1; 3600,390,0,1.14,1,1,1; 7200,390,
        0,1.14,1,1,1; 10800,390,0,1.14,1,1,1; 14400,390,0,1.14,1,1,1; 18000,390,0,
        1.14,1,1,1; 21600,390,0,5.7,1,1,1; 25200,390,1.4,5.7,0,0,0; 28800,390,1.4,
        5.7,0,1,0.7; 32400,390,0,5.7,0,1,0.7; 36000,0,0,1.14,0,1,0.7; 39600,0,0,1.14,
        0,1,0.7; 43200,0,0,1.14,0,1,0.7; 46800,0,0,1.14,0,0.7,1; 50400,0,0,1.14,0,
        0.7,1; 54000,0,0,1.14,0,0.7,1; 57600,0,0,1.14,0,0.7,1; 61200,0,0,1.14,0,0.7,
        1; 64800,390,0,5.7,0,0,0; 68400,390,1.4,5.7,0,0,0; 72000,390,1.4,5.7,0,0,0;
        75600,390,1.4,5.7,0,0,0; 79200,390,0,1.14,1,1,1; 82800,390,0,1.14,1,1,1; 86400,
        390,0,1.14,1,1,1; 90000,390,0,1.14,1,1,1; 93600,390,0,1.14,1,1,1; 97200,390,
        0,1.14,1,1,1; 100800,390,0,1.14,1,1,1; 104400,390,0,1.14,1,1,1; 108000,390,
        0,5.7,1,1,1; 111600,390,1.4,5.7,0,0,0; 115200,390,1.4,5.7,0,1,0.7; 118800,
        390,0,5.7,0,1,0.7; 122400,0,0,1.14,0,1,0.7; 126000,0,0,1.14,0,1,0.7; 129600,
        0,0,1.14,0,1,0.7; 133200,0,0,1.14,0,0.7,1; 136800,0,0,1.14,0,0.7,1; 140400,
        0,0,1.14,0,0.7,1; 144000,0,0,1.14,0,0.7,1; 147600,0,0,1.14,0,0.7,1; 151200,
        390,0,5.7,0,0,0; 154800,390,1.4,5.7,0,0,0; 158400,390,1.4,5.7,0,0,0; 162000,
        390,1.4,5.7,0,0,0; 165600,390,0,1.14,1,1,1; 169200,390,0,1.14,1,1,1; 172800,
        390,0,1.14,1,1,1; 176400,390,0,1.14,1,1,1; 180000,390,0,1.14,1,1,1; 183600,
        390,0,1.14,1,1,1; 187200,390,0,1.14,1,1,1; 190800,390,0,1.14,1,1,1; 194400,
        390,0,5.7,1,1,1; 198000,390,1.4,5.7,0,0,0; 201600,390,1.4,5.7,0,1,0.7; 205200,
        390,0,5.7,0,1,0.7; 208800,0,0,1.14,0,1,0.7; 212400,0,0,1.14,0,1,0.7; 216000,
        0,0,1.14,0,1,0.7; 219600,0,0,1.14,0,0.7,1; 223200,390,0,5.7,0,0.7,1; 226800,
        390,0,5.7,0,0.7,1; 230400,390,0,5.7,0,0.7,1; 234000,390,0,5.7,0,0.7,1; 237600,
        390,0,5.7,0,0,0; 241200,390,1.4,5.7,0,0,0; 244800,390,1.4,5.7,0,0,0; 248400,
        390,1.4,5.7,0,0,0; 252000,390,0,1.14,1,1,1; 255600,390,0,1.14,1,1,1; 259200,
        390,0,1.14,1,1,1; 262800,390,0,1.14,1,1,1; 266400,390,0,1.14,1,1,1; 270000,
        390,0,1.14,1,1,1; 273600,390,0,1.14,1,1,1; 277200,390,0,1.14,1,1,1; 280800,
        390,0,5.7,1,1,1; 284400,390,1.4,5.7,0,0,0; 288000,390,1.4,5.7,0,1,0.7; 291600,
        390,0,5.7,0,1,0.7; 295200,0,0,1.14,0,1,0.7; 298800,0,0,1.14,0,1,0.7; 302400,
        0,0,1.14,0,1,0.7; 306000,0,0,1.14,0,0.7,1; 309600,0,0,1.14,0,0.7,1; 313200,
        0,0,1.14,0,0.7,1; 316800,0,0,1.14,0,0.7,1; 320400,0,0,1.14,0,0.7,1; 324000,
        390,0,5.7,0,0,0; 327600,390,1.4,5.7,0,0,0; 331200,390,1.4,5.7,0,0,0; 334800,
        390,1.4,5.7,0,0,0; 338400,390,0,1.14,1,1,1; 342000,390,0,1.14,1,1,1; 345600,
        390,0,1.14,1,1,1; 349200,390,0,1.14,1,1,1; 352800,390,0,1.14,1,1,1; 356400,
        390,0,1.14,1,1,1; 360000,390,0,1.14,1,1,1; 363600,390,0,1.14,1,1,1; 367200,
        390,0,5.7,1,1,1; 370800,390,1.4,5.7,0,0,0; 374400,390,1.4,5.7,0,1,0.7; 378000,
        390,0,5.7,0,1,0.7; 381600,0,0,1.14,0,1,0.7; 385200,0,0,1.14,0,1,0.7; 388800,
        0,0,1.14,0,1,0.7; 392400,0,0,1.14,0,0.7,1; 396000,0,0,1.14,0,0.7,1; 399600,
        0,0,1.14,0,0.7,1; 403200,0,0,1.14,0,0.7,1; 406800,0,0,1.14,0,0.7,1; 410400,
        390,0,5.7,0,0,0; 414000,390,1.4,5.7,0,0,0; 417600,390,1.4,5.7,0,0,0; 421200,
        390,1.4,5.7,0,0,0; 424800,390,0,1.14,1,1,1; 428400,390,0,1.14,1,1,1; 432000,
        390,0,1.14,1,1,1; 435600,390,0,1.14,1,1,1; 439200,390,0,1.14,1,1,1; 442800,
        390,0,1.14,1,1,1; 446400,390,0,1.14,1,1,1; 450000,390,0,1.14,1,1,1; 453600,
        390,1.4,5.7,1,1,1; 457200,390,1.4,5.7,0,0,0; 460800,390,0.7,5.7,0,1,0.7; 464400,
        390,0.7,5.7,0,1,0.7; 468000,390,0.7,5.7,0,1,0.7; 471600,390,0.7,5.7,0,1,0.7;
        475200,390,0.7,5.7,0,1,0.7; 478800,390,0.7,5.7,0,0.7,1; 482400,390,0.7,5.7,
        0,0.7,1; 486000,390,0.7,5.7,0,0.7,1; 489600,390,0.7,5.7,0,0.7,1; 493200,390,
        0.7,5.7,0,0.7,1; 496800,390,0.7,5.7,0,0,0; 500400,390,1.4,5.7,0,0,0; 504000,
        390,1.4,5.7,0,0,0; 507600,390,1.4,5.7,0,0,0; 511200,390,0,1.14,1,1,1; 514800,
        390,0,1.14,1,1,1; 518400,390,0,1.14,1,1,1; 522000,390,0,1.14,1,1,1; 525600,
        390,0,1.14,1,1,1; 529200,390,0,1.14,1,1,1; 532800,390,0,1.14,1,1,1; 536400,
        390,0,1.14,1,1,1; 540000,390,1.4,5.7,1,1,1; 543600,390,1.4,5.7,0,0,0; 547200,
        390,0.7,5.7,0,1,0.7; 550800,390,0.7,5.7,0,1,0.7; 554400,390,0.7,5.7,0,1,0.7;
        558000,390,0.7,5.7,0,1,0.7; 561600,390,0.7,5.7,0,1,0.7; 565200,390,0.7,5.7,
        0,0.7,1; 568800,390,0.7,5.7,0,0.7,1; 572400,390,0.7,5.7,0,0.7,1; 576000,390,
        0.7,5.7,0,0.7,1; 579600,390,0.7,5.7,0,0.7,1; 583200,390,0.7,5.7,0,0,0; 586800,
        390,1.4,5.7,0,0,0; 590400,390,1.4,5.7,0,0,0; 594000,390,1.4,5.7,0,0,0; 597600,
        390,0,1.14,1,1,1; 601200,390,0,1.14,1,1,1; 604800,390,0,1.14,1,1,1]
      "Table matrix (time = first column; e.g., table=[0,2])";
    Modelica.Blocks.Interfaces.RealOutput Occupants_Energy
      "Inhabitants total loads" annotation (__Dymola_tag={"Solar","Energy"},
        Placement(transformation(
          extent={{-14,-14},{14,14}},
          rotation=90,
          origin={-94,94}), iconTransformation(extent={{80,40},{120,80}})));
    Modelica.Blocks.Interfaces.RealOutput ApportElectrique_Energy
      "Electric total consumption" annotation (__Dymola_tag={"Solar","Energy"},
        Placement(transformation(
          extent={{-14,-14},{14,14}},
          rotation=90,
          origin={-70,94}), iconTransformation(extent={{80,40},{120,80}})));
    Modelica.Blocks.Interfaces.RealOutput Eclairage_Energy
      "Lights total consumption" annotation (__Dymola_tag={"Solar","Energy"},
        Placement(transformation(
          extent={{-14,-14},{14,14}},
          rotation=90,
          origin={-48,94}), iconTransformation(extent={{80,40},{120,80}})));
    Modelica.Blocks.Interfaces.RealOutput Electromenager_Energy
      "Equipments total consumption" annotation (__Dymola_tag={"Solar","Energy"},
        Placement(transformation(
          extent={{-14,-14},{14,14}},
          rotation=90,
          origin={-24,94}), iconTransformation(extent={{80,40},{120,80}})));
  equation
    connect(grid.terminal,Consumer. terminal) annotation (Line(
        points={{38,-38},{38,-34},{68,-34}},
        color={0,120,120},
        smooth=Smooth.None));
    connect(pv_SOUTH.P,sum_gains1. u[1]) annotation (Line(points={{29,47},{40,47},
            {40,9.2},{44.8,9.2}},   color={0,0,127}));
    connect(pv_WEST.P,sum_gains1. u[2]) annotation (Line(points={{29,17},{40,17},{
            40,10},{44.8,10}},  color={0,0,127}));
    connect(pv_EAST.P,sum_gains1. u[3]) annotation (Line(points={{29,-13},{40,-13},
            {40,10.8},{44.8,10.8}}, color={0,0,127}));
    connect(sum_gains1.y,pv_production. u)
      annotation (Line(points={{58.6,10},{58.6,10},{70.8,10}},
                                                     color={0,0,127}));
    connect(pv_SOUTH.terminal,Consumer. terminal)
      annotation (Line(points={{8,40},{-8,40},{-8,-34},{68,-34}},     color={0,120,120}));
    connect(pv_WEST.terminal,Consumer. terminal)
      annotation (Line(points={{8,10},{-4,10},{-4,-34},{68,-34}},   color={0,120,120}));
    connect(pv_EAST.terminal,Consumer. terminal) annotation (Line(points={{8,-20},
            {0,-20},{0,-34},{68,-34}},
                       color={0,120,120}));
    connect(pv_production.y,PV_Energy)
      annotation (Line(points={{84.6,10},{88,10},{110,10}},       color={0,0,127}));
    connect(weaBus,pv_EAST. weaBus) annotation (Line(
        points={{46,73},{48,73},{48,64},{0,64},{0,-6},{18,-6},{18,-11}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(weaBus,pv_WEST. weaBus) annotation (Line(
        points={{46,73},{48,73},{48,64},{0,64},{0,24},{18,24},{18,19}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(weaBus,pv_SOUTH. weaBus) annotation (Line(
        points={{46,73},{48,73},{48,64},{0,64},{0,56},{18,56},{18,49}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%first",
        index=-1,
        extent={{-6,3},{-6,3}}));
    connect(Weather.weaBus, weaBus) annotation (Line(
        points={{68,86},{46,86},{46,73},{46,73}},
        color={255,204,51},
        thickness=0.5), Text(
        string="%second",
        index=1,
        extent={{6,3},{6,3}}));
    connect(sum.y, ConsoElectrique.u)
      annotation (Line(points={{-59.4,0},{-47.6,0}},           color={0,0,127}));
    connect(Coef[1].y, ProdOccupants.u) annotation (Line(points={{-101.6,0},{-86,
            0},{-86,28},{-77.6,28}},
                                  color={0,0,127}));
    connect(Coef[2].y, ConsoEclairage.u) annotation (Line(points={{-101.6,0},{-86,
            0},{-86,-28},{-75.6,-28}},
                                    color={0,0,127}));
    connect(Coef[3].y, ConsoElectromenager.u) annotation (Line(points={{-101.6,0},
            {-86,0},{-86,-54},{-75.6,-54}},color={0,0,127}));
    connect(schedules.y[1], Coef[1].u) annotation (Line(points={{-119,0},{-114,0},
            {-110.8,0}}, color={0,0,127}));
    connect(schedules.y[2], Coef[2].u) annotation (Line(points={{-119,0},{-114,0},
            {-110.8,0}}, color={0,0,127}));
    connect(schedules.y[3], Coef[3].u) annotation (Line(points={{-119,0},{-114,0},
            {-110.8,0}}, color={0,0,127}));
    connect(Coef[2].y, sum.u[1]) annotation (Line(points={{-101.6,0},{-73.2,0},{
            -73.2,-0.6}},
                    color={0,0,127}));
    connect(Coef[3].y, sum.u[2]) annotation (Line(points={{-101.6,0},{-73.2,0},{
            -73.2,0.6}},
                   color={0,0,127}));
    connect(ProdOccupants.y, Occupants_Energy) annotation (Line(points={{-59.2,28},
            {-56,28},{-56,54},{-94,54},{-94,94}}, color={0,0,127}));
    connect(ConsoElectrique.y, ApportElectrique_Energy) annotation (Line(points={{
            -29.2,0},{-28,0},{-28,58},{-70,58},{-70,94}}, color={0,0,127}));
    connect(ConsoEclairage.y, Eclairage_Energy) annotation (Line(points={{-57.2,
            -28},{-40,-28},{-22,-28},{-22,64},{-48,64},{-48,94}}, color={0,0,127}));
    connect(ConsoElectromenager.y, Electromenager_Energy) annotation (Line(points=
           {{-57.2,-54},{-18,-54},{-18,72},{-24,72},{-24,94}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-140,-100},
              {100,100}})),                                        Diagram(
          coordinateSystem(preserveAspectRatio=false, extent={{-140,-100},{100,100}}),
          graphics={Text(
            extent={{-130,-68},{-32,-84}},
            lineColor={28,108,200},
            textString="House loads",
            textStyle={TextStyle.Bold}), Text(
            extent={{-4,-68},{94,-84}},
            lineColor={28,108,200},
            textString="PV production",
            textStyle={TextStyle.Bold})}),
      Documentation(info="<html>
<p>Mode that compute the collector production according to PV area and Thermal collector area.</p>
</html>"));
  end Annual_production_pv_area;
end Backup;
