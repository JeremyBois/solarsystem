# SolarSystem Bibliothèque:

***Mise à jour: 20150715***

## Bibliothèque modelica:
Cette bibliothèque permet de rapidement modéliser des systèmes solaires et des
algorithmes de contrôle grâce aux outils disponibles.

## État du dossier Models:
Ce dossier regroupe les modélisations des systèmes solaires et des maisons.

### Modèle SolarSystem.Models.Systems.IGC_system.System.IGC_airVectorRecyclePassive_solar5:
    - bordeauxAirRecycle-4p_20150702

### Modèle IGC_airVectorRecyclePassive_solar_MeteoFrance3:
    - bordeauxAirRecycleM2012-4p_20150703
