within ;
package SolarSystem "Some solar system"
  extends Modelica.Icons.Package;


  annotation (uses(              Buildings(version="3.0.0"), Modelica(version=
          "3.2.2")),
    version="0.7",
    conversion(from(
        version="1",
        script="ConvertFromSolarSystem_1.mos",
        version=""), noneFromVersion="2",
    noneFromVersion="0.5"),
  Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
          100}}), graphics={Polygon(
        points={{16,74},{16,74}},
        lineColor={0,0,255},
        smooth=Smooth.None,
        fillColor={0,0,255},
        fillPattern=FillPattern.Solid)}),
  Documentation(info="<html>
</html>"));
end SolarSystem;
