within SolarSystem.House.BaseClasses;
partial model RoomHeatMassBalanceSimple "Base model for a room"
  extends SolarSystem.House.BaseClasses.ConstructionRecordsNoWindow;

  replaceable package Medium =
    Modelica.Media.Interfaces.PartialMedium "Medium in the component"
      annotation (choicesAllMatching = true);

  parameter Integer nPorts=0 "Number of ports" annotation (Evaluate=true,
      Dialog(
      connectorSizing=true,
      tab="General",
      group="Ports"));
  Modelica.Fluid.Vessels.BaseClasses.VesselFluidPorts_b ports[nPorts](
      redeclare each package Medium = Medium) "Fluid inlets and outlets"
    annotation (Placement(transformation(
        extent={{-40,-10},{40,10}},
        origin={-260,-60},
        rotation=90), iconTransformation(
        extent={{-40,-10},{40,10}},
        rotation=90,
        origin={-150,-100})));
  parameter Modelica.SIunits.Angle lat "Latitude";
  final parameter Modelica.SIunits.Volume V=AFlo*hRoo "Volume";
  parameter Modelica.SIunits.Area AFlo "Floor area";
  parameter Modelica.SIunits.Length hRoo "Average room height";

  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a heaPorAir
    "Heat port to air volume" annotation (Placement(transformation(extent={{-270,30},
            {-250,50}}),   iconTransformation(extent={{-20,-10},{0,10}})));
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a heaPorRad
    "Heat port for radiative heat gain and radiative temperature" annotation (
      Placement(transformation(extent={{-270,-10},{-250,10}}),
                                                           iconTransformation(
          extent={{-20,-48},{0,-28}})));
  ////////////////////////////////////////////////////////////////////////
  // Constructions
  Buildings.Rooms.Constructions.Construction conExt[NConExt](
    A=datConExt.A,
    til=datConExt.til,
    final layers={datConExt[i].layers for i in 1:NConExt},
    steadyStateInitial=datConExt.steadyStateInitial,
    T_a_start=datConExt.T_a_start,
    T_b_start=datConExt.T_b_start) if haveConExt
    "Heat conduction through exterior construction that have no window"
    annotation (Placement(transformation(extent={{288,100},{242,146}})));
  Buildings.Rooms.Constructions.Construction conPar[NConPar](
    A=datConPar.A,
    til=datConPar.til,
    final layers={datConPar[i].layers for i in 1:NConPar},
    steadyStateInitial=datConPar.steadyStateInitial,
    T_a_start=datConPar.T_a_start,
    T_b_start=datConPar.T_b_start) if haveConPar
    "Heat conduction through partitions that have both sides inside the thermal zone"
    annotation (Placement(transformation(extent={{282,-122},{244,-84}})));
  Buildings.Rooms.Constructions.Construction conBou[NConBou](
    A=datConBou.A,
    til=datConBou.til,
    final layers={datConBou[i].layers for i in 1:NConBou},
    steadyStateInitial=datConBou.steadyStateInitial,
    T_a_start=datConBou.T_a_start,
    T_b_start=datConBou.T_b_start) if haveConBou
    "Heat conduction through opaque constructions that have the boundary conditions of the other side exposed"
    annotation (Placement(transformation(extent={{282,-156},{242,-116}})));
  parameter Boolean linearizeRadiation=true
    "Set to true to linearize emissive power";
  ////////////////////////////////////////////////////////////////////////
  // Convection
  parameter Buildings.HeatTransfer.Types.InteriorConvection intConMod=Buildings.HeatTransfer.Types.InteriorConvection.Temperature
    "Convective heat transfer model for room-facing surfaces of opaque constructions"
    annotation (Dialog(group="Convective heat transfer"));
  parameter Modelica.SIunits.CoefficientOfHeatTransfer hIntFixed=3.0
    "Constant convection coefficient for room-facing surfaces of opaque constructions"
    annotation (Dialog(group="Convective heat transfer", enable=(conMod ==
          Buildings.HeatTransfer.Types.InteriorConvection.Fixed)));
  parameter Buildings.HeatTransfer.Types.ExteriorConvection extConMod=Buildings.HeatTransfer.Types.ExteriorConvection.TemperatureWind
    "Convective heat transfer model for exterior facing surfaces of opaque constructions"
    annotation (Dialog(group="Convective heat transfer"));
  parameter Modelica.SIunits.CoefficientOfHeatTransfer hExtFixed=10.0
    "Constant convection coefficient for exterior facing surfaces of opaque constructions"
    annotation (Dialog(group="Convective heat transfer", enable=(conMod ==
          Buildings.HeatTransfer.Types.ExteriorConvection.Fixed)));
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal(min=0) = V*1.2/3600
    "Nominal mass flow rate" annotation (Dialog(group="Nominal condition"));
  parameter Boolean homotopyInitialization = true "= true, use homotopy method"
    annotation(Evaluate=true, Dialog(tab="Advanced"));
  ////////////////////////////////////////////////////////////////////////
  // Models for boundary conditions
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a surf_conBou[nConBou] if
    haveConBou "Heat port at surface b of construction conBou" annotation (
      Placement(transformation(extent={{-270,-190},{-250,-170}}),
        iconTransformation(extent={{50,-170},{70,-150}})));
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a surf_surBou[nSurBou] if
    haveSurBou "Heat port of surface that is connected to the room air"
    annotation (Placement(transformation(extent={{-270,-150},{-250,-130}}),
        iconTransformation(extent={{-48,-150},{-28,-130}})));
  // Reassign the tilt since a construction that is declared as a ceiling of the
  // room model has an exterior-facing surface that is a floor
  Buildings.Rooms.BaseClasses.ExteriorBoundaryConditions bouConExt(
    final nCon=nConExt,
    final lat=lat,
    linearizeRadiation=linearizeRadiation,
    final conMod=extConMod,
    final conPar=datConExt,
    final hFixed=hExtFixed) if haveConExt
    "Exterior boundary conditions for constructions without a window"
    annotation (Placement(transformation(extent={{352,114},{382,144}})));
  // Reassign the tilt since a construction that is declared as a ceiling of the
  // room model has an exterior-facing surface that is a floor

  Buildings.BoundaryConditions.WeatherData.Bus weaBus annotation (Placement(
        transformation(extent={{170,150},{190,170}}), iconTransformation(extent=
           {{166,166},{192,192}})));

  replaceable MixedAirHeatMassBalanceSimple air constrainedby
    SolarSystem.House.BaseClasses.PartialAirHeatMassBalanceSimple(
    redeclare final package Medium = Medium,
    nPorts=nPorts,
    final nConExt=nConExt,
    final nConPar=nConPar,
    final nConBou=nConBou,
    final nSurBou=nSurBou,
    final datConExt=datConExt,
    final datConPar=datConPar,
    final datConBou=datConBou,
    final surBou=surBou,
    final V=V) "Convective heat and mass balance of air"
    annotation (Placement(transformation(extent={{40,-142},{64,-118}})));

  Buildings.Rooms.BaseClasses.InfraredRadiationGainDistribution irRadGai(
    final nConExt=nConExt,
    final nConExtWin=nConExtWin,
    final nConPar=nConPar,
    final nConBou=nConBou,
    final nSurBou=nSurBou,
    final datConExtWin=datConExtWin,
    final datConExt = datConExt,
    final datConPar = datConPar,
    final datConBou = datConBou,
    final surBou = surBou,
    final haveShade=false)
    "Distribution for infrared radiative heat gains (e.g., due to equipment and people)"
    annotation (Placement(transformation(extent={{-100,-40},{-80,-20}})));
  Buildings.Rooms.BaseClasses.InfraredRadiationExchange irRadExc(
    final nConExt=nConExt,
    final nConExtWin=nConExtWin,
    final nConPar=nConPar,
    final nConBou=nConBou,
    final nSurBou=nSurBou,
    final datConExtWin=datConExtWin,
    final datConExt = datConExt,
    final datConPar = datConPar,
    final datConBou = datConBou,
    final surBou = surBou,
    final linearizeRadiation = linearizeRadiation,
    final homotopyInitialization = homotopyInitialization)
    "Infrared radiative heat exchange"
    annotation (Placement(transformation(extent={{-100,0},{-80,20}})));

  Buildings.Rooms.BaseClasses.RadiationTemperature radTem(
    final nConExt=nConExt,
    final nConExtWin=nConExtWin,
    final nConPar=nConPar,
    final nConBou=nConBou,
    final nSurBou=nSurBou,
    final datConExtWin=datConExtWin,
    final datConExt=datConExt,
    final datConPar=datConPar,
    final datConBou=datConBou,
    final surBou=surBou,
    final haveShade=false) "Radiative temperature of the room"
    annotation (Placement(transformation(extent={{-100,-80},{-80,-60}})));

protected
  final parameter Boolean isFloorConExt[NConExt]=
    datConExt.isFloor "Flag to indicate if floor for exterior constructions";
  final parameter Boolean isFloorConPar_a[NConPar]=
    datConPar.isFloor "Flag to indicate if floor for constructions";
  final parameter Boolean isFloorConPar_b[NConPar]=
    datConPar.isCeiling "Flag to indicate if floor for constructions";
  final parameter Boolean isFloorConBou[NConBou]=
    datConBou.isFloor
    "Flag to indicate if floor for constructions with exterior boundary conditions exposed to outside of room model";
  parameter Boolean isFloorSurBou[NSurBou]=
    surBou.isFloor
    "Flag to indicate if floor for constructions that are modeled outside of this room";

  Buildings.Rooms.BaseClasses.RadiationAdapter radiationAdapter
    annotation (Placement(transformation(extent={{-180,120},{-160,140}})));

equation
  connect(conBou.opa_a, surf_conBou) annotation (Line(
      points={{282,-122.667},{282,-122},{288,-122},{288,-216},{-240,-216},{-240,
          -180},{-260,-180}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(conExt.opa_a, bouConExt.opa_a) annotation (Line(
      points={{288,138.333},{334,138.333},{334,139},{352,139}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(weaBus, bouConExt.weaBus) annotation (Line(
      points={{180,160},{400,160},{400,130},{378.15,130},{378.15,130.05}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
 // Connect statements from the model BaseClasses.MixedAir
  connect(conExt.opa_b, irRadExc.conExt) annotation (Line(
      points={{241.847,138.333},{160,138.333},{160,60},{-60,60},{-60,20},{-80,
          20},{-80,19.1667}},
      color={190,0,0},
      smooth=Smooth.None));
  connect(conPar.opa_a, irRadExc.conPar_a) annotation (Line(
      points={{282,-90.3333},{288,-90.3333},{288,-106},{160,-106},{160,60},{-60,
          60},{-60,8},{-80,8},{-80,7.5},{-79.9167,7.5}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(conPar.opa_b, irRadExc.conPar_b) annotation (Line(
      points={{243.873,-90.3333},{160,-90.3333},{160,60},{-60,60},{-60,5.83333},
          {-79.9167,5.83333}},
      color={191,0,0},
      smooth=Smooth.None));

  connect(conBou.opa_b, irRadExc.conBou) annotation (Line(
      points={{241.867,-122.667},{160,-122.667},{160,60},{-60,60},{-60,3.33333},
          {-79.9167,3.33333}},
      color={191,0,0},
      smooth=Smooth.None));

  connect(surf_surBou, irRadExc.conSurBou) annotation (Line(
      points={{-260,-140},{-232,-140},{-232,-210},{160,-210},{160,60},{-60,60},
          {-60,0.833333},{-79.9583,0.833333}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(irRadGai.conExt, conExt.opa_b) annotation (Line(
      points={{-80,-20.8333},{-80,-20},{-60,-20},{-60,60},{160,60},{160,138.333},
          {241.847,138.333}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(irRadGai.conPar_a, conPar.opa_a) annotation (Line(
      points={{-79.9167,-32.5},{-60,-32.5},{-60,60},{160,60},{160,-106},{288,
          -106},{288,-90.3333},{282,-90.3333}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(irRadGai.conPar_b, conPar.opa_b) annotation (Line(
      points={{-79.9167,-34.1667},{-60,-34.1667},{-60,60},{160,60},{160,
          -90.3333},{243.873,-90.3333}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(irRadGai.conBou, conBou.opa_b) annotation (Line(
      points={{-79.9167,-36.6667},{-60,-36.6667},{-60,60},{160,60},{160,
          -122.667},{241.867,-122.667}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(irRadGai.conSurBou, surf_surBou) annotation (Line(
      points={{-79.9583,-39.1667},{-60,-39.1667},{-60,60},{160,60},{160,-210},{
          -232,-210},{-232,-140},{-260,-140}},
      color={191,0,0},
      smooth=Smooth.None));

  connect(conExt.opa_b, radTem.conExt) annotation (Line(
      points={{241.847,138.333},{160,138.333},{160,60},{-60,60},{-60,-60.8333},
          {-80,-60.8333}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(conPar.opa_a, radTem.conPar_a) annotation (Line(
      points={{282,-90.3333},{288,-90.3333},{288,-106},{160,-106},{160,60},{-60,
          60},{-60,-72.5},{-79.9167,-72.5}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(conPar.opa_b, radTem.conPar_b) annotation (Line(
      points={{243.873,-90.3333},{160,-90.3333},{160,60},{-60,60},{-60,-74.1667},
          {-79.9167,-74.1667}},
      color={191,0,0},
      smooth=Smooth.None));

  connect(conBou.opa_b, radTem.conBou) annotation (Line(
      points={{241.867,-122.667},{160,-122.667},{160,60},{-60,60},{-60,-76.6667},
          {-79.9167,-76.6667}},
      color={191,0,0},
      smooth=Smooth.None));

  connect(surf_surBou, radTem.conSurBou) annotation (Line(
      points={{-260,-140},{-232,-140},{-232,-210},{160,-210},{160,60},{-60,60},
          {-60,-79.1667},{-79.9583,-79.1667}},
      color={191,0,0},
      smooth=Smooth.None));

  connect(radTem.TRad, radiationAdapter.TRad) annotation (Line(
      points={{-100.417,-77.6667},{-144,-77.6667},{-144,-78},{-186,-78},{-186,
          130},{-182,130}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(radiationAdapter.rad, heaPorRad)
                                     annotation (Line(
      points={{-170.2,120},{-170,120},{-170,114},{-226,114},{-226,4.44089e-16},
          {-260,4.44089e-16}},
      color={191,0,0},
      smooth=Smooth.None));

  for i in 1:nPorts loop
    connect(ports[i],air. ports[i])
                                  annotation (Line(
      points={{-260,-60},{-216,-60},{-216,-206},{52,-206},{52,-141.9}},
      color={0,127,255},
      smooth=Smooth.None));
  end for;

  connect(air.conExt, conExt.opa_b) annotation (Line(
      points={{64,-119},{160,-119},{160,138.333},{241.847,138.333}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(air.conPar_a, conPar.opa_a) annotation (Line(
      points={{64.1,-133},{160,-133},{160,-106},{288,-106},{288,-90.3333},{282,
          -90.3333}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(air.conPar_b, conPar.opa_b) annotation (Line(
      points={{64.1,-135},{160,-135},{160,-90},{243.873,-90},{243.873,-90.3333}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(air.conBou, conBou.opa_b) annotation (Line(
      points={{64.1,-138},{160,-138},{160,-122.667},{241.867,-122.667}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(air.conSurBou, surf_surBou) annotation (Line(
      points={{64.05,-141},{160,-141},{160,-210},{-232,-210},{-232,-140},{-260,-140}},
      color={191,0,0},
      smooth=Smooth.None));

  connect(radiationAdapter.QRad_flow, irRadGai.Q_flow) annotation (Line(
      points={{-159,130},{-130,130},{-130,-30},{-100.833,-30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(heaPorAir, air.heaPorAir) annotation (Line(
      points={{-260,40},{-200,40},{-200,-130},{40.2,-130}},
      color={191,0,0},
      smooth=Smooth.None));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false,extent={{-260,-220},{460,
            200}}), graphics),
        Icon(coordinateSystem(preserveAspectRatio=true, extent={{-200,-200},{200, 200}}),
                    graphics={
        Text(
          extent={{-104,210},{84,242}},
          lineColor={0,0,255},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid,
          textString="%name"),
        Text(
          extent={{-214,114},{-138,82}},
          lineColor={0,0,127},
          textString="q"),
        Text(
          extent={{-212,176},{-136,144}},
          lineColor={0,0,127},
          textString="u"),
        Text(
          extent={{-14,-160},{44,-186}},
          lineColor={0,0,0},
          fillColor={61,61,61},
          fillPattern=FillPattern.Solid,
          textString="boundary"),
        Rectangle(
          extent={{-160,-160},{160,160}},
          lineColor={200,95,95},
          fillColor={200,95,95},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{140,70},{160,-70}},
          lineColor={95,95,95},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{146,70},{154,-70}},
          lineColor={95,95,95},
          fillColor={170,213,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-60,12},{-22,-10}},
          lineColor={0,0,0},
          fillColor={61,61,61},
          fillPattern=FillPattern.Solid,
          textString="air"),
        Text(
          extent={{-72,-22},{-22,-50}},
          lineColor={0,0,0},
          fillColor={61,61,61},
          fillPattern=FillPattern.Solid,
          textString="radiation"),
        Text(
          extent={{-104,-124},{-54,-152}},
          lineColor={0,0,0},
          fillColor={61,61,61},
          fillPattern=FillPattern.Solid,
          textString="surface")}),
    preferredView="info",
    defaultComponentName="roo",
    Documentation(info="<html>
<p>
Partial model for a room heat and mass balance.
</p>
<p>
This is the base class for
<a href=\"modelica://Buildings.Rooms.FFD\">Buildings.Rooms.FFD</a>
and for
<a href=\"modelica://Buildings.Rooms.MixedAir\">Buildings.Rooms.MixedAir</a>
</p>
<p>
See
<a href=\"modelica://Buildings.Rooms.UsersGuide\">Buildings.Rooms.UsersGuide</a>
for detailed explanations.
</p>
</html>",   revisions="<html>
<ul>
<li>
March 13, 2015, by Michael Wetter:<br/>
Changed model to avoid a translation error
in OpenModelica.
</li>
<li>
July 25, 2014, by Michael Wetter:<br/>
Propagated parameter <code>homotopyInitialization</code>.
</li>
<li>
August 1, 2013, by Michael Wetter:<br/>
Introduced this model as a partial base class because the latent heat gains
are treated differently in the mixed air and in the CFD model.
</li>
<li>
July 16, 2013, by Michael Wetter:<br/>
Redesigned implementation to remove one level of model hierarchy on the room-side heat and mass balance.
This change was done to facilitate the implementation of non-uniform room air heat and mass balance,
which required separating the convection and long-wave radiation models.<br/>
Changed assignment
<code>solRadExc(tauGla={0.6 for i in 1:NConExtWin})</code> to
<code>solRadExc(tauGla={datConExtWin[i].glaSys.glass[datConExtWin[i].glaSys.nLay].tauSol for i in 1:NConExtWin})</code> to
better take into account the solar properties of the glass.
</li>
<li>
March 7 2012, by Michael Wetter:<br/>
Added optional parameters <code>ove</code> and <code>sidFin</code> to
the parameter <code>datConExtWin</code>.
This allows modeling windows with an overhang or with side fins.
</li>
<li>
February 8 2012, by Michael Wetter:<br/>
Changed model to use new implementation of
<a href=\"modelica://Buildings.HeatTransfer.Radiosity.OutdoorRadiosity\">
Buildings.HeatTransfer.Radiosity.OutdoorRadiosity</a>.
This change leads to the use of the same equations for the radiative
heat transfer between window and ambient as is used for
the opaque constructions.
</li>
<li>
December 12, 2011, by Wangda Zuo:<br/>
Add glass thickness as a parameter for conExtWinRad. It is needed by the claculation of property for uncoated glass.
</li>
<li>
December 6, 2011, by Michael Wetter:<br/>
Fixed bug that caused convective heat gains to be
removed from the room instead of added to the room.
This error was caused by a wrong sign in
<a href=\"modelica://Buildings.Rooms.BaseClasses.MixedAirHeatGain\">
Buildings.Rooms.BaseClasses.MixedAirHeatGain</a>.
This closes ticket <a href=\"https://github.com/lbl-srg/modelica-buildings/issues/46\">issue 46</a>.
</li>
<li>
August 9, 2011, by Michael Wetter:<br/>
Fixed bug that caused too high a surface temperature of the window frame.
The previous version did not compute the infrared radiation exchange between the
window frame and the sky. This has been corrected by adding the instance
<code>skyRadExcWin</code> and the parameter <code>absIRFra</code> to the
model
<a href=\"modelica://Buildings.Rooms.BaseClasses.ExteriorBoundaryConditionsWithWindow\">
Buildings.Rooms.BaseClasses.ExteriorBoundaryConditionsWithWindow</a>.
This closes ticket <a href=\"https://github.com/lbl-srg/modelica-buildings/issues/36\">issue 36</a>.
</li>
<li>
August 9, 2011 by Michael Wetter:<br/>
Changed assignment of tilt in instances <code>bouConExt</code> and <code>bouConExtWin</code>.
This fixes the bug in <a href=\"https://github.com/lbl-srg/modelica-buildings/issues/35\">issue 35</a>
that led to the wrong solar radiation gain for roofs and floors.
</li>
<li>
March 23, 2011, by Michael Wetter:<br/>
Propagated convection model to exterior boundary condition models.
</li>
<li>
December 14, 2010, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"));
end RoomHeatMassBalanceSimple;
