within SolarSystem.Data.Layers;
record Insulation50Concrete150 =
    Buildings.HeatTransfer.Data.OpaqueConstructions.Generic (material={
        GlassWool(                x=0.05),Buildings.HeatTransfer.Data.Solids.Concrete(x=0.15)}, final nLay=2)
  "Construction with 50 mm insulation and 150 mm concrete";
