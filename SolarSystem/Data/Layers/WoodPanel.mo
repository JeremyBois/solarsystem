within SolarSystem.Data.Layers;
record WoodPanel =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.3,
    d=750,
    c=1200) "WoodPanel(k=0.3)";
