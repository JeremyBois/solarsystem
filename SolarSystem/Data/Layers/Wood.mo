within SolarSystem.Data.Layers;
record Wood =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.36,
    d=600,
    c=1380) "Wood(k=0.36)";
