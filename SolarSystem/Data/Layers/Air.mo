within SolarSystem.Data.Layers;
record Air =
    Buildings.HeatTransfer.Data.Resistances.Carpet (            R=0.2165)
  "Air strip in wall";
