within SolarSystem.Data.Layers;
record Glass =
    Buildings.HeatTransfer.Data.Glasses.Generic (
    x=0.004,
    k=1.0,
    tauSol=0.8,
    rhoSol_a=0.062,
    rhoSol_b=0.063,
    tauIR=0,
    absIR_a=0.84,
    absIR_b=0.84) "Generic Bronze Glass 4mm (k=1.0)";
