within SolarSystem.Data.Function;
function AlgoHorloge

  input Integer Heure_debut;
  input Integer Minute_debut;
  input Integer Seconde_debut;

input Real t_in;

output Real Heure[3];

protected
  Real t;
  Real Jour_plus;

algorithm
  // On acc�de au temps par la variable time (voir Horloge)
  // On lui ajoute les heures, minutes, secondes pour commencer � l'heure de d�part sp�cifi� par l'utilisateur
  // On a un t en secondes
  t:=t_in+Heure_debut*3600+Minute_debut*60+Seconde_debut;

  // On calcule le nombre e jour �coul� gr�ce au modulo (1jour == 86400sec), puis on soustrait � l'heure les jours pass�s(en sec)
  Jour_plus:=(t-mod(t,86400))/86400;
  t:=t-Jour_plus*86400;

  // On obtient l'heure de la journ�e avec le modulo (1h == 3600sec)
  Heure[1]:=((t-mod(t,3600))/3600);
  t:=t-Heure[1]*3600;

  // On obtient les minutes de la journ�e avec le modulo (1min == 60sec)
  Heure[2]:=((t-mod(t,60))/60);
  t:=(t-Heure[2]*60);

  // Il reste finalement que les secondes
  Heure[3]:=(t);

end AlgoHorloge;
