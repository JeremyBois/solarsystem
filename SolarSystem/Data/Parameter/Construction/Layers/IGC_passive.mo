within SolarSystem.Data.Parameter.Construction.Layers;
package IGC_passive
  "Regroup all new component used inside the IGC passive house."
  extends Modelica.Icons.MaterialPropertiesPackage;
  record Optibric =
      Buildings.HeatTransfer.Data.Solids.Generic (
      k=0.1515,
      d=685,
      c=1000) "Optibric PV 4G (k=0.1515) R=1.32 with 20cm of brick.";
  record Hourdis_polystyrene =
      Buildings.HeatTransfer.Data.Solids.Generic (
      k=0.0276,
      d=35,
      c=1450) "Hourdis polystyr�ne � languette(k=0.0276)";
  record Polyurethane =
      Buildings.HeatTransfer.Data.Solids.Generic (
      k=0.0215,
      d=35,
      c=1590) "Polyurethane Tms Si (Efisol) (k=0.0215)";
  record Chape_beton =
      Buildings.HeatTransfer.Data.Solids.Generic (
      k=0.92,
      d=2300,
      c=880) "Chape_beton(k=0.92)";
  record Beton =
      Buildings.HeatTransfer.Data.Solids.Generic (
      k=1.13,
      d=2000,
      c=1000) "B�ton(k=1.13)";
  record LaineRoche_souffle =
      Buildings.HeatTransfer.Data.Solids.Generic (
      k=0.045,
      d=150,
      c=1030) "Laine de roche souffl�e(k=0.045)";
  record Laine_verre =
      Buildings.HeatTransfer.Data.Solids.Generic (
      k=0.03218,
      d=20,
      c=840) "(Glass wool, k=0.03218)";
  record Verre_vitrage =
                 Buildings.HeatTransfer.Data.Glasses.Generic (
      x=0.005,
      k=0.5,
      tauSol={0.8},
      rhoSol_a={0.075},
      rhoSol_b={0.075},
      tauIR=0,
      absIR_a=0.88,
      absIR_b=0.88)
    "Generic Bronze Glass 4mm including warm edge and ITR (eq. k=0.5)"
    annotation (
      defaultComponentPrefixes="parameter",
      defaultComponentName="datGla");
  record Verre_velux =
                 Buildings.HeatTransfer.Data.Glasses.Generic (
      x=0.004,
      k=1,
      tauSol={0.44},
      rhoSol_a={0.075},
      rhoSol_b={0.075},
      tauIR=0,
      absIR_a=0.88,
      absIR_b=0.88)
    "Generic Bronze Glass 4mm including warm edge and ITR (eq. k=0.5)"
    annotation (
      defaultComponentPrefixes="parameter",
      defaultComponentName="datGla");
  record StGobain_window =
      Buildings.HeatTransfer.Data.GlazingSystems.Generic (
      glass= {SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Verre_vitrage(x=0.01),
                   SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Verre_vitrage(x=0.01)},
      gas={Buildings.HeatTransfer.Data.Gases.Argon(
                           x=0.016)},
      UFra=1.4) "Ug = 1, Uf =1.4"
    annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Modifications 2015-11-26:</span></h4>
<p>Modifications du facteur solaire et de la transmission lumineuse</p>
<p>Impossible de d&eacute;finir un Uw donc performance fen&ecirc;tre hazardeuse.</p>
<p><br><h4><span style=\"color:#008000\">Modifications 2015-11-28:</span></h4></p>
<p>Afin d&rsquo;obtenir le bon Ug, Uf et Uw les &eacute;paisseurs de la lame de gaz et des verres sont modifi&eacute;es. On modifie aussi la conductivit&eacute; du verre pour simuler les traitement qui r&eacute;duisent les d&eacute;perditions.</p>
<p>La conductivit&eacute; des gaz d&eacute;pends de deux coefficient <b><img src=\"modelica://SolarSystem/Images/equations/equation-Sx14jvCB.png\" alt=\"a_k\"/></b> et <b><img src=\"modelica://SolarSystem/Images/equations/equation-tkaAyPWs.png\" alt=\"b_k\"/></b> et la conductivit&eacute; s&rsquo;&eacute;crit sous la forme:</p>
<p align=\"center\"><img src=\"modelica://SolarSystem/Images/equations/equation-7klQ3V92.png\" alt=\"a_k + b_K * T\"/></p>
<p>Conductivit&eacute; argon = 0.01737 (&agrave; 20&deg;C)</p>
<p>Conductivit&eacute; air = 0.02562 (&agrave; 20&deg;C)</p>
</html>"));
  record Velux_window =
      Buildings.HeatTransfer.Data.GlazingSystems.Generic (
      glass={SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Verre_velux(x=0.01),
                   SolarSystem.Data.Parameter.Construction.Layers.IGC_passive.Verre_velux(x=0.01)},
      gas={Buildings.HeatTransfer.Data.Gases.Air(
                           x=0.016)},
      UFra=1.8) "Fen�tre de toit Velux Uw = 1.6" annotation (Documentation(info="<html>
<h4><span style=\"color:#008000\">Modifications 2015-11-27:</span></h4>
<p>Modifications du facteur solaire et de la transmission lumineuse pour correspondre &agrave; celle du Velux.</p>
<p>Impossible de d&eacute;finir un Uw donc performance fen&ecirc;tre hazardeuse.</p>
<p><br><h4><span style=\"color:#008000\">Modifications 2015-11-28:</span></h4></p>
<p>Afin d&rsquo;obtenir le bon Ug, Uf et Uw les &eacute;paisseurs de la lame de gaz et des verres sont modifi&eacute;es. On modifie aussi la conductivit&eacute; du verre pour simuler les traitement qui r&eacute;duisent les d&eacute;perditions.</p>
<p>La conductivit&eacute; des gaz d&eacute;pends de deux coefficient <img src=\"modelica://SolarSystem/Images/equations/equation-Sx14jvCB.png\"/> et <img src=\"modelica://SolarSystem/Images/equations/equation-tkaAyPWs.png\"/> et la conductivit&eacute; s&rsquo;&eacute;crit sous la forme:</p>
<p align=\"center\"><img src=\"modelica://SolarSystem/Images/equations/equation-7klQ3V92.png\"/></p>
<p>Conductivit&eacute; argon = 0.01737 (&agrave; 20&deg;C)</p>
<p>Conductivit&eacute; air = 0.02562 (&agrave; 20&deg;C)</p>
</html>"));
end IGC_passive;
