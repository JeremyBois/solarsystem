within SolarSystem.Data.Parameter.Construction.Layers;
record GlassWool =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.036,
    d=30,
    c=670) "Glass Wook(k=0.036)";
