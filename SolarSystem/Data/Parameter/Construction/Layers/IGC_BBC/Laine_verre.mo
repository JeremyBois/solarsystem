within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Laine_verre =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.035,
    d=20,
    c=840) "Brique(k=0.035)";
