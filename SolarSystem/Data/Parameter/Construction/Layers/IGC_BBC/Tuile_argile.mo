within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Tuile_argile =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=1,
    d=2000,
    c=800) "Tuile argile(k=0.41)";
