within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Gypsum =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.25,
    d=900,
    c=1000) "Gypsum Plasterboard(k=0.25)";
