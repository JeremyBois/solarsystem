within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Linear_bridge =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=1,
    d=0,
    c=0) "Linear bridge resistance R=1 with 1 meter";
