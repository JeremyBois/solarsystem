within SolarSystem.Data.Parameter.Construction.Layers;
package IGC_BBC "All material records"
extends Modelica.Icons.MaterialPropertiesPackage;


annotation (Documentation(info="<html>
<p>Data from energy plus data (Aur&eacute;lie).</p>
<p>Certaines caract&eacute;ristiques semblent &eacute;tranges:</p>
<ul>
<li>Brique isolante + forte densit&eacute; ?? Optibric for the passive house with the same insulation but lower density and volumique mass (extract from technical data)</li>
</ul>
</html>"));
end IGC_BBC;
