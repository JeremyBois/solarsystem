within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Platre =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.25,
    d=820,
    c=1000) "Pl�tre 0-25(k=0.25)";
