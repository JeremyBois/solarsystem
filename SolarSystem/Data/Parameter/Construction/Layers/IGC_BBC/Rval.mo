within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Rval =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=6.67,
    d=0,
    c=0) "Rval with no mass (k=6.67 used to have R=0.15 with 1 meter)";
