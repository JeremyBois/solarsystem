within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Chape_beton =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.92,
    d=2300,
    c=880) "Chape_beton(k=0.92)";
