within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Polyurethane =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.0215,
    d=35,
    c=1590) "Polyurethane Tms Si (Efisol) (k=0.0215)";
