within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Planche_bois =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.14,
    d=650,
    c=1200) "Planche bois(k=0.14)";
