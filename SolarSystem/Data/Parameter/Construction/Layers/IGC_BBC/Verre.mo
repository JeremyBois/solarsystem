within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Verre = Buildings.HeatTransfer.Data.Glasses.Generic (
    x=0.004,
    k=1.0,
    tauSol=0.834,
    rhoSol_a=0.062,
    rhoSol_b=0.063,
    tauIR=0,
    absIR_a=0.837,
    absIR_b=0.837) "Generic Bronze Glass 4mm (k=1.0)"
  annotation (
    defaultComponentPrefixes="parameter",
    defaultComponentName="datGla");
