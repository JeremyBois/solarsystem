within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Urea =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.04,
    d=10,
    c=1400) "Urea Formaldéhyde Foam(k=0.04)";
