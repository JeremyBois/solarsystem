within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Hourdis =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.03,
    d=35,
    c=1450) "Hourdis(k=0.03)";
