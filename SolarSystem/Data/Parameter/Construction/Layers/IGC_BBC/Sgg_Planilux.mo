within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Sgg_Planilux =
    Buildings.HeatTransfer.Data.GlazingSystems.Generic (
    final glass={SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Verre(),
                 SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC.Verre()},
    final gas={Buildings.HeatTransfer.Data.Gases.Argon(
                         x=0.016)},
    UFra=1.4) "Saint-Gobain Glass SGG PLANILUX 4/16/4";
