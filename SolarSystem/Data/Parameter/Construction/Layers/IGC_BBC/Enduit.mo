within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Enduit =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=1.15,
    d=2000,
    c=850) "Enduit(k=1.15)";
