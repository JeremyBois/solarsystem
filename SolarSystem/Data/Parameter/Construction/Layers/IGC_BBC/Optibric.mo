within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Optibric =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.1515,
    d=685,
    c=1000) "Optibric PV 4G (k=0.1515) R=1.32 with 20cm of brick.";
