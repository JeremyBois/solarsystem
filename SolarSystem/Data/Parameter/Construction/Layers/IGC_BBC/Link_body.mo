within SolarSystem.Data.Parameter.Construction.Layers.IGC_BBC;
record Link_body =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.1,
    d=12,
    c=840) "Link body(k=0.1)";
