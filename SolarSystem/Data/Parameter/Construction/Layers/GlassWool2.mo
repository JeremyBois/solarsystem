within SolarSystem.Data.Parameter.Construction.Layers;
record GlassWool2 =
    Buildings.HeatTransfer.Data.Solids.Generic (
    k=0.04,
    d=30,
    c=670) "Glass Wook(k=0.04)";
