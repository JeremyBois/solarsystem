within SolarSystem.Data.Parameter.Construction.Layers;
record DoubleClearArgon16 =
    Buildings.HeatTransfer.Data.GlazingSystems.Generic (
    final glass={Glass(),
                 Glass()},
    final gas={Buildings.HeatTransfer.Data.Gases.Argon(
                         x=0.016)},
    UFra=1.4) "Double pane, clear glass 4mm, argon 16, clear glass 4mm";
