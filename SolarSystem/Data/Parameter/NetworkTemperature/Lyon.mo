within SolarSystem.Data.Parameter.NetworkTemperature;
record Lyon =
    SolarSystem.Data.Parameter.NetworkTemperature.GenericTable (
    final table=[0,273.15 + 6.9; 2628000,273.15 + 7.6; 5256000,273.15 + 9.7;
        7884000,273.15 + 11; 10512000,273.15 + 13; 13140000,273.15 + 15;
        15768000,273.15 + 16; 18396000,273.15 + 16; 21024000,273.15 + 14;
        23652000,273.15 + 12; 26280000,273.15 + 9.2; 28908000,273.15 + 7.4],
    final path = "D:/Github/solarsystem/Meteo/Lyon/FRA_Lyon.074810_IWEC.mos");
