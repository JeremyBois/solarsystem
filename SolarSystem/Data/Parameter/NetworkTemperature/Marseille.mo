within SolarSystem.Data.Parameter.NetworkTemperature;
record Marseille =
    SolarSystem.Data.Parameter.NetworkTemperature.GenericTable (
    final table=[0,273.15 + 12; 2628000,273.15 + 12; 5256000,273.15 + 13; 7884000,
        273.15 + 14; 10512000,273.15 + 16; 13140000,273.15 + 18; 15768000,
        273.15 + 19; 18396000,273.15 + 19; 21024000,273.15 + 18; 23652000,
        273.15 + 16; 26280000,273.15 + 14; 28908000,273.15 + 12],
    final path = "D:/Github/solarsystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos");
