within SolarSystem.Data.Parameter.NetworkTemperature;
record Chambery =
    SolarSystem.Data.Parameter.NetworkTemperature.GenericTable (
    final table=[0,273.15 + 6.4; 2628000,273.15 + 7.4; 5256000,273.15 + 8.9;
        7884000,273.15 + 10; 10512000,273.15 + 12; 13140000,273.15 + 14;
        15768000,273.15 + 15; 18396000,273.15 + 15; 21024000,273.15 + 13;
        23652000,273.15 + 11; 26280000,273.15 + 8; 28908000,273.15 + 6.8],
    final path = "D:/Github/solarsystem/Meteo/Chambery/FR_Chambery_74910TM2.mos");
