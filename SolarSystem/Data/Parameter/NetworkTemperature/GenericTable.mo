within SolarSystem.Data.Parameter.NetworkTemperature;
record GenericTable
  "Generic data record providing inputs for list to interpolate"
  extends Modelica.Icons.Record;

  parameter Real table[:, :] "Table of pipe network temperature";
  parameter String path="File path" "Weather file location path";
annotation(Documentation(info="<html>
  <p>
    Generic data file which is used for the
    <a href=\"modelica://Buildings.Fluid.SolarCollectors.BaseClasses.PartialSolarCollector\">
    Buildings.Fluid.Solarcollectors.BaseClasses.PartialSolarCollector</a> model. Establishes
    the base inputs needed to create model-specific data packages.
  </p>
</html>"));
end GenericTable;
