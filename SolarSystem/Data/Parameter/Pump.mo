within SolarSystem.Data.Parameter;
package Pump "Pumps records"
extends Modelica.Icons.MaterialPropertiesPackage;
  package IGC
    extends Modelica.Icons.MaterialPropertiesPackage;
    record FlowControlled
      "Generic data record for pumps and fans with prescribed m_flow or dp"
      extends Modelica.Icons.Record;

      parameter
        Buildings.Fluid.Movers.BaseClasses.Characteristics.efficiencyParameters
        hydraulicEfficiency(
          V_flow={0},
          eta={0.7}) "Hydraulic efficiency";
      parameter
        Buildings.Fluid.Movers.BaseClasses.Characteristics.efficiencyParameters
        motorEfficiency(
          V_flow={0},
          eta={0.7}) "Electric motor efficiency";

      // Power requires default values to avoid in Dymola the message
      // Failed to expand the variable Power.V_flow
      parameter
        Buildings.Fluid.Movers.BaseClasses.Characteristics.powerParameters power(V_flow={0},
          P={0}) "Volume flow rate vs. electrical power consumption"
        annotation (Dialog(enable=use_powerCharacteristic));

      parameter Boolean motorCooledByFluid=true
        "If true, then motor heat is added to fluid stream";
      parameter Boolean use_powerCharacteristic=false
        "Use powerCharacteristic instead of efficiencyCharacteristic";

      annotation(defaultComponentPrefixes = "parameter",
                 defaultComponentName = "per",
      Documentation(
    info="<html>
<p>
Record containing parameters for pumps or fans
that have either the mass flow rate or the pressure rise
as an input signal.
</p>
<p>
This record may be used to assign for example fan performance data using
declaration such as
</p>
<pre>
  Buildings.Fluid.Movers.FlowControlled_m_flow fan(
      redeclare package Medium = Medium) \"Fan\";
</pre>
<p>
This data record can be used with
<a href=\"modelica://Buildings.Fluid.Movers.FlowControlled_dp\">
Buildings.Fluid.Movers.FlowControlled_dp</a>
and with
<a href=\"modelica://Buildings.Fluid.Movers.FlowControlled_m_flow\">
Buildings.Fluid.Movers.FlowControlled_m_flow</a>.
</p>
<p>
For
<a href=\"modelica://Buildings.Fluid.Movers.SpeedControlled_y\">
Buildings.Fluid.Movers.SpeedControlled_y</a>,
use the record
<a href=\"modelica://Buildings.Fluid.Movers.Data.SpeedControlled_y\">
Buildings.Fluid.Movers.Data.SpeedControlled_y</a>.
</p>
<p>
For
<a href=\"modelica://Buildings.Fluid.Movers.SpeedControlled_Nrpm\">
Buildings.Fluid.Movers.SpeedControlled_Nrpm</a>,
use the record
<a href=\"modelica://Buildings.Fluid.Movers.Data.Generic_Nrpm\">
Buildings.Fluid.Movers.Data.Generic_Nrpm</a>
</p>
</html>",
    revisions="<html>
<ul>
<li>
January 6, 2015, by Michael Wetter:<br/>
Revised record for OpenModelica.
</li>
<li>
November 22, 2014, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"));
    end FlowControlled;
  end IGC;

  package SolisArt
    extends Modelica.Icons.MaterialPropertiesPackage;
    record C6_5_SolisConfort
      "Pump data for C6 and C5 pumps inside SolisConfort system"
      extends Buildings.Fluid.Movers.Data.Generic(
        speed_rpm_nominal=3000,
        use_powerCharacteristic=false,
        motorEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{8.75,17.5,
              26.25,35,43.75,52.5,61.25,70}, eta={1,1,1,1,1,1,1,1}),
        hydraulicEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{8.75,17.5,
              26.25,35,43.75,52.5,61.25,70}, eta={1,1,1,1,1,1,1,
              1}),
        pressure(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{5,10,15,
              20,25,30,35,40}, dp=dp_nominal[1]*{4.9,4.6,4.3,4,3.5,3.2,2.5,1}),
        motorCooledByFluid=true);
      annotation (
    defaultComponentPrefixes="parameter",
    defaultComponentName="per",
    Documentation(info="<html>
<p>See <a href=\"modelica://Buildings.Fluid.Movers.Data.Pumps.Stratos25slash1to6\">Buildings.Fluid.Movers.Data.Pumps.Stratos25slash1to6 </a>for more information about how the data is derived. </p>
</html>",       revisions="<html>
<ul>
<li>
December 12, 2014, by Michael Wetter:<br/>
Added <code>defaultComponentPrefixes</code> and
<code>defaultComponentName</code> annotations.
</li>
<li>April 22, 2014
    by Filip Jorissen:<br/>
       Initial version
</li>
</ul>
</html>"));
    end C6_5_SolisConfort;

    record C2_4_SolisConfort
      "Pump data for C2 and C4 pumps inside SolisConfort system"
      extends Buildings.Fluid.Movers.Data.Generic(
        speed_rpm_nominal=3000,
        use_powerCharacteristic=false,
        motorEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{8.75,17.5,
              26.25,35,43.75,52.5,61.25,70}, eta={1,1,1,1,1,1,1,1}),
        hydraulicEfficiency(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{8.75,17.5,
              26.25,35,43.75,52.5,61.25,70}, eta={1,1,1,1,1,1,1,
              1}),
        pressure(V_flow=(Collector.nPanels*Collector.per.A/3600/1000)*{8.75,17.5,
              26.25,35,43.75,52.5,61.25,70}, dp=dp_nominal[2]*{4.9,4.6,4.3,4,3.5,3.2,
              2.5,1}),
        motorCooledByFluid=true);
      annotation (
    defaultComponentPrefixes="parameter",
    defaultComponentName="per",
    Documentation(info="<html>
<p>See <a href=\"modelica://Buildings.Fluid.Movers.Data.Pumps.Stratos25slash1to6\">Buildings.Fluid.Movers.Data.Pumps.Stratos25slash1to6 </a>for more information about how the data is derived. </p>
</html>",       revisions="<html>
<ul>
<li>
December 12, 2014, by Michael Wetter:<br/>
Added <code>defaultComponentPrefixes</code> and
<code>defaultComponentName</code> annotations.
</li>
<li>April 22, 2014
    by Filip Jorissen:<br/>
       Initial version
</li>
</ul>
</html>"));
    end C2_4_SolisConfort;
  end SolisArt;
end Pump;
