within SolarSystem.Data.Parameter;
package Optimization201706
  "Library for optimization 2017-06. Add correct description of all collector using Solar Keymark"
  extends Modelica.Icons.MaterialPropertiesPackage;

  package Exchangers
    extends Modelica.Icons.MaterialPropertiesPackage;

    record AbstractExchanger "Thermal characteristic for an exchanger material"

      extends Modelica.Icons.Record;

      parameter Modelica.SIunits.SpecificHeatCapacity heatCapacity;

      parameter Modelica.SIunits.Density density;

      annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
            coordinateSystem(preserveAspectRatio=false)));
    end AbstractExchanger;

    record Default =
        SolarSystem.Data.Parameter.Optimization201706.Exchangers.AbstractExchanger
        (heatCapacity=490, density=8000) "Exchanger with default values.";
  end Exchangers;

  package Collectors
    extends Modelica.Icons.MaterialPropertiesPackage;

    record SkyPro =
        Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
        final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Aperture,
        final A=2.281,
        final mDry=51,
        final V=1.76/1000,
        final dp_nominal=1,
        final mperA_flow_nominal=0.017,
        final B0=-0.10498124106144555,
        final B1=0.013812759307203016,
        final y_intercept=0.718,
        final slope=-0.975,
        final IAMDiff=0.981,
        final C1=1.051,
        final C2=0.004,
        final G_nominal=1000,
        final dT_nominal=10)
      "Kloben SkyPro CPC 58 12 - Tubular - Optimization201706";
    record IDMK =
        Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
        final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Aperture,
        final A=2.32,
        final mDry=49,
        final V=1.7/1000,
        final dp_nominal=1,
        final mperA_flow_nominal=0.02,
        final B0=-0.13962931149401214,
        final B1=-0.0004013931653787405,
         y_intercept=0.776,
        final slope=-5.103,
        final IAMDiff=0.93,
         C1=3.293,
         C2=0.011,
        final G_nominal=1000,
        final dT_nominal=10) "Sonnekraft IDMK AL 25 - Flat - Optimization201706";
    record CPCStar =
        Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
        final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Aperture,
        final A=2.33,
        final mDry=41.2,
        final V=2.31/1000,
        final dp_nominal=1,
        final mperA_flow_nominal=0.02,
        final B0=-0.07091458295597303,
        final B1=0.0006266981656039863,
        final y_intercept=0.644,
        final slope=-4.777,
        final IAMDiff=0.93,
        final C1=0.749,
        final C2=0.005,
        final G_nominal=1000,
        final dT_nominal=10) "Ritter CPC Star - Tubular - Optimization201706";
    record EnergyECO =
        Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
        final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Aperture,
        final A=2.31,
        final mDry=34,
        final V=1.9/1000,
        final dp_nominal=1,
        final mperA_flow_nominal=0.02,
        final B0=-0.12842062008015948,
        final B1=-0.0008198286015232396,
        final y_intercept=0.74,
        final slope=-5.979,
        final IAMDiff=0.93,
        final C1=5.116,
        final C2=0.023,
        final G_nominal=1000,
        final dT_nominal=10) "Dimas Energy ECO 25 - Flat - Optimization201706";
  end Collectors;

  package Glazings "Glazing properties for glazing components"
    extends Modelica.Icons.MaterialPropertiesPackage;
    record AbstractGlazing
      extends Modelica.Icons.Record;

      parameter Buildings.HeatTransfer.Data.Glasses.Generic Exterior
        "Exterior glazing surface"
        annotation (Placement(transformation(extent={{-10,52},{10,72}})));
      parameter Buildings.HeatTransfer.Data.Glasses.Generic Interior
        "Interior glazing surface"
        annotation (Placement(transformation(extent={{-10,-68},{10,-48}})));
      parameter Buildings.HeatTransfer.Data.Gases.Argon Gas
        "Gas between glazing surfaces"
        annotation (Placement(transformation(extent={{-10,-6},{10,14}})));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
            coordinateSystem(preserveAspectRatio=false)));

    end AbstractGlazing;

    record PlanithermXN =
        SolarSystem.Data.Parameter.Optimization201706.Glazings.AbstractGlazing
        (
        Exterior(
          tauSol={0.849},
          rhoSol_a={0.076},
          rhoSol_b={0.076},
          absIR_a=0.837,
          absIR_b=0.837,
          x=0.004),
        Interior(
          rhoSol_a={0.293},
          rhoSol_b={0.261},
          absIR_a=0.047,
          absIR_b=0.840,
          tauSol={0.643},
          x=0.004),
        Gas(x=0.016));
    record PlanithermONE =
        SolarSystem.Data.Parameter.Optimization201706.Glazings.AbstractGlazing
        (
        Exterior(
          tauSol={0.849},
          rhoSol_a={0.076},
          rhoSol_b={0.076},
          absIR_a=0.837,
          absIR_b=0.837,
          x=0.004),
        Interior(
          tauSol={0.465},
          absIR_a=0.013,
          absIR_b=0.837,
          rhoSol_a={0.469},
          rhoSol_b={0.412},
          x=0.004),
        Gas(x=0.016));
    record CoolLiteKN155 =
        SolarSystem.Data.Parameter.Optimization201706.Glazings.AbstractGlazing
        (
        Exterior(
          absIR_a=0.837,
          tauSol={0.368},
          rhoSol_a={0.215},
          rhoSol_b={0.237},
          absIR_b=0.114,
          x=0.006),
        Interior(
          tauSol={0.817},
          rhoSol_a={0.073},
          rhoSol_b={0.073},
          absIR_a=0.837,
          absIR_b=0.837,
          x=0.006),
        Gas(
          x=0.016,
          a_k=2.873E-3,
          b_k=7.760E-5,
          a_mu=3.723E-6,
          b_mu=4.940E-8,
          a_c=1002.737,
          b_c=1.2324E-2,
          MM=28.97E-3));
    record OptiwhiteKGlass =
        SolarSystem.Data.Parameter.Optimization201706.Glazings.AbstractGlazing
        (
        Exterior(
          tauSol={0.904},
          rhoSol_a={0.08},
          rhoSol_b={0.08},
          absIR_a=0.84,
          absIR_b=0.84,
          x=0.0039),
        Interior(
          absIR_b=0.837,
          tauSol={0.704},
          rhoSol_a={0.118},
          rhoSol_b={0.106},
          absIR_a=0.173,
          x=0.00385),
        Gas(x=0.016)) "Pilkington";
    record CoolLiteKN160_II =
        SolarSystem.Data.Parameter.Optimization201706.Glazings.AbstractGlazing
        (
        Exterior(
          absIR_a=0.837,
          tauSol={0.355},
          rhoSol_a={0.235},
          rhoSol_b={0.264},
          absIR_b=0.082,
          x=0.006),
        Interior(
          tauSol={0.817},
          rhoSol_a={0.073},
          rhoSol_b={0.073},
          absIR_a=0.837,
          absIR_b=0.837,
          x=0.006),
        Gas(
          x=0.016,
          a_k=2.873E-3,
          b_k=7.760E-5,
          a_mu=3.723E-6,
          b_mu=4.940E-8,
          a_c=1002.737,
          b_c=1.2324E-2,
          MM=28.97E-3));
  end Glazings;
end Optimization201706;
