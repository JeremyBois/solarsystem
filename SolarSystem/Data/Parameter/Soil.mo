within SolarSystem.Data.Parameter;
record Soil =  Buildings.HeatTransfer.Data.Solids.Generic (
    x=3,
    k=1.7,
    d=1900,
    c=790) "Soil";
