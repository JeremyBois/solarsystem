within SolarSystem.Data.Parameter;
package Optimization201701
  "Library for optimization 2017-01"
  extends Modelica.Icons.MaterialPropertiesPackage;

  package Exchangers
    extends Modelica.Icons.MaterialPropertiesPackage;

    record AbstractExchanger "Thermal characteristic for an exchanger material"

      extends Modelica.Icons.Record;

      parameter Modelica.SIunits.SpecificHeatCapacity heatCapacity;

      parameter Modelica.SIunits.Density density;

      annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
            coordinateSystem(preserveAspectRatio=false)));
    end AbstractExchanger;

    record Default =
        SolarSystem.Data.Parameter.Optimization201701.Exchangers.AbstractExchanger
        (heatCapacity=490, density=8000) "Exchanger with default values.";
  end Exchangers;

  package Collectors
    extends Modelica.Icons.MaterialPropertiesPackage;

    record SkyPro_tube =
        Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
        final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Aperture,
        final A=2.281,
        final mDry=52,
        final V=1.8/1000,
        final dp_nominal=1,
        final mperA_flow_nominal=0.0084,
        final B0=-0.137,
        final B1=0.0166,
        final y_intercept=0.623,
        final slope=-0.975,
        final IAMDiff=1.07,
        final C1=0.9249,
        final C2=0.00069,
        final G_nominal=1000,
        final dT_nominal=10)
      "SkyPro - Tubular solar collector - Optimization201701";
    record Idmk_flat =
        Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
      final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Gross,
        final A=2.32,
        final mDry=54,
        final V=1.35/1000,
        final dp_nominal=1,
        final mperA_flow_nominal=0.011,
        final B0=-0.137,
        final B1=0.0166,
         y_intercept=0.78,
        final slope=-5.103,
        final IAMDiff=1,
         C1=3.796,
         C2=0.013,
        final G_nominal=1000,
        final dT_nominal=10) "IDMK 2.5 - Flate glazed solar collector - Optimization201701";
    record Radco_flat =
        Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
        final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Gross,
        final A=2.109,
        final mDry=34,
        final V=3.3/1000,
        final dp_nominal=1,
        final mperA_flow_nominal=0.0204,
        final B0=-0.137,
        final B1=0.0166,
        final y_intercept=0.834,
        final slope=-4.777,
        final IAMDiff=0.84,
        final C1=1.4539,
        final C2=0.0589,
        final G_nominal=1000,
        final dT_nominal=10) "Radco - Flate glazed solar collector - Optimization201701";
    record Dimas_flat =
        Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
        final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Aperture,
        final A=2.312,
        final mDry=41,
        final V=1.9/1000,
        final dp_nominal=1,
        final mperA_flow_nominal=0.0189,
        final B0=-0.137,
        final B1=0.0166,
        final y_intercept=0.664,
        final slope=-5.979,
        final IAMDiff=0.93,
        final C1=4.9510,
        final C2=0.01527,
        final G_nominal=1000,
        final dT_nominal=10)
      "Dimas - Flate glazed solar collector - ECO 25";
  end Collectors;

  package Glazings "Glazing properties for glazing components"
    extends Modelica.Icons.MaterialPropertiesPackage;
    record AbstractGlazing
      extends Modelica.Icons.Record;


      parameter Buildings.HeatTransfer.Data.Glasses.Generic Exterior
        "Exterior glazing surface"
        annotation (Placement(transformation(extent={{-10,52},{10,72}})));
      parameter Buildings.HeatTransfer.Data.Glasses.Generic Interior
        "Interior glazing surface"
        annotation (Placement(transformation(extent={{-10,-68},{10,-48}})));
      parameter Buildings.HeatTransfer.Data.Gases.Argon Gas
        "Gas between glazing surfaces"
        annotation (Placement(transformation(extent={{-10,-6},{10,14}})));
      annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
            coordinateSystem(preserveAspectRatio=false)));

    end AbstractGlazing;

    record PlanithermXN =
        SolarSystem.Data.Parameter.Optimization201701.Glazings.AbstractGlazing
        (
        Exterior(
          tauSol={0.849},
          rhoSol_a={0.076},
          rhoSol_b={0.076},
          absIR_a=0.837,
          absIR_b=0.837,
          x=0.004),
        Interior(
          rhoSol_a={0.293},
          rhoSol_b={0.261},
          absIR_a=0.047,
          absIR_b=0.840,
          tauSol={0.643},
          x=0.004),
        Gas(x=0.016));
    record PlanithermONE =
        SolarSystem.Data.Parameter.Optimization201701.Glazings.AbstractGlazing
        (
        Exterior(
          tauSol={0.849},
          rhoSol_a={0.076},
          rhoSol_b={0.076},
          absIR_a=0.837,
          absIR_b=0.837,
          x=0.004),
        Interior(
          tauSol={0.465},
          absIR_a=0.013,
          absIR_b=0.837,
          rhoSol_a={0.469},
          rhoSol_b={0.412},
          x=0.004),
        Gas(x=0.016));
    record CoolLiteKN155 =
        SolarSystem.Data.Parameter.Optimization201701.Glazings.AbstractGlazing
        (
        Exterior(
          absIR_a=0.837,
          tauSol={0.368},
          rhoSol_a={0.215},
          rhoSol_b={0.237},
          absIR_b=0.114,
          x=0.006),
        Interior(
          tauSol={0.817},
          rhoSol_a={0.073},
          rhoSol_b={0.073},
          absIR_a=0.837,
          absIR_b=0.837,
          x=0.006),
        Gas(x=0.016, a_k= 2.873E-3, b_k= 7.760E-5,
                         a_mu=3.723E-6, b_mu=4.940E-8,
                         a_c=1002.737,  b_c= 1.2324E-2,
                         MM=28.97E-3));
    record OptiwhiteKGlass =
        SolarSystem.Data.Parameter.Optimization201701.Glazings.AbstractGlazing
        (
        Exterior(
          tauSol={0.904},
          rhoSol_a={0.08},
          rhoSol_b={0.08},
          absIR_a=0.84,
          absIR_b=0.84,
          x=0.0039),
        Interior(
          absIR_b=0.837,
          tauSol={0.704},
          rhoSol_a={0.118},
          rhoSol_b={0.106},
          absIR_a=0.173,
          x=0.00385),
        Gas(x=0.016)) "Pilkington";
    record CoolLiteKN160_II =
        SolarSystem.Data.Parameter.Optimization201701.Glazings.AbstractGlazing
        (
        Exterior(
          absIR_a=0.837,
          tauSol={0.355},
          rhoSol_a={0.235},
          rhoSol_b={0.264},
          absIR_b=0.082,
          x=0.006),
        Interior(
          tauSol={0.817},
          rhoSol_a={0.073},
          rhoSol_b={0.073},
          absIR_a=0.837,
          absIR_b=0.837,
          x=0.006),
        Gas(x=0.016, a_k= 2.873E-3, b_k= 7.760E-5,
                         a_mu=3.723E-6, b_mu=4.940E-8,
                         a_c=1002.737,  b_c= 1.2324E-2,
                         MM=28.97E-3));
  end Glazings;
end Optimization201701;
