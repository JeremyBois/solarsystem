within SolarSystem.Data.Parameter.SolarCollector;
record IDMK2_5_buildings =
    Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
    final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Gross,
    final A=2.32,
    final mDry=54,
    final V=1.35/1000,
    final dp_nominal=1,
    final mperA_flow_nominal=0.011,
    final B0=-0.137,
    final B1=0.0166,
    final y_intercept=0.78,
    final slope=-5.103,
    final IAMDiff=1,
    final C1=3.796,
    final C2=0.013,
    final G_nominal=1000,
    final dT_nominal=10) "IDMK 2,5 (buildings model)"
    annotation(Documentation(info="<html>
<p><b>References</b> </p>
<p>Aucunes certifications, donn&eacute;es constructeur</p>
</html>"));
