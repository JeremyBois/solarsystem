within SolarSystem.Data.Parameter.SolarCollector;
block PartialParameters "Partial model for parameters"

  parameter Modelica.SIunits.Area S "Area of the collector";
  parameter Integer nSeg(min=3)=3 "Number of segments";
  parameter Real Roptique "Y intercept (Maximum efficiency)";
  parameter Real a1 "Linear losses EN12975 ratings data";
  parameter Real a2 "Quadratic losses from EN12975 ratings data";

  annotation(Documentation(info="<html>
    <p>
      Partial parameters used in all solar collector models
    </p>
  </html>",
  revisions=""));
end PartialParameters;
