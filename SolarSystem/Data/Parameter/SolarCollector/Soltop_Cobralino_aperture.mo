within SolarSystem.Data.Parameter.SolarCollector;
record Soltop_Cobralino_aperture =
    Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
    final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Aperture,
    final A=1.957,
    final mDry=39,
    final V=1.8/1000,
    final dp_nominal=1,
    final mperA_flow_nominal=0.0043,
    final B0=-0.137,
    final B1=0.0166,
    final y_intercept=0.841,
    final slope=-4.13,
    final IAMDiff=1,
    final C1=3.62,
    final C2=0.0108,
    final G_nominal=1000,
    final dT_nominal=10) "Soltop Cobralino AK 2.2V (IAM = 1)"
    annotation(Documentation(info="<html>
<p><b>References</b> </p><p>Ratings data taken from the <a href=\"http://www.solar-rating.org\">Solar Rating and Certification Corporation website</a>.</p>
</html>"));
