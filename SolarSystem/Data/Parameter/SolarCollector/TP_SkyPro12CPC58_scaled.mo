within SolarSystem.Data.Parameter.SolarCollector;
record TP_SkyPro12CPC58_scaled =
    Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
    final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Aperture,
    final A=2.32,
    final mDry=52.9,
    final V=1.83/1000,
    final dp_nominal=1,
    final mperA_flow_nominal=0.0085,
    final B0=-0.137,
    final B1=0.0166,
    final y_intercept=0.63,
    final slope=-0.975,
    final IAMDiff=1.02,
    final C1=0.9249,
    final C2=0.00069,
    final G_nominal=1000,
    final dT_nominal=10)
  "Tubular solar collector from SkyPro scaled to IDMK area"
    annotation(Documentation(info="<html>
<p><b>References</b> </p>
<p>Ratings data taken from the <a href=\"http://www.solar-rating.org\">Solar Rating and Certification Corporation website</a>. SRCC# = 2011125B.</p>
</html>"));
