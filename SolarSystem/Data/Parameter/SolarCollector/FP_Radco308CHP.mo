within SolarSystem.Data.Parameter.SolarCollector;
record FP_Radco308CHP =
    Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
    final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Gross,
    final A=2.193,
    final mDry=34,
    final V=3.3/1000,
    final dp_nominal=1,
    final mperA_flow_nominal=0.0204,
    final B0=-0.137,
    final B1=0.0166,
    final y_intercept=0.834,
    final slope=-4.777,
    final IAMDiff=0.96,
    final C1=1.4539,
    final C2=0.0589,
    final G_nominal=1000,
    final dT_nominal=10) "Flate glazed solar collector from Radco"
    annotation(Documentation(info="<html>
<p><b>References</b> </p><p>Ratings data taken from the <a href=\"http://www.solar-rating.org\">Solar Rating and Certification Corporation website</a>. SRCC# = 10001856.</p>
</html>"));
