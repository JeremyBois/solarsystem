within SolarSystem.Data.Parameter.SolarCollector;
record TisunFA_mini =
    Buildings.Fluid.SolarCollectors.Data.GenericSolarCollector (
    final ATyp=Buildings.Fluid.SolarCollectors.Types.Area.Aperture,
    final A=13.82/10,
    final mDry=345/10,
    final V=12.2/(1000*10),
    final dp_nominal=100/10,
    final mperA_flow_nominal=0.0175,
    final B0=-0.117,
    final B1=0.0156,
    final y_intercept=0.745,
    final slope=-5.103,
    final IAMDiff=1,
    final C1=3.26,
    final C2=0.0185,
    final G_nominal=1000,
    final dT_nominal=10)
    annotation(Documentation(info = "<html>
    <h4>References</h4>
      <p>
        Ratings data taken from the <a href=\"http://www.solar-rating.org\">
        Solar Rating and Certification Corporation website</a>. SRCC# = 2012021A.<br/>
      </p>
    </html>"));
