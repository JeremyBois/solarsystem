within SolarSystem.Data.Parameter.SolarCollector;
record SolarParameters_old
  "Generic data record providing inputs for specific collector data records"

  parameter Modelica.SIunits.Area A "Area";
  parameter Modelica.SIunits.Mass mDry "Dry weight";
  parameter Modelica.SIunits.Volume V "Fluid volume";
  parameter Modelica.SIunits.Pressure dp_nominal
    "Pressure drop during test conditions";
  parameter Real mperA_flow_nominal(unit="kg/(s.m2)")
    "Mass flow rate per unit area of collector";
  parameter Real Roptique "Maximum efficiency";
  parameter Real a1 "Linear losses EN12975 ratings data";
  parameter Real a2 "Quadratic losses from EN12975 ratings data";
annotation(Documentation(info="<html>
<p>Generic collector data file which is used for solars collectors.</p>
<p>Establishes the base inputs needed to create model-specific data packages. </p>
</html>"));
end SolarParameters_old;
