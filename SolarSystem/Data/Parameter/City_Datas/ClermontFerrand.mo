within SolarSystem.Data.Parameter.City_Datas;
record ClermontFerrand =
    SolarSystem.Data.Parameter.City_Datas.City_Data (final water=[0,273.15 +
        6.9; 2628000,273.15 + 7.4; 5256000,273.15 + 9.2; 7884000,273.15 + 11;
        10512000,273.15 + 12; 13140000,273.15 + 14; 15768000,273.15 + 15;
        18396000,273.15 + 15; 21024000,273.15 + 14; 23652000,273.15 + 11;
        26280000,273.15 + 8.9; 28908000,273.15 + 7.3; 31449600,273.15 + 6.9],
        final lattitude=0.798660535111111,
        final path="D:/Github/Lib/Modelica/solarsystem/Meteo/Clermont-Ferrand/FRA_Clermont-Ferrand.074600_IWEC.mos")
  "Ground temperature for Bordeaux not Clermont-Ferrand";
