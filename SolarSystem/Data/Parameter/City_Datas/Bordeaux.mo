within SolarSystem.Data.Parameter.City_Datas;
record Bordeaux =
    SolarSystem.Data.Parameter.City_Datas.City_Data (final water=[0,273.15 +
        8.9; 2628000,273.15 + 9.3; 5256000,273.15 + 11; 7884000,273.15 + 12;
        10512000,273.15 + 14; 13140000,273.15 + 15; 15768000,273.15 + 16;
        18396000,273.15 + 16; 21024000,273.15 + 15; 23652000,273.15 + 13;
        26280000,273.15 + 11; 28908000,273.15 + 9.2; 31449600,273.15 + 8.9],
        final lattitude=0.78248927549303,
        final path="D:/Github/Lib/Modelica/solarsystem/Meteo/Bordeaux/FRA_Bordeaux.075100_IWEC.mos");
