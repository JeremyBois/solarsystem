within SolarSystem.Data.Parameter.City_Datas;
record Limoges =
    SolarSystem.Data.Parameter.City_Datas.City_Data (final water=[0,273.15 +
        7; 2628000,273.15 + 7.4; 5256000,273.15 + 9; 7884000,273.15 + 10;
        10512000,273.15 + 12; 13140000,273.15 + 14; 15768000,273.15 + 14;
        18396000,273.15 + 14; 21024000,273.15 + 13; 23652000,273.15 + 11;
        26280000,273.15 + 8.8; 28908000,273.15 + 7.3; 31449600,273.15 + 7],
        final lattitude=0.7998277777777779,
        final path="D:/Github/Lib/Modelica/solarsystem/Meteo/Limoges/Limoges.mos");
