within SolarSystem.Data.Parameter.City_Datas;
record Marseille =
    SolarSystem.Data.Parameter.City_Datas.City_Data (final water=[0,273.15 + 12;
        2628000,273.15 + 12; 5256000,273.15 + 13; 7884000,273.15 + 14; 10512000,
        273.15 + 16; 13140000,273.15 + 18; 15768000,273.15 + 19; 18396000,
        273.15 + 19; 21024000,273.15 + 18; 23652000,273.15 + 16; 26280000,
        273.15 + 14; 28908000,273.15 + 12; 31449600,273.15 + 12],
        final lattitude=0.75572756611354,
        final path= "D:/Github/Lib/Modelica/solarsystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
  annotation (Documentation(info="<html>
<p>ground false</p>
</html>"));
