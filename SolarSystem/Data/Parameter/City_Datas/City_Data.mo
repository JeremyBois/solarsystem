within SolarSystem.Data.Parameter.City_Datas;
record City_Data "Generic data record providing weather informations inputs."
  extends Modelica.Icons.Record;

  parameter Real water[:, :] "Table of pipe network temperature";
  parameter Real ground[:, :]=[0, 273.15 + 10] "Table of ground temperature";
  parameter Modelica.SIunits.Angle lattitude( displayUnit="degree")
    "City lattitude";
  parameter String path="File path" "Weather file location path";
annotation(Documentation(info="<html>
  <p>
    Generic data file which is used for the
    <a href=\"modelica://Buildings.Fluid.SolarCollectors.BaseClasses.PartialSolarCollector\">
    Buildings.Fluid.Solarcollectors.BaseClasses.PartialSolarCollector</a> model. Establishes
    the base inputs needed to create model-specific data packages.
  </p>
</html>"));
end City_Data;
