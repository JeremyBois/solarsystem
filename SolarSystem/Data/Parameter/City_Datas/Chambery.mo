within SolarSystem.Data.Parameter.City_Datas;
record Chambery =
    SolarSystem.Data.Parameter.City_Datas.City_Data (final water=[0,273.15 +
        6.4; 2628000,273.15 + 7.4; 5256000,273.15 + 8.9; 7884000,273.15 + 10;
        10512000,273.15 + 12; 13140000,273.15 + 14; 15768000,273.15 + 15;
        18396000,273.15 + 15; 21024000,273.15 + 13; 23652000,273.15 + 11;
        26280000,273.15 + 8; 28908000,273.15 + 6.8; 31449600,273.15 + 6.4],
        final lattitude=0.79528836830985,
        final path="D:/Github/Lib/Modelica/solarsystem/Meteo/Chambery/FR_Chambery_74910TM2.mos");
