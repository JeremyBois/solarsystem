within SolarSystem.Data.Parameter.City_Datas;
record Toulouse =
    SolarSystem.Data.Parameter.City_Datas.City_Data (final water=[0,273.15 +
        8.6; 2628000,273.15 + 9.2; 5256000,273.15 + 11; 7884000,273.15 + 12;
        10512000,273.15 + 14; 13140000,273.15 + 16; 15768000,273.15 + 17;
        18396000,273.15 + 17; 21024000,273.15 + 16; 23652000,273.15 + 13;
        26280000,273.15 + 11; 28908000,273.15 + 9; 31449600,273.15 + 8.6],
        final lattitude=0.7606559637777778,
        final path="D:/Github/Lib/Modelica/solarsystem/Meteo/Toulouse/Toulouse_2005.mos");
