within SolarSystem.Data.Parameter.City_Datas;
record Biarritz =
    SolarSystem.Data.Parameter.City_Datas.City_Data (final water=[0,273.15 +
        11; 2628000,273.15 + 11; 5256000,273.15 + 12; 7884000,273.15 + 13;
        10512000,273.15 + 14; 13140000,273.15 + 16; 15768000,273.15 + 17;
        18396000,273.15 + 17; 21024000,273.15 + 16; 23652000,273.15 + 14;
        26280000,273.15 + 12; 28908000,273.15 + 11; 31449600,273.15 + 11],
        final lattitude=0.7585141,
        final path="D:/Github/Lib/Modelica/solarsystem/Meteo/Biarritz/Biarritz.mos")
  annotation (Documentation(info="<html>
<p>Pr&eacute;parer le fichier m&eacute;t&eacute;o pour &eacute;viter le bug.</p>
</html>"));
