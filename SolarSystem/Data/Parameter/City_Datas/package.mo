within SolarSystem.Data.Parameter;
package City_Datas
extends Modelica.Icons.MaterialPropertiesPackage;


annotation (Documentation(info="<html>
<h4>2016-12-02</h4>
<p>Correction of periodic scheduling. Last value must match first value !</p>
</html>"));
end City_Datas;
