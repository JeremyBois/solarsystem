within SolarSystem.Data.Parameter.City_Datas;
record Strasbourg =
    SolarSystem.Data.Parameter.City_Datas.City_Data (final water=[0,273.15 +
        5.3; 2628000,273.15 + 5.8; 5256000,273.15 + 7.7; 7884000,273.15 + 9.5;
        10512000,273.15 + 11; 13140000,273.15 + 13; 15768000,273.15 + 14;
        18396000,273.15 + 14; 21024000,273.15 + 12; 23652000,273.15 + 9.8;
        26280000,273.15 + 7.5; 28908000,273.15 + 5.8; 31449600,273.15 + 5.3],
        final lattitude=0.84793912244281,
        final path="D:/Github/Lib/Modelica/solarsystem/Meteo/Strasbourg/FRA_Strasbourg.071900_IWEC.mos");
