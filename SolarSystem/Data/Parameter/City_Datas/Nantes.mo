within SolarSystem.Data.Parameter.City_Datas;
record Nantes =
    SolarSystem.Data.Parameter.City_Datas.City_Data (final water=[0,273.15 +
        8.3; 2628000,273.15 + 8.5; 5256000,273.15 + 9.9; 7884000,273.15 + 11;
        10512000,273.15 + 13; 13140000,273.15 + 14; 15768000,273.15 + 15;
        18396000,273.15 + 15; 21024000,273.15 + 14; 23652000,273.15 + 12;
        26280000,273.15 + 9.8; 28908000,273.15 + 8.6; 31449600,273.15 + 8.3],
        final lattitude=0.8236928244444446,
        final path="D:/Github/Lib/Modelica/solarsystem/Meteo/Nantes/FRA_Nantes.072220_IWEC.mos")
  "Ground temperature for Bordeaux not Nantes";
