within SolarSystem.Data.Unit;
type MassFlowRatePerArea =
                    Real (quantity="MassFlowRatePerArea", final unit="kg/(s.m2)")
  "A mass flow rate per unit of surface";
