within SolarSystem.Data.Unit;
type CoefficientDeperditionQuadratique = Real (final quantity= "CoefficientDeperditionQuadratique", final unit="W/(m2.K2)");
