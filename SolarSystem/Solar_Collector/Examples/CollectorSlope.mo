within SolarSystem.Solar_Collector.Examples;
model CollectorSlope
  extends Modelica.Icons.Example;

  package Medium = Buildings.Media.Water
    "Medium water model with glycol";

  parameter Real BaseFlow = 5;

  Real deltaT_plan "Delta between outside and collector plan";
  Real deltaT_tube "Delta between outside and collector tube";
  Modelica.SIunits.Power PowerToWater[2];
  Modelica.SIunits.Power PowerToCollector[2];
  Modelica.SIunits.Efficiency rend_plan "Collector efficiency of plan";
  Modelica.SIunits.Efficiency rend_tube "Collector efficiency of tube";


  EN12975_Integrator_complete Plan(
    azi=0,
    til=33,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    redeclare package Medium = Medium,
    lat=weather_file.lattitude,
    nSeg=10,
    nPanels=5,
    per=iDMK2_5_buildings)
    annotation (Placement(transformation(extent={{-28,10},{6,44}})));
public
  replaceable Data.Parameter.City_Datas.Bordeaux weather_file constrainedby
    Data.Parameter.City_Datas.City_Data
    annotation (Placement(transformation(extent={{-96,78},{-78,96}})));
protected
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 Weather(
      computeWetBulbTemperature=false,
    filNam=weather_file.path) "Weather datas"
    annotation (Placement(transformation(extent={{100,72},{68,100}})));
  Buildings.Fluid.Sources.Boundary_pT SourceOutput(
    redeclare package Medium = Medium,
    use_T_in=false,
    nPorts=1) annotation (Placement(transformation(
        extent={{7,-7},{-7,7}},
        rotation=0,
        origin={93,21})));
  Buildings.Fluid.Sources.MassFlowSource_T SourceFlow(
    redeclare package Medium = Medium,
    use_m_flow_in=true,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{-90,-16},{-74,0}})));
public
  Buildings.Fluid.Sensors.TemperatureTwoPort Tin_plan(redeclare package Medium =
        Medium, m_flow_nominal=BaseFlow/3600*Plan.nPanels*Plan.per.A)
                                                                annotation (
      Placement(transformation(
        extent={{7,8},{-7,-8}},
        rotation=180,
        origin={-50,-7})));
public
  Buildings.Fluid.Sensors.TemperatureTwoPort Tout_plan(redeclare package Medium =
        Medium, m_flow_nominal=BaseFlow/3600*Plan.nPanels*Plan.per.A)
    annotation (Placement(transformation(
        extent={{7,8},{-7,-8}},
        rotation=180,
        origin={58,21})));
public
  Modelica.Blocks.Sources.Constant MassFlowRate(k=BaseFlow/3600*Plan.nPanels*
        Plan.per.A)
    annotation (Placement(transformation(extent={{-100,48},{-88,60}})));
  Buildings.BoundaryConditions.WeatherData.Bus
                                     weaBus "Weather data" annotation (
     Placement(transformation(extent={{-16,60},{12,86}}),  iconTransformation(
          extent={{-10,88},{10,108}})));
protected
  Modelica.Thermal.HeatTransfer.Sources.PrescribedTemperature Toutside
    "Boundary condition for construction" annotation (Placement(transformation(
        extent={{0,0},{16,16}},
        rotation=0,
        origin={34,52})));
public
  Data.Parameter.SolarCollector.IDMK2_5_buildings iDMK2_5_buildings
    annotation (Placement(transformation(extent={{8,38},{28,58}})));
  Data.Parameter.SolarCollector.TP_SkyPro12CPC58 tP_SkyPro12CPC58_1
    annotation (Placement(transformation(extent={{10,-22},{30,-2}})));
  EN12975_Integrator_complete Tube(
    azi=0,
    til=33,
    rho=0.2,
    nColType=Buildings.Fluid.SolarCollectors.Types.NumberSelection.Number,
    redeclare package Medium = Medium,
    lat=weather_file.lattitude,
    nSeg=10,
    nPanels=5,
    per=tP_SkyPro12CPC58_1)
    annotation (Placement(transformation(extent={{-26,-48},{8,-14}})));
protected
  Buildings.Fluid.Sources.Boundary_pT SourceOutput1(
    redeclare package Medium = Medium,
    use_T_in=false,
    nPorts=1) annotation (Placement(transformation(
        extent={{7,-7},{-7,7}},
        rotation=0,
        origin={95,-61})));
public
  Buildings.Fluid.Sensors.TemperatureTwoPort Tout_tube(redeclare package Medium =
        Medium, m_flow_nominal=BaseFlow/3600*Tube.nPanels*Tube.per.A)
                                                                annotation (
      Placement(transformation(
        extent={{7,8},{-7,-8}},
        rotation=180,
        origin={60,-59})));
protected
  Buildings.Fluid.Sources.MassFlowSource_T SourceFlow1(
    redeclare package Medium = Medium,
    use_m_flow_in=true,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{-88,-64},{-72,-48}})));
public
  Buildings.Fluid.Sensors.TemperatureTwoPort Tin_tube(redeclare package Medium =
        Medium, m_flow_nominal=BaseFlow/3600*Tube.nPanels*Tube.per.A)
                                                                annotation (
      Placement(transformation(
        extent={{7,8},{-7,-8}},
        rotation=180,
        origin={-48,-41})));
equation

  deltaT_plan =(Plan.Tinside[10] + Plan.Tinside[1])/2 - Toutside.T;
  deltaT_tube =(Tube.Tinside[10] + Tube.Tinside[1])/2 - Toutside.T;
  PowerToWater[1] =MassFlowRate.y*Medium.cp_const*(Tout_plan.T - Tin_plan.T);
  PowerToWater[2] =MassFlowRate.y*Medium.cp_const*(Tout_tube.T - Tin_tube.T);
  PowerToCollector[1] =(Plan.HDirTil.H + Plan.HDifTilIso.H)*Plan.nPanels*Plan.per.A;
  PowerToCollector[2] =(Tube.HDirTil.H + Tube.HDifTilIso.H)*Tube.nPanels*Tube.per.A;
  rend_plan = max(0, min(1, PowerToWater[1] / PowerToCollector[1]));
  rend_tube = max(0, min(1, PowerToWater[2] / PowerToCollector[2]));
  connect(Tin_plan.port_b, Plan.port_a) annotation (Line(points={{-43,-7},{-40,-7},
          {-40,27},{-28,27}}, color={0,127,255}));
  connect(Plan.port_b, Tout_plan.port_a) annotation (Line(points={{6,27},{38,27},
          {38,21},{51,21}}, color={0,127,255}));
  connect(Tout_plan.port_b, SourceOutput.ports[1])
    annotation (Line(points={{65,21},{74.5,21},{86,21}}, color={0,127,255}));
  connect(MassFlowRate.y, SourceFlow.m_flow_in) annotation (Line(points={{-87.4,
          54},{-80,54},{-80,20},{-98,20},{-98,-1.6},{-90,-1.6}},     color={0,0,
          127}));
  connect(Weather.weaBus, weaBus) annotation (Line(
      points={{68,86},{34,86},{-2,86},{-2,73}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}}));
  connect(weaBus, Plan.weaBus) annotation (Line(
      points={{-2,73},{-2,73},{-2,54},{-28,54},{-28,43.32}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus.TDryBul, Toutside.T) annotation (Line(
      points={{-2,73},{8,73},{18,73},{18,60},{32.4,60}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, Tube.weaBus) annotation (Line(
      points={{-2,73},{-2,73},{-2,54},{-34,54},{-34,0},{-26,0},{-26,-14.68}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(SourceFlow.ports[1], Tin_plan.port_a)
    annotation (Line(points={{-74,-8},{-57,-8},{-57,-7}}, color={0,127,255}));
  connect(Tube.port_b, Tout_tube.port_a) annotation (Line(points={{8,-31},{26,-31},
          {26,-30},{40,-30},{40,-59},{53,-59}}, color={0,127,255}));
  connect(Tout_tube.port_b, SourceOutput1.ports[1]) annotation (Line(points={{67,
          -59},{77.5,-59},{77.5,-61},{88,-61}}, color={0,127,255}));
  connect(Tin_tube.port_b, Tube.port_a) annotation (Line(points={{-41,-41},{-34.5,
          -41},{-34.5,-31},{-26,-31}}, color={0,127,255}));
  connect(SourceFlow1.ports[1], Tin_tube.port_a) annotation (Line(points={{-72,-56},
          {-64,-56},{-64,-41},{-55,-41}}, color={0,127,255}));
  connect(MassFlowRate.y, SourceFlow1.m_flow_in) annotation (Line(points={{-87.4,
          54},{-80,54},{-80,20},{-98,20},{-98,-49.6},{-88,-49.6}}, color={0,0,127}));
  annotation (Documentation(info="<html>
<p>40/3600*Collector.nPanels*Collector.per.A</p>
</html>"),
    experiment(
      StopTime=3.1536e+007,
      Interval=1800,
      Tolerance=1e-006,
      __Dymola_Algorithm="Cvode"),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end CollectorSlope;
