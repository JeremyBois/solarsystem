within SolarSystem.Solar_Collector;
model EN12975_Integrator_complete
  "Extends buildings solar collector to get access to integrator and power repartition"
  extends Buildings.Fluid.SolarCollectors.EN12975;

  Modelica.SIunits.DensityOfHeatFlowRate TotalPowerLost;
  Modelica.SIunits.DensityOfHeatFlowRate TotalPowerGain;

    Modelica.Blocks.Interfaces.RealOutput Tinside[nSeg]
    annotation (Placement(transformation(extent={{90,-90},{130,-50}})));
public
  Modelica.Blocks.Continuous.Integrator integrator
    annotation (Placement(transformation(extent={{60,70},{80,90}})));
equation
  // Integrator of incident irradiation
  integrator.u = (HDirTil.H + HDifTilIso.H) * TotalArea_internal;

  // Power losses
  TotalPowerLost = QLos[1].Q_flow * nSeg / TotalArea_internal "Total power lost per square meter";

  // Power gains
  TotalPowerGain = heaGai[1].Q_flow * nSeg / TotalArea_internal "Total power gains per square meter";

  connect(temSen.T, Tinside) annotation (Line(points={{-8,-16},{-24,-16},{-28,-16},
          {-28,-70},{110,-70}}, color={0,0,127}));
end EN12975_Integrator_complete;
