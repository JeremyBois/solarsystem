within SolarSystem.Solar_Collector;
model SolarGrid "A grid with three groups of solar PVs (south, east, west)."

public
  Buildings.Electrical.AC.OnePhase.Loads.Inductive Consumer(V_nominal=120, mode=Buildings.Electrical.Types.Load.VariableZ_P_input)
    "Consumes the power generted by the PVs or grid if PVs not enough"
    annotation (Placement(transformation(extent={{20,-56},{40,-36}})));
protected
  Buildings.Electrical.AC.OnePhase.Sources.Grid grid(f=60, V=120)
    "Electrical grid model"
           annotation (Placement(transformation(extent={{-22,-68},{2,-92}})));
public
  Buildings.Electrical.AC.OnePhase.Sources.PVSimpleOriented pv_WEST(
    V_nominal=120,
    lat=lat,
    A=A_west,
    pf=pf,
    eta_DCAC=eta_DCAC,
    fAct=fAct,
    eta=eta,
    til=til,
    azi=1.5707963267949) "PV array oriented"
    annotation (Placement(transformation(extent={{-40,-4},{-20,16}})));
protected
  Modelica.Blocks.Math.Sum sum_needs(nin=4, k={1,1,floor_area,floor_area})
    annotation (__Dymola_tag={"Power", "Internal"}, Placement(transformation(extent={{-30,66},{-18,
            78}})));
protected
  Modelica.Blocks.Routing.Multiplex4 electrical_needs
    annotation (Placement(transformation(extent={{-54,64},{-38,80}})));
  Modelica.Blocks.Math.Gain to_negative(k=-1)
    "Power load negative to simulate a load" annotation (Placement(
        transformation(
        extent={{-6,-6},{6,6}},
        rotation=-90,
        origin={52,30})));
protected
  Modelica.Blocks.Continuous.Integrator pv_production(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J")) "Heating energy in Joules"
    annotation (Placement(transformation(extent={{24,0},{36,12}})));
protected
  Modelica.Blocks.Continuous.Integrator Energy_from_grid(
    k=1,
    initType=Modelica.Blocks.Types.Init.InitialState,
    y_start=0,
    u(unit="W"),
    y(unit="J"))
    annotation (Placement(transformation(extent={{20,-88},{36,-72}})));
public
  Buildings.Electrical.AC.OnePhase.Sources.PVSimpleOriented pv_EAST(
    V_nominal=120,
    lat=lat,
    pf=pf,
    eta_DCAC=eta_DCAC,
    A=A_east,
    fAct=fAct,
    eta=eta,
    til=til,
    azi=-1.5707963267949) "PV array oriented East"
    annotation (Placement(transformation(extent={{-40,-34},{-20,-14}})));
  Buildings.Electrical.AC.OnePhase.Sources.PVSimpleOriented pv_SOUTH(
    V_nominal=120,
    azi=0,
    lat=lat,
    A=A_south,
    pf=pf,
    eta_DCAC=eta_DCAC,
    fAct=fAct,
    eta=eta,
    til=til)              "PV array oriented South"
    annotation (Placement(transformation(extent={{-40,26},{-20,46}})));
protected
  Modelica.Blocks.Math.Sum sum_gains1(nin=3, k={1,1,1})
    annotation (__Dymola_tag={"Power", "Internal"}, Placement(transformation(extent={{-2,0},{10,12}})));
public
  parameter Modelica.SIunits.Angle lat "Latitude" annotation (Dialog(group="Site"));
  parameter Modelica.SIunits.Angle til "Surface tilt" annotation (Dialog(group="Site"));
  parameter Modelica.SIunits.Area floor_area "House floor area" annotation (Dialog(group="Site"));
  parameter Modelica.SIunits.Area A_south "Net surface area of PV on south roof" annotation (Dialog(group="Area"));
  parameter Modelica.SIunits.Area A_west "Net surface area of PV on west roof" annotation (Dialog(group="Area"));
  parameter Modelica.SIunits.Area A_east "Net surface area of PV on east roof" annotation (Dialog(group="Area"));
  parameter Real fAct=0.9 "Fraction of surface area with active solar cells" annotation (Dialog(group="Characteristics"));
  parameter Real eta=0.12 "Module conversion efficiency" annotation (Dialog(group="Characteristics"));
  parameter Real pf=0.9 "Power factor" annotation (Dialog(group="Characteristics"));
  parameter Real eta_DCAC=0.9 "Efficiency of DC/AC conversion" annotation (Dialog(group="Characteristics"));

  Modelica.Blocks.Interfaces.RealVectorInput Electrical_loads[4]
    "([Heating [W], DHW [W], Lights [W/m2], Equipments [W/m2])"
                                         annotation (Placement(transformation(extent={{-120,-20},{
            -80,20}}), iconTransformation(extent={{-118,-18},{-78,22}})));

  Buildings.BoundaryConditions.WeatherData.Bus
                                     weaBus "Weather data" annotation (
     Placement(transformation(extent={{-14,84},{14,110}}), iconTransformation(
          extent={{-10,88},{10,108}})));
  Modelica.Blocks.Interfaces.RealOutput PV_Energy "PVs total solar production" annotation (__Dymola_tag={"Solar", "Energy"},
      Placement(transformation(extent={{96,46},{124,74}}), iconTransformation(extent={{80,40},{120,
            80}})));
  Modelica.Blocks.Interfaces.RealOutput Grid_Energy
    "Grid electrical energy consumption minus solar energy production" annotation (__Dymola_tag={"Energy", "Electric"}, Placement(
        transformation(extent={{96,-74},{124,-46}}), iconTransformation(extent={{80,-80},{120,-40}})));
equation

  // Cannot connect using graphical interface so hardcode it
  Energy_from_grid.u = grid.P.real;

  connect(grid.terminal, Consumer.terminal) annotation (Line(
      points={{-10,-68},{-10,-46},{20,-46}},
      color={0,120,120},
      smooth=Smooth.None));
  connect(electrical_needs.y,sum_needs. u)
    annotation (Line(points={{-37.2,72},{-37.2,72},{-31.2,72}},
                                                           color={0,0,127}));
  connect(sum_needs.y,to_negative. u)
    annotation (Line(points={{-17.4,72},{52,72},{52,37.2}},color={0,0,127}));
  connect(to_negative.y, Consumer.Pow)
    annotation (Line(points={{52,23.4},{52,23.4},{52,-46},{40,-46}}, color={0,0,127}));
  connect(pv_SOUTH.P,sum_gains1. u[1]) annotation (Line(points={{-19,43},{-8,43},{-8,5.2},{-3.2,5.2}},
                                  color={0,0,127}));
  connect(pv_WEST.P,sum_gains1. u[2]) annotation (Line(points={{-19,13},{-8,13},{-8,6},{-3.2,6}},
                              color={0,0,127}));
  connect(pv_EAST.P,sum_gains1. u[3]) annotation (Line(points={{-19,-17},{-8,-17},{-8,6.8},{-3.2,6.8}},
                                  color={0,0,127}));
  connect(sum_gains1.y,pv_production. u)
    annotation (Line(points={{10.6,6},{10.6,6},{22.8,6}},
                                                   color={0,0,127}));
  connect(pv_SOUTH.terminal, Consumer.terminal)
    annotation (Line(points={{-40,36},{-56,36},{-56,-46},{20,-46}}, color={0,120,120}));
  connect(pv_WEST.terminal, Consumer.terminal)
    annotation (Line(points={{-40,6},{-52,6},{-52,-46},{20,-46}}, color={0,120,120}));
  connect(pv_EAST.terminal, Consumer.terminal) annotation (Line(points={{-40,-24},{-48,-24},{-48,-46},
          {20,-46}}, color={0,120,120}));
  connect(pv_production.y, PV_Energy)
    annotation (Line(points={{36.6,6},{80,6},{80,60},{110,60}}, color={0,0,127}));
  connect(weaBus, pv_EAST.weaBus) annotation (Line(
      points={{1.77636e-015,97},{1.77636e-015,78},{0,78},{0,60},{-48,60},{-48,-10},{-30,-10},{-30,
          -15}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  connect(weaBus, pv_WEST.weaBus) annotation (Line(
      points={{1.77636e-015,97},{1.77636e-015,78},{0,78},{0,60},{-48,60},{-48,20},{-30,20},{-30,15}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));

  connect(weaBus, pv_SOUTH.weaBus) annotation (Line(
      points={{1.77636e-015,97},{1.77636e-015,78},{0,78},{0,60},{-48,60},{-48,52},{-30,52},{-30,45}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));

  connect(Electrical_loads[1], electrical_needs.u1[1]) annotation (Line(points={{-100,-15},{-84,-15},
          {-84,79.2},{-55.6,79.2}}, color={0,0,127}));
  connect(Electrical_loads[2], electrical_needs.u2[1]) annotation (Line(points={{-100,-5},{-80,-5},
          {-80,74.4},{-55.6,74.4}}, color={0,0,127}));
  connect(Electrical_loads[3], electrical_needs.u3[1]) annotation (Line(points={{-100,5},{-76,5},{
          -76,69.6},{-55.6,69.6}}, color={0,0,127}));
  connect(Electrical_loads[4], electrical_needs.u4[1]) annotation (Line(points={{-100,15},{-72,15},
          {-72,64.8},{-55.6,64.8}}, color={0,0,127}));
  connect(Energy_from_grid.y, Grid_Energy) annotation (Line(points={{36.8,-80},{68,-80},{80,-80},{
          80,-60},{110,-60}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={27,0,55},
          fillColor={139,210,205},
          fillPattern=FillPattern.Solid),
        Ellipse(
          extent={{-24,28},{28,-24}},
          lineColor={255,255,0},
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Line(
          points={{-6,-6},{8,8}},
          color={255,255,0},
          smooth=Smooth.None,
          thickness=1,
          origin={-24,32},
          rotation=90),
        Line(
          points={{-50,2},{-30,2}},
          color={255,255,0},
          smooth=Smooth.None,
          thickness=1),
        Line(
          points={{-36,-38},{-20,-22}},
          color={255,255,0},
          smooth=Smooth.None,
          thickness=1),
        Line(
          points={{-10,0},{10,0}},
          color={255,255,0},
          smooth=Smooth.None,
          thickness=1,
          origin={2,-38},
          rotation=90),
        Line(
          points={{-8,-8},{6,6}},
          color={255,255,0},
          smooth=Smooth.None,
          thickness=1,
          origin={30,-28},
          rotation=90),
        Line(
          points={{32,2},{52,2}},
          color={255,255,0},
          smooth=Smooth.None,
          thickness=1),
        Line(
          points={{-8,-8},{6,6}},
          color={255,255,0},
          smooth=Smooth.None,
          thickness=1,
          origin={28,34},
          rotation=180),
        Line(
          points={{-10,0},{10,0}},
          color={255,255,0},
          smooth=Smooth.None,
          thickness=1,
          origin={0,42},
          rotation=90)}),                                        Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    Documentation(info="<html>
<h4><span style=\"color: #000000\">Last modifications: 2016-10-03</span></h4>
<p>This model allows to compute solar production for 3 groups of PVs (South, West, East) according to heating, DHW, and internals electrical loads (lights and equipments).</p>
<p>Each group can be remove using 0 as area.</p>
<p>Outputs:</p>
<ul>
<li>Electrical grid energy needed</li>
<li>Solar energy production</li>
</ul>
<p><br>West and East does not prodive the same performance and West seems better for at least Bordeaux weather data informations.</p>
</html>"));
end SolarGrid;
