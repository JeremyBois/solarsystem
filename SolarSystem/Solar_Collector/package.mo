within SolarSystem;
package Solar_Collector "Solar collectors models"
  extends Modelica.Icons.Package;


annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
          -100},{100,100}}), graphics={
        Ellipse(
        extent={{-68,70},{66,-64}},
        lineColor={200,200,10},
        fillColor={200,200,10},
        fillPattern=FillPattern.Solid),
        Line(
        points={{-16,-16},{8,8}},
        color={200,200,10},
        smooth=Smooth.None,
        thickness=1,
        origin={-72,76},
        rotation=90),
        Line(
        points={{-100,0},{-74,0}},
        color={200,200,10},
        smooth=Smooth.None,
        thickness=1),
        Line(
        points={{-82,-78},{-56,-52}},
        color={200,200,10},
        smooth=Smooth.None,
        thickness=1),
        Line(
        points={{-10,0},{22,1.36196e-015}},
        color={200,200,10},
        smooth=Smooth.None,
        thickness=1,
        origin={0,-88},
        rotation=90),
        Line(
        points={{-8,-8},{18,18}},
        color={200,200,10},
        smooth=Smooth.None,
        thickness=1,
        origin={72,-70},
        rotation=90),
        Line(
        points={{72,0},{100,0}},
        color={200,200,10},
        smooth=Smooth.None,
        thickness=1),
        Line(
        points={{-8,-8},{16,16}},
        color={200,200,10},
        smooth=Smooth.None,
        thickness=1,
        origin={72,76},
        rotation=180),
        Line(
        points={{-18,-6.49062e-015},{10,0}},
        color={200,200,10},
        smooth=Smooth.None,
        thickness=1,
        origin={0,90},
        rotation=90)}));
end Solar_Collector;
