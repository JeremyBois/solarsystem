within SolarSystem.Solar_Collector.Old;
model SolarGain
  import SI = Modelica.SIunits;
  extends SolarSystem.Data.Parameter.SolarCollector.PartialParameters;

  Modelica.Blocks.Interfaces.RealInput DirRad "Direct radiation"
    annotation (Placement(transformation(extent={{-120,50},{-80,90}})));
  Modelica.Blocks.Interfaces.RealInput DiffRad "Diffuse radiation"
    annotation (Placement(transformation(extent={{-120,10},{-80,50}})));
  Modelica.Blocks.Interfaces.RealInput Tfluid[nSeg] "Fluid temperature"
    annotation (Placement(transformation(extent={{-120,-90},{-80,-50}})));
  Modelica.Blocks.Interfaces.RealOutput SolarFlow[nSeg](final unit="W")
    "Solar heat gain"
    annotation (Placement(transformation(extent={{80,-16},{112,16}})));
  Modelica.Blocks.Interfaces.RealInput Text "Outside temperature"
    annotation (Placement(transformation(extent={{-120,-50},{-80,-10}})));

  SI.Irradiance ITotal " Direct and indirect Irradiation";
  SI.ThermalInsulance Tstar[nSeg];

equation
  // Direct and indirect radiation
  ITotal = DirRad + DiffRad;

  for i in 1 : nSeg loop
      // Intermediate variables
      Tstar[i] = (Tfluid[i] - Text) / ITotal;

      // Transmitted powers
      SolarFlow[i] = (ITotal * S / nSeg) * (Roptique - (a1*Tstar[i] + a2*ITotal*Tstar[i]*Tstar[i]));

  end for;

  annotation (Diagram(graphics));
end SolarGain;
