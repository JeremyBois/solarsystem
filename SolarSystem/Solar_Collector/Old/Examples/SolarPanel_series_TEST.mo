within SolarSystem.Solar_Collector.Old.Examples;
model SolarPanel_series_TEST
extends Modelica.Icons.Example;

  inner Modelica.Fluid.System system(
                                    T_ambient(displayUnit="degC")=
      283.15, p_ambient=300000)
    annotation (Placement(transformation(extent={{100,48},{120,68}})));

  Buildings.Fluid.Sensors.Temperature
                                    Tavant(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{-62,34},{-42,54}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    use_m_flow_in=false,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    m_flow=0.1,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{-102,-30},{-82,-10}})));
  Buildings.Fluid.Sensors.Temperature
                                    Tapres(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{42,16},{62,36}})));
  Buildings.Fluid.Sources.FixedBoundary boundary(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    p=300000,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{94,-42},{74,-22}})));
  SolarPanel_ISO    SolarPanels(
    nSeg=20,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    panelSpec(
      A=2,
      mDry=20,
      V=0.05,
      dp_nominal(displayUnit="Pa") = 100,
      mperA_flow_nominal=0.011,
      Roptique=0.8,
      a1=1.5,
      a2=0.001))
    annotation (Placement(transformation(extent={{-20,-36},{26,18}})));

  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(filNam=
        "C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-6,38},{6,50}})));
equation
  connect(Meteo.weaBus, SolarPanels.weaBus) annotation (Line(
      points={{6,44},{14,44},{14,8},{-14.02,8},{-14.02,2.88}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(boundary1.ports[1], SolarPanels.port_a) annotation (Line(
      points={{-82,-20},{-52,-20},{-52,-9},{-20,-9}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(SolarPanels.port_b, boundary.ports[1]) annotation (Line(
      points={{26,-9},{50,-9},{50,-32},{74,-32}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(SolarPanels.port_b, Tapres.port) annotation (Line(
      points={{26,-9},{40,-9},{40,16},{52,16}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tavant.port, SolarPanels.port_a) annotation (Line(
      points={{-52,34},{-36,34},{-36,-9},{-20,-9}},
      color={0,127,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=true,  extent={{-100,
            -60},{120,80}}),        graphics), Icon(coordinateSystem(
          extent={{-100,-60},{120,80}})),
    Commands(executeCall=alist - h),
    experiment(StopTime=3.1536e+007));
end SolarPanel_series_TEST;
