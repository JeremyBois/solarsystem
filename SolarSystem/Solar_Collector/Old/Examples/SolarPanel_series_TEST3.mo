within SolarSystem.Solar_Collector.Old.Examples;
model SolarPanel_series_TEST3
extends Modelica.Icons.Example;
  inner Modelica.Fluid.System system(
                                    T_ambient(displayUnit="degC")=
      283.15, p_ambient=300000)
    annotation (Placement(transformation(extent={{100,80},{120,100}})));

  Buildings.Fluid.Sensors.Temperature
                                    Tavant(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{-10,0},{10,20}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    m_flow=0.1,
    use_m_flow_in=true,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{-106,-66},{-86,-46}})));
  Buildings.Fluid.Sensors.Temperature
                                    Tapres(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{38,0},{58,20}})));
  Buildings.Fluid.Sources.FixedBoundary boundary(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    p=300000,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{108,-32},{88,-12}})));

  Classes.Control.Scheduled_solarPanel_control1 modulator(
    minGlobalRadiation=20,
    flowElse=0.000005,
    matrixSchedule=[6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21; 0,0,0,0,0,0,
        0,30,30,30,30,30,0,0,0,0],
    matrixFlowValue={0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,
        0.1,0.1},
    flowRate(start=0.1))
    annotation (Placement(transformation(extent={{-50,62},{-22,84}})));
  SolarPanel_ISO    SolarPanels(
    nSeg=20,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    panelSpec(
      A=2,
      mDry=20,
      V=0.05,
      dp_nominal(displayUnit="Pa") = 100,
      mperA_flow_nominal=0.011,
      Roptique=0.8,
      a1=1.5,
      a2=0.001))
    annotation (Placement(transformation(extent={{4,-62},{50,-8}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(filNam=
        "C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{18,-6},{30,6}})));
equation

  connect(modulator.flowRate, boundary1.m_flow_in) annotation (Line(
      points={{-22.6759,72.6},{-18,72.6},{-18,-26},{-124,-26},{-124,-48},
          {-106,-48}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(Tavant.T, modulator.T_in) annotation (Line(
      points={{7,10},{14,10},{14,52},{-60,52},{-60,70.35},{-48.4069,
          70.35}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tapres.T, modulator.T_out) annotation (Line(
      points={{55,10},{60,10},{60,56},{-54,56},{-54,65.4},{-48.4552,
          65.4}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(SolarPanels.port_b, boundary.ports[1]) annotation (Line(
      points={{50,-35},{70,-35},{70,-22},{88,-22}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary1.ports[1], SolarPanels.port_a) annotation (Line(
      points={{-86,-56},{-42,-56},{-42,-35},{4,-35}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tavant.port, SolarPanels.port_a) annotation (Line(
      points={{0,0},{2,0},{2,-35},{4,-35}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tapres.port, SolarPanels.port_b) annotation (Line(
      points={{48,0},{50,0},{50,-35}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Meteo.weaBus, SolarPanels.weaBus) annotation (Line(
      points={{30,0},{38,0},{38,-18},{9.98,-18},{9.98,-23.12}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=true,  extent={{-140,
            -80},{120,100}}),       graphics), Icon(coordinateSystem(
          extent={{-140,-80},{120,100}})),
    Commands(executeCall=alist - h),
    experiment(StopTime=3.1536e+007));
end SolarPanel_series_TEST3;
