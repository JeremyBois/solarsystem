within SolarSystem.Solar_Collector.Old.Examples;
model SolarPanel_series_TEST5 "Not working for now"
extends Modelica.Icons.Example;
  inner Modelica.Fluid.System system(
                                    T_ambient(displayUnit="degC")=
      283.15, p_ambient=300000)
    annotation (Placement(transformation(extent={{100,80},{120,100}})));

  Buildings.Fluid.Sensors.Temperature
                                    Tavant(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{-10,0},{10,20}})));
  Buildings.Fluid.Sources.MassFlowSource_T
                                 boundary1(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    m_flow=0.1,
    use_m_flow_in=true,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{-106,-66},{-86,-46}})));
  Buildings.Fluid.Sensors.Temperature
                                    Tapres(redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater)
    annotation (Placement(transformation(extent={{38,0},{58,20}})));
  Buildings.Fluid.Sources.FixedBoundary boundary(
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    p=300000,
    T=283.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{108,-32},{88,-12}})));

public
  BaseClassestt.DiffuseIsotropic RadiationInd(
    outSkyCon=false,
    outGroCon=false,
    til=0.5235987755983) "Indirect radiation on the panel"
    annotation (Placement(transformation(extent={{-116,44},{-92,64}})));
  BaseClassestt.DirectTiltedSurface RadiationDir(
    azi(displayUnit="deg") = 3.1415926535898,
    til=0.5235987755983,
    lat(displayUnit="deg") = 0.78539816339745) "Direct radiation on the panel"
    annotation (Placement(transformation(extent={{-116,72},{-92,90}})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3
                     Meteo(filNam="C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    "Donn�es m�t�o de la station m�t�o (Ajaccio)"
    annotation (Placement(transformation(extent={{-140,62},{-128,74}})));
  Classes.Control.Scheduled_solarPanel_control_noSample modulator(
    minGlobalRadiation=20,
    flowElse=0.000005,
    flowRate(start=0.000005))
    annotation (Placement(transformation(extent={{-50,62},{-22,84}})));
  SolarPanel_ISO    SolarPanels(
    nSeg=20,
    redeclare package Medium =
        Modelica.Media.Water.ConstantPropertyLiquidWater,
    panelSpec(
      A=2,
      mDry=20,
      V=0.05,
      dp_nominal(displayUnit="Pa") = 100,
      mperA_flow_nominal=0.011,
      Roptique=0.8,
      a1=1.5,
      a2=0.001))
    annotation (Placement(transformation(extent={{2,-62},{48,-8}})));
equation

  connect(Meteo.weaBus,RadiationDir. weaBus) annotation (Line(
      points={{-128,68},{-122,68},{-122,81},{-116,81}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));
  connect(Meteo.weaBus,RadiationInd. weaBus) annotation (Line(
      points={{-128,68},{-122,68},{-122,54},{-116,54}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));

  connect(RadiationDir.H, modulator.dirRad) annotation (Line(
      points={{-90.8,81},{-70.4,81},{-70.4,81.45},{-48.4069,81.45}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(RadiationInd.H, modulator.difRad) annotation (Line(
      points={{-90.8,54},{-70,54},{-70,77.25},{-48.4069,77.25}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(modulator.flowRate, boundary1.m_flow_in) annotation (Line(
      points={{-22.6759,72.6},{-18,72.6},{-18,-26},{-124,-26},{-124,-48},
          {-106,-48}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(modulator.T_in, Tavant.T) annotation (Line(
      points={{-48.4069,70.35},{-54,70.35},{-54,34},{18,34},{18,10},{7,
          10}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(modulator.T_out, Tapres.T) annotation (Line(
      points={{-48.4552,65.4},{-54,65.4},{-54,34},{62,34},{62,10},{55,
          10}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(SolarPanels.port_b, boundary.ports[1]) annotation (Line(
      points={{48,-35},{68,-35},{68,-22},{88,-22}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary1.ports[1], SolarPanels.port_a) annotation (Line(
      points={{-86,-56},{-42,-56},{-42,-35},{2,-35}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tavant.port, SolarPanels.port_a) annotation (Line(
      points={{0,0},{2,0},{2,-35}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Tapres.port, SolarPanels.port_b) annotation (Line(
      points={{48,0},{48,-35}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Meteo.weaBus, SolarPanels.weaBus) annotation (Line(
      points={{-128,68},{-122,68},{-122,-18},{7.98,-18},{7.98,-23.12}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None));

  annotation (Diagram(coordinateSystem(preserveAspectRatio=true,  extent={{-140,
            -80},{120,100}}),       graphics), Icon(coordinateSystem(
          extent={{-140,-80},{120,100}})),
    Commands(executeCall=alist - h),
    experiment(StopTime=3.1536e+007, Interval=120));
end SolarPanel_series_TEST5;
