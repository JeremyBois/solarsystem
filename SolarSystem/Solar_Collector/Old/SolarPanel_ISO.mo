within SolarSystem.Solar_Collector.Old;
model SolarPanel_ISO
extends SolarSystem.Solar_Collector.Old.PartialSolarPanel(final spec=panelSpec);
  parameter SolarSystem.Data.Parameter.SolarCollector.SolarParameters_old panelSpec
    "Performance data" annotation (choicesAllMatching=true);
  SolarGain solarGain(
    nSeg=nSeg,
    Roptique=panelSpec.Roptique,
    a1=panelSpec.a1,
    a2=panelSpec.a2,
    S=TotalArea_internal) annotation (Placement(transformation(extent={{6,50},{28,70}})));
equation
  connect(temSen.T, solarGain.Tfluid) annotation (Line(
      points={{-2,-32},{-10,-32},{-10,53},{6,53}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(solarGain.SolarFlow, heaGain.Q_flow) annotation (Line(
      points={{27.56,60},{62,60}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(HDirTil.H, solarGain.DirRad) annotation (Line(
      points={{-39,80},{-20,80},{-20,67},{6,67}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(HDifTilIso.H, solarGain.DiffRad) annotation (Line(
      points={{-39,40},{-20,40},{-20,63},{6,63}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(weaBus.TDryBul, solarGain.Text) annotation (Line(
      points={{-74,44},{-74,56},{6,56},{6,57}},
      color={255,204,51},
      thickness=0.5,
      smooth=Smooth.None), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}}));
  annotation (Diagram(graphics), Icon(graphics));
end SolarPanel_ISO;
