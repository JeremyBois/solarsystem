within SolarSystem.Utilities.Example;
model stay_on_test
  extends Modelica.Icons.Example;
  Timers.Stay_on stay_on[2](pause={1000,100})
    annotation (Placement(transformation(extent={{60,-16},{98,20}})));
  Modelica.Blocks.Sources.SawTooth value(
    amplitude=4,
    offset=-1,
    period=200)
               annotation (Placement(transformation(extent={{-80,20},
            {-60,40}})));
  Modelica.Blocks.Logical.LessThreshold Condition(threshold=2.5)
    "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-40,20},{-20,40}})));
  Modelica.Blocks.Sources.SawTooth value1(
    amplitude=4,
    offset=-1,
    period=200)
               annotation (Placement(transformation(extent={{-80,-40},
            {-60,-20}})));
  Modelica.Blocks.Logical.GreaterThreshold
                                        Condition1(
                                                  threshold=2.5)
    "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-40,-40},{-20,-20}})));
equation
  connect(value.y, Condition.u) annotation (Line(
      points={{-59,30},{-42,30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(value1.y, Condition1.u)
                                annotation (Line(
      points={{-59,-30},{-42,-30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Condition.y, stay_on[1].in_value) annotation (Line(
      points={{-19,30},{20,30},{20,2},{60,2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Condition1.y, stay_on[2].in_value) annotation (Line(
      points={{-19,-30},{20,-30},{20,2},{60,2}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(graphics), Documentation(info="<html>
<p>Test the stay_on modul.</p>
</html>"),
    experiment(StopTime=10000),
    __Dymola_experimentSetupOutput);
end stay_on_test;
