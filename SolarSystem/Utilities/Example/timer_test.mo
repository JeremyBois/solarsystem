within SolarSystem.Utilities.Example;
model timer_test
  extends Modelica.Icons.Example;
  Timers.Real_timer timer1(out_value(start=252), pause=1000)
    annotation (Placement(transformation(extent={{0,52},{44,88}})));
  Modelica.Blocks.Sources.Sine sine1(
    freqHz=1/86400,
    offset=273.15,
    amplitude=20,
    y(unit="K"),
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-78,60},{-58,80}})));
  Timers.Real_timer timer[2](
    out_value(start={252,252}),
    started_value(start={0,0}),
    pause={1000,300})
    annotation (Placement(transformation(extent={{0,-18},{44,18}})));
  Modelica.Blocks.Sources.Sine sine2(
    freqHz=1/86400,
    offset=273.15,
    amplitude=20,
    y(unit="K"),
    phase=-1.5707963267949,
    startTime=200)
    annotation (Placement(transformation(extent={{-80,20},{-60,40}})));
  Modelica.Blocks.Sources.Sine sine3(
    freqHz=1/86400,
    offset=273.15,
    amplitude=20,
    y(unit="K"),
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-82,-40},{-62,-20}})));
equation
  connect(sine1.y, timer1.in_value) annotation (Line(
      points={{-57,70},{0,70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine2.y, timer[1].in_value) annotation (Line(
      points={{-59,30},{-40,30},{-40,0},{0,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine3.y, timer[2].in_value) annotation (Line(
      points={{-61,-30},{-40,-30},{-40,0},{0,0}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(graphics), Documentation(info="<html>
<p>Test the timer with only one input</p>
<p>Test timer with multiple inputs and differents &QUOT;pause&QUOT; values.</p>
</html>"),
    experiment(StopTime=10000),
    __Dymola_experimentSetupOutput(doublePrecision=true));
end timer_test;

model Timer_test
extends Modelica.Icons.Example;
  Timers.Real_timer real_Timer(out_value(start=252), pause=1000)
    annotation (Placement(transformation(extent={{60,80},{100,120}})));
  Modelica.Blocks.Sources.Sine sine1(
    freqHz=1/86400,
    offset=273.15,
    amplitude=20,
    y(unit="K"),
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-100,62},{-80,82}})));
  Timers.Wait_for Wait_for(wait_for=500)
    "Outter value can become true because inner value lower than 2.5 during at least 90sec"
    annotation (Placement(transformation(extent={{60,20},{100,60}})));
  Modelica.Blocks.Logical.GreaterThreshold Condition(each threshold=255)
    "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-40,10},{-20,30}})));
  Timers.Stay_on Stay_on(pause=5000)
    annotation (Placement(transformation(extent={{60,-40},{100,0}})));
  Timers.Wait_for_tempo                  Tempo(
    out_value(start={false,false,false}),
    n=3,
    wait_for={1000,1500})
    annotation (Placement(transformation(extent={{60,-100},{100,-60}})));
  Modelica.Blocks.Logical.LessThreshold Condition1(each threshold=257.283)
    "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-40,-20},{-20,0}})));
  Modelica.Blocks.Logical.And Conditions "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-8,-2},{12,18}})));
equation
  connect(sine1.y, real_Timer.in_value) annotation (Line(
      points={{-79,72},{-10,72},{-10,100},{60,100}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine1.y, Condition.u) annotation (Line(
      points={{-79,72},{-52,72},{-52,20},{-42,20}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine1.y, Condition1.u) annotation (Line(
      points={{-79,72},{-66,72},{-66,72},{-52,72},{-52,-10},{-42,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Condition.y, Conditions.u1) annotation (Line(
      points={{-19,20},{-14,20},{-14,8},{-10,8}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Condition1.y, Conditions.u2) annotation (Line(
      points={{-19,-10},{-14,-10},{-14,0},{-10,0}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Conditions.y, Wait_for.in_value) annotation (Line(
      points={{13,8},{36,8},{36,40},{60,40}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Conditions.y, Stay_on.in_value) annotation (Line(
      points={{13,8},{36,8},{36,-20},{60,-20}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Conditions.y, Tempo.in_value[1]) annotation (Line(
      points={{13,8},{36,8},{36,-82.6667},{60,-82.6667}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Conditions.y, Tempo.in_value[2]) annotation (Line(
      points={{13,8},{36,8},{36,-80},{60,-80}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(Conditions.y, Tempo.in_value[3]) annotation (Line(
      points={{13,8},{36,8},{36,-77.3333},{60,-77.3333}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,120}}), graphics), Icon(coordinateSystem(extent={{-100,-100},
            {100,120}})));
end Timer_test;
