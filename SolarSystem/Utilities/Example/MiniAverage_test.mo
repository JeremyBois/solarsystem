within SolarSystem.Utilities.Example;
model MiniAverage_test
  extends Modelica.Icons.Example;
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(
      computeWetBulbTemperature=false, filNam=
        "C:/Users/bois/Documents/GitHub/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    annotation (Placement(transformation(extent={{-14,20},{14,48}})));
  Other.MiniAverage extract_TdryBulb(filNam=
        "C:/Users/bois/Documents/GitHub/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos")
    annotation (Placement(transformation(extent={{-34,-44},{34,0}})));
  parameter String filNam="C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Bordeaux/FRA_Bordeaux.075100_IWEC.mos"
    "Name of weather data file";
  annotation (Diagram(graphics), experiment(StopTime=3.1536e+007, Interval=360),
    __Dymola_experimentSetupOutput);
end MiniAverage_test;
