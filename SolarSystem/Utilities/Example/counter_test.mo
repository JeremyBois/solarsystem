within SolarSystem.Utilities.Example;
model counter_test
  import SolarSystem;
  extends Modelica.Icons.Example;
 parameter Integer n=4 "Number of inputs";

  SolarSystem.Utilities.Other.Counter counter(n=n)
    annotation (Placement(transformation(extent={{0,60},{20,80}})));
  Modelica.Blocks.Sources.Constant sine[n](k={1,4,5,1})
    annotation (Placement(transformation(extent={{-100,60},{-80,80}})));
  Modelica.Blocks.Logical.LessThreshold Condition[n](each threshold=2.5)
    "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-48,60},{-28,80}})));
  Modelica.Blocks.Sources.Sine sine1[n](
    each amplitude=5,
    each freqHz=1/500,
    each phase=-1.5707963267949,
    startTime={0,50,100,150})
    annotation (Placement(transformation(extent={{-100,-20},{-80,0}})));
  Modelica.Blocks.Logical.LessThreshold Condition1[
                                                  n](each threshold=2.5)
    "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-50,-20},{-30,0}})));
  SolarSystem.Utilities.Other.Counter counter1(n=n)
    annotation (Placement(transformation(extent={{0,-20},{20,0}})));
equation
  connect(sine.y, Condition.u) annotation (Line(
      points={{-79,70},{-50,70}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Condition.y, counter.in_value) annotation (Line(
      points={{-27,70},{0,70}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(sine1.y, Condition1.u) annotation (Line(
      points={{-79,-10},{-52,-10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Condition1.y, counter1.in_value) annotation (Line(
      points={{-29,-10},{0,-10}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(graphics),
    experiment(StopTime=1000),
    __Dymola_experimentSetupOutput);
end counter_test;
