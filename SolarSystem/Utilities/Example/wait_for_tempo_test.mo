within SolarSystem.Utilities.Example;
model wait_for_tempo_test
  import SolarSystem;
  extends Modelica.Icons.Example;
  parameter Integer n=3 "Number of inputs";
  Modelica.Blocks.Logical.LessThreshold Condition[n](each threshold=2.5)
    "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-50,-10},{-30,10}})));
  Modelica.Blocks.Sources.Sine sine(
    amplitude=5,
    freqHz=1/500,
    offset=0,
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-100,20},{-80,40}})));
  Modelica.Blocks.Sources.Pulse sine1(
    amplitude=5,
    offset=0,
    startTime=0,
    period=100)
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
  Modelica.Blocks.Sources.Constant sine2
    annotation (Placement(transformation(extent={{-100,-40},{-80,-20}})));
  SolarSystem.Utilities.Timers.Wait_for_tempo tempo(
    out_value(start={false,false,false}),
    n=n,
    wait_for={30,10}) annotation (Placement(transformation(extent={{0,-10},{20,10}})));
equation
  connect(sine.y, Condition[1].u) annotation (Line(
      points={{-79,30},{-68,30},{-68,0},{-52,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine1.y, Condition[2].u) annotation (Line(
      points={{-79,0},{-52,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine2.y, Condition[3].u) annotation (Line(
      points={{-79,-30},{-68,-30},{-68,0},{-52,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Condition.y, tempo.in_value) annotation (Line(
      points={{-29,0},{0,0}},
      color={255,0,255},
      smooth=Smooth.None));
  annotation (Diagram(graphics),
    experiment(StopTime=1000),
    __Dymola_experimentSetupOutput);
end wait_for_tempo_test;
