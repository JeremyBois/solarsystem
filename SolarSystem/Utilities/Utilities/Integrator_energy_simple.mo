within SolarSystem.Utilities.Utilities;
model Integrator_energy_simple

  Modelica.Blocks.Interfaces.RealInput Tin "Low temperature" annotation (
      Placement(transformation(extent={{-120,0},{-80,40}}),
        iconTransformation(extent={{-120,0},{-80,40}})));
  Modelica.Blocks.Interfaces.RealInput Tout "Hight temperature"
    annotation (Placement(transformation(extent={{-120,60},{-80,100}}),
        iconTransformation(extent={{-120,60},{-80,100}})));
  Modelica.Blocks.Interfaces.RealInput m_flow "Mass flow rate inside T1 and T2"
                                      annotation (Placement(
        transformation(extent={{-120,-50},{-80,-10}}), iconTransformation(
          extent={{-120,-100},{-80,-60}})));
protected
  Modelica.Blocks.Math.Add add(k2=-1)
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  Modelica.Blocks.Math.MultiProduct multiProduct(nu=3)
    annotation (Placement(transformation(extent={{10,-8},{26,8}})));
  Modelica.Blocks.Sources.RealExpression Cp(y=y)
    annotation (Placement(transformation(extent={{-102,-90},{-82,-70}})));
  Modelica.Blocks.Continuous.Integrator integrator
    annotation (Placement(transformation(extent={{48,-10},{68,10}})));

public
  Modelica.Blocks.Interfaces.RealOutput Energy "Energy evolution"
    annotation (Placement(transformation(extent={{80,-20},{120,20}}),
        iconTransformation(extent={{80,-20},{120,20}})));

  parameter Modelica.Blocks.Interfaces.RealOutput y=4180 "Value of Real output";
equation
  connect(Tout, add.u1) annotation (Line(
      points={{-100,80},{-70,80},{-70,56},{-62,56}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tin, add.u2) annotation (Line(
      points={{-100,20},{-70,20},{-70,44},{-62,44}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiProduct.y, integrator.u) annotation (Line(
      points={{27.36,0},{46,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(integrator.y, Energy) annotation (Line(
      points={{69,0},{100,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add.y, multiProduct.u[1]) annotation (Line(
      points={{-39,50},{-20,50},{-20,3.73333},{10,3.73333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(m_flow, multiProduct.u[2]) annotation (Line(
      points={{-100,-30},{-20,-30},{-20,4.44089e-016},{10,4.44089e-016}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Cp.y, multiProduct.u[3]) annotation (Line(
      points={{-81,-80},{-18,-80},{-18,-3.73333},{10,-3.73333}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
                                      graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
        graphics={ Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-98,10},{100,-2}},
          lineColor={0,128,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="Energy Integrator")}),
    Documentation(info="<html>
<p>Integration a fluid heat transfer using a constant fluid capacity. </p>
</html>"));
end Integrator_energy_simple;
