within SolarSystem.Utilities.Utilities;
model ZeroOrder

  Modelica.Blocks.Interfaces.RealInput u
    annotation (Placement(transformation(extent={{-120,-20},{-80,20}})));
  Modelica.Blocks.Interfaces.RealOutput y
    annotation (Placement(transformation(extent={{86,-10},{106,10}})));
  parameter Modelica.SIunits.Time t0 "Start time";
  parameter Modelica.SIunits.Duration It=1 "Scheduled time";
equation
  when sample(t0,It) then
    y=u;
  end when;
  annotation (Diagram(graphics), Documentation(info="<html>
</html>"));
end ZeroOrder;
