within SolarSystem.Utilities.Utilities;
model MiniAverage
  "Compute the current day average temperature and the minimal temperature during this day"

  parameter String filNam="C:/Users/bois/Documents/Dymola/SystemeSolaire/SolarSystem/Meteo/Marseille/FRA_Marseille.076500_IWEC.mos"
    "Name of weather data file"                                                                                                     annotation (Dialog(
        __Dymola_loadSelector(filter="Weather files (*.mos)", caption=
            "Select weather file")));
  parameter Modelica.SIunits.Temperature Tmoy_first=279.94
    "Average temperature from first day";
  parameter Modelica.SIunits.Temperature Tmin_first=275.15
    "Minimal temperature from first day";
  parameter Real offset=3600*24 "Temperature offset (time of an output step)";

protected
  Modelica.Blocks.Tables.CombiTable1Ds datRea(
    final tableOnFile=true,
    final tableName="tab1",
    final fileName=Buildings.BoundaryConditions.WeatherData.BaseClasses.getAbsolutePath(filNam),
    final smoothness=Modelica.Blocks.Types.Smoothness.ContinuousDerivative,
    final columns={2,3,4,5,6,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,
        28,29,30}) "Data reader"
    annotation (Placement(transformation(extent={{-30,0},{-10,20}})));
  Buildings.BoundaryConditions.WeatherData.BaseClasses.ConvertTime
                          conTim "Convert simulation time to calendar time"
    annotation (Placement(transformation(extent={{-60,0},{-40,20}})));
  Buildings.Utilities.SimulationTime
                           simTim "Simulation time"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-90,90})));
  Modelica.Blocks.Math.UnitConversions.From_degC conTDryBul
    annotation (Placement(transformation(extent={{0,0},{20,20}})));
  Buildings.BoundaryConditions.WeatherData.BaseClasses.CheckTemperature
    cheTemDryBul "Check dry bulb temperature "
    annotation (Placement(transformation(extent={{30,0},{50,20}})));
  Modelica.Blocks.Math.Add add annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-72,50})));
  Modelica.Blocks.Sources.RealExpression offset_value(y=offset)
                                                           annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=-90,
        origin={-56,90})));
  Modelica.Blocks.Math.Mean TmoyDay(                 f=1/3600/24, y(start=
          Tmoy_first)) "Day averaged outdoor temperature"
    annotation (Placement(transformation(extent={{60,0},{80,20}})));

  Modelica.Blocks.Sources.RealExpression Temp_mini_day(y=Tmin_day)
    annotation (Placement(transformation(extent={{40,40},{60,60}})));
public
  Modelica.Blocks.Interfaces.RealOutput TminOfDay(start=Tmin_first)
    annotation (Placement(transformation(extent={{92,30},{132,70}}),
        iconTransformation(extent={{92,26},{120,54}})));
  Modelica.Blocks.Interfaces.RealOutput TmoyOfDay(start=Tmoy_first)
    annotation (Placement(transformation(extent={{92,-30},{132,-70}}),
        iconTransformation(extent={{92,-54},{120,-26}})));

public
Real Tmin(start=Tmin_first);
Real Tmin_day(start=Tmin_first);

equation
    // Check temperature every 30' (change it only if new temperature is below old value)
    when mod(time, 3600/2) < 1 then
      Tmin = (if pre(Tmin) > cheTemDryBul.TOut then cheTemDryBul.TOut else Tmin);
    end when;

   //Reinit minimal value to first day temperature
   when mod(time, 3600*24) < 1 then
     Tmin_day = Tmin;
     reinit(Tmin, cheTemDryBul.TOut);
   end when;

  connect(conTim.calTim,datRea. u) annotation (Line(
      points={{-39,10},{-32,10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(datRea.y[1], conTDryBul.u) annotation (Line(
      points={{-9,9.04},{-6,9.04},{-6,10},{-2,10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(conTDryBul.y, cheTemDryBul.TIn) annotation (Line(
      points={{21,10},{28,10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(simTim.y, add.u2) annotation (Line(
      points={{-90,79},{-90,68},{-78,68},{-78,62}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(offset_value.y, add.u1)
                            annotation (Line(
      points={{-56,79},{-56,68},{-66,68},{-66,62}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add.y, conTim.simTim) annotation (Line(
      points={{-72,39},{-72,10},{-62,10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(cheTemDryBul.TOut, TmoyDay.u) annotation (Line(
      points={{51,10},{58,10}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TmoyDay.y, TmoyOfDay) annotation (Line(
      points={{81,10},{86,10},{86,-50},{112,-50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Temp_mini_day.y, TminOfDay) annotation (Line(
      points={{61,50},{112,50}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(graphics),
    experiment(StopTime=3.1536e+007, Interval=120),
    __Dymola_experimentSetupOutput,
    Icon(graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-98,64},{100,52}},
          lineColor={0,128,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="Average and Minimal"),
        Text(
          extent={{-102,-36},{102,-54}},
          lineColor={0,128,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="Step = %offset"),
        Text(
          extent={{-98,34},{94,18}},
          lineColor={0,128,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="Temperature")}),
    Documentation(info="<html>
<p>Compute minimal and average temperature during an define time (offset). Input must be a weather file with the .mos extension.</p>
<p>Be careful with simulation time step. It must match file time step (or a multiple of file time step).</p>
<p><br><a href=\"SolarSystem.Classes.Other.Examples.MiniAverage_TEST\">SolarSystem.Classes.Other.Examples.MiniAverage_TEST</a></p>
</html>"));
end MiniAverage;
