within SolarSystem.Utilities.Utilities;
model Integrator_energy "Only integrate in a specific direction (when mass flow rate >= 0)"

  Modelica.Blocks.Interfaces.RealInput Tin "Low temperature" annotation (
      Placement(transformation(extent={{-120,10},{-80,50}}),
        iconTransformation(extent={{-120,0},{-80,40}})));
  Modelica.Blocks.Interfaces.RealInput Tout "Hight temperature"
    annotation (Placement(transformation(extent={{-120,60},{-80,100}}),
        iconTransformation(extent={{-120,60},{-80,100}})));
  Modelica.Blocks.Interfaces.RealInput m_flow "Mass flow rate inside T1 and T2"
                                      annotation (Placement(
        transformation(extent={{-120,-50},{-80,-10}}), iconTransformation(
          extent={{-120,-100},{-80,-60}})));
protected
  Modelica.Blocks.Math.Add add(k2=-1)
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualThreshold
    annotation (Placement(transformation(extent={{-60,-40},{-40,-20}})));
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{-20,-40},{0,-20}})));
  Modelica.Blocks.Sources.RealExpression zero_flow
    annotation (Placement(transformation(extent={{-60,-74},{-40,-54}})));
  Modelica.Blocks.Math.MultiProduct multiProduct(nu=3)
    annotation (Placement(transformation(extent={{22,10},{36,24}})));
  Modelica.Blocks.Sources.RealExpression Cp(y=y)
    annotation (Placement(transformation(extent={{-60,-90},{-40,-70}})));
  Modelica.Blocks.Continuous.Integrator integrator
    annotation (Placement(transformation(extent={{46,8},{66,28}})));
  Modelica.Blocks.Math.Abs abs1
    annotation (Placement(transformation(extent={{-26,40},{-6,60}})));

public
  Modelica.Blocks.Interfaces.RealOutput Energy "Energy evolution"
    annotation (Placement(transformation(extent={{80,-20},{120,20}}),
        iconTransformation(extent={{80,-20},{120,20}})));

  parameter Modelica.Blocks.Interfaces.RealOutput y=4180 "Value of Real output";
equation
  connect(m_flow, greaterEqualThreshold.u) annotation (Line(
      points={{-100,-30},{-62,-30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(greaterEqualThreshold.y, switch1.u2) annotation (Line(
      points={{-39,-30},{-22,-30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(m_flow, switch1.u1) annotation (Line(
      points={{-100,-30},{-72,-30},{-72,0},{-32,0},{-32,-22},{-22,-22}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(zero_flow.y, switch1.u3) annotation (Line(
      points={{-39,-64},{-30,-64},{-30,-38},{-22,-38}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tout, add.u1) annotation (Line(
      points={{-100,80},{-70,80},{-70,56},{-62,56}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tin, add.u2) annotation (Line(
      points={{-100,30},{-70,30},{-70,44},{-62,44}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiProduct.y, integrator.u) annotation (Line(
      points={{37.19,17},{40,17},{40,18},{44,18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(integrator.y, Energy) annotation (Line(
      points={{67,18},{74,18},{74,0},{100,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add.y, abs1.u) annotation (Line(
      points={{-39,50},{-28,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(abs1.y, multiProduct.u[1]) annotation (Line(
      points={{-5,50},{0,50},{0,20.2667},{22,20.2667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(switch1.y, multiProduct.u[2]) annotation (Line(
      points={{1,-30},{8,-30},{8,17},{22,17}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Cp.y, multiProduct.u[3]) annotation (Line(
      points={{-39,-80},{16,-80},{16,13.7333},{22,13.7333}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent=
            {{-100,-100},{100,100}}), graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
        graphics={ Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-98,10},{100,-2}},
          lineColor={0,128,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="Energy Integrator")}),
    Documentation(info="<html>
<p>Integration a fluid heat transfer only a specific direction using a constant fluid capacity. </p>
</html>"));
end Integrator_energy;
