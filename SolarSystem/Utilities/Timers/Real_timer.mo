within SolarSystem.Utilities.Timers;
model Real_timer
  "This model allows to wait a `pause` time before change the output value."

  Modelica.Blocks.Interfaces.RealInput in_value
    annotation (Placement(transformation(extent={{-120,-20},{-80,20}}),
        iconTransformation(extent={{-120,-20},{-80,20}})));
  Modelica.Blocks.Interfaces.RealOutput out_value
    annotation (Placement(transformation(extent={{100,-20},{140,20}}),
        iconTransformation(extent={{100,-20},{140,20}})));

 parameter Modelica.SIunits.Time  pause=200;
 discrete Modelica.SIunits.Time started_value(start=0);
 Modelica.SIunits.Time timer;

equation
// We accept new value for output
when timer > pause then
  started_value = time;  // Keep trace of new value time position
  out_value = in_value;  // Let pass input signal
  reinit(timer, 0);
end when;

// Finally
timer = time - pre(started_value);

  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),
                      graphics), Icon(graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,255},
          fillPattern=FillPattern.Sphere,
          fillColor={0,127,127}), Text(
          extent={{-46,30},{48,-24}},
          lineColor={0,0,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="%pause",
          fontName="Consolas")}),
    Documentation(info="<html>
<p>This model allows to wait a &QUOT;pause&QUOT; time before change the output value.</p>
<p>If inputs are <b>Booleans</b> you must use <a href=\" SolarSystem.Classes.Control.Bloc.boolean_timer\">SolarSystem.Classes.Control.Bloc.boolean_timer</a> instead.</p>
</html>"));
end Real_timer;
