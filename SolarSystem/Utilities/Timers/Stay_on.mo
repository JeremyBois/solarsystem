within SolarSystem.Utilities.Timers;
model Stay_on
  " Once input become true the output stay true at least during a pause time."

  Modelica.Blocks.Interfaces.BooleanInput
                                       in_value
    annotation (Placement(transformation(extent={{-120,-20},{-80,20}}),
        iconTransformation(extent={{-120,-20},{-80,20}})));
  Modelica.Blocks.Interfaces.BooleanOutput
                                        out_value(start=true)
    annotation (Placement(transformation(extent={{100,-20},{140,20}}),
        iconTransformation(extent={{100,-20},{140,20}})));

parameter Modelica.SIunits.Time  pause=200;
discrete Modelica.SIunits.Time started_on;

protected
Modelica.SIunits.Time timer;
Modelica.SIunits.Time pre_state(start=0);
discrete Modelica.SIunits.Time started_off;

initial equation
  started_on = time;
equation
// When "in_value" becomes true again
when in_value and pre_state == 1 then
  started_on = time;  // Keep trace of new value time position
  reinit(timer, 0);
  reinit(pre_state, 0);
end when;

// "Out_value" state (we use 0.5 because we can't check equality)
out_value = (if pre_state < 0.5 then true else false);

// When "out_value" becomes false
when not out_value then
  started_off = time; // Keep trace of new value time position
end when;

// When "in_value" becomes false after pause duration
when timer >= pause and not in_value then
  pre_state = 1;
end when;

// Finally
timer = time - pre(started_on);

  annotation (Diagram(graphics), Icon(graphics={Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={127,0,127},
          fillPattern=FillPattern.Sphere,
          fillColor={128,0,255}), Text(
          extent={{-46,30},{42,-24}},
          lineColor={0,0,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="%pause",
          fontName="Consolas")}),
    Documentation(info="<html>
<p>This model allows to wait a &QUOT;pause&QUOT; time when the inner value becomes true. During this time the outter value can only be true even if inner value becomes false.</p>
<p>Check for stay_on_test for an example <a href=\"SolarSystem.Classes.Control.Bloc.Example.stay_on_test\">SolarSystem.Classes.Control.Bloc.Example.stay_on_test</a>.</p>
</html>"));
end Stay_on;
