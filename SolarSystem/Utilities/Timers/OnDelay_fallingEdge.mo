within SolarSystem.Utilities.Timers;
block OnDelay_fallingEdge
  "Delay a falling edge of the input, but do not delay a rising edge."
  extends Modelica.Blocks.Interfaces.partialBooleanSISO;
  parameter Modelica.SIunits.Time delayTime "Delay time";

protected
  Boolean delaySignal(start=false, fixed=true);
  discrete Modelica.SIunits.Time t_next;

initial equation
  pre(u) = false;
  pre(t_next) = time - 1;

algorithm
  when not u then
    delaySignal := true;
    t_next := time + delayTime;
  elsewhen u then
    delaySignal := false;
    t_next := time - 1;
  end when;

equation
  if delaySignal then
    y = time <= t_next;
  else
    y = true;
  end if;
  annotation (Icon(graphics={
        Text(
          extent={{-250,-120},{250,-150}},
          lineColor={0,0,0},
          textString="%delayTime s"),
        Line(points={{-82,-64},{-62,-64},{-62,-22},{-16,-22},{-16,-66},{66,-66}},
            color={0,0,0}),
        Line(points={{-80,32},{-62,32},{-62,76},{38,76},{38,32},{66,32}},
            color={255,0,255})}), Documentation(info="<html>
<p>
A falling edge of the Boolean input u gives a delayed output.
A rising edge of the input is immediately given to the output.
</p>

</html>"));
end OnDelay_fallingEdge;
