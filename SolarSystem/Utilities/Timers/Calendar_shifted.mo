within SolarSystem.Utilities.Timers;
model Calendar_shifted "Shift time to use data in the \"future\""

protected
  Modelica.Blocks.Math.Add add
    "Add 30 minutes to time to shift weather data reader"
    annotation (Placement(transformation(extent={{-120,-28},{-100,-8}})));
  Buildings.BoundaryConditions.WeatherData.BaseClasses.ConvertTime conTim1
    "Convert simulation time to calendar time"
    annotation (Placement(transformation(extent={{-80,-28},{-60,-8}})));
  Buildings.Utilities.Time.ModelTime
                           modTim "Model time"
    annotation (Placement(transformation(extent={{-160,-58},{-140,-38}})));
  Modelica.Blocks.Sources.Constant Shift(final k=shift)
    "Constant used to shift weather data reader"
    annotation (Placement(transformation(extent={{-160,2},{-140,22}})));
public
  parameter Modelica.SIunits.Time shift=1800 "Shift size [s]";
  Modelica.Blocks.Interfaces.RealOutput calendar_shifted_time
    "Shifted calendar time." annotation (Placement(transformation(extent={{36,-38},
            {76,2}}),  iconTransformation(extent={{40,-32},{62,-10}})));
equation
  connect(add.y, conTim1.modTim) annotation (Line(
      points={{-99,-18},{-82,-18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Shift.y, add.u1) annotation (Line(
      points={{-139,12},{-130,12},{-130,-12},{-122,-12}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(modTim.y, add.u2) annotation (Line(
      points={{-139,-48},{-130,-48},{-130,-24},{-122,-24}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(conTim1.calTim, calendar_shifted_time) annotation (Line(
      points={{-59,-18},{56,-18}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-160,
            -120},{40,80}}),graphics), Icon(coordinateSystem(extent={{-160,-120},
            {40,80}}, preserveAspectRatio=false), graphics={
                                Rectangle(
        extent={{-160,-120},{40,80}},
        lineColor={0,0,127},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
        Text(
          extent={{14,-14},{38,-24}},
          lineColor={0,0,127},
          textString="calTim"),
        Rectangle(
          extent={{-126,56},{0,38}},
          lineColor={0,0,0},
          fillPattern=FillPattern.Solid,
          fillColor={120,120,120}),
        Rectangle(extent={{-126,38},{0,-82}}, lineColor={0,0,0}),
        Line(
          points={{-84,-82},{-84,38}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{-42,-82},{-42,38}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{0,8},{-126,8}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{0,-22},{-126,-22}},
          color={0,0,0},
          smooth=Smooth.None),
        Line(
          points={{0,-52},{-126,-52}},
          color={0,0,0},
          smooth=Smooth.None)}));
end Calendar_shifted;
