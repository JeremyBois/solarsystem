within SolarSystem.Utilities.Timers;
block OnDelay_pause "Force the output to stay true at least delayTime time"
  extends Modelica.Blocks.Interfaces.partialBooleanSISO;
  parameter Modelica.SIunits.Time delayTime "Delay time";

protected
  discrete Modelica.SIunits.Time t_next;

initial equation
  pre(u) = false;
  pre(t_next) = time - 1;

equation
  // Only update delay if output false (avoid output to always stay true)
  when (u and not pre(y)) then
    t_next = time + delayTime;
  end when;

  if time <= t_next then
    y = true;
  // If delay over just let the output be the same as the input
  else
    y = u;
  end if;

  annotation (Icon(graphics={
        Text(
          extent={{-250,-120},{250,-150}},
          lineColor={0,0,0},
          textString="%delayTime s"),
        Line(points={{-82,-64},{-62,-64},{-62,-22},{-16,-22},{-16,-66},{66,-66}},
            color={0,0,0}),
        Line(points={{-80,32},{-62,32},{-62,76},{38,76},{38,32},{66,32}},
            color={255,0,255})}), Documentation(info="<html>
<p>
Wait delayTime when a rising edge occurs. During this delay the output cannot be false.
</p>

</html>"));
end OnDelay_pause;
