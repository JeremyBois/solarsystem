within SolarSystem.Utilities.Logical;
model True_seeker " : Return true if any true in u"

  Modelica.Blocks.Interfaces.BooleanVectorInput
                                          u[n]
                                            annotation (Placement(
        transformation(extent={{-120,-20},{-80,20}}), iconTransformation(extent=
           {{-120,-20},{-80,20}})));
  Modelica.Blocks.Interfaces.BooleanOutput y annotation (Placement(
        transformation(extent={{96,-10},{116,10}}), iconTransformation(extent={{
            80,-20},{120,20}})));

parameter Integer n=2;

equation
  y = Modelica.Math.BooleanVectors.anyTrue(u);

  annotation (Icon(graphics={
        Polygon(
          points={{-100,100},{100,100},{100,-100},{-100,-100},{-100,100},
              {-100,100},{-100,100}},
          lineColor={255,0,255},
          smooth=Smooth.None),    Text(
          extent={{-88,74},{104,42}},
          lineColor={0,0,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          textStyle={TextStyle.Bold},
          textString="Any True ?",
          fontName="Consolas"),
        Text(
          extent={{-98,-38},{98,-60}},
          lineColor={0,0,0},
          textString="vector size = %n",
          fontName="Consolas")}),
                              Diagram(coordinateSystem(preserveAspectRatio=
            false, extent={{-100,-100},{100,100}}),
                                      graphics),
    Documentation(info="<html>
<p>This model return True as output if at least one element inside the vector input are True, else output is False</p>
</html>"));
end True_seeker;
