within SolarSystem.Utilities.Logical;
block Hysteresis_inversed
  "Transform Real to Boolean signal with reversed Hysteresis"

  extends Modelica.Blocks.Icons.PartialBooleanBlock;
  parameter Real uLow(start=0) "if y=false and u<uLow, switch to y=true";
  parameter Real uHigh(start=1) "if y=true and u>uHigh, switch to y=false";
  parameter Boolean pre_y_start=true "Value of pre(y) at initial time";

  Modelica.Blocks.Interfaces.RealInput u annotation (Placement(transformation(
          extent={{-140,-20},{-100,20}}, rotation=0)));
  Modelica.Blocks.Interfaces.BooleanOutput y annotation (Placement(
        transformation(extent={{100,-10},{120,10}}, rotation=0)));

initial equation
  pre(y) = pre_y_start;
equation
  y = u > uLow and not pre(y) or u <= uHigh;
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{
            100,100}}), graphics={Polygon(
            points={{-65,89},{-73,67},{-57,67},{-65,89}},
            lineColor={192,192,192},
            fillColor={192,192,192},
            fillPattern=FillPattern.Solid),Line(points={{-65,67},{-65,-81}},
          color={192,192,192}),Line(points={{-90,-70},{82,-70}}, color={192,
          192,192}),Polygon(
            points={{90,-70},{68,-62},{68,-78},{90,-70}},
            lineColor={192,192,192},
            fillColor={192,192,192},
            fillPattern=FillPattern.Solid),Text(
            extent={{70,-80},{94,-100}},
            lineColor={160,160,164},
            textString="u"),Text(
            extent={{-65,93},{-12,75}},
            lineColor={160,160,164},
            textString="y"),Line(
            points={{-80,-70},{30,-70}},
            color={0,0,0},
            thickness=0.5),Line(
            points={{-50,10},{80,10}},
            color={0,0,0},
            thickness=0.5),Line(
            points={{-50,10},{-50,-70}},
            color={0,0,0},
            thickness=0.5),Line(
            points={{30,10},{30,-70}},
            color={0,0,0},
            thickness=0.5),Line(
            points={{-14,15},{-4,10},{-14,5}},
            color={0,0,0},
            thickness=0.5),Line(
            points={{-4,-65},{-14,-70},{-4,-75}},
            color={0,0,0},
            thickness=0.5),Line(
            points={{25,-20},{30,-30},{36,-20}},
            color={0,0,0},
            thickness=0.5),Line(
            points={{-55,-30},{-50,-19},{-45,-30}},
            color={0,0,0},
            thickness=0.5),Text(
            extent={{-99,2},{-70,18}},
            lineColor={160,160,164},
            textString="true"),Text(
            extent={{-98,-87},{-66,-73}},
            lineColor={160,160,164},
            textString="false"),Text(
            extent={{19,-87},{44,-70}},
            lineColor={0,0,0},
            textString="uHigh"),Text(
            extent={{-63,-88},{-38,-71}},
            lineColor={0,0,0},
            textString="uLow"),Line(points={{-69,10},{-60,10}}, color={160,
          160,164})}),
    Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,
            100}}), graphics={
        Polygon(
          points={{-80,90},{-88,68},{-72,68},{-80,90}},
          lineColor={192,192,192},
          fillColor={192,192,192},
          fillPattern=FillPattern.Solid),
        Line(points={{-80,68},{-80,-29}}, color={192,192,192}),
        Polygon(
          points={{92,-29},{70,-21},{70,-37},{92,-29}},
          lineColor={192,192,192},
          fillColor={192,192,192},
          fillPattern=FillPattern.Solid),
        Line(points={{-79,-29},{84,-29}}, color={192,192,192}),
        Line(points={{-79,-29},{41,-29}}, color={0,0,0}),
        Line(points={{-13,59},{3,51},{-13,44}},    color={0,0,0}),
        Line(points={{41,51},{41,-29}}, color={0,0,0}),
        Line(points={{-57,7},{-49,26},{-40,7}},
                                             color={0,0,0}),
        Line(points={{-49,51},{81,51}}, color={0,0,0}),
        Line(points={{4,-21},{-11,-29},{4,-37}},color={0,0,0}),
        Line(points={{31,23},{41,5},{51,23}},     color={0,0,0}),
        Line(points={{-49,51},{-49,-29}}, color={0,0,0}),
        Text(
          extent={{-92,-49},{-9,-92}},
          lineColor={192,192,192},
          textString="%uLow"),
        Text(
          extent={{2,-49},{91,-92}},
          lineColor={192,192,192},
          textString="%uHigh"),
        Rectangle(extent={{-91,-49},{-8,-92}}, lineColor={192,192,192}),
        Line(points={{-49,-29},{-49,-49}}, color={192,192,192}),
        Rectangle(extent={{2,-49},{91,-92}}, lineColor={192,192,192}),
        Line(points={{41,-29},{41,-49}}, color={192,192,192})}),
    Documentation(info="<HTML>
<p>
This block transforms a <b>Real</b> input signal into a <b>Boolean</b>
output signal:
</p>
<ul>
<li> When the output was <b>false</b> and the input becomes
     <b>greater</b> than parameter <b>uHigh</b>, the output
     switches to <b>true</b>.</li>
<li> When the output was <b>true</b> and the input becomes
     <b>less</b> than parameter <b>uLow</b>, the output
     switches to <b>false</b>.</li>
</ul>
<p>
The start value of the output is defined via parameter
<b>pre_y_start</b> (= value of pre(y) at initial time).
The default value of this parameter is <b>false</b>.
</p>
</html>"));
end Hysteresis_inversed;
