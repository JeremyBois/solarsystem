within SolarSystem.Utilities.Other;
model TankConvection
  "Compute tank convection outside the house model to make integration easier"

  // Surfaces
public
  parameter Modelica.SIunits.Area A[4] "Heat transfer area for each surface";

  // Walls inclinaison
protected
  parameter Modelica.SIunits.Angle F_=
    SolarSystem.Data.Parameter.Construction.Tilt.Floor "Tilt for foor";
  parameter Modelica.SIunits.Angle W_=
    SolarSystem.Data.Parameter.Construction.Tilt.Wall "Tilt for wall";

  Buildings.HeatTransfer.Convection.Interior convSurBou[4](
    final A=A,
    final hFixed={7.7,7.7,7.7,7.7},
    each conMod=Buildings.HeatTransfer.Types.InteriorConvection.Temperature,
    each final homotopyInitialization=true,
    final til={W_,W_,F_,F_})               "Convective heat transfer"
    annotation (Placement(transformation(extent={{-40,-10},{-60,10}})));
protected
  Modelica.Thermal.HeatTransfer.Components.ThermalCollector theConSurBou(final m=4)
    "Thermal collector to convert from vector to scalar connector"
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={64,0})));
public
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a port_a[4]
    "1=vertical DHW, 2=vertical Buffer, 3=horizontal DHW, 4=horizontal Buffer"
    annotation (Placement(transformation(extent={{-106,-10},{-86,10}})));
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_b port_b
    annotation (Placement(transformation(extent={{86,-10},{106,10}})));
protected
  Modelica.Blocks.Continuous.Integrator Integrator[2]
    "Integration of tank losses after conduction and convection."
    annotation (Placement(transformation(extent={{20,60},{40,80}})));
public
  Modelica.Blocks.Interfaces.RealOutput DHW_Energy(unit="J")
    "Energy lost from sanitary tank"
    annotation (Placement(transformation(extent={{94,72},{114,92}})));
protected
  Modelica.Blocks.Math.Add cumul[2] annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-10,50})));
  Modelica.Thermal.HeatTransfer.Sensors.HeatFlowSensor heatFlowSensor[4]
    annotation (Placement(transformation(extent={{-20,10},{0,-10}})));

public
  Modelica.Blocks.Interfaces.RealOutput Buffer_Energy(unit="J")
    "Energy lost from buffer tank"
    annotation (Placement(transformation(extent={{94,42},{114,62}})));
equation
  connect(theConSurBou.port_b, port_b) annotation (Line(points={{74,-1.77636e-015},
          {74,0},{96,0}}, color={191,0,0}));
  connect(port_a, convSurBou.fluid)
    annotation (Line(points={{-96,0},{-60,0}}, color={191,0,0}));
  connect(cumul.y, Integrator.u)
    annotation (Line(points={{-10,61},{-10,70},{18,70}}, color={0,0,127}));
  connect(convSurBou.solid, heatFlowSensor.port_a)
    annotation (Line(points={{-40,0},{-30,0},{-20,0}}, color={191,0,0}));
  connect(heatFlowSensor.port_b, theConSurBou.port_a) annotation (Line(points={{
          0,0},{12,0},{12,1.77636e-015},{54,1.77636e-015}}, color={191,0,0}));
  connect(heatFlowSensor[1].Q_flow, cumul[1].u1) annotation (Line(points={{-10,10},
          {-10,20},{-16,20},{-16,38}}, color={0,0,127}));
  connect(heatFlowSensor[3].Q_flow, cumul[1].u2) annotation (Line(points={{-10,10},
          {-10,10},{-10,20},{-4,20},{-4,38}}, color={0,0,127}));
  connect(heatFlowSensor[2].Q_flow, cumul[2].u1) annotation (Line(points={{-10,10},
          {-10,10},{-10,20},{-18,20},{-18,30},{-16,30},{-16,38}},
                                                color={0,0,127}));
  connect(heatFlowSensor[4].Q_flow, cumul[2].u2) annotation (Line(points={{-10,10},
          {-10,20},{-2,20},{-2,30},{-4,30},{-4,38}},
                                     color={0,0,127}));
  connect(Integrator[1].y, DHW_Energy) annotation (Line(points={{41,70},{70,70},
          {70,82},{104,82}}, color={0,0,127}));
  connect(Integrator[2].y, Buffer_Energy) annotation (Line(points={{41,70},{70,70},
          {70,52},{104,52}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end TankConvection;
