within SolarSystem.Utilities.Other;
model Integrator_with_conditions_smooth
  "Integration of energy if at least one input conditions is True."

   parameter Integer n=2 "Number of inputs";

  Modelica.Blocks.Interfaces.BooleanVectorInput Conditions[n]
    "Array of condition used to allow or not to compute power integration."
    annotation (Placement(transformation(extent={{-120,40},{-80,80}}),
        iconTransformation(extent={{-110,44},{-90,64}})));
  Modelica.Blocks.Interfaces.RealInput Power "Power to integrate" annotation (
      Placement(transformation(extent={{-126,0},{-86,40}}),
        iconTransformation(extent={{-108,8},{-86,30}})));
protected
  Modelica.Blocks.Continuous.Integrator Computed_integration
    "Integration with respect to conditions."
    annotation (Placement(transformation(extent={{-60,50},{-40,70}})));

public
  Modelica.Blocks.Interfaces.RealOutput Computed_Energy( each unit="J")
    "Energy evolution account for condtions. " annotation (__Dymola_tag={"Energy"}, Placement(
        transformation(extent={{-20,40},{20,80}}), iconTransformation(extent={{-18,50},
            {2,70}})));
protected
  Modelica.Blocks.Continuous.Integrator Classical_integration
    "Integration of power without taking care of inputs conditions."
    annotation (Placement(transformation(extent={{-60,10},{-40,30}})));
public
  Modelica.Blocks.Interfaces.RealOutput Classic_Energy( each unit="J")
    "Energy evolution without account for condtions. " annotation (__Dymola_tag={"Energy"}, Placement(
        transformation(extent={{-20,0},{20,40}}),    iconTransformation(extent={{-18,10},
            {2,30}})));
equation
  // Power calculation
  Computed_integration.u = smooth(0, if Modelica.Math.BooleanVectors.anyTrue(Conditions) then Power else 0)
    "Compute power gain inside collector";
  Classical_integration.u = Power;
  connect(Computed_integration.y, Computed_Energy) annotation (Line(
      points={{-39,60},{0,60}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Classical_integration.y, Classic_Energy) annotation (Line(
      points={{-39,20},{0,20}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,0},
            {-20,80}}),               graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,0},{-20,80}}),
        graphics={ Rectangle(
          extent={{-100,80},{-18,0}},
          lineColor={0,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-84,56},{-24,28}},
          lineColor={0,128,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="%name")}),
    Documentation(info="<html>
<p>Integration a fluid heat transfer using a constant fluid capacity. </p>
</html>"));
end Integrator_with_conditions_smooth;
