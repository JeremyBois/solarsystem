within SolarSystem.Utilities.Other;
model Integrator_losses
  "Two integrator for losses and gains (see documentation)"

  Modelica.Blocks.Interfaces.RealInput Heat_flow "Heat flow rate to integrate"
    annotation (Placement(transformation(extent={{-120,-20},{-80,20}}),
        iconTransformation(extent={{-108,-14},{-80,14}})));
protected
  Modelica.Blocks.Continuous.Integrator integrator_cumul(k=-1)
    "Gain of -1 in order to have positive energy for lost energy."
    annotation (Placement(transformation(extent={{-60,20},{-40,40}})));

public
  Modelica.Blocks.Interfaces.RealOutput Energy_cumulated
    "Energy evolution representing losses and gains (lost energy positive)"
    annotation (Placement(transformation(extent={{-20,10},{20,50}}),
        iconTransformation(extent={{-20,8},{4,32}})));
protected
  Modelica.Blocks.Continuous.Integrator integrator_losses(k=-1)
    "Gain of -1 in order to have positive energy for lost energy."
    annotation (Placement(transformation(extent={{-60,-40},{-40,-20}})));
public
  Modelica.Blocks.Interfaces.RealOutput Energy_losses
    "Energy evolution for losses only" annotation (Placement(transformation(
          extent={{-20,-50},{20,-10}}), iconTransformation(extent={{-20,-32},{2,
            -10}})));
equation
  // Connection for losses integrator
  integrator_losses.u = if Heat_flow <= 0 then Heat_flow else 0;
  integrator_cumul.u = Heat_flow;

  connect(integrator_cumul.y, Energy_cumulated) annotation (Line(
      points={{-39,30},{0,30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(integrator_losses.y, Energy_losses) annotation (Line(
      points={{-39,-30},{0,-30}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -40},{-20,40}}),          graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-40},{-20,40}}),
        graphics={ Rectangle(
          extent={{-100,40},{-20,-40}},
          lineColor={0,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-92,12},{-24,-10}},
          lineColor={0,128,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="%name")}),
    Documentation(info="<html>
<p>Integration a fluid heat transfer only a specific direction using a constant fluid capacity. </p>
</html>"));
end Integrator_losses;
