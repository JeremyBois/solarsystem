within SolarSystem.Utilities.Other;
model Integrator_energy
  "Integration of energy. Positive when Tin-Tout and m_flow are > 0 or if when Tin-Tout and m_flow are < 0, negative otherwise"

  Modelica.Blocks.Interfaces.RealInput Tin "Inlet temperature" annotation (
      Placement(transformation(extent={{-120,30},{-80,70}}),
        iconTransformation(extent={{-120,38},{-94,64}})));
  Modelica.Blocks.Interfaces.RealInput Tout "Outlet temperature"
    annotation (Placement(transformation(extent={{-120,60},{-80,100}}),
        iconTransformation(extent={{-120,74},{-94,100}})));
  Modelica.Blocks.Interfaces.RealInput m_flow "Mass flow rate inside T1 and T2"
                                      annotation (Placement(
        transformation(extent={{-120,0},{-80,40}}),    iconTransformation(
          extent={{-120,0},{-94,26}})));
protected
  Modelica.Blocks.Continuous.Integrator integrator
    annotation (Placement(transformation(extent={{-40,40},{-20,60}})));

public
  Modelica.Blocks.Interfaces.RealOutput Energy( each unit="J")
    "Energy evolution"
    annotation (__Dymola_tag={"Energy"}, Placement(transformation(extent={{-6,30},{34,70}}),
        iconTransformation(extent={{0,34},{30,64}})));

  parameter Modelica.SIunits.SpecificHeatCapacity Cp_value=4180
    "Value of Real output";
equation
  // Power calculation (gain when outlet temperature decrease with positive flow rate)
  integrator.u = m_flow * Cp_value * (Tin - Tout)
    "Compute power gain inside collector";
  connect(integrator.y, Energy) annotation (Line(
      points={{-19,50},{14,50}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,0},
            {0,100}})),                          Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,0},{0,100}}),
        graphics={ Rectangle(
          extent={{-100,100},{0,0}},
          lineColor={0,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-86,62},{-8,42}},
          lineColor={0,128,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="%name")}),
    Documentation(info="<html>
<p>Integration a fluid heat transfer only a specific direction using a constant fluid capacity. </p>
</html>"));
end Integrator_energy;
