within SolarSystem.Utilities.Other;
model Horloge
parameter Integer Heure_debut = 6;
parameter Integer Minute_debut = 0;
parameter Integer Seconde_debut = 0;

protected
  Integer Heure[3];

public
  Modelica.Blocks.Interfaces.IntegerOutput sec
    annotation (Placement(transformation(extent={{60,-50},{100,-10}})));
  Modelica.Blocks.Interfaces.IntegerOutput min
    annotation (Placement(transformation(extent={{60,-10},{100,30}})));
  Modelica.Blocks.Interfaces.IntegerOutput H
    annotation (Placement(transformation(extent={{60,30},{100,70}})));

equation
  H=Heure[1];
  min=Heure[2];
  sec=Heure[3];

  Heure = integer(SolarSystem.Data.Function.AlgoHorloge(
      Heure_debut,
      Minute_debut,
      Seconde_debut,
      time));

  annotation (Diagram(graphics), Documentation(info="<html>
<p>Simulate an horloge.</p>
</html>"));
end Horloge;
