within SolarSystem.Utilities.Other;
model Integrator_energy_air "Integration of energy. Positive when Tout-Tin and m_flow are > 0 or when Tout-Tin and m_flow are < 0, negative otherwise.
  Can be use for exchanger where power come from another source than inputs temperature."

  Modelica.Blocks.Interfaces.RealInput Tin "Low temperature" annotation (
      Placement(transformation(extent={{-120,-10},{-80,30}}),
        iconTransformation(extent={{-110,-4},{-84,22}})));
  Modelica.Blocks.Interfaces.RealInput Tout "Hight temperature"
    annotation (Placement(transformation(extent={{-120,20},{-80,60}}),
        iconTransformation(extent={{-110,26},{-84,52}})));
  Modelica.Blocks.Interfaces.RealInput m_flow "Mass flow rate inside T1 and T2"
                                      annotation (Placement(
        transformation(extent={{-120,-46},{-80,-6}}),  iconTransformation(
          extent={{-110,-40},{-84,-14}})));
protected
  Modelica.Blocks.Continuous.Integrator integrator
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));

public
  Modelica.Blocks.Interfaces.RealOutput Energy( unit="J") "Energy evolution"
    annotation (__Dymola_tag={"Energy"}, Placement(transformation(extent={{-26,-20},{14,20}}),
        iconTransformation(extent={{-20,-20},{6,6}})));
  Modelica.Blocks.Interfaces.RealInput Cp_air annotation (Placement(
        transformation(extent={{-120,-80},{-80,-40}}),  iconTransformation(
          extent={{-110,-74},{-84,-48}})));
equation
  // Power calculation on air (gain when outlet temperature increase with positive flow rate)
  integrator.u = m_flow * Cp_air * (Tout - Tin)
    "Compute power gain inside collector";
  connect(integrator.y, Energy) annotation (Line(
      points={{-39,0},{-6,0}},
      color={0,0,127},
      smooth=Smooth.None));

  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -80},{-20,60}}),          graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-80},{-20,60}}),
        graphics={ Rectangle(
          extent={{-100,60},{-20,-80}},
          lineColor={0,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-90,6},{-22,-20}},
          lineColor={0,128,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="%name")}),
    Documentation(info="<html>
<p>Integration a fluid heat transfer only a specific direction using a constant fluid capacity. </p>
</html>"));
end Integrator_energy_air;
