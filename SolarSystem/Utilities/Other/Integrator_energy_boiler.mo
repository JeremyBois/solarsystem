within SolarSystem.Utilities.Other;
model Integrator_energy_boiler
  "Only integrate in a specific direction (when mass flow rate >= 0)"

  Modelica.Blocks.Interfaces.RealInput Tin "Low temperature" annotation (
      Placement(transformation(extent={{-120,10},{-80,50}}),
        iconTransformation(extent={{-120,0},{-80,40}})));
  Modelica.Blocks.Interfaces.RealInput Tout "Hight temperature"
    annotation (Placement(transformation(extent={{-120,60},{-80,100}}),
        iconTransformation(extent={{-120,60},{-80,100}})));
  Modelica.Blocks.Interfaces.RealInput m_flow "Mass flow rate inside T1 and T2"
                                      annotation (Placement(
        transformation(extent={{-120,-50},{-80,-10}}), iconTransformation(
          extent={{-120,-60},{-80,-20}})));
protected
  Modelica.Blocks.Math.Add add(k2=-1)
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualThreshold
    annotation (Placement(transformation(extent={{-64,-40},{-44,-20}})));
  Modelica.Blocks.Logical.Switch switch1
    annotation (Placement(transformation(extent={{-12,-40},{8,-20}})));
  Modelica.Blocks.Sources.RealExpression zero_flow
    annotation (Placement(transformation(extent={{-80,-94},{-60,-74}})));
  Modelica.Blocks.Math.MultiProduct multiProduct(nu=3)
    annotation (Placement(transformation(extent={{26,10},{42,26}})));
  Modelica.Blocks.Sources.RealExpression Cp(y=Cp_value)
    annotation (Placement(transformation(extent={{-20,-92},{0,-72}})));
  Modelica.Blocks.Continuous.Integrator integrator
    annotation (Placement(transformation(extent={{50,8},{70,28}})));
  Modelica.Blocks.Math.Abs abs1
    annotation (Placement(transformation(extent={{-26,40},{-6,60}})));

public
  Modelica.Blocks.Interfaces.RealOutput Energy "Energy evolution"
    annotation (Placement(transformation(extent={{80,-20},{120,20}}),
        iconTransformation(extent={{80,-20},{120,20}})));

  parameter Modelica.SIunits.SpecificHeatCapacity Cp_value=4180
    "Value of Real output";
protected
  Modelica.Blocks.Logical.And Conditions
    annotation (Placement(transformation(extent={{-32,-34},{-24,-26}})));
public
  Modelica.Blocks.Interfaces.RealInput extra_valve "Valve position" annotation (
     Placement(transformation(extent={{-120,-80},{-80,-40}}),
        iconTransformation(extent={{-120,-100},{-80,-60}})));
protected
  Modelica.Blocks.Logical.LessThreshold         greaterEqualThreshold1(threshold=
       0.5)
    annotation (Placement(transformation(extent={{-64,-70},{-44,-50}})));
equation
  connect(m_flow, greaterEqualThreshold.u) annotation (Line(
      points={{-100,-30},{-66,-30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(m_flow, switch1.u1) annotation (Line(
      points={{-100,-30},{-72,-30},{-72,0},{-32,0},{-32,-22},{-14,-22}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(zero_flow.y, switch1.u3) annotation (Line(
      points={{-59,-84},{-30,-84},{-30,-38},{-14,-38}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tout, add.u1) annotation (Line(
      points={{-100,80},{-70,80},{-70,56},{-62,56}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Tin, add.u2) annotation (Line(
      points={{-100,30},{-70,30},{-70,44},{-62,44}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiProduct.y, integrator.u) annotation (Line(
      points={{43.36,18},{48,18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(integrator.y, Energy) annotation (Line(
      points={{71,18},{74,18},{74,0},{100,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(add.y, abs1.u) annotation (Line(
      points={{-39,50},{-28,50}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(abs1.y, multiProduct.u[1]) annotation (Line(
      points={{-5,50},{0,50},{0,21.7333},{26,21.7333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(switch1.y, multiProduct.u[2]) annotation (Line(
      points={{9,-30},{14,-30},{14,18},{26,18}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Cp.y, multiProduct.u[3]) annotation (Line(
      points={{1,-82},{16,-82},{16,14.2667},{26,14.2667}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Conditions.y, switch1.u2) annotation (Line(
      points={{-23.6,-30},{-14,-30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(greaterEqualThreshold.y, Conditions.u1) annotation (Line(
      points={{-43,-30},{-32.8,-30}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(greaterEqualThreshold1.y, Conditions.u2) annotation (Line(
      points={{-43,-60},{-40,-60},{-40,-33.2},{-32.8,-33.2}},
      color={255,0,255},
      smooth=Smooth.None));
  connect(extra_valve, greaterEqualThreshold1.u) annotation (Line(
      points={{-100,-60},{-66,-60}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}),        graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
        graphics={ Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-98,10},{100,-2}},
          lineColor={0,128,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="Integrator")}),
    Documentation(info="<html>
<p>Integration a fluid heat transfer only a specific direction using a constant fluid capacity. </p>
</html>"));
end Integrator_energy_boiler;
