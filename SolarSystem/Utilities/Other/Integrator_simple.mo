within SolarSystem.Utilities.Other;
model Integrator_simple
  "Integration of cumulated heat transfer (losses as positives and gain as negative)"

  Modelica.Blocks.Interfaces.RealInput Heat_flow "Heat flow rate to integrate"
    annotation (Placement(transformation(extent={{-120,-20},{-80,20}}),
        iconTransformation(extent={{-108,-14},{-80,14}})));
protected
  Modelica.Blocks.Continuous.Integrator integrator_cumul(k=k)
    "Gain of -1 in order to have positive energy for lost energy."
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));

public
  Modelica.Blocks.Interfaces.RealOutput Energy( each unit="J")
    "Energy evolution representing losses and gains (lost energy positive)"
    annotation (__Dymola_tag={"Energy"}, Placement(transformation(extent={{-20,-20},{20,20}}),
        iconTransformation(extent={{-20,-10},{4,14}})));
  parameter Real k=-1 "Integrator gain (negative for losses)";
equation

  connect(Heat_flow, integrator_cumul.u) annotation (Line(
      points={{-100,0},{-62,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(integrator_cumul.y, Energy) annotation (Line(
      points={{-39,0},{0,0}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -40},{-20,40}}),          graphics), Icon(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-40},{-20,40}}),
        graphics={ Rectangle(
          extent={{-100,40},{-20,-40}},
          lineColor={0,127,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-92,12},{-24,-10}},
          lineColor={0,128,0},
          fillColor={0,127,0},
          fillPattern=FillPattern.Solid,
          fontName="Consolas",
          textStyle={TextStyle.Bold},
          textString="%name")}),
    Documentation(info="<html>
<p>Integration a fluid heat transfer only a specific direction using a constant fluid capacity. </p>
</html>"));
end Integrator_simple;
