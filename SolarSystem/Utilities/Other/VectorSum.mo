within SolarSystem.Utilities.Other;
model VectorSum

  Modelica.Blocks.Interfaces.RealVectorInput u[length]
    annotation (Placement(transformation(extent={{-110,-20},{-70,20}})));
  Modelica.Blocks.Interfaces.RealOutput y
    annotation (Placement(transformation(extent={{80,-20},{120,20}})));
parameter Integer length=3 "Input length";

algorithm
   for j in 1:length loop
       y := y + u[j];
   end for;
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), Documentation(info="<html>
<p>This model provides a simple way to compute a vector sum.</p>
</html>", revisions="<html>
<ul>
<li>April 17 2010, by Jeremy Bois:<br>First implementation. </li>
</ul>
</html>"));
end VectorSum;
