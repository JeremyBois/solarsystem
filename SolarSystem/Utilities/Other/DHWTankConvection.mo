within SolarSystem.Utilities.Other;
model DHWTankConvection
  "Compute tank convection outside the house model to make integration easier"

  // Surfaces
public
  parameter Modelica.SIunits.Area A[2] "Heat transfer area for each surface";

  // Walls inclinaison
protected
  parameter Modelica.SIunits.Angle F_=
    SolarSystem.Data.Parameter.Construction.Tilt.Floor "Tilt for foor";
  parameter Modelica.SIunits.Angle W_=
    SolarSystem.Data.Parameter.Construction.Tilt.Wall "Tilt for wall";

  Buildings.HeatTransfer.Convection.Interior convSurBou[2](
    final A=A,
    final hFixed={7.7,7.7},
    each conMod=Buildings.HeatTransfer.Types.InteriorConvection.Temperature,
    each final homotopyInitialization=true,
    final til={W_,F_})               "Convective heat transfer"
    annotation (Placement(transformation(extent={{-40,-10},{-60,10}})));
protected
  Modelica.Thermal.HeatTransfer.Components.ThermalCollector theConSurBou(final m=2)
    "Thermal collector to convert from vector to scalar connector"
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={64,0})));
public
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a port_a[2]
    "1=vertical DHW, 2=horizontal DHW"
    annotation (Placement(transformation(extent={{-106,-10},{-86,10}})));
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_b port_b
    annotation (Placement(transformation(extent={{86,-10},{106,10}})));
protected
  Modelica.Blocks.Continuous.Integrator Integrator
    "Integration of tank losses after conduction and convection."
    annotation (Placement(transformation(extent={{20,60},{40,80}})));
public
  Modelica.Blocks.Interfaces.RealOutput DHW_Energy(unit="J")
    "Energy lost from sanitary tank"
    annotation (Placement(transformation(extent={{94,72},{114,92}})));
protected
  Modelica.Blocks.Math.Add cumul annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-10,50})));
  Modelica.Thermal.HeatTransfer.Sensors.HeatFlowSensor heatFlowSensor[2]
    annotation (Placement(transformation(extent={{-20,10},{0,-10}})));

equation
  connect(theConSurBou.port_b, port_b) annotation (Line(points={{74,-1.77636e-015},
          {74,0},{96,0}}, color={191,0,0}));
  connect(port_a, convSurBou.fluid)
    annotation (Line(points={{-96,0},{-60,0}}, color={191,0,0}));
  connect(convSurBou.solid, heatFlowSensor.port_a)
    annotation (Line(points={{-40,0},{-30,0},{-20,0}}, color={191,0,0}));
  connect(heatFlowSensor[1].Q_flow, cumul.u2) annotation (Line(points={{-10,10},
          {-8,10},{-8,38},{-4,38}}, color={0,0,127}));
  connect(heatFlowSensor[2].Q_flow, cumul.u1) annotation (Line(points={{-10,10},
          {-14,10},{-14,38},{-16,38}}, color={0,0,127}));
  connect(Integrator.y, DHW_Energy) annotation (Line(points={{41,70},{68,70},{
          68,82},{104,82}}, color={0,0,127}));
  connect(cumul.y, Integrator.u) annotation (Line(points={{-10,61},{-10,61},{
          -10,70},{18,70}}, color={0,0,127}));
  connect(heatFlowSensor.port_b, theConSurBou.port_a)
    annotation (Line(points={{0,0},{27,0},{54,0}}, color={191,0,0}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end DHWTankConvection;
