within SolarSystem.Utilities.Examples;
model Shadow_test
  extends Modelica.Icons.Example;
  Modelica.Blocks.Sources.RealExpression coef(y=99 + 273.15) "Shadow"
    annotation (Placement(transformation(extent={{-80,-6},{-64,10}})));
  Modelica.Blocks.Sources.RealExpression coef1(y=120 + 273.15) "Shadow"
    annotation (Placement(transformation(extent={{-80,-26},{-64,-10}})));
  Modelica.Blocks.Sources.RealExpression coef2(y=101 + 273.15) "Shadow"
    annotation (Placement(transformation(extent={{-80,16},{-64,32}})));
  Limit.Shadow
         shadow(n=3) annotation (Placement(transformation(extent={{
            -22,-16},{34,16}})));
equation
  connect(coef2.y, shadow.in_value[1]) annotation (Line(
      points={{-63.2,24},{-46,24},{-46,-2.13333},{-24.24,-2.13333}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(coef.y, shadow.in_value[2]) annotation (Line(
      points={{-63.2,2},{-44,2},{-44,1.11022e-016},{-24.24,
          1.11022e-016}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(coef1.y, shadow.in_value[3]) annotation (Line(
      points={{-63.2,-18},{-44,-18},{-44,2.13333},{-24.24,2.13333}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(graphics), experiment(StopTime=3.1536e+007, Interval=360),
    __Dymola_experimentSetupOutput);
end Shadow_test;
