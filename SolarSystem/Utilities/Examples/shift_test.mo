within SolarSystem.Utilities.Examples;
model Shift_test "Test shift implementation"
extends Modelica.Icons.Example;
  Other.Shift shift_right(k=+20)
    annotation (Placement(transformation(extent={{-20,16},{0,36}})));
  Modelica.Blocks.Sources.Constant dump_data(k=22)
    annotation (Placement(transformation(extent={{-90,-6},{-70,14}})));
  Other.Shift shift_left(k=-20)
    annotation (Placement(transformation(extent={{-20,-40},{0,-20}})));
  Other.Shift multi_shift[2](k={10,20})
    annotation (Placement(transformation(extent={{-20,60},{0,80}})));
equation
  connect(dump_data.y, shift_right.u) annotation (Line(points={{-69,4},{-44,4},
          {-44,26},{-22,26}}, color={0,0,127}));
  connect(dump_data.y, shift_left.u) annotation (Line(points={{-69,4},{-44,4},{
          -44,-30},{-22,-30}}, color={0,0,127}));
  connect(dump_data.y, multi_shift[1].u) annotation (Line(points={{-69,4},{-70,
          4},{-60,4},{-60,70},{-22,70}}, color={0,0,127}));
  connect(dump_data.y, multi_shift[2].u) annotation (Line(points={{-69,4},{-60,
          4},{-60,70},{-22,70}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(coordinateSystem(
          preserveAspectRatio=false)));
end Shift_test;
