within SolarSystem.Utilities.Examples;
model OnDelay_test
extends Modelica.Icons.Example;
  Timers.OnDelay_risingEdge onDelay_risingEdge(delayTime=100)
    annotation (Placement(transformation(extent={{40,70},{60,90}})));
  Timers.OnDelay_fallingEdge onDelay_fallingEdge(delayTime=20)
    annotation (Placement(transformation(extent={{40,40},{60,60}})));
  Modelica.Blocks.Sources.SawTooth value(
    amplitude=4,
    offset=-1,
    period=200,
    startTime=10)
               annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
  Modelica.Blocks.Logical.LessThreshold Condition(threshold=2.5)
    "True if inner value lower than 2.5"
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));
  Timers.Stay_on stay_on(pause=190)
    annotation (Placement(transformation(extent={{40,-70},{60,-50}})));
  Timers.OnDelay_pause onDelay_pause(delayTime=190)
    annotation (Placement(transformation(extent={{40,10},{60,30}})));
  Timers.Wait_for wait_for(wait_for=100)
    annotation (Placement(transformation(extent={{40,-100},{60,-80}})));
equation
  connect(value.y,Condition. u) annotation (Line(
      points={{-79,0},{-62,0}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(Condition.y, onDelay_risingEdge.u) annotation (Line(points={{-39,0},{
          0,0},{0,80},{38,80}}, color={255,0,255}));
  connect(Condition.y, onDelay_fallingEdge.u) annotation (Line(points={{-39,0},
          {0,0},{0,50},{36,50},{38,50}}, color={255,0,255}));
  connect(Condition.y, stay_on.in_value) annotation (Line(points={{-39,0},{-40,
          0},{-40,0},{0,0},{0,0},{0,-60},{40,-60},{40,-60}}, color={255,0,255}));
  connect(Condition.y, onDelay_pause.u) annotation (Line(points={{-39,0},{0,0},
          {0,20},{38,20}}, color={255,0,255}));
  connect(Condition.y, wait_for.in_value) annotation (Line(points={{-39,0},{0,0},
          {0,-90},{40,-90}}, color={255,0,255}));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}})));
end OnDelay_test;
