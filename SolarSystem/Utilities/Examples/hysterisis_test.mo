within SolarSystem.Utilities.Examples;
model hysterisis_test
extends Modelica.Icons.Example;
  Modelica.Blocks.Logical.Hysteresis hysteresis(uLow=5, uHigh=15)
    annotation (Placement(transformation(extent={{0,20},{20,40}})));
  Utilities.Logical.Hysteresis_as_input hysteresis_input
    annotation (Placement(transformation(extent={{0,-40},{20,-20}})));
  Modelica.Blocks.Sources.Sine sine(
    offset=0,
    freqHz=1/1000,
    amplitude=20,
    phase=-1.5707963267949)
    annotation (Placement(transformation(extent={{-100,20},{-80,40}})));
  Modelica.Blocks.Sources.Constant sine2(k=5)
    annotation (Placement(transformation(extent={{-100,-60},{-80,-40}})));
  Modelica.Blocks.Sources.Constant sine1(k=15)
    annotation (Placement(transformation(extent={{-100,-20},{-80,0}})));
equation
  connect(sine.y, hysteresis.u) annotation (Line(
      points={{-79,30},{-2,30}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine2.y, hysteresis_input.uLow) annotation (Line(
      points={{-79,-50},{-40,-50},{-40,-36},{-2,-36}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine1.y, hysteresis_input.uHigh) annotation (Line(
      points={{-79,-10},{-40,-10},{-40,-24},{-2,-24}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sine.y, hysteresis_input.u) annotation (Line(
      points={{-79,30},{-20,30},{-20,-30},{-2,-30}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics));
end hysterisis_test;
