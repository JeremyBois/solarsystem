within SolarSystem.Exchanger_and_Tank;
model StratifiedEnhancedInternal_TwoHex
  "A model of a water storage tank with a secondary loop and intenral heat exchanger"
  extends Buildings.Fluid.Storage.StratifiedEnhanced;

  replaceable package MediumHex =
      Modelica.Media.Interfaces.PartialMedium "Medium in the heat exchanger"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Bottom, second row = Top"));

  parameter Modelica.SIunits.Height hHex_a[2]
    "Height of portHex_a of the heat exchanger, measured from tank bottom"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Bottom, second row = Top"));

  parameter Modelica.SIunits.Height hHex_b[2]
    "Height of portHex_b of the heat exchanger, measured from tank bottom"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Bottom, second row = Top"));

  parameter Integer hexSegMult[2](each min=1) = {2, 2}
    "Number of heat exchanger segments in each tank segment"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Bottom, second row = Top"));

  parameter Modelica.SIunits.Diameter dExtHex[2] = {0.025, 0.025}
    "Exterior diameter of the heat exchanger pipe"
    annotation(Dialog(group="Heat exchanger (first row = Bottom, second row = Top"));

  parameter Modelica.SIunits.HeatFlowRate Q_flow_nominal[2] = {1, 1}
    "Heat transfer at nominal conditions"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Bottom, second row = Top"));
  parameter Modelica.SIunits.Temperature TTan_nominal[2]
    "Temperature of fluid inside the tank at nominal heat transfer conditions"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Bottom, second row = Top"));
  parameter Modelica.SIunits.Temperature THex_nominal[2]
    "Temperature of fluid inside the heat exchanger at nominal heat transfer conditions"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Bottom, second row = Top"));
  parameter Real r_nominal[2](each min=0, each max=1)={0.5, 0.5}
    "Ratio between coil inside and outside convective heat transfer at nominal heat transfer conditions"
          annotation(Dialog(tab="General", group="Heat exchanger (first row = Bottom, second row = Top"));

  parameter Modelica.SIunits.MassFlowRate mHex_flow_nominal[2]
    "Nominal mass flow rate through the heat exchanger"
    annotation(Dialog(group="Heat exchanger (first row = Bottom, second row = Top"));

  parameter Modelica.SIunits.Pressure dpHex_nominal[2](displayUnit="Pa") = {2500, 2500}
    "Pressure drop across the heat exchanger at nominal conditions"
    annotation(Dialog(group="Heat exchanger (first row = Bottom, second row = Top"));

  parameter Boolean computeFlowResistance[2]={true, true}
    "=true, compute flow resistance. Set to false to assume no friction"
    annotation (Dialog(tab="Flow resistance heat exchanger"));

  parameter Boolean from_dp[2]={false, false}
    "= true, use m_flow = f(dp) else dp = f(m_flow)"
    annotation (Dialog(tab="Flow resistance heat exchanger"));

  parameter Boolean linearizeFlowResistance[2]={false, false}
    "= true, use linear relation between m_flow and dp for any flow rate"
    annotation (Dialog(tab="Flow resistance heat exchanger"));

  parameter Real deltaM[2]={0.1, 0.1}
    "Fraction of nominal flow rate where flow transitions to laminar"
    annotation (Dialog(tab="Flow resistance heat exchanger"));

  parameter Modelica.Fluid.Types.Dynamics energyDynamicsHex=
    Modelica.Fluid.Types.Dynamics.DynamicFreeInitial
    "Formulation of energy balance"
    annotation(Evaluate=true, Dialog(tab = "Dynamics heat exchanger", group="Equations"));
  parameter Modelica.Fluid.Types.Dynamics massDynamicsHex=
    energyDynamicsHex "Formulation of mass balance"
    annotation(Evaluate=true, Dialog(tab = "Dynamics heat exchanger", group="Equations"));

  parameter Modelica.SIunits.Length lHex[2]=
    {rTan*abs(segHex_a[1]-segHex_b[1])*Modelica.Constants.pi,
     rTan*abs(segHex_a[2]-segHex_b[2])*Modelica.Constants.pi}
    "Approximate length of the heat exchanger"
     annotation(Dialog(tab = "Dynamics heat exchanger", group="Equations"));

  parameter Modelica.SIunits.Area ACroHex[2]=
    {(dExtHex[1]^2-(0.8*dExtHex[1])^2)*Modelica.Constants.pi/4,
    (dExtHex[2]^2-(0.8*dExtHex[2])^2)*Modelica.Constants.pi/4}
    "Cross sectional area of the heat exchanger"
    annotation(Dialog(tab = "Dynamics heat exchanger", group="Equations"));

  parameter Modelica.SIunits.SpecificHeatCapacity cHex[2]={490, 490}
    "Specific heat capacity of the heat exchanger material"
    annotation(Dialog(tab = "Dynamics heat exchanger", group="Equations"));

  parameter Modelica.SIunits.Density dHex[2]={8000, 8000}
    "Density of the heat exchanger material"
    annotation(Dialog(tab = "Dynamics heat exchanger", group="Equations"));

  parameter Modelica.SIunits.HeatCapacity CHex[2]=
    {ACroHex[1]*lHex[1]*dHex[1]*cHex[1],
     ACroHex[2]*lHex[2]*dHex[2]*cHex[2]}
    "Capacitance of the heat exchanger without the fluid"
    annotation(Dialog(tab = "Dynamics heat exchanger", group="Equations"));

  Modelica.Fluid.Interfaces.FluidPort_a portHex_a1(redeclare final package
      Medium = MediumHex) "Heat exchanger inlet" annotation (Placement(
        transformation(extent={{-110,-48},{-90,-28}}), iconTransformation(
          extent={{-110,-48},{-90,-28}})));
  Modelica.Fluid.Interfaces.FluidPort_b portHex_b1(redeclare final package
      Medium = MediumHex) "Heat exchanger outlet" annotation (Placement(
        transformation(extent={{-110,-90},{-90,-70}}), iconTransformation(
          extent={{-110,-90},{-90,-70}})));

  Buildings.Fluid.Storage.BaseClasses.IndirectTankHeatExchanger indTanHex1(
    final nSeg=nSegHex[1],
    final CHex=CHex[1],
    final Q_flow_nominal=Q_flow_nominal[1],
    final TTan_nominal=TTan_nominal[1],
    final THex_nominal=THex_nominal[1],
    final r_nominal=r_nominal[1],
    redeclare final package MediumTan = Medium,
    redeclare final package MediumHex = MediumHex,
    final dp_nominal=dpHex_nominal[1],
    final m_flow_nominal=mHex_flow_nominal[1],
    final energyDynamics=energyDynamicsHex,
    final massDynamics=massDynamicsHex,
    m_flow_small=1e-4*abs(mHex_flow_nominal[1]),
    final computeFlowResistance=computeFlowResistance[1],
    from_dp=from_dp[1],
    final linearizeFlowResistance=linearizeFlowResistance[1],
    final deltaM=deltaM[1],
    final volHexFlu=volHexFlu[1],
    final dExtHex=dExtHex[1]) "Heat exchanger inside the tank"
                                                             annotation (Placement(
        transformation(
        extent={{-10,-15},{10,15}},
        rotation=180,
        origin={-87,32})));

protected
  final parameter Integer segHex_a[2] = {nSeg-integer(hHex_a[1]/segHeight),
                                         nSeg-integer(hHex_a[2]/segHeight)}
    "Tank segment in which port a1 of the heat exchanger is located in"
    annotation(Evaluate=true, Dialog(group="Heat exchanger (first row = Bottom, second row = Top"));

  final parameter Integer segHex_b[2] = {nSeg-integer(hHex_b[1]/segHeight),
                                         nSeg-integer(hHex_b[2]/segHeight)}
    "Tank segment in which port b1 of the heat exchanger is located in"
    annotation(Evaluate=true, Dialog(group="Heat exchanger (first row = Bottom, second row = Top"));

  final parameter Modelica.SIunits.Height segHeight = hTan/nSeg
    "Height of each tank segment (relative to bottom of same segment)";

  final parameter Modelica.SIunits.Length dHHex[2] = {abs(hHex_a[1]-hHex_b[1]),
                                                      abs(hHex_a[2]-hHex_b[2])}
    "Vertical distance between the heat exchanger inlet and outlet";

  final parameter Modelica.SIunits.Volume volHexFlu[2]=
    {Modelica.Constants.pi * (0.8*dExtHex[1])^2/4 *lHex[1],
     Modelica.Constants.pi * (0.8*dExtHex[2])^2/4 *lHex[2]}
    "Volume of the heat exchanger";

  final parameter Integer nSegHexTan[2] = {abs(segHex_a[1]-segHex_b[1]) + 1,
                                           abs(segHex_a[2]-segHex_b[2]) + 1}
    "Number of tank segments the heat exchanger resides in";

  final parameter Integer nSegHex[2] = {nSegHexTan[1]*hexSegMult[1],
                                        nSegHexTan[2]*hexSegMult[2]}
    "Number of heat exchanger segments";
public
  Buildings.Fluid.Storage.BaseClasses.IndirectTankHeatExchanger indTanHex2(
    final nSeg=nSegHex[2],
    final CHex=CHex[2],
    final volHexFlu=volHexFlu[2],
    final Q_flow_nominal=Q_flow_nominal[2],
    final TTan_nominal=TTan_nominal[2],
    final THex_nominal=THex_nominal[2],
    final r_nominal=r_nominal[2],
    redeclare final package MediumTan = Medium,
    redeclare final package MediumHex = MediumHex,
    final dp_nominal=dpHex_nominal[2],
    final m_flow_nominal=mHex_flow_nominal[2],
    final energyDynamics=energyDynamicsHex,
    final massDynamics=massDynamicsHex,
    m_flow_small=1e-4*abs(mHex_flow_nominal[2]),
    final computeFlowResistance=computeFlowResistance[2],
    from_dp=from_dp[2],
    final linearizeFlowResistance=linearizeFlowResistance[2],
    final deltaM=deltaM[2],
    final dExtHex=dExtHex[2]) "Heat exchanger inside the tank"
                                                             annotation (Placement(
        transformation(
        extent={{-10,15},{10,-15}},
        rotation=180,
        origin={-13,92})));
  Modelica.Fluid.Interfaces.FluidPort_a portHex_a2(
    redeclare final package Medium =MediumHex) "Heat exchanger inlet"
   annotation (Placement(transformation(extent={{90,82},{110,102}}),
                   iconTransformation(extent={{90,40},{110,60}})));
  Modelica.Fluid.Interfaces.FluidPort_b portHex_b2(redeclare final package
      Medium = MediumHex) "Heat exchanger outlet" annotation (Placement(
        transformation(extent={{90,14},{110,34}}), iconTransformation(extent={{90,16},
            {110,36}})));

initial equation
   assert(hHex_a[1] >= 0 and hHex_a[1] <= hTan,
     "The parameter hHex_a is outside its valid range.");
   assert(hHex_a[2] >= 0 and hHex_a[2] <= hTan,
     "The parameter hHex_a is outside its valid range.");

   assert(hHex_b[1] >= 0 and hHex_b[1] <= hTan,
     "The parameter hHex_b is outside its valid range.");
   assert(hHex_b[2] >= 0 and hHex_b[2] <= hTan,
     "The parameter hHex_b is outside its valid range.");

   assert(dHHex[1] > 0,
     "The parameters hHex_a and hHex_b must not be equal.");
   assert(dHHex[2] > 0,
     "The parameters hHex_a and hHex_b must not be equal.");

equation
   for j in 1:nSegHexTan[1] loop
     for i in 1:hexSegMult[1] loop
      connect(indTanHex1.port[(j - 1)*hexSegMult[1] + i], heaPorVol[segHex_a[1] + j -
        1]) annotation (Line(
          points={{-87,41.8},{-20,41.8},{-20,-2.22045e-16},{0,-2.22045e-16}},
          color={191,0,0},
          smooth=Smooth.None));
     end for;
   end for;

   for j in 1:nSegHexTan[2] loop
     for i in 1:hexSegMult[2] loop
      connect(indTanHex2.port[(j - 1)*hexSegMult[2] + i], heaPorVol[segHex_a[2] + j -
        1]) annotation (Line(
          points={{-13,82.2},{-14,82.2},{-14,-2.22045e-016},{0,-2.22045e-016}},
          color={191,0,0},
          smooth=Smooth.None));
     end for;
   end for;
  connect(portHex_a1, indTanHex1.port_a) annotation (Line(
      points={{-100,-38},{-74,-38},{-74,32},{-77,32}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(indTanHex1.port_b, portHex_b1) annotation (Line(
      points={{-97,32},{-100,32},{-100,20},{-76,20},{-76,-80},{-100,-80}},
      color={0,127,255},
      smooth=Smooth.None));

  connect(portHex_a2, indTanHex2.port_a) annotation (Line(
      points={{100,92},{-3,92}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(indTanHex2.port_b, portHex_b2) annotation (Line(
      points={{-23,92},{-28,92},{-28,72},{10,72},{10,86},{72,86},{72,24},{100,24}},
      color={0,127,255},
      smooth=Smooth.None));

           annotation (Line(
      points={{-73.2,69},{-70,69},{-70,28},{-16,28},{-16,-2.22045e-16},{0,-2.22045e-16}},
      color={191,0,0},
      smooth=Smooth.None), Icon(coordinateSystem(preserveAspectRatio=false,
          extent={{-100,-100},{100,100}}), graphics={
        Rectangle(
          extent={{-94,-38},{28,-42}},
          lineColor={255,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-70,-50},{28,-54}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-94,-78},{-68,-82}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-72,-50},{-68,-80}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-4,-48},{28,-50}},
          lineColor={0,0,255},
          fillPattern=FillPattern.Solid,
          fillColor={0,0,255}),
        Rectangle(
          extent={{-4,-42},{28,-46}},
          lineColor={255,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-22,-44},{-2,-48}},
          pattern=LinePattern.None,
          fillColor={255,85,85},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-26,52},{90,48}},
          lineColor={200,0,0},
          fillColor={200,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-26,40},{82,36}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{80,24},{108,20}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{80,40},{84,22}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-26,48},{6,44}},
          lineColor={200,0,0},
          fillColor={200,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-26,42},{6,40}},
          lineColor={0,0,255},
          fillPattern=FillPattern.Solid,
          fillColor={0,0,255}),
        Rectangle(
          extent={{2,46},{22,42}},
          pattern=LinePattern.None,
          fillColor={255,85,85},
          fillPattern=FillPattern.Solid)}),
              Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics),
defaultComponentName = "tan",
Documentation(info = "<html>
<p>
This is a model of a stratified storage tank for thermal energy storage with built-in heat exchanger.
</p>
<p>
See the 
<a href=\"modelica://Buildings.Fluid.Storage.UsersGuide\">
Buildings.Fluid.Storage.UsersGuide</a>
for more information.
</p>
<h4>Limitations</h4>
<p>
The model requires at least 4 fluid segments. Hence, set <code>nSeg</code> to 4 or higher.
</p>
</html>",
revisions = "<html>
<ul>
<li>
April 18, 2014 by Michael Wetter:<br/>
Added missing ceiling function in computation of <code>botHexSeg</code>.
Without this function, this parameter can take on zero, which is wrong
because the Modelica uses one-based arrays.

Revised the model as the old version required the port<sub>a</sub>
of the heat exchanger to be located higher than port<sub>b</sub>. 
This makes sense if the heat exchanger is used to heat up the tank, 
but not if it is used to cool down a tank, such as in a cooling plant.
The following parameters were changed:
<ol>
<li>Changed <code>hexTopHeight</code> to <code>hHex_a</code>.</li>
<li>Changed <code>hexBotHeight</code> to <code>hHex_b</code>.</li>
<li>Changed <code>topHexSeg</code> to <code>segHex_a</code>,
 and made it protected as this is deduced from <code>hHex_a</code>.</li>
<li>Changed <code>botHexSeg</code> to <code>segHex_b</code>,
 and made it protected as this is deduced from <code>hHex_b</code>.</li>
</ol>
The names of the following ports have been changed:
<ol>
<li>Changed <code>port_a1</code> to <code>portHex_a</code>.</li>
<li>Changed <code>port_b1</code> to <code>portHex_b</code>.</li>
</ol>
The conversion script should update old instances of 
this model automatically in Dymola for all of the above changes.
</li>
<li>
May 10, 2013 by Michael Wetter:<br/>
Removed <code>m_flow_nominal_tank</code> which was not used.
</li>
<li>
January 29, 2013 by Peter Grant:<br/>
First implementation.
</li>
</ul>
</html>"));
end StratifiedEnhancedInternal_TwoHex;
