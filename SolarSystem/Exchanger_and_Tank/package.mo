within SolarSystem;
package Exchanger_and_Tank "Tanks with exchanger models"
  extends Modelica.Icons.Package;


annotation (Icon(graphics={
        Rectangle(
          extent={{-28,28},{54,-44}},
          lineColor={0,0,255},
          pattern=LinePattern.None,
          fillColor={0,0,127},
          fillPattern=FillPattern.Solid),
        Rectangle(
        extent={{-34,-44},{64,-52}},
        lineColor={100,100,0},
        pattern=LinePattern.None,
        fillColor={100,100,0},
        fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-80,-22},{42,-26}},
          lineColor={255,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-56,-34},{42,-38}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-80,-62},{-54,-66}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-58,-34},{-54,-64}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-8,-28},{12,-32}},
          pattern=LinePattern.None,
          fillColor={255,85,85},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-28,68},{54,28}},
          lineColor={255,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{64,68},{54,-52}},
          lineColor={0,0,255},
          pattern=LinePattern.None,
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-28,68},{-38,-52}},
          lineColor={0,0,255},
          pattern=LinePattern.None,
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-38,74},{64,66}},
          lineColor={0,0,255},
          pattern=LinePattern.None,
          fillColor={255,255,0},
          fillPattern=FillPattern.Solid)}));
end Exchanger_and_Tank;
