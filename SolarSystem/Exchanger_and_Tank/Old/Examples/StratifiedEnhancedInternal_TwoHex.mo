within SolarSystem.Exchanger_and_Tank.Old.Examples;
model StratifiedEnhancedInternal_TwoHex
  "Example showing the use of StratifiedEnhancedInternalHex"
  extends Modelica.Icons.Example;

  package Medium = Buildings.Media.ConstantPropertyLiquidWater
    "Buildings library model for water";
  package MediumE = Buildings.Media.ConstantPropertyLiquidWater
    "Medium water model";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.153
    "Mass flow rate for pump S6 S5 and splitter";
  Buildings.Fluid.Sources.Boundary_pT boundary(      nPorts=1, redeclare
      package Medium = Medium)                                 annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={106,8})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary2(
    redeclare package Medium = Medium,
    nPorts=1,
    m_flow=0.3,
    T=333.15) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-78,8})));
  Buildings.Fluid.Sources.Boundary_pT boundary3(          redeclare package
      Medium = Medium, nPorts=1)                            annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-74,-28})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senTem( m_flow_nominal=0.1,
      redeclare package Medium = Medium)
    annotation (Placement(transformation(extent={{56,-2},{76,18}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=273.15 + 20.0)
    annotation (Placement(transformation(extent={{-94,42},{-74,62}})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary1(
    use_T_in=true,
    redeclare package Medium = Medium,
    m_flow=0,
    nPorts=1)
    annotation (Placement(transformation(extent={{-58,38},{-38,58}})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{-80,-80},{-60,-60}})));
  Buildings.Fluid.Sources.Boundary_pT boundary5(          redeclare package
      Medium = Medium, nPorts=1)                            annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={90,30})));

  Buildings.Fluid.Sources.MassFlowSource_T boundary6(
    redeclare package Medium = Medium,
    use_T_in=false,
    nPorts=1,
    m_flow=0,
    T=293.15)
    annotation (Placement(transformation(extent={{100,50},{80,70}})));
  StratifiedEnhancedInternalHex_double_old Solar_tank(
    redeclare package Medium = MediumE,
    redeclare package MediumHex = MediumE,
    hexSegMult={2,2},
    CHex={200,200},
    m_flow_nominal=m_flow_nominal,
    VTan(displayUnit="l") = 0.4,
    dExtHex(displayUnit="mm") = {0.0279,0.0337},
    Q_flow_nominal(displayUnit="W") = {25000,53000},
    TTan_nominal(displayUnit="degC") = {333.15,318.15},
    dIns(displayUnit="mm") = 0.1,
    mHex_flow_nominal={m_flow_nominal*1.7,m_flow_nominal},
    hTan=1,
    nSeg=10,
    hexBotHeight={0.6,0.2},
    tau(displayUnit="min") = 120,
    hexTopHeight={0.99,0.3},
    THex_nominal={353.15,283.15})
    annotation (Placement(transformation(extent={{-20,-22},{32,44}})));
equation
  connect(senTem.port_b, boundary.ports[1]) annotation (Line(
      points={{76,8},{86,8},{86,8},{96,8}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(realExpression.y, boundary1.T_in) annotation (Line(
      points={{-73,52},{-60,52}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(boundary1.ports[1], Solar_tank.port_a) annotation (Line(
      points={{-38,48},{-30,48},{-30,11},{-20,11}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Solar_tank.port_b, senTem.port_a) annotation (Line(
      points={{32,11},{44,11},{44,8},{56,8}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary6.ports[1], Solar_tank.port_a2) annotation (Line(
      points={{80,60},{56,60},{56,27.5},{32,27.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Solar_tank.port_b2, boundary5.ports[1]) annotation (Line(
      points={{32,18.26},{56,18.26},{56,30},{80,30}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary2.ports[1], Solar_tank.port_a1) annotation (Line(
      points={{-68,8},{-42,8},{-42,-1.54},{-20,-1.54}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(Solar_tank.port_b1, boundary3.ports[1]) annotation (Line(
      points={{-20,-10.78},{-42,-10.78},{-42,-28},{-64,-28}},
      color={0,127,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), __Dymola_Commands(file=
          "modelica://Buildings/Resources/Scripts/Dymola/Fluid/Storage/Examples/StratifiedEnhancedInternalHex.mos"
        "Simulate and Plot"),
        Documentation(info="<html>
        <p>
        This model provides an example of how the <a href=\"modelica://Buildings.Fluid.Storage.StratifiedEnhancedInternalHex\">
        Buildings.Fluid.Storage.StratifiedEnhancedInternalHex</a> model can be used. In it a constant
        water draw is taken from the tank while a constant flow of hot water is passed through the heat
        exchanger to heat the water in the tank.<br/>
        </p>
        </html>",
        revisions = "<html>
        <ul>
        <li>
        Mar 27, 2013 by Peter Grant:<br/>
        First implementation
        </li>
        </ul>
        </html>"),
    experiment(StopTime=10000),
    __Dymola_experimentSetupOutput);
end StratifiedEnhancedInternal_TwoHex;
