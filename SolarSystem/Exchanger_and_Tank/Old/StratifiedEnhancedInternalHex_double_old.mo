within SolarSystem.Exchanger_and_Tank.Old;
model StratifiedEnhancedInternalHex_double_old
  "A model of a water storage tank with a secondary loop and 2 internal heat exchanger"
  extends Buildings.Fluid.Storage.StratifiedEnhanced;

  replaceable package MediumHex =
      Modelica.Media.Interfaces.PartialMedium
    "Medium in the heat exchanger loop"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Top, second row = Bottom exchanger)"));

  parameter Modelica.SIunits.Height hexTopHeight[2]
    "Height of the top of the heat exchanger"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Top, second row = Bottom exchanger)"));

  parameter Modelica.SIunits.Height hexBotHeight[2]
    "Height of the bottom of the heat exchanger"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Top, second row = Bottom exchanger)"));

  parameter Integer hexSegMult[2] = {2, 2}
    "Number of heat exchanger segments in each tank segment"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Top, second row = Bottom exchanger)"));

  parameter Modelica.SIunits.HeatCapacity CHex[2] = {25.163, 25.163}
    "Capacitance of the heat exchanger"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Top, second row = Bottom exchanger)"));

  parameter Modelica.SIunits.HeatFlowRate Q_flow_nominal[2]
    "Heat transfer at nominal conditions"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Top, second row = Bottom exchanger)"));
  parameter Modelica.SIunits.Temperature TTan_nominal[2]
    "Temperature of fluid inside the tank at nominal heat transfer conditions"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Top, second row = Bottom exchanger)"));
  parameter Modelica.SIunits.Temperature THex_nominal[2]
    "Temperature of fluid inside the heat exchanger at nominal heat transfer conditions"
    annotation(Dialog(tab="General", group="Heat exchanger (first row = Top, second row = Bottom exchanger)"));
  parameter Real r_nominal[2](min=0, max=1)={0.5, 0.5}
    "Ratio between coil inside and outside convective heat transfer at nominal heat transfer conditions"
          annotation(Dialog(tab="General", group="Heat exchanger (first row = Top, second row = Bottom exchanger)"));

  parameter Modelica.SIunits.MassFlowRate mHex_flow_nominal[2]
    "Nominal mass flow rate through the heat exchanger"
    annotation(Dialog(group="Heat exchanger (first row = Top, second row = Bottom exchanger)"));

  parameter Modelica.SIunits.Diameter dExtHex[2] = {0.025, 0.025}
    "Exterior diameter of the heat exchanger pipe"
    annotation(Dialog(group="Heat exchanger (first row = Top, second row = Bottom exchanger)"));

  parameter Modelica.SIunits.Pressure dpHex_nominal[2](displayUnit="Pa") = {2500, 2500}
    "Pressure drop across the heat exchanger at nominal conditions"
    annotation(Dialog(group="Heat exchanger (first column = Top, second column = Bottom exchanger)"));

  parameter Modelica.Fluid.Types.Dynamics energyDynamicsHex=Modelica.Fluid.Types.Dynamics.DynamicFreeInitial
    "Formulation of energy balance"
    annotation(Evaluate=true, Dialog(tab = "Dynamics heat exchanger", group="Equations"));
  parameter Modelica.Fluid.Types.Dynamics massDynamicsHex=energyDynamicsHex
    "Formulation of mass balance"
    annotation(Evaluate=true, Dialog(tab = "Dynamics heat exchanger", group="Equations"));

  parameter Integer topHexSeg[2] = {integer(ceil(hexTopHeight[1]/segHeight[1])), integer(ceil(hexTopHeight[2]/segHeight[2]))}
    "Segment the top of the heat exchanger is located in"
    annotation(Evaluate=true);

  parameter Integer botHexSeg[2] = {integer(hexBotHeight[1]/segHeight[1]), integer(hexBotHeight[2]/segHeight[2])}
    "Segment the bottom of the heat exchanger is located in"
    annotation(Evaluate=true);

  parameter Boolean computeFlowResistance=true
    "=true, compute flow resistance. Set to false to assume no friction"
    annotation (Dialog(tab="Flow resistance heat exchanger"));
  parameter Boolean from_dp=false
    "= true, use m_flow = f(dp) else dp = f(m_flow)"
    annotation (Dialog(tab="Flow resistance heat exchanger"));
  parameter Boolean linearizeFlowResistance=false
    "= true, use linear relation between m_flow and dp for any flow rate"
    annotation (Dialog(tab="Flow resistance heat exchanger"));

  parameter Real deltaM=0.1
    "Fraction of nominal flow rate where flow transitions to laminar"
    annotation (Dialog(tab="Flow resistance heat exchanger"));

  Modelica.Fluid.Interfaces.FluidPort_a port_a1(
    redeclare package Medium =MediumHex) "Heat exchanger inlet"
   annotation (Placement(transformation(extent={{-110,-48},{-90,-28}}),
                   iconTransformation(extent={{-110,-48},{-90,-28}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_b1(
     redeclare package Medium = MediumHex) "Heat exchanger outlet"
   annotation (Placement(transformation(extent={{-110,-76},{-90,-56}}),
        iconTransformation(extent={{-110,-76},{-90,-56}})));

  Buildings.Fluid.Storage.BaseClasses.IndirectTankHeatExchanger indTanHexBottom(
    final volHexFlu=volHexFlu[1],
    final Q_flow_nominal=Q_flow_nominal[1],
    final TTan_nominal=TTan_nominal[1],
    final THex_nominal=THex_nominal[1],
    final dExtHex=dExtHex[1],
    redeclare final package Medium = Medium,
    redeclare final package MediumHex = MediumHex,
    final energyDynamics=energyDynamicsHex,
    final massDynamics=massDynamicsHex,
    m_flow_small=1e-4*abs(mHex_flow_nominal[1]),
    final computeFlowResistance=computeFlowResistance,
    from_dp=from_dp,
    linearizeFlowResistance=linearizeFlowResistance,
    deltaM=deltaM,
    final dp_nominal=dpHex_nominal[2],
    final m_flow_nominal=mHex_flow_nominal[2],
    final nSeg=nSegHex[2],
    final CHex=CHex[2],
    final r_nominal=r_nominal[2]) "Heat exchanger inside the tank"
                                     annotation (Placement(
        transformation(
        extent={{-10,-15},{10,15}},
        rotation=180,
        origin={-87,32})));
  Modelica.Fluid.Interfaces.FluidPort_a port_a2(
    redeclare package Medium =MediumHex) "Heat exchanger inlet"
   annotation (Placement(transformation(extent={{90,80},{110,100}}),
                   iconTransformation(extent={{90,40},{110,60}})));
  Modelica.Fluid.Interfaces.FluidPort_b port_b2(
     redeclare package Medium = MediumHex) "Heat exchanger outlet"
   annotation (Placement(transformation(extent={{90,12},{110,32}}),
        iconTransformation(extent={{90,12},{110,32}})));
  Buildings.Fluid.Storage.BaseClasses.IndirectTankHeatExchanger indTanHexTop(
    final volHexFlu=volHexFlu[2],
    final Q_flow_nominal=Q_flow_nominal[2],
    final TTan_nominal=TTan_nominal[2],
    final THex_nominal=THex_nominal[2],
    final dExtHex=dExtHex[2],
    redeclare final package Medium = Medium,
    redeclare final package MediumHex = MediumHex,
    final energyDynamics=energyDynamicsHex,
    final massDynamics=massDynamicsHex,
    m_flow_small=1e-4*abs(mHex_flow_nominal[2]),
    final computeFlowResistance=computeFlowResistance,
    from_dp=from_dp,
    linearizeFlowResistance=linearizeFlowResistance,
    deltaM=deltaM,
    final dp_nominal=dpHex_nominal[1],
    final m_flow_nominal=mHex_flow_nominal[1],
    final nSeg=nSegHex[1],
    final CHex=CHex[1],
    final r_nominal=r_nominal[1]) "Heat exchanger inside the tank"
                                     annotation (Placement(
        transformation(
        extent={{-10,15},{10,-15}},
        rotation=180,
        origin={-7,94})));
  parameter Modelica.SIunits.Height segHeight[2] = {hTan/nSeg, hTan/nSeg}
    "Height of each tank segment (relative to bottom of same segment)";

  parameter Modelica.SIunits.Volume volHexFlu[2] = {0.25 * Modelica.Constants.pi * dExtHex[1], 0.25 * Modelica.Constants.pi * dExtHex[2]}
    "Volume of the heat exchanger";

  parameter Integer nSegHex[2] = {(topHexSeg[1] - botHexSeg[1] + 1)*hexSegMult[1], (topHexSeg[2] - botHexSeg[2] + 1)*hexSegMult[2]}
    "Number of segments in the heat exchanger";

  parameter Integer nSegHexTan[2] = {topHexSeg[1] - botHexSeg[1] + 1, topHexSeg[2] - botHexSeg[2] + 1}
    "Number of tank segments the heat exchanger resides in";

equation
   for j in 1:nSegHexTan[2] loop
     for i in 1:hexSegMult[2] loop
      connect(indTanHexBottom.port[(j-1)*hexSegMult[2] + i], heaPorVol[nSeg - topHexSeg[2]+j])
      annotation (Line(
       points={{-87,41.8},{-20,41.8},{-20,0},{0,0}},
       color={191,0,0},
       smooth=Smooth.None));
     end for;
   end for;
   for j in 1:nSegHexTan[1] loop
     for i in 1:hexSegMult[1] loop
      connect(indTanHexTop.port[(j-1)*hexSegMult[1] + i], heaPorVol[nSeg - topHexSeg[1]+j])
      annotation (Line(
       points={{-7,84.2},{-20,84.2},{-20,0},{0,0}},
       color={191,0,0},
       smooth=Smooth.None));
     end for;
   end for;
  connect(port_a1, indTanHexBottom.port_a)
                                     annotation (Line(
      points={{-100,-38},{-74,-38},{-74,32},{-77,32}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(indTanHexBottom.port_b, port_b1)
                                     annotation (Line(
      points={{-97,32},{-100,32},{-100,20},{-76,20},{-76,-66},{-100,-66}},
      color={0,127,255},
      smooth=Smooth.None));

  connect(port_a2, indTanHexTop.port_a) annotation (Line(
      points={{100,90},{52,90},{52,94},{3,94}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(port_b2, indTanHexTop.port_b) annotation (Line(
      points={{100,22},{94,22},{94,78},{-24,78},{-24,94},{-17,94}},
      color={0,127,255},
      smooth=Smooth.None));
           annotation (Line(
      points={{-73.2,69},{-70,69},{-70,28},{-16,28},{-16,-2.22045e-16},{0,-2.22045e-16}},
      color={191,0,0},
      smooth=Smooth.None), Icon(coordinateSystem(preserveAspectRatio=false,
          extent={{-100,-100},{100,100}}), graphics={
        Rectangle(
          extent={{-94,-38},{28,-42}},
          lineColor={255,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-70,-50},{28,-54}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-94,-66},{-68,-70}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-72,-50},{-68,-70}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-4,-48},{28,-50}},
          lineColor={0,0,255},
          fillPattern=FillPattern.Solid,
          fillColor={0,0,255}),
        Rectangle(
          extent={{-4,-42},{28,-46}},
          lineColor={255,0,0},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-22,-44},{-2,-48}},
          pattern=LinePattern.None,
          fillColor={255,85,85},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-32,52},{100,48}},
          lineColor={200,0,0},
          fillColor={200,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-32,48},{0,44}},
          lineColor={200,0,0},
          fillColor={200,0,0},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-32,40},{76,36}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{-32,42},{0,40}},
          lineColor={0,0,255},
          fillPattern=FillPattern.Solid,
          fillColor={0,0,255}),
        Rectangle(
          extent={{-10,46},{10,42}},
          pattern=LinePattern.None,
          fillColor={255,85,85},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{76,40},{80,20}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),
        Rectangle(
          extent={{76,22},{102,18}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid)}),
              Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics),
            defaultComponentName = "tan",
            Documentation(info = "<html>
            This model is an extension of 
            <a href=\"Buildings.Fluid.Storage.StratifiedEnhanced\">Buildings.Fluid.Storage.StratifiedEnhanced</a>.<br/>
            <p>
            The modifications consist of adding a heat exchanger 
            (<a href=\"Buildings.Fluid.Storage.BaseClasses.IndirectTankHeatExchanger\">
            Buildings.Fluid.Storage.BaseClasses.IndirectTankHeatExchanger</a>) and fluid ports to connect to the heat exchanger.
            The modifications allow to run a fluid through the tank causing heat transfer to the stored fluid. 
            A typical example is a storage tank in a solar hot water system.
            </p>
            <p>
            The heat exchanger model assumes flow through the inside of a helical coil heat exchanger, 
            and stagnant fluid on the outside. Parameters are used to describe the 
            heat transfer on the inside of the heat exchanger at nominal conditions, and 
            geometry of the outside of the heat exchanger. This information is used to compute 
            an <i>hA</i>-value for each side of the coil. Convection calculations are then performed to identify heat transfer 
            between the heat transfer fluid and the fluid in the tank.
            </p>
            <p>
            Default values describing the heat exchanger are taken from the documentation for a Vitocell 100-B, 300 L tank.
            </p>
            </html>",
            revisions = "<html>
            <ul>
            <li>
            May 10, 2013 by Michael Wetter:<br/>
            Removed <code>m_flow_nominal_tank</code> which was not used.
            </li>
            <li>
            January 29, 2013 by Peter Grant:<br/>
            First implementation.
            </li>
            </ul>
            </html>"));
end StratifiedEnhancedInternalHex_double_old;
