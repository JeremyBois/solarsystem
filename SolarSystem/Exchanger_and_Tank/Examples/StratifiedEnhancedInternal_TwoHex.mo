within SolarSystem.Exchanger_and_Tank.Examples;
model StratifiedEnhancedInternal_TwoHex
  "Example showing the use of StratifiedEnhancedInternalHex"
  import SolarSystem;
  extends Modelica.Icons.Example;

  package MediumE = Buildings.Media.Water "MediumE water model";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal = 0.153
    "Mass flow rate for pump S6 S5 and splitter";
  Buildings.Fluid.Sources.Boundary_pT boundary(      nPorts=1, redeclare
      package Medium = MediumE)                                 annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={106,8})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary2(
    m_flow=0.3,
    nPorts=1,
    redeclare package Medium = MediumE,
    T=333.15) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-78,8})));
  Buildings.Fluid.Sources.Boundary_pT boundary3(          redeclare package
      Medium = MediumE, nPorts=1)                            annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-80,-28})));
  Buildings.Fluid.Sensors.TemperatureTwoPort senTem( m_flow_nominal=0.1,
    redeclare package Medium = MediumE)
    annotation (Placement(transformation(extent={{56,-2},{76,18}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=273.15 + 20.0)
    annotation (Placement(transformation(extent={{-94,42},{-74,62}})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary1(
    use_T_in=true,
    redeclare package Medium = MediumE,
    m_flow=0,
    nPorts=1)
    annotation (Placement(transformation(extent={{-58,38},{-38,58}})));
  inner Modelica.Fluid.System system
    annotation (Placement(transformation(extent={{-80,-80},{-60,-60}})));
  Buildings.Fluid.Sources.Boundary_pT boundary5(          redeclare package
      Medium = MediumE, nPorts=1)                            annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={90,30})));

  Buildings.Fluid.Sources.MassFlowSource_T boundary6(
    redeclare package Medium = MediumE,
    use_T_in=false,
    m_flow=0,
    T=293.15,
    nPorts=1)
    annotation (Placement(transformation(extent={{100,50},{80,70}})));
  SolarSystem.Exchanger_and_Tank.StratifiedEnhancedInternal_TwoHex tan(
    VTan(displayUnit="l") = 0.4,
    dIns(displayUnit="mm") = 0.1,
    nSeg=10,
    tau(displayUnit="min") = 120,
    Q_flow_nominal={25000,53000},
    mHex_flow_nominal={m_flow_nominal*1.7,m_flow_nominal},
    m_flow_nominal=0.01,
    redeclare package Medium = MediumE,
    redeclare package MediumHex = MediumE,
    hTan=1,
    hHex_b={0.2,0.6},
    dExtHex(displayUnit="mm") = {0.0279,0.0337},
    hHex_a={0.3,0.99},
    TTan_nominal={333.15,318.15},
    THex_nominal={353.15,283.15})
    annotation (Placement(transformation(extent={{-24,-26},{38,48}})));

equation
  connect(senTem.port_b, boundary.ports[1]) annotation (Line(
      points={{76,8},{96,8}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(realExpression.y, boundary1.T_in) annotation (Line(
      points={{-73,52},{-60,52}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(boundary6.ports[1], tan.portHex_a2) annotation (Line(
      points={{80,60},{52,60},{52,29.5},{38,29.5}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary5.ports[1], tan.portHex_b2) annotation (Line(
      points={{80,30},{60,30},{60,20.62},{38,20.62}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(tan.port_b, senTem.port_a) annotation (Line(
      points={{38,11},{50,11},{50,8},{56,8}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary1.ports[1], tan.port_a) annotation (Line(
      points={{-38,48},{-32,48},{-32,11},{-24,11}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary2.ports[1], tan.portHex_a1) annotation (Line(
      points={{-68,8},{-40,8},{-40,-3.06},{-24,-3.06}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(boundary3.ports[1], tan.portHex_b1) annotation (Line(
      points={{-70,-28},{-40,-28},{-40,-18.6},{-24,-18.6}},
      color={0,127,255},
      smooth=Smooth.None));
  annotation (Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics), __Dymola_Commands(file=
          "modelica://Buildings/Resources/Scripts/Dymola/Fluid/Storage/Examples/StratifiedEnhancedInternalHex.mos"
        "Simulate and Plot"),
        Documentation(info="<html>
        <p>
        This model provides an example of how the <a href=\"modelica://Buildings.Fluid.Storage.StratifiedEnhancedInternalHex\">
        Buildings.Fluid.Storage.StratifiedEnhancedInternalHex</a> model can be used. In it a constant
        water draw is taken from the tank while a constant flow of hot water is passed through the heat
        exchanger to heat the water in the tank.<br/>
        </p>
        </html>",
        revisions = "<html>
        <ul>
        <li>
        Mar 27, 2013 by Peter Grant:<br/>
        First implementation
        </li>
        </ul>
        </html>"),
    experiment(StopTime=10000),
    __Dymola_experimentSetupOutput);
end StratifiedEnhancedInternal_TwoHex;
