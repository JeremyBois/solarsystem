# Fichiers météos

***Mise à jour: 20151130***


## Météo:
Ce dossier (`\Meteo\`) contient des fichiers provenant de Météo France
et des `.epw` provenant du site d’Energy Plus ou de Meteonorm.

### Météo France:
Fichiers météos provenant de MeteoFrance transformé en .mos.

Voir script pour plus de détail.
`Cours-Python\Work\Tools\weather_transform.py`

***Mérignac:***
Ces fichiers sont le résultat d’un mix entre le epw de Bordeaux et les fichiers
de la station météo de Mérignac.
On récupère le Global, le Direct, le Diffus et le température extérieure du fichier
de météo France.

### Meteonorm:
Les fichiers venant de météonorm ne sont pas directement compatible avec le script
de conversion java pour la transformation en .mos.
Il faut ajouter les 3 dernières colonnes (remplir de 0 est suffisant vu l’intérêt de ces colonnes)
Il faut remplacer la colonne Z (correspond à `ceiling_height`) par une version de fichier fonctionnant à la conversion.
Il est aussi important de vérifier que des <""> ne soit pas inséré à cause du traitement par excel/calc (virgule = séparateur en français)

## Conversion rapide
***On se place dans le dossier contenant l’exécutable de java***
cd "C:\Program Files (x86)\Java\jre1.8.0_60\bin"

***On lance la conversion du fichier météo avec le script disponible dans la bibliothèque Buildings***
.\java.exe -jar "D:\Github\modelica-buildings\Buildings\Resources\bin\ConvertWeatherData.jar" <CHEMIN VERS EPW>
