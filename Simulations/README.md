# Simulations

***Mise à jour: 20160209***


## Fonction:
Ce dossier contient l’ensemble des simulations faites avec **Dymola**.
Le dossier principal est `Base`, les `core_<entier>` sont utilisés en cas de
simulations multiples.
En effet Dymola ne peut pas simuler un ensemble de simulations dans le même dossier
car il construit un ensemble de fichier pour simuler le modèle.


## Informations:

***Temps de début/fin de simulation (toujours à minuit):***

 - 1er Octobre   = 23587200    sec
 - 30 Avril      = 10281600    sec
 - 31 Mars       = 7689600     sec
 - Année entière = 365*24*3600 sec
 - 1er Octobre moins 1 semaine ---> 31 Mars (22982400 ---> 39225600)

## Tests:

#### 12_test13 ####

    Dynamic       = S2
    Filter        = None
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (top of model)
    Dymola        = 15.1
    Parallel      = Non
    StepSize      = 600

**Non**

#### 12_test14 ####

    Dynamic       = S2 V3V
    Filter        = None
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (top of model)
    Dymola        = 15.1
    Parallel      = Non
    StepSize      = 600

**Non**


#### 12_test15 ####

    Dynamic       = S2
    Filter        = S2 S2-V2V S6 S6-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (top of model)
    Dymola        = 15.1
    Parallel      = Non
    StepSize      = 600

**Non**


#### 12_test16 ####

    Dynamic       = S2 V3V S6
    Filter        = S2 S2-V2V S6 S6-V2V V3V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (top of model)
    Dymola        = 15.1
    Parallel      = Non
    StepSize      = 600

**Non**


#### 12_test17 ####

    Dynamic       = S2 S6
    Filter        = S2 S2-V2V S6 S6-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 60
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (top of model)
    Dymola        = 15.1
    Parallel      = Non
    StepSize      = 600

**Non**


#### 12_test18 ####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (top of model)
    Dymola        = 15.1
    Parallel      = Non
    StepSize      = 600

**Result = 47 000sec**


#### 12_test19 ####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = True
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (top of model)
    Dymola        = 15.1
    Parallel      = Non
    StepSize      = 600

**Non**


#### 12_test20 ####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between V3V and Collector
    Dymola        = 15.1
    Parallel      = Non
    StepSize      = 600

**Non**


#### 12_test21 ####

    Dynamic       = S2 V3V
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between V3V and Collector
    Dymola        = 15.1
    Parallel      = Non
    StepSize      = 600

**Non**


#### 12_test22 ####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (top of model)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 600

**Result = 45 000sec**


#### 12_test22 ####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (top of model)
    Dymola        = 16.1
    Parallel      = Oui
    StepSize      = 600

**Non**


<!-- ### Les simulations qui suivent sont sans ECS ... ###
**... et le temps de simulation n’est donc pas le bon**

#### 12_test23 ####
a.k.a Bordeaux-4p-33incl-IDMK_20160128

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (between splitters)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 180

**Best Result = 16 000sec soit 4.4 heures**


#### 12_test24####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (between splitters)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 60

**Result = 17 000sec**


#### 12_test25####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (between splitters)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 120

**Result = 17 500sec**


#### 12_test26####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (between splitters)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 240

**Result = 17 500sec**


#### 12_test27####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (between splitters)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 300

**Result = 18 000sec**


#### 12_test28####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (between splitters)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 360

**Result = 18 000sec** -->

#### 12_test29 ####
**À Limoges (bordeaux pour les précédentes)**

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (between splitters)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 180

**Best Result = 38 000sec soit 10.5 heures**



### Modèle 13  ###
#### 13_test1 ####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (between splitters)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 180
    homotopyV3V   = False
    homotopyV2V   = True
    V3VPercent    = True
    V2VPercent    = False

**Result: 3.74e4**


#### 13_test3 ####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (between splitters)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 180
    homotopyV3V   = True
    homotopyV2V   = True
    V3VPercent    = True
    V2VPercent    = True

**Result: 3.61e4**

#### 13_test4 ####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (between splitters)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 180
    homotopyV3V   = False
    homotopyV2V   = False
    V3VPercent    = True
    V2VPercent    = True

**Result: 3.65e4**

#### 13_test5 ####

    Dynamic       = S2
    Filter        = S2 S2-V2V
    riseTime_pump = riseTime_V2V = riseTime_V3V = 30
    Hex from_dp   = False
    DeltaM        = 0.05
    tau           = 15
    T_start       = 289.55
    Surge tank    = between tanks (between splitters)
    Dymola        = 16.1
    Parallel      = Non
    StepSize      = 180
    homotopyV3V   = False
    homotopyV2V   = True
    V3VPercent    = True
    V2VPercent    = True

**Best Result: 3.6e4 soit 10h**
**Débits négatifs très diminués**
